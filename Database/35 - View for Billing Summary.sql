USE [Courier]
GO

/****** Object:  View [dbo].[BillingSummaryView]    Script Date: 11/13/2016 4:42:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[BillingSummaryView]
AS
SELECT Payment.Orders, Payment.AccountType, Payment.Id, Payment.PaymentAmount, Payment.CreatedOn, Customer.CustomerName, Customer.CustomerCompany, Customer.PaymentTypeId, Customer.Salesperson,
             (SELECT TOP (1) PlanName
            FROM  dbo.CustomerPricePlan AS CustomerPricePlan
            WHERE (Customer.PricePlan = Id)) AS PricePlan
FROM  dbo.Payment AS Payment INNER JOIN
         dbo.Customer AS Customer ON Customer.UserName = Payment.Payer
WHERE (Payment.IsSuccessful = 1) AND EXISTS
             (SELECT 1 AS Expr1
            FROM  dbo.[Order] AS Orders
            WHERE (Customer.Id = CustomerId) AND (IsDeleted <> 1) AND (NOT (ConsignmentNumber IS NULL)) AND (NOT (LEN(ConsignmentNumber) = 0)) AND (ConsignmentNumber IN
                         (SELECT value
                        FROM  dbo.SplitStrings_XML(Payment.Orders, ',') AS SplitStrings_XML_1)) AND (0 <> Status) AND (1 <> Status) AND (12 <> Status) AND (8 <> Status) AND (4 <> Status) AND (2 <> Status))

GO
