/*
   Wednesday, January 20, 20163:07:48 PM
   User: sa
   Server: 52.76.161.186
   Database: CDS
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ZipCode
	DROP CONSTRAINT DF_ZipCode_CreatedOn
GO
ALTER TABLE dbo.ZipCode
	DROP CONSTRAINT DF_ZipCode_UpdatedOn
GO
CREATE TABLE dbo.Tmp_ZipCode
	(
	Id int NOT NULL IDENTITY (1, 1),
	PostalCode nvarchar(32) NULL,
	BuildingName nvarchar(1024) NULL,
	UnitNumber nvarchar(32) NULL,
	StreetName nvarchar(1024) NULL,
	BuildingType nvarchar(1024) NULL,
	Longitude float(53) NULL,
	Latitude float(53) NULL,
	DistrictCode nvarchar(16) NULL,
	Zone nvarchar(16) NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ZipCode SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_ZipCode ADD CONSTRAINT
	DF_ZipCode_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_ZipCode ADD CONSTRAINT
	DF_ZipCode_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_ZipCode ON
GO
IF EXISTS(SELECT * FROM dbo.ZipCode)
	 EXEC('INSERT INTO dbo.Tmp_ZipCode (Id, PostalCode, BuildingName, UnitNumber, StreetName, BuildingType, Longitude, Latitude, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, PostalCode, BuildingName, UnitNumber, StreetName, BuildingType, Longitude, Latitude, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.ZipCode WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ZipCode OFF
GO
DROP TABLE dbo.ZipCode
GO
EXECUTE sp_rename N'dbo.Tmp_ZipCode', N'ZipCode', 'OBJECT' 
GO
ALTER TABLE dbo.ZipCode ADD CONSTRAINT
	PK_ZipCode PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.ZipCode', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ZipCode', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ZipCode', 'Object', 'CONTROL') as Contr_Per 