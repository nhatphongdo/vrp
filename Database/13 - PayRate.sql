/*
   Friday, August 5, 201612:28:38 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.PayRate
	(
	Id int NOT NULL IDENTITY (1, 1),
	PaySchemeId int NOT NULL,
	ServiceId int NOT NULL,
	SizeId int NOT NULL,
	Amount decimal(18, 5) NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.PayRate ADD CONSTRAINT
	PK_PayRate PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.PayRate SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PayRate', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PayRate', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PayRate', 'Object', 'CONTROL') as Contr_Per 