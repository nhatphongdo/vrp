IF NOT EXISTS ( SELECT  *
                FROM    SYSCOLUMNS sc
                WHERE   EXISTS ( SELECT id
                                 FROM   [dbo].[sysobjects]
                                 WHERE  NAME LIKE 'CustomerPricePlan'
                                        AND sc.id = id )
                        AND sc.name = 'DefaultPromoCode' ) 
    ALTER TABLE [CustomerPricePlan]  ADD  DefaultPromoCode NVARCHAR(MAX)