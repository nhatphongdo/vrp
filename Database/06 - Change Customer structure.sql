/*
   Sunday, January 31, 20165:13:28 PM
   User: sa
   Server: 52.76.161.186
   Database: CDS
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT FK_Customer_PaymentType
GO
ALTER TABLE dbo.PaymentType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PaymentType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PaymentType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PaymentType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT DF_Customer_Credit
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT DF_Customer_IsActive
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT DF_Customer_PhoneType
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT DF_Customer_IsDeleted
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT DF_Customer_CreatedOn
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT DF_Customer_UpdatedOn
GO
CREATE TABLE dbo.Tmp_Customer
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	UserName nvarchar(256) NULL,
	CustomerName nvarchar(1024) NULL,
	CustomerPhoto nvarchar(1024) NULL,
	CustomerCompany nvarchar(1024) NULL,
	CustomerAddress nvarchar(2048) NULL,
	CustomerCountry nvarchar(32) NULL,
	CustomerPhone nvarchar(128) NULL,
	CustomerZipCode nvarchar(64) NULL,
	PaymentTypeId int NOT NULL,
	Credit decimal(18, 5) NOT NULL,
	IsActive bit NOT NULL,
	Salesperson nvarchar(256) NULL,
	PricePlan int NULL,
	DefaultPromoCode nvarchar(MAX) NULL,
	NotificationKey nvarchar(1024) NULL,
	PhoneType int NOT NULL,
	Remark nvarchar(MAX) NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Customer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Customer ADD CONSTRAINT
	DF_Customer_Credit DEFAULT ((0)) FOR Credit
GO
ALTER TABLE dbo.Tmp_Customer ADD CONSTRAINT
	DF_Customer_IsActive DEFAULT ((1)) FOR IsActive
GO
ALTER TABLE dbo.Tmp_Customer ADD CONSTRAINT
	DF_Customer_PhoneType DEFAULT ((0)) FOR PhoneType
GO
ALTER TABLE dbo.Tmp_Customer ADD CONSTRAINT
	DF_Customer_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_Customer ADD CONSTRAINT
	DF_Customer_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_Customer ADD CONSTRAINT
	DF_Customer_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Customer ON
GO
IF EXISTS(SELECT * FROM dbo.Customer)
	 EXEC('INSERT INTO dbo.Tmp_Customer (Id, UserName, CustomerName, CustomerPhoto, CustomerCompany, CustomerAddress, CustomerCountry, CustomerPhone, CustomerZipCode, PaymentTypeId, Credit, IsActive, Salesperson, PricePlan, DefaultPromoCode, NotificationKey, PhoneType, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, UserName, CustomerName, CustomerPhoto, CustomerCompany, CustomerAddress, CustomerCountry, CustomerPhone, CustomerZipCode, PaymentTypeId, Credit, IsActive, Salesperson, PricePlan, DefaultPromoCode, NotificationKey, PhoneType, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.Customer WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Customer OFF
GO
ALTER TABLE dbo.Invoice
	DROP CONSTRAINT FK_Invoice_Customer
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Customer
GO
DROP TABLE dbo.Customer
GO
EXECUTE sp_rename N'dbo.Tmp_Customer', N'Customer', 'OBJECT' 
GO
ALTER TABLE dbo.Customer ADD CONSTRAINT
	PK_Customer PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Customer ADD CONSTRAINT
	FK_Customer_PaymentType FOREIGN KEY
	(
	PaymentTypeId
	) REFERENCES dbo.PaymentType
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Customer', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Customer', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Customer', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Customer FOREIGN KEY
	(
	CustomerId
	) REFERENCES dbo.Customer
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Invoice ADD CONSTRAINT
	FK_Invoice_Customer FOREIGN KEY
	(
	IssueToCustomerId
	) REFERENCES dbo.Customer
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Invoice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Invoice', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Invoice', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Invoice', 'Object', 'CONTROL') as Contr_Per 