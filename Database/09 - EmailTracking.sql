/*
   Monday, June 27, 201612:50:53 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.EmailTracking
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	EmailType nvarchar(512) NULL,
	OrderId bigint NULL,
	Information nvarchar(MAX) NULL,
	SentOn datetime NOT NULL,
	SentBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.EmailTracking ADD CONSTRAINT
	PK_EmailTracking PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.EmailTracking SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EmailTracking', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EmailTracking', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EmailTracking', 'Object', 'CONTROL') as Contr_Per 