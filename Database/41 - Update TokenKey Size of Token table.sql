/*
   Friday, December 2, 201612:27:59 PM
   User: courier
   Server: 52.74.132.250
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Token
	DROP CONSTRAINT DF_Token_IsExpired
GO
ALTER TABLE dbo.Token
	DROP CONSTRAINT DF_Table_1_ExpireTime
GO
ALTER TABLE dbo.Token
	DROP CONSTRAINT DF_Token_CreatedOn
GO
CREATE TABLE dbo.Tmp_Token
	(
	Id int NOT NULL IDENTITY (1, 1),
	UserName nvarchar(256) NULL,
	TokenKey nvarchar(MAX) NULL,
	BindingValue nvarchar(MAX) NULL,
	IsExpired bit NOT NULL,
	ExpiredOn datetime NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Token SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Token ADD CONSTRAINT
	DF_Token_IsExpired DEFAULT ((0)) FOR IsExpired
GO
ALTER TABLE dbo.Tmp_Token ADD CONSTRAINT
	DF_Table_1_ExpireTime DEFAULT (getdate()) FOR ExpiredOn
GO
ALTER TABLE dbo.Tmp_Token ADD CONSTRAINT
	DF_Token_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Token ON
GO
IF EXISTS(SELECT * FROM dbo.Token)
	 EXEC('INSERT INTO dbo.Tmp_Token (Id, UserName, TokenKey, BindingValue, IsExpired, ExpiredOn, CreatedOn, CreatedBy)
		SELECT Id, UserName, CONVERT(nvarchar(MAX), TokenKey), BindingValue, IsExpired, ExpiredOn, CreatedOn, CreatedBy FROM dbo.Token WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Token OFF
GO
DROP TABLE dbo.Token
GO
EXECUTE sp_rename N'dbo.Tmp_Token', N'Token', 'OBJECT' 
GO
ALTER TABLE dbo.Token ADD CONSTRAINT
	PK_Token PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Token', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Token', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Token', 'Object', 'CONTROL') as Contr_Per 