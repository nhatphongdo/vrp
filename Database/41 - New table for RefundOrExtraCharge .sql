USE [Courier]
GO

/****** Object:  Table [dbo].[RefundOrExtraCharge]    Script Date: 28/11/2016 7:07:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RefundOrExtraCharge](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[ConsignmentNumber] [nvarchar](max) NOT NULL,
	[Reason] [int] NOT NULL,
	[Amount] [decimal](18, 5) NOT NULL,
	[ApprovalStatus] [int] NOT NULL,
	[CreatedBy] [nvarchar](256) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[PaymentCategory] [int] NOT NULL,
 CONSTRAINT [PK_RefundPayment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[RefundOrExtraCharge]  WITH CHECK ADD  CONSTRAINT [FK_RefundOrExtraCharge_ChargingReason] FOREIGN KEY([Reason])
REFERENCES [dbo].[ChargingReason] ([Id])
GO

ALTER TABLE [dbo].[RefundOrExtraCharge] CHECK CONSTRAINT [FK_RefundOrExtraCharge_ChargingReason]
GO

ALTER TABLE [dbo].[RefundOrExtraCharge]  WITH CHECK ADD  CONSTRAINT [FK_RefundOrExtraCharge_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO

ALTER TABLE [dbo].[RefundOrExtraCharge] CHECK CONSTRAINT [FK_RefundOrExtraCharge_Customer]
GO


