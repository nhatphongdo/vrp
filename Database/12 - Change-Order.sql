/*
   Wednesday, August 3, 20166:59:05 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Size
GO
ALTER TABLE dbo.Size SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Size', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Size', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Size', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Service
GO
ALTER TABLE dbo.Service SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Service', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Service', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Service', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_ProductType
GO
ALTER TABLE dbo.ProductType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ProductType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ProductType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ProductType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_PaymentType
GO
ALTER TABLE dbo.PaymentType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PaymentType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PaymentType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PaymentType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Customer
GO
ALTER TABLE dbo.Customer SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Customer', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Customer', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Customer', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_PaymentType
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_Price
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_Type
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_SizeId
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_Status
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_IsDeleted
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_CreatedOn
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_UpdatedOn
GO
CREATE TABLE dbo.Tmp_Order
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	CustomerId bigint NULL,
	BinCode nvarchar(64) NULL,
	ConsignmentNumber nvarchar(256) NULL,
	GroupTrackingNumber nvarchar(256) NULL,
	FromName nvarchar(1024) NULL,
	FromZipCode nvarchar(64) NULL,
	FromAddress nvarchar(2048) NULL,
	FromMobilePhone nvarchar(128) NULL,
	FromSignature nvarchar(1024) NULL,
	ToName nvarchar(1024) NULL,
	ToZipCode nvarchar(64) NULL,
	ToAddress nvarchar(2048) NULL,
	ToMobilePhone nvarchar(128) NULL,
	ToSignature nvarchar(1024) NULL,
	PaymentTypeId int NOT NULL,
	Price decimal(18, 5) NOT NULL,
	ServiceId int NOT NULL,
	ProductTypeId int NOT NULL,
	SizeId int NOT NULL,
	Remark nvarchar(MAX) NULL,
	Status int NOT NULL,
	PickupTimeSlotId int NULL,
	PickupDate datetime NULL,
	DeliveryTimeSlotId int NULL,
	DeliveryDate datetime NULL,
	QrCodePath nvarchar(1024) NULL,
	PromoCode nvarchar(256) NULL,
	ConfirmedOn datetime NULL,
	ConfirmedBy nvarchar(256) NULL,
	RejectNote nvarchar(MAX) NULL,
	CollectedOn datetime NULL,
	CollectedBy nvarchar(256) NULL,
	ProceededOn datetime NULL,
	ProceededBy nvarchar(256) NULL,
	ProceededOutOn datetime NULL,
	ProceededOutBy nvarchar(256) NULL,
	DeliveredOn datetime NULL,
	DeliveredBy nvarchar(256) NULL,
	CanceledOn datetime NULL,
	CanceledBy nvarchar(256) NULL,
	FullfilmentNumber nvarchar(50) NULL,
	OperatorRemark nvarchar(MAX) NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Order SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_PaymentType DEFAULT ((0)) FOR PaymentTypeId
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_Price DEFAULT ((0)) FOR Price
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_Type DEFAULT ((0)) FOR ProductTypeId
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_SizeId DEFAULT ((0)) FOR SizeId
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_Status DEFAULT ((0)) FOR Status
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Order ON
GO
IF EXISTS(SELECT * FROM dbo.[Order])
	 EXEC('INSERT INTO dbo.Tmp_Order (Id, CustomerId, BinCode, ConsignmentNumber, GroupTrackingNumber, FromName, FromZipCode, FromAddress, FromMobilePhone, FromSignature, ToName, ToZipCode, ToAddress, ToMobilePhone, ToSignature, PaymentTypeId, Price, ServiceId, ProductTypeId, SizeId, Remark, Status, PickupTimeSlotId, PickupDate, DeliveryTimeSlotId, DeliveryDate, QrCodePath, PromoCode, ConfirmedOn, ConfirmedBy, RejectNote, CollectedOn, CollectedBy, ProceededOn, ProceededBy, ProceededOutOn, ProceededOutBy, DeliveredOn, DeliveredBy, CanceledOn, CanceledBy, FullfilmentNumber, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, CustomerId, BinCode, ConsignmentNumber, GroupTrackingNumber, FromName, FromZipCode, FromAddress, FromMobilePhone, FromSignature, ToName, ToZipCode, ToAddress, ToMobilePhone, ToSignature, PaymentTypeId, Price, ServiceId, ProductTypeId, SizeId, Remark, Status, PickupTimeSlotId, PickupDate, DeliveryTimeSlotId, DeliveryDate, QrCodePath, PromoCode, ConfirmedOn, ConfirmedBy, RejectNote, CollectedOn, CollectedBy, ProceededOn, ProceededBy, ProceededOutOn, ProceededOutBy, DeliveredOn, DeliveredBy, CanceledOn, CanceledBy, FullfilmentNumber, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.[Order] WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Order OFF
GO
ALTER TABLE dbo.ExtraRequest
	DROP CONSTRAINT FK_ExtraRequest_Order
GO
DROP TABLE dbo.[Order]
GO
EXECUTE sp_rename N'dbo.Tmp_Order', N'Order', 'OBJECT' 
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Customer FOREIGN KEY
	(
	CustomerId
	) REFERENCES dbo.Customer
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_PaymentType FOREIGN KEY
	(
	PaymentTypeId
	) REFERENCES dbo.PaymentType
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_ProductType FOREIGN KEY
	(
	ProductTypeId
	) REFERENCES dbo.ProductType
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Service FOREIGN KEY
	(
	ServiceId
	) REFERENCES dbo.Service
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Size FOREIGN KEY
	(
	SizeId
	) REFERENCES dbo.Size
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ExtraRequest ADD CONSTRAINT
	FK_ExtraRequest_Order FOREIGN KEY
	(
	OrderId
	) REFERENCES dbo.[Order]
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ExtraRequest SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ExtraRequest', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ExtraRequest', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ExtraRequest', 'Object', 'CONTROL') as Contr_Per 