USE [Courier]
GO

/****** Object:  View [dbo].[OrdersView]    Script Date: 11/2/2016 11:54:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[OrdersView]
AS
SELECT distinct O.*,
FromGroup.DistrictCode as FromDistrictCode,FromGroup.Zone as FromZoneCode,ToGroup.DistrictCode as ToDistrictCode,ToGroup.Zone as ToZoneCode,
C.CustomerName, C.CustomerCompany,
RPR.RequestDate as RPRRequestDate,RPR.RequestContent as RPRRequestContent,RDR.RequestDate as RDRRequestDate,RDR.RequestContent as RDRRequestContent,
(select SUM(Amount) from CodOptionOfOrder COO where COO.OrderId=O.Id and COO.IsDeleted = 0) as TotalCodAmount,
(select SUM(CollectedAmount) from CodOptionOfOrder COO where COO.OrderId=O.Id and COO.IsDeleted = 0 and COO.IsCollected = 1) as TotalCodCollectedAmount,
(select CAST(CODT.[Type] AS NVARCHAR(1024)) + ',' from CodOptionOfOrder COO 
 left join CodOption as CODT on CODT.Id = COO.CodOptionId AND CODT.IsDeleted = 0 
 where COO.OrderId=O.Id and COO.IsDeleted = 0
 FOR XML PATH('')) as CodTypes,
(select CAST(CODT.[Type] AS NVARCHAR(1024)) + ',' from CodOptionOfOrder COO 
 left join CodOption as CODT on CODT.Id = COO.CodOptionId AND CODT.IsDeleted = 0 
 where COO.OrderId=O.Id and COO.[IsCollected] = 1 and COO.IsDeleted = 0
 FOR XML PATH('')) as CodCollectedTypes,
case 
	--Item has been picked/Enroute to pickup
	when (O.Status = 12  and IHBPDriver.Id is not null) then IHBPDriver.StaffName
	when (O.Status = 12  and IHBPOPS.Id is not null) then IHBPOPS.ContractorName
	when (O.Status = 12  and IHBPDriver.Id is null and IHBPOPS.Id is null) then JH.CompletedBy
	--Item in warehouse/Item in depot
	when (O.Status = 5  and IIDOPS.Id is not null) then IIDOPS.ContractorName
	when (O.Status = 5  and IIDOPS.Id is null) then O.ProceededBy
	--Pickup Failed
	when (O.Status = 4  and PFDriver.Id is not null) then PFDriver.StaffName
	when (O.Status = 4  and PFOPS.Id is not null) then PFOPS.ContractorName
	when (O.Status = 4  and PFDriver.Id is null and PFOPS.Id is null) then O.CollectedBy
	--Item is delivered 
	when (O.Status = 7  and IDDriver.Id is not null) then IDDriver.StaffName
	when (O.Status = 7  and PFOPS.Id is  null) then O.DeliveredBy
	--Item Collected
	when (O.Status = 3  and ICDriver.Id is not null) then ICDriver.StaffName
	when (O.Status = 3  and ICDriver.Id is null) then O.CollectedBy
	--Item out depot/Delivery in progress
	when (O.Status = 6  and IODDriver.Id is not null) then IODDriver.StaffName
	when (O.Status = 6  and IODOPS.Id is not null) then IODOPS.ContractorName
	when (O.Status = 6  and IODDriver.Id is null) then O.ProceededOutBy
	when (O.Status = 6  and IHBPDriver.Id is null and IHBPOPS.Id is null) then JH.CompletedBy
	--Delivery Failed
	when (O.Status = 9  and DFDriver.Id is not null) then DFDriver.StaffName 
	when (O.Status = 9  and DFOPS.Id is not null) then DFOPS.ContractorName
	when (O.Status = 9  and DFDriver.Id is null and DFOPS.Id is null) then O.DeliveredBy
	--Spl order for pickup
	when (O.Status = 10  and SPPUDriver.Id is not null) then SPPUDriver.StaffName 
	when (O.Status = 10  and SPPUDriver.Id is null) then OT.CreatedBy
	--Spl order for delivery
	when (O.Status = 11  and SPDDriver.Id is not null) then SPDDriver.StaffName 
	when (O.Status = 11  and SPDDriver.Id is null) then OT.CreatedBy
	--Re pickup
	when (O.Status = 13  and RPUDriver.Id is not null) then RPUDriver.StaffName 
	when (O.Status = 13  and RPUDriver.Id is null) then OT.CreatedBy
	--Re delivery
	when (O.Status = 14  and RDDriver.Id is not null) then RDDriver.StaffName 
	when (O.Status = 14  and RDDriver.Id is null) then OT.CreatedBy
	--Return to sender
	when (O.Status = 15  and RSDOPS.Id is not null) then RSDOPS.ContractorName 
	when (O.Status = 15  and RSDOPS.Id is null) then O.UpdatedBy

else ''
  End as DriverOrOpsName,

  case 
    --Item has been picked/Enroute to pickup
	when (O.Status = 12  and IHBPDriver.Id is not null) then IHBPDriver.StaffMobile
	when (O.Status = 12  and IHBPOPS.Id is not null) then IHBPOPS.ContractorMobile
	--Item in warehouse/Item in depot
	when (O.Status = 5  and IIDOPS.Id is not null) then IIDOPS.ContractorMobile
	--Pickup Failed
	when (O.Status = 4  and PFDriver.Id is not null) then PFDriver.StaffMobile
	when (O.Status = 4  and PFOPS.Id is not null) then PFOPS.ContractorMobile
	--Item is delivered 
	when (O.Status = 7  and IDDriver.Id is not null) then IDDriver.StaffMobile
	--Item Collected
	when (O.Status = 3  and ICDriver.Id is not null) then ICDriver.StaffMobile
	--Item out depot/Delivery in progress
	when (O.Status = 6  and IODDriver.Id is not null) then IODDriver.StaffMobile
	--Delivery Failed
	when (O.Status = 9  and DFDriver.Id is not null) then DFDriver.StaffMobile 
	when (O.Status = 9  and DFOPS.Id is not null) then DFOPS.ContractorMobile
	--Spl order for pickup
	when (O.Status = 10  and SPPUDriver.Id is not null) then SPPUDriver.StaffMobile
	--Spl order for delivery
	when (O.Status = 11  and SPDDriver.Id is not null) then SPPUDriver.StaffMobile
	--Re pickup
	when (O.Status = 13  and RPUDriver.Id is not null) then SPDDriver.StaffMobile
	--Re delivery
	when (O.Status = 14  and RDDriver.Id is not null) then RDDriver.StaffMobile
	--Return to sender
	when (O.Status = 15  and RSDOPS.Id is not null) then RSDOPS.ContractorMobile

else ''
  End as DriverOrOpsMobile,

  case 
    --Item has been picked/Enroute to pickup
	when (O.Status = 12) then J.TakenOn
	--Item in warehouse/Item in depot
	when (O.Status = 5) then O.ProceededOn
	--Pickup Failed
	when (O.Status = 4) then O.CollectedOn
	--Item is delivered 
	when (O.Status = 7) then O.DeliveredOn
	--Item Collected 
	when (O.Status = 3) then O.CollectedOn
	--Item out depot/Delivery in progress
	when (O.Status = 6) then O.ProceededOutOn
	--Delivery Failed
	when (O.Status = 9) then O.DeliveredOn
	--Spl order for pickup
	when (O.Status = 10) then OT.CreatedOn 
	--Spl order for delivery
	when (O.Status = 11) then OT.CreatedOn 
	--Re pickup
	when (O.Status = 13) then OT.CreatedOn 
	--Re delivery
	when (O.Status = 14) then OT.CreatedOn 
	--Return to sender
	when (O.Status = 15 ) then O.UpdatedOn


else ''
  End as DriverOrOpsTimeStamp

FROM [order] O
LEFT JOIN ZipCode AS FromGroup on O.FromZipCode = FromGroup.PostalCode
LEFT JOIN ZipCode as ToGroup on O.ToZipCode = ToGroup.PostalCode
LEFT JOIN ExtraRequest as RPR on RPR.OrderId = O.Id and RPR.Id = (SELECT MAX(Id) from ExtraRequest RPRL WHERE RPRL.OrderId = O.Id and RPRL.RequestType = 3 and RPRL.IsDeleted = 0)
LEFT JOIN ExtraRequest as RDR on RDR.OrderId = O.Id and RDR.Id = (SELECT MAX(Id) from ExtraRequest RDRL WHERE RDRL.OrderId = O.Id and RDRL.RequestType = 2 and RDRL.IsDeleted = 0)
LEFT JOIN Customer C on C.Id = O.CustomerId
--LEFT JOIN CodOptionOfOrder as CODO on CODO.OrderId = O.Id and CODO.IsDeleted = 0
--LEFT JOIN CodOption as CODT on CODT.Id = CODO.CodOptionId and  CODT.IsDeleted = 0

LEFT JOIN JobHistory JH on JH.ConsignmentNumber = O.ConsignmentNumber and JH.Id = (SELECT MAX(ID) from JobHistory JHL WHERE JHL.ConsignmentNumber = O.ConsignmentNumber)
LEFT JOIN Job J on J.Id = JH.JobId
LEFT JOIN OrderTracking OT on OT.ConsignmentNumber = O.ConsignmentNumber and OT.Id = (SELECT MAX(ID) from OrderTracking OTL WHERE OTL.ConsignmentNumber = O.ConsignmentNumber and OTL.Status = O.Status)

LEFT JOIN Staff as IHBPDriver on IHBPDriver.UserName = JH.CompletedBy and J.DriverType = 0
LEFT JOIN Contractor as IHBPOPS on IHBPOPS.UserName = JH.CompletedBy and J.DriverType = 1

LEFT JOIN Contractor as IIDOPS on IIDOPS.UserName = O.ProceededBy


LEFT JOIN Staff as PFDriver on PFDriver.UserName = O.CollectedBy
LEFT JOIN Contractor as PFOPS on PFOPS.UserName = O.CollectedBy


LEFT JOIN Staff as IDDriver on IDDriver.UserName = O.DeliveredBy
LEFT JOIN Staff as ICDriver on ICDriver.UserName = O.CollectedBy

--LEFT JOIN Staff as IODDriver on IODDriver.UserName = O.ProceededOutBy
LEFT JOIN Staff as IODDriver on IODDriver.UserName = JH.CompletedBy and J.DriverType = 0
LEFT JOIN Contractor as IODOPS on IODOPS.UserName = JH.CompletedBy and J.DriverType = 1

LEFT JOIN Staff as DFDriver on DFDriver.UserName = O.DeliveredBy
LEFT JOIN Contractor as DFOPS on DFOPS.UserName = O.DeliveredBy
LEFT JOIN Staff as SPPUDriver on SPPUDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as SPDDriver on SPDDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as RPUDriver on RPUDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as RDDriver on RDDriver.UserName = OT.CreatedBy
LEFT JOIN Contractor as RSDOPS on RSDOPS.UserName = O.UpdatedBy
WHERE O.IsDeleted = 0 

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -747
         Left = 0
      End
      Begin Tables = 
         Begin Table = "O"
            Begin Extent = 
               Top = 12
               Left = 76
               Bottom = 259
               Right = 436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FromGroup"
            Begin Extent = 
               Top = 12
               Left = 512
               Bottom = 259
               Right = 787
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ToGroup"
            Begin Extent = 
               Top = 12
               Left = 863
               Bottom = 259
               Right = 1138
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RPR"
            Begin Extent = 
               Top = 12
               Left = 1214
               Bottom = 259
               Right = 1502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RDR"
            Begin Extent = 
               Top = 12
               Left = 1578
               Bottom = 259
               Right = 1866
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 12
               Left = 1942
               Bottom = 259
               Right = 2271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JH"
            Begin Extent = 
               Top = 264
               Left = 499
               Bottom = 511
               Right = 846
            End
            DisplayFlags = 280
            ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'OrdersView'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'TopColumn = 0
         End
         Begin Table = "J"
            Begin Extent = 
               Top = 264
               Left = 922
               Bottom = 511
               Right = 1234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "OT"
            Begin Extent = 
               Top = 264
               Left = 1310
               Bottom = 511
               Right = 1657
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IHBPDriver"
            Begin Extent = 
               Top = 264
               Left = 1733
               Bottom = 511
               Right = 2067
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "IHBPOPS"
            Begin Extent = 
               Top = 264
               Left = 2143
               Bottom = 511
               Right = 2456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IIDOPS"
            Begin Extent = 
               Top = 264
               Left = 2532
               Bottom = 511
               Right = 2845
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PFDriver"
            Begin Extent = 
               Top = 516
               Left = 76
               Bottom = 763
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PFOPS"
            Begin Extent = 
               Top = 516
               Left = 486
               Bottom = 763
               Right = 799
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IDDriver"
            Begin Extent = 
               Top = 516
               Left = 875
               Bottom = 763
               Right = 1209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ICDriver"
            Begin Extent = 
               Top = 516
               Left = 1285
               Bottom = 763
               Right = 1619
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IODDriver"
            Begin Extent = 
               Top = 516
               Left = 1695
               Bottom = 763
               Right = 2029
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IODOPS"
            Begin Extent = 
               Top = 516
               Left = 2105
               Bottom = 763
               Right = 2418
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DFDriver"
            Begin Extent = 
               Top = 516
               Left = 2494
               Bottom = 763
               Right = 2828
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DFOPS"
            Begin Extent = 
               Top = 768
               Left = 76
               Bottom = 1015
               Right = 389
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SPPUDriver"
            Begin Extent = 
               Top = 768
               Left = 465
               Bottom = 1015
               Right = 799
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'OrdersView'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane3', @value=N'SPDDriver"
            Begin Extent = 
               Top = 768
               Left = 875
               Bottom = 1015
               Right = 1209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RPUDriver"
            Begin Extent = 
               Top = 768
               Left = 1285
               Bottom = 1015
               Right = 1619
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RDDriver"
            Begin Extent = 
               Top = 768
               Left = 1695
               Bottom = 1015
               Right = 2029
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RSDOPS"
            Begin Extent = 
               Top = 768
               Left = 2105
               Bottom = 1015
               Right = 2418
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'OrdersView'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=3 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'OrdersView'
GO


