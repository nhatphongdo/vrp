/*
   Thursday, December 1, 20166:15:29 PM
   User: vrp
   Server: 52.74.132.250
   Database: VRP
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PlanResult ADD
	IsDeleted bit NOT NULL CONSTRAINT DF_PlanResult_IsDeleted DEFAULT 0
GO
ALTER TABLE dbo.PlanResult SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PlanResult', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PlanResult', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PlanResult', 'Object', 'CONTROL') as Contr_Per 