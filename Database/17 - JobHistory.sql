/*
   Tuesday, August 9, 20166:22:28 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.JobHistory
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	JobId bigint NOT NULL,
	ConsignmentNumber nvarchar(256) NULL,
	IsPickup bit NOT NULL,
	IsSuccessful bit NOT NULL,
	CompletedOn datetime NULL,
	CompletedBy nvarchar(256) NULL,
	PayRateId int NULL,
	Payment decimal(18, 5) NOT NULL,
	Rating int NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.JobHistory ADD CONSTRAINT
	PK_JobHistory PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.JobHistory ADD CONSTRAINT
	FK_JobHistory_Job FOREIGN KEY
	(
	Id
	) REFERENCES dbo.JobHistory
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.JobHistory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.JobHistory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.JobHistory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.JobHistory', 'Object', 'CONTROL') as Contr_Per 