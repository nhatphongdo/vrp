/*
   Friday, August 5, 201611:51:21 AM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.PayScheme
	(
	Id int NOT NULL IDENTITY (1, 1),
	SchemeName nvarchar(2048) NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.PayScheme ADD CONSTRAINT
	PK_PayScheme PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.PayScheme SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PayScheme', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PayScheme', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PayScheme', 'Object', 'CONTROL') as Contr_Per 