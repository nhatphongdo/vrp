/*
   Thursday, August 11, 201610:06:06 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.CodOption
	(
	Id int NOT NULL IDENTITY (1, 1),
	Description nvarchar(MAX) NULL,
	IsCashCollection bit NOT NULL,
	IsChequeCollection bit NOT NULL,
	ChargeAmount decimal(18, 5) NOT NULL,
	IsChargeInPercentage bit NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.CodOption ADD CONSTRAINT
	DF_CodOption_ChargeAmount DEFAULT ((0)) FOR ChargeAmount
GO
ALTER TABLE dbo.CodOption ADD CONSTRAINT
	DF_CodOption_IsChargeInPercentage DEFAULT ((1)) FOR IsChargeInPercentage
GO
ALTER TABLE dbo.CodOption ADD CONSTRAINT
	DF_CodOption_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.CodOption ADD CONSTRAINT
	DF_CodOption_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.CodOption ADD CONSTRAINT
	DF_CodOption_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
ALTER TABLE dbo.CodOption ADD CONSTRAINT
	PK_CodOption PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.CodOption SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'CONTROL') as Contr_Per 