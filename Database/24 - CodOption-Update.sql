/*
   Thursday, August 11, 201610:29:58 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.CodOption
	DROP CONSTRAINT DF_CodOption_ChargeAmount
GO
ALTER TABLE dbo.CodOption
	DROP CONSTRAINT DF_CodOption_IsChargeInPercentage
GO
ALTER TABLE dbo.CodOption
	DROP CONSTRAINT DF_CodOption_IsDeleted
GO
ALTER TABLE dbo.CodOption
	DROP CONSTRAINT DF_CodOption_CreatedOn
GO
ALTER TABLE dbo.CodOption
	DROP CONSTRAINT DF_CodOption_UpdatedOn
GO
CREATE TABLE dbo.Tmp_CodOption
	(
	Id int NOT NULL IDENTITY (1, 1),
	Description nvarchar(MAX) NULL,
	Type int NOT NULL,
	ChargeAmount decimal(18, 5) NOT NULL,
	IsChargeInPercentage bit NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_CodOption SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_CodOption ADD CONSTRAINT
	DF_CodOption_ChargeAmount DEFAULT ((0)) FOR ChargeAmount
GO
ALTER TABLE dbo.Tmp_CodOption ADD CONSTRAINT
	DF_CodOption_IsChargeInPercentage DEFAULT ((1)) FOR IsChargeInPercentage
GO
ALTER TABLE dbo.Tmp_CodOption ADD CONSTRAINT
	DF_CodOption_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_CodOption ADD CONSTRAINT
	DF_CodOption_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_CodOption ADD CONSTRAINT
	DF_CodOption_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_CodOption ON
GO
IF EXISTS(SELECT * FROM dbo.CodOption)
	 EXEC('INSERT INTO dbo.Tmp_CodOption (Id, Description, Type, ChargeAmount, IsChargeInPercentage, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, Description, CONVERT(int, IsCashCollection), ChargeAmount, IsChargeInPercentage, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.CodOption WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_CodOption OFF
GO
ALTER TABLE dbo.CodOptionOfOrder
	DROP CONSTRAINT FK_CodOptionOfOrder_CodOption
GO
DROP TABLE dbo.CodOption
GO
EXECUTE sp_rename N'dbo.Tmp_CodOption', N'CodOption', 'OBJECT' 
GO
ALTER TABLE dbo.CodOption ADD CONSTRAINT
	PK_CodOption PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.CodOptionOfOrder ADD CONSTRAINT
	FK_CodOptionOfOrder_CodOption FOREIGN KEY
	(
	CodOptionId
	) REFERENCES dbo.CodOption
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.CodOptionOfOrder SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CodOptionOfOrder', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CodOptionOfOrder', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CodOptionOfOrder', 'Object', 'CONTROL') as Contr_Per 