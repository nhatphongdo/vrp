USE [master]
GO
/****** Object:  Database [VRP]    Script Date: 10/20/2016 10:09:55 AM ******/
CREATE DATABASE [VRP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VRP', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VRP.mdf' , SIZE = 149504KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VRP_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VRP_log.ldf' , SIZE = 291904KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VRP] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VRP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VRP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VRP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VRP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VRP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VRP] SET ARITHABORT OFF 
GO
ALTER DATABASE [VRP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VRP] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [VRP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VRP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VRP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VRP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VRP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VRP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VRP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VRP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VRP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VRP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VRP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VRP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VRP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VRP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VRP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VRP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VRP] SET RECOVERY FULL 
GO
ALTER DATABASE [VRP] SET  MULTI_USER 
GO
ALTER DATABASE [VRP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VRP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VRP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VRP] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'VRP', N'ON'
GO
USE [VRP]
GO
/****** Object:  User [vrp]    Script Date: 10/20/2016 10:09:55 AM ******/
CREATE USER [vrp] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [roadbull]    Script Date: 10/20/2016 10:09:55 AM ******/
CREATE USER [roadbull] FOR LOGIN [roadbull] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [cds]    Script Date: 10/20/2016 10:09:55 AM ******/
CREATE USER [cds] FOR LOGIN [cds] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [vrp]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [vrp]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [vrp]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [vrp]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [vrp]
GO
ALTER ROLE [db_datareader] ADD MEMBER [vrp]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [vrp]
GO
ALTER ROLE [db_owner] ADD MEMBER [roadbull]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [roadbull]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [roadbull]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [roadbull]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [roadbull]
GO
ALTER ROLE [db_datareader] ADD MEMBER [roadbull]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [roadbull]
GO
ALTER ROLE [db_owner] ADD MEMBER [cds]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [cds]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [cds]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [cds]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [cds]
GO
ALTER ROLE [db_datareader] ADD MEMBER [cds]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [cds]
GO
/****** Object:  StoredProcedure [dbo].[DeleteDistances]    Script Date: 10/20/2016 10:09:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDistances]
	-- Add the parameters for the stored procedure here
	@planId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Distance] WHERE [dbo].[Distance].PlanId = @planId
END

GO
/****** Object:  StoredProcedure [dbo].[InsertDistance]    Script Date: 10/20/2016 10:09:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertDistance]
	-- Add the parameters for the stored procedure here
	@fromId int, 
	@toId int,
	@planId int,
	@distance float = 0,
	@time float = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Distance]
           ([FromId]
           ,[ToId]
           ,[Distance]
           ,[Duration]
           ,[PlanId]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[UpdatedOn]
           ,[UpdatedBy])
     VALUES
           (@fromId
           ,@toId
           ,@distance
           ,@time
           ,@planId
           ,GETDATE()
           ,NULL
           ,GETDATE()
           ,NULL)

END

GO
/****** Object:  Table [dbo].[Distance]    Script Date: 10/20/2016 10:09:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Distance](
	[FromId] [int] NOT NULL,
	[ToId] [int] NOT NULL,
	[Distance] [float] NOT NULL,
	[Duration] [float] NOT NULL,
	[PlanId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Distance] PRIMARY KEY CLUSTERED 
(
	[FromId] ASC,
	[ToId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Location]    Script Date: 10/20/2016 10:09:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](1024) NULL,
	[PostalCode] [nvarchar](32) NULL,
	[StreetName] [nvarchar](1024) NULL,
	[UnitNumber] [nvarchar](32) NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[Starting] [bit] NOT NULL,
	[MustBeVisited] [int] NOT NULL,
	[ServiceTime] [float] NOT NULL,
	[Type] [int] NOT NULL,
	[PickupAmount] [float] NOT NULL,
	[DeliveryAmount] [float] NOT NULL,
	[Profit] [decimal](18, 2) NOT NULL,
	[FromTime] [float] NOT NULL,
	[ToTime] [float] NOT NULL,
	[PlanId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Plan]    Script Date: 10/20/2016 10:09:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberOfCustomers] [int] NOT NULL,
	[ComputationMethod] [int] NOT NULL,
	[RouteType] [int] NOT NULL,
	[AverageVehicleSpeed] [float] NOT NULL,
	[NumberOfVehicleTypes] [int] NOT NULL,
	[Vehicles] [nvarchar](max) NULL,
	[ReturnToDepot] [bit] NOT NULL,
	[TimeWindowType] [int] NOT NULL,
	[IsIncludingWaitingTime] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Plan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlanResult]    Script Date: 10/20/2016 10:09:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlanResult](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PlanId] [int] NOT NULL,
	[Result] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_PlanResult] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Setting]    Script Date: 10/20/2016 10:09:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[Value] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_FromId]    Script Date: 10/20/2016 10:09:55 AM ******/
CREATE NONCLUSTERED INDEX [IX_FromId] ON [dbo].[Distance]
(
	[FromId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ToId]    Script Date: 10/20/2016 10:09:55 AM ******/
CREATE NONCLUSTERED INDEX [IX_ToId] ON [dbo].[Distance]
(
	[ToId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Distance] ADD  CONSTRAINT [DF_Distance_Distance]  DEFAULT ((0)) FOR [Distance]
GO
ALTER TABLE [dbo].[Distance] ADD  CONSTRAINT [DF_Distance_Time]  DEFAULT ((0)) FOR [Duration]
GO
ALTER TABLE [dbo].[Distance] ADD  CONSTRAINT [DF_Distance_PlanId]  DEFAULT ((0)) FOR [PlanId]
GO
ALTER TABLE [dbo].[Distance] ADD  CONSTRAINT [DF_Distance_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Distance] ADD  CONSTRAINT [DF_Distance_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_Starting]  DEFAULT ((0)) FOR [Starting]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_MustBeVisited]  DEFAULT ((0)) FOR [MustBeVisited]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_ServiceTime]  DEFAULT ((0)) FOR [ServiceTime]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_Supply]  DEFAULT ((0)) FOR [PickupAmount]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_DeliveryAmount]  DEFAULT ((0)) FOR [DeliveryAmount]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_Revenue]  DEFAULT ((0)) FOR [Profit]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_FromTime]  DEFAULT ((0)) FOR [FromTime]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_ToTime]  DEFAULT ((0)) FOR [ToTime]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_Customers]  DEFAULT ((0)) FOR [NumberOfCustomers]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_ComputationMethod]  DEFAULT ((0)) FOR [ComputationMethod]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_RouteType]  DEFAULT ((0)) FOR [RouteType]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_AverageVehicleSpeed]  DEFAULT ((0)) FOR [AverageVehicleSpeed]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_NumberOfVehicleTypes]  DEFAULT ((0)) FOR [NumberOfVehicleTypes]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_ReturnToDepot]  DEFAULT ((1)) FOR [ReturnToDepot]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_TimeWindowType]  DEFAULT ((0)) FOR [TimeWindowType]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_DistanceLimit]  DEFAULT ((0)) FOR [IsIncludingWaitingTime]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[PlanResult] ADD  CONSTRAINT [DF_PlanResult_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PlanResult] ADD  CONSTRAINT [DF_PlanResult_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Setting] ADD  CONSTRAINT [DF_Setting_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Setting] ADD  CONSTRAINT [DF_Setting_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Distance]  WITH CHECK ADD  CONSTRAINT [FK_Distance_Location] FOREIGN KEY([FromId])
REFERENCES [dbo].[Location] ([Id])
GO
ALTER TABLE [dbo].[Distance] CHECK CONSTRAINT [FK_Distance_Location]
GO
ALTER TABLE [dbo].[Distance]  WITH CHECK ADD  CONSTRAINT [FK_Distance_Location1] FOREIGN KEY([ToId])
REFERENCES [dbo].[Location] ([Id])
GO
ALTER TABLE [dbo].[Distance] CHECK CONSTRAINT [FK_Distance_Location1]
GO
ALTER TABLE [dbo].[Distance]  WITH CHECK ADD  CONSTRAINT [FK_Distance_Plan] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([Id])
GO
ALTER TABLE [dbo].[Distance] CHECK CONSTRAINT [FK_Distance_Plan]
GO
ALTER TABLE [dbo].[Location]  WITH CHECK ADD  CONSTRAINT [FK_Location_Plan] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([Id])
GO
ALTER TABLE [dbo].[Location] CHECK CONSTRAINT [FK_Location_Plan]
GO
ALTER TABLE [dbo].[PlanResult]  WITH CHECK ADD  CONSTRAINT [FK_PlanResult_Plan] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([Id])
GO
ALTER TABLE [dbo].[PlanResult] CHECK CONSTRAINT [FK_PlanResult_Plan]
GO
USE [master]
GO
ALTER DATABASE [VRP] SET  READ_WRITE 
GO
