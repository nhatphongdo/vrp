/*
   Sunday, January 31, 20169:54:03 PM
   User: sa
   Server: 52.76.161.186
   Database: CDS
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Charge
	DROP CONSTRAINT DF_Charge_ChargeAmount
GO
ALTER TABLE dbo.Charge
	DROP CONSTRAINT DF_Charge_IsChargeInPercentage
GO
ALTER TABLE dbo.Charge
	DROP CONSTRAINT DF_Charge_IsDeleted
GO
ALTER TABLE dbo.Charge
	DROP CONSTRAINT DF_Charge_CreatedOn
GO
ALTER TABLE dbo.Charge
	DROP CONSTRAINT DF_Charge_UpdatedOn
GO
CREATE TABLE dbo.Tmp_Charge
	(
	Id int NOT NULL IDENTITY (1, 1),
	ChargeCode nvarchar(256) NULL,
	Description nvarchar(MAX) NULL,
	ValidFrom datetime NULL,
	ValidTo datetime NULL,
	ChargeAmount decimal(18, 5) NOT NULL,
	IsChargeInPercentage bit NOT NULL,
	ServiceId int NULL,
	SizeId int NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Charge SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Charge ADD CONSTRAINT
	DF_Charge_ChargeAmount DEFAULT ((0)) FOR ChargeAmount
GO
ALTER TABLE dbo.Tmp_Charge ADD CONSTRAINT
	DF_Charge_IsChargeInPercentage DEFAULT ((1)) FOR IsChargeInPercentage
GO
ALTER TABLE dbo.Tmp_Charge ADD CONSTRAINT
	DF_Charge_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_Charge ADD CONSTRAINT
	DF_Charge_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_Charge ADD CONSTRAINT
	DF_Charge_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Charge ON
GO
IF EXISTS(SELECT * FROM dbo.Charge)
	 EXEC('INSERT INTO dbo.Tmp_Charge (Id, ChargeCode, Description, ValidFrom, ValidTo, ChargeAmount, IsChargeInPercentage, ServiceId, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, ChargeCode, Description, ValidFrom, ValidTo, ChargeAmount, IsChargeInPercentage, ServiceId, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.Charge WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Charge OFF
GO
DROP TABLE dbo.Charge
GO
EXECUTE sp_rename N'dbo.Tmp_Charge', N'Charge', 'OBJECT' 
GO
ALTER TABLE dbo.Charge ADD CONSTRAINT
	PK_Charge PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX IX_Charge_ChargeCode ON dbo.Charge
	(
	ChargeCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Charge', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Charge', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Charge', 'Object', 'CONTROL') as Contr_Per 