/*
   Sunday, January 24, 20169:20:08 AM
   User: sa
   Server: 52.76.161.186
   Database: VRP
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT FK_Location_Plan
GO
ALTER TABLE dbo.[Plan] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[Plan]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[Plan]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[Plan]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_Starting
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_MustBeVisited
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_ServiceTime
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_Type
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_Supply
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_DeliveryAmount
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_Revenue
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_FromTime
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_ToTime
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_CreatedOn
GO
ALTER TABLE dbo.Location
	DROP CONSTRAINT DF_Location_UpdatedOn
GO
CREATE TABLE dbo.Tmp_Location
	(
	Id int NOT NULL IDENTITY (1, 1),
	OrderId nvarchar(MAX) NULL,
	CustomerName nvarchar(1024) NULL,
	PostalCode nvarchar(32) NULL,
	StreetName nvarchar(1024) NULL,
	UnitNumber nvarchar(32) NULL,
	Longitude float(53) NULL,
	Latitude float(53) NULL,
	Starting bit NOT NULL,
	MustBeVisited int NOT NULL,
	ServiceTime float(53) NOT NULL,
	Type int NOT NULL,
	PickupAmount float(53) NOT NULL,
	DeliveryAmount float(53) NOT NULL,
	Profit decimal(18, 2) NOT NULL,
	FromTime float(53) NOT NULL,
	ToTime float(53) NOT NULL,
	PlanId int NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Location SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_Starting DEFAULT ((0)) FOR Starting
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_MustBeVisited DEFAULT ((0)) FOR MustBeVisited
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_ServiceTime DEFAULT ((0)) FOR ServiceTime
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_Type DEFAULT ((0)) FOR Type
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_Supply DEFAULT ((0)) FOR PickupAmount
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_DeliveryAmount DEFAULT ((0)) FOR DeliveryAmount
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_Revenue DEFAULT ((0)) FOR Profit
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_FromTime DEFAULT ((0)) FOR FromTime
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_ToTime DEFAULT ((0)) FOR ToTime
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_Location ADD CONSTRAINT
	DF_Location_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Location ON
GO
IF EXISTS(SELECT * FROM dbo.Location)
	 EXEC('INSERT INTO dbo.Tmp_Location (Id, OrderId, CustomerName, PostalCode, StreetName, UnitNumber, Longitude, Latitude, Starting, MustBeVisited, ServiceTime, Type, PickupAmount, DeliveryAmount, Profit, FromTime, ToTime, PlanId, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, CONVERT(nvarchar(MAX), OrderId), CustomerName, PostalCode, StreetName, UnitNumber, Longitude, Latitude, Starting, MustBeVisited, ServiceTime, Type, PickupAmount, DeliveryAmount, Profit, FromTime, ToTime, PlanId, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.Location WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Location OFF
GO
ALTER TABLE dbo.Distance
	DROP CONSTRAINT FK_Distance_Location
GO
ALTER TABLE dbo.Distance
	DROP CONSTRAINT FK_Distance_Location1
GO
DROP TABLE dbo.Location
GO
EXECUTE sp_rename N'dbo.Tmp_Location', N'Location', 'OBJECT' 
GO
ALTER TABLE dbo.Location ADD CONSTRAINT
	PK_Location PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Location ADD CONSTRAINT
	FK_Location_Plan FOREIGN KEY
	(
	PlanId
	) REFERENCES dbo.[Plan]
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Location', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Location', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Location', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Distance ADD CONSTRAINT
	FK_Distance_Location FOREIGN KEY
	(
	FromId
	) REFERENCES dbo.Location
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Distance ADD CONSTRAINT
	FK_Distance_Location1 FOREIGN KEY
	(
	ToId
	) REFERENCES dbo.Location
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Distance SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Distance', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Distance', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Distance', 'Object', 'CONTROL') as Contr_Per 