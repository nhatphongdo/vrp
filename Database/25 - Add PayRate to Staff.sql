/*
   Saturday, August 13, 20168:30:05 AM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Staff
	DROP CONSTRAINT DF_Staff_Rating
GO
ALTER TABLE dbo.Staff
	DROP CONSTRAINT DF_Staff_PhoneType
GO
ALTER TABLE dbo.Staff
	DROP CONSTRAINT DF_Staff_IsActive
GO
ALTER TABLE dbo.Staff
	DROP CONSTRAINT DF_Staff_IsDeleted
GO
ALTER TABLE dbo.Staff
	DROP CONSTRAINT DF_Staff_CreatedOn
GO
ALTER TABLE dbo.Staff
	DROP CONSTRAINT DF_Staff_UpdatedOn
GO
CREATE TABLE dbo.Tmp_Staff
	(
	Id int NOT NULL IDENTITY (1, 1),
	UserName nvarchar(256) NULL,
	StaffName nvarchar(1024) NULL,
	StaffAddress nvarchar(2048) NULL,
	StaffCountry nvarchar(32) NULL,
	StaffMobile nvarchar(128) NULL,
	StaffPhoto nvarchar(1024) NULL,
	StaffIdentityNumber nvarchar(128) NULL,
	ICNumber nvarchar(128) NULL,
	DrivingLicense nvarchar(256) NULL,
	PayRateId int NULL,
	Rating int NOT NULL,
	Remark nvarchar(MAX) NULL,
	NotificationKey nvarchar(1024) NULL,
	PhoneType int NOT NULL,
	IsActive bit NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Staff SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Staff ADD CONSTRAINT
	DF_Staff_Rating DEFAULT ((0)) FOR Rating
GO
ALTER TABLE dbo.Tmp_Staff ADD CONSTRAINT
	DF_Staff_PhoneType DEFAULT ((0)) FOR PhoneType
GO
ALTER TABLE dbo.Tmp_Staff ADD CONSTRAINT
	DF_Staff_IsActive DEFAULT ((1)) FOR IsActive
GO
ALTER TABLE dbo.Tmp_Staff ADD CONSTRAINT
	DF_Staff_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_Staff ADD CONSTRAINT
	DF_Staff_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_Staff ADD CONSTRAINT
	DF_Staff_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Staff ON
GO
IF EXISTS(SELECT * FROM dbo.Staff)
	 EXEC('INSERT INTO dbo.Tmp_Staff (Id, UserName, StaffName, StaffAddress, StaffCountry, StaffMobile, StaffPhoto, StaffIdentityNumber, ICNumber, DrivingLicense, Rating, Remark, NotificationKey, PhoneType, IsActive, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, UserName, StaffName, StaffAddress, StaffCountry, StaffMobile, StaffPhoto, StaffIdentityNumber, ICNumber, DrivingLicense, Rating, Remark, NotificationKey, PhoneType, IsActive, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.Staff WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Staff OFF
GO
DROP TABLE dbo.Staff
GO
EXECUTE sp_rename N'dbo.Tmp_Staff', N'Staff', 'OBJECT' 
GO
ALTER TABLE dbo.Staff ADD CONSTRAINT
	PK_Staff PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Staff', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Staff', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Staff', 'Object', 'CONTROL') as Contr_Per 