/*
   Wednesday, August 17, 20163:44:44 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TopupPayment
	DROP CONSTRAINT DF_TopupPayment_PaymentAmount
GO
ALTER TABLE dbo.TopupPayment
	DROP CONSTRAINT DF_TopupPayment_IsSuccessful
GO
ALTER TABLE dbo.TopupPayment
	DROP CONSTRAINT DF_TopupPayment_CreatedOn
GO
CREATE TABLE dbo.Tmp_TopupPayment
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	PaymentAccountId bigint NULL,
	Payer nvarchar(256) NULL,
	PaymentAmount decimal(18, 5) NOT NULL,
	TransferAmount decimal(18, 5) NOT NULL,
	Status int NOT NULL,
	IsSuccessful bit NOT NULL,
	CreatedOn datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_TopupPayment SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_TopupPayment ADD CONSTRAINT
	DF_TopupPayment_PaymentAmount DEFAULT ((0)) FOR PaymentAmount
GO
ALTER TABLE dbo.Tmp_TopupPayment ADD CONSTRAINT
	DF_TopupPayment_TransferAmount DEFAULT 0 FOR TransferAmount
GO
ALTER TABLE dbo.Tmp_TopupPayment ADD CONSTRAINT
	DF_TopupPayment_Status DEFAULT 0 FOR Status
GO
ALTER TABLE dbo.Tmp_TopupPayment ADD CONSTRAINT
	DF_TopupPayment_IsSuccessful DEFAULT ((0)) FOR IsSuccessful
GO
ALTER TABLE dbo.Tmp_TopupPayment ADD CONSTRAINT
	DF_TopupPayment_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_TopupPayment ON
GO
IF EXISTS(SELECT * FROM dbo.TopupPayment)
	 EXEC('INSERT INTO dbo.Tmp_TopupPayment (Id, PaymentAccountId, Payer, PaymentAmount, IsSuccessful, CreatedOn)
		SELECT Id, PaymentAccountId, Payer, PaymentAmount, IsSuccessful, CreatedOn FROM dbo.TopupPayment WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_TopupPayment OFF
GO
DROP TABLE dbo.TopupPayment
GO
EXECUTE sp_rename N'dbo.Tmp_TopupPayment', N'TopupPayment', 'OBJECT' 
GO
ALTER TABLE dbo.TopupPayment ADD CONSTRAINT
	PK_TopupPayment PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.TopupPayment', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TopupPayment', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TopupPayment', 'Object', 'CONTROL') as Contr_Per 