/*
   Thursday, August 11, 201610:15:25 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.CodOption SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CodOption', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[Order]', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_CodOptionOfOrder
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	OrderId bigint NOT NULL,
	CodOptionId int NOT NULL,
	Amount decimal(18, 5) NOT NULL,
	IsCollected bit NOT NULL,
	CollectedAmount decimal(18, 5) NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_CodOptionOfOrder SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_CodOptionOfOrder OFF
GO
IF EXISTS(SELECT * FROM dbo.CodOptionOfOrder)
	 EXEC('INSERT INTO dbo.Tmp_CodOptionOfOrder (OrderId, CodOptionId, Amount, IsCollected, CollectedAmount, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT OrderId, CodOptionId, Amount, IsCollected, CollectedAmount, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.CodOptionOfOrder WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.CodOptionOfOrder
GO
EXECUTE sp_rename N'dbo.Tmp_CodOptionOfOrder', N'CodOptionOfOrder', 'OBJECT' 
GO
ALTER TABLE dbo.CodOptionOfOrder ADD CONSTRAINT
	PK_CodOptionOfOrder_1 PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.CodOptionOfOrder ADD CONSTRAINT
	FK_CodOptionOfOrder_Order FOREIGN KEY
	(
	OrderId
	) REFERENCES dbo.[Order]
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.CodOptionOfOrder ADD CONSTRAINT
	FK_CodOptionOfOrder_CodOption FOREIGN KEY
	(
	CodOptionId
	) REFERENCES dbo.CodOption
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CodOptionOfOrder', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CodOptionOfOrder', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CodOptionOfOrder', 'Object', 'CONTROL') as Contr_Per 