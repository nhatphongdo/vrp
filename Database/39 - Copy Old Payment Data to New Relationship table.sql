USE [Courier]
GO

;with tmp(PaymentId, ConsignmentNumber, Orders) as (
select Id, LEFT(Orders, CHARINDEX(',',Orders+',')-1),
    STUFF(Orders, 1, CHARINDEX(',',Orders+','), '')
from Payment
union all
select PaymentId, LEFT(Orders, CHARINDEX(',',Orders+',')-1),
    STUFF(Orders, 1, CHARINDEX(',',Orders+','), '')
from tmp
where Orders > ''
)
insert into [dbo].[OrdersOfPayment]
           ([PaymentId]
           ,[ConsignmentNumber])
    select PaymentId, ConsignmentNumber
    from tmp
    order by PaymentId
    OPTION (maxrecursion 0)
GO
