/*
   Sunday, January 24, 20161:56:57 PM
   User: sa
   Server: 52.76.161.186
   Database: CDS
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT FK_Job_Vehicle
GO
ALTER TABLE dbo.Vehicle SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Vehicle', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Vehicle', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Vehicle', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_VehicleId
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_DriverId
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_DriverType
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_TotalPickups
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_TotalDeliveries
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Table_1_TotalPickupAmount
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_TotalDrivingTime
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_TotalWorkingTime
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_IsSpecial
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_IsDeleted
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_CreatedOn
GO
ALTER TABLE dbo.Job
	DROP CONSTRAINT DF_Job_UpdatedOn
GO
CREATE TABLE dbo.Tmp_Job
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	VehicleId int NULL,
	DriverId int NOT NULL,
	DriverType int NOT NULL,
	Routes nvarchar(MAX) NULL,
	TakenOn datetime NULL,
	TakenBy nvarchar(256) NULL,
	CompletedOn datetime NULL,
	TotalPickups int NOT NULL,
	TotalDeliveries int NOT NULL,
	TotalDistances float(53) NOT NULL,
	TotalDrivingTime float(53) NOT NULL,
	TotalWorkingTime float(53) NOT NULL,
	IsSpecial bit NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Job SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_VehicleId DEFAULT ((0)) FOR VehicleId
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_DriverId DEFAULT ((0)) FOR DriverId
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_DriverType DEFAULT ((0)) FOR DriverType
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_TotalPickups DEFAULT ((0)) FOR TotalPickups
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_TotalDeliveries DEFAULT ((0)) FOR TotalDeliveries
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Table_1_TotalPickupAmount DEFAULT ((0)) FOR TotalDistances
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_TotalDrivingTime DEFAULT ((0)) FOR TotalDrivingTime
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_TotalWorkingTime DEFAULT ((0)) FOR TotalWorkingTime
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_IsSpecial DEFAULT ((0)) FOR IsSpecial
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_Job ADD CONSTRAINT
	DF_Job_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Job ON
GO
IF EXISTS(SELECT * FROM dbo.Job)
	 EXEC('INSERT INTO dbo.Tmp_Job (Id, VehicleId, DriverId, DriverType, Routes, TakenOn, TakenBy, CompletedOn, TotalPickups, TotalDeliveries, TotalDistances, TotalDrivingTime, TotalWorkingTime, IsSpecial, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, VehicleId, DriverId, DriverType, Routes, TakenOn, TakenBy, CompletedOn, TotalPickups, TotalDeliveries, TotalDistances, TotalDrivingTime, TotalWorkingTime, IsSpecial, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.Job WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Job OFF
GO
ALTER TABLE dbo.TrackLocation
	DROP CONSTRAINT FK_TrackLocation_Job
GO
DROP TABLE dbo.Job
GO
EXECUTE sp_rename N'dbo.Tmp_Job', N'Job', 'OBJECT' 
GO
ALTER TABLE dbo.Job ADD CONSTRAINT
	PK_Job PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Job ADD CONSTRAINT
	FK_Job_Vehicle FOREIGN KEY
	(
	VehicleId
	) REFERENCES dbo.Vehicle
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Job', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Job', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Job', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.TrackLocation ADD CONSTRAINT
	FK_TrackLocation_Job FOREIGN KEY
	(
	JobId
	) REFERENCES dbo.Job
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TrackLocation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.TrackLocation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TrackLocation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TrackLocation', 'Object', 'CONTROL') as Contr_Per 