IF NOT EXISTS ( SELECT  *
                FROM    SYSCOLUMNS sc
                WHERE   EXISTS ( SELECT id
                                 FROM   [dbo].[sysobjects]
                                 WHERE  NAME LIKE 'SmsMessage'
                                        AND sc.id = id ) ) 
BEGIN
	BEGIN TRANSACTION
	SET QUOTED_IDENTIFIER ON
	SET ARITHABORT ON
	SET NUMERIC_ROUNDABORT OFF
	SET CONCAT_NULL_YIELDS_NULL ON
	SET ANSI_NULLS ON
	SET ANSI_PADDING ON
	SET ANSI_WARNINGS ON
	COMMIT
	BEGIN TRANSACTION
	CREATE TABLE [dbo].[SmsMessage](
		[Id] [bigint] IDENTITY(1,1) NOT NULL,
		[PhoneNumber] [nvarchar](50) NULL,
		[Content] [nvarchar](max) NULL,
		[CreatedOn] [datetime] NULL,
		[Processed] [datetime] NULL,
		[FailedCount] [int] NULL,
	 CONSTRAINT [PK_SmsMessage] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	ALTER TABLE dbo.SmsMessage SET (LOCK_ESCALATION = TABLE)
	COMMIT
END
select Has_Perms_By_Name(N'dbo.SmsMessage', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SmsMessage', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SmsMessage', 'Object', 'CONTROL') as Contr_Per 
