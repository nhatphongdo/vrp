/*
   Tuesday, November 22, 20169:30:39 AM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Payment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Payment', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Payment', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Payment', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrdersOfPayment ADD CONSTRAINT
	FK_OrdersOfPayment_Payment FOREIGN KEY
	(
	PaymentId
	) REFERENCES dbo.Payment
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrdersOfPayment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.OrdersOfPayment', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.OrdersOfPayment', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.OrdersOfPayment', 'Object', 'CONTROL') as Contr_Per 