/*
   Tuesday, February 2, 20166:38:59 PM
   User: sa
   Server: 52.76.161.186
   Database: CDS
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.OrderTracking
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	ConsignmentNumber nvarchar(256) NULL,
	BinCode nvarchar(64) NULL,
	Status int NOT NULL,
	PickupTimeSlotId int NULL,
	PickupDate datetime NULL,
	DeliveryTimeSlotId int NULL,
	DeliveryDate datetime NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.OrderTracking ADD CONSTRAINT
	DF_OrderTracking_Status DEFAULT 0 FOR Status
GO
ALTER TABLE dbo.OrderTracking ADD CONSTRAINT
	DF_OrderTracking_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.OrderTracking ADD CONSTRAINT
	PK_OrderTracking PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.OrderTracking SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.OrderTracking', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.OrderTracking', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.OrderTracking', 'Object', 'CONTROL') as Contr_Per 