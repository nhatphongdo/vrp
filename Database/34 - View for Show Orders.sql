/****** Object:  View [dbo].[OrdersView]    Script Date: 4/11/2016 1:44:19 PM ******/
DROP VIEW [dbo].[OrdersView]
GO

/****** Object:  View [dbo].[OrdersView]    Script Date: 4/11/2016 1:44:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[OrdersView]
AS
SELECT distinct O.*,
FromGroup.DistrictCode as FromDistrictCode,FromGroup.Zone as FromZoneCode,ToGroup.DistrictCode as ToDistrictCode,ToGroup.Zone as ToZoneCode,
C.CustomerName, C.CustomerCompany,
RPR.RequestDate as RPRRequestDate,RPR.RequestContent as RPRRequestContent,RDR.RequestDate as RDRRequestDate,RDR.RequestContent as RDRRequestContent,
(select SUM(Amount) from CodOptionOfOrder COO where COO.OrderId=O.Id and COO.IsDeleted = 0) as TotalCodAmount,
(select SUM(CollectedAmount) from CodOptionOfOrder COO where COO.OrderId=O.Id and COO.IsDeleted = 0 and COO.IsCollected = 1) as TotalCodCollectedAmount,
(select CAST(CODT.[Type] AS NVARCHAR(1024)) + ',' from CodOptionOfOrder COO 
 left join CodOption as CODT on CODT.Id = COO.CodOptionId AND CODT.IsDeleted = 0 
 where COO.OrderId=O.Id and COO.IsDeleted = 0
 FOR XML PATH('')) as CodTypes,
(select CAST(CODT.[Type] AS NVARCHAR(1024)) + ',' from CodOptionOfOrder COO 
 left join CodOption as CODT on CODT.Id = COO.CodOptionId AND CODT.IsDeleted = 0 
 where COO.OrderId=O.Id and COO.[IsCollected] = 1 and COO.IsDeleted = 0
 FOR XML PATH('')) as CodCollectedTypes,
case 
	--Item has been picked/Enroute to pickup
	when (O.Status = 12  and IHBPDriver.Id is not null) then IHBPDriver.StaffName
	when (O.Status = 12  and IHBPOPS.Id is not null) then IHBPOPS.ContractorName
	when (O.Status = 12  and IHBPDriver.Id is null and IHBPOPS.Id is null) then JH.CompletedBy
	--Item in warehouse/Item in depot
	when (O.Status = 5  and IIDOPS.Id is not null) then IIDOPS.ContractorName
	when (O.Status = 5  and IIDOPS.Id is null) then O.ProceededBy
	--Pickup Failed
	when (O.Status = 4  and PFDriver.Id is not null) then PFDriver.StaffName
	when (O.Status = 4  and PFOPS.Id is not null) then PFOPS.ContractorName
	when (O.Status = 4  and PFDriver.Id is null and PFOPS.Id is null) then O.CollectedBy
	--Item is delivered 
	when (O.Status = 7  and IDDriver.Id is not null) then IDDriver.StaffName
	when (O.Status = 7  and PFOPS.Id is  null) then O.DeliveredBy
	--Item Collected
	when (O.Status = 3  and ICDriver.Id is not null) then ICDriver.StaffName
	when (O.Status = 3  and ICDriver.Id is null) then O.CollectedBy
	--Item out depot/Delivery in progress
	when (O.Status = 6  and IODDriver.Id is not null) then IODDriver.StaffName
	when (O.Status = 6  and IODOPS.Id is not null) then IODOPS.ContractorName
	when (O.Status = 6  and IODDriver.Id is null) then O.ProceededOutBy
	when (O.Status = 6  and IHBPDriver.Id is null and IHBPOPS.Id is null) then JH.CompletedBy
	--Delivery Failed
	when (O.Status = 9  and DFDriver.Id is not null) then DFDriver.StaffName 
	when (O.Status = 9  and DFOPS.Id is not null) then DFOPS.ContractorName
	when (O.Status = 9  and DFDriver.Id is null and DFOPS.Id is null) then O.DeliveredBy
	--Spl order for pickup
	when (O.Status = 10  and SPPUDriver.Id is not null) then SPPUDriver.StaffName 
	when (O.Status = 10  and SPPUDriver.Id is null) then OT.CreatedBy
	--Spl order for delivery
	when (O.Status = 11  and SPDDriver.Id is not null) then SPDDriver.StaffName 
	when (O.Status = 11  and SPDDriver.Id is null) then OT.CreatedBy
	--Re pickup
	when (O.Status = 13  and RPUDriver.Id is not null) then RPUDriver.StaffName 
	when (O.Status = 13  and RPUDriver.Id is null) then OT.CreatedBy
	--Re delivery
	when (O.Status = 14  and RDDriver.Id is not null) then RDDriver.StaffName 
	when (O.Status = 14  and RDDriver.Id is null) then OT.CreatedBy
	--Return to sender
	when (O.Status = 15  and RSDOPS.Id is not null) then RSDOPS.ContractorName 
	when (O.Status = 15  and RSDOPS.Id is null) then O.UpdatedBy

else ''
  End as DriverOrOpsName,

  case 
    --Item has been picked/Enroute to pickup
	when (O.Status = 12  and IHBPDriver.Id is not null) then IHBPDriver.StaffMobile
	when (O.Status = 12  and IHBPOPS.Id is not null) then IHBPOPS.ContractorMobile
	--Item in warehouse/Item in depot
	when (O.Status = 5  and IIDOPS.Id is not null) then IIDOPS.ContractorMobile
	--Pickup Failed
	when (O.Status = 4  and PFDriver.Id is not null) then PFDriver.StaffMobile
	when (O.Status = 4  and PFOPS.Id is not null) then PFOPS.ContractorMobile
	--Item is delivered 
	when (O.Status = 7  and IDDriver.Id is not null) then IDDriver.StaffMobile
	--Item Collected
	when (O.Status = 3  and ICDriver.Id is not null) then ICDriver.StaffMobile
	--Item out depot/Delivery in progress
	when (O.Status = 6  and IODDriver.Id is not null) then IODDriver.StaffMobile
	--Delivery Failed
	when (O.Status = 9  and DFDriver.Id is not null) then DFDriver.StaffMobile 
	when (O.Status = 9  and DFOPS.Id is not null) then DFOPS.ContractorMobile
	--Spl order for pickup
	when (O.Status = 10  and SPPUDriver.Id is not null) then SPPUDriver.StaffMobile
	--Spl order for delivery
	when (O.Status = 11  and SPDDriver.Id is not null) then SPPUDriver.StaffMobile
	--Re pickup
	when (O.Status = 13  and RPUDriver.Id is not null) then SPDDriver.StaffMobile
	--Re delivery
	when (O.Status = 14  and RDDriver.Id is not null) then RDDriver.StaffMobile
	--Return to sender
	when (O.Status = 15  and RSDOPS.Id is not null) then RSDOPS.ContractorMobile

else ''
  End as DriverOrOpsMobile,

  case 
    --Item has been picked/Enroute to pickup
	when (O.Status = 12) then J.TakenOn
	--Item in warehouse/Item in depot
	when (O.Status = 5) then O.ProceededOn
	--Pickup Failed
	when (O.Status = 4) then O.CollectedOn
	--Item is delivered 
	when (O.Status = 7) then O.DeliveredOn
	--Item Collected 
	when (O.Status = 3) then O.CollectedOn
	--Item out depot/Delivery in progress
	when (O.Status = 6) then O.ProceededOutOn
	--Delivery Failed
	when (O.Status = 9) then O.DeliveredOn
	--Spl order for pickup
	when (O.Status = 10) then OT.CreatedOn 
	--Spl order for delivery
	when (O.Status = 11) then OT.CreatedOn 
	--Re pickup
	when (O.Status = 13) then OT.CreatedOn 
	--Re delivery
	when (O.Status = 14) then OT.CreatedOn 
	--Return to sender
	when (O.Status = 15 ) then O.UpdatedOn


else ''
  End as DriverOrOpsTimeStamp

FROM [order] O
LEFT JOIN ZipCode AS FromGroup on O.FromZipCode = FromGroup.PostalCode
LEFT JOIN ZipCode as ToGroup on O.ToZipCode = ToGroup.PostalCode
LEFT JOIN ExtraRequest as RPR on RPR.OrderId = O.Id and RPR.Id = (SELECT MAX(Id) from ExtraRequest RPRL WHERE RPRL.OrderId = O.Id and RPRL.RequestType = 3 and RPRL.IsDeleted = 0)
LEFT JOIN ExtraRequest as RDR on RDR.OrderId = O.Id and RDR.Id = (SELECT MAX(Id) from ExtraRequest RDRL WHERE RDRL.OrderId = O.Id and RDRL.RequestType = 2 and RDRL.IsDeleted = 0)
LEFT JOIN Customer C on C.Id = O.CustomerId
--LEFT JOIN CodOptionOfOrder as CODO on CODO.OrderId = O.Id and CODO.IsDeleted = 0
--LEFT JOIN CodOption as CODT on CODT.Id = CODO.CodOptionId and  CODT.IsDeleted = 0

LEFT JOIN JobHistory JH on JH.ConsignmentNumber = O.ConsignmentNumber and JH.Id = (SELECT MAX(ID) from JobHistory JHL WHERE JHL.ConsignmentNumber = O.ConsignmentNumber)
LEFT JOIN Job J on J.Id = JH.JobId
LEFT JOIN OrderTracking OT on OT.ConsignmentNumber = O.ConsignmentNumber and OT.Id = (SELECT MAX(ID) from OrderTracking OTL WHERE OTL.ConsignmentNumber = O.ConsignmentNumber and OTL.Status = O.Status)

LEFT JOIN Staff as IHBPDriver on IHBPDriver.UserName = JH.CompletedBy and J.DriverType = 0
LEFT JOIN Contractor as IHBPOPS on IHBPOPS.UserName = JH.CompletedBy and J.DriverType = 1

LEFT JOIN Contractor as IIDOPS on IIDOPS.UserName = O.ProceededBy


LEFT JOIN Staff as PFDriver on PFDriver.UserName = O.CollectedBy
LEFT JOIN Contractor as PFOPS on PFOPS.UserName = O.CollectedBy


LEFT JOIN Staff as IDDriver on IDDriver.UserName = O.DeliveredBy
LEFT JOIN Staff as ICDriver on ICDriver.UserName = O.CollectedBy

--LEFT JOIN Staff as IODDriver on IODDriver.UserName = O.ProceededOutBy
LEFT JOIN Staff as IODDriver on IODDriver.UserName = JH.CompletedBy and J.DriverType = 0
LEFT JOIN Contractor as IODOPS on IODOPS.UserName = JH.CompletedBy and J.DriverType = 1

LEFT JOIN Staff as DFDriver on DFDriver.UserName = O.DeliveredBy
LEFT JOIN Contractor as DFOPS on DFOPS.UserName = O.DeliveredBy
LEFT JOIN Staff as SPPUDriver on SPPUDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as SPDDriver on SPDDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as RPUDriver on RPUDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as RDDriver on RDDriver.UserName = OT.CreatedBy
LEFT JOIN Contractor as RSDOPS on RSDOPS.UserName = O.UpdatedBy
WHERE O.IsDeleted = 0 


GO
