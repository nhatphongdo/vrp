/*
   Wednesday, August 17, 20163:43:04 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PaymentAccount
	DROP CONSTRAINT DF_PaymentAccount_IsDeleted
GO
ALTER TABLE dbo.PaymentAccount
	DROP CONSTRAINT DF_PaymentAccount_CreatedOn
GO
ALTER TABLE dbo.PaymentAccount
	DROP CONSTRAINT DF_PaymentAccount_UpdatedOn
GO
CREATE TABLE dbo.Tmp_PaymentAccount
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	Owner nvarchar(256) NULL,
	AccountType int NOT NULL,
	FirstName nvarchar(1024) NULL,
	LastName nvarchar(1024) NULL,
	MobileNumber nvarchar(128) NULL,
	CompanyName nvarchar(1024) NULL,
	TransactionNumber nvarchar(1024) NULL,
	TransactionOn datetime NULL,
	CardNumber nvarchar(256) NULL,
	ExpireMonth int NULL,
	ExpireYear int NULL,
	PaypalCardId nvarchar(1024) NULL,
	Currency nvarchar(16) NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_PaymentAccount SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_PaymentAccount ADD CONSTRAINT
	DF_PaymentAccount_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_PaymentAccount ADD CONSTRAINT
	DF_PaymentAccount_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_PaymentAccount ADD CONSTRAINT
	DF_PaymentAccount_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_PaymentAccount ON
GO
IF EXISTS(SELECT * FROM dbo.PaymentAccount)
	 EXEC('INSERT INTO dbo.Tmp_PaymentAccount (Id, Owner, AccountType, FirstName, LastName, CardNumber, ExpireMonth, ExpireYear, PaypalCardId, Currency, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, Owner, AccountType, FirstName, LastName, CardNumber, ExpireMonth, ExpireYear, PaypalCardId, Currency, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.PaymentAccount WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_PaymentAccount OFF
GO
ALTER TABLE dbo.Payment
	DROP CONSTRAINT FK_Payment_PaymentAccount
GO
DROP TABLE dbo.PaymentAccount
GO
EXECUTE sp_rename N'dbo.Tmp_PaymentAccount', N'PaymentAccount', 'OBJECT' 
GO
ALTER TABLE dbo.PaymentAccount ADD CONSTRAINT
	PK_PaymentAccount PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.PaymentAccount', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PaymentAccount', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PaymentAccount', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Payment ADD CONSTRAINT
	FK_Payment_PaymentAccount FOREIGN KEY
	(
	PaymentAccountId
	) REFERENCES dbo.PaymentAccount
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Payment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Payment', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Payment', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Payment', 'Object', 'CONTROL') as Contr_Per 