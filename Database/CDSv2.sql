USE [master]
GO
/****** Object:  Database [Courier]    Script Date: 8/25/2016 11:34:03 AM ******/
CREATE DATABASE [Courier]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Courier', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Courier.mdf' , SIZE = 7168KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Courier_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Courier_log.ldf' , SIZE = 2560KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Courier] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Courier].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Courier] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Courier] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Courier] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Courier] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Courier] SET ARITHABORT OFF 
GO
ALTER DATABASE [Courier] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Courier] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Courier] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Courier] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Courier] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Courier] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Courier] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Courier] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Courier] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Courier] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Courier] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Courier] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Courier] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Courier] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Courier] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Courier] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Courier] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Courier] SET RECOVERY FULL 
GO
ALTER DATABASE [Courier] SET  MULTI_USER 
GO
ALTER DATABASE [Courier] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Courier] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Courier] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Courier] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Courier] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Courier', N'ON'
GO
USE [Courier]
GO
/****** Object:  Schema [HangFire]    Script Date: 8/25/2016 11:34:03 AM ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 8/25/2016 11:34:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Charge]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Charge](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChargeCode] [nvarchar](256) NULL,
	[Description] [nvarchar](max) NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
	[ChargeAmount] [decimal](18, 5) NOT NULL CONSTRAINT [DF_Charge_ChargeAmount]  DEFAULT ((0)),
	[IsChargeInPercentage] [bit] NOT NULL CONSTRAINT [DF_Charge_IsChargeInPercentage]  DEFAULT ((1)),
	[ServiceId] [int] NULL,
	[SizeId] [int] NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Charge_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Charge_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Charge_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Charge] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CodOption]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CodOption](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Type] [int] NOT NULL,
	[ChargeAmount] [decimal](18, 5) NOT NULL CONSTRAINT [DF_CodOption_ChargeAmount]  DEFAULT ((0)),
	[IsChargeInPercentage] [bit] NOT NULL CONSTRAINT [DF_CodOption_IsChargeInPercentage]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_CodOption_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_CodOption_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_CodOption_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_CodOption] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CodOptionOfOrder]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CodOptionOfOrder](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[CodOptionId] [int] NOT NULL,
	[Amount] [decimal](18, 5) NOT NULL,
	[IsCollected] [bit] NOT NULL,
	[CollectedAmount] [decimal](18, 5) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_CodOptionOfOrder_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConsignmentNumber]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConsignmentNumber](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_ConsignmentNumber_Id]  DEFAULT (newid()),
	[ConsignmentNumbers] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ConsignmentNumber_CreatedOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_ConsignmentNumber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contractor]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contractor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[ContractorName] [nvarchar](1024) NULL,
	[ContractorAddress] [nvarchar](2048) NULL,
	[ContractorCountry] [nvarchar](32) NULL,
	[ContractorMobile] [nvarchar](128) NULL,
	[ContractorPhoto] [nvarchar](1024) NULL,
	[ICNumber] [nvarchar](128) NULL,
	[VehicleType] [nvarchar](256) NULL,
	[DrivingLicense] [nvarchar](256) NULL,
	[VehicleNumber] [nvarchar](128) NULL,
	[PayRateId] [int] NULL,
	[Rating] [int] NOT NULL CONSTRAINT [DF_Contractor_Rating]  DEFAULT ((0)),
	[Remark] [nvarchar](max) NULL,
	[NotificationKey] [nvarchar](1024) NULL,
	[PhoneType] [int] NOT NULL CONSTRAINT [DF_Contractor_PhoneType]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Contractor_IsActive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Contractor_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Contractor_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Contractor_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Contractor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [nvarchar](32) NULL,
	[CountryName] [nvarchar](1024) NULL,
	[CountryImage] [nvarchar](1024) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Country_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Country_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[CustomerName] [nvarchar](1024) NULL,
	[CustomerPhoto] [nvarchar](1024) NULL,
	[CustomerCompany] [nvarchar](1024) NULL,
	[CustomerAddress] [nvarchar](2048) NULL,
	[CustomerCountry] [nvarchar](32) NULL,
	[CustomerPhone] [nvarchar](128) NULL,
	[CustomerZipCode] [nvarchar](64) NULL,
	[PaymentTypeId] [int] NOT NULL,
	[Credit] [decimal](18, 5) NOT NULL CONSTRAINT [DF_Customer_Credit]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Customer_IsActive]  DEFAULT ((1)),
	[Salesperson] [nvarchar](256) NULL,
	[PricePlan] [int] NULL,
	[DefaultPromoCode] [nvarchar](max) NULL,
	[NotificationKey] [nvarchar](1024) NULL,
	[PhoneType] [int] NOT NULL CONSTRAINT [DF_Customer_PhoneType]  DEFAULT ((0)),
	[Remark] [nvarchar](max) NULL,
	[MerchantUserName] [nvarchar](256) NULL,
	[CompanyPrefix] [nvarchar](16) NULL,
	[UseGroupPrice] [bit] NOT NULL CONSTRAINT [DF_Customer_UseGroupPrice]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Customer_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Customer_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Customer_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerPricePlan]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerPricePlan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PlanName] [nvarchar](1024) NULL,
	[RebateAmount] [float] NOT NULL CONSTRAINT [DF_Table_1_Amount]  DEFAULT ((0)),
	[Comparison] [int] NOT NULL CONSTRAINT [DF_CustomerPricePlan_Comparison]  DEFAULT ((0)),
	[LimitValue] [decimal](18, 5) NOT NULL CONSTRAINT [DF_CustomerPricePlan_LimitValue]  DEFAULT ((0)),
	[UnitType] [int] NOT NULL CONSTRAINT [DF_CustomerPricePlan_UnitType]  DEFAULT ((0)),
	[PeriodType] [int] NOT NULL CONSTRAINT [DF_CustomerPricePlan_PeriodType]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_CustomerPricePlan_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_CustomerPricePlan_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_CustomerPricePlan_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_CustomerPricePlan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTracking]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTracking](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmailType] [nvarchar](512) NULL,
	[OrderId] [bigint] NULL,
	[Information] [nvarchar](max) NULL,
	[SentOn] [datetime] NOT NULL,
	[SentBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_EmailTracking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExtraRequest]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExtraRequest](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](256) NULL,
	[OrderId] [bigint] NOT NULL,
	[RequestType] [int] NOT NULL,
	[RequestContent] [nvarchar](max) NULL,
	[RequestDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsVisited] [bit] NOT NULL,
 CONSTRAINT [PK_ExtraRequest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Orders] [nvarchar](max) NULL,
	[IssueToCustomerId] [bigint] NOT NULL,
	[InvoicePeriodType] [int] NOT NULL,
	[InvoicePeriod] [int] NOT NULL,
	[InvoicePath] [nvarchar](2048) NULL,
	[IssueBy] [nvarchar](256) NULL,
	[IssueOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Job]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[VehicleId] [int] NULL CONSTRAINT [DF_Job_VehicleId]  DEFAULT ((0)),
	[DriverId] [int] NOT NULL CONSTRAINT [DF_Job_DriverId]  DEFAULT ((0)),
	[DriverType] [int] NOT NULL CONSTRAINT [DF_Job_DriverType]  DEFAULT ((0)),
	[Routes] [nvarchar](max) NULL,
	[TakenOn] [datetime] NULL,
	[TakenBy] [nvarchar](256) NULL,
	[CompletedOn] [datetime] NULL,
	[TotalPickups] [int] NOT NULL CONSTRAINT [DF_Job_TotalPickups]  DEFAULT ((0)),
	[TotalDeliveries] [int] NOT NULL CONSTRAINT [DF_Job_TotalDeliveries]  DEFAULT ((0)),
	[TotalDistances] [float] NOT NULL CONSTRAINT [DF_Table_1_TotalPickupAmount]  DEFAULT ((0)),
	[TotalDrivingTime] [float] NOT NULL CONSTRAINT [DF_Job_TotalDrivingTime]  DEFAULT ((0)),
	[TotalWorkingTime] [float] NOT NULL CONSTRAINT [DF_Job_TotalWorkingTime]  DEFAULT ((0)),
	[TasksCompleted] [int] NOT NULL CONSTRAINT [DF_Job_TasksCompleted]  DEFAULT ((0)),
	[IsSpecial] [bit] NOT NULL CONSTRAINT [DF_Job_IsSpecial]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Job_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Job_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Job_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[JobHistory]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[ConsignmentNumber] [nvarchar](256) NULL,
	[IsPickup] [bit] NOT NULL,
	[IsSuccessful] [bit] NOT NULL,
	[CompletedOn] [datetime] NULL,
	[CompletedBy] [nvarchar](256) NULL,
	[PayRateId] [int] NULL,
	[Payment] [decimal](18, 5) NOT NULL,
	[DeductionAmount] [decimal](18, 5) NOT NULL CONSTRAINT [DF_JobHistory_DeductionAmount]  DEFAULT ((0)),
	[Rating] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_JobHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Log]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[StackTrace] [nvarchar](max) NULL,
	[Location] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Log_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[ToDriver] [bit] NOT NULL,
	[SendTo] [nvarchar](256) NULL,
	[SendToToken] [nvarchar](1024) NULL,
	[SendOn] [datetime] NOT NULL,
	[SendBy] [nvarchar](256) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NULL,
	[BinCode] [nvarchar](64) NULL,
	[ConsignmentNumber] [nvarchar](256) NULL,
	[GroupTrackingNumber] [nvarchar](256) NULL,
	[FromName] [nvarchar](1024) NULL,
	[FromZipCode] [nvarchar](64) NULL,
	[FromAddress] [nvarchar](2048) NULL,
	[FromMobilePhone] [nvarchar](128) NULL,
	[FromSignature] [nvarchar](1024) NULL,
	[ToName] [nvarchar](1024) NULL,
	[ToZipCode] [nvarchar](64) NULL,
	[ToAddress] [nvarchar](2048) NULL,
	[ToMobilePhone] [nvarchar](128) NULL,
	[ToSignature] [nvarchar](1024) NULL,
	[PaymentTypeId] [int] NOT NULL CONSTRAINT [DF_Order_PaymentType]  DEFAULT ((0)),
	[Price] [decimal](18, 5) NOT NULL CONSTRAINT [DF_Order_Price]  DEFAULT ((0)),
	[ServiceId] [int] NOT NULL,
	[ProductTypeId] [int] NOT NULL CONSTRAINT [DF_Order_Type]  DEFAULT ((0)),
	[SizeId] [int] NOT NULL CONSTRAINT [DF_Order_SizeId]  DEFAULT ((0)),
	[Remark] [nvarchar](max) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Order_Status]  DEFAULT ((0)),
	[PickupTimeSlotId] [int] NULL,
	[PickupDate] [datetime] NULL,
	[DeliveryTimeSlotId] [int] NULL,
	[DeliveryDate] [datetime] NULL,
	[QrCodePath] [nvarchar](1024) NULL,
	[PromoCode] [nvarchar](256) NULL,
	[ConfirmedOn] [datetime] NULL,
	[ConfirmedBy] [nvarchar](256) NULL,
	[RejectNote] [nvarchar](max) NULL,
	[CollectedOn] [datetime] NULL,
	[CollectedBy] [nvarchar](256) NULL,
	[ProceededOn] [datetime] NULL,
	[ProceededBy] [nvarchar](256) NULL,
	[ProceededOutOn] [datetime] NULL,
	[ProceededOutBy] [nvarchar](256) NULL,
	[DeliveredOn] [datetime] NULL,
	[DeliveredBy] [nvarchar](256) NULL,
	[CanceledOn] [datetime] NULL,
	[CanceledBy] [nvarchar](256) NULL,
	[FullfilmentNumber] [nvarchar](50) NULL,
	[OperatorRemark] [nvarchar](max) NULL,
	[IsExchange] [bit] NOT NULL CONSTRAINT [DF_Order_IsExchange]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Order_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Order_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Order_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderTracking]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderTracking](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ConsignmentNumber] [nvarchar](256) NULL,
	[BinCode] [nvarchar](64) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_OrderTracking_Status]  DEFAULT ((0)),
	[PickupTimeSlotId] [int] NULL,
	[PickupDate] [datetime] NULL,
	[DeliveryTimeSlotId] [int] NULL,
	[DeliveryDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_OrderTracking_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_OrderTracking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Orders] [nvarchar](max) NULL,
	[PaymentAccountId] [bigint] NULL,
	[Payer] [nvarchar](256) NULL,
	[PaymentAmount] [decimal](18, 5) NOT NULL CONSTRAINT [DF_Payment_PaymentAmount]  DEFAULT ((0)),
	[AccountType] [int] NOT NULL CONSTRAINT [DF_Payment_AccountType]  DEFAULT ((0)),
	[IsSuccessful] [bit] NOT NULL CONSTRAINT [DF_Payment_IsSuccessful]  DEFAULT ((0)),
	[Remark] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Payment_CreatedOn]  DEFAULT (getdate()),
	[InvoiceSent] [bit] NOT NULL CONSTRAINT [DF_Payment_InvoiceSent]  DEFAULT ((0)),
	[InvoiceSentOn] [datetime] NULL CONSTRAINT [DF_Payment_InvoiceSentOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentAccount]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentAccount](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Owner] [nvarchar](256) NULL,
	[AccountType] [int] NOT NULL,
	[FirstName] [nvarchar](1024) NULL,
	[LastName] [nvarchar](1024) NULL,
	[MobileNumber] [nvarchar](128) NULL,
	[CompanyName] [nvarchar](1024) NULL,
	[TransactionNumber] [nvarchar](1024) NULL,
	[TransactionOn] [datetime] NULL,
	[CardNumber] [nvarchar](256) NULL,
	[ExpireMonth] [int] NULL,
	[ExpireYear] [int] NULL,
	[PaypalCardId] [nvarchar](1024) NULL,
	[Currency] [nvarchar](16) NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PaymentAccount_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_PaymentAccount_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_PaymentAccount_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_PaymentAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentType]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PaymentTypeName] [nvarchar](1024) NULL,
	[Description] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PaymentType_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_PaymentType_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_PaymentType_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayRate]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PaySchemeId] [int] NOT NULL,
	[ServiceId] [int] NOT NULL,
	[SizeId] [int] NOT NULL,
	[Amount] [decimal](18, 5) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_PayRate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayScheme]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayScheme](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchemeName] [nvarchar](2048) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_PayScheme] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pricing]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing](
	[ProductTypeId] [int] NOT NULL,
	[ServiceId] [int] NOT NULL,
	[SizeId] [int] NOT NULL,
	[ChargedCost] [decimal](18, 5) NOT NULL CONSTRAINT [DF_Pricing_ChargedCost]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Pricing_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Pricing_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Pricing_1] PRIMARY KEY CLUSTERED 
(
	[ProductTypeId] ASC,
	[ServiceId] ASC,
	[SizeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductTypeName] [nvarchar](1024) NULL,
	[Description] [nvarchar](max) NULL,
	[BinCode] [nvarchar](16) NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_ProductType_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ProductType_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_ProductType_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Service]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Service](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [nvarchar](1024) NULL,
	[Description] [nvarchar](max) NULL,
	[Cost] [decimal](18, 5) NOT NULL CONSTRAINT [DF_Service_Cost]  DEFAULT ((0)),
	[IsPickupTimeAllow] [bit] NOT NULL CONSTRAINT [DF_Service_IsPickupTimeAllow]  DEFAULT ((1)),
	[IsPickupDateAllow] [bit] NOT NULL CONSTRAINT [DF_Service_IsPickupDateAllow]  DEFAULT ((1)),
	[IsDeliveryTimeAllow] [bit] NOT NULL CONSTRAINT [DF_Service_IsDeliveryTimeAllow]  DEFAULT ((1)),
	[IsDeliveryDateAllow] [bit] NOT NULL CONSTRAINT [DF_Service_IsDeliveryDateAllow]  DEFAULT ((1)),
	[AvailablePickupTimeSlots] [nvarchar](1024) NULL,
	[AvailablePickupDateRange] [nvarchar](1024) NULL,
	[AvailableDeliveryTimeSlots] [nvarchar](1024) NULL,
	[AvailableDeliveryDateRange] [nvarchar](1024) NULL,
	[ProductTypeId] [int] NOT NULL CONSTRAINT [DF_Service_ProductTypeId]  DEFAULT ((0)),
	[LimitToSizes] [nvarchar](1024) NULL,
	[IsAppliedDefaultPromoCode] [bit] NOT NULL CONSTRAINT [DF_Service_IsAppliedDefaultPromoCode]  DEFAULT ((1)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Service_IsActive]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Service_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Service_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Service_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Setting]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](512) NULL,
	[Value] [nvarchar](max) NULL,
	[Type] [int] NOT NULL CONSTRAINT [DF_Setting_Type]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Setting_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Setting_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Size]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Size](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SizeName] [nvarchar](1024) NULL,
	[Description] [nvarchar](max) NULL,
	[FromWeight] [float] NOT NULL CONSTRAINT [DF_Size_FromWeight]  DEFAULT ((0)),
	[ToWeight] [float] NOT NULL CONSTRAINT [DF_Size_ToWeight]  DEFAULT ((0)),
	[FromLength] [float] NOT NULL CONSTRAINT [DF_Size_FromLength]  DEFAULT ((0)),
	[ToLength] [float] NOT NULL CONSTRAINT [DF_Size_ToLength]  DEFAULT ((0)),
	[ProductTypeId] [int] NOT NULL CONSTRAINT [DF_Size_ProductTypeId]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Size_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Size_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Size_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Size] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Staff]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[StaffName] [nvarchar](1024) NULL,
	[StaffAddress] [nvarchar](2048) NULL,
	[StaffCountry] [nvarchar](32) NULL,
	[StaffMobile] [nvarchar](128) NULL,
	[StaffPhoto] [nvarchar](1024) NULL,
	[StaffIdentityNumber] [nvarchar](128) NULL,
	[ICNumber] [nvarchar](128) NULL,
	[DrivingLicense] [nvarchar](256) NULL,
	[PayRateId] [int] NULL,
	[Rating] [int] NOT NULL CONSTRAINT [DF_Staff_Rating]  DEFAULT ((0)),
	[Remark] [nvarchar](max) NULL,
	[NotificationKey] [nvarchar](1024) NULL,
	[PhoneType] [int] NOT NULL CONSTRAINT [DF_Staff_PhoneType]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Staff_IsActive]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Staff_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Staff_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Staff_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TimeSlot]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeSlot](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TimeSlotName] [nvarchar](128) NULL,
	[BinCode] [nvarchar](16) NULL,
	[FromTime] [datetime] NOT NULL CONSTRAINT [DF_TimeSlot_FromTime]  DEFAULT (getdate()),
	[ToTime] [datetime] NOT NULL CONSTRAINT [DF_TimeSlot_ToTime]  DEFAULT (getdate()),
	[IsAllowPreOrder] [bit] NOT NULL CONSTRAINT [DF_TimeSlot_IsAllowPreOrder]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_TimeSlot_IsDeleted]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_TimeSlot_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_TimeSlot_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_TimeSlot] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Token]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Token](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[TokenKey] [nvarchar](1024) NULL,
	[BindingValue] [nvarchar](max) NULL,
	[IsExpired] [bit] NOT NULL CONSTRAINT [DF_Token_IsExpired]  DEFAULT ((0)),
	[ExpiredOn] [datetime] NOT NULL CONSTRAINT [DF_Table_1_ExpireTime]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Token_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Token] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TopupPayment]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TopupPayment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PaymentAccountId] [bigint] NULL,
	[Payer] [nvarchar](256) NULL,
	[PaymentAmount] [decimal](18, 5) NOT NULL CONSTRAINT [DF_TopupPayment_PaymentAmount]  DEFAULT ((0)),
	[TransferAmount] [decimal](18, 5) NOT NULL CONSTRAINT [DF_TopupPayment_TransferAmount]  DEFAULT ((0)),
	[Status] [int] NOT NULL CONSTRAINT [DF_TopupPayment_Status]  DEFAULT ((0)),
	[IsSuccessful] [bit] NOT NULL CONSTRAINT [DF_TopupPayment_IsSuccessful]  DEFAULT ((0)),
	[ApprovedOn] [datetime] NULL,
	[ApprovedBy] [nvarchar](256) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_TopupPayment_CreatedOn]  DEFAULT (getdate()),
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_TopupPayment_UpdatedOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_TopupPayment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TrackLocation]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrackLocation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[ConsignmentNumber] [nvarchar](256) NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_TrackLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_UserProfile_IsActive]  DEFAULT ((1)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_UserProfile_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_UserProfile_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VehicleNumber] [nvarchar](128) NULL,
	[VehicleType] [nvarchar](256) NULL,
	[Capacity] [float] NOT NULL,
	[MileAge] [float] NOT NULL,
	[Color] [nvarchar](128) NULL,
	[FixedCostPerTrip] [decimal](18, 2) NOT NULL,
	[CostPerKilometer] [decimal](18, 2) NOT NULL,
	[DistanceLimit] [float] NOT NULL,
	[WorkStartTime] [float] NOT NULL,
	[DrivingTimeLimit] [float] NOT NULL,
	[WorkingTimeLimit] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ZipCode]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZipCode](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PostalCode] [nvarchar](32) NULL,
	[BuildingName] [nvarchar](1024) NULL,
	[UnitNumber] [nvarchar](32) NULL,
	[StreetName] [nvarchar](1024) NULL,
	[BuildingType] [nvarchar](1024) NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[DistrictCode] [nvarchar](16) NULL,
	[Zone] [nvarchar](16) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ZipCode_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_ZipCode_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_ZipCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [smallint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Counter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Job]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[List]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Server]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Set]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[State]    Script Date: 8/25/2016 11:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Charge_ChargeCode]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Charge_ChargeCode] ON [dbo].[Charge]
(
	[ChargeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Index_ConsignmentNumber]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [Index_ConsignmentNumber] ON [dbo].[Order]
(
	[ConsignmentNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UX_HangFire_CounterAggregated_Key]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_CounterAggregated_Key] ON [HangFire].[AggregatedCounter]
(
	[Key] ASC
)
INCLUDE ( 	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Counter_Key]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Counter_Key] ON [HangFire].[Counter]
(
	[Key] ASC
)
INCLUDE ( 	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Hash_ExpireAt]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Hash_Key]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_Key] ON [HangFire].[Hash]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UX_HangFire_Hash_Key_Field]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_Hash_Key_Field] ON [HangFire].[Hash]
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Job_ExpireAt]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Job_StateName]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job]
(
	[StateName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_JobParameter_JobIdAndName]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_JobParameter_JobIdAndName] ON [HangFire].[JobParameter]
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_JobQueue_QueueAndFetchedAt]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_JobQueue_QueueAndFetchedAt] ON [HangFire].[JobQueue]
(
	[Queue] ASC,
	[FetchedAt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_List_ExpireAt]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_List_Key]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_Key] ON [HangFire].[List]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Set_ExpireAt]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_ExpireAt] ON [HangFire].[Set]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Set_Key]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_Key] ON [HangFire].[Set]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UX_HangFire_Set_KeyAndValue]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_Set_KeyAndValue] ON [HangFire].[Set]
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_State_JobId]    Script Date: 8/25/2016 11:34:04 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_State_JobId] ON [HangFire].[State]
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Invoice] ADD  CONSTRAINT [DF_Invoice_InvoicePeriodType]  DEFAULT ((0)) FOR [InvoicePeriodType]
GO
ALTER TABLE [dbo].[Invoice] ADD  CONSTRAINT [DF_Invoice_InvoicePeriod]  DEFAULT ((0)) FOR [InvoicePeriod]
GO
ALTER TABLE [dbo].[Invoice] ADD  CONSTRAINT [DF_Invoice_IssueOn]  DEFAULT (getdate()) FOR [IssueOn]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_ToDriver]  DEFAULT ((1)) FOR [ToDriver]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_SendOn]  DEFAULT (getdate()) FOR [SendOn]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[TrackLocation] ADD  CONSTRAINT [DF_TrackLocation_Latitude]  DEFAULT ((0)) FOR [Latitude]
GO
ALTER TABLE [dbo].[TrackLocation] ADD  CONSTRAINT [DF_TrackLocation_Longitude]  DEFAULT ((0)) FOR [Longitude]
GO
ALTER TABLE [dbo].[TrackLocation] ADD  CONSTRAINT [DF_TrackLocation_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_Capacity]  DEFAULT ((0)) FOR [Capacity]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_MileAge]  DEFAULT ((0)) FOR [MileAge]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_FixedCostPerTrip_1]  DEFAULT ((0)) FOR [FixedCostPerTrip]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_CostPerKilometer_1]  DEFAULT ((0)) FOR [CostPerKilometer]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_DistanceLimit_1]  DEFAULT ((0)) FOR [DistanceLimit]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_WorkStartTime]  DEFAULT ((8)) FOR [WorkStartTime]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_DrivingTimeLimit_1]  DEFAULT ((0)) FOR [DrivingTimeLimit]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_WorkingTimeLimit_1]  DEFAULT ((0)) FOR [WorkingTimeLimit]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Vehicle] ADD  CONSTRAINT [DF_Vehicle_UpdatedOn]  DEFAULT ((0)) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[CodOptionOfOrder]  WITH CHECK ADD  CONSTRAINT [FK_CodOptionOfOrder_CodOption] FOREIGN KEY([CodOptionId])
REFERENCES [dbo].[CodOption] ([Id])
GO
ALTER TABLE [dbo].[CodOptionOfOrder] CHECK CONSTRAINT [FK_CodOptionOfOrder_CodOption]
GO
ALTER TABLE [dbo].[CodOptionOfOrder]  WITH CHECK ADD  CONSTRAINT [FK_CodOptionOfOrder_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[CodOptionOfOrder] CHECK CONSTRAINT [FK_CodOptionOfOrder_Order]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_PaymentType] FOREIGN KEY([PaymentTypeId])
REFERENCES [dbo].[PaymentType] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_PaymentType]
GO
ALTER TABLE [dbo].[ExtraRequest]  WITH CHECK ADD  CONSTRAINT [FK_ExtraRequest_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[ExtraRequest] CHECK CONSTRAINT [FK_ExtraRequest_Order]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Customer] FOREIGN KEY([IssueToCustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_Customer]
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_Vehicle] FOREIGN KEY([VehicleId])
REFERENCES [dbo].[Vehicle] ([Id])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_Vehicle]
GO
ALTER TABLE [dbo].[JobHistory]  WITH CHECK ADD  CONSTRAINT [FK_JobHistory_Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([Id])
GO
ALTER TABLE [dbo].[JobHistory] CHECK CONSTRAINT [FK_JobHistory_Job]
GO
ALTER TABLE [dbo].[JobHistory]  WITH CHECK ADD  CONSTRAINT [FK_JobHistory_PayRate] FOREIGN KEY([PayRateId])
REFERENCES [dbo].[PayRate] ([Id])
GO
ALTER TABLE [dbo].[JobHistory] CHECK CONSTRAINT [FK_JobHistory_PayRate]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customer]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_PaymentType] FOREIGN KEY([PaymentTypeId])
REFERENCES [dbo].[PaymentType] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_PaymentType]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_ProductType] FOREIGN KEY([ProductTypeId])
REFERENCES [dbo].[ProductType] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_ProductType]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Service] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Service] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Service]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Size] FOREIGN KEY([SizeId])
REFERENCES [dbo].[Size] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Size]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_PaymentAccount] FOREIGN KEY([PaymentAccountId])
REFERENCES [dbo].[PaymentAccount] ([Id])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_PaymentAccount]
GO
ALTER TABLE [dbo].[PayRate]  WITH CHECK ADD  CONSTRAINT [FK_PayRate_PayScheme] FOREIGN KEY([PaySchemeId])
REFERENCES [dbo].[PayScheme] ([Id])
GO
ALTER TABLE [dbo].[PayRate] CHECK CONSTRAINT [FK_PayRate_PayScheme]
GO
ALTER TABLE [dbo].[PayRate]  WITH CHECK ADD  CONSTRAINT [FK_PayRate_Service] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Service] ([Id])
GO
ALTER TABLE [dbo].[PayRate] CHECK CONSTRAINT [FK_PayRate_Service]
GO
ALTER TABLE [dbo].[PayRate]  WITH CHECK ADD  CONSTRAINT [FK_PayRate_Size] FOREIGN KEY([SizeId])
REFERENCES [dbo].[Size] ([Id])
GO
ALTER TABLE [dbo].[PayRate] CHECK CONSTRAINT [FK_PayRate_Size]
GO
ALTER TABLE [dbo].[Pricing]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_ProductType] FOREIGN KEY([ProductTypeId])
REFERENCES [dbo].[ProductType] ([Id])
GO
ALTER TABLE [dbo].[Pricing] CHECK CONSTRAINT [FK_Pricing_ProductType]
GO
ALTER TABLE [dbo].[Pricing]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_Service] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Service] ([Id])
GO
ALTER TABLE [dbo].[Pricing] CHECK CONSTRAINT [FK_Pricing_Service]
GO
ALTER TABLE [dbo].[Pricing]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_Size] FOREIGN KEY([SizeId])
REFERENCES [dbo].[Size] ([Id])
GO
ALTER TABLE [dbo].[Pricing] CHECK CONSTRAINT [FK_Pricing_Size]
GO
ALTER TABLE [dbo].[Service]  WITH CHECK ADD  CONSTRAINT [FK_Service_ProductType] FOREIGN KEY([ProductTypeId])
REFERENCES [dbo].[ProductType] ([Id])
GO
ALTER TABLE [dbo].[Service] CHECK CONSTRAINT [FK_Service_ProductType]
GO
ALTER TABLE [dbo].[Size]  WITH CHECK ADD  CONSTRAINT [FK_Size_ProductType] FOREIGN KEY([ProductTypeId])
REFERENCES [dbo].[ProductType] ([Id])
GO
ALTER TABLE [dbo].[Size] CHECK CONSTRAINT [FK_Size_ProductType]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK_Staff_PayScheme] FOREIGN KEY([PayRateId])
REFERENCES [dbo].[PayScheme] ([Id])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_Staff_PayScheme]
GO
ALTER TABLE [dbo].[TrackLocation]  WITH CHECK ADD  CONSTRAINT [FK_TrackLocation_Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([Id])
GO
ALTER TABLE [dbo].[TrackLocation] CHECK CONSTRAINT [FK_TrackLocation_Job]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
USE [master]
GO
ALTER DATABASE [Courier] SET  READ_WRITE 
GO
