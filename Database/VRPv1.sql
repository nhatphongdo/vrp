USE [master]
GO
/****** Object:  Database [VRP]    Script Date: 10/14/2015 1:09:08 PM ******/
CREATE DATABASE [VRP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VRP', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\VRP.mdf' , SIZE = 17408KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VRP_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\VRP_log.ldf' , SIZE = 76736KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VRP] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VRP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VRP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VRP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VRP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VRP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VRP] SET ARITHABORT OFF 
GO
ALTER DATABASE [VRP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VRP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VRP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VRP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VRP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VRP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VRP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VRP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VRP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VRP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VRP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VRP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VRP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VRP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VRP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VRP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VRP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VRP] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [VRP] SET  MULTI_USER 
GO
ALTER DATABASE [VRP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VRP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VRP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VRP] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [VRP]
GO
/****** Object:  User [vrp]    Script Date: 10/14/2015 1:09:12 PM ******/
CREATE USER [vrp] FOR LOGIN [vrp] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [vrp]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [vrp]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [vrp]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [vrp]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [vrp]
GO
ALTER ROLE [db_datareader] ADD MEMBER [vrp]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [vrp]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 10/14/2015 1:09:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Distance]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Distance](
	[FromId] [int] NOT NULL,
	[ToId] [int] NOT NULL,
	[Distance] [float] NOT NULL CONSTRAINT [DF_Distance_Distance]  DEFAULT ((0)),
	[Duration] [float] NOT NULL CONSTRAINT [DF_Distance_Time]  DEFAULT ((0)),
	[PlanId] [int] NOT NULL CONSTRAINT [DF_Distance_PlanId]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Distance_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Distance_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Distance] PRIMARY KEY CLUSTERED 
(
	[FromId] ASC,
	[ToId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Location]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](1024) NULL,
	[PostalCode] [nvarchar](32) NULL,
	[StreetName] [nvarchar](1024) NULL,
	[UnitNumber] [nvarchar](32) NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[Starting] [bit] NOT NULL CONSTRAINT [DF_Location_Starting]  DEFAULT ((0)),
	[MustBeVisited] [int] NOT NULL CONSTRAINT [DF_Location_MustBeVisited]  DEFAULT ((0)),
	[ServiceTime] [float] NOT NULL CONSTRAINT [DF_Location_ServiceTime]  DEFAULT ((0)),
	[Service] [int] NOT NULL CONSTRAINT [DF_Location_Service]  DEFAULT ((0)),
	[Type] [int] NOT NULL CONSTRAINT [DF_Location_Type]  DEFAULT ((0)),
	[Supply] [float] NOT NULL CONSTRAINT [DF_Location_Supply]  DEFAULT ((0)),
	[Revenue] [decimal](18, 2) NOT NULL CONSTRAINT [DF_Location_Revenue]  DEFAULT ((0)),
	[FromTime] [float] NOT NULL CONSTRAINT [DF_Location_FromTime]  DEFAULT ((0)),
	[ToTime] [float] NOT NULL CONSTRAINT [DF_Location_ToTime]  DEFAULT ((0)),
	[PlanId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Location_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Location_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Plan]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberOfCustomers] [int] NOT NULL CONSTRAINT [DF_Plan_Customers]  DEFAULT ((0)),
	[Type] [int] NOT NULL CONSTRAINT [DF_Plan_Type]  DEFAULT ((0)),
	[ComputationMethod] [int] NOT NULL CONSTRAINT [DF_Plan_ComputationMethod]  DEFAULT ((0)),
	[RouteType] [int] NOT NULL CONSTRAINT [DF_Plan_RouteType]  DEFAULT ((0)),
	[AverageVehicleSpeed] [float] NOT NULL CONSTRAINT [DF_Plan_AverageVehicleSpeed]  DEFAULT ((0)),
	[NumberOfVehicleTypes] [int] NOT NULL CONSTRAINT [DF_Plan_NumberOfVehicleTypes]  DEFAULT ((0)),
	[AllVehiclesAreUsed] [bit] NOT NULL CONSTRAINT [DF_Plan_AllVehiclesAreUsed]  DEFAULT ((0)),
	[ReturnToDepot] [bit] NOT NULL CONSTRAINT [DF_Plan_ReturnToDepot]  DEFAULT ((1)),
	[TimeWindowType] [int] NOT NULL CONSTRAINT [DF_Plan_TimeWindowType]  DEFAULT ((0)),
	[WorkStartTime] [float] NOT NULL CONSTRAINT [DF_Plan_WorkStartTime]  DEFAULT ((8.00)),
	[WorkEndTime] [float] NOT NULL CONSTRAINT [DF_Plan_WorkEndTime]  DEFAULT ((18.00)),
	[DrivingTimeLimit] [float] NOT NULL CONSTRAINT [DF_Plan_DrivingTimeLimit]  DEFAULT ((8)),
	[WorkingTimeLimit] [float] NOT NULL CONSTRAINT [DF_Plan_WorkingTimeLimit]  DEFAULT ((10)),
	[DistanceLimit] [float] NOT NULL CONSTRAINT [DF_Plan_DistanceLimit]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Plan_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Plan_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Plan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlanResult]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlanResult](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PlanId] [int] NOT NULL,
	[Result] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_PlanResult_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_PlanResult_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_PlanResult] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostalCode]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostalCode](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PostalCode] [nvarchar](32) NULL,
	[BuildingName] [nvarchar](1024) NULL,
	[UnitNumber] [nvarchar](32) NULL,
	[StreetName] [nvarchar](1024) NULL,
	[BuildingType] [nvarchar](1024) NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_PostalCode_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_PostalCode_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_PostalCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Setting]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[Value] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Setting_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Setting_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 10/14/2015 1:09:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](512) NULL,
	[Type] [nvarchar](128) NULL,
	[Capacity] [float] NOT NULL CONSTRAINT [DF_Vehicle_Capacity]  DEFAULT ((0)),
	[FixedCostPerTrip] [decimal](18, 2) NOT NULL CONSTRAINT [DF_Vehicle_FixedCostPerTrip]  DEFAULT ((0)),
	[CostPerKilometer] [decimal](18, 2) NOT NULL CONSTRAINT [DF_Vehicle_CostPerKilometer]  DEFAULT ((0)),
	[Quantity] [int] NOT NULL CONSTRAINT [DF_Vehicle_Quantity]  DEFAULT ((0)),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_Vehicle_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](256) NULL,
	[UpdatedOn] [datetime] NOT NULL CONSTRAINT [DF_Vehicle_UpdatedOn]  DEFAULT (getdate()),
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 10/14/2015 1:09:14 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 10/14/2015 1:09:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 10/14/2015 1:09:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 10/14/2015 1:09:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 10/14/2015 1:09:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 10/14/2015 1:09:14 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Distance]  WITH CHECK ADD  CONSTRAINT [FK_Distance_Location] FOREIGN KEY([FromId])
REFERENCES [dbo].[Location] ([Id])
GO
ALTER TABLE [dbo].[Distance] CHECK CONSTRAINT [FK_Distance_Location]
GO
ALTER TABLE [dbo].[Distance]  WITH CHECK ADD  CONSTRAINT [FK_Distance_Location1] FOREIGN KEY([ToId])
REFERENCES [dbo].[Location] ([Id])
GO
ALTER TABLE [dbo].[Distance] CHECK CONSTRAINT [FK_Distance_Location1]
GO
ALTER TABLE [dbo].[Distance]  WITH CHECK ADD  CONSTRAINT [FK_Distance_Plan] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([Id])
GO
ALTER TABLE [dbo].[Distance] CHECK CONSTRAINT [FK_Distance_Plan]
GO
ALTER TABLE [dbo].[Location]  WITH CHECK ADD  CONSTRAINT [FK_Location_Plan] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([Id])
GO
ALTER TABLE [dbo].[Location] CHECK CONSTRAINT [FK_Location_Plan]
GO
ALTER TABLE [dbo].[PlanResult]  WITH CHECK ADD  CONSTRAINT [FK_PlanResult_Plan] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([Id])
GO
ALTER TABLE [dbo].[PlanResult] CHECK CONSTRAINT [FK_PlanResult_Plan]
GO
USE [master]
GO
ALTER DATABASE [VRP] SET  READ_WRITE 
GO
