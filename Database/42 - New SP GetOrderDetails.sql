USE [Courier]
GO

/****** Object:  StoredProcedure [dbo].[USP_GetOrderDetails]    Script Date: 24/11/2016 2:47:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_GetOrderDetails]
@ConsignmentNumber varchar(max)
AS
BEGIN
Declare 
@CodOptionTypesCash int = 0,
@CodOptionTypesCheque int = 1,
@ExtraRequestTypeReDelivery int = 2, --As per ExtraRequestType enum in CDS
@ExtraRequestTypeRePickUp int = 3,--As per ExtraRequestType enum in CDS
@AccountTypeDriver int = 0,--As per AccountTypes enum in CDS
@AccountTypeContractor int = 1,--As per AccountTypes enum in CDS
@StatusItemHasBeingPicked int = 12, --As per OrderStatuses enum in  CDS
@StatusItemInDepot int = 5, --As per OrderStatuses enum in  CDS
@StatusPickupFailed int = 4, --As per OrderStatuses enum in  CDS
@StatusItemDelivered int = 7, --As per OrderStatuses enum in  CDS
@StatusItemCollected int = 3, --As per OrderStatuses enum in  CDS
@StatusItemOutDepot int = 6, --As per OrderStatuses enum in  CDS
@StatusDeliveryFailed int = 9, --As per OrderStatuses enum in  CDS
@StatusSpecialOrderForPickup int = 10, --As per OrderStatuses enum in  CDS
@StatusSpecialOrderForDelivery int = 11, --As per OrderStatuses enum in  CDS
@StatusRePickup int = 13, --As per OrderStatuses enum in  CDS
@StatusReDelivery int = 14, --As per OrderStatuses enum in  CDS
@StatusReturnToSender int = 15 --As per OrderStatuses enum in  CDS
SELECT 
case 
	--Item has been picked/Enroute to pickup
	when (O.Status = @StatusItemHasBeingPicked  and IHBPDriver.Id is not null) then IHBPDriver.StaffName
	when (O.Status = @StatusItemHasBeingPicked  and IHBPOPS.Id is not null) then IHBPOPS.ContractorName
	when (O.Status = @StatusItemHasBeingPicked  and IHBPDriver.Id is null and IHBPOPS.Id is null) then JH.CompletedBy
	--Item in warehouse/Item in depot
	when (O.Status = @StatusItemInDepot  and IIDOPS.Id is not null) then IIDOPS.ContractorName
	when (O.Status = @StatusItemInDepot  and IIDOPS.Id is null) then O.ProceededBy
	--Pickup Failed
	when (O.Status = @StatusPickupFailed  and PFDriver.Id is not null) then PFDriver.StaffName
	when (O.Status = @StatusPickupFailed  and PFOPS.Id is not null) then PFOPS.ContractorName
	when (O.Status = @StatusPickupFailed  and PFDriver.Id is null and PFOPS.Id is null) then O.CollectedBy
	--Item is delivered 
	when (O.Status = @StatusItemDelivered  and IDDriver.Id is not null) then IDDriver.StaffName
	when (O.Status = @StatusItemDelivered  and PFOPS.Id is  null) then O.DeliveredBy
	--Item Collected
	when (O.Status = @StatusItemCollected  and ICDriver.Id is not null) then ICDriver.StaffName
	when (O.Status = @StatusItemCollected  and ICDriver.Id is null) then O.CollectedBy
	--Item out depot/Delivery in progress
	when (O.Status = @StatusItemOutDepot  and IODDriver.Id is not null) then IODDriver.StaffName
	when (O.Status = @StatusItemOutDepot  and IODOPS.Id is not null) then IODOPS.ContractorName
	when (O.Status = @StatusItemOutDepot  and IODDriver.Id is null) then O.ProceededOutBy
	when (O.Status = @StatusItemOutDepot  and IHBPDriver.Id is null and IHBPOPS.Id is null) then JH.CompletedBy
	--Delivery Failed
	when (O.Status = @StatusDeliveryFailed  and DFDriver.Id is not null) then DFDriver.StaffName 
	when (O.Status = @StatusDeliveryFailed  and DFOPS.Id is not null) then DFOPS.ContractorName
	when (O.Status = @StatusDeliveryFailed  and DFDriver.Id is null and DFOPS.Id is null) then O.DeliveredBy
	--Spl order for pickup
	when (O.Status = @StatusSpecialOrderForPickup  and SPPUDriver.Id is not null) then SPPUDriver.StaffName 
	when (O.Status = @StatusSpecialOrderForPickup  and SPPUDriver.Id is null) then OT.CreatedBy
	--Spl order for delivery
	when (O.Status = @StatusSpecialOrderForDelivery  and SPDDriver.Id is not null) then SPDDriver.StaffName 
	when (O.Status = @StatusSpecialOrderForDelivery  and SPDDriver.Id is null) then OT.CreatedBy
	--Re pickup
	when (O.Status = @StatusRePickup  and RPUDriver.Id is not null) then RPUDriver.StaffName 
	when (O.Status = @StatusRePickup  and RPUDriver.Id is null) then OT.CreatedBy
	--Re delivery
	when (O.Status = @StatusReDelivery  and RDDriver.Id is not null) then RDDriver.StaffName 
	when (O.Status = @StatusReDelivery  and RDDriver.Id is null) then OT.CreatedBy
	--Return to sender
	when (O.Status = @StatusReturnToSender  and RSDOPS.Id is not null) then RSDOPS.ContractorName 
	when (O.Status = @StatusReturnToSender  and RSDOPS.Id is null) then O.UpdatedBy

else ''
  End as DriverOrOpsName,

  case 
    --Item has been picked/Enroute to pickup
	when (O.Status = @StatusItemHasBeingPicked  and IHBPDriver.Id is not null) then IHBPDriver.StaffMobile
	when (O.Status = @StatusItemHasBeingPicked  and IHBPOPS.Id is not null) then IHBPOPS.ContractorMobile
	--Item in warehouse/Item in depot
	when (O.Status = @StatusItemInDepot  and IIDOPS.Id is not null) then IIDOPS.ContractorMobile
	--Pickup Failed
	when (O.Status = @StatusPickupFailed  and PFDriver.Id is not null) then PFDriver.StaffMobile
	when (O.Status = @StatusPickupFailed  and PFOPS.Id is not null) then PFOPS.ContractorMobile
	--Item is delivered 
	when (O.Status = @StatusItemDelivered  and IDDriver.Id is not null) then IDDriver.StaffMobile
	--Item Collected
	when (O.Status = @StatusItemCollected  and ICDriver.Id is not null) then ICDriver.StaffMobile
	--Item out depot/Delivery in progress
	when (O.Status = @StatusItemOutDepot  and IODDriver.Id is not null) then IODDriver.StaffMobile
	--Delivery Failed
	when (O.Status = @StatusDeliveryFailed  and DFDriver.Id is not null) then DFDriver.StaffMobile 
	when (O.Status = @StatusDeliveryFailed  and DFOPS.Id is not null) then DFOPS.ContractorMobile
	--Spl order for pickup
	when (O.Status = @StatusSpecialOrderForPickup  and SPPUDriver.Id is not null) then SPPUDriver.StaffMobile
	--Spl order for delivery
	when (O.Status = @StatusSpecialOrderForDelivery  and SPDDriver.Id is not null) then SPPUDriver.StaffMobile
	--Re pickup
	when (O.Status = @StatusRePickup  and RPUDriver.Id is not null) then SPDDriver.StaffMobile
	--Re delivery
	when (O.Status = @StatusReDelivery  and RDDriver.Id is not null) then RDDriver.StaffMobile
	--Return to sender
	when (O.Status = @StatusReturnToSender  and RSDOPS.Id is not null) then RSDOPS.ContractorMobile

else ''
  End as DriverOrOpsMobile,

  case 
    --Item has been picked/Enroute to pickup
	when (O.Status = @StatusItemHasBeingPicked) then J.TakenOn
	--Item in warehouse/Item in depot
	when (O.Status = @StatusItemInDepot) then O.ProceededOn
	--Pickup Failed
	when (O.Status = @StatusPickupFailed) then O.CollectedOn
	--Item is delivered 
	when (O.Status = @StatusItemDelivered) then O.DeliveredOn
	--Item Collected 
	when (O.Status = @StatusItemCollected) then O.CollectedOn
	--Item out depot/Delivery in progress
	when (O.Status = @StatusItemOutDepot) then O.ProceededOutOn
	--Delivery Failed
	when (O.Status = @StatusDeliveryFailed) then O.DeliveredOn
	--Spl order for pickup
	when (O.Status = @StatusSpecialOrderForPickup) then OT.CreatedOn 
	--Spl order for delivery
	when (O.Status = @StatusSpecialOrderForDelivery) then OT.CreatedOn 
	--Re pickup
	when (O.Status = @StatusRePickup) then OT.CreatedOn 
	--Re delivery
	when (O.Status = @StatusReDelivery) then OT.CreatedOn 
	--Return to sender
	when (O.Status = @StatusReturnToSender ) then O.UpdatedOn


else ''
  End as DriverOrOpsTimeStamp

FROM [order] O
LEFT JOIN JobHistory JH on JH.ConsignmentNumber = O.ConsignmentNumber and JH.Id = (SELECT MAX(ID) from JobHistory JHL WHERE JHL.ConsignmentNumber = O.ConsignmentNumber)
LEFT JOIN Job J on J.Id = JH.JobId
LEFT JOIN OrderTracking OT on OT.ConsignmentNumber = O.ConsignmentNumber and OT.Id = (SELECT MAX(ID) from OrderTracking OTL WHERE OTL.ConsignmentNumber = O.ConsignmentNumber and OTL.Status = O.Status)

LEFT JOIN Staff as IHBPDriver on IHBPDriver.UserName = JH.CompletedBy and J.DriverType = @AccountTypeDriver
LEFT JOIN Contractor as IHBPOPS on IHBPOPS.UserName = JH.CompletedBy and J.DriverType = @AccountTypeContractor

LEFT JOIN Contractor as IIDOPS on IIDOPS.UserName = O.ProceededBy


LEFT JOIN Staff as PFDriver on PFDriver.UserName = O.CollectedBy
LEFT JOIN Contractor as PFOPS on PFOPS.UserName = O.CollectedBy


LEFT JOIN Staff as IDDriver on IDDriver.UserName = O.DeliveredBy
LEFT JOIN Staff as ICDriver on ICDriver.UserName = O.CollectedBy

--LEFT JOIN Staff as IODDriver on IODDriver.UserName = O.ProceededOutBy
LEFT JOIN Staff as IODDriver on IODDriver.UserName = JH.CompletedBy and J.DriverType = @AccountTypeDriver
LEFT JOIN Contractor as IODOPS on IODOPS.UserName = JH.CompletedBy and J.DriverType = @AccountTypeContractor

LEFT JOIN Staff as DFDriver on DFDriver.UserName = O.DeliveredBy
LEFT JOIN Contractor as DFOPS on DFOPS.UserName = O.DeliveredBy
LEFT JOIN Staff as SPPUDriver on SPPUDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as SPDDriver on SPDDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as RPUDriver on RPUDriver.UserName = OT.CreatedBy
LEFT JOIN Staff as RDDriver on RDDriver.UserName = OT.CreatedBy
LEFT JOIN Contractor as RSDOPS on RSDOPS.UserName = O.UpdatedBy
WHERE O.IsDeleted = 0 and O.ConsignmentNumber = @ConsignmentNumber

END
GO


