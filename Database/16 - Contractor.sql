/*
   Tuesday, August 9, 20165:30:50 PM
   User: 
   Server: PHONGDO-SURFACE
   Database: Courier
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Contractor
	DROP CONSTRAINT DF_Contractor_Rating
GO
ALTER TABLE dbo.Contractor
	DROP CONSTRAINT DF_Contractor_PhoneType
GO
ALTER TABLE dbo.Contractor
	DROP CONSTRAINT DF_Contractor_IsActive
GO
ALTER TABLE dbo.Contractor
	DROP CONSTRAINT DF_Contractor_IsDeleted
GO
ALTER TABLE dbo.Contractor
	DROP CONSTRAINT DF_Contractor_CreatedOn
GO
ALTER TABLE dbo.Contractor
	DROP CONSTRAINT DF_Contractor_UpdatedOn
GO
CREATE TABLE dbo.Tmp_Contractor
	(
	Id int NOT NULL IDENTITY (1, 1),
	UserName nvarchar(256) NULL,
	ContractorName nvarchar(1024) NULL,
	ContractorAddress nvarchar(2048) NULL,
	ContractorCountry nvarchar(32) NULL,
	ContractorMobile nvarchar(128) NULL,
	ContractorPhoto nvarchar(1024) NULL,
	ICNumber nvarchar(128) NULL,
	VehicleType nvarchar(256) NULL,
	DrivingLicense nvarchar(256) NULL,
	VehicleNumber nvarchar(128) NULL,
	PayRateId int NULL,
	Rating int NOT NULL,
	Remark nvarchar(MAX) NULL,
	NotificationKey nvarchar(1024) NULL,
	PhoneType int NOT NULL,
	IsActive bit NOT NULL,
	IsDeleted bit NOT NULL,
	CreatedOn datetime NOT NULL,
	CreatedBy nvarchar(256) NULL,
	UpdatedOn datetime NOT NULL,
	UpdatedBy nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Contractor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Contractor ADD CONSTRAINT
	DF_Contractor_Rating DEFAULT ((0)) FOR Rating
GO
ALTER TABLE dbo.Tmp_Contractor ADD CONSTRAINT
	DF_Contractor_PhoneType DEFAULT ((0)) FOR PhoneType
GO
ALTER TABLE dbo.Tmp_Contractor ADD CONSTRAINT
	DF_Contractor_IsActive DEFAULT ((0)) FOR IsActive
GO
ALTER TABLE dbo.Tmp_Contractor ADD CONSTRAINT
	DF_Contractor_IsDeleted DEFAULT ((0)) FOR IsDeleted
GO
ALTER TABLE dbo.Tmp_Contractor ADD CONSTRAINT
	DF_Contractor_CreatedOn DEFAULT (getdate()) FOR CreatedOn
GO
ALTER TABLE dbo.Tmp_Contractor ADD CONSTRAINT
	DF_Contractor_UpdatedOn DEFAULT (getdate()) FOR UpdatedOn
GO
SET IDENTITY_INSERT dbo.Tmp_Contractor ON
GO
IF EXISTS(SELECT * FROM dbo.Contractor)
	 EXEC('INSERT INTO dbo.Tmp_Contractor (Id, UserName, ContractorName, ContractorAddress, ContractorCountry, ContractorMobile, ContractorPhoto, ICNumber, VehicleType, DrivingLicense, VehicleNumber, Rating, Remark, NotificationKey, PhoneType, IsActive, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy)
		SELECT Id, UserName, ContractorName, ContractorAddress, ContractorCountry, ContractorMobile, ContractorPhoto, ICNumber, VehicleType, DrivingLicense, VehicleNumber, Rating, Remark, NotificationKey, PhoneType, IsActive, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy FROM dbo.Contractor WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Contractor OFF
GO
DROP TABLE dbo.Contractor
GO
EXECUTE sp_rename N'dbo.Tmp_Contractor', N'Contractor', 'OBJECT' 
GO
ALTER TABLE dbo.Contractor ADD CONSTRAINT
	PK_Contractor PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Contractor', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Contractor', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Contractor', 'Object', 'CONTROL') as Contr_Per 