package com.roadbull.inventory;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class DispatchInfoActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 42;
    private EditText consignmentNumber;
    private EditText routeTo;
    private EditText driver;
    private Button scanQr;
    private ProgressDialog dialog;

    private ScannerDevice scanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_info);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        consignmentNumber = (EditText) findViewById(R.id.consignment_number);
        consignmentNumber.setOnFocusChangeListener(this);
        consignmentNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loadInfo();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
        routeTo = (EditText) findViewById(R.id.route_to);
        routeTo.setKeyListener(null);
        driver = (EditText) findViewById(R.id.driver);
        driver.setKeyListener(null);

        scanQr = (Button) findViewById(R.id.scanQr);
        scanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check Android 6 permission
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                } else {
                    ActivityCompat.requestPermissions(getContext(),
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });

        scanner = new ScannerDevice(getContext(), new ScannerDevice.ScannerCallbackInterface() {
            @Override
            public void onSuccess(String data) {
                String[] parts = data == null ? new String[]{} : data.split("\\|");
                if (parts.length > 1) {
                    // Grey out this Consignment Number
                    consignmentNumber.setText(parts[1]);
                    loadInfo();
                }
            }
        });
    }


    public DispatchInfoActivity getContext() {
        return this;
    }


    /**
     * react to the user tapping the back/up icon in the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle scan result
            String[] parts = scanResult.getContents() == null ? new String[]{} : scanResult.getContents().split("\\|");
            if (parts.length > 1) {
                // Grey out this Consignment Number
                consignmentNumber.setText(parts[1]);
                loadInfo();
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (scanner != null) {
            scanner.destroy();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.CAMERA)
                        && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                }
            }
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.getId() == R.id.job_id && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        }
    }


    private void loadInfo() {
        if (consignmentNumber.getText().toString().isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getResources().getString(R.string.invalid_to_consignment_number))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.DispatchInfo(consignmentNumber.getText().toString(), token, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    routeTo.setText(result.getString("RouteTo"));
                    driver.setText(result.getString("Driver"));

                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_unknown_error))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
