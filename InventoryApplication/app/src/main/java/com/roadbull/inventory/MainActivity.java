package com.roadbull.inventory;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set events
        Button item_collected = (Button) findViewById(R.id.item_collected);
        item_collected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ItemCollectedActivity.class));
            }
        });

        Button qr_details = (Button) findViewById(R.id.qr_details);
        qr_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), QrDetailActivity.class));
            }
        });

        Button stock_in_warehouse = (Button) findViewById(R.id.stock_in_warehouse);
        stock_in_warehouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ItemsInWarehouseActivity.class));
            }
        });

        Button print_qr = (Button) findViewById(R.id.print_qr_code);
        print_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Not implemented
            }
        });

        Button failed_delivery = (Button) findViewById(R.id.failed_delivery);
        failed_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), FailedDeliveryActivity.class));
            }
        });

        Button statistics = (Button) findViewById(R.id.statistics);
        statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), StatisticsActivity.class));
            }
        });

        Button verifying_products = (Button) findViewById(R.id.verifying_products);
        verifying_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), VerifyingProductsActivity.class));
            }
        });

        Button dispatch_info = (Button) findViewById(R.id.dispatch_info);
        dispatch_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), DispatchInfoActivity.class));
            }
        });

        Button logout = (Button) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.TOKEN_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.REMEMBER_ACCOUNT);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_USERNAME_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_PASSWORD_KEY);

                finish();
                startActivity(new Intent(getContext(), LoginActivity.class));

            }
        });
    }


    public MainActivity getContext() {
        return this;
    }
}
