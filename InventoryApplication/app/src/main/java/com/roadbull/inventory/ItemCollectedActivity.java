package com.roadbull.inventory;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ItemCollectedActivity extends AppCompatActivity {

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 42;

    private ArrayList<JSONObject> itemList;
    private OrderItemsAdapter listAdapter;
    private TextView mCountLabel;
    private Button mReloadButton;
    private ListView mListView;
    private Button mScanQrButton;
    private ProgressDialog dialog;
    private int scannedCount;

    private ScannerDevice scanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_collected);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        itemList = new ArrayList<>();
        listAdapter = new OrderItemsAdapter(getContext(), itemList);

        mCountLabel = (TextView) findViewById(R.id.count);

        mReloadButton = (Button) findViewById(R.id.reload);
        mReloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadItems();
            }
        });

        mListView = (ListView) findViewById(R.id.listView);
        mListView.setAdapter(listAdapter);

        mScanQrButton = (Button) findViewById(R.id.scanQr);
        mScanQrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check Android 6 permission
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                } else {
                    ActivityCompat.requestPermissions(getContext(),
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });

        scanner = new ScannerDevice(getContext(), new ScannerDevice.ScannerCallbackInterface() {
            @Override
            public void onSuccess(String data) {
                String[] parts = data == null ? new String[]{} : data.split("\\|");
                if (parts.length > 1) {
                    // Grey out this Consignment Number
                    String consignmentNumber = parts[1];
                    putIntoWarehouse(consignmentNumber);
                }
            }
        });

        loadItems();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (scanner != null) {
            scanner.destroy();
        }
    }


    public ItemCollectedActivity getContext() {
        return this;
    }


    /**
     * react to the user tapping the back/up icon in the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle scan result
            String[] parts = scanResult.getContents() == null ? new String[]{} : scanResult.getContents().split("\\|");
            if (parts.length > 1) {
                // Grey out this Consignment Number
                String consignmentNumber = parts[1];
                putIntoWarehouse(consignmentNumber);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.CAMERA)
                        && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                }
            }
        }
    }


    private void loadItems() {
        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        scannedCount = 0;
        listAdapter.clear();
        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.CollectedItems(token, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    JSONArray orders = result.getJSONArray("Result");

                    mCountLabel.setText(String.valueOf(scannedCount) + "/" + String.valueOf(orders.length()));

                    for (int i = 0; i < orders.length(); i++) {
                        listAdapter.add(orders.getJSONObject(i));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_unknown_error))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }


    private void putIntoWarehouse(final String consignmentNumber) {
        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.PutInto(token, consignmentNumber, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    // Grey out
                    for (int i = 0; i < listAdapter.getCount(); i++) {
                        if (listAdapter.getItem(i).optString("ConsignmentNumber").equalsIgnoreCase(consignmentNumber)) {
                            if (listAdapter.getItem(i).optBoolean("Proceeded", false) == false) {
                                ++scannedCount;
                                mCountLabel.setText(String.valueOf(scannedCount) + "/" + String.valueOf(listAdapter.getCount()));
                            }
                            listAdapter.getItem(i).put("Proceeded", true);
                        }
                    }
                    listAdapter.notifyDataSetChanged();

                    final AlertDialog alert = new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.put_item_in_warehouse_successful))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                    // Hide after some seconds
                    final Handler handler  = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alert.isShowing()) {
                                alert.dismiss();
                            }
                        }
                    };

                    alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });

                    handler.postDelayed(runnable, 500); // 0.5 second

                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_unknown_error))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
