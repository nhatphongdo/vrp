package com.roadbull.inventory;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by nhatp on 6/9/2016.
 */
public class JobItemsAdapter extends ArrayAdapter<JSONObject> {
    private final Context context;
    private ArrayList<JSONObject> list;

    public JobItemsAdapter(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.job_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.job_item, parent, false);

        TextView number = (TextView) rowView.findViewById(R.id.number);
        TextView consignmentNumber = (TextView) rowView.findViewById(R.id.consignment_number);
        TextView type = (TextView) rowView.findViewById(R.id.type);
        number.setText(String.valueOf(position + 1));
        consignmentNumber.setText(list.get(position).optString("ConsignmentNumber"));
        type.setText(list.get(position).optInt("Type") == 0 ? "PU" : "D");

        if (list.get(position).optBoolean("Proceeded", false)) {
            number.setPaintFlags(number.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            consignmentNumber.setPaintFlags(consignmentNumber.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            type.setPaintFlags(consignmentNumber.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            number.setTextColor(Color.GRAY);
            consignmentNumber.setTextColor(Color.GRAY);
            type.setTextColor(Color.GRAY);
        }

        return rowView;
    }
}
