package com.roadbull.inventory;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Do Nhat Phong on 10/29/2015.
 */
public class GlobalPreferences {
    private static GlobalPreferences _instance;
    private static SharedPreferences prefs;

    public static final String PREFS_NAME = "RoadbullInventoryPrefs";

    public static final String TOKEN_KEY = "TOKEN_KEY";
    public static final String REMEMBER_ACCOUNT = "REMEMBER_ACCOUNT";
    public static final String USER_ID_KEY = "USER_ID_KEY";
    public static final String USER_EMAIL_KEY = "USER_EMAIL_KEY";
    public static final String USER_USERNAME_KEY = "USER_USERNAME_KEY";
    public static final String USER_PASSWORD_KEY = "USER_PASSWORD_KEY";
    public static final String USER_FULLNAME_KEY = "USER_FULLNAME_KEY";
    public static final String USER_ADDRESS_KEY = "USER_ADDRESS_KEY";
    public static final String USER_COUNTRY_KEY = "USER_COUNTRY_KEY";
    public static final String USER_MOBILE_KEY = "USER_MOBILE_KEY";
    public static final String USER_PHOTO_KEY = "USER_PHOTO_KEY";
    public static final String USER_DRIVING_LICENSE_KEY = "USER_DRIVING_LICENSE_KEY";
    public static final String USER_IC_NUMBER_KEY = "USER_IC_NUMBER_KEY";
    public static final String USER_STAFF_NUMBER_KEY = "USER_STAFF_NUMBER_KEY";
    public static final String USER_VEHICLE_NUMBER_KEY = "USER_VEHICLE_NUMBER_KEY";
    public static final String USER_VEHICLE_TYPE_KEY = "USER_VEHICLE_TYPE_KEY";


    private static Bitmap avatarBitmap;


    public GlobalPreferences(Context context) {
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static GlobalPreferences getInstance(Context context) {
        if (_instance == null) {
            _instance = new GlobalPreferences(context);
        }

        return _instance;
    }

    public void save(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void save(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void save(String key, long value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public void save(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void save(String key, float value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public String get(String key, String defaultValue) {
        return prefs.getString(key, defaultValue);
    }

    public int get(String key, int defaultValue) {
        return prefs.getInt(key, defaultValue);
    }

    public long get(String key, long defaultValue) {
        return prefs.getLong(key, defaultValue);
    }

    public boolean get(String key, boolean defaultValue) {
        return prefs.getBoolean(key, defaultValue);
    }

    public float get(String key, float defaultValue) {
        return prefs.getFloat(key, defaultValue);
    }

    public void remove(String key) {
        prefs.edit().remove(key).commit();
    }

    public void clear() {
        prefs.edit().clear().commit();
    }

    public static boolean isPhoneValid(String email) {
        boolean isValid = false;

        String expression = "^[0-9]+$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static void setAvatar(Bitmap bitmap) {
        avatarBitmap = bitmap;
    }

    public static Bitmap getAvatar() {
        return avatarBitmap;
    }
}
