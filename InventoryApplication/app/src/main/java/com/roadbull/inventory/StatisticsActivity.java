package com.roadbull.inventory;

import android.app.ProgressDialog;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

public class StatisticsActivity extends AppCompatActivity {

    private Button from_date_button;
    private Button to_date_button;
    private TableLayout orders_table;
    private ProgressDialog dialog;
    private JSONArray orders;
    private Date fromDate;
    private Date toDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        from_date_button = (Button) findViewById(R.id.from_date);
        from_date_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                final CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", now.get(Calendar.MONTH) + 1, now.get(Calendar.YEAR));

                if (fromDate != null) {
                    dialogCaldroidFragment.setSelectedDate(fromDate);
                }

                // Add a listener as per documentation
                dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

                    @Override
                    public void onSelectDate(Date date, View view) {
                        // You can perform your method on selected date
                        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                        fromDate = date;
                        from_date_button.setText(dateFormat.format(date));
                        dialogCaldroidFragment.dismiss();
                    }
                });

                // show the dialog
                dialogCaldroidFragment.show(getContext().getSupportFragmentManager(), "TAG");
            }
        });
        to_date_button = (Button) findViewById(R.id.to_date);
        to_date_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                final CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", now.get(Calendar.MONTH) + 1, now.get(Calendar.YEAR));

                if (toDate != null) {
                    dialogCaldroidFragment.setSelectedDate(toDate);
                }

                // Add a listener as per documentation
                dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

                    @Override
                    public void onSelectDate(Date date, View view) {
                        // You can perform your method on selected date
                        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                        toDate = date;
                        to_date_button.setText(dateFormat.format(date));
                        dialogCaldroidFragment.dismiss();
                    }
                });

                // show the dialog
                dialogCaldroidFragment.show(getContext().getSupportFragmentManager(), "TAG");
            }
        });
        orders_table = (TableLayout) findViewById(R.id.orders_table);

        Button refresh = (Button) findViewById(R.id.reload);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadStatistics();
            }
        });

        loadStatistics();
    }


    public StatisticsActivity getContext() {
        return this;
    }


    /**
     * react to the user tapping the back/up icon in the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    private void setTextAppearance(TextView textView, int resId) {

        if (Build.VERSION.SDK_INT < 23) {
            textView.setTextAppearance(getContext(), resId);
        } else {
            textView.setTextAppearance(resId);
        }
    }


    private Button createCell() {
        Button cell = new Button(getContext());
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT);
        setTextAppearance(cell, android.R.style.TextAppearance_Medium);
        cell.setLayoutParams(layoutParams);
        int horizontal_margin = (int) getResources().getDimension(R.dimen.text_margin);
        int vertical_margin = (int) getResources().getDimension(R.dimen.activity_vertical_margin);
        cell.setPadding(horizontal_margin, vertical_margin, horizontal_margin, vertical_margin);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cell.setBackground(getDrawable(R.drawable.black_border));
                return cell;
            }
            cell.setBackground(getResources().getDrawable(R.drawable.black_border));
        } else {
            cell.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border));
        }
        return cell;
    }


    private void loadStatistics() {
        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.Statistics(fromDate, toDate, token, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    SimpleDateFormat outputFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                    Date fromDate = format.parse(result.getString("FromDate"));
                    Date toDate = format.parse(result.getString("ToDate"));
                    from_date_button.setText(outputFormat.format(fromDate));
                    to_date_button.setText(outputFormat.format(toDate));

                    orders_table.removeAllViewsInLayout();
                    orders = result.getJSONArray("Result");

                    DisplayMetrics displaymetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                    Button cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8;
                    // Header
                    TableRow header = new TableRow(getContext());
                    header.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                    cell1 = createCell();
                    cell1.setWidth((int)(displaymetrics.widthPixels * 0.5));
                    cell1.setText(getResources().getString(R.string.status_header));
                    header.addView(cell1);
                    cell2 = createCell();
                    cell2.setText(getResources().getString(R.string.monday));
                    header.addView(cell2);
                    cell3 = createCell();
                    cell3.setText(getResources().getString(R.string.tuesday));
                    header.addView(cell3);
                    cell4 = createCell();
                    cell4.setText(getResources().getString(R.string.wednesday));
                    header.addView(cell4);
                    cell5 = createCell();
                    cell5.setText(getResources().getString(R.string.thursday));
                    header.addView(cell5);
                    cell6 = createCell();
                    cell6.setText(getResources().getString(R.string.friday));
                    header.addView(cell6);
                    cell7 = createCell();
                    cell7.setText(getResources().getString(R.string.saturday));
                    header.addView(cell7);
                    cell8 = createCell();
                    cell8.setText(getResources().getString(R.string.total));
                    header.addView(cell8);
                    orders_table.addView(header, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                    for (int i = 0; i < orders.length(); i++) {
                        JSONObject order = orders.getJSONObject(i);
                        int status = order.getInt("Status");
                        JSONArray orderCount = order.getJSONArray("Orders");

                        TableRow row = new TableRow(getContext());
                        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));
                        cell1 = createCell();
                        switch (status) {
                            case 0:
                                cell1.setText(getResources().getString(R.string.order_is_submitted));
                                break;
                            case 1:
                                cell1.setText(getResources().getString(R.string.order_is_confirmed));
                                break;
                            case 2:
                                cell1.setText(getResources().getString(R.string.order_is_rejected));
                                break;
                            case 3:
                                cell1.setText(getResources().getString(R.string.item_is_collected));
                                break;
                            case 4:
                                cell1.setText(getResources().getString(R.string.pickup_failed));
                                break;
                            case 5:
                                cell1.setText(getResources().getString(R.string.item_in_warehouse));
                                break;
                            case 6:
                                cell1.setText(getResources().getString(R.string.delivery_in_progress));
                                break;
                            case 7:
                                cell1.setText(getResources().getString(R.string.item_is_delivered));
                                break;
                            case 8:
                                cell1.setText(getResources().getString(R.string.order_is_canceled));
                                break;
                            case 9:
                                cell1.setText(getResources().getString(R.string.delivery_failed));
                                break;
                            case 10:
                                cell1.setText(getResources().getString(R.string.special_pickup));
                                break;
                            case 11:
                                cell1.setText(getResources().getString(R.string.special_delivery));
                                break;
                            case 12:
                                cell1.setText(getResources().getString(R.string.enroute_to_pickup));
                                break;
                            case 13:
                                cell1.setText(getResources().getString(R.string.repickup));
                                break;
                            case 14:
                                cell1.setText(getResources().getString(R.string.redelivery));
                                break;
                        }
                        cell1.setWidth((int)(displaymetrics.widthPixels * 0.5));
                        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
                        cell1.setLayoutParams(layoutParams);
                        row.addView(cell1);
                        cell2 = createCell();
                        row.addView(cell2);
                        cell3 = createCell();
                        row.addView(cell3);
                        cell4 = createCell();
                        row.addView(cell4);
                        cell5 = createCell();
                        row.addView(cell5);
                        cell6 = createCell();
                        row.addView(cell6);
                        cell7 = createCell();
                        row.addView(cell7);

                        int total = 0;
                        SimpleDateFormat formatNoTZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Calendar calendar = Calendar.getInstance();
                        for (int j = 0; j < orderCount.length(); j++) {
                            total += orderCount.getJSONObject(j).getInt("Count");
                            Date date = formatNoTZ.parse(orderCount.getJSONObject(j).getString("Date"));
                            calendar.setTime(date);
                            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                            if (dayOfWeek == Calendar.MONDAY) {
                                cell2.setText(String.valueOf(orderCount.getJSONObject(j).getInt("Count")));
                            }
                            if (dayOfWeek == Calendar.TUESDAY) {
                                cell3.setText(String.valueOf(orderCount.getJSONObject(j).getInt("Count")));
                            }
                            if (dayOfWeek == Calendar.WEDNESDAY) {
                                cell4.setText(String.valueOf(orderCount.getJSONObject(j).getInt("Count")));
                            }
                            if (dayOfWeek == Calendar.THURSDAY) {
                                cell5.setText(String.valueOf(orderCount.getJSONObject(j).getInt("Count")));
                            }
                            if (dayOfWeek == Calendar.FRIDAY) {
                                cell6.setText(String.valueOf(orderCount.getJSONObject(j).getInt("Count")));
                            }
                            if (dayOfWeek == Calendar.SATURDAY) {
                                cell7.setText(String.valueOf(orderCount.getJSONObject(j).getInt("Count")));
                            }
                        }
                        cell8 = createCell();
                        cell8.setText(String.valueOf(total));
                        row.addView(cell8);

                        orders_table.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_unknown_error))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
