package com.roadbull.inventory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by nhatp on 6/9/2016.
 */
public class SelectListItemsAdapter extends ArrayAdapter<JSONObject> {
    private final Context context;

    public SelectListItemsAdapter(Context context) {
        super(context, R.layout.select_list_item);
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.select_list_item, parent, false);

        TextView text = (TextView) rowView.findViewById(R.id.text_content);
        text.setText(this.getItem(position).optString("Text"));

        return rowView;
    }


    @Override
    public View getDropDownView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.select_list_item, parent, false);

        TextView text = (TextView) rowView.findViewById(R.id.text_content);
        text.setText(this.getItem(position).optString("Text"));

        return rowView;
    }
}
