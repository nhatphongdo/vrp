package com.roadbull.inventory;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class ApiServices {
    interface ServiceCallbackInterface {

        void onSuccess(int statusCode, Object response) throws JSONException;

        void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error);

    }

    public final static String BaseUrl = "http://cds.roadbull.com/";
    public final static String LoginServiceUrl = BaseUrl + "api/accounts/login";
    public final static String SettingsServiceUrl = BaseUrl + "api/inventory/settings";
    public final static String CollectedItemsServiceUrl = BaseUrl + "api/inventory/collected";
    public final static String PutItemIntoWarehouseServiceUrl = BaseUrl + "api/inventory/putinto";
    public final static String OrderDetailServiceUrl = BaseUrl + "api/inventory/order";
    public final static String OrderChangeRequestServiceUrl = BaseUrl + "api/inventory/changerequest";
    public final static String WarehouseServiceUrl = BaseUrl + "api/inventory/warehouse";
    public final static String FailedDeliveriesServiceUrl = BaseUrl + "api/inventory/faildeliveries";
    public final static String PutFailedItemIntoWarehouseServiceUrl = BaseUrl + "api/inventory/putfailedinto";
    public final static String SendFailedServiceUrl = BaseUrl + "api/inventory/sendfailed";
    public final static String StatisticsServiceUrl = BaseUrl + "api/inventory/statistics";
    public final static String VerifyJobServiceUrl = BaseUrl + "api/inventory/job";
    public final static String UploadSignatureServiceUrl = BaseUrl + "api/inventory/uploadSignature";
    public final static String SendAcknowledgeServiceUrl = BaseUrl + "api/inventory/acknowledge";
    public final static String DispatchInfoServiceUrl = BaseUrl + "api/inventory/dispatchinfo";


    protected static void InvokeWebservice(String serviceUrl, String token, RequestParams params, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }
        client.setTimeout(60000);
        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void UploadFile(String serviceUrl, String token, String filePath, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }

        File file = new File(filePath);
        RequestParams params = new RequestParams();
        try {
            params.put("uploadFile", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onFailure(500, null, null, e);
            }
        }

        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void UploadFile(String serviceUrl, String token, InputStream input, String filename, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }

        RequestParams params = new RequestParams();
        params.put("uploadFile", input, filename);

        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void Login(String username, String password, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("UserName", username);
        params.add("Password", password);
        InvokeWebservice(LoginServiceUrl, null, params, callback);
    }

    public static void CollectedItems(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(CollectedItemsServiceUrl, token, null, callback);
    }

    public static void PutInto(String token, String consignmentNumber, ServiceCallbackInterface callback) {
        InvokeWebservice(PutItemIntoWarehouseServiceUrl + "/" + consignmentNumber, token, null, callback);
    }

    public static void OrderDetail(String token, String consignmentNumber, ServiceCallbackInterface callback) {
        InvokeWebservice(OrderDetailServiceUrl + "/" + consignmentNumber, token, null, callback);
    }

    public static void OrderChangeRequest(String token, String consignmentNumber, String binCode, int sizeId,
                                          String fromName, String fromPostalCode, String fromAddress, String fromMobile,
                                          String toName, String toPostalCode, String toAddress, String toMobile,
                                          Date pickupDate, int pickupTimeSlotId, Date deliveryDate, int deliveryTimeSlotId,
                                          String remark, int status, ServiceCallbackInterface callback) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        RequestParams params = new RequestParams();
        params.add("ConsignmentNumber", consignmentNumber);
        params.add("BinCode", binCode);
        params.add("FromName", fromName);
        params.add("FromZipCode", fromPostalCode);
        params.add("FromAddress", fromAddress);
        params.add("FromMobilePhone", fromMobile);
        params.add("ToName", toName);
        params.add("ToZipCode", toPostalCode);
        params.add("ToAddress", toAddress);
        params.add("ToMobilePhone", toMobile);
        params.add("SizeId", String.valueOf(sizeId));
        params.add("PickupDate", dateFormat.format(pickupDate));
        params.add("PickupTimeSlotId", String.valueOf(pickupTimeSlotId));
        params.add("DeliveryDate", dateFormat.format(deliveryDate));
        params.add("DeliveryTimeSlotId", String.valueOf(deliveryTimeSlotId));
        params.add("Remark", remark);
        params.add("Status", String.valueOf(status));

        InvokeWebservice(OrderChangeRequestServiceUrl, token, params, callback);
    }

    public static void Warehouse(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(WarehouseServiceUrl, token, null, callback);
    }

    public static void FailedDeliveries(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(FailedDeliveriesServiceUrl, token, null, callback);
    }

    public static void PutFailedInto(String token, String consignmentNumber, ServiceCallbackInterface callback) {
        InvokeWebservice(PutFailedItemIntoWarehouseServiceUrl + "/" + consignmentNumber, token, null, callback);
    }

    public static void SendFailedEmail(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(SendFailedServiceUrl, token, null, callback);
    }

    public static void Statistics(Date fromDate, Date toDate, String token, ServiceCallbackInterface callback) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        InvokeWebservice(StatisticsServiceUrl + "/" + (fromDate == null ? "0" : dateFormat.format(fromDate))
                + "/" + (toDate == null ? "0" : dateFormat.format(toDate)), token, null, callback);
    }

    public static void VerifyJob(String job, String token, ServiceCallbackInterface callback) {
        InvokeWebservice(VerifyJobServiceUrl + "/" + job, token, null, callback);
    }

    public static void SendAcknowledge(String info, String consignmentNumbers, String token, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("Info", info);
        params.add("ConsignmentNumbers", consignmentNumbers);
        InvokeWebservice(SendAcknowledgeServiceUrl, token, params, callback);
    }

    public static void DispatchInfo(String consignmentNumber, String token, ServiceCallbackInterface callback) {
        InvokeWebservice(DispatchInfoServiceUrl + "/" + consignmentNumber, token, null, callback);
    }
}
