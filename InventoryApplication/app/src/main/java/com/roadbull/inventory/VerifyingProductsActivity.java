package com.roadbull.inventory;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class VerifyingProductsActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 42;

    private ArrayList<JSONObject> itemList;
    private JobItemsAdapter listAdapter;
    private EditText jobId;
    private TextView info;
    private ListView listView;
    private Button acknowledge;
    private Button scanQr;
    private ProgressDialog dialog;
    private int scannedCount;
    private int deliveries;

    private ScannerDevice scanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifying_products);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        itemList = new ArrayList<>();
        listAdapter = new JobItemsAdapter(getContext(), itemList);

        jobId = (EditText) findViewById(R.id.job_id);
        jobId.setOnFocusChangeListener(this);
        jobId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loadJob();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
        info = (TextView) findViewById(R.id.count_info);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(listAdapter);
        scanQr = (Button) findViewById(R.id.scanQr);
        scanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check Android 6 permission
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                } else {
                    ActivityCompat.requestPermissions(getContext(),
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });
        acknowledge = (Button) findViewById(R.id.acknowledge);
        acknowledge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent newIntent = new Intent(getContext(), DriverSignActivity.class);
                    String numbers = "";
                    for (int i = 0; i < listAdapter.getCount(); i++) {
                        if (listAdapter.getItem(i).optBoolean("Proceeded", false) == true) {
                            numbers += listAdapter.getItem(i).getString("ConsignmentNumber") + ",";
                        }
                    }
                    if (!numbers.isEmpty()) {
                        newIntent.putExtra("ConsignmentNumbers", numbers);
                        startActivity(newIntent);
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        });

        scanner = new ScannerDevice(getContext(), new ScannerDevice.ScannerCallbackInterface() {
            @Override
            public void onSuccess(String data) {
                String[] parts = data == null ? new String[]{} : data.split("\\|");
                if (parts.length > 1) {
                    // Grey out this Consignment Number
                    String consignmentNumber = parts[1];
                    for (int i = 0; i < listAdapter.getCount(); i++) {
                        if (listAdapter.getItem(i).optString("ConsignmentNumber").equalsIgnoreCase(consignmentNumber)) {
                            if (listAdapter.getItem(i).optBoolean("Proceeded", false) == false) {
                                ++scannedCount;
                                info.setText(String.valueOf(scannedCount) + "/" + String.valueOf(listAdapter.getCount()) +
                                        " (" + String.valueOf(deliveries) + " deliveries, " + String.valueOf(listAdapter.getCount() - deliveries) + " picks up)");
                            }
                            try {
                                listAdapter.getItem(i).put("Proceeded", true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    listAdapter.notifyDataSetChanged();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (scanner != null) {
            scanner.destroy();
        }
    }


    public VerifyingProductsActivity getContext() {
        return this;
    }


    /**
     * react to the user tapping the back/up icon in the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.getId() == R.id.job_id && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle scan result
            String[] parts = scanResult.getContents() == null ? new String[]{} : scanResult.getContents().split("\\|");
            if (parts.length > 1) {
                // Grey out this Consignment Number
                String consignmentNumber = parts[1];
                for (int i = 0; i < listAdapter.getCount(); i++) {
                    if (listAdapter.getItem(i).optString("ConsignmentNumber").equalsIgnoreCase(consignmentNumber)) {
                        if (listAdapter.getItem(i).optBoolean("Proceeded", false) == false) {
                            ++scannedCount;
                            info.setText(String.valueOf(scannedCount) + "/" + String.valueOf(listAdapter.getCount()) +
                                    " (" + String.valueOf(deliveries) + " deliveries, " + String.valueOf(listAdapter.getCount() - deliveries) + " picks up)");
                        }
                        try {
                            listAdapter.getItem(i).put("Proceeded", true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                listAdapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.CAMERA)
                        && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                }
            }
        }
    }


    private void loadJob() {

        // Validate
        if (jobId.getText().toString().isEmpty()) {
            jobId.setError(getResources().getString(R.string.error_job_id));
            return;
        }

        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        scannedCount = 0;
        deliveries = 0;
        listAdapter.clear();
        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.VerifyJob(jobId.getText().toString(), token, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    JSONArray orders = result.getJSONArray("Result");

                    for (int i = 0; i < orders.length(); i++) {
                        listAdapter.add(orders.getJSONObject(i));
                        if (orders.getJSONObject(i).getInt("Type") == 1) {
                            ++deliveries;
                        }
                    }

                    info.setText(String.valueOf(scannedCount) + "/" + String.valueOf(orders.length()) +
                            " (" + String.valueOf(deliveries) + " deliveries, " + String.valueOf(orders.length() - deliveries) + " picks up)");

                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_unknown_error))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
