package com.roadbull.inventory;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private CheckBox mRememberMe;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.username);
        mEmailView.setOnFocusChangeListener(this);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mPasswordView.setOnFocusChangeListener(this);

        mRememberMe = (CheckBox) findViewById(R.id.remember);

        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        // Try to login with remember
        boolean isRemember = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.REMEMBER_ACCOUNT, false);
        if (isRemember == true) {
            mRememberMe.setChecked(isRemember);
            mEmailView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_USERNAME_KEY, ""));
            mPasswordView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_PASSWORD_KEY, ""));
            attemptLogin();
        }
    }


    public LoginActivity getContext() {
        return this;
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String userName = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(userName)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            ApiServices.Login(userName, password, new ApiServices.ServiceCallbackInterface() {
                @Override
                public void onSuccess(int statusCode, Object response) {
                    try {
                        final JSONObject loginResult = (JSONObject) response;

                        if (loginResult.getInt("Code") != 0) {
                            // Error
                            showProgress(false);
                            mPasswordView.setError(loginResult.getString("Message").replaceAll(" \\| ", "\n"));
                            mPasswordView.requestFocus();
                        } else {
                            boolean validRole = false;
                            JSONArray roles = loginResult.optJSONArray("Roles");
                            for (int i = 0; i < roles.length(); i++) {
                                if (roles.optString(i).equalsIgnoreCase("Manager")) {
                                    validRole = true;
                                    break;
                                }
                            }

                            if (!validRole) {
                                showProgress(false);
                                mEmailView.setError(getString(R.string.error_invalid_permission));
                                mEmailView.requestFocus();
                                return;
                            }

                            // Save information
                            if (mRememberMe.isChecked() == true) {
                                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_PASSWORD_KEY, mPasswordView.getText().toString());
                                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.REMEMBER_ACCOUNT, true);
                            } else {
                                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_PASSWORD_KEY);
                                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.REMEMBER_ACCOUNT, false);
                            }

                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.TOKEN_KEY, loginResult.getString("Token"));
                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_USERNAME_KEY, mEmailView.getText().toString());

                            showProgress(false);
                            finish();
                            startActivity(new Intent(getContext(), MainActivity.class));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showProgress(false);
                        mPasswordView.setError(getString(R.string.error_unknown_error));
                        mPasswordView.requestFocus();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    error.printStackTrace();
                    showProgress(false);
                    mPasswordView.setError(getString(R.string.error_network_problem));
                    mPasswordView.requestFocus();
                }
            });
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v.getId() == R.id.username || v.getId() == R.id.password) && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        }
    }
}

