package com.roadbull.inventory;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.gcacace.signaturepad.views.SignaturePad;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Driver;

import cz.msebera.android.httpclient.Header;

public class DriverSignActivity extends AppCompatActivity {

    private SignaturePad signaturePad;
    private Bitmap signature;
    private EditText name;
    private Button submitButton;
    private ProgressDialog dialog;
    private String ordersString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_sign);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ordersString = getIntent().getStringExtra("ConsignmentNumbers");

        signaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onSigned() {
                // Event triggered when the pad is signed
                signature = signaturePad.getTransparentSignatureBitmap();
            }

            @Override
            public void onClear() {
                // Event triggered when the pad is cleared
                signature = null;
            }
        });
        name = (EditText) findViewById(R.id.full_name);
        submitButton = (Button) findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }


    public DriverSignActivity getContext() {
        return this;
    }


    /**
     * react to the user tapping the back/up icon in the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    public void submit() {
        if (signature == null) {
            // No proof
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getResources().getString(R.string.invalid_signature))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        if (name.getText().toString().isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getResources().getString(R.string.invalid_full_name))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        signature.compress(Bitmap.CompressFormat.PNG, 80, bao);
        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");
        ByteArrayInputStream bai = new ByteArrayInputStream(bao.toByteArray());
        ApiServices.UploadFile(ApiServices.UploadSignatureServiceUrl, token, bai, "sign.png", new ApiServices.ServiceCallbackInterface() {

            @Override
            public void onSuccess(int statusCode, Object response) {
                try {
                    JSONObject result = (JSONObject) response;
                    if (result.getInt("Code") != 0) {
                        dialog.hide();

                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        return;
                    }

                    ApiServices.SendAcknowledge(result.getString("Path") + "|" + name.getText(), ordersString, token, new ApiServices.ServiceCallbackInterface() {
                        @Override
                        public void onSuccess(int statusCode, Object response) {
                            dialog.hide();

                            JSONObject result = (JSONObject) response;
                            try {
                                if (result.getInt("Code") != 0) {
                                    new AlertDialog.Builder(getContext())
                                            .setTitle(getContext().getTitle())
                                            .setMessage(result.getString("Message"))
                                            .setPositiveButton(android.R.string.ok, null)
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                    return;
                                }

                                new AlertDialog.Builder(getContext())
                                        .setTitle(getContext().getTitle())
                                        .setMessage(getResources().getString(R.string.submit_successful))
                                        .setPositiveButton(android.R.string.ok, null)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            error.printStackTrace();
                            dialog.hide();
                            new AlertDialog.Builder(getContext())
                                    .setTitle(getContext().getTitle())
                                    .setMessage(getString(R.string.error_network_problem))
                                    .setPositiveButton(android.R.string.ok, null)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    });
                } catch (Exception exc) {
                    exc.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_network_problem))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
