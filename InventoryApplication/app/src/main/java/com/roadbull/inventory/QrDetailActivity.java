package com.roadbull.inventory;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.cipherlab.barcode.GeneralString;
import com.cipherlab.barcode.ReaderManager;
import com.cipherlab.barcode.decoder.BcReaderType;
import com.cipherlab.barcode.decoder.KeyboardEmulationType;
import com.cipherlab.barcode.decoderparams.ReaderOutputConfiguration;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import hirondelle.date4j.DateTime;

public class QrDetailActivity extends AppCompatActivity implements View.OnFocusChangeListener,
        AdapterView.OnItemSelectedListener {

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 42;

    private Button scanQr;
    private EditText consignmentNumber;
    private Spinner sizeSpinner;
    private TextView binCode;
    private EditText fromName;
    private EditText fromPostalCode;
    private EditText fromAddress;
    private EditText fromMobile;
    private EditText toName;
    private EditText toPostalCode;
    private EditText toAddress;
    private EditText toMobile;
    private Button pickUpDateButton;
    private Spinner pickUpTime;
    private Button deliveryDateButton;
    private Spinner deliveryTime;
    private EditText remark;
    private Spinner statusSpinner;
    private Button sendEmail;
    private ProgressDialog dialog;
    private int status;
    private int sizeId;
    private Date pickupDate;
    private int pickupTimeslotId;
    private Date deliveryDate;
    private int deliveryTimeslotId;
    private JSONArray sizes;
    private JSONObject timeOptions;
    private JSONArray statuses;
    private String productTypeBinCode;

    private SelectListItemsAdapter sizesAdapter;
    private SelectListItemsAdapter pickupTimesAdapter;
    private SelectListItemsAdapter deliveryTimesAdapter;
    private SelectListItemsAdapter statusesAdapter;

    private ScannerDevice scanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        scanQr = (Button) findViewById(R.id.scan_qr);
        scanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check Android 6 permission
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                } else {
                    ActivityCompat.requestPermissions(getContext(),
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });

        consignmentNumber = (EditText) findViewById(R.id.consignment_number);
        consignmentNumber.setOnFocusChangeListener(this);
        consignmentNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE && !consignmentNumber.getText().toString().isEmpty()) {
                    loadOrder(consignmentNumber.getText().toString());
                    return true;
                }
                return false;
            }
        });
        sizeSpinner = (Spinner) findViewById(R.id.size);
        sizeSpinner.setOnFocusChangeListener(this);
        sizeSpinner.setOnItemSelectedListener(this);
        sizesAdapter = new SelectListItemsAdapter(getContext());
        sizeSpinner.setAdapter(sizesAdapter);
        binCode = (TextView) findViewById(R.id.bin_code);
        fromName = (EditText) findViewById(R.id.from_name);
        fromName.setOnFocusChangeListener(this);
        fromPostalCode = (EditText) findViewById(R.id.from_postal_code);
        fromPostalCode.setOnFocusChangeListener(this);
        fromAddress = (EditText) findViewById(R.id.from_address);
        fromAddress.setOnFocusChangeListener(this);
        fromMobile = (EditText) findViewById(R.id.from_mobile);
        fromMobile.setOnFocusChangeListener(this);
        toName = (EditText) findViewById(R.id.to_name);
        toName.setOnFocusChangeListener(this);
        toPostalCode = (EditText) findViewById(R.id.to_postal_code);
        toPostalCode.setOnFocusChangeListener(this);
        toAddress = (EditText) findViewById(R.id.to_address);
        toAddress.setOnFocusChangeListener(this);
        toMobile = (EditText) findViewById(R.id.to_mobile);
        toMobile.setOnFocusChangeListener(this);
        pickUpDateButton = (Button) findViewById(R.id.pick_up_date);
        pickUpDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                final CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", now.get(Calendar.MONTH) + 1, now.get(Calendar.YEAR));

                Calendar minPickupCalendar = Calendar.getInstance();
                minPickupCalendar.set(Calendar.MILLISECOND, 0);
                minPickupCalendar.set(Calendar.SECOND, 0);
                minPickupCalendar.set(Calendar.MINUTE, 0);
                minPickupCalendar.set(Calendar.HOUR_OF_DAY, 0);
                dialogCaldroidFragment.setMinDate(minPickupCalendar.getTime());
                if (pickupDate != null) {
                    dialogCaldroidFragment.setSelectedDate(pickupDate);
                }
                // dialogCaldroidFragment.setDisableDates(GetDisabledDates(minPickupDate, maxPickupDate));

                // Add a listener as per documentation
                dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

                    @Override
                    public void onSelectDate(Date date, View view) {
                        // You can perform your method on selected date
                        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                        pickupDate = date;
                        pickUpDateButton.setText(dateFormat.format(date));
                        dialogCaldroidFragment.dismiss();
                    }
                });

                // show the dialog
                dialogCaldroidFragment.show(getContext().getSupportFragmentManager(), "TAG");
            }
        });
        pickUpTime = (Spinner) findViewById(R.id.pick_up_time);
        pickUpTime.setOnFocusChangeListener(this);
        pickUpTime.setOnItemSelectedListener(this);
        pickupTimesAdapter = new SelectListItemsAdapter(getContext());
        pickUpTime.setAdapter(pickupTimesAdapter);
        deliveryDateButton = (Button) findViewById(R.id.delivery_date);
        deliveryDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                final CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", now.get(Calendar.MONTH) + 1, now.get(Calendar.YEAR));

                Calendar minPickupCalendar = Calendar.getInstance();
                minPickupCalendar.set(Calendar.MILLISECOND, 0);
                minPickupCalendar.set(Calendar.SECOND, 0);
                minPickupCalendar.set(Calendar.MINUTE, 0);
                minPickupCalendar.set(Calendar.HOUR_OF_DAY, 0);
                dialogCaldroidFragment.setMinDate(minPickupCalendar.getTime());
                if (deliveryDate != null) {
                    dialogCaldroidFragment.setSelectedDate(deliveryDate);
                }
                // dialogCaldroidFragment.setDisableDates(GetDisabledDates(minPickupDate, maxPickupDate));

                // Add a listener as per documentation
                dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

                    @Override
                    public void onSelectDate(Date date, View view) {
                        // You can perform your method on selected date
                        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                        deliveryDate = date;
                        deliveryDateButton.setText(dateFormat.format(date));
                        dialogCaldroidFragment.dismiss();

                        // Change Bin Code
                        if (deliveryTimesAdapter != null) {
                            for (int i = 0; i < deliveryTimesAdapter.getCount(); i++) {
                                if (deliveryTimesAdapter.getItem(i).optInt("Value") != deliveryTimeslotId) {
                                    continue;
                                }

                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(deliveryDate);
                                binCode.setText(String.valueOf(calendar.get(Calendar.DAY_OF_WEEK) - 1) +
                                        deliveryTimesAdapter.getItem(i).optString("BinCode", "") +
                                        (productTypeBinCode == "" ? "-" + productTypeBinCode : ""));
                            }
                        }
                    }
                });

                // show the dialog
                dialogCaldroidFragment.show(getContext().getSupportFragmentManager(), "TAG");
            }
        });
        deliveryTime = (Spinner) findViewById(R.id.delivery_time);
        deliveryTime.setOnFocusChangeListener(this);
        deliveryTime.setOnItemSelectedListener(this);
        deliveryTimesAdapter = new SelectListItemsAdapter(getContext());
        deliveryTime.setAdapter(deliveryTimesAdapter);
        remark = (EditText) findViewById(R.id.remark);
        remark.setOnFocusChangeListener(this);
        statusSpinner = (Spinner) findViewById(R.id.status);
        statusSpinner.setOnFocusChangeListener(this);
        statusSpinner.setOnItemSelectedListener(this);
        statusesAdapter = new SelectListItemsAdapter(getContext());
        statusSpinner.setAdapter(statusesAdapter);
        sendEmail = (Button) findViewById(R.id.send_email);
        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });

        scanner = new ScannerDevice(getContext(), new ScannerDevice.ScannerCallbackInterface() {
            @Override
            public void onSuccess(String data) {
                String[] parts = data == null ? new String[]{} : data.split("\\|");
                if (parts.length > 1) {
                    // Grey out this Consignment Number
                    String consignmentNumberStr = parts[1];
                    loadOrder(consignmentNumberStr);
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (scanner != null) {
            scanner.destroy();
        }
    }


    /**
     * react to the user tapping the back/up icon in the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    public QrDetailActivity getContext() {
        return this;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle scan result
            String[] parts = scanResult.getContents() == null ? new String[]{} : scanResult.getContents().split("\\|");
            if (parts.length > 1) {
                // Grey out this Consignment Number
                String consignmentNumberStr = parts[1];
                loadOrder(consignmentNumberStr);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.CAMERA)
                        && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getContext());
                    integrator.initiateScan();
                }
            }
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v.getId() == R.id.size || v.getId() == R.id.from_name || v.getId() == R.id.from_postal_code
                || v.getId() == R.id.from_address || v.getId() == R.id.from_mobile
                || v.getId() == R.id.to_name || v.getId() == R.id.to_postal_code
                || v.getId() == R.id.to_address || v.getId() == R.id.to_mobile
                || v.getId() == R.id.pick_up_time || v.getId() == R.id.delivery_time
                || v.getId() == R.id.remark || v.getId() == R.id.status
                || v.getId() == R.id.consignment_number) && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }


    private void loadOrder(final String consignment_number) {
        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.OrderDetail(token, consignment_number, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    JSONObject order = result.getJSONObject("Order");
                    consignmentNumber.setText(order.getString("ConsignmentNumber"));
                    binCode.setText(order.getString("BinCode"));
                    fromAddress.setText(order.getString("FromAddress"));
                    fromMobile.setText(order.getString("FromMobilePhone"));
                    fromName.setText(order.getString("FromName"));
                    fromPostalCode.setText(order.getString("FromZipCode"));
                    toAddress.setText(order.getString("ToAddress"));
                    toMobile.setText(order.getString("ToMobilePhone"));
                    toName.setText(order.getString("ToName"));
                    toPostalCode.setText(order.getString("ToZipCode"));
                    if (order.isNull("Remark")) {
                        remark.setText("");
                    } else {
                        remark.setText(order.getString("Remark"));
                    }
                    productTypeBinCode = order.getString("ProductTypeBinCode");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    SimpleDateFormat outputFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                    status = order.getInt("Status");
                    sizeId = order.getInt("SizeId");
                    if (order.isNull("PickupDate")) {
                        pickupDate = null;
                        pickUpDateButton.setText(getString(R.string.empty_date));
                    } else {
                        pickupDate = dateFormat.parse(order.getString("PickupDate"));
                        pickUpDateButton.setText(outputFormat.format(pickupDate));
                    }
                    if (order.isNull("PickupTimeSlotId")) {
                        pickupTimeslotId = 0;
                    } else {
                        pickupTimeslotId = order.getInt("PickupTimeSlotId");
                    }
                    if (order.isNull("DeliveryDate")) {
                        deliveryDate = null;
                        deliveryDateButton.setText(getString(R.string.empty_date));
                    } else {
                        deliveryDate = dateFormat.parse(order.getString("DeliveryDate"));
                        deliveryDateButton.setText(outputFormat.format(deliveryDate));
                    }
                    if (order.isNull("DeliveryTimeSlotId")) {
                        deliveryTimeslotId = 0;
                    } else {
                        deliveryTimeslotId = order.getInt("DeliveryTimeSlotId");
                    }

                    sizes = result.getJSONArray("Sizes");
                    sizesAdapter.clear();
                    int selectedIndex = 0;
                    for (int i = 0; i < sizes.length(); i++) {
                        JSONObject item = new JSONObject();
                        item.put("Value", sizes.getJSONObject(i).getInt("Id"));
                        item.put("Text", sizes.getJSONObject(i).getString("SizeName"));
                        sizesAdapter.add(item);
                        if (sizes.getJSONObject(i).getInt("Id") == sizeId) {
                            selectedIndex = i;
                        }
                    }
                    sizesAdapter.notifyDataSetChanged();
                    sizeSpinner.setSelection(selectedIndex);

                    timeOptions = result.getJSONObject("TimeOptions");

                    JSONArray pickupTimeslots = timeOptions.getJSONArray("AvailablePickupTimes");
                    pickupTimesAdapter.clear();
                    selectedIndex = 0;
                    for (int i = 0; i < pickupTimeslots.length(); i++) {
                        JSONObject item = new JSONObject();
                        item.put("Value", pickupTimeslots.getJSONObject(i).getInt("Value"));
                        item.put("Text", pickupTimeslots.getJSONObject(i).getString("Text"));
                        item.put("BinCode", pickupTimeslots.getJSONObject(i).getString("BinCode"));
                        pickupTimesAdapter.add(item);
                        if (pickupTimeslots.getJSONObject(i).getInt("Value") == pickupTimeslotId) {
                            selectedIndex = i;
                        }
                    }
                    pickupTimesAdapter.notifyDataSetChanged();
                    pickUpTime.setSelection(selectedIndex);

                    JSONArray deliveryTimeslots = timeOptions.getJSONArray("AvailableDeliveryTimes");
                    deliveryTimesAdapter.clear();
                    selectedIndex = 0;
                    for (int i = 0; i < deliveryTimeslots.length(); i++) {
                        JSONObject item = new JSONObject();
                        item.put("Value", deliveryTimeslots.getJSONObject(i).getInt("Value"));
                        item.put("Text", deliveryTimeslots.getJSONObject(i).getString("Text"));
                        item.put("BinCode", deliveryTimeslots.getJSONObject(i).getString("BinCode"));
                        deliveryTimesAdapter.add(item);
                        if (deliveryTimeslots.getJSONObject(i).getInt("Value") == deliveryTimeslotId) {
                            selectedIndex = i;
                        }
                    }
                    deliveryTimesAdapter.notifyDataSetChanged();
                    deliveryTime.setSelection(selectedIndex);

                    statuses = result.getJSONArray("Statuses");
                    statusesAdapter.clear();
                    selectedIndex = 0;
                    for (int i = 0; i < statuses.length(); i++) {
                        JSONObject item = new JSONObject();
                        item.put("Value", statuses.getJSONObject(i).getInt("Value"));
                        String statusText = statuses.getJSONObject(i).getString("Text");
                        switch (statuses.getJSONObject(i).getInt("Value")) {
                            case 0:
                                statusText = getString(R.string.order_is_submitted);
                                break;
                            case 1:
                                statusText = getString(R.string.order_is_confirmed);
                                break;
                            case 2:
                                statusText = getString(R.string.order_is_rejected);
                                break;
                            case 3:
                                statusText = getString(R.string.item_is_collected);
                                break;
                            case 4:
                                statusText = getString(R.string.pickup_failed);
                                break;
                            case 5:
                                statusText = getString(R.string.item_in_warehouse);
                                break;
                            case 6:
                                statusText = getString(R.string.delivery_in_progress) + ", "
                                        + getString(R.string.dispatched_to_driver) + " " + result.getString("AssignedDriver");
                                break;
                            case 7:
                                statusText = getString(R.string.item_is_delivered);
                                break;
                            case 8:
                                statusText = getString(R.string.order_is_canceled);
                                break;
                            case 9:
                                statusText = getString(R.string.delivery_failed);
                                break;
                            case 10:
                                statusText = getString(R.string.special_pickup);
                                break;
                            case 11:
                                statusText = getString(R.string.special_delivery);
                                break;
                            case 12:
                                statusText = getString(R.string.enroute_to_pickup) + ", "
                                        + getString(R.string.dispatched_to_driver) + " " + result.getString("AssignedDriver");
                                break;
                            case 13:
                                statusText = getString(R.string.repickup);
                                break;
                            case 14:
                                statusText = getString(R.string.redelivery);
                                break;
                        }
                        item.put("Text", statusText);
                        statusesAdapter.add(item);
                        if (statuses.getJSONObject(i).getInt("Value") == status) {
                            selectedIndex = i;
                        }
                    }
                    statusesAdapter.notifyDataSetChanged();
                    statusSpinner.setSelection(selectedIndex);

                    // Hide keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(consignmentNumber.getWindowToken(), 0);

                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_unknown_error))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == sizeSpinner) {
            sizeId = sizesAdapter.getItem(position).optInt("Value", 0);
        } else if (parent == pickUpTime) {
            pickupTimeslotId = pickupTimesAdapter.getItem(position).optInt("Value", 0);
        } else if (parent == deliveryTime) {
            deliveryTimeslotId = deliveryTimesAdapter.getItem(position).optInt("Value", 0);
            // Change Bin Code
            if (deliveryDate != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(deliveryDate);
                binCode.setText(String.valueOf(calendar.get(Calendar.DAY_OF_WEEK) - 1) +
                        deliveryTimesAdapter.getItem(position).optString("BinCode", "") +
                        (productTypeBinCode == "" ? "-" + productTypeBinCode : ""));
            }
        } else if (parent == statusSpinner) {
            status = statusesAdapter.getItem(position).optInt("Value", 0);
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void sendEmail() {
        String _consignmentNumber = consignmentNumber.getText().toString();
        String _binCode = binCode.getText().toString();
        String _fromName = fromName.getText().toString();
        String _fromPostalCode = fromPostalCode.getText().toString();
        String _fromAddress = fromAddress.getText().toString();
        String _fromMobile = fromMobile.getText().toString();
        String _toName = toName.getText().toString();
        String _toPostalCode = toPostalCode.getText().toString();
        String _toAddress = toAddress.getText().toString();
        String _toMobile = toMobile.getText().toString();
        String _remark = remark.getText().toString();

        if (_consignmentNumber.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.not_scanned_qr))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_binCode.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_bin_code))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (sizeId <= 0) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_size))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_fromName.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_from_name))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_fromPostalCode.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_from_postal_code))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_fromAddress.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_from_address))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_fromMobile.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_from_mobile))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_toName.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_to_name))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_toPostalCode.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_to_postal_code))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_toAddress.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_to_address))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (_toMobile.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_to_mobile))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (pickupDate == null) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_pickup_date))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (pickupTimeslotId <= 0) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_pickup_time))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (deliveryDate == null) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_delivery_date))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (deliveryTimeslotId <= 0) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_delivery_time))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        if (status <= 0) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getTitle())
                    .setMessage(getString(R.string.invalid_status))
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

            return;
        }

        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.OrderChangeRequest(token, _consignmentNumber, _binCode, sizeId, _fromName, _fromPostalCode,
                _fromAddress, _fromMobile, _toName, _toPostalCode, _toAddress, _toMobile, pickupDate, pickupTimeslotId,
                deliveryDate, deliveryTimeslotId, _remark, status, new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        dialog.hide();

                        JSONObject result = (JSONObject) response;

                        if (result.getInt("Code") != 0) {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(getContext().getTitle())
                                    .setMessage(result.getString("Message"))
                                    .setPositiveButton(android.R.string.ok, null)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                            return;
                        }

                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(getString(R.string.submit_successful))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        error.printStackTrace();
                        dialog.hide();
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(getString(R.string.error_network_problem))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
    }

}
