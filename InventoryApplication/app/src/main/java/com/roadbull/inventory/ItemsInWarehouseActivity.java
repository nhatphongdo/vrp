package com.roadbull.inventory;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import hirondelle.date4j.DateTime;

public class ItemsInWarehouseActivity extends AppCompatActivity {

    private Button refresh;
    private TextView total_items;
    private TextView total_parcels;
    private TextView total_documents;
    private TextView datetime;
    private TableLayout orders_table;
    private ProgressDialog dialog;
    private JSONArray orders;
    private View.OnClickListener mListener;
    private Handler handler = new Handler();
    private SimpleDateFormat formatDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_in_warehouse);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        refresh = (Button) findViewById(R.id.reload);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadWarehouse();
            }
        });
        total_items = (TextView) findViewById(R.id.total_items);
        total_parcels = (TextView) findViewById(R.id.total_parcels);
        total_documents = (TextView) findViewById(R.id.total_documents);
        orders_table = (TableLayout) findViewById(R.id.orders_table);
        datetime = (TextView) findViewById(R.id.datetime);

        mListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getTag() == null || ((String) v.getTag()).isEmpty() || orders == null) {
                    return;
                }

                // Find orders
                try {
                    for (int i = 0; i < orders.length(); i++) {
                        if (orders.getJSONObject(i).getString("BinCode").equalsIgnoreCase((String) v.getTag())) {
                            Intent newIntent = new Intent(getContext(), BinCodeItemsActivity.class);
                            newIntent.putExtra("BinCode", (String) v.getTag());
                            newIntent.putExtra("Orders", orders.getJSONObject(i).getJSONArray("Orders").toString());
                            startActivity(newIntent);
                        }
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        };

        formatDate = new SimpleDateFormat("'Today is: 'EEEE, dd MMM yyyy, 'Time is: ' HH:mm:ss");
        handler.postDelayed(runnable, 1000);
        loadWarehouse();
    }


    /**
     * react to the user tapping the back/up icon in the action bar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    private ItemsInWarehouseActivity getContext() {
        return this;
    }


    private void setTextAppearance(TextView textView, int resId) {

        if (Build.VERSION.SDK_INT < 23) {
            textView.setTextAppearance(getContext(), resId);
        } else {
            textView.setTextAppearance(resId);
        }
    }


    private Button createCell() {
        Button cell = new Button(getContext());
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        setTextAppearance(cell, android.R.style.TextAppearance_Medium);
        cell.setLayoutParams(layoutParams);
        cell.setOnClickListener(mListener);
        int horizontal_margin = (int) getResources().getDimension(R.dimen.text_margin);
        int vertical_margin = (int) getResources().getDimension(R.dimen.activity_vertical_margin);
        cell.setPadding(horizontal_margin, vertical_margin, horizontal_margin, vertical_margin);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cell.setBackground(getDrawable(R.drawable.black_border));
                return cell;
            }
            cell.setBackground(getResources().getDrawable(R.drawable.black_border));
        } else {
            cell.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border));
        }
        return cell;
    }


    private void setColor(Button cell, int checkDayOfWeek, int dayOfWeek) {
        if (dayOfWeek < checkDayOfWeek) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cell.setBackground(getDrawable(R.drawable.black_border_green));
                }
                cell.setBackground(getResources().getDrawable(R.drawable.black_border_green));
            } else {
                cell.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_green));
            }
        } else if (dayOfWeek == checkDayOfWeek) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cell.setBackground(getDrawable(R.drawable.black_border_yellow));
                }
                cell.setBackground(getResources().getDrawable(R.drawable.black_border_yellow));
            } else {
                cell.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_yellow));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cell.setBackground(getDrawable(R.drawable.black_border_red));
                }
                cell.setBackground(getResources().getDrawable(R.drawable.black_border_red));
            } else {
                cell.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_red));
            }
            cell.setTextColor(Color.WHITE);
        }
    }


    private void loadWarehouse() {
        dialog = ProgressDialog.show(getContext(), "", getString(R.string.message_loading), true);

        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.Warehouse(token, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(getContext().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    total_items.setText(String.valueOf(result.getInt("TotalItems")));
                    total_parcels.setText(String.valueOf(result.getInt("TotalParcels")));
                    total_documents.setText(String.valueOf(result.getInt("TotalDocuments")));

                    orders_table.removeAllViewsInLayout();
                    orders = result.getJSONArray("Orders");

                    Button cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8;
                    // Header
                    TableRow header = new TableRow(getContext());
                    header.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                    cell1 = createCell();
                    cell1.setText("");
                    header.addView(cell1);
                    cell2 = createCell();
                    cell2.setText("1");
                    header.addView(cell2);
                    cell3 = createCell();
                    cell3.setText("2");
                    header.addView(cell3);
                    cell4 = createCell();
                    cell4.setText("3");
                    header.addView(cell4);
                    cell5 = createCell();
                    cell5.setText("4");
                    header.addView(cell5);
                    cell6 = createCell();
                    cell6.setText("5");
                    header.addView(cell6);
                    cell7 = createCell();
                    cell7.setText("6");
                    header.addView(cell7);
                    cell8 = createCell();
                    cell8.setText("7");
                    header.addView(cell8);
                    orders_table.addView(header, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                    HashMap<String, ArrayList<Integer>> counts = new HashMap<>();
                    for (int i = 0; i < orders.length(); i++) {
                        JSONObject order = orders.getJSONObject(i);
                        int index = Integer.parseInt(order.getString("BinCode").substring(0, 1)) - 1;
                        String binCode = order.getString("BinCode").substring(1);

                        if (counts.containsKey(binCode)) {
                            counts.get(binCode).set(index, order.getJSONArray("Orders").length());
                        } else {
                            ArrayList<Integer> list = new ArrayList<>();
                            list.add(0);
                            list.add(0);
                            list.add(0);
                            list.add(0);
                            list.add(0);
                            list.add(0);
                            list.add(0);
                            list.set(index, order.getJSONArray("Orders").length());
                            counts.put(binCode, list);
                        }
                    }

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

                    Object[] keys = counts.keySet().toArray();
                    Arrays.sort(keys);
                    for (int i = 0; i < keys.length; i++) {
                        TableRow row = new TableRow(getContext());
                        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));
                        cell1 = createCell();
                        String keyValue = (String) keys[i];
                        if (keyValue.isEmpty()) {
                            cell1.setText("Fail Bin");
                        } else {
                            cell1.setText(keyValue);
                        }
                        row.addView(cell1);
                        cell2 = createCell();
                        if (counts.get(keyValue).get(0) > 0) {
                            cell2.setText(String.valueOf(counts.get(keyValue).get(0)));
                            cell2.setTag("1" + keyValue);
                            setColor(cell2, Calendar.MONDAY, dayOfWeek);
                        }
                        row.addView(cell2);
                        cell3 = createCell();
                        if (counts.get(keyValue).get(1) > 0) {
                            cell3.setText(String.valueOf(counts.get(keyValue).get(1)));
                            cell3.setTag("2" + keyValue);
                            setColor(cell3, Calendar.TUESDAY, dayOfWeek);
                        }
                        row.addView(cell3);
                        cell4 = createCell();
                        if (counts.get(keyValue).get(2) > 0) {
                            cell4.setText(String.valueOf(counts.get(keyValue).get(2)));
                            cell4.setTag("3" + keyValue);
                            setColor(cell4, Calendar.WEDNESDAY, dayOfWeek);
                        }
                        row.addView(cell4);
                        cell5 = createCell();
                        if (counts.get(keyValue).get(3) > 0) {
                            cell5.setText(String.valueOf(counts.get(keyValue).get(3)));
                            cell5.setTag("4" + keyValue);
                            setColor(cell5, Calendar.THURSDAY, dayOfWeek);
                        }
                        row.addView(cell5);
                        cell6 = createCell();
                        if (counts.get(keyValue).get(4) > 0) {
                            cell6.setText(String.valueOf(counts.get(keyValue).get(4)));
                            cell6.setTag("5" + keyValue);
                            setColor(cell6, Calendar.FRIDAY, dayOfWeek);
                        }
                        row.addView(cell6);
                        cell7 = createCell();
                        if (counts.get(keyValue).get(5) > 0) {
                            cell7.setText(String.valueOf(counts.get(keyValue).get(5)));
                            cell7.setTag("6" + keyValue);
                            setColor(cell7, Calendar.SATURDAY, dayOfWeek);
                        }
                        row.addView(cell7);
                        cell8 = createCell();
                        if (counts.get(keyValue).get(6) > 0) {
                            cell8.setText(String.valueOf(counts.get(keyValue).get(6)));
                            cell8.setTag("7" + keyValue);
                            setColor(cell8, Calendar.SUNDAY, dayOfWeek);
                        }
                        row.addView(cell8);
                        orders_table.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getTitle())
                            .setMessage(getString(R.string.error_unknown_error))
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getContext())
                        .setTitle(getContext().getTitle())
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            datetime.setText(formatDate.format(new Date()));
            handler.postDelayed(this, 1000);
        }
    };
}
