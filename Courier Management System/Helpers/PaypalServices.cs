﻿using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using Braintree;
using PayPal.Api;
using CreditCard = PayPal.Api.CreditCard;
using Transaction = PayPal.Api.Transaction;

namespace Courier_Management_System.Helpers
{
    public class PaypalServices
    {
        public const string PayPalMode = "live"; // Or "live" / "sandbox"

        private static readonly BraintreeGateway Gateway;

        static PaypalServices()
        {
            Gateway = new BraintreeGateway
            {
                Environment = Environment.PRODUCTION,
                MerchantId = SettingProvider.BrainTreeMerchantId,
                PublicKey = SettingProvider.BrainTreePublicKey,
                PrivateKey = SettingProvider.BrainTreePrivateKey
            };
        }

        public static string GetAccessToken()
        {
            var sdkConfig = new Dictionary<string, string> { { "mode", PayPalMode } };
            var accessToken = new OAuthTokenCredential(SettingProvider.PaypalClientId, SettingProvider.PaypalSecretKey, sdkConfig).GetAccessToken();

            return accessToken;
        }

        public static CreditCard SaveCreditCard(string accessToken, string cardType, string cardNumber, int expireMonth, int expireYear, string firstName, string lastName, string cvv)
        {
            var sdkConfig = new Dictionary<string, string> { { "mode", PayPalMode } };
            var apiContext = new APIContext(accessToken)
            {
                Config = sdkConfig
            };

            var credtCard = new CreditCard
            {
                type = cardType,
                number = cardNumber,
                expire_month = expireMonth,
                expire_year = expireYear,
                first_name = firstName,
                last_name = lastName,
                cvv2 = cvv
            };

            var createdCreditCard = credtCard.Create(apiContext);

            return createdCreditCard;
        }

        public static string InitPayWithPaypal(HttpRequestBase request, UrlHelper url, long paymentId, string mapId, string accessToken, decimal amount, string description = "", string currency = "SGD")
        {
            var sdkConfig = new Dictionary<string, string> { { "mode", PayPalMode } };
            var apiContext = new APIContext(accessToken) { Config = sdkConfig };

            var amnt = new Amount
            {
                currency = currency,
                total = amount.ToString("#,###,###.00", CultureInfo.GetCultureInfo("en-US"))
            };

            var transactionList = new List<Transaction>();
            var tran = new Transaction
            {
                description = description,
                amount = amnt
            };
            transactionList.Add(tran);

            var payr = new Payer { payment_method = "paypal" };

            var redirUrls = new RedirectUrls
            {
                cancel_url = url.Action("PaypalPayment", "Order", new
                {
                    success = "false",
                    payment = paymentId.ToString(),
                    cni = mapId
                }, request?.Url?.Scheme),
                return_url = url.Action("PaypalPayment", "Order", new
                {
                    success = "true",
                    payment = paymentId.ToString(),
                    cni = mapId
                }, request?.Url?.Scheme),
            };

            var pymnt = new PayPal.Api.Payment
            {
                intent = "sale",
                payer = payr,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            var createdPayment = pymnt.Create(apiContext);
            return createdPayment.GetApprovalUrl();
        }

        public static string InitTopupPayWithPaypal(HttpRequestBase request, UrlHelper url, long paymentId, string accessToken, decimal amount, string description = "", string currency = "SGD")
        {
            var sdkConfig = new Dictionary<string, string> { { "mode", PayPalMode } };
            var apiContext = new APIContext(accessToken) { Config = sdkConfig };

            var amnt = new Amount
            {
                currency = currency,
                total = amount.ToString("#,###,###.00", CultureInfo.GetCultureInfo("en-US"))
            };

            var transactionList = new List<Transaction>();
            var tran = new Transaction
            {
                description = description,
                amount = amnt
            };
            transactionList.Add(tran);

            var payr = new Payer { payment_method = "paypal" };

            var redirUrls = new RedirectUrls
            {
                cancel_url = url.Action("PaypalPayment", "Account", new
                {
                    success = "false",
                    payment = paymentId.ToString(),
                    topUpAmount = amount
                }, request?.Url?.Scheme),
                return_url = url.Action("PaypalPayment", "Account", new
                {
                    success = "true",
                    payment = paymentId.ToString(),
                    topUpAmount = amount
                }, request?.Url?.Scheme),
            };

            var pymnt = new PayPal.Api.Payment
            {
                intent = "sale",
                payer = payr,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            var createdPayment = pymnt.Create(apiContext);
            return createdPayment.GetApprovalUrl();
        }

        public static void PayWithPaypal(string accessToken, string paymentId, string payerId)
        {
            var sdkConfig = new Dictionary<string, string> { { "mode", PayPalMode } };
            var apiContext = new APIContext(accessToken);
            apiContext.Config = sdkConfig;

            var pymntExecution = new PaymentExecution { payer_id = payerId };
            var executedPayment = PayPal.Api.Payment.Execute(apiContext, paymentId, pymntExecution);
        }

        public static void PayWithCreditCard(string accessToken, string cardId, decimal amount, string description = "", string currency = "SGD")
        {
            var sdkConfig = new Dictionary<string, string> { { "mode", PayPalMode } };
            var apiContext = new APIContext(accessToken) { Config = sdkConfig };

            var credCardToken = new CreditCardToken
            {
                credit_card_id = cardId
            };

            var fundInstrument = new FundingInstrument
            {
                credit_card_token = credCardToken
            };

            var fundingInstrumentList = new List<FundingInstrument> { fundInstrument };

            var payr = new Payer
            {
                funding_instruments = fundingInstrumentList,
                payment_method = "credit_card"
            };

            var amnt = new Amount
            {
                currency = currency,
                total = amount.ToString("#,###,###.00", CultureInfo.GetCultureInfo("en-US"))
            };

            var tran = new Transaction
            {
                description = description,
                amount = amnt
            };

            var transactions = new List<Transaction> { tran };

            var pymnt = new PayPal.Api.Payment
            {
                intent = "sale",
                payer = payr,
                transactions = transactions
            };

            var createdPayment = pymnt.Create(apiContext);
        }

        public static string GetBrainTreeClientToken()
        {
            return Gateway.ClientToken.generate();
        }

        public static Result<Braintree.Transaction> PayWithBrainTree(decimal amount, string nonce)
        {
            var request = new TransactionRequest
            {
                Amount = amount,
                PaymentMethodNonce = nonce,
                Options = new TransactionOptionsRequest
                {
                    SubmitForSettlement = true
                }
            };

            var result = Gateway.Transaction.Sale(request);
            return result;
        }
    }
}
