﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Courier_Management_System.Helpers
{
    public static class EnumHelpers
    {
        public static List<SelectListItem> ConvertEnumToSelectList<T>(string nullText = "")
        {
            var list = (from object value in Enum.GetValues(typeof(T))
                        select new SelectListItem()
                               {
                                   Text = string.IsNullOrEmpty(GetDisplayValue(value)) ? Enum.GetName(typeof(T), value) : GetDisplayValue(value),
                                   Value = ((int) value).ToString()
                               }).ToList();

            if (!string.IsNullOrEmpty(nullText))
            {
                list.Insert(0,
                            new SelectListItem()
                            {
                                Text = nullText,
                                Value = ""
                            });
            }

            return list;
        }

        public static string GetDisplayValue<T>(T value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            var descriptionAttributes = fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null)
            {
                return string.Empty;
            }
            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
        }
    }
}