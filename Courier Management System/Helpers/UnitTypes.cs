﻿using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Helpers
{
    public enum UnitTypes
    {
        [Display(Name = "As amount spent")]
        SpentAmount,
        [Display(Name = "As shipment ordered")]
        Shipment
    }
}
