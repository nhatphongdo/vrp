﻿using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Helpers
{
    public enum PricePlans
    {
        [Display(Name = "Undefined")]
        Undefined,
        [Display(Name="Prepaid Biz Lite 1")]
        PrepaidBizLite1,
        [Display(Name = "Prepaid Biz Lite 2")]
        PrepaidBizLite2,
        [Display(Name = "Prepaid Biz Lite 3")]
        PrepaidBizLite3,
        [Display(Name = "Postpaid Classic")]
        PostPaidClassic,
        [Display(Name = "Postpaid Enterprise")]
        PostPaidEnterprise,
        [Display(Name = "Postpaid Elite")]
        PostpaidElite
    }
}
