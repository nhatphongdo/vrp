﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Courier_Management_System.Helpers
{
    public class ScheduleJobHelper
    {
        public static CourierDBEntities dbContext = new CourierDBEntities();

        public static async void SendSmsService()
        {
            try
            {
                var listSMS = dbContext.SmsMessages.Where(x => !x.Processed.HasValue).Take(100).ToList();
                
                if(listSMS.Count() == 0)
                {
                    return;
                }

                foreach (var item in listSMS)
                {
                    var client = new HttpClient();
                    var sms = dbContext.SmsMessages.FirstOrDefault(x=> x.Id == item.Id && !x.Processed.HasValue);

                    if(sms == null)
                    {
                        continue;
                    }

                    client.BaseAddress = new Uri(string.Format("http://roadbull.dyndns.org/index.php?md=smsapi&num={0}&msg={1}&pass=Roadbullsms",
                        sms.PhoneNumber, HttpUtility.UrlEncode(sms.Content)));
                    var response = await client.GetAsync("");
                    if (response.IsSuccessStatusCode)
                    {
                        sms.Processed = DateTime.Now;                        
                    }
                    else
                    {
                        sms.FailedCount++;
                        LogHelper.Log(string.Format("SMS sends failed. Information: Number: {0}, Message: {1}", sms.PhoneNumber, sms.Content));
                    }
                    await dbContext.SaveChangesAsync();
                }                
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
            }
        }
    }
}