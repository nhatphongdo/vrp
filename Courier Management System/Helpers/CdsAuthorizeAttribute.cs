﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Courier_Management_System.Helpers
{
    public class CdsAuthorizeAttribute : AuthorizeAttribute
    {
        //
        // Summary:
        //     Gets or sets the user roles that are un-authorized to access the controller or action
        //     method (Black-list). This will have higher priority than White-list
        //
        // Returns:
        //     The user roles that are un-authorized to access the controller or action method.
        public string DeniedRoles { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var deniedRoles = (DeniedRoles ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (httpContext.User.Identity.IsAuthenticated &&
                deniedRoles.Any(role => httpContext.User.IsInRole(role)))
            {
                // This user is logged in and has role in denied list
                return false;
            }

            // Proceed as normal
            var authorized = base.AuthorizeCore(httpContext);
            return authorized;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                // if not logged, it will work as normal Authorize and redirect to the Login
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                // logged and wihout the role to access it - redirect to the custom controller action
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "AccessDenied"
                }));
            }
        }
    }
}
