﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Courier_Management_System.Helpers
{
    public class DateTimeModelBinder : DefaultModelBinder
    {
        private string[] _customFormats;

        public DateTimeModelBinder(string[] customFormats)
        {
            _customFormats = customFormats;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null)
            {
                return null;
            }
            DateTime result;
            if (DateTime.TryParseExact(value.AttemptedValue, _customFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
            {
                return result;
            }

            return null;
        }
    }
}
