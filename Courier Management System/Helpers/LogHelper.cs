﻿using System;
using System.Web;

namespace Courier_Management_System.Helpers
{
    public class LogHelper
    {
        public static void Log(string message, string location = "")
        {
            var db = new CourierDBEntities();

            var log = new Log()
            {
                CreatedBy = HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : "",
                CreatedOn = DateTime.Now,
                Message = message,
                Location = location
            };
            db.Logs.Add(log);
            try
            {
                db.SaveChanges();
            }
            catch (Exception exc)
            {
            }
        }

        public static void Log(Exception exception)
        {
            var db = new CourierDBEntities();

            var log = new Log()
            {
                CreatedBy = HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : "",
                CreatedOn = DateTime.Now,
                Message = exception.Message,
                StackTrace = exception.ToString(),
                Location = exception.Source
            };
            db.Logs.Add(log);
            try
            {
                db.SaveChanges();
            }
            catch (Exception exc)
            {
            }
        }
    }
}
