﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Models;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using HashidsNet;
using BarcodeLib;

namespace Courier_Management_System.Helpers
{
    public class OrderHelper
    {
        public const string HASH_SALT = "Roadbu!! C0mpany Consignment Numb3r";
        public const string HASH_SALT_GROUP = "Gr0upTrackin9 Numb3r";
        public const string HASH_SALT_REQUEST = "RO4dbuL! C0mpany 3xtra Reque$t";

        public static string GenerateConsignmentNumber(long id)
        {
            var a1 = (int)(id & uint.MaxValue);
            var a2 = (int)(id >> 32);

            var hashids = new Hashids(HASH_SALT, 10, "0123456789ABCDEF");
            return hashids.Encode(a1, a2);
        }

        public static string GenerateGroupTrackingNumber(long id)
        {
            var a1 = (int)(id & uint.MaxValue);
            var a2 = (int)(id >> 32);

            var hashids = new Hashids(HASH_SALT_GROUP, 10, "0123456789ABCDEF");
            return "G" + hashids.Encode(a1, a2);
        }

        public static string GenerateExtraRequestNumber(long id)
        {
            var a1 = (int)(id & uint.MaxValue);
            var a2 = (int)(id >> 32);

            var hashids = new Hashids(HASH_SALT_REQUEST, 10, "0123456789ABCDEF");
            return hashids.Encode(a1, a2);
        }

        public static Bitmap GenerateQrCode(List<SelectListItem> paymentTypes, List<SelectListItem> services, List<SelectListItem> productTypes, List<SelectListItem> sizes, Order order)
        {
            var paymentType = paymentTypes.FirstOrDefault(x => x.Value == order.PaymentTypeId.ToString())?.Text;
            var service = services.FirstOrDefault(x => x.Value == order.ServiceId.ToString())?.Text;
            var productType = productTypes.FirstOrDefault(x => x.Value == order.ProductTypeId.ToString())?.Text;
            var size = sizes.FirstOrDefault(x => x.Value == order.SizeId.ToString())?.Text;
            var qrCodeContent = $"{order.Id}|{order.ConsignmentNumber}|{order.FromName}|{order.FromAddress}|{order.ToName}|{order.ToAddress}|{paymentType}|{service}|{productType}|{size}|{order.BinCode}";
            if (qrCodeContent.Length > 4000)
            {
                // Maximum size of QR content, generate with another content
                qrCodeContent = $"{order.Id}|{order.ConsignmentNumber}|{order.FromName}|{order.ToName}|{paymentType}|{service}|{productType}|{size}|{order.BinCode}";
            }
            var qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            var qrCode = qrEncoder.Encode(qrCodeContent);
            var renderer = new GraphicsRenderer(new FixedModuleSize(20, QuietZoneModules.Four), Brushes.Black, Brushes.White);

            var ms = new MemoryStream();
            renderer.WriteToStream(qrCode.Matrix, ImageFormat.Jpeg, ms);

            var bitmap = new Bitmap(ms);

            return bitmap;
        }

        public static string SaveQrCode(string consignmentNumber, Bitmap bitmap)
        {
            // Check folder
            var folder = PathHelper.OrderUploadBaseDirectory + PathHelper.StickersDirectory;
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(folder)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(folder));
            }

            var resultFileName = $"{Path.GetRandomFileName()}_{consignmentNumber}.jpg";
            var resultFileUrl = folder + resultFileName;
            var resultFilePath = HttpContext.Current.Server.MapPath(resultFileUrl);
            var jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            var encoder = System.Drawing.Imaging.Encoder.Quality;
            var encoderParameters = new EncoderParameters(1);
            var encoderParameter = new EncoderParameter(encoder, 100L);
            encoderParameters.Param[0] = encoderParameter;
            bitmap.Save(resultFilePath, jpgEncoder, encoderParameters);
            return resultFileUrl;
        }

        public static Bitmap GenerateBarCode(string consignmentNumber)
        {
            if (string.IsNullOrEmpty(consignmentNumber))
            {
                return null;
            }

            var barCodeContent = $"{consignmentNumber}";

            var barcode = new Barcode()
            {
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 100,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black
            };

            var barCode = barcode.Encode(TYPE.CODE128B, barCodeContent);

            var renderer = new GraphicsRenderer(new FixedModuleSize(20, QuietZoneModules.Four), Brushes.Black, Brushes.White);
            var ms = new MemoryStream();
            barCode.Save(ms, ImageFormat.Jpeg);

            var bitmap = new Bitmap(ms);

            return bitmap;
        }

        public static string SaveBarCode(string consignmentNumber, Bitmap bitmap)
        {
            // Check folder
            var folder = PathHelper.OrderUploadBaseDirectory + PathHelper.BarCodesDirectory;
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(folder)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(folder));
            }

            var resultFileName = $"{consignmentNumber}.jpg";
            var resultFileUrl = folder + resultFileName;
            var resultFilePath = HttpContext.Current.Server.MapPath(resultFileUrl);
            var jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            var encoder = System.Drawing.Imaging.Encoder.Quality;
            var encoderParameters = new EncoderParameters(1);
            var encoderParameter = new EncoderParameter(encoder, 100L);
            encoderParameters.Param[0] = encoderParameter;
            bitmap.Save(resultFilePath, jpgEncoder, encoderParameters);
            return resultFileUrl;
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            var codecs = ImageCodecInfo.GetImageDecoders();
            return codecs.FirstOrDefault(codec => codec.FormatID == format.Guid);
        }

        public static string GenerateBinCode(Order order)
        {
            var db = new CourierDBEntities();

            var timeSlot = db.TimeSlots.FirstOrDefault(x => x.Id == order.DeliveryTimeSlotId);
            var productType = db.ProductTypes.FirstOrDefault(x => x.Id == order.ProductTypeId);
            return (order.DeliveryDate.HasValue ? ((int)order.DeliveryDate.Value.DayOfWeek).ToString() : "") +
                   (timeSlot?.BinCode ?? "") + (!string.IsNullOrEmpty(productType?.BinCode) ? "-" + productType?.BinCode : "");
        }

        public static dynamic CalculatePrice(PriceRequestViewModel model)
        {
            var db = new CourierDBEntities();

            // Get base price
            var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == model.ServiceId);
            var servicePrice = service?.Cost ?? 0;

            // Get add-on price
            var addOnPrice = db.Pricings.Where(x => x.ProductTypeId == model.ProductTypeId && x.ServiceId == model.ServiceId &&
                                                    x.SizeId == model.SizeId).Select(x => x.ChargedCost).FirstOrDefault();

            var subTotal = servicePrice + addOnPrice;

            var now = DateTime.Now.Date;
            decimal defaultPromoCodeCost = 0;
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.IsActive && x.Id == model.CustomerId);
            if (customer != null && !string.IsNullOrEmpty(customer.DefaultPromoCode))
            {
                var promoCodes = customer.DefaultPromoCode.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var promos = db.Charges.Where(x => !x.IsDeleted && (!x.ValidFrom.HasValue || (x.ValidFrom.HasValue && x.ValidFrom.Value <= now))
                                                   && (!x.ValidTo.HasValue || (x.ValidTo.HasValue && x.ValidTo.Value >= now))
                                                   && promoCodes.Contains(x.ChargeCode.ToLower())).ToList();

                if (service != null && service.IsAppliedDefaultPromoCode)
                {
                    foreach (var promo in promos)
                    {
                        if ((!promo.ServiceId.HasValue || promo.ServiceId.Value == service.Id) &&
                            (!promo.SizeId.HasValue || promo.SizeId.Value == model.SizeId))
                        {
                            defaultPromoCodeCost += promo.IsChargeInPercentage
                                                        ? promo.ChargeAmount * subTotal
                                                        : promo.ChargeAmount;
                        }
                    }
                }
            }

            // Get sur-charge or promotion
            var codes = (model.AddOnCode ?? "").ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var addInCharges = db.Charges.Where(x => !x.IsDeleted && (!x.ValidFrom.HasValue || (x.ValidFrom.HasValue && x.ValidFrom.Value <= now))
                                                     && (!x.ValidTo.HasValue || (x.ValidTo.HasValue && x.ValidTo.Value >= now))
                                                     && (!x.ServiceId.HasValue || (x.ServiceId.HasValue && x.ServiceId.Value == service.Id))
                                                     && (!x.SizeId.HasValue || (x.SizeId.HasValue && x.SizeId.Value == model.SizeId))
                                                     && ((string.IsNullOrEmpty(x.ChargeCode) && string.IsNullOrEmpty(model.AddOnCode)) || codes.Contains(x.ChargeCode.ToLower())))
                                 .ToList();

            decimal totalPromotion = 0;
            decimal totalSurcharge = 0;
            var intermediateTotal = subTotal + defaultPromoCodeCost;
            foreach (var charge in addInCharges)
            {
                if (charge.ChargeAmount < 0)
                {
                    // Only 1 promotion is allowed
                    if (totalPromotion == 0)
                    {
                        totalPromotion -= (charge.IsChargeInPercentage ? charge.ChargeAmount * intermediateTotal : charge.ChargeAmount);
                        intermediateTotal -= (charge.IsChargeInPercentage ? charge.ChargeAmount * intermediateTotal : charge.ChargeAmount);
                    }
                }
                else
                {
                    totalSurcharge += (charge.IsChargeInPercentage ? charge.ChargeAmount * intermediateTotal : charge.ChargeAmount);
                    intermediateTotal += (charge.IsChargeInPercentage ? charge.ChargeAmount * intermediateTotal : charge.ChargeAmount);
                }
            }

            // Get COD
            var codIds = (model.Cod ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var codAmounts = (model.CodAmounts ?? "").Split(new char[] { ',' });
            var codOptions = db.CodOptions.Where(x => !x.IsDeleted && codIds.Contains(x.Id.ToString())).ToList();
            decimal totalCodAmount = 0;
            foreach (var codOption in codOptions)
            {
                decimal amount = 0;
                var index = Array.IndexOf(codIds, codOption.Id.ToString());
                if (index >= 0 && index < codAmounts.Length)
                {
                    decimal.TryParse(string.IsNullOrEmpty(codAmounts[index]) ? "" : codAmounts[index], out amount);
                }

                if (codOption.IsChargeInPercentage)
                {
                    totalCodAmount += amount * codOption.ChargeAmount;
                }
                else
                {
                    totalCodAmount += codOption.ChargeAmount;
                }
            }

            if (model.IsExchange)
            {
                totalCodAmount += (decimal)SettingProvider.ExchangeOrderCharge;
            }

            return new
            {
                BasePrice = servicePrice,
                AddOnPrice = addOnPrice,
                DefaultPromotion = defaultPromoCodeCost,
                Promotion = totalPromotion,
                Surcharge = totalSurcharge + totalCodAmount,
                Total = Math.Max(SettingProvider.MinimumOrderAmount, servicePrice + addOnPrice + defaultPromoCodeCost - totalPromotion + totalSurcharge + totalCodAmount),
                Charges = addInCharges.Select(x => new
                {
                    x.ChargeCode,
                    x.ChargeAmount,
                    x.IsChargeInPercentage
                })
            };
        }

        public static bool SendOrderEmail(UrlHelper url, List<Order> orders, string mapId)
        {
            // Load template
            try
            {
                var emailTemplate = string.Join("\n",
                                                File.ReadAllLines(HttpContext.Current.Server.MapPath("~/Templates/email_order_confirm.html"), Encoding.UTF8));
                var itemTemplate = string.Join("\n",
                                               File.ReadAllLines(HttpContext.Current.Server.MapPath("~/Templates/email_order_tracking_item.html"), Encoding.UTF8));

                var link = url.Action("Tracking",
                                      "Order",
                                      new
                                      {
                                          area = "",
                                          id = mapId
                                      },
                                      HttpContext.Current.Request.Url.Scheme);
                emailTemplate = emailTemplate.Replace("{OrderTrackingCommonLink}", link);

                var customerId = 0L;
                var orderBuilder = new StringBuilder();
                foreach (var group in orders.GroupBy(x => x.GroupTrackingNumber))
                {
                    if (!string.IsNullOrEmpty(group.Key))
                    {
                        link = url.Action("Tracking",
                                          "Order",
                                          new
                                          {
                                              area = "",
                                              id = group.Key
                                          },
                                          HttpContext.Current.Request.Url.Scheme);
                        orderBuilder.AppendFormat("Group Tracking Number: <a href='{0}' target='_blank'><font size='5' color='blue'>{1}</font></a>", link, group.Key);
                        orderBuilder.Append("<br />");
                    }
                    foreach (var order in group)
                    {
                        var temp = itemTemplate.Replace("{ConsignmentNumber}", order.ConsignmentNumber);
                        //link = url.Action("Tracking",
                        //                  "Order",
                        //                  new
                        //                  {
                        //                      area = "",
                        //                      id = order.ConsignmentNumber
                        //                  },
                        //                  HttpContext.Current.Request.Url.Scheme);
                        link = url.Action("Tracking",
                                          "Order",
                                          new
                                          {
                                              area = "",
                                              id = "RB_" + order.OrderNumber
                                          },
                                          HttpContext.Current.Request.Url.Scheme);
                        temp = temp.Replace("{TrackingLink}", link);
                        orderBuilder.Append(temp);
                        customerId = order.CustomerId.HasValue ? order.CustomerId.Value : 0;
                    }
                    // Separate groups
                    orderBuilder.Append("<br />");
                }

                emailTemplate = emailTemplate.Replace("{OrderTrackingLinks}", orderBuilder.ToString());

                var db = new CourierDBEntities();
                var customer = db.Customers.FirstOrDefault(x => x.Id == customerId);

                if (customer != null)
                {
                    var result = AccountHelper.SendEmailSync(customer.UserName, "Order confirmation", emailTemplate);
                    if (result.Code == 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static bool SendFailedPickupEmail(UrlHelper url, Order order, ExtraRequest request)
        {
            // Load template
            try
            {
                var emailTemplate = string.Join("\n",
                                                File.ReadAllLines(HttpContext.Current.Server.MapPath("~/Templates/email_failed_pickup.html"), Encoding.UTF8));

                var link = url.Action("Tracking",
                                      "Order",
                                      new
                                      {
                                          area = "",
                                          id = order.ConsignmentNumber
                                      },
                                      HttpContext.Current.Request.Url.Scheme);
                emailTemplate = emailTemplate.Replace("{OrderTrackingCommonLink}", link);
                emailTemplate = emailTemplate.Replace("{PickupTime}", order.CollectedOn?.ToString("dd/MM/yyyy HH:mm:ss"));
                link = url.Action("ExtraRequest",
                                  "Order",
                                  new
                                  {
                                      area = "",
                                      id = request?.Code
                                  },
                                  HttpContext.Current.Request.Url.Scheme);
                emailTemplate = emailTemplate.Replace("{RequestLink}", link);

                var db = new CourierDBEntities();
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);

                if (customer != null)
                {
                    var result = AccountHelper.SendEmailSync(customer.UserName, "Order status", emailTemplate);
                    if (result.Code == 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static bool SendFailedDeliveryEmail(UrlHelper url, Order order, ExtraRequest request)
        {
            return true;
            // Load template
            try
            {
                var emailTemplate = string.Join("\n",
                                                File.ReadAllLines(HttpContext.Current.Server.MapPath("~/Templates/email_failed_delivery.html"), Encoding.UTF8));

                var link = url.Action("Tracking",
                                      "Order",
                                      new
                                      {
                                          area = "",
                                          id = order.ConsignmentNumber
                                      },
                                      HttpContext.Current.Request.Url.Scheme);
                emailTemplate = emailTemplate.Replace("{OrderTrackingCommonLink}", link);
                emailTemplate = emailTemplate.Replace("{DeliveryTime}", order.DeliveredOn?.ToString("dd/MM/yyyy HH:mm:ss"));
                link = url.Action("ExtraRequest",
                                  "Order",
                                  new
                                  {
                                      area = "",
                                      id = request?.Code
                                  },
                                  HttpContext.Current.Request.Url.Scheme);
                emailTemplate = emailTemplate.Replace("{RequestLink}", link);

                var db = new CourierDBEntities();
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);

                if (customer != null)
                {
                    var result = AccountHelper.SendEmailSync(customer.UserName, "Order status", emailTemplate);
                    if (result.Code == 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static async Task<bool> SendOrderSms(UrlHelper url, List<Order> orders, string mapId)
        {
            return true;
            // Load template
            try
            {
                var smsTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Templates/sms_order_confirm.txt"), Encoding.UTF8);
                var customerId = orders.Select(x => x.CustomerId).FirstOrDefault();

                var db = new CourierDBEntities();
                var customer = db.Customers.FirstOrDefault(x => x.Id == customerId);

                if (customer != null)
                {
                    smsTemplate = smsTemplate.Replace("{Customer}", customer.CustomerName);
                }

                string link;
                foreach (var group in orders.GroupBy(x => x.GroupTrackingNumber))
                {
                    if (string.IsNullOrEmpty(group.Key))
                    {
                        // Individual order
                        foreach (var order in group)
                        {
                            //link = url.Action("Tracking",
                            //                  "Order",
                            //                  new
                            //                  {
                            //                      area = "",
                            //                      id = order.ConsignmentNumber
                            //                  },
                            //                  HttpContext.Current.Request.Url.Scheme);
                            link = url.Action("Tracking",
                                              "Order",
                                              new
                                              {
                                                  area = "",
                                                  id = "RB_" + order.OrderNumber
                                              },
                                              HttpContext.Current.Request.Url.Scheme);

                            var result = await AccountHelper.SendSmsService(order.ToMobilePhone, smsTemplate.Replace("{OrderTrackingLinks}", link));
                        }
                    }
                    else
                    {
                        link = url.Action("Tracking",
                                          "Order",
                                          new
                                          {
                                              area = "",
                                              id = group.Key
                                          },
                                          HttpContext.Current.Request.Url.Scheme);

                        var result = await AccountHelper.SendSmsService(group.FirstOrDefault().ToMobilePhone, smsTemplate.Replace("{OrderTrackingLinks}", link));
                    }
                }

                //link = url.Action("Tracking",
                //                      "Order",
                //                      new
                //                      {
                //                          area = "",
                //                          id = mapId
                //                      },
                //                      HttpContext.Current.Request.Url.Scheme);
                //smsTemplate = smsTemplate.Replace("{OrderTrackingCommonLink}", link);

                return true;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static async Task<bool> SendOrderCollectedSms(UrlHelper url, Order order, ExtraRequest request)
        {
            // Load template
            try
            {
                var db = new CourierDBEntities();
                var smsTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Templates/sms_order_collected.txt"), Encoding.UTF8);
                smsTemplate = smsTemplate.Replace("[Customer]", order.ToName);
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);
                smsTemplate = smsTemplate.Replace("[Sender]", customer == null ? "" : customer.CustomerCompany);
                smsTemplate = smsTemplate.Replace("[Date]", order.DeliveryDate?.ToString("dd/MM/yy"));
                var timeslot = db.TimeSlots.FirstOrDefault(x => x.Id == order.DeliveryTimeSlotId);
                if (timeslot != null && timeslot.TimeSlotName.Equals("Booking + 3hrs", StringComparison.OrdinalIgnoreCase))
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", "a time within 3 hours from " + order.ConfirmedOn?.ToString("hh:mmtt"));
                }
                else if (timeslot != null && timeslot.TimeSlotName.Equals("Booking + 4hrs", StringComparison.OrdinalIgnoreCase))
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", "a time within 4 hours from " + order.ConfirmedOn?.ToString("hh:mmtt"));
                }
                else
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", timeslot == null ? "" : ("(" + timeslot.FromTime.ToString("hh:mmtt") + "-" + timeslot.ToTime.ToString("hh:mmtt") + ")"));
                }

                var link = url == null
                               ? ""
                               : url.Action("Tracking",
                                            "Order",
                                            new
                                            {
                                                area = "",
                                                // id = order.ConsignmentNumber
                                                id = "RB_" + order.OrderNumber
                                            },
                                            HttpContext.Current.Request.Url.Scheme);

                smsTemplate = smsTemplate.Replace("[TrackingLink]", link);
                link = url == null
                           ? ""
                           : url.Action("ExtraRequest",
                                        "Order",
                                        new
                                        {
                                            area = "",
                                            id = request?.Code
                                        },
                                        HttpContext.Current.Request.Url.Scheme);
                smsTemplate = smsTemplate.Replace("[RequestLink]", link);

                var result = await AccountHelper.SendSmsService(order.ToMobilePhone, smsTemplate);
                return result;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static async Task<bool> SendOrderDeliveredSms(UrlHelper url, Order order)
        {
            // Load template
            try
            {
                var db = new CourierDBEntities();
                var smsTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Templates/sms_order_delivered.txt"), Encoding.UTF8);
                smsTemplate = smsTemplate.Replace("[Customer]", order.ToName);
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);
                smsTemplate = smsTemplate.Replace("[Sender]", customer == null ? "" : customer.CustomerCompany);
                smsTemplate = smsTemplate.Replace("[Date]", order.DeliveredOn?.ToString("dd/MM/yy"));
                smsTemplate = smsTemplate.Replace("[Time]", order.DeliveredOn?.ToString("HH:mm"));

                var result = await AccountHelper.SendSmsService(order.ToMobilePhone, smsTemplate);
                return result;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static async Task<bool> SendFailedPickupSms(UrlHelper url, Order order, ExtraRequest request)
        {
            return true;
            // Load template
            try
            {
                var db = new CourierDBEntities();
                var smsTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Templates/sms_order_failed_pickup.txt"), Encoding.UTF8);
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);
                smsTemplate = smsTemplate.Replace("[Sender]", customer == null ? "" : customer.CustomerCompany);
                smsTemplate = smsTemplate.Replace("[Date]", order.CollectedOn?.ToString("dd/MM/yy"));
                smsTemplate = smsTemplate.Replace("[Time]", order.CollectedOn?.ToString("HH:mm"));
                var link = url.Action("ExtraRequest",
                                      "Order",
                                      new
                                      {
                                          area = "",
                                          id = request?.Code
                                      },
                                      HttpContext.Current.Request.Url.Scheme);
                smsTemplate = smsTemplate.Replace("[RequestLink]", link);

                var result = await AccountHelper.SendSmsService(order.FromMobilePhone, smsTemplate);
                return result;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static async Task<bool> SendFailedDeliverySms(UrlHelper url, Order order, ExtraRequest request)
        {
            // Load template
            try
            {
                var db = new CourierDBEntities();
                var smsTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Templates/sms_order_failed_delivery.txt"), Encoding.UTF8);
                smsTemplate = smsTemplate.Replace("[Customer]", order.ToName);
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);
                smsTemplate = smsTemplate.Replace("[Sender]", customer == null ? "" : customer.CustomerCompany);
                smsTemplate = smsTemplate.Replace("[Date]", order.DeliveredOn?.ToString("dd/MM/yy"));
                smsTemplate = smsTemplate.Replace("[Time]", order.DeliveredOn?.ToString("HH:mm"));
                var link = url.Action("ExtraRequest",
                                      "Order",
                                      new
                                      {
                                          area = "",
                                          id = request?.Code
                                      },
                                      HttpContext.Current.Request.Url.Scheme);
                smsTemplate = smsTemplate.Replace("[RequestLink]", link);

                var result = await AccountHelper.SendSmsService(order.ToMobilePhone, smsTemplate);
                return result;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static async Task<bool> SendOrderRedeliverySms(UrlHelper url, Order order)
        {
            // Load template
            try
            {
                var db = new CourierDBEntities();
                var smsTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Templates/sms_order_redelivery.txt"), Encoding.UTF8);
                smsTemplate = smsTemplate.Replace("[Customer]", order.ToName);
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);
                smsTemplate = smsTemplate.Replace("[Sender]", customer == null ? "" : customer.CustomerCompany);
                smsTemplate = smsTemplate.Replace("[Date]", order.DeliveryDate?.ToString("dd/MM/yy"));
                var timeslot = db.TimeSlots.FirstOrDefault(x => x.Id == order.DeliveryTimeSlotId);
                if (timeslot != null && timeslot.TimeSlotName.Equals("Booking + 3hrs", StringComparison.OrdinalIgnoreCase))
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", "a time within 3 hours from " + order.ConfirmedOn?.ToString("hh:mmtt"));
                }
                else if (timeslot != null && timeslot.TimeSlotName.Equals("Booking + 4hrs", StringComparison.OrdinalIgnoreCase))
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", "a time within 4 hours from " + order.ConfirmedOn?.ToString("hh:mmtt"));
                }
                else
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", timeslot == null ? "" : ("(" + timeslot.FromTime.ToString("hh:mmtt") + "-" + timeslot.ToTime.ToString("hh:mmtt") + ")"));
                }

                var result = await AccountHelper.SendSmsService(order.ToMobilePhone, smsTemplate);
                return result;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static async Task<bool> SendTodayDeliverySms(UrlHelper url, Order order)
        {
            // Load template
            try
            {
                var db = new CourierDBEntities();
                var smsTemplate = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Templates/sms_delivery_today.txt"), Encoding.UTF8);
                smsTemplate = smsTemplate.Replace("[Customer]", order.ToName);
                var customer = db.Customers.FirstOrDefault(x => x.Id == order.CustomerId);
                smsTemplate = smsTemplate.Replace("[Sender]", customer == null ? "" : customer.CustomerCompany);
                var timeslot = db.TimeSlots.FirstOrDefault(x => x.Id == order.DeliveryTimeSlotId);
                if (timeslot != null && timeslot.TimeSlotName.Equals("Booking + 3hrs", StringComparison.OrdinalIgnoreCase))
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", "a time within 3 hours from " + order.ConfirmedOn?.ToString("hh:mmtt"));
                }
                else if (timeslot != null && timeslot.TimeSlotName.Equals("Booking + 4hrs", StringComparison.OrdinalIgnoreCase))
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", "a time within 4 hours from " + order.ConfirmedOn?.ToString("hh:mmtt"));
                }
                else
                {
                    smsTemplate = smsTemplate.Replace("[TimeSlot]", timeslot == null ? "" : ("(" + timeslot.FromTime.ToString("hh:mmtt") + "-" + timeslot.ToTime.ToString("hh:mmtt") + ")"));
                }

                var result = await AccountHelper.SendSmsService(order.ToMobilePhone, smsTemplate);
                return result;
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }

        public static void SendDailyDeliverySms()
        {
            var beginDate = DateTime.Now.Date;
            var endDate = beginDate.AddDays(1);
            var db = new CourierDBEntities();
            var orders = db.Orders
                           .Where(
                                  x =>
                                  !x.IsDeleted && (x.Status == (int)OrderStatuses.ItemInDepot || x.Status == (int)OrderStatuses.ItemOutDepot || x.Status == (int)OrderStatuses.SpecialOrderForDelivery) &&
                                  x.DeliveryDate >= beginDate && x.DeliveryDate < endDate)
                           .ToList();
            foreach (var order in orders)
            {
                var result = SendTodayDeliverySms(HttpContext.Current != null ? new UrlHelper(HttpContext.Current.Request.RequestContext) : null, order).Result;
                LogHelper.Log(string.Format("Auto send Delivery notification daily of order #{0} is {1}", order.ConsignmentNumber, result ? "successful" : "failed"));
            }
        }

        public static string ToCustomerOrderNumber(string rbOrderNumber, string prefix)
        {
            if (prefix == null || rbOrderNumber == null) return rbOrderNumber;
            if (rbOrderNumber.IndexOf(prefix, StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                return rbOrderNumber.Remove(0, prefix.Length);
            }

            return rbOrderNumber;
        }
    }
}