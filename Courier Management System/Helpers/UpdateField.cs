﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Courier_Management_System.Helpers
{
    public enum UpdateUsingField
    {
        [Display(Name = "Consignment Number")]
        ConsignmentNumber = 1, //1
        [Display(Name = "Order Number")]
        OrderNumber = 2, //2
    }
}