﻿
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Helpers
{
    public enum PaymentTypes
    {
        [Display(Name = "Pre-Paid")]
        PrePaid,
        [Display(Name = "Post-Paid")]
        PostPaid
    }

    public enum PaymentStatus
    {
        Undefined,
        Pending,
        Approved,
        Declined
    }
}
