﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courier_Management_System.Helpers
{
    public enum PaymentAccountTypes
    {
        [Display(Name = "Paypal Account")]
        PaypalAccount,
        [Display(Name = "Visa Credit Card")]
        VisaCard,
        [Display(Name = "Master Credit Card")]
        MasterCard,
        [Display(Name = "BrainTree Account")]
        BrainTree,
        [Display(Name = "Bank Transfer")]
        Bank,
        [Display(Name = "Credit Card")]
        CreditCard
    }
}
