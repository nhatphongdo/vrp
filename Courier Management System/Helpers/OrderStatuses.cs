﻿using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Helpers
{
    public enum OrderStatuses
    {
        [Display(Name = "Order is Submitted")]
        OrderSubmitted, // 0
        [Display(Name = "Order is Confirmed")]
        OrderConfirmed, // 1
        [Display(Name = "Order is Rejected")]
        OrderRejected, // 2
        [Display(Name = "Item is Collected")]
        ItemCollected, // 3
        [Display(Name = "Pickup Failed")]
        PickupFailed, // 4
        [Display(Name = "Item is in Warehouse")]
        ItemInDepot, // 5
        [Display(Name = "Delivery in progress")]
        ItemOutDepot, // 6
        [Display(Name = "Item is Delivered")]
        ItemDelivered, // 7
        [Display(Name = "Order is Canceled")]
        OrderCanceled, // 8
        [Display(Name = "Delivery Failed")]
        DeliveryFailed, // 9
        [Display(Name = "Order is Moved to Special Jobs For Pickup")]
        SpecialOrderForPickup, // 10
        [Display(Name = "Order is Moved to Special Jobs For Delivery")]
        SpecialOrderForDelivery, // 11
        [Display(Name = "Enroute to Pickup")]
        ItemHasBeingPicked, // 12
        [Display(Name = "Item is Picked up again")]
        RePickup, // 13
        [Display(Name = "Re-Delivery in Progress")]
        ReDelivery, // 14,
        [Display(Name = "Item is Returned to Sender")]
        ReturnToSender, // 15
    }
}
