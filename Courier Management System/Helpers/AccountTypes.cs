﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Helpers
{
    public enum AccountTypes
    {
        Staff = 0,
        Contractor = 1,

        [Display(Name = "Outsourced Contractor")]
        OutsourcedContractor = 2
    }
}