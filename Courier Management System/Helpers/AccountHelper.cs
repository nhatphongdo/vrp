﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Courier_Management_System.Models;
using DevExpress.Xpo.XtraData;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

namespace Courier_Management_System.Helpers
{
    public static class AccountHelper
    {
        // First item MUST be Administrator
        public const string AdministratorRole = "Administrator";
        public const string CustomerRole = "Customer";
        public const string StaffDriverRole = "Staff Driver";
        public const string ContractorDriverRole = "Contractor Driver";
        public const string SalesPersonRole = "Sales Person";
        public const string ManagerRole = "Manager";
        public const string VendorRole = "Vendor";
        public const string MerchantRole = "Merchant";
        // Keep below names for compability
        //     public const string CustomerCareRole = "Customer Care";
        //     public const string FinanceRole = "Finance";
        //     public const string Operations1Role = "Operations1";
        //     public const string Operations2Role = "Operations2";

        // Rules
        public const string BasicDashboard = "Basic Dashboard"; // Rule A
        public const string AdvancedDashboard = "Advanced Dashboard"; // Other options in Dashboard
        //public const string ShowOrdersMenu = "Show Orders Menu"; // Orders sub-menu
        public const string BasicOrderList = "Basic Order List"; // Rule B
        public const string NoDetailOrderList = "No Detail Order List"; // Rule B1
        public const string NoSenderInformation = "No Sender Information"; // Use combination with B1
        //public const string ShowInvoicesReport = "Show Invoices Report"; // Rule E1
        //public const string AllowExportInvoicesReport = "Allow Export Invoices Report"; // Rule E = E1 + this
        //public const string ShowFailedOrders = "Show Failed Orders"; // Rule F
        //public const string ShowCustomers = "Show Customers"; // Rule C - No export
        //public const string ShowCustomersNoDelete = "Show Customers No Delete"; // Rule C1       
        //public const string ShowSalesReport = "Show Sales Report"; //Rule E2
        //public const string ShowPostalCode = "Show Postal Code"; // Rule G
        //public const string EditOrder = "Edit Order"; // Rule D
        public const string EditOrderPrice = "Edit Order Price"; // Rule D1, need to be used in companion with Rule D
        public const string ShowCustomersWithLimitation = "Show Customers With Limitation"; // Rules for Ops 1
        //public const string ShowRePickupDeliveryRequests = "Show RePickupDelivery Request"; // Rule for RBCC

        // "Function" rules
        public const string OrdersShowOrders = "Orders - Show Orders";
        public const string OrdersAddOrder = "Orders - Add Order";
        public const string OrdersEditOrder = "Orders - Edit Order";
        public const string OrdersDeleteOrder = "Orders - Delete Order";
        public const string OrdersExportOrders = "Orders - Export Orders";
        public const string OrdersBulkUpdate = "Orders - Bulk Update Orders";
        public const string OrdersBulkExportVrp = "Orders - Bulk Export VRP";
        public const string OrdersBulkPrintQr = "Orders - Bulk Print QR";
        public const string OrdersShowOrderEvents = "Orders - Show Order Events";
        public const string OrdersShowTodayOrders = "Orders - Show Today Orders";
        public const string OrdersExportTodayOrders = "Orders - Export Today Orders";
        public const string OrdersDeleteTodayOrder = "Orders - Delete Today Order";
        public const string OrdersShowInvoicesReport = "Orders - Show Invoices Report";
        public const string OrdersInvoicesReportPrintInvoice = "Orders - Invoices Report - Print Invoice";
        public const string OrdersInvoicesReportSendInvoice = "Orders - Invoices Report - Send Invoice";
        public const string OrdersExportInvoicesReport = "Orders - Export Invoices Report";
        public const string OrdersShowFailedOrders = "Orders - Show Failed Orders";
        public const string OrdersExportFailedOrders = "Orders - Export Failed Orders";
        public const string OrdersDeleteFailedOrder = "Orders - Delete Failed Order";
        public const string OrdersEditFailedOrder = "Orders - Edit Failed Order";
        public const string OrdersShowRePickupRequests = "Orders - Show Re-Pickup Requests";
        public const string OrdersEditRePickupRequest = "Orders - Edit Re-Pickup Request";
        public const string OrdersDeleteRePickupRequest = "Orders - Delete Re-Pickup Request";
        public const string OrdersExportRePickupRequests = "Orders - Export Re-Pickup Request";
        public const string OrdersShowReDeliveryRequests = "Orders - Show Re-Delivery Requests";
        public const string OrdersEditReDeliveryRequest = "Orders - Edit Re-Delivery Request";
        public const string OrdersDeleteReDeliveryRequest = "Orders - Delete Re-Delivery Request";
        public const string OrdersExportReDeliveryRequests = "Orders - Export Re-Delivery Requests";
        public const string OrdersShowSalesReport = "Orders - Show Sales Report";
        public const string OrdersExportSalesReport = "Orders - Export Sales Report";
        public const string OrdersShowManagementReport = "Orders - Show Management Report";
        public const string OrdersShowOrderDetail = "Orders - Show Order Detail";
        public const string DispatchShowTodayJobs = "Dispatch Orders - Show Today Jobs";
        public const string DispatchShowSpecialJobs = "Dispatch Orders - Show Special Jobs";
        public const string DispatchDeleteJob = "Dispatch Orders - Delete Job";
        public const string DispatchImportJobs = "Dispatch Orders - Import Jobs";
        public const string DispatchPrintPlan = "Dispatch Orders - Print Plan";
        public const string DispatchPrintSpecialPlan = "Dispatch Orders - Print Special Plan";
        public const string DispatchShowJobsReport = "Dispatch Orders - Show Jobs Report";
        public const string DispatchTrackDriversLocation = "Dispatch Orders - Track Drivers Location";
        public const string DispatchDriverHistory = "Dispatch Orders - Driver History";
        public const string ReportsSalesDeliveryReport = "Reports - Sales Delivery Report";
        public const string ReportsExportSalesDeliveryReport = "Reports - Export Sales Delivery Report";
        public const string ReportsSalesServiceReport = "Reports - Sales Service Report";
        public const string ReportsExportSalesServiceReport = "Reports - Export Sales Service Report";
        public const string ReportsFailedDeliveryReport = "Reports - Failed Delivery Report";
        public const string ReportsExportFailedDeliveryReport = "Reports - Export Failed Delivery Report";
        public const string ReportsCodReport = "Reports - COD Report";
        public const string ReportsExportCodReport = "Reports - Export COD Report";
        public const string ReportsJobHistoryReport = "Reports - Job History Report";
        public const string ReportsOpsDeliverySummary = "Report - Ops Delivery Summary";
        public const string ReportsExportOpsDeliverySummary = "Reports - Export Ops Delivery Summary";
        public const string ReportsOpsPickupSummary = "Reports - Ops Pickup Summary";
        public const string ReportsExportOpsPickupSummary = "Reports - Export Ops Pickup Summary";
        public const string ReportsBillingSummary = "Reports - Billing Summary";
        public const string ReportsExportBillingSummary = "Reports - Export Billing Summary";
        public const string ReportsBillingReport = "Reports - Billing Report";
        public const string ReportsExportBillingReport = "Reports - Export Billing Report";
        public const string DriversShowDrivers = "Drivers - Show Drivers";
        public const string DriversAddDriver = "Drivers - Add Driver";
        public const string DriversEditDriver = "Drivers - Edit Driver";
        public const string DriversDeleteDriver = "Drivers - Delete Driver";
        public const string DriversExportDrivers = "Drivers - Export Driver";
        public const string DriversShowPayScheme = "Drivers - Show Pay Scheme";
        public const string DriversAddPayScheme = "Drivers - Add Pay Scheme";
        public const string DriversEditPayScheme = "Drivers - Edit Pay Scheme";
        public const string DriversDeletePayScheme = "Drivers - Delete Pay Scheme";
        public const string DriversExportPaySchemes = "Drivers - Export Pay Schemes";
        public const string DriversShowDriverHistory = "Drivers - Show Driver History";
        public const string DriversDeleteDriverHistory = "Drivers - Delete Driver History";
        public const string DriversExportDriverHistory = "Drivers - Export Driver History";
        public const string DriversShowDriverPayslip = "Drivers - Show Driver Payslip";
        public const string DriversEmailDriverPayslip = "Drivers - Email Driver Payslip";
        public const string CustomersShowCustomers = "Customers - Show Customers";
        public const string CustomersAddCustomer = "Customers - Add Customer";
        public const string CustomersEditCustomer = "Customers - Edit Customer";
        public const string CustomersDeleteCustomer = "Customers - Delete Customer";
        public const string CustomersExportCustomers = "Customers - Export Customers";
        public const string CustomersShowPaymentTypes = "Customers - Show Payment Types";
        public const string CustomersAddPaymentType = "Customers - Add Payment Type";
        public const string CustomersEditPaymentType = "Customers - Edit Payment Type";
        public const string CustomersDeletePaymentType = "Customers - Delete Payment Type";
        public const string CustomersShowPaymentStatus = "Customers - Show Payment Status";
        public const string CustomersEditPaymentStatus = "Customers - Edit Payment Status";
        public const string CustomersExportPaymentStatus = "Customers - Export Payment Status";
        public const string CustomersAddRefund = "Customers - Add Refund";
        public const string CustomersShowRefundStatus = "Customers - Show Refund Status";
        public const string CustomersEditRefundStatus = "Customers - Edit Refund Status";
        public const string CustomersExportRefundStatus = "Customers - Export Refund Status";
        public const string CustomersShowPricePlans = "Customers - Show Price Plans";
        public const string CustomersAddPricePlan = "Customers - Add Price Plan";
        public const string CustomersEditPricePlan = "Customers - Edit Price Plan";
        public const string CustomersDeletePricePlan = "Customers - Delete Price Plan";
        public const string VehiclesShowVehicles = "Vehicles - Show Vehicles";
        public const string VehiclesAddVehicle = "Vehicles - Add Vehicle";
        public const string VehiclesEditVehicle = "Vehicles - Edit Vehicle";
        public const string VehiclesDeleteVehicle = "Vehicles - Delete Vehicle";
        public const string ProductsShowProductTypes = "Products - Show Product Types";
        public const string ProductsAddProductType = "Products - Add Product Type";
        public const string ProductsEditProductType = "Products - Edit Product Type";
        public const string ProductsDeleteProductType = "Products - Delete Product Type";
        public const string ProductsShowServices = "Products - Show Services";
        public const string ProductsAddService = "Products - Add Service";
        public const string ProductsEditService = "Products - Edit Service";
        public const string ProductsDeleteService = "Products - Delete Service";
        public const string ProductsShowSizes = "Products - Show Sizes";
        public const string ProductsAddSize = "Products - Add Size";
        public const string ProductsEditSize = "Products - Edit Size";
        public const string ProductsDeleteSize = "Products - Delete Size";
        public const string ProductsShowTimeSlots = "Products - Show Time Slots";
        public const string ProductsAddTimeSlot = "Products - Add Time Slot";
        public const string ProductsEditTimeSlot = "Products - Edit Time Slot";
        public const string ProductsDeleteTimeSlot = "Products - Delete Time Slot";
        public const string ProductsShowAddOnPrices = "Products - Show Addon Prices";
        public const string PromotionsShowCharges = "Promotions - Show Charges";
        public const string PromotionsAddCharge = "Promotions - Add Charge";
        public const string PromotionsEditCharge = "Promotions - Edit Charge";
        public const string PromotionsDeleteCharge = "Promotions - Delete Charge";
        public const string PromotionsShowCodOptions = "Promotions - Show COD Options";
        public const string PromotionsAddCodOption = "Promotions - Add COD Option";
        public const string PromotionsEditCodOption = "Promotions - Edit COD Option";
        public const string PromotionsDeleteCodOption = "Promotions - Delete COD Option";
        public const string NotificationsForDrivers = "Notifications - Notifications for Drivers";
        public const string NotificationsSendToDrivers = "Notifications - Send Notifications to Drivers";
        public const string NotificationsForUsers = "Notifications - Notifications for Users";
        public const string NotificationsSendToUsers = "Notifications - Send Notifications to Users";
        public const string UsersShowUsers = "Users - Show Users";
        public const string UsersEditUser = "Users - Edit User";
        public const string UsersAccessRights = "Users - Access Rights";
        public const string UsersShowRoles = "Users - Show Roles";
        public const string UsersAddRole = "Users - Add Role";
        public const string UsersEditRole = "Users - Edit Role";
        public const string UsersDeleteRole = "Users - Delete Role";
        public const string CountriesShowCountries = "Countries - Show Countries";
        public const string CountriesAddCountry = "Countries - Add Country";
        public const string CountriesEditCountry = "Countries - Edit Country";
        public const string CountriesDeleteCountry = "Countries - Delete Country";
        public const string PostalCodesShowPostals = "Postal Codes - Show Postals";
        public const string PostalCodesAddPostal = "Postal Codes - Add Postal";
        public const string PostalCodesEditPostal = "Postal Codes - Edit Postal";
        public const string PostalCodesDeletePostal = "Postal Codes - Delete Postal";
        public const string PostalCodesUpdateGeolocation = "Postal Codes - Update Geolocation";
        public const string SettingsShowSettings = "Settings - Show Settings";
        public const string SettingsEditSetting = "Settings - Edit Setting";

        public static int AccessRightsIndex = 8;
        public static string[] RoleNames =
        {
            //-------- THESE ARE ROLES -----------------
            AdministratorRole,
            CustomerRole,
            StaffDriverRole,
            ContractorDriverRole,
            ManagerRole,
            SalesPersonRole,
            VendorRole,
            MerchantRole,
            //--------- THESE ARE ACCESS RIGHTS ----------------
            BasicDashboard,
            AdvancedDashboard,
            BasicOrderList,
            EditOrderPrice,
            NoDetailOrderList,
            ShowCustomersWithLimitation,
            OrdersShowOrders,
            OrdersAddOrder,
            OrdersEditOrder,
            OrdersDeleteOrder,
            OrdersExportOrders,
            OrdersBulkUpdate,
            OrdersBulkExportVrp,
            OrdersBulkPrintQr,
            OrdersShowOrderEvents,
            OrdersShowTodayOrders,
            OrdersExportTodayOrders,
            OrdersDeleteTodayOrder,
            OrdersShowInvoicesReport,
            OrdersInvoicesReportPrintInvoice,
            OrdersInvoicesReportSendInvoice,
            OrdersExportInvoicesReport,
            OrdersShowFailedOrders,
            OrdersDeleteFailedOrder,
            OrdersEditFailedOrder,
            OrdersExportFailedOrders,
            OrdersShowRePickupRequests,
            OrdersEditRePickupRequest,
            OrdersDeleteRePickupRequest,
            OrdersExportRePickupRequests,
            OrdersShowReDeliveryRequests,
            OrdersEditReDeliveryRequest,
            OrdersDeleteReDeliveryRequest,
            OrdersExportReDeliveryRequests,
            OrdersShowSalesReport,
            OrdersExportSalesReport,
            OrdersShowManagementReport,
            OrdersShowOrderDetail,
            DispatchShowTodayJobs,
            DispatchShowSpecialJobs,
            DispatchDeleteJob,
            DispatchImportJobs,
            DispatchPrintPlan,
            DispatchPrintSpecialPlan,
            DispatchShowJobsReport,
            DispatchTrackDriversLocation,
            DispatchDriverHistory,
            ReportsSalesDeliveryReport,
            ReportsExportSalesDeliveryReport,
            ReportsSalesServiceReport,
            ReportsExportSalesServiceReport,
            ReportsFailedDeliveryReport,
            ReportsExportFailedDeliveryReport,
            ReportsCodReport,
            ReportsExportCodReport,
            ReportsJobHistoryReport,
            ReportsOpsDeliverySummary,
            ReportsExportOpsDeliverySummary,
            ReportsOpsPickupSummary,
            ReportsExportOpsPickupSummary,
            ReportsBillingSummary,
            ReportsExportBillingSummary,
            ReportsBillingReport,
            ReportsExportBillingReport,
            DriversShowDrivers,
            DriversAddDriver,
            DriversEditDriver,
            DriversDeleteDriver,
            DriversExportDrivers,
            DriversShowPayScheme,
            DriversAddPayScheme,
            DriversEditPayScheme,
            DriversDeletePayScheme,
            DriversExportPaySchemes,
            DriversShowDriverHistory,
            DriversDeleteDriverHistory,
            DriversExportDriverHistory,
            DriversShowDriverPayslip,
            DriversEmailDriverPayslip,
            CustomersShowCustomers,
            CustomersAddCustomer,
            CustomersEditCustomer,
            CustomersDeleteCustomer,
            CustomersExportCustomers,
            CustomersShowPaymentTypes,
            CustomersAddPaymentType,
            CustomersEditPaymentType,
            CustomersDeletePaymentType,
            CustomersShowPaymentStatus,
            CustomersEditPaymentStatus,
            CustomersExportPaymentStatus,
            CustomersAddRefund,
            CustomersShowRefundStatus,
            CustomersEditRefundStatus,
            CustomersExportRefundStatus,
            VehiclesShowVehicles,
            VehiclesAddVehicle,
            VehiclesEditVehicle,
            VehiclesDeleteVehicle,
            ProductsShowProductTypes,
            ProductsAddProductType,
            ProductsEditProductType,
            ProductsDeleteProductType,
            ProductsShowServices,
            ProductsAddService,
            ProductsEditService,
            ProductsDeleteService,
            ProductsShowSizes,
            ProductsAddSize,
            ProductsEditSize,
            ProductsDeleteSize,
            ProductsShowTimeSlots,
            ProductsAddTimeSlot,
            ProductsEditTimeSlot,
            ProductsDeleteTimeSlot,
            ProductsShowAddOnPrices,
            PromotionsShowCharges,
            PromotionsAddCharge,
            PromotionsEditCharge,
            PromotionsDeleteCharge,
            PromotionsShowCodOptions,
            PromotionsAddCodOption,
            PromotionsEditCodOption,
            PromotionsDeleteCodOption,
            NotificationsForDrivers,
            NotificationsSendToDrivers,
            NotificationsForUsers,
            NotificationsSendToUsers,
            UsersShowUsers,
            UsersEditUser,
            UsersAccessRights,
            UsersShowRoles,
            UsersAddRole,
            UsersEditRole,
            UsersDeleteRole,
            CountriesShowCountries,
            CountriesAddCountry,
            CountriesEditCountry,
            CountriesDeleteCountry,
            PostalCodesShowPostals,
            PostalCodesAddPostal,
            PostalCodesEditPostal,
            PostalCodesDeletePostal,
            PostalCodesUpdateGeolocation,
            SettingsShowSettings,
            SettingsEditSetting
        };

        private static ApplicationSignInManager _signInManager;
        private static ApplicationUserManager _userManager;

        public static ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public static ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        public const int DefaultExpireTime = 6 * 60; // In minutes
        public const int DefaultLongLiveCookie = 30 * 60 * 60; // In minutes

        public static async Task<dynamic> Authenticate(LoginViewModel model)
        {
            var db = new CourierDBEntities();

            if (model == null)
            {
                return new
                       {
                           Code = 100,
                           Message = "Invalid parameters"
                       };
            }
            if (string.IsNullOrEmpty(model.UserName))
            {
                return new
                       {
                           Code = 100,
                           Message = "User Name is required"
                       };
            }
            if (string.IsNullOrEmpty(model.Password))
            {
                return new
                       {
                           Code = 100,
                           Message = "Password is required"
                       };
            }

            var userProfile = db.UserProfiles.FirstOrDefault(x => x.UserName.Equals(model.UserName, StringComparison.OrdinalIgnoreCase));
            if (userProfile != null && userProfile.IsActive == false)
            {
                return new
                       {
                           Code = 100,
                           Message = "Your account is deactivated"
                       };
            }

            var userIdentity = await UserManager.FindAsync(model.UserName, model.Password);
            if (userIdentity != null)
            {
                var expiredTime = SettingProvider.DefaultTokenExpireTime;
                if (expiredTime <= 0)
                {
                    expiredTime = DefaultExpireTime;
                }

                var claims = new List<Claim>
                             {
                                 new Claim(ClaimTypes.Name, userIdentity.UserName),
                                 new Claim(ClaimTypes.NameIdentifier, userIdentity.Id)
                             };

                var roles = await UserManager.GetRolesAsync(userIdentity.Id);
                claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

                if (!string.IsNullOrEmpty(userIdentity.Email))
                {
                    claims.Add(new Claim(ClaimTypes.Email, userIdentity.Email));
                }
                var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

                var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                var currentUtc = new SystemClock().UtcNow;
                ticket.Properties.IssuedUtc = currentUtc;
                if (model.RememberMe)
                {
                    ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromMinutes(DefaultLongLiveCookie));
                }
                else
                {
                    ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromMinutes(expiredTime));
                }
                ticket.Properties.IsPersistent = model.RememberMe;
                var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);

                var ctx = HttpContext.Current.Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;
                authenticationManager.SignIn(ticket.Properties, identity);

                var token = new Token()
                            {
                                UserName = model.UserName,
                                CreatedOn = currentUtc.DateTime,
                                CreatedBy = HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : "",
                                ExpiredOn = ticket.Properties.ExpiresUtc.Value.DateTime,
                                TokenKey = accessToken,
                                IsExpired = false,
                            };
                db.Tokens.Add(token);

                try
                {
                    db.SaveChanges();

                    return new
                           {
                               Code = 0,
                               Token = token.TokenKey,
                               ExpiredOn = token.ExpiredOn,
                               Roles = await UserManager.GetRolesAsync(userIdentity.Id)
                           };
                }
                catch (Exception exc)
                {
                    LogHelper.Log(exc);
                    return new
                           {
                               Code = 500,
                               Message = "Errors occur in processing request. Please contact to administrator to resolve."
                           };
                }
            }
            else
            {
                return new
                       {
                           Code = 200,
                           Message = "Login failed. Please check your username and password and try again."
                       };
            }
        }

        public static async Task<dynamic> SignOut()
        {
            var ctx = HttpContext.Current.Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut();

            return new
                   {
                       Code = 0
                   };
        }

        public static async Task<dynamic> Profile(string username, IList<string> roles)
        {
            var db = new CourierDBEntities();

            var profiles = new List<dynamic>();
            foreach (var role in roles)
            {
                switch (role)
                {
                    case CustomerRole:
                        var customer = (from c in db.Customers
                                        from user in db.AspNetUsers
                                        where !c.IsDeleted && c.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                              && user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                        select new
                                               {
                                                   c.Credit,
                                                   Address = c.CustomerAddress,
                                                   Company = c.CustomerCompany,
                                                   Country = c.CustomerCountry,
                                                   Name = c.CustomerName,
                                                   Mobile = c.CustomerPhone,
                                                   ZipCode = c.CustomerZipCode,
                                                   Photo = c.CustomerPhoto,
                                                   c.Id,
                                                   c.PaymentTypeId,
                                                   c.PaymentType.PaymentTypeName,
                                                   c.IsActive,
                                                   user.Email,
                                                   IsDriver = false
                                               })
                            .FirstOrDefault();
                        if (customer != null)
                        {
                            profiles.Add(
                                         new
                                         {
                                             ProfileType = CustomerRole,
                                             Profile = customer
                                         });
                        }
                        break;
                    case AdministratorRole:
                        var userObj = db.AspNetUsers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                        if (userObj != null)
                        {
                            profiles.Add(
                                         new
                                         {
                                             ProfileType = AdministratorRole,
                                             Profile = new
                                                       {
                                                           userObj.Id,
                                                           userObj.Email,
                                                           IsDriver = false
                                                       }
                                         });
                        }
                        break;
                    case ContractorDriverRole:
                        var contractor = (from d in db.Contractors
                                          from user in db.AspNetUsers
                                          where !d.IsDeleted && d.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                                && user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                          select new
                                                 {
                                                     d.Id,
                                                     user.Email,
                                                     Address = d.ContractorAddress,
                                                     Country = d.ContractorCountry,
                                                     Mobile = d.ContractorMobile,
                                                     Name = d.ContractorName,
                                                     Photo = d.ContractorPhoto,
                                                     d.DrivingLicense,
                                                     d.ICNumber,
                                                     d.VehicleNumber,
                                                     d.VehicleType,
                                                     IsDriver = true
                                                 })
                            .FirstOrDefault();
                        if (contractor != null)
                        {
                            profiles.Add(
                                         new
                                         {
                                             ProfileType = ContractorDriverRole,
                                             Profile = new
                                                       {
                                                           contractor.Id,
                                                           contractor.Email,
                                                           contractor.Address,
                                                           contractor.Country,
                                                           contractor.Mobile,
                                                           contractor.Name,
                                                           Photo = contractor.Photo.ToAbsoluteUrl(),
                                                           contractor.DrivingLicense,
                                                           contractor.ICNumber,
                                                           contractor.VehicleNumber,
                                                           contractor.VehicleType,
                                                           contractor.IsDriver
                                                       }
                                         });
                        }
                        break;
                    case StaffDriverRole:
                        var staff = (from d in db.Staffs
                                     from user in db.AspNetUsers
                                     where !d.IsDeleted && d.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                           && user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                     select new
                                            {
                                                d.Id,
                                                user.Email,
                                                Address = d.StaffAddress,
                                                Country = d.StaffCountry,
                                                StaffNumber = d.StaffIdentityNumber,
                                                Mobile = d.StaffMobile,
                                                Name = d.StaffName,
                                                Photo = d.StaffPhoto,
                                                d.DrivingLicense,
                                                d.ICNumber,
                                                IsDriver = true
                                            })
                            .FirstOrDefault();
                        if (staff != null)
                        {
                            profiles.Add(
                                         new
                                         {
                                             ProfileType = StaffDriverRole,
                                             Profile = new
                                                       {
                                                           staff.Id,
                                                           staff.Email,
                                                           staff.Address,
                                                           staff.Country,
                                                           staff.Mobile,
                                                           staff.Name,
                                                           Photo = staff.Photo.ToAbsoluteUrl(),
                                                           staff.DrivingLicense,
                                                           staff.ICNumber,
                                                           staff.StaffNumber,
                                                           staff.IsDriver
                                                       }
                                         });
                        }
                        break;
                }
            }

            return profiles;
        }

        public static async Task<dynamic> Update(string userId, string username, IList<string> roles, UpdateViewModel model)
        {
            var db = new CourierDBEntities();

            var user = await UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                return new
                       {
                           Code = 404,
                           Message = "User does not exists"
                       };
            }

            IdentityResult result;
            // Change password if need
            if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.NewPassword))
            {
                result = await UserManager.ChangePasswordAsync(userId, model.Password, model.NewPassword);
                if (!result.Succeeded)
                {
                    return new
                           {
                               Code = 500,
                               Message = string.Join(" | ", result.Errors)
                           };
                }
            }

            // Change email if need
            if (!string.IsNullOrEmpty(model.Email) && !user.Email.Equals(model.Email, StringComparison.OrdinalIgnoreCase))
            {
                user.Email = model.Email;
                result = await UserManager.UpdateAsync(user);
                if (!result.Succeeded)
                {
                    return new
                           {
                               Code = 500,
                               Message = string.Join(" | ", result.Errors)
                           };
                }
            }

            foreach (var role in roles)
            {
                switch (role)
                {
                    case CustomerRole:
                        var customer = (from c in db.Customers
                                        where c.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                        select c).FirstOrDefault();
                        if (customer != null)
                        {
                            customer.CustomerName = model.Name;
                            customer.CustomerCountry = model.CountryCode;
                            customer.CustomerPhone = model.MobileNumber;
                            customer.CustomerCompany = model.Company;
                            customer.CustomerAddress = model.Address;
                            customer.CustomerZipCode = model.PostalCode;
                            if (!string.IsNullOrEmpty(model.Photo))
                            {
                                customer.CustomerPhoto = model.Photo;
                            }
                            customer.UpdatedOn = DateTime.Now;
                            customer.UpdatedBy = username;
                        }
                        break;
                    case ContractorDriverRole:
                        var contractor = (from d in db.Contractors
                                          where d.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                          select d).FirstOrDefault();
                        if (contractor != null)
                        {
                            contractor.ContractorCountry = model.CountryCode;
                            contractor.ContractorName = model.Name;
                            contractor.ContractorMobile = model.MobileNumber;
                            if (!string.IsNullOrEmpty(model.Photo))
                            {
                                contractor.ContractorPhoto = model.Photo;
                            }
                            contractor.UpdatedOn = DateTime.Now;
                            contractor.UpdatedBy = username;
                        }
                        break;
                    case StaffDriverRole:
                        var staff = (from d in db.Staffs
                                     where d.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                                     select d).FirstOrDefault();
                        if (staff != null)
                        {
                            staff.StaffCountry = model.CountryCode;
                            staff.StaffName = model.Name;
                            staff.StaffMobile = model.MobileNumber;
                            if (!string.IsNullOrEmpty(model.Photo))
                            {
                                staff.StaffPhoto = model.Photo;
                            }
                            staff.UpdatedOn = DateTime.Now;
                            staff.UpdatedBy = username;
                        }
                        break;
                }
            }

            try
            {
                db.SaveChanges();
                return new
                       {
                           Code = 0,
                           Message = "Your account is updated successfully."
                       };
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return new
                       {
                           Code = 500,
                           Message = "Errors occur in processing request. Please contact to administrator to resolve."
                       };
            }
        }

        public static async Task<dynamic> UpdateNotificationToken(string username, IList<string> roles, NotificationTokenViewModel model)
        {
            var db = new CourierDBEntities();

            foreach (var role in roles)
            {
                switch (role)
                {
                    case CustomerRole:
                        var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                        if (customer != null)
                        {
                            customer.NotificationKey = model.Token;
                            customer.PhoneType = (int) model.PhoneType;
                            customer.UpdatedOn = DateTime.Now;
                            customer.UpdatedBy = username;
                        }
                        break;
                    case AdministratorRole:
                        break;
                    case ContractorDriverRole:
                        var contractor = db.Contractors.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                        if (contractor != null)
                        {
                            contractor.NotificationKey = model.Token;
                            contractor.PhoneType = (int) model.PhoneType;
                            contractor.UpdatedOn = DateTime.Now;
                            contractor.UpdatedBy = username;
                        }
                        break;
                    case StaffDriverRole:
                        var staff = db.Staffs.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                        if (staff != null)
                        {
                            staff.NotificationKey = model.Token;
                            staff.PhoneType = (int) model.PhoneType;
                            staff.UpdatedOn = DateTime.Now;
                            staff.UpdatedBy = username;
                        }
                        break;
                }
            }

            try
            {
                db.SaveChanges();
                return new
                       {
                           Code = 0,
                           Message = "Your account is updated successfully."
                       };
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return new
                       {
                           Code = 500,
                           Message = "Errors occur in processing request. Please contact to administrator to resolve."
                       };
            }
        }

        public static async Task<dynamic> SendEmail(string username, string title, string content)
        {
            var user = await UserManager.FindByNameAsync(username);
            if (user == null) // || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                // Don't reveal that the user does not exist or is not confirmed
                return new
                       {
                           Code = 404,
                           Message = "Cannot find this user or user's email is not verified."
                       };
            }

            await UserManager.SendEmailAsync(user.Id, title, content);
            return new
                   {
                       Code = 0
                   };
        }

        public static async Task<dynamic> SendSms(string username, string content)
        {
            var user = await UserManager.FindByNameAsync(username);
            if (user == null) // || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                // Don't reveal that the user does not exist or is not confirmed
                return new
                       {
                           Code = 404,
                           Message = "Cannot find this user or user's email is not verified."
                       };
            }

            await UserManager.SendSmsAsync(user.Id, content);
            return new
                   {
                       Code = 0
                   };
        }

        public static dynamic SendEmailSync(string username, string title, string content)
        {
            var user = UserManager.FindByName(username);
            if (user == null) // || !(UserManager.IsEmailConfirmed(user.Id)))
            {
                // Don't reveal that the user does not exist or is not confirmed
                return new
                       {
                           Code = 404,
                           Message = "Cannot find this user or user's email is not verified."
                       };
            }

            UserManager.SendEmail(user.Id, title, content);
            return new
                   {
                       Code = 0
                   };
        }

        public static dynamic SendSmsSync(string username, string content)
        {
            var user = UserManager.FindByName(username);
            if (user == null) // || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                // Don't reveal that the user does not exist or is not confirmed
                return new
                       {
                           Code = 404,
                           Message = "Cannot find this user or user's email is not verified."
                       };
            }

            UserManager.SendSms(user.Id, content);
            return new
                   {
                       Code = 0
                   };
        }

        public static async Task<bool> SendSmsService(string number, string content)
        {
            if(string.IsNullOrEmpty(number) || string.IsNullOrEmpty(content))
            {
                return false;
            }

            try
            {
                var db = new CourierDBEntities();

                var smsMessage = new SmsMessage();
                smsMessage.CreatedOn = DateTime.Now;
                smsMessage.PhoneNumber = number;
                smsMessage.Content = content;

                db.SmsMessages.Add(smsMessage);
                await db.SaveChangesAsync();

                return true;               
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return false;
            }
        }
    }
}
