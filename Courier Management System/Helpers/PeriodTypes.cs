﻿using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Helpers
{
    public enum PeriodTypes
    {
        [Display(Name = "In 1 month")]
        Month,
        [Display(Name = "In 1 quarter")]
        Quarter,
        [Display(Name = "In 1 year")]
        Year,
        [Display(Name = "When meet")]
        WhenMeet
    }
}
