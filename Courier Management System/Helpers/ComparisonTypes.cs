﻿using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Helpers
{
    public enum ComparisonTypes
    {
        [Display(Name = "Less than limit value")]
        LessThan,
        [Display(Name = "Less than or Equal to limit value")]
        LessThanOrEqualTo,
        [Display(Name = "Equal to limit value")]
        EqualTo,
        [Display(Name = "Greater than limit value")]
        GreaterThan,
        [Display(Name = "Greater than or Equal to limit value")]
        GreaterThanOrEqualTo
    }
}
