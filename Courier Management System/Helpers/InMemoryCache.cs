﻿using System;
using System.Runtime.Caching;

namespace Courier_Management_System.Helpers
{
    public static class InMemoryCache
    {
        public static T GetOrSet<T>(string cacheKey, Func<T> getItemCallback)
        {
            T item;
            try
            {
                item = (T) MemoryCache.Default.Get(cacheKey);
            }
            catch (Exception exc)
            {
                item = default(T);
            }
            
            if (item != null)
            {
                return item;
            }

            item = getItemCallback();
            MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(60));
            return item;
        }

        public static void Set(string cacheKey, object value)
        {
            if (MemoryCache.Default.Contains(cacheKey))
            {
                MemoryCache.Default[cacheKey] = value;
            }
            else
            {
                MemoryCache.Default.Add(cacheKey, value, DateTime.Now.AddMinutes(60));
            }
        }

        public static void ClearCache()
        {
            MemoryCache.Default.Trim(100);
        }
    }
}
