﻿using System;
using System.Web;
using DevExpress.Web;

namespace Courier_Management_System.Helpers
{
    public static class PathHelper
    {
        public static UploadControlValidationSettings UploadSettings = new UploadControlValidationSettings
        {
            AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".gif", ".png" },
            MaxFileSize = 4194304
        };

        public const string CustomerUploadBaseDirectory = "~/Uploads/Customers/Photos/";
        public const string CountryUploadBaseDirectory = "~/Uploads/Countries/";
        public const string DriverUploadBaseDirectory = "~/Uploads/Drivers/Photos/";
        public const string OrderUploadBaseDirectory = "~/Uploads/Orders/";
        public const string InventoryUploadBaseDirectory = "~/Uploads/Inventory/";
        public const string StickersDirectory = "Stickers/";
        public const string SignaturesDirectory = "Signatures/";
        public const string ProofsDirectory = "Proofs/";
        public const string BarCodesDirectory = "BarCodes/";

        public static string ToAbsoluteUrl(this string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
            {
                return relativeUrl;
            }

            if (HttpContext.Current == null)
            {
                return relativeUrl;
            }

            if (relativeUrl.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ||
                relativeUrl.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                return relativeUrl;
            }

            if (relativeUrl.StartsWith("/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~");
            }
            if (!relativeUrl.StartsWith("~/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~/");
            }

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return $"{url.Scheme}://{url.Host}{port}{VirtualPathUtility.ToAbsolute(relativeUrl)}";
        }
    }
}
