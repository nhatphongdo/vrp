﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Courier_Management_System.Helpers
{
    public static class SettingProvider
    {
        [Description("This is time to expire generated secured token, in seconds")]
        public static int DefaultTokenExpireTime
        {
            get
            {
                return GetPropertyValue<int>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Integer);
            }
        }

        [Description("Minimum Order Amount")]
        public static decimal MinimumOrderAmount
        {
            get
            {
                return GetPropertyValue<decimal>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Decimal);
            }
        }

        [Description("Close Window Time, in minutes")]
        public static double CloseWindowTime
        {
            get
            {
                return GetPropertyValue<double>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Float);
            }
        }

        [Description("Default Service Time, in minutes")]
        public static double ServiceTime
        {
            get
            {
                return GetPropertyValue<double>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Float);
            }
        }

        [Description("VRP System Url")]
        public static string VrpLink
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value, PropertyTypes.String);
            }
        }

        [Description("Host address for SMTP service")]
        public static string SmtpHostAddress
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Port for SMTP service")]
        public static int SmtpHostPort
        {
            get
            {
                return GetPropertyValue<int>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Integer);
            }
        }

        [Description("Username for SMTP service")]
        public static string SmtpUsername
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Password for SMTP service")]
        public static string SmtpPassword
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Use secured connection for SMTP service")]
        public static bool SmtpSecured
        {
            get
            {
                return GetPropertyValue<bool>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Boolean);
            }
        }

        [Description("Sender's email address")]
        public static string SmtpSenderEmail
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Sender's name")]
        public static string SmtpSenderName
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Terms and Conditions link")]
        public static string TermsAndConditionsUrl
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Company info link")]
        public static string CompanyInfoUrl
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("News link")]
        public static string NewsUrl
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Type of Service link")]
        public static string TypeOfServiceUrl
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Service Guidelines link")]
        public static string ServiceGuidelinesUrl
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Google GCM Server Key")]
        public static string GcmServerKey
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Braintree Merchant Id Key")]
        public static string BrainTreeMerchantId
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Braintree Public Key")]
        public static string BrainTreePublicKey
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Braintree Private Key")]
        public static string BrainTreePrivateKey
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Paypal Client Id Key")]
        public static string PaypalClientId
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Paypal Secret Key")]
        public static string PaypalSecretKey
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("Google Map Web Client Key")]
        public static string GoogleClientKey
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.String);
            }
        }

        [Description("ID of Standard Document service, used for importing Bulk Order")]
        public static int StandardDocumentServiceId
        {
            get
            {
                return GetPropertyValue<int>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Integer);
            }
        }

        [Description("ID of Standard Parcel service, used for importing Bulk Order")]
        public static int StandardParcelServiceId
        {
            get
            {
                return GetPropertyValue<int>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Integer);
            }
        }

        [Description("ID of Schedule Parcel service, used for importing Bulk Order")]
        public static int ScheduleParcelServiceId
        {
            get
            {
                return GetPropertyValue<int>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Integer);
            }
        }

        [Description("Out-Of-Service Days")]
        public static string OutOfServiceDays
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.DateArray);
            }
        }

        [Description("Operators' Email")]
        public static string OperatorsEmail
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value, PropertyTypes.String);
            }
        }

        [Description("Pickup Requests")]
        public static string PickupRequests
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value, PropertyTypes.StringArray);
            }
        }

        [Description("Delivery Requests")]
        public static string DeliveryRequests
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value, PropertyTypes.StringArray);
            }
        }

        [Description("Failed Pickup Reasons")]
        public static string FailedPickupReasons
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value, PropertyTypes.StringArray);
            }
        }

        [Description("Failed Delivery Reasons")]
        public static string FailedDeliveryReasons
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value, PropertyTypes.StringArray);
            }
        }

        [Description("Exchange Order charge")]
        public static double ExchangeOrderCharge
        {
            get
            {
                return GetPropertyValue<double>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Float);
            }
        }

        [Description("Radius of Driver locations view")]
        public static double DriverLocationsRadius
        {
            get
            {
                return GetPropertyValue<double>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value.ToString(), PropertyTypes.Float);
            }
        }

        [Description("Postal Code of Depot")]
        public static string DepotPostalCode
        {
            get
            {
                return GetPropertyValue<string>(MethodBase.GetCurrentMethod().Name);
            }
            set
            {
                SetPropertyValue(MethodBase.GetCurrentMethod().Name, value, PropertyTypes.String);
            }
        }

        public static List<string> GetProperties()
        {
            // Get properties through Reflections
            var type = typeof(SettingProvider);
            var result = type.GetProperties(BindingFlags.Public | BindingFlags.Static).Select(x => x.Name).ToList();

            return result;
        }

        public static void ClearCache()
        {
            InMemoryCache.ClearCache();
        }

        public static string GetPropertyDescription(string name)
        {
            var type = typeof(SettingProvider);
            var result = type.GetProperties(BindingFlags.Public | BindingFlags.Static).ToList();
            foreach (var item in result)
            {
                if (item.Name == name)
                {
                    var attrs = item.GetCustomAttributes(typeof(DescriptionAttribute), true);
                    foreach (var attr in attrs)
                    {
                        return ((DescriptionAttribute) attr).Description;
                    }
                }
            }

            return "";
        }

        private static T GetPropertyValue<T>(string name) where T : IConvertible
        {
            if (name.StartsWith("get_"))
            {
                name = name.Substring(4);
            }
            var str = InMemoryCache.GetOrSet<string>(name, () =>
            {
                using (var context = new CourierDBEntities())
                {
                    var value = context.Settings.Where(x => x.Name == name)
                        .Select(x => x.Value)
                        .FirstOrDefault();

                    return value ?? "";
                }
            });

            try
            {
                return (T) Convert.ChangeType(str, typeof(T));
            }
            catch (Exception exc)
            {
                return default(T);
            }
        }

        private static void SetPropertyValue(string name, string value, PropertyTypes type)
        {
            if (name.StartsWith("set_"))
            {
                name = name.Substring(4);
            }
            using (var context = new CourierDBEntities())
            {
                var setting = context.Settings.FirstOrDefault(x => x.Name == name);

                if (setting == null)
                {
                    setting = new Setting()
                    {
                        Name = name,
                        CreatedOn = DateTime.Now,
                        CreatedBy = HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : "",
                        Type = (int) type
                    };
                    context.Settings.Add(setting);
                }

                if (setting.Type == (int) type)
                {
                    setting.Value = value;
                    setting.UpdatedOn = DateTime.Now;
                    setting.UpdatedBy = HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : "";
                }

                try
                {
                    context.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogHelper.Log(exc);
                }

                InMemoryCache.Set(name, value);
            }
        }
    }

    public enum PropertyTypes
    {
        Integer,
        Float,
        Boolean,
        String,
        DateTime,
        Decimal,
        DateArray,
        StringArray
    }
}
