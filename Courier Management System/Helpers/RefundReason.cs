﻿namespace Courier_Management_System.Helpers
{
    public enum PaymentCategory
    {
        None = 0,
        Refund = 1,
        ExtraCharge = 2
    }
}