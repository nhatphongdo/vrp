﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Courier_Management_System.Helpers
{
    public enum ExtraRequestType
    {
        DeliveryRequest,
        PickupRequest,
        ReDeliveryRequest,
        RePickupRequest
    }
}