//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Courier_Management_System
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pricing
    {
        public int ProductTypeId { get; set; }
        public int ServiceId { get; set; }
        public int SizeId { get; set; }
        public decimal ChargedCost { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual ProductType ProductType { get; set; }
        public virtual Service Service { get; set; }
        public virtual Size Size { get; set; }
    }
}
