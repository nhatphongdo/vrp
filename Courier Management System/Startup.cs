﻿using System;
using Courier_Management_System.Helpers;
using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.Dashboard;

[assembly : OwinStartupAttribute(typeof(Courier_Management_System.Startup))]

namespace Courier_Management_System
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            // Start background job
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard("/hangfire",
                                     new DashboardOptions
                                     {
                                         AuthorizationFilters = new[] { new AuthorizationFilter { Roles = AccountHelper.AdministratorRole } }
                                     });
            app.UseHangfireServer();

            var time = System.Configuration.ConfigurationManager.AppSettings["DailySmsTime"];
            if (string.IsNullOrEmpty(time))
            {
                time = "08:00";
            }
            var parts = time.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            var hour = 0;
            var minute = 0;
            if (!int.TryParse(parts[0], out hour) || hour < 0 || hour > 23)
            {
                hour = 8;
            }
            if (parts.Length >= 2)
            {
                if (!int.TryParse(parts[1], out minute) || minute < 0 || minute > 59)
                {
                    minute = 0;
                }
            }

            RecurringJob.AddOrUpdate(() => OrderHelper.SendDailyDeliverySms(), Cron.Daily(hour, minute), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate(() => ScheduleJobHelper.SendSmsService(), Cron.Minutely,TimeZoneInfo.Local);
            //DbInterception.Add(new FtsInterceptor());
        }
    }
}