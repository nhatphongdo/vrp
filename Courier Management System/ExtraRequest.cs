//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Courier_Management_System
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExtraRequest
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public long OrderId { get; set; }
        public int RequestType { get; set; }
        public string RequestContent { get; set; }
        public Nullable<System.DateTime> RequestDate { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVisited { get; set; }
    
        public virtual Order Order { get; set; }
    }
}
