﻿var currentServiceTime = null;
var firstTime = 0;
$(function () { 

    $("input.number-spinner").each(function () {
        $(this).TouchSpin({
            min: $(this).data('min-value') || 0,
            max: $(this).data('max-value') || 1000000000,
            step: $(this).data('step') || 1,
            maxboostedstep: $(this).data('max-boosted-step') || 1000,
            prefix: $(this).data('prefix') || '',
            postfix: $(this).data('postfix') || '',
            decimals: $(this).data('decimals') || 0
        });
    });

    $('#FromZipCode').on('change', onCheckFromZipCode);
    $('#ToZipCode').on('change', onCheckToZipCode);

    $("#CustomerId").select2({
        theme: "classic",
        ajax: {
            url: findCustomersLink,
            type: 'POST',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                return {
                    results: data
                };
            },
            cache: true
        },
        placeholder: "Find a Customer by Name, Company or Address",
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        templateResult: function (item) {
            if (item.loading) {
                return item.text;
            }

            var markup = '<div class="clearfix">' +
                '<div class="col-md-3">' + (item.CustomerName || "") + '</div>' +
                '<div class="col-md-3">' + (item.CustomerCompany || "") + '</div>' +
                '<div class="col-md-6">' + (item.CustomerAddress || "") + '</div>' +
                '</div>';

            return markup;
        },
        templateSelection: function (item) {
            if (item.id) {
                if (item.text) {
                    return item.text;
                }

                if (!$('#FromName').val() && !$('#FromAddress').val()) {
                    $('#FromName').val(item.CustomerName);
                    $('#FromAddress').val(item.CustomerAddress);
                }

                return (item.CustomerName + (item.CustomerCompany ? (" / " + item.CustomerCompany) : "") + " (" + item.CustomerAddress + ")");
            } else {
                return item.text;
            }
        }
    });

    $('#ServiceId').on('change', function () {
        updatePrice();

        $.ajax({
            url: '/api/orders/timeoptions/' + $(this).val(),
            type: 'POST',
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + getCookie("CdsAccessToken"));
            },
            success: function (result) {
                currentServiceTime = result;
                $('#PickupTimeSlotId').html("");
                PickupDate.SetMinDate(null);
                PickupDate.SetMaxDate(null);
                DeliveryDate.SetMinDate(null);
                DeliveryDate.SetMaxDate(null);
                $('#DeliveryTimeSlotId').html("");

                if (result.IsPickupTimeAllow == true) {
                    $('.is-pickup-time-allow').show();
                } else {
                    $('.is-pickup-time-allow').hide();
                }

                if (result.IsPickupDateAllow == true) {
                    $('.is-pickup-date-allow').show();
                } else {
                    $('.is-pickup-date-allow').hide();
                }

                if (result.IsDeliveryTimeAllow == true) {
                    $('.is-delivery-time-allow').show();
                }
                else {
                    $('.is-delivery-time-allow').hide();
                }

                if (result.IsDeliveryDateAllow == true) {
                    $('.is-delivery-date-allow').show();
                } else {
                    $('.is-delivery-date-allow').hide();
                }

                for (var i = 0; i < result.AvailablePickupTimes.length; i++) {
                    $('<option>')
                        .attr("value", result.AvailablePickupTimes[i].Value)
                        .text(result.AvailablePickupTimes[i].Text)
                        .appendTo($('#PickupTimeSlotId'));
                }
                $('#PickupTimeSlotId').val($('#PickupTimeSlotId').data("val"));
                var pickupMinDate, pickupMaxDate;
                var range = result.AvailablePickupDateRange ? result.AvailablePickupDateRange.split(',') : [];
                if (range.length == 2) {
                    pickupMinDate = new Date();
                    //pickupMinDate.setDate(pickupMinDate.getDate() - 1 + parseInt(range[0]) || 0);
                    //PickupDate.SetMinDate(pickupMinDate);
                    //pickupMaxDate = new Date();
                    //pickupMaxDate.setDate(pickupMaxDate.getDate() + parseInt(range[1]) || 0);
                    //PickupDate.SetMaxDate(pickupMaxDate);
                }

                for (var i = 0; i < result.AvailableDeliveryTimes.length; i++) {
                    $('<option>')
                        .attr("value", result.AvailableDeliveryTimes[i].Value)
                        .text(result.AvailableDeliveryTimes[i].Text)
                        .appendTo($('#DeliveryTimeSlotId'));
                }
                $('#DeliveryTimeSlotId').val($('#DeliveryTimeSlotId').data("val"));
                range = result.AvailableDeliveryDateRange ? result.AvailableDeliveryDateRange.split(',') : [];
                if (range.length == 2) {
                    //pickupMinDate.setDate(pickupMinDate.getDate() - 1 + parseInt(range[0]) || 0);
                    //DeliveryDate.SetMinDate(pickupMinDate);
                    //pickupMaxDate.setDate(pickupMaxDate.getDate() + parseInt(range[1]) || 0);
                    //DeliveryDate.SetMaxDate(pickupMaxDate);
                }
            },
            error: function (e) {
                currentServiceTime = null;
                $('#PickupTimeSlotId').html("");
                PickupDate.SetMinDate(null);
                PickupDate.SetMaxDate(null);
                DeliveryDate.SetMinDate(null);
                DeliveryDate.SetMaxDate(null);
                $('#DeliveryTimeSlotId').html("");
            }
        });
    });

    $('#SizeId').on('change', function () {
        updatePrice();
    });

    $('#ProductTypeId').on('change', function () {
        $.ajax({
            url: loadServiceOptionsLink,
            type: 'POST',
            data: {
                productTypeId: $(this).val(),
                selectedId: serviceId
            },
            dataType: 'html',
            success: function (result) {
                $('#ServiceId').html(result);
                $('#ServiceId').trigger('change');
            },
            error: function (e) {
                $('#ServiceId').html("");
            }
        });

        $.ajax({
            url: loadSizeOptionsLink,
            type: 'POST',
            data: {
                productTypeId: $(this).val(),
                selectedId: sizeId
            },
            dataType: 'html',
            success: function (result) {
                $('#SizeId').html(result);
                $('#SizeId').trigger('change');
            },
            error: function (e) {
                $('#SizeId').html("");
            }
        });
    });

    $('#ProductTypeId').trigger('change');

    $('#CustomerId').on('change', function () {
        updatePrice();
    });

    $('#PromoCode').on('change', function() {
        updatePrice();
    });

    $('button[type=submit]').on('click', function (e) {
        e.preventDefault();

        // Check conditions
        if (currentServiceTime != null) {
            var pickupTime = $('#PickupTimeSlotId').val();
            var pickupDate = PickupDate.GetDate();
            var deliveryTime = $('#DeliveryTimeSlotId').val();
            var deliveryDate = DeliveryDate.GetDate();

            if (currentServiceTime.IsPickupTimeAllow && !pickupTime) {
                $('#PickupTimeSlotId').addClass("input-validation-error");
                $('span[data-valmsg-for="PickupTimeSlotId"]').addClass("field-validation-error");
                $('span[data-valmsg-for="PickupTimeSlotId"]').html("");
                $('<span for="PickupTimeSlotId" class="">The Pickup time field is required.</span>').appendTo($('span[data-valmsg-for="PickupTimeSlotId"]'));
                return;
            } else {
                $('#PickupTimeSlotId').removeClass("input-validation-error");
                $('span[data-valmsg-for="PickupTimeSlotId"]').removeClass("field-validation-error");
                $('span[data-valmsg-for="PickupTimeSlotId"]').html("");
            }

            if (currentServiceTime.IsPickupDateAllow && !pickupDate) {
                $('#PickupDate').addClass("input-validation-error");
                $('span[data-valmsg-for="PickupDate"]').addClass("field-validation-error");
                $('span[data-valmsg-for="PickupDate"]').html("");
                $('<span for="PickupDate" class="">The Pickup date field is required.</span>').appendTo($('span[data-valmsg-for="PickupDate"]'));
                return;
            } else {
                $('#PickupDate').removeClass("input-validation-error");
                $('span[data-valmsg-for="PickupDate"]').removeClass("field-validation-error");
                $('span[data-valmsg-for="PickupDate"]').html("");
            }

            if (currentServiceTime.IsDeliveryTimeAllow && !deliveryTime) {
                $('#DeliveryTimeSlotId').addClass("input-validation-error");
                $('span[data-valmsg-for="DeliveryTimeSlotId"]').addClass("field-validation-error");
                $('span[data-valmsg-for="DeliveryTimeSlotId"]').html("");
                $('<span for="DeliveryTimeSlotId" class="">The Delivery time field is required.</span>').appendTo($('span[data-valmsg-for="DeliveryTimeSlotId"]'));
                return;
            } else {
                $('#DeliveryTimeSlotId').removeClass("input-validation-error");
                $('span[data-valmsg-for="DeliveryTimeSlotId"]').removeClass("field-validation-error");
                $('span[data-valmsg-for="DeliveryTimeSlotId"]').html("");
            }

            if (currentServiceTime.IsPickupTimeAllow && currentServiceTime.IsDeliveryTimeAllow && pickupTime && deliveryTime) {
                var pickupParts = ($("#PickupTimeSlotId option[value='" + pickupTime + "']").text() || "").split(' ');
                var deliveryParts = ($("#DeliveryTimeSlotId option[value='" + deliveryTime + "']").text() || "").split(' ');
                var beginPickupTime = pickupParts.length >= 3 ? pickupParts[pickupParts.length - 3].substr(1) : "00:00";
                var beginDeliveryTime = deliveryParts.length >= 3 ? deliveryParts[deliveryParts.length - 3].substr(1) : "00:00";
                if (parseFloat(beginPickupTime) >= parseFloat(beginDeliveryTime)) {
                    // Ignore if different date or not defined dates
                    //if ((!pickupDate && !deliveryDate) || (pickupDate.getFullYear() == deliveryDate.getFullYear() && pickupDate.getMonth() == deliveryDate.getMonth() && pickupDate.getDate() == deliveryDate.getDate())) {
                    //    $('#DeliveryTimeSlotId').addClass("input-validation-error");
                    //    $('span[data-valmsg-for="DeliveryTimeSlotId"]').addClass("field-validation-error");
                    //    $('span[data-valmsg-for="DeliveryTimeSlotId"]').html("");
                    //    $('<span for="DeliveryTimeSlotId" class="">The Delivery time must be later than Pickup time.</span>').appendTo($('span[data-valmsg-for="DeliveryTimeSlotId"]'));
                    //    return;
                    //} else
                    {
                        $('#DeliveryTimeSlotId').removeClass("input-validation-error");
                        $('span[data-valmsg-for="DeliveryTimeSlotId"]').removeClass("field-validation-error");
                        $('span[data-valmsg-for="DeliveryTimeSlotId"]').html("");
                    }
                }
            }

            if (currentServiceTime.IsDeliveryDateAllow && !deliveryDate) {
                $('#DeliveryDate').addClass("input-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').addClass("field-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').html("");
                $('<span for="DeliveryDate" class="">The Delivery date field is required.</span>').appendTo($('span[data-valmsg-for="DeliveryDate"]'));
                return;
            } else {
                $('#DeliveryDate').removeClass("input-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').removeClass("field-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').html("");
            }

            if (pickupDate && deliveryDate && pickupDate.getTime() > deliveryDate.getTime()) {
                $('#DeliveryDate').addClass("input-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').addClass("field-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').html("");
                $('<span for="DeliveryDate" class="">The Delivery date must be later than Pickup date.</span>').appendTo($('span[data-valmsg-for="DeliveryDate"]'));
                return;
            } else {
                $('#DeliveryDate').removeClass("input-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').removeClass("field-validation-error");
                $('span[data-valmsg-for="DeliveryDate"]').html("");
            }
        }


        $('#orderForm').submit();
    });
});

function onCheckFromZipCode() {
    $.ajax({
        url: "/api/Locations/CheckZipCode/" + $('#FromZipCode').val(),
        type: "POST",
        success: function (data) {
            $('#FromAddress').val((data.UnitNumber || "") + (data.StreetName != null ? (" " + data.StreetName) : ""));
        }
    });
}

function onCheckToZipCode() {
    $.ajax({
        url: "/api/Locations/CheckZipCode/" + $('#ToZipCode').val(),
        type: "POST",
        success: function (data) {
            $('#ToAddress').val((data.UnitNumber || "") + (data.StreetName != null ? (" " + data.StreetName) : ""));
        }
    });
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function updatePrice() {
    var serviceId = $('#ServiceId').val();
    var sizeId = $('#SizeId').val();

    if (!serviceId || !sizeId || firstTime < 2) {
        //$('#Price').val("");
        //$('#priceDetail').html("");
        ++firstTime; // Ignore 2 first times (for Service and Size)
        return;
    }

    $.ajax({
        url: '/api/orders/price',
        type: 'POST',
        data: {
            productTypeId: $('#ProductTypeId').val(),
            serviceId: serviceId,
            sizeId: sizeId,
            customerId: $('#CustomerId').val(),
            addOnCode: $('#PromoCode').val()
        },
        dataType: 'json',
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + getCookie("CdsAccessToken"));
        },
        success: function (result) {
            //$('#Price').val(result.Total.toFixed(2));
            $('#Price').val((result.BasePrice + result.AddOnPrice + result.DefaultPromotion + result.Promotion + result.Surcharge).toFixed(2));
            // Print detail below
            $('#priceDetail').html("");
            var table = $('<table>').addClass("table table-striped").appendTo($('#priceDetail'));
            var tbody = $('<tbody>').appendTo(table);
            var trHead = $('<tr>').appendTo(tbody);
            $('<th style="width: 10px">#</th>').appendTo(trHead);
            $('<th>Item</th>').appendTo(trHead);
            $('<th>Cost</th>').appendTo(trHead);
            $('<tr><td>1.</td><td>Service cost</td><td>' + result.BasePrice + '</td></tr>').appendTo(tbody);
            $('<tr><td>2.</td><td>Add on cost</td><td>' + result.AddOnPrice + '</td></tr>').appendTo(tbody);
            $('<tr><td>3.</td><td>Default Promo Code</td><td>' + result.DefaultPromotion + '</td></tr>').appendTo(tbody);
            $('<tr><td>4.</td><td>Special Promo Code</td><td>-' + result.Promotion + '</td></tr>').appendTo(tbody);
            $('<tr><td>5.</td><td>Surcharge</td><td>' + result.Surcharge + '</td></tr>').appendTo(tbody);
        },
        error: function (e) {
            $('#Price').val("");
            $('#priceDetail').html("");
        }
    });
}
