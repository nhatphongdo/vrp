//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Courier_Management_System
{
    using System;
    using System.Collections.Generic;
    
    public partial class Payment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Payment()
        {
            this.OrdersOfPayments = new HashSet<OrdersOfPayment>();
        }
    
        public long Id { get; set; }
        public string Orders { get; set; }
        public Nullable<long> PaymentAccountId { get; set; }
        public string Payer { get; set; }
        public decimal PaymentAmount { get; set; }
        public int AccountType { get; set; }
        public bool IsSuccessful { get; set; }
        public string Remark { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool InvoiceSent { get; set; }
        public Nullable<System.DateTime> InvoiceSentOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrdersOfPayment> OrdersOfPayments { get; set; }
        public virtual PaymentAccount PaymentAccount { get; set; }
    }
}
