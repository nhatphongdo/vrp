﻿using System;
using System.Collections.Generic;
using System.Linq;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Services.Implement
{
    public class OrderService : IOrderService
    {
        public IQueryable<OrderViewModel> GetOrdersList(DateTime? pickupStartDate, DateTime? pickupEndDate, DateTime? deliveryStartDate,
            DateTime? deliveryEndDate, CourierDBEntities dbContext)
        {
            var cod = "";
            var codCollected = "";
            var types = new List<int>();
            var collectedTypes = new List<int>();

            foreach (var t in cod.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                CodOptionTypes type;
                if (Enum.TryParse(t, true, out type))
                {
                    types.Add((int)type);
                }
            }
            foreach (var t in codCollected.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                CodOptionTypes type;
                if (Enum.TryParse(t, true, out type))
                {
                    collectedTypes.Add((int)type);
                }
            }

            var result = from order in dbContext.Orders
                         where
                         !order.IsDeleted &&
                         (types.Count == 0 ||
                          order.CodOptionOfOrders.Where(c => !c.IsDeleted && types.Contains(c.CodOption.Type)).Select(x => x.CodOption.Type).Count() != 0) &&
                         (collectedTypes.Count == 0 ||
                          order.CodOptionOfOrders.Where(c => !c.IsDeleted && c.IsCollected && collectedTypes.Contains(c.CodOption.Type)).Select(c => c.CodOption.Type).Distinct().Count() != 0)
                         join zipCode in dbContext.ZipCodes on order.FromZipCode equals zipCode.PostalCode into fromGroup
                         join zipCode in dbContext.ZipCodes on order.ToZipCode equals zipCode.PostalCode into toGroup
                         select new OrderViewModel()
                         {
                             CreatedOn = order.CreatedOn,
                             Id = order.Id,
                             BinCode = order.BinCode,
                             CanceledOn = order.CanceledOn,
                             UpdatedOn = order.UpdatedOn,
                             ServiceId = order.ServiceId,
                             PaymentTypeId = order.PaymentTypeId,
                             ProductTypeId = order.ProductTypeId,
                             CollectedBy = order.CollectedBy,
                             CollectedOn = order.CollectedOn,
                             ConfirmedBy = order.ConfirmedBy,
                             ConfirmedOn = order.ConfirmedOn,
                             ConsignmentNumber = order.ConsignmentNumber,
                             CustomerId = order.CustomerId,
                             CustomerName = order.Customer.CustomerName,
                             CustomerCompany = order.Customer.CustomerCompany,
                             DeliveredBy = order.DeliveredBy,
                             DeliveredOn = order.DeliveredOn,
                             FromAddress = order.FromAddress,
                             FromName = order.FromName,
                             FromZipCode = order.FromZipCode,
                             FromMobilePhone = order.FromMobilePhone,
                             FromSignature = order.FromSignature,
                             Price = order.Price,
                             ProceededOn = order.ProceededOn,
                             ProceededBy = order.ProceededBy,
                             ProceededOutOn = order.ProceededOutOn,
                             ProceededOutBy = order.ProceededOutBy,
                             RejectNote = order.RejectNote,
                             Remark = order.Remark,
                             Status = (OrderStatuses)order.Status,
                             ToAddress = order.ToAddress,
                             ToName = order.ToName,
                             ToZipCode = order.ToZipCode,
                             ToMobilePhone = order.ToMobilePhone,
                             ToSignature = order.ToSignature,
                             QrCodePath = order.QrCodePath,
                             PromoCode = order.PromoCode,
                             SizeId = order.SizeId,
                             PickupTimeSlotId = order.PickupTimeSlotId,
                             PickupDate = order.PickupDate,
                             DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                             DeliveryDate = order.DeliveryDate,
                             FromDistrictCode = fromGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                             FromZoneCode = fromGroup.Select(x => x.Zone).FirstOrDefault(),
                             ToDistrictCode = toGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                             ToZoneCode = toGroup.Select(x => x.Zone).FirstOrDefault(),
                             OperatorRemark = order.OperatorRemark,
                             OrderNumber = order.OrderNumber,
                             RequestDate = order.Status == (int)OrderStatuses.PickupFailed
                                                      ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                      : order.Status == (int)OrderStatuses.DeliveryFailed
                                                          ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.ReDeliveryRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                          : null,
                             RequestRemark = order.Status == (int)OrderStatuses.PickupFailed
                                                        ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault()
                                                        : order.Status == (int)OrderStatuses.DeliveryFailed
                                                            ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.ReDeliveryRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault()
                                                            : "",
                             IsExchange = order.IsExchange,
                             TotalCodAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted).Sum(x => x.Amount),
                             TotalCodCollectedAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted && x.IsCollected).Sum(x => x.CollectedAmount),
                         };
            if (pickupStartDate != null && pickupEndDate != null)
            {
                result = result.Where(item => item.PickupDate >= pickupStartDate && item.PickupDate <= pickupEndDate);
            }
            if (deliveryStartDate != null && deliveryEndDate != null)
            {
                result = result.Where(item => item.DeliveryDate >= deliveryStartDate && item.DeliveryDate <= deliveryEndDate);
            }
            return result;
        }

        public IQueryable<Order> GetOrdersListByIdArray(string idsstr, CourierDBEntities dbContext)
        {
            var idsArr = idsstr?.Split(',');

            if (idsArr == null) return null;
            var result = from r in dbContext.Orders where idsArr.ToList().Contains(r.Id.ToString()) select r;

            return result;
        }
    }
}
