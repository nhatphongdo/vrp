﻿using System;
using System.Linq;

namespace Courier_Management_System.Services.Implement
{
    public class JobService : IJobService
    {
        public IQueryable<dynamic> GetJobsList(int driverId, int driverType, CourierDBEntities dbContext)
        {
            var jobs = (from job in dbContext.Jobs
                        from vehicle in dbContext.Vehicles
                        where !job.IsDeleted && !job.IsSpecial &&
                              !job.CompletedOn.HasValue && job.DriverId == driverId &&
                              job.DriverType == (int)driverType && vehicle.Id == job.VehicleId
                        select new
                        {
                            job.Id,
                            job.VehicleId,
                            vehicle.VehicleNumber,
                            job.TakenOn,
                            job.TakenBy,
                            job.TotalPickups,
                            job.CreatedOn
                        });

            return jobs;
        }

        public IQueryable<dynamic> GetRouteByJobId(int jobId, CourierDBEntities dbContext)
        {
            throw new NotImplementedException();
        }
    }
}
