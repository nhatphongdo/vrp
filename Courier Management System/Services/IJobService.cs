﻿using System.Collections.Generic;
using System.Linq;

namespace Courier_Management_System.Services
{
    public interface IJobService
    {
        IQueryable<dynamic> GetJobsList(int driverId, int driverType, CourierDBEntities dbContext);
        IQueryable<dynamic> GetRouteByJobId(int jobId, CourierDBEntities dbContext);
    }
}
