﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Courier_Management_System.Models;

namespace Courier_Management_System.Services
{
    public interface IOrderService
    {
        IQueryable<OrderViewModel> GetOrdersList(DateTime? pickupStartDate, DateTime? pickupEndDate, DateTime? deliveryStartDate, DateTime? devilveryEndDate, CourierDBEntities dbContext);
        IQueryable<Order> GetOrdersListByIdArray(string idsstr, CourierDBEntities dbContext);
    }
}
