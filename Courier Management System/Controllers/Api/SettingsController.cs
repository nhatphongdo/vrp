﻿using System.Threading.Tasks;
using System.Web.Http;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Controllers.Api
{
    [RoutePrefix("api/settings")]
    public class SettingsController : ApiController
    {
        private CourierDBEntities db = new CourierDBEntities();

        [Route("urls")]
        [HttpPost]
        public async Task<IHttpActionResult> Urls()
        {
            return Ok(new
            {
                CompanyInfo = SettingProvider.CompanyInfoUrl,
                TermsAndConditions = SettingProvider.TermsAndConditionsUrl,
                News = SettingProvider.NewsUrl,
                TypeOfService = SettingProvider.TypeOfServiceUrl,
                ServiceGuidelines = SettingProvider.ServiceGuidelinesUrl
            });
        }

        [Route("offdays")]
        [HttpPost]
        public async Task<IHttpActionResult> OffDays()
        {
            return Ok(new
            {
                OutOfServiceDays = SettingProvider.OutOfServiceDays
            });
        }

        [Route("requests")]
        [HttpPost]
        public async Task<IHttpActionResult> Requests()
        {
            return Ok(new
            {
                PickupRequests = SettingProvider.PickupRequests,
                DeliveryRequests = SettingProvider.DeliveryRequests,
                FailedPickupReasons = SettingProvider.FailedPickupReasons,
                FailedDeliveryReasons = SettingProvider.FailedDeliveryReasons,
                DriverLocationsRadius = SettingProvider.DriverLocationsRadius
            });
        }
    }
}
