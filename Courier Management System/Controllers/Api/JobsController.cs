﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using Courier_Management_System.Services;
using Courier_Management_System.Services.Implement;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;

namespace Courier_Management_System.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/jobs")]
    public class JobsController : ApiController
    {
        #region Private

        private CourierDBEntities db = new CourierDBEntities();

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public JobsController()
        {
            db.Database.CommandTimeout = 600;
        }

        public JobsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        #endregion

        [Route("list")]
        [HttpPost]
        public async Task<IHttpActionResult> List()
        {
            // Get driver's information of this account
            IList<string> roles = new List<string>();
            try
            {
                roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            }
            catch (Exception exc)
            {
                // No roles
                return NotFound();
            }

            var profiles = await AccountHelper.Profile(User.Identity.GetUserName(), roles);

            var driverId = 0;
            var driverType = AccountTypes.Staff;
            foreach (var profile in profiles)
            {
                if (profile.Profile.IsDriver == true)
                {
                    driverId = profile.Profile.Id;
                    driverType = profile.ProfileType == AccountHelper.StaffDriverRole
                                     ? AccountTypes.Staff
                                     : AccountTypes.Contractor;
                    break;
                }
            }

            if (driverId == 0)
            {
                return NotFound();
            }

            try
            {
                var startDate = DateTime.Now.Date;
                var endDate = startDate.AddDays(1);
                var list = (from job in db.Jobs
                            from vehicle in db.Vehicles
                            where !job.IsDeleted && !job.IsSpecial &&
                                  //job.TakenOn >= startDate && job.TakenOn < endDate && 
                                  !job.CompletedOn.HasValue && job.DriverId == driverId &&
                                  job.DriverType == (int)driverType && vehicle.Id == job.VehicleId
                            select new
                            {
                                job.Id,
                                job.VehicleId,
                                vehicle.VehicleNumber,
                                job.Routes,
                                job.TakenOn,
                                job.TakenBy,
                                job.TotalPickups,
                                job.TotalDeliveries,
                                job.TotalDistances,
                                job.TotalDrivingTime,
                                job.TotalWorkingTime,
                                job.CreatedOn
                            }).ToList();
                var jobs = new List<dynamic>();
                foreach (var item in list)
                {
                    var route = JsonConvert.DeserializeObject<JobViewModel>(item.Routes);

                    // Get order list
                    var orderIds = route.Locations.Where(x => !string.IsNullOrEmpty(x.OrderId)).Select(x => x.OrderId).Distinct().ToList();
                    var ids = new List<long>();
                    foreach (var orderId in orderIds)
                    {
                        ids.AddRange(orderId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i)));
                    }

                    ids = ids.Distinct().ToList();
                    var orders = db.Orders.Where(x => !x.IsDeleted && ids.Contains(x.Id))
                                   .Select(
                                           x => new
                                           {
                                               x.Id,
                                               x.FromName,
                                               x.FromAddress,
                                               x.FromMobilePhone,
                                               x.Customer.CustomerCompany,
                                               x.ToName,
                                               x.ToAddress,
                                               x.ToMobilePhone,
                                               x.ConsignmentNumber,
                                               x.Status,
                                               x.Size.SizeName,
                                               x.PickupTimeSlotId,
                                               x.DeliveryTimeSlotId,
                                               x.CreatedOn,
                                               x.ConfirmedOn,
                                               x.OperatorRemark,
                                               CodOptionOfOrders = x.CodOptionOfOrders
                                                                         .Where(i => !i.IsDeleted)
                                                                         .Select(
                                                                                 i => new
                                                                                 {
                                                                                     i.Amount,
                                                                                     CodOptionType = i.CodOption.Type,
                                                                                     CodOptionDescription = i.CodOption.Description
                                                                                 })
                                                                         .ToList(),
                                               x.IsExchange,
                                               ExtraRequests = x.ExtraRequests
                                                                     .Where(i => !i.IsDeleted)
                                                                     .OrderByDescending(i => i.CreatedOn)
                                                                     .Select(
                                                                             i => new
                                                                             {
                                                                                 RequestContent = i.RequestContent,
                                                                                 RequestType = i.RequestType
                                                                             })
                                                                     .ToList(),
                                           })
                                   .ToList();

                    foreach (var location in route.Locations)
                    {
                        var oids = (string.IsNullOrEmpty(location.OrderId) ? "" : location.OrderId).Split(
                                                                                                          new char[] { ',' },
                                                                                                          StringSplitOptions.RemoveEmptyEntries);
                        var tempOrders = orders.Where(x => oids.Contains(x.Id.ToString())).ToList();
                        location.ConsignmentNumbers = tempOrders.Select(x => x.ConsignmentNumber).ToList();
                        if (tempOrders.Count > 0)
                        {
                            if (location.Type == 0)
                            {
                                // Pickup
                                location.CustomerName = tempOrders.FirstOrDefault().FromName;
                                location.CustomerAddress = tempOrders.FirstOrDefault().FromAddress;
                                location.CustomerMobilePhone = tempOrders.FirstOrDefault().FromMobilePhone;
                                location.CustomerCompany = tempOrders.FirstOrDefault().CustomerCompany;
                                location.IsDone = tempOrders.All(x => x.Status != (int)OrderStatuses.ItemHasBeingPicked);
                                location.SizeName = tempOrders.FirstOrDefault().SizeName;
                            }
                            else
                            {
                                // Delivery
                                location.CustomerName = tempOrders.FirstOrDefault().ToName;
                                location.CustomerAddress = tempOrders.FirstOrDefault().ToAddress;
                                location.CustomerMobilePhone = tempOrders.FirstOrDefault().ToMobilePhone;
                                location.IsDone = tempOrders.All(x => x.Status != (int)OrderStatuses.ItemOutDepot);
                                location.SizeName = tempOrders.FirstOrDefault().SizeName;
                            }
                        }
                        else
                        {
                            location.CustomerName = "";
                            location.CustomerAddress = "";
                            location.CustomerMobilePhone = "";
                            location.CustomerCompany = "";
                            location.IsDone = location.Type == 0
                                                  ? tempOrders.All(x => x.Status != (int)OrderStatuses.ItemHasBeingPicked)
                                                  : tempOrders.All(x => x.Status != (int)OrderStatuses.ItemOutDepot);
                            location.SizeName = "";
                        }

                        location.Orders = new List<Order>();
                        foreach (var order in orders.Where(x => oids.Contains(x.Id.ToString())).ToList())
                        {
                            var newOrder = new Order()
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                                Status = order.Status,
                                Size = new Size()
                                {
                                    SizeName = order.SizeName,
                                },
                                PickupTimeSlotId = order.PickupTimeSlotId,
                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                CreatedOn = order.CreatedOn,
                                ConfirmedOn = order.ConfirmedOn,
                                OperatorRemark = order.OperatorRemark,
                                CodOptionOfOrders = order.CodOptionOfOrders
                                                                        .Select(
                                                                                x => new CodOptionOfOrder()
                                                                                {
                                                                                    Amount = x.Amount,
                                                                                    CodOption = new CodOption()
                                                                                    {
                                                                                        Type = x.CodOptionType,
                                                                                        Description = x.CodOptionDescription
                                                                                    }
                                                                                })
                                                                        .ToList(),
                                IsExchange = order.IsExchange,
                                ExtraRequests = order.ExtraRequests
                                                                    .Select(
                                                                            x => new ExtraRequest()
                                                                            {
                                                                                RequestType = x.RequestType,
                                                                                RequestContent = x.RequestContent
                                                                            })
                                                                    .ToList()
                            };
                            location.Orders.Add(newOrder);
                        }
                    }

                    // Parse path
                    route.Route.FootSteps = JsonConvert.DeserializeObject<List<RouteStepViewModel>>(route.Route.Path);

                    jobs.Add(
                             new
                             {
                                 item.Id,
                                 item.VehicleId,
                                 item.VehicleNumber,
                                 Routes = route,
                                 item.TakenOn,
                                 item.TakenBy,
                                 item.TotalPickups,
                                 item.TotalDeliveries,
                                 item.TotalDistances,
                                 item.TotalDrivingTime,
                                 item.TotalWorkingTime,
                                 CreatedOn = item.CreatedOn.ToString("O")
                             });
                    break;
                }

                return Ok(
                          new
                          {
                              Code = 0,
                              Result = jobs,
                              Timeslots = db.TimeSlots.Where(x => !x.IsDeleted)
                                            .ToList()
                                            .Select(
                                                    x => new
                                                    {
                                                        x.Id,
                                                        Text = x.TimeSlotName,
                                                        FromTime = x.FromTime.ToString("hh:mm tt"),
                                                        ToTime = x.ToTime.ToString("hh:mm tt")
                                                    })
                                            .ToList()
                          });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to administrator to resolve."
                          });
            }
        }

        [Route("history")]
        [HttpPost]
        public async Task<IHttpActionResult> History()
        {
            // Get driver's information of this account
            IList<string> roles = new List<string>();
            try
            {
                roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            }
            catch (Exception exc)
            {
                // No roles
                return NotFound();
            }

            var profiles = await AccountHelper.Profile(User.Identity.GetUserName(), roles);

            var driverId = 0;
            var driverType = AccountTypes.Staff;
            foreach (var profile in profiles)
            {
                if (profile.Profile.IsDriver == true)
                {
                    driverId = profile.Profile.Id;
                    driverType = profile.ProfileType == AccountHelper.StaffDriverRole
                                     ? AccountTypes.Staff
                                     : AccountTypes.Contractor;
                    break;
                }
            }

            if (driverId == 0)
            {
                return NotFound();
            }

            try
            {
                var list = (from job in db.Jobs
                            from vehicle in db.Vehicles
                            where !job.IsDeleted && job.DriverId == driverId && job.DriverType == (int)driverType && vehicle.Id == job.VehicleId
                            orderby job.CompletedOn descending
                            select new
                            {
                                job.Id,
                                job.VehicleId,
                                vehicle.VehicleNumber,
                                job.TakenOn,
                                job.TakenBy,
                                job.TotalPickups,
                                job.TotalDeliveries,
                                job.TotalDistances,
                                job.TotalDrivingTime,
                                job.TotalWorkingTime,
                                job.CompletedOn
                            }).ToList();

                return Ok(
                          new
                          {
                              Code = 0,
                              Result = list
                          });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to administrator to resolve."
                          });
            }
        }

        [Route("special")]
        [HttpPost]
        public async Task<IHttpActionResult> Special()
        {
            // Get driver's information of this account
            IList<string> roles = new List<string>();
            try
            {
                roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            }
            catch (Exception exc)
            {
                // No roles
                return NotFound();
            }

            var profiles = await AccountHelper.Profile(User.Identity.GetUserName(), roles);

            var driverId = 0;
            var driverType = AccountTypes.Staff;
            foreach (var profile in profiles)
            {
                if (profile.Profile.IsDriver == true)
                {
                    driverId = profile.Profile.Id;
                    driverType = profile.ProfileType == AccountHelper.StaffDriverRole
                                     ? AccountTypes.Staff
                                     : AccountTypes.Contractor;
                    break;
                }
            }

            if (driverId == 0)
            {
                return NotFound();
            }

            try
            {
                var list = (from job in db.Jobs
                            where !job.IsDeleted && !job.CompletedOn.HasValue && job.DriverId == driverId && job.DriverType == (int)driverType && job.IsSpecial
                            select new
                            {
                                job.Id,
                                job.Routes,
                                job.TakenOn,
                                job.TakenBy,
                                job.CreatedOn
                            }).ToList();

                var orderIds = list.SelectMany(item => (item.Routes ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i))).Distinct().ToList();
                var orders = (from order in db.Orders
                              join zipCode in db.ZipCodes on order.FromZipCode equals zipCode.PostalCode into fromGroup
                              join zipCode in db.ZipCodes on order.ToZipCode equals zipCode.PostalCode into toGroup
                              where !order.IsDeleted && orderIds.Contains(order.Id) &&
                                    (order.Status == (int)OrderStatuses.SpecialOrderForPickup || order.Status == (int)OrderStatuses.SpecialOrderForDelivery ||
                                     order.Status == (int)OrderStatuses.ItemHasBeingPicked || order.Status == (int)OrderStatuses.ItemOutDepot)
                              select new
                              {
                                  order.Id,
                                  order.ConsignmentNumber,
                                  order.Status,
                                  order.BinCode,
                                  order.FromAddress,
                                  order.FromZipCode,
                                  order.FromMobilePhone,
                                  order.FromName,
                                  order.ToName,
                                  order.ToAddress,
                                  order.ToZipCode,
                                  order.ToMobilePhone,
                                  order.Customer.CustomerCompany,
                                  order.Size.SizeName,
                                  order.ProductType.ProductTypeName,
                                  order.PickupTimeSlotId,
                                  order.DeliveryTimeSlotId,
                                  order.OperatorRemark,
                                  ExtraRequests = order.ExtraRequests
                                                              .Where(x => !x.IsDeleted)
                                                              .OrderByDescending(x => x.CreatedOn)
                                                              .Select(
                                                                      x => new
                                                                      {
                                                                          RequestContent = x.RequestContent,
                                                                          RequestType = x.RequestType
                                                                      })
                                                              .ToList(),
                                  Type = (order.Status == (int)OrderStatuses.SpecialOrderForPickup || order.Status == (int)OrderStatuses.ItemHasBeingPicked) ? 0 : 1,
                                  CodOptionOfOrders = order.CodOptionOfOrders
                                                                  .Where(x => !x.IsDeleted)
                                                                  .Select(
                                                                          x => new
                                                                          {
                                                                              Amount = x.Amount,
                                                                              CodOption = new
                                                                              {
                                                                                  Type = x.CodOption.Type,
                                                                                  Description = x.CodOption.Description
                                                                              }
                                                                          })
                                                                  .ToList(),
                                  IsExchange = order.IsExchange,
                                  FromGeolocation = new
                                  {
                                      Latitude = fromGroup.Select(x => x.Latitude).FirstOrDefault() ?? 0,
                                      Longitude = fromGroup.Select(x => x.Longitude).FirstOrDefault() ?? 0,
                                  },
                                  ToGeolocation = new
                                  {
                                      Latitude = toGroup.Select(x => x.Latitude).FirstOrDefault() ?? 0,
                                      Longitude = toGroup.Select(x => x.Longitude).FirstOrDefault() ?? 0,
                                  }
                              })
                    .ToList();

                var jobs = new List<dynamic>();
                foreach (var item in list)
                {
                    // Get order list
                    var ids = (item.Routes ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList();
                    jobs.Add(
                             new
                             {
                                 item.Id,
                                 Orders = orders.Where(x => ids.Contains(x.Id.ToString())).ToList(),
                                 item.TakenOn,
                                 item.TakenBy,
                                 CreatedOn = item.CreatedOn.ToString("O")
                             });
                }

                return Ok(
                          new
                          {
                              Code = 0,
                              Result = jobs,
                              Timeslots = db.TimeSlots.Where(x => !x.IsDeleted)
                                            .ToList()
                                            .Select(
                                                    x => new
                                                    {
                                                        x.Id,
                                                        Text = x.TimeSlotName,
                                                        FromTime = x.FromTime.ToString("hh:mm tt"),
                                                        ToTime = x.ToTime.ToString("hh:mm tt")
                                                    })
                                            .ToList()
                          });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to administrator to resolve."
                          });
            }
        }

        [Route("historysummary")]
        [HttpPost]
        public async Task<IHttpActionResult> HistorySummary(JobHistorySummaryViewModel model)
        {
            // Get driver's information of this account
            IList<string> roles = new List<string>();
            try
            {
                roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            }
            catch (Exception exc)
            {
                // No roles
                return NotFound();
            }

            var username = User.Identity.GetUserName();
            var profiles = await AccountHelper.Profile(username, roles);

            var driverId = 0;
            var driverType = AccountTypes.Staff;
            foreach (var profile in profiles)
            {
                if (profile.Profile.IsDriver == true)
                {
                    driverId = profile.Profile.Id;
                    driverType = profile.ProfileType == AccountHelper.StaffDriverRole
                                     ? AccountTypes.Staff
                                     : AccountTypes.Contractor;
                    break;
                }
            }

            if (driverId == 0)
            {
                return NotFound();
            }

            try
            {
                var list = db.JobHistories.Where(
                                                 x => !x.IsDeleted && x.CompletedBy.ToLower() == username.ToLower() && x.CompletedOn.HasValue &&
                                                      DbFunctions.TruncateTime(x.CompletedOn) >= model.FromDate && DbFunctions.TruncateTime(x.CompletedOn) <= model.ToDate)
                             .GroupBy(x => x.IsPickup)
                             .Select(
                                     g => new
                                     {
                                         IsPickup = g.Key,
                                         Count = g.Count(),
                                         Amount = g.Sum(x => x.Payment - x.DeductionAmount)
                                     })
                             .ToList();

                return Ok(
                          new
                          {
                              Code = 0,
                              Result = list
                          });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to administrator to resolve."
                          });
            }
        }

        [Route("historydetail")]
        [HttpPost]
        public async Task<IHttpActionResult> HistoryDetail(JobHistorySummaryViewModel model)
        {
            // Get driver's information of this account
            IList<string> roles = new List<string>();
            try
            {
                roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            }
            catch (Exception exc)
            {
                // No roles
                return NotFound();
            }

            var username = User.Identity.GetUserName();
            var profiles = await AccountHelper.Profile(username, roles);

            var driverId = 0;
            var driverType = AccountTypes.Staff;
            foreach (var profile in profiles)
            {
                if (profile.Profile.IsDriver == true)
                {
                    driverId = profile.Profile.Id;
                    driverType = profile.ProfileType == AccountHelper.StaffDriverRole
                                     ? AccountTypes.Staff
                                     : AccountTypes.Contractor;
                    break;
                }
            }

            if (driverId == 0)
            {
                return NotFound();
            }

            try
            {
                var list = db.JobHistories.Where(
                                                 x => !x.IsDeleted && x.CompletedBy.ToLower() == username.ToLower() && x.IsPickup == model.IsPickup && x.CompletedOn.HasValue &&
                                                      DbFunctions.TruncateTime(x.CompletedOn) >= model.FromDate && DbFunctions.TruncateTime(x.CompletedOn) <= model.ToDate)
                             .Join(
                                   db.Orders,
                                   job => job.ConsignmentNumber.ToLower(),
                                   order => order.ConsignmentNumber.ToLower(),
                                   (job, order) => new
                                   {
                                       job.CompletedOn,
                                       Size = order.Size.SizeName
                                   })
                             .OrderByDescending(x => x.CompletedOn)
                             .ToList();

                return Ok(
                          new
                          {
                              Code = 0,
                              Result = list
                          });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to administrator to resolve."
                          });
            }
        }

        //Duy: Create new API to call multijob
        [Route("myjobs")]
        [HttpPost]
        public async Task<IHttpActionResult> MyJobs()
        {
            // Get driver's information of this account
            IList<string> roles = new List<string>();
            try
            {
                roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            }
            catch (Exception exc)
            {
                // No roles
                return NotFound();
            }

            var profiles = await AccountHelper.Profile(User.Identity.GetUserName(), roles);

            var driverId = 0;
            var driverType = AccountTypes.Staff;
            foreach (var profile in profiles)
            {
                if (profile.Profile.IsDriver == true)
                {
                    driverId = profile.Profile.Id;
                    driverType = profile.ProfileType == AccountHelper.StaffDriverRole
                                     ? AccountTypes.Staff
                                     : AccountTypes.Contractor;
                    break;
                }
            }

            if (driverId == 0)
            {
                return NotFound();
            }

            try
            {
                var jobService = new JobService();
                return Ok(
                          new
                          {
                              Code = 0,
                              Result = jobService.GetJobsList(driverId, (int)driverType, db)
                          });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to administrator to resolve."
                          });
            }
        }

        [Route("loadjob/{jobId}")]
        [HttpPost]
        public async Task<IHttpActionResult> LoadJob(int? jobId)
        {
            // Get driver's information of this account
            IList<string> roles = new List<string>();
            try
            {
                roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
                if (roles == null) return NotFound();
            }
            catch (Exception exc)
            {
                // No roles
                return NotFound();
            }

            try
            {
                var selectedJob = (from job in db.Jobs
                                   from vehicle in db.Vehicles
                                   where job.Id == jobId.Value
                                   select new
                                   {
                                       job.Id,
                                       job.VehicleId,
                                       vehicle.VehicleNumber,
                                       job.Routes,
                                       job.TakenOn,
                                       job.TakenBy,
                                       job.TotalPickups,
                                       job.TotalDeliveries,
                                       job.TotalDistances,
                                       job.TotalDrivingTime,
                                       job.TotalWorkingTime,
                                       job.CreatedOn
                                   }).FirstOrDefault();
                if (selectedJob == null) return NotFound();

                var jobs = new List<dynamic>();
                var route = JsonConvert.DeserializeObject<JobViewModel>(selectedJob.Routes);

                // Get order list
                var orderIds = route.Locations.Where(x => !string.IsNullOrEmpty(x.OrderId)).Select(x => x.OrderId).Distinct().ToList();
                var ids = new List<long>();
                foreach (var orderId in orderIds)
                {
                    ids.AddRange(orderId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i)));
                }

                ids = ids.Distinct().ToList();
                var orders = db.Orders.Where(x => !x.IsDeleted && ids.Contains(x.Id))
                               .Select(
                                       x => new
                                       {
                                           x.Id,
                                           x.FromName,
                                           x.FromAddress,
                                           x.FromMobilePhone,
                                           x.Customer.CustomerCompany,
                                           x.ToName,
                                           x.ToAddress,
                                           x.ToMobilePhone,
                                           x.ConsignmentNumber,
                                           x.Status,
                                           x.Size.SizeName,
                                           x.PickupTimeSlotId,
                                           x.DeliveryTimeSlotId,
                                           x.CreatedOn,
                                           x.ConfirmedOn,
                                           x.OperatorRemark,
                                           CodOptionOfOrders = x.CodOptionOfOrders
                                                                     .Where(i => !i.IsDeleted)
                                                                     .Select(
                                                                             i => new
                                                                             {
                                                                                 i.Amount,
                                                                                 CodOptionType = i.CodOption.Type,
                                                                                 CodOptionDescription = i.CodOption.Description
                                                                             })
                                                                     .ToList(),
                                           x.IsExchange,
                                           ExtraRequests = x.ExtraRequests
                                                                 .Where(i => !i.IsDeleted)
                                                                 .OrderByDescending(i => i.CreatedOn)
                                                                 .Select(
                                                                         i => new
                                                                         {
                                                                             RequestContent = i.RequestContent,
                                                                             RequestType = i.RequestType
                                                                         })
                                                                 .ToList(),
                                       })
                               .ToList();

                foreach (var location in route.Locations)
                {
                    var oids = (string.IsNullOrEmpty(location.OrderId) ? "" : location.OrderId).Split(
                                                                                                      new char[] { ',' },
                                                                                                      StringSplitOptions.RemoveEmptyEntries);
                    var tempOrders = orders.Where(x => oids.Contains(x.Id.ToString())).ToList();
                    location.ConsignmentNumbers = tempOrders.Select(x => x.ConsignmentNumber).ToList();
                    if (tempOrders.Count > 0)
                    {
                        if (location.Type == 0)
                        {
                            // Pickup
                            location.CustomerName = tempOrders.FirstOrDefault().FromName;
                            location.CustomerAddress = tempOrders.FirstOrDefault().FromAddress;
                            location.CustomerMobilePhone = tempOrders.FirstOrDefault().FromMobilePhone;
                            location.CustomerCompany = tempOrders.FirstOrDefault().CustomerCompany;
                            location.IsDone = tempOrders.All(x => x.Status != (int)OrderStatuses.ItemHasBeingPicked);
                            location.SizeName = tempOrders.FirstOrDefault().SizeName;
                        }
                        else
                        {
                            // Delivery
                            location.CustomerName = tempOrders.FirstOrDefault().ToName;
                            location.CustomerAddress = tempOrders.FirstOrDefault().ToAddress;
                            location.CustomerMobilePhone = tempOrders.FirstOrDefault().ToMobilePhone;
                            location.IsDone = tempOrders.All(x => x.Status != (int)OrderStatuses.ItemOutDepot);
                            location.SizeName = tempOrders.FirstOrDefault().SizeName;
                        }
                    }
                    else
                    {
                        location.CustomerName = "";
                        location.CustomerAddress = "";
                        location.CustomerMobilePhone = "";
                        location.CustomerCompany = "";
                        location.IsDone = location.Type == 0
                                              ? tempOrders.All(x => x.Status != (int)OrderStatuses.ItemHasBeingPicked)
                                              : tempOrders.All(x => x.Status != (int)OrderStatuses.ItemOutDepot);
                        location.SizeName = "";
                    }

                    location.Orders = new List<Order>();
                    foreach (var order in orders.Where(x => oids.Contains(x.Id.ToString())).ToList())
                    {
                        var newOrder = new Order()
                        {
                            ConsignmentNumber = order.ConsignmentNumber,
                            Status = order.Status,
                            Size = new Size()
                            {
                                SizeName = order.SizeName,
                            },
                            PickupTimeSlotId = order.PickupTimeSlotId,
                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                            CreatedOn = order.CreatedOn,
                            ConfirmedOn = order.ConfirmedOn,
                            OperatorRemark = order.OperatorRemark,
                            CodOptionOfOrders = order.CodOptionOfOrders
                                                                    .Select(
                                                                            x => new CodOptionOfOrder()
                                                                            {
                                                                                Amount = x.Amount,
                                                                                CodOption = new CodOption()
                                                                                {
                                                                                    Type = x.CodOptionType,
                                                                                    Description = x.CodOptionDescription
                                                                                }
                                                                            })
                                                                    .ToList(),
                            IsExchange = order.IsExchange,
                            ExtraRequests = order.ExtraRequests
                                                                .Select(
                                                                        x => new ExtraRequest()
                                                                        {
                                                                            RequestType = x.RequestType,
                                                                            RequestContent = x.RequestContent
                                                                        })
                                                                .ToList()
                        };
                        location.Orders.Add(newOrder);
                    }
                }

                // Parse path
                route.Route.FootSteps = JsonConvert.DeserializeObject<List<RouteStepViewModel>>(route.Route.Path);

                jobs.Add(
                         new
                         {
                             selectedJob.Id,
                             selectedJob.VehicleId,
                             selectedJob.VehicleNumber,
                             Routes = route,
                             selectedJob.TakenOn,
                             selectedJob.TakenBy,
                             selectedJob.TotalPickups,
                             selectedJob.TotalDeliveries,
                             selectedJob.TotalDistances,
                             selectedJob.TotalDrivingTime,
                             selectedJob.TotalWorkingTime,
                             CreatedOn = selectedJob.CreatedOn.ToString("O")
                         });


                return Ok(
                          new
                          {
                              Code = 0,
                              Result = jobs,
                              Timeslots = db.TimeSlots.Where(x => !x.IsDeleted)
                                            .ToList()
                                            .Select(
                                                    x => new
                                                    {
                                                        x.Id,
                                                        Text = x.TimeSlotName,
                                                        FromTime = x.FromTime.ToString("hh:mm tt"),
                                                        ToTime = x.ToTime.ToString("hh:mm tt")
                                                    })
                                            .ToList()
                          });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to administrator to resolve."
                          });
            }
        }
    }
}
