﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/locations")]
    public class LocationsController : ApiController
    {
        private CourierDBEntities db = new CourierDBEntities();

        [Route("checkZipCode/{postalCode}")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> CheckZipCode(string postalCode)
        {
            var zipCode = await db.ZipCodes.FirstOrDefaultAsync(x => x.PostalCode.Equals(postalCode, StringComparison.OrdinalIgnoreCase));
            if (zipCode == null)
            {
                return NotFound();
            }

            return Ok(zipCode);
        }

        [Route("checkAddress")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> CheckAddress(string address)
        {
            address = address.Trim().ToLower();
            var zipCode = await db.ZipCodes.FirstOrDefaultAsync(x => address.StartsWith(x.UnitNumber.Trim().ToLower() + " " + x.StreetName.Trim().ToLower()) ||
                        address.StartsWith(x.UnitNumber.Trim().ToLower() + "," + x.StreetName.Trim().ToLower()) ||
                        address.StartsWith(x.UnitNumber.Trim().ToLower() + ", " + x.StreetName.Trim().ToLower()) ||
                        address.StartsWith(x.UnitNumber.Trim().ToLower() + " ," + x.StreetName.Trim().ToLower()));
            if (zipCode == null)
            {
                return NotFound();
            }

            return Ok(zipCode);
        }

        [Route("countries")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Countries()
        {
            var result = db.Countries.Select(x => new
            {
                x.CountryCode,
                x.CountryName
            }).ToList();
            return Ok(result);
        }

        [Route("track")]
        [HttpPost]
        public async Task<IHttpActionResult> TrackLocation(TrackLocationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    Code = 100,
                    Message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage))
                });
            }

            var location = new TrackLocation()
            {
                ConsignmentNumber = model.ConsignmentNumber,
                JobId = model.JobId,
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                CreatedOn = DateTime.Now,
                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
            };

            try
            {
                db.TrackLocations.Add(location);
                db.SaveChanges();
                return Ok(new
                {
                    Code = 0
                });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(new
                {
                    Code = 500,
                    Message = "Errors occur in processing request. Please contact to administrator to resolve."
                });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
