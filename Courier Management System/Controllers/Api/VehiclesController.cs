﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Courier_Management_System.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/vehicles")]
    public class VehiclesController : ApiController
    {
        #region Private

        private CourierDBEntities db = new CourierDBEntities();
        
        #endregion

        [Route("list")]
        [HttpPost]
        public async Task<IHttpActionResult> List()
        {
            var list = (from vehicle in db.Vehicles
                        where !vehicle.IsDeleted && vehicle.IsActive
                        select new
                        {
                            vehicle.Id,
                            vehicle.Capacity,
                            vehicle.CostPerKilometer,
                            vehicle.DistanceLimit,
                            vehicle.DrivingTimeLimit,
                            vehicle.FixedCostPerTrip,
                            vehicle.VehicleNumber,
                            vehicle.WorkStartTime,
                            vehicle.WorkingTimeLimit
                        }).ToList();

            return Ok(list);
        }
    }
}
