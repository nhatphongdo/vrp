﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using Microsoft.AspNet.Identity;

namespace Courier_Management_System.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/paymentaccounts")]
    public class PaymentAccountsController : ApiController
    {
        private CourierDBEntities db = new CourierDBEntities();

        [Route("list")]
        [HttpPost]
        public async Task<IHttpActionResult> List()
        {
            var username = User.Identity.GetUserName();
            var paymentAccounts = db.PaymentAccounts.Where(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase))
                .Select(x => new
                {
                    x.Id,
                    x.CardNumber,
                    x.AccountType
                })
                .ToList();
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));

            if (customer == null)
            {
                return Ok(new
                {
                    Code = 404,
                    Message = "Cannot find related customer information."
                });
            }

            return Ok(new
            {
                Code = 0,
                Credit = customer.Credit,
                PaymentType = customer.PaymentType.PaymentTypeName,
                //PaymentAccounts = paymentAccounts
            });
        }

        [Route("add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(PaymentAccountViewModel model)
        {
            var username = User.Identity.GetUserName();
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));

            if (customer == null)
            {
                return Ok(new
                {
                    Code = 404,
                    Message = "Cannot find related customer information."
                });
            }

            if (ModelState.IsValid)
            {
                // Save to DB
                var index = string.IsNullOrEmpty(customer.CustomerName) ? 0 : customer.CustomerName.IndexOf(' ');
                var firstName = string.IsNullOrEmpty(customer.CustomerName) ? "" : customer.CustomerName.Substring(0, index);
                var lastName = string.IsNullOrEmpty(customer.CustomerName) ? "" : customer.CustomerName.Substring(index + 1);

                // Save to Paypal
                var paypalAccessToken = PaypalServices.GetAccessToken();
                if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                {
                    // Not token
                    return Ok(new
                    {
                        Code = 500,
                        Message = "We cannot process your request right now. Please try again after few minutes."
                    });
                }
                else
                {
                    var result = PaypalServices.SaveCreditCard(paypalAccessToken,
                        model.AccountType == (int) PaymentAccountTypes.VisaCard ? "visa" : "mastercard",
                        model.CardNumber, model.ExpireMonth.GetValueOrDefault(), model.ExpireYear.GetValueOrDefault(),
                        firstName, lastName, model.CardCVV);

                    if (result.state.Equals("ok", StringComparison.OrdinalIgnoreCase))
                    {
                        var account = new PaymentAccount()
                        {
                            AccountType = model.AccountType,
                            CardNumber = result.number,
                            Currency = "USD",
                            PaypalCardId = result.id,
                            IsDeleted = false,
                            Owner = username,
                            CreatedOn = DateTime.Now,
                            CreatedBy = username,
                            UpdatedOn = DateTime.Now,
                            UpdatedBy = username
                        };

                        db.PaymentAccounts.Add(account);
                        try
                        {
                            db.SaveChanges();
                            return Ok(new
                            {
                                Code = 0
                            });
                        }
                        catch (Exception exc)
                        {
                            LogHelper.Log(exc);
                            return Ok(new
                            {
                                Code = 500,
                                Message =
                                    "Errors occur in processing request. Please contact to Customer Care for support."
                            });
                        }
                    }
                    else
                    {
                        return Ok(new
                        {
                            Code = 500,
                            Message = "We cannot process your request right now. Please try again after few minutes."
                        });
                    }
                }
            }
            else
            {
                return Ok(new
                {
                    Code = 500,
                    Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault()
                });
            }
        }
    }
}
