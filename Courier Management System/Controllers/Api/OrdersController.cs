﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Braintree;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/orders")]
    public class OrdersController : ApiController
    {
        private CourierDBEntities db = new CourierDBEntities();

        [Route("producttypes")]
        [HttpPost]
        public async Task<IHttpActionResult> ProductTypes()
        {
            var productTypes = db.ProductTypes.Where(x => !x.IsDeleted)
                                 .Select(
                                         x => new
                                              {
                                                  x.Id,
                                                  x.ProductTypeName,
                                                  x.Description,
                                                  Sizes = x.Sizes
                                                           .Where(s => !s.IsDeleted)
                                                           .Select(
                                                                   s => new
                                                                        {
                                                                            s.Id,
                                                                            s.Description,
                                                                            s.FromWeight,
                                                                            s.ToWeight,
                                                                            s.FromLength,
                                                                            s.ToLength,
                                                                            s.SizeName,
                                                                            Services = x.Services
                                                                                        .Where(
                                                                                               ser => !ser.IsDeleted && ser.IsActive &&
                                                                                                      (string.IsNullOrEmpty(ser.LimitToSizes) ||
                                                                                                       (ser.LimitToSizes + ",").Contains(s.Id.ToString() + ",")))
                                                                                        .Select(
                                                                                                ser => new
                                                                                                       {
                                                                                                           ser.Id,
                                                                                                           ser.ServiceName,
                                                                                                           ser.Description,
                                                                                                           ser.Cost,
                                                                                                           ser.IsDeliveryDateAllow,
                                                                                                           ser.IsDeliveryTimeAllow,
                                                                                                           ser.IsPickupTimeAllow,
                                                                                                           ser.IsPickupDateAllow,
                                                                                                           ser.AvailableDeliveryDateRange,
                                                                                                           ser.AvailableDeliveryTimeSlots,
                                                                                                           ser.AvailablePickupDateRange,
                                                                                                           ser.AvailablePickupTimeSlots,
                                                                                                           PickupTimeSlots = db.TimeSlots
                                                                                                                               .Where(
                                                                                                                                      t => !t.IsDeleted &&
                                                                                                                                           ("," + ser.AvailablePickupTimeSlots + ",").Contains("," + t.Id.ToString() + ","))
                                                                                                                               .OrderBy(t => t.FromTime)
                                                                                                                               .Select(
                                                                                                                                       t => new
                                                                                                                                            {
                                                                                                                                                t.Id,
                                                                                                                                                t.TimeSlotName,
                                                                                                                                                t.FromTime,
                                                                                                                                                t.ToTime,
                                                                                                                                                t.IsAllowPreOrder
                                                                                                                                            }).ToList(),
                                                                                                           DeliveryTimeSlots = db.TimeSlots
                                                                                                                                 .Where(
                                                                                                                                        t => !t.IsDeleted &&
                                                                                                                                             ("," + ser.AvailableDeliveryTimeSlots + ",").Contains(
                                                                                                                                                                                                   "," + t.Id.ToString() +
                                                                                                                                                                                                   ","))
                                                                                                                                 .OrderBy(t => t.FromTime)
                                                                                                                                 .Select(
                                                                                                                                         t => new
                                                                                                                                              {
                                                                                                                                                  t.Id,
                                                                                                                                                  t.TimeSlotName,
                                                                                                                                                  t.FromTime,
                                                                                                                                                  t.ToTime,
                                                                                                                                                  t.IsAllowPreOrder
                                                                                                                                              }).ToList()
                                                                                                       })
                                                                                        .ToList()
                                                                        }).ToList()
                                              })
                                 .ToList();

            return Ok(
                      new
                      {
                          CloseWindowTime = SettingProvider.CloseWindowTime,
                          ProductTypes = productTypes
                      });
        }

        [Route("sizes")]
        [HttpPost]
        public async Task<IHttpActionResult> Sizes(SizeRequestViewModel model)
        {
            var sizes = db.Sizes.Where(x => !x.IsDeleted && x.ProductTypeId == model.ProductTypeId)
                          .Select(
                                  x => new
                                       {
                                           x.Id,
                                           x.SizeName,
                                           x.FromLength,
                                           x.ToLength,
                                           x.FromWeight,
                                           x.ToWeight,
                                           x.Description
                                       })
                          .ToList();
            return Ok(sizes);
        }

        [Route("services")]
        [HttpPost]
        public async Task<IHttpActionResult> Services(ServiceRequestViewModel model)
        {
            var services =
                db.Services.Where(x => !x.IsDeleted && x.IsActive && (string.IsNullOrEmpty(x.LimitToSizes) || (x.LimitToSizes + ",").Contains(model.SizeId.ToString() + ",")) && x.ProductTypeId == model.ProductTypeId)
                  .Select(
                          x => new
                               {
                                   x.Id,
                                   x.ServiceName,
                                   x.Description,
                                   x.Cost,
                                   x.IsDeliveryDateAllow,
                                   x.IsDeliveryTimeAllow,
                                   x.IsPickupTimeAllow,
                                   x.IsPickupDateAllow,
                                   x.AvailableDeliveryDateRange,
                                   x.AvailableDeliveryTimeSlots,
                                   x.AvailablePickupDateRange,
                                   x.AvailablePickupTimeSlots
                               })
                  .ToList();
            return Ok(services);
        }

        [Route("price")]
        [HttpPost]
        public async Task<IHttpActionResult> Price(PriceRequestViewModel model)
        {
            return Ok(OrderHelper.CalculatePrice(model));
        }

        [Route("timeoptions/{serviceId}")]
        [HttpPost]
        public async Task<IHttpActionResult> TimeOptions(int? serviceId)
        {
            var timeSlots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();

            var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == serviceId);
            if (service == null)
            {
                return NotFound();
            }

            var pickupTimeSlots = new List<System.Web.Mvc.SelectListItem>();
            if (!string.IsNullOrEmpty(service.AvailablePickupTimeSlots))
            {
                pickupTimeSlots.AddRange(
                                         from item in
                                         service.AvailablePickupTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                         select timeSlots.FirstOrDefault(x => x.Id.ToString() == item)
                                         into timeSlot
                                         orderby timeSlot.FromTime
                                         where timeSlot != null
                                         select new System.Web.Mvc.SelectListItem()
                                                {
                                                    Text =
                                                        timeSlot.TimeSlotName + " (" + timeSlot.FromTime.ToString("HH:mm") + " - " +
                                                        timeSlot.ToTime.ToString("HH:mm") + ")",
                                                    Value = timeSlot.Id.ToString()
                                                });
            }

            var deliveryTimeSlots = new List<System.Web.Mvc.SelectListItem>();
            if (!string.IsNullOrEmpty(service.AvailableDeliveryTimeSlots))
            {
                deliveryTimeSlots.AddRange(
                                           from item in
                                           service.AvailableDeliveryTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                           select timeSlots.FirstOrDefault(x => x.Id.ToString() == item)
                                           into timeSlot
                                           orderby timeSlot.FromTime
                                           where timeSlot != null
                                           select new System.Web.Mvc.SelectListItem()
                                                  {
                                                      Text =
                                                          timeSlot.TimeSlotName + " (" + timeSlot.FromTime.ToString("HH:mm") + " - " +
                                                          timeSlot.ToTime.ToString("HH:mm") + ")",
                                                      Value = timeSlot.Id.ToString()
                                                  });
            }

            return Ok(
                      new
                      {
                          service.IsPickupTimeAllow,
                          service.IsPickupDateAllow,
                          service.IsDeliveryTimeAllow,
                          service.IsDeliveryDateAllow,
                          AvailablePickupTimes = pickupTimeSlots,
                          service.AvailablePickupDateRange,
                          AvailableDeliveryTimes = deliveryTimeSlots,
                          service.AvailableDeliveryDateRange
                      });
        }

        [Route("uploadSignature")]
        [HttpPost]
        public async Task<IHttpActionResult> UploadSignature()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
            if (file != null && file.ContentLength > 0)
            {
                // Check folder
                if (!Directory.Exists(
                                      HttpContext.Current.Server.MapPath(
                                                                         PathHelper.OrderUploadBaseDirectory +
                                                                         PathHelper.SignaturesDirectory)))
                {
                    Directory.CreateDirectory(
                                              HttpContext.Current.Server.MapPath(
                                                                                 PathHelper.OrderUploadBaseDirectory +
                                                                                 PathHelper.SignaturesDirectory));
                }

                var resultFileName = $"{Path.GetRandomFileName()}_{file.FileName}";
                var resultFileUrl = PathHelper.OrderUploadBaseDirectory + PathHelper.SignaturesDirectory + resultFileName;
                var resultFilePath = HttpContext.Current.Server.MapPath(resultFileUrl);
                file.SaveAs(resultFilePath);

                var name = file.FileName;
                var url = Url.Content(resultFileUrl).ToAbsoluteUrl();
                var sizeInKilobytes = file.ContentLength / 1024;
                var sizeText = sizeInKilobytes.ToString() + " KB";

                return Ok(
                          new
                          {
                              Code = 0,
                              Path = url
                          });
            }

            return Ok(
                      new
                      {
                          Code = 400,
                          Message = "There is no upload file."
                      });
        }

        [Route("uploadProof")]
        [HttpPost]
        public async Task<IHttpActionResult> UploadProof()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
            if (file != null && file.ContentLength > 0)
            {
                // Check folder
                if (
                    !Directory.Exists(
                                      HttpContext.Current.Server.MapPath(
                                                                         PathHelper.OrderUploadBaseDirectory +
                                                                         PathHelper.ProofsDirectory)))
                {
                    Directory.CreateDirectory(
                                              HttpContext.Current.Server.MapPath(
                                                                                 PathHelper.OrderUploadBaseDirectory +
                                                                                 PathHelper.ProofsDirectory));
                }

                var resultFileName = $"{Path.GetRandomFileName()}_{file.FileName}";
                var resultFileUrl = PathHelper.OrderUploadBaseDirectory + PathHelper.ProofsDirectory + resultFileName;
                var resultFilePath = HttpContext.Current.Server.MapPath(resultFileUrl);
                file.SaveAs(resultFilePath);

                var name = file.FileName;
                var url = Url.Content(resultFileUrl).ToAbsoluteUrl();
                var sizeInKilobytes = file.ContentLength / 1024;
                var sizeText = sizeInKilobytes.ToString() + " KB";

                return Ok(
                          new
                          {
                              Code = 0,
                              Path = url
                          });
            }

            return Ok(
                      new
                      {
                          Code = 400,
                          Message = "There is no upload file."
                      });
        }

        [Route("arriveConfirm")]
        [HttpPost]
        public async Task<IHttpActionResult> ArriveConfirm(OrderArriveConfirmViewModel model)
        {
            var job = db.Jobs.FirstOrDefault(x => !x.IsDeleted && x.Id == model.JobId);
            if (job == null)
            {
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Cannot find the corresponding job."
                          });
            }

            if (!string.IsNullOrEmpty(model.ProofPath))
            {
                model.ProofPath = model.ProofPath.Trim('\n', '\r');
            }

            var consignmentNumbers = model.ConsignmentNumber.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orders = db.Orders.Where(x => !x.IsDeleted && consignmentNumbers.Contains(x.ConsignmentNumber.ToLower())).ToList();

            foreach (var order in orders)
            {
                if ((model.IsPickup && (order.Status != (int) OrderStatuses.ItemHasBeingPicked && order.Status != (int) OrderStatuses.SpecialOrderForPickup)) ||
                    (!model.IsPickup && (order.Status != (int) OrderStatuses.ItemOutDepot && order.Status != (int) OrderStatuses.SpecialOrderForDelivery)))
                {
                    return Ok(
                              new
                              {
                                  Code = 500,
                                  Message = "This order is not in correct status. Please contact to administrator to resolve."
                              });
                }

                ExtraRequest request = null;

                if (model.IsPickup)
                {
                    order.Status = model.IsSuccessful
                                       ? (int) OrderStatuses.ItemCollected
                                       : (int) OrderStatuses.PickupFailed;
                    order.CollectedOn = DateTime.Now;
                    order.CollectedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    order.FromSignature = model.ProofPath;

                    if (!model.IsSuccessful)
                    {
                        // Generate failed ticket
                        request = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.RePickupRequest && !x.IsDeleted);
                        if (request == null)
                        {
                            request = new ExtraRequest
                                      {
                                          CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          OrderId = order.Id,
                                          RequestType = (int) ExtraRequestType.RePickupRequest,
                                          CreatedOn = DateTime.Now,
                                          IsDeleted = false,
                                          IsVisited = false,
                                          UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          UpdatedOn = DateTime.Now
                                      };
                            db.ExtraRequests.Add(request);
                        }

                        ++job.TasksCompleted;
                    }
                    else
                    {
                        // Generate delivery ticket
                        request = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.DeliveryRequest && !x.IsDeleted);
                        if (request == null)
                        {
                            request = new ExtraRequest
                                      {
                                          CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          OrderId = order.Id,
                                          RequestType = (int) ExtraRequestType.DeliveryRequest,
                                          CreatedOn = DateTime.Now,
                                          IsDeleted = false,
                                          IsVisited = false,
                                          UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          UpdatedOn = DateTime.Now
                                      };
                            db.ExtraRequests.Add(request);
                        }

                        // Pickup completed
                        ++job.TasksCompleted;
                    }
                }
                else
                {
                    order.Status = model.IsSuccessful
                                       ? (int) OrderStatuses.ItemDelivered
                                       : (int) OrderStatuses.DeliveryFailed;
                    order.DeliveredOn = DateTime.Now;
                    order.DeliveredBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    order.ToSignature = model.ProofPath;

                    if (!model.IsSuccessful)
                    {
                        // Generate failed ticket
                        request = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.ReDeliveryRequest && !x.IsDeleted);
                        if (request == null)
                        {
                            request = new ExtraRequest
                                      {
                                          CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          OrderId = order.Id,
                                          RequestType = (int) ExtraRequestType.ReDeliveryRequest,
                                          CreatedOn = DateTime.Now,
                                          IsDeleted = false,
                                          IsVisited = false,
                                          UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          UpdatedOn = DateTime.Now
                                      };
                            db.ExtraRequests.Add(request);
                        }

                        ++job.TasksCompleted;
                    }
                    else
                    {
                        // Deliver completed
                        ++job.TasksCompleted;
                    }
                }

                order.UpdatedOn = DateTime.Now;
                order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";

                // COD
                if (model.IsSuccessful && !model.IsPickup && model.Rating > 0)
                {
                    foreach (var cod in order.CodOptionOfOrders)
                    {
                        cod.CollectedAmount = cod.Amount;
                        cod.IsCollected = true;
                        cod.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        cod.UpdatedOn = DateTime.Now;
                    }
                }

                var track = new OrderTracking
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                PickupTimeSlotId = order.PickupTimeSlotId,
                                PickupDate = order.PickupDate,
                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                DeliveryDate = order.DeliveryDate,
                                BinCode = order.BinCode,
                                Status = order.Status
                            };
                db.OrderTrackings.Add(track);

                // Job History
                var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                var jobHistory =
                    db.JobHistories.Where(x => !x.IsDeleted && x.JobId == job.Id && x.IsPickup == model.IsPickup && x.ConsignmentNumber == order.ConsignmentNumber).OrderByDescending(x => x.Id).FirstOrDefault();
                if (jobHistory == null)
                {
                    jobHistory = new JobHistory()
                                 {
                                     ConsignmentNumber = order.ConsignmentNumber,
                                     CreatedBy = username,
                                     CreatedOn = DateTime.Now,
                                     UpdatedBy = username,
                                     UpdatedOn = DateTime.Now,
                                     JobId = job.Id,
                                     IsDeleted = false,
                                     IsPickup = model.IsPickup
                                 };
                    db.JobHistories.Add(jobHistory);
                }
                jobHistory.Rating = model.Rating;
                jobHistory.CompletedBy = username;
                jobHistory.CompletedOn = DateTime.Now;
                jobHistory.IsSuccessful = model.IsSuccessful;
                jobHistory.DeductionAmount = 0;
                jobHistory.PayRateId = null;
                jobHistory.Payment = 0;

                var driver = db.Contractors.FirstOrDefault(x => !x.IsDeleted && x.IsActive && x.UserName.ToLower() == username.ToLower());
                if (driver != null)
                {
                    var payscheme = db.PaySchemes.FirstOrDefault(x => !x.IsDeleted && x.Id == driver.PayRateId);
                    var payrate = payscheme?.PayRates.FirstOrDefault(x => !x.IsDeleted && x.ServiceId == order.ServiceId && x.SizeId == order.SizeId);
                    if (payrate != null)
                    {
                        jobHistory.PayRateId = payrate.Id;
                        jobHistory.Payment = model.IsSuccessful ? payrate.Amount : 0;
                    }
                }
                else
                {
                    var staff = db.Staffs.FirstOrDefault(x => !x.IsDeleted && x.IsActive && x.UserName.ToLower() == username.ToLower());
                    if (staff != null)
                    {
                        var payscheme = db.PaySchemes.FirstOrDefault(x => !x.IsDeleted && x.Id == staff.PayRateId);
                        var payrate = payscheme?.PayRates.FirstOrDefault(x => !x.IsDeleted && x.ServiceId == order.ServiceId && x.SizeId == order.SizeId);
                        if (payrate != null)
                        {
                            jobHistory.PayRateId = payrate.Id;
                            jobHistory.Payment = model.IsSuccessful ? payrate.Amount : 0;
                        }
                    }
                }

                try
                {
                    db.SaveChanges();

                    // Check if this is the last order in job
                    //if (job.TasksCompleted >= job.TotalPickups + job.TotalDeliveries)
                    if (db.JobHistories.Where(x => !x.IsDeleted && x.JobId == job.Id && x.CompletedBy == username).All(x => x.CompletedOn.HasValue))
                    {
                        job.CompletedOn = DateTime.Now;
                    }
                    db.SaveChanges();

                    if (request != null)
                    {
                        request.Code = OrderHelper.GenerateExtraRequestNumber(request.Id);
                        db.SaveChanges();
                    }

                    if (model.IsPickup && !model.IsSuccessful)
                    {
                        OrderHelper.SendFailedPickupEmail(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), order, request);
                        OrderHelper.SendFailedPickupSms(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), order, request);
                    }
                    else if (model.IsPickup && model.IsSuccessful)
                    {
                        OrderHelper.SendOrderCollectedSms(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), order, request);
                    }
                    else if (!model.IsPickup && !model.IsSuccessful)
                    {
                        //OrderHelper.SendFailedDeliveryEmail(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), order, request);
                        OrderHelper.SendFailedDeliverySms(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), order, request);
                    }
                    else if (!model.IsPickup && model.IsSuccessful)
                    {
                        OrderHelper.SendOrderDeliveredSms(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), order);
                    }
                }
                catch (Exception exc)
                {
                    LogHelper.Log(exc);
                    return Ok(
                              new
                              {
                                  Code = 500,
                                  Message = "Errors occur in processing request. Please contact to administrator to resolve."
                              });
                }
            }

            return Ok(
                      new
                      {
                          Code = 0,
                      });
        }

        [AllowAnonymous]
        [Route("tracking/{consignmentNumbers}")]
        [HttpPost]
        public async Task<IHttpActionResult> Tracking(string consignmentNumbers)
        {
            if (string.IsNullOrEmpty(consignmentNumbers))
            {
                return Ok(
                          new
                          {
                              Code = 404,
                              Message = "Cannot find orders corresponding to these consignment numbers"
                          });
            }

            // Check payment
            var orderIds = consignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orders = db.Orders
                           .Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower()))
                           .Select(
                                   x => new
                                        {
                                            x.Id,
                                            x.ConsignmentNumber,
                                            x.Status,
                                            x.CollectedOn,
                                            x.ConfirmedOn,
                                            x.CreatedOn,
                                            x.DeliveredOn,
                                            x.ProceededOn,
                                            x.ProceededOutOn,
                                            x.CanceledOn
                                        })
                           .ToList();
            return Ok(
                      new
                      {
                          Code = 0,
                          Orders = orders
                      });
        }

        [Route("book")]
        [HttpPost]
        public async Task<IHttpActionResult> Book(ApiBookOrderViewModel model)
        {
            try
            {
                DateTime dateTime;
                DateTime? pickupDate = null;
                DateTime? deliveryDate = null;
                if (DateTime.TryParseExact(model.PickupDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime))
                {
                    pickupDate = dateTime;
                }
                if (DateTime.TryParseExact(model.DeliveryDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime))
                {
                    deliveryDate = dateTime;
                }

                if (pickupDate.Value.DayOfWeek != DayOfWeek.Sunday && deliveryDate.Value.DayOfWeek != DayOfWeek.Sunday)
                {
                    var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.IsActive && x.Id == model.CustomerId);
                    if (customer == null)
                    {
                        return Ok(
                                  new
                                  {
                                      Code = 404,
                                      Message = "Cannot retrieve customer's information"
                                  });
                    }

                    var price = OrderHelper.CalculatePrice(
                                                           new PriceRequestViewModel()
                                                           {
                                                               ProductTypeId = model.ProductTypeId,
                                                               ServiceId = model.ServiceId,
                                                               CustomerId = model.CustomerId,
                                                               SizeId = model.SizeId,
                                                               AddOnCode = model.PromoCode
                                                           });
                    var priceTotal = price.Total;

                    //Remove limit Minimum Amount for iCommerce.
                    if (customer.Id == 20404)
                    {
                        priceTotal = price.BasePrice + price.AddOnPrice + price.DefaultPromotion + price.Surcharge - price.Promotion;
                    }

                    var order = new Order()
                    {
                        FromZipCode = model.FromZipCode,
                        FromName = model.FromName,
                        FromAddress = model.FromAddress,
                        FromMobilePhone = model.FromMobilePhone,
                        ToName = model.ToName,
                        ToZipCode = model.ToZipCode,
                        ToAddress = model.ToAddress,
                        ToMobilePhone = model.ToMobilePhone,
                        PaymentTypeId = customer.PaymentTypeId,
                        ServiceId = model.ServiceId,
                        ProductTypeId = model.ProductTypeId,
                        Remark = model.Remark,
                        Price = priceTotal,
                        Status = (int)OrderStatuses.OrderSubmitted,
                        CustomerId = model.CustomerId,
                        SizeId = model.SizeId,
                        PickupTimeSlotId = model.PickupTimeSlotId,
                        PickupDate = pickupDate,
                        DeliveryTimeSlotId = model.DeliveryTimeSlotId,
                        DeliveryDate = deliveryDate,
                        PromoCode = model.PromoCode,
                        OrderNumber = string.IsNullOrWhiteSpace(model.OrderNumber) ? "" : customer.CompanyPrefix + model.OrderNumber,
                        IsDeleted = false,
                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        CreatedOn = DateTime.Now,
                        UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        UpdatedOn = DateTime.Now
                    };

                    var service = db.Services.FirstOrDefault(x => x.Id == model.ServiceId);
                    if (service != null && !service.IsPickupDateAllow)
                    {
                        order.PickupDate = DateTime.Now.Date;
                    }
                    if (service != null && !service.IsDeliveryDateAllow)
                    {
                        order.DeliveryDate = order.PickupDate;
                    }

                    order.BinCode = OrderHelper.GenerateBinCode(order);

                    db.Orders.Add(order);

                    try
                    {
                        db.SaveChanges();

                        order.ConsignmentNumber = OrderHelper.GenerateConsignmentNumber(order.Id);

                        // Build QR code
                        var paymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                           x => new System.Web.Mvc.SelectListItem()
                                                                                           {
                                                                                               Text = x.PaymentTypeName,
                                                                                               Value = x.Id.ToString()
                                                                                           }).ToList();
                        var services = db.Services.Where(x => !x.IsDeleted).Select(
                                                                                   x => new System.Web.Mvc.SelectListItem()
                                                                                   {
                                                                                       Text = x.ServiceName,
                                                                                       Value = x.Id.ToString()
                                                                                   }).ToList();
                        var sizes = db.Sizes.Where(x => !x.IsDeleted).Select(
                                                                             x => new System.Web.Mvc.SelectListItem()
                                                                             {
                                                                                 Text = x.SizeName,
                                                                                 Value = x.Id.ToString()
                                                                             }).ToList();
                        var productTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                           x => new System.Web.Mvc.SelectListItem()
                                                                                           {
                                                                                               Text = x.ProductTypeName,
                                                                                               Value = x.Id.ToString()
                                                                                           }).ToList();
                        // Build QR Code
                        var bitmap = OrderHelper.GenerateQrCode(paymentTypes, services, productTypes, sizes, order);
                        order.QrCodePath = Url.Content(OrderHelper.SaveQrCode(order.ConsignmentNumber, bitmap));
                        //Build Bar Code
                        var barCode = OrderHelper.GenerateBarCode(order.ConsignmentNumber);
                        var barCodePath = OrderHelper.SaveBarCode(order.ConsignmentNumber, barCode);

                        var track = new OrderTracking
                        {
                            ConsignmentNumber = order.ConsignmentNumber,
                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            CreatedOn = DateTime.Now,
                            PickupTimeSlotId = order.PickupTimeSlotId,
                            PickupDate = order.PickupDate,
                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                            DeliveryDate = order.DeliveryDate,
                            BinCode = order.BinCode,
                            Status = order.Status
                        };
                        db.OrderTrackings.Add(track);

                        var mapCN = new ConsignmentNumber()
                        {
                            ConsignmentNumbers = order.ConsignmentNumber,
                            Id = Guid.NewGuid(),
                            CreatedOn = DateTime.Now
                        };
                        db.ConsignmentNumbers.Add(mapCN);

                        db.SaveChanges();

                        if (paymentTypes.FirstOrDefault(x => x.Value == customer.PaymentTypeId.ToString())?.Text?.ToLower().Contains("postpaid") == true)
                        {
                            // Check balance
                            if (customer.Credit < priceTotal)
                            {
                                return Ok(
                                          new
                                          {
                                              Code = 500,
                                              Message = "Sorry you have used up your credit limit. Please pay your outstanding amount due before placing order. Thank you."
                                          });
                            }

                            // For Post Paid
                            order.Status = (int)OrderStatuses.OrderConfirmed;
                            order.ConfirmedOn = DateTime.Now;
                            customer.Credit -= priceTotal;
                            // Save payment
                            var payment = new Payment()
                            {
                                Payer = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                AccountType = (int)PaymentTypes.PostPaid,
                                IsSuccessful = true,
                                PaymentAmount = priceTotal,
                                CreatedOn = DateTime.Now,
                            };
                            var orderInPayment = new OrdersOfPayment()
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                            };
                            payment.OrdersOfPayments.Add(orderInPayment);
                            db.Payments.Add(payment);

                            track = new OrderTracking
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                PickupTimeSlotId = order.PickupTimeSlotId,
                                PickupDate = order.PickupDate,
                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                DeliveryDate = order.DeliveryDate,
                                BinCode = order.BinCode,
                                Status = order.Status
                            };
                            db.OrderTrackings.Add(track);

                            db.SaveChanges();

                            OrderHelper.SendOrderEmail(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), new List<Order>() { order }, mapCN.Id.ToString());

                            return Ok(
                                      new
                                      {
                                          Code = 0,
                                          OrderNumber = order.ConsignmentNumber
                                      });
                        }
                        else
                        {
                            // For Pre Paid
                            if (model.PaymentAccountId == 0)
                            {
                                // Check balance
                                if (customer.Credit < priceTotal)
                                {
                                    return Ok(
                                              new
                                              {
                                                  Code = 500,
                                                  Message = "You don't have enough Credit to make payment. Please purchase more credit or use other method."
                                              });
                                }

                                order.Status = (int)OrderStatuses.OrderConfirmed;
                                order.ConfirmedOn = DateTime.Now;
                                customer.Credit -= priceTotal;
                                // Save payment
                                var payment = new Payment()
                                {
                                    Payer = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                    AccountType = (int)PaymentTypes.PrePaid,
                                    IsSuccessful = true,
                                    PaymentAmount = priceTotal,
                                    CreatedOn = DateTime.Now
                                };
                                var orderInPayment = new OrdersOfPayment()
                                {
                                    ConsignmentNumber = order.ConsignmentNumber,
                                };
                                payment.OrdersOfPayments.Add(orderInPayment);
                                db.Payments.Add(payment);

                                track = new OrderTracking
                                {
                                    ConsignmentNumber = order.ConsignmentNumber,
                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                    CreatedOn = DateTime.Now,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    BinCode = order.BinCode,
                                    Status = order.Status
                                };
                                db.OrderTrackings.Add(track);

                                db.SaveChanges();

                                OrderHelper.SendOrderEmail(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), new List<Order>() { order }, mapCN.Id.ToString());

                                return Ok(
                                          new
                                          {
                                              Code = 0,
                                              OrderNumber = order.ConsignmentNumber
                                          });
                            }
                            else
                            {
                                return Ok(
                                          new
                                          {
                                              Code = 300,
                                              OrderId = mapCN.Id.ToString()
                                          });
                                //var paymentAccount = db.PaymentAccounts.FirstOrDefault(x => !x.IsDeleted && x.Owner.Equals(User.Identity.IsAuthenticated ? User.Identity.Name : "", StringComparison.OrdinalIgnoreCase) && x.Id == model.PaymentAccountId);
                                //if (paymentAccount == null)
                                //{
                                //    return Ok(new
                                //    {
                                //        Code = 404,
                                //        Message = "You don't have this payment account. Please try with the other account."
                                //    });
                                //}

                                //var payment = new Payment()
                                //{
                                //    Orders = order.ConsignmentNumber,
                                //    Payer = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                //    IsSuccessful = false,
                                //    AccountType = (int) PaymentTypes.PrePaid,
                                //    PaymentAccountId = model.PaymentAccountId,
                                //    PaymentAmount = priceTotal,
                                //    CreatedOn = DateTime.Now
                                //};

                                //db.Payments.Add(payment);
                                //db.SaveChanges();

                                //// Call Paypal to pay
                                //var paypalAccessToken = PaypalServices.GetAccessToken();
                                //if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                                //{
                                //    // Not token
                                //    return Ok(new
                                //    {
                                //        Code = 500,
                                //        Message = "We cannot process your payment right now. Please try again after few minutes."
                                //    });
                                //}

                                //if (paymentAccount.AccountType == (int) PaymentAccountTypes.PaypalAccount)
                                //{
                                //    // This is paypal
                                //    return Ok(new
                                //    {
                                //        Code = 500,
                                //        Message = "Not support payment method"
                                //    });
                                //}
                                //else
                                //{
                                //    // This is credit card
                                //    PaypalServices.PayWithCreditCard(paypalAccessToken,
                                //        paymentAccount.PaypalCardId,
                                //        payment.PaymentAmount,
                                //        $"Pay for order {order.ConsignmentNumber} on Roadbull website.",
                                //        paymentAccount.Currency);

                                //    payment.IsSuccessful = true;

                                //    order.Status = (int) OrderStatuses.OrderConfirmed;
                                //    order.ConfirmedOn = DateTime.Now;

                                //    track = new OrderTracking
                                //    {
                                //        ConsignmentNumber = order.ConsignmentNumber,
                                //        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                //        CreatedOn = DateTime.Now,
                                //        PickupTimeSlotId = order.PickupTimeSlotId,
                                //        PickupDate = order.PickupDate,
                                //        DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                //        DeliveryDate = order.DeliveryDate,
                                //        BinCode = order.BinCode,
                                //        Status = order.Status
                                //    };
                                //    db.OrderTrackings.Add(track);

                                //    db.SaveChanges();

                                //    OrderHelper.SendOrderEmail(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), new List<Order>() { order }, mapCN.Id.ToString());

                                //    return Ok(new
                                //    {
                                //        Code = 0
                                //    });
                                //}
                            }
                        }
                    }
                    catch (PayPal.PaymentsException exc)
                    {
                        LogHelper.Log(exc);
                        return Ok(
                                  new
                                  {
                                      Code = 500,
                                      Message = exc.Details.message + "<br />" + (exc.Details.details != null ? string.Join("<br />", exc.Details.details.Select(x => x.issue)) : "")
                                  });
                    }
                    catch (Exception exc)
                    {
                        // Rollback
                        LogHelper.Log(exc);
                        return Ok(
                                  new
                                  {
                                      Code = 500,
                                      Message = "Errors occur in processing request. Please contact to Customer Care for support."
                                  });
                    }
                }
                else
                {
                    return Ok(
                                          new
                                          {
                                              Code = 500,
                                              Message = "Sorry the booking you have made is not within our operational days. Please revise your order. Thank you."
                                          });
                }
            }
            catch (Exception exc)
            {
                // Rollback
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "Errors occur in processing request. Please contact to Customer Care for support."
                          });
            }
        }


        [Route("token")]
        [HttpPost]
        public async Task<IHttpActionResult> Token()
        {
            return Ok(
                      new
                      {
                          Token = PaypalServices.GetBrainTreeClientToken()
                      });
        }

        [Route("checkout")]
        [HttpPost]
        public async Task<IHttpActionResult> Checkout(CheckoutViewModel model)
        {
            var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id.ToString().Equals(model.OrderId, StringComparison.OrdinalIgnoreCase));

            if (mapDN == null)
            {
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help."
                          });
            }

            var orderIds = mapDN.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orders = db.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
            if (orders.Count == 0)
            {
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help."
                          });
            }

            var total = orders.Sum(x => x.Price);
            total = Math.Max(SettingProvider.MinimumOrderAmount, total);

            try
            {
                var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                // Create payment account to save data
                var nonce = model.PaymentMethodNonce;
                var paymentAccount = new PaymentAccount
                                     {
                                         Owner = username,
                                         AccountType = (int) PaymentAccountTypes.BrainTree,
                                         PaypalCardId = nonce,
                                         IsDeleted = false,
                                         CreatedOn = DateTime.Now,
                                         CreatedBy = username,
                                         UpdatedOn = DateTime.Now,
                                         UpdatedBy = username
                                     };
                db.PaymentAccounts.Add(paymentAccount);
                db.SaveChanges();

                // Save payment
                var payment = new Payment()
                              {
                                  Payer = username,
                                  IsSuccessful = false,
                                  AccountType = (int) PaymentTypes.PrePaid,
                                  PaymentAccountId = paymentAccount.Id,
                                  PaymentAmount = total,
                                  CreatedOn = DateTime.Now
                              };
                foreach (var consignmentNumber in mapDN.ConsignmentNumbers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var orderInPayment = new OrdersOfPayment()
                                         {
                                             ConsignmentNumber = consignmentNumber,
                                         };
                    payment.OrdersOfPayments.Add(orderInPayment);
                }

                db.Payments.Add(payment);
                db.SaveChanges();

                // Call BrainTree to pay
                var result = PaypalServices.PayWithBrainTree(payment.PaymentAmount, nonce);

                if (result.IsSuccess())
                {
                    payment.IsSuccessful = true;

                    foreach (var order in orders)
                    {
                        order.Status = (int) OrderStatuses.OrderConfirmed;
                        order.ConfirmedOn = DateTime.Now;

                        var track = new OrderTracking
                                    {
                                        ConsignmentNumber = order.ConsignmentNumber,
                                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                        CreatedOn = DateTime.Now,
                                        PickupTimeSlotId = order.PickupTimeSlotId,
                                        PickupDate = order.PickupDate,
                                        DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                        DeliveryDate = order.DeliveryDate,
                                        BinCode = order.BinCode,
                                        Status = order.Status
                                    };
                        db.OrderTrackings.Add(track);
                    }

                    db.SaveChanges();

                    OrderHelper.SendOrderEmail(new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext), orders, model.OrderId);

                    return Ok(
                              new
                              {
                                  Code = 0
                              });
                }
                else
                {
                    var message = "We cannot process your payment right now. Please try again after few minutes.";
                    if (result.Errors != null && result.Errors.Count > 0)
                    {
                        message = string.Join("\n", result.Errors.DeepAll().Select(x => x.Message));
                    }
                    else if (result.Transaction != null)
                    {
                        if (result.Transaction.Status == TransactionStatus.PROCESSOR_DECLINED)
                        {
                            message = string.IsNullOrEmpty(result.Transaction.ProcessorResponseText)
                                          ? result.Transaction.AdditionalProcessorResponse
                                          : result.Transaction.ProcessorResponseText;
                        }
                        else if (result.Transaction.Status == TransactionStatus.SETTLEMENT_DECLINED)
                        {
                            message = result.Transaction.ProcessorSettlementResponseText;
                        }
                        else if (result.Transaction.Status == TransactionStatus.GATEWAY_REJECTED)
                        {
                            message = result.Transaction.GatewayRejectionReason.ToString();
                        }
                    }

                    return Ok(
                              new
                              {
                                  Code = 500,
                                  Message = message
                              });
                }
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(
                          new
                          {
                              Code = 500,
                              Message = "We cannot process your payment right now. Please try again after few minutes."
                          });
            }
        }

        [Route("info/{consignmentNumber}")]
        [HttpPost]
        public async Task<IHttpActionResult> Info(string consignmentNumber)
        {
            if (string.IsNullOrEmpty(consignmentNumber))
            {
                return Ok(
                          new
                          {
                              Code = 404,
                              Message = "Cannot find orders corresponding to this consignment number"
                          });
            }

            var order = db.Orders
                          .Where(x => !x.IsDeleted && x.ConsignmentNumber.ToLower() == consignmentNumber.ToLower())
                          .Select(
                                  x => new
                                       {
                                           Size = x.Size.SizeName,
                                           CodOptionOfOrders = x.CodOptionOfOrders
                                                                .Where(o => !o.IsDeleted)
                                                                .Select(
                                                                        o => new
                                                                             {
                                                                                 Amount = o.Amount,
                                                                                 CodOption = new
                                                                                             {
                                                                                                 Type = o.CodOption.Type,
                                                                                                 Description = o.CodOption.Description
                                                                                             }
                                                                             })
                                                                .ToList(),
                                           IsExchange = x.IsExchange,
                                           OperatorRemark = x.OperatorRemark,
                                           ExtraRequests = x.ExtraRequests
                                                            .Where(r => !r.IsDeleted)
                                                            .OrderByDescending(r => r.CreatedOn)
                                                            .Select(
                                                                    r => new
                                                                         {
                                                                             r.RequestContent,
                                                                             r.RequestType
                                                                         })
                                                            .ToList(),
                                           Price = x.Price
                                       })
                          .FirstOrDefault();
            return Ok(
                      new
                      {
                          Code = 0,
                          Order = order
                      });
        }

        [Route("remark")]
        [HttpPost]
        public async Task<IHttpActionResult> Remark(OrdersRemarkViewModel model)
        {
            if (string.IsNullOrEmpty(model.ConsignmentNumbers))
            {
                return Ok(
                          new
                          {
                              Code = 0,
                              Orders = new List<dynamic>()
                          });
            }

            var consignmentNumbers = model.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orders = db.Orders
                           .Where(x => !x.IsDeleted && consignmentNumbers.Contains(x.ConsignmentNumber.ToLower()) && !string.IsNullOrEmpty(x.OperatorRemark))
                           .Select(
                                   x => new
                                        {
                                            x.ConsignmentNumber,
                                            x.OperatorRemark,
                                        })
                           .ToList();
            return Ok(
                      new
                      {
                          Code = 0,
                          Orders = orders
                      });
        }
    }
}
