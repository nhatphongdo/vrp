﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using Newtonsoft.Json;

namespace Courier_Management_System.Controllers.Api
{
    [Authorize(Roles = AccountHelper.ManagerRole)]
    [RoutePrefix("api/inventory")]
    public class InventoryController : ApiController
    {
        private CourierDBEntities db = new CourierDBEntities();

        [Route("settings")]
        [HttpPost]
        public async Task<IHttpActionResult> Settings()
        {
            return Ok(new
            {
                Code = 0,
                OperatorsEmail = SettingProvider.OperatorsEmail
            });
        }

        [Route("collected")]
        [HttpPost]
        public async Task<IHttpActionResult> Collected()
        {
            var orders = await db.Orders
                .Where(x => x.Status == (int)OrderStatuses.ItemCollected && !x.IsDeleted)
                .Select(x => new
                {
                    x.ConsignmentNumber
                })
                .ToListAsync();
            return Ok(new
            {
                Code = 0,
                Result = orders
            });
        }

        [Route("putinto/{consignmentNumber}")]
        [HttpPost]
        public async Task<IHttpActionResult> PutInto(string consignmentNumber)
        {
            if (consignmentNumber == null)
            {
                consignmentNumber = "";
            }

            var order = await db.Orders
                .FirstOrDefaultAsync(x => x.ConsignmentNumber.ToLower() == consignmentNumber.ToLower() && !x.IsDeleted);

            if (order == null)
            {
                return Ok(new
                {
                    Code = 404,
                    Message = "Cannot find order corresponding to this consignment number"
                });
            }

            if (order.Status != (int)OrderStatuses.ItemCollected)
            {
                return Ok(new
                {
                    Code = 403,
                    Message = "Item is not in the correct status"
                });
            }

            order.Status = (int)OrderStatuses.ItemInDepot;
            order.ProceededOn = DateTime.Now;
            order.ProceededBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";

            // Add track
            var track = new OrderTracking
            {
                ConsignmentNumber = order.ConsignmentNumber,
                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                CreatedOn = DateTime.Now,
                PickupTimeSlotId = order.PickupTimeSlotId,
                PickupDate = order.PickupDate,
                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                DeliveryDate = order.DeliveryDate,
                BinCode = order.BinCode,
                Status = (int)OrderStatuses.ItemInDepot
            };
            db.OrderTrackings.Add(track);

            try
            {
                await db.SaveChangesAsync();
                return Ok(new
                {
                    Code = 0
                });
            }
            catch (Exception exc)
            {
                return Ok(new
                {
                    Code = 500,
                    Message = exc.Message
                });
            }
        }

        [Route("order/{consignmentNumber}")]
        [HttpPost]
        public async Task<IHttpActionResult> Order(string consignmentNumber)
        {
            if (consignmentNumber == null)
            {
                consignmentNumber = "";
            }

            var order = await db.Orders
                .FirstOrDefaultAsync(x => x.ConsignmentNumber.ToLower() == consignmentNumber.ToLower() && !x.IsDeleted);

            if (order == null)
            {
                return Ok(new
                {
                    Code = 404,
                    Message = "Cannot find order corresponding to this consignment number"
                });
            }

            // Load sizes
            var sizes = await db.Sizes.Where(x => !x.IsDeleted && x.ProductTypeId == order.ProductTypeId)
                .Select(x => new
                {
                    x.Id,
                    x.SizeName,
                    x.FromLength,
                    x.ToLength,
                    x.FromWeight,
                    x.ToWeight,
                    x.Description
                })
                .ToListAsync();

            // Load times
            var timeSlots = await db.TimeSlots.Where(x => !x.IsDeleted).ToListAsync();

            //var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == order.ServiceId);
            var service = order.Service;

            var pickupTimeSlots = new List<dynamic>();
            if (!string.IsNullOrEmpty(service.AvailablePickupTimeSlots))
            {
                pickupTimeSlots.AddRange(
                    from item in
                        service.AvailablePickupTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    select timeSlots.FirstOrDefault(x => x.Id.ToString() == item)
                    into timeSlot
                    orderby timeSlot.FromTime
                    where timeSlot != null
                    select new
                    {
                        Text = timeSlot.TimeSlotName + " (" + timeSlot.FromTime.ToString("HH:mm") + " - " +
                            timeSlot.ToTime.ToString("HH:mm") + ")",
                        Value = timeSlot.Id.ToString(),
                        timeSlot.BinCode
                    });
            }

            var deliveryTimeSlots = new List<dynamic>();
            if (!string.IsNullOrEmpty(service.AvailableDeliveryTimeSlots))
            {
                deliveryTimeSlots.AddRange(
                    from item in
                        service.AvailableDeliveryTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    select timeSlots.FirstOrDefault(x => x.Id.ToString() == item)
                    into timeSlot
                    orderby timeSlot.FromTime
                    where timeSlot != null
                    select new
                    {
                        Text = timeSlot.TimeSlotName + " (" + timeSlot.FromTime.ToString("HH:mm") + " - " +
                            timeSlot.ToTime.ToString("HH:mm") + ")",
                        Value = timeSlot.Id.ToString(),
                        timeSlot.BinCode
                    });
            }

            var job = await db.Jobs.Where(x => !x.IsDeleted && (x.Routes.Contains("\"" + order.Id + "\"") ||
                                                               x.Routes.Contains("\"" + order.Id + ",") ||
                                                               x.Routes.Contains("," + order.Id + ",") ||
                                                               x.Routes.Contains("," + order.Id + "\"")))
                .FirstOrDefaultAsync();
            var driverName = "";
            if (job != null)
            {
                if (job.DriverType == (int)AccountTypes.Staff)
                {
                    driverName = await db.Staffs.Where(x => !x.IsDeleted && x.Id == job.DriverId)
                        .Select(x => x.StaffName)
                        .FirstOrDefaultAsync();
                }
                else
                {
                    driverName = await db.Contractors.Where(x => !x.IsDeleted && x.Id == job.DriverId)
                        .Select(x => x.ContractorName)
                        .FirstOrDefaultAsync();
                }
            }


            return Ok(new
            {
                Code = 0,
                Order = new
                {
                    order.ConsignmentNumber,
                    order.Status,
                    order.BinCode,
                    order.FromAddress,
                    order.FromMobilePhone,
                    order.FromName,
                    order.FromZipCode,
                    order.ToAddress,
                    order.ToMobilePhone,
                    order.ToName,
                    order.ToZipCode,
                    order.SizeId,
                    order.PickupDate,
                    order.PickupTimeSlotId,
                    order.DeliveryDate,
                    order.DeliveryTimeSlotId,
                    order.Remark,
                    ProductTypeBinCode = order.ProductType.BinCode
                },
                Sizes = sizes,
                TimeOptions = new
                {
                    service.IsPickupTimeAllow,
                    service.IsPickupDateAllow,
                    service.IsDeliveryTimeAllow,
                    service.IsDeliveryDateAllow,
                    AvailablePickupTimes = pickupTimeSlots,
                    service.AvailablePickupDateRange,
                    AvailableDeliveryTimes = deliveryTimeSlots,
                    service.AvailableDeliveryDateRange
                },
                Statuses = EnumHelpers.ConvertEnumToSelectList<OrderStatuses>(),
                AssignedDriver = driverName,
                AssignedDriverId = job?.DriverId
            });
        }

        [Route("changerequest")]
        [HttpPost]
        public async Task<IHttpActionResult> ChangeRequest(OrderViewModel model)
        {
            if (model == null)
            {
                return Ok(new
                {
                    Code = 500,
                    Message = "There is no data in request"
                });
            }

            var order = db.Orders.FirstOrDefault(x => x.ConsignmentNumber.ToLower() == model.ConsignmentNumber.ToLower());
            if (order == null)
            {
                return Ok(new
                {
                    Code = 500,
                    Message = "There is no data in request"
                });
            }

            // Send email with information
            var emails = (SettingProvider.OperatorsEmail ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                foreach (var email in emails)
                {
                    // Load template
                    var emailTemplate = string.Join("\n",
                        File.ReadAllLines(
                            HttpContext.Current.Server.MapPath("~/Templates/email_order_change_request.html"),
                            Encoding.UTF8));

                    emailTemplate = emailTemplate.Replace("{Consignment Number}", model.ConsignmentNumber);

                    emailTemplate = emailTemplate.Replace("{Old Size}", order.Size.SizeName);
                    emailTemplate = emailTemplate.Replace("{Old Bin Code}", order.BinCode);
                    emailTemplate = emailTemplate.Replace("{Old From Name}", order.FromName);
                    emailTemplate = emailTemplate.Replace("{Old From Mobile}", order.FromMobilePhone);
                    emailTemplate = emailTemplate.Replace("{Old To Name}", order.ToName);
                    emailTemplate = emailTemplate.Replace("{Old To Mobile}", order.ToMobilePhone);
                    emailTemplate = emailTemplate.Replace("{Old Pickup Date}", order.PickupDate?.ToString("dd/MM/yyyy"));
                    emailTemplate = emailTemplate.Replace("{Old Delivery Date}", order.DeliveryDate?.ToString("dd/MM/yyyy"));
                    var timeSlot = await db.TimeSlots.FirstOrDefaultAsync(x => x.Id == order.PickupTimeSlotId);
                    emailTemplate = emailTemplate.Replace("{Old Pickup Timeslot Name}", timeSlot != null ? string.Format("{0}({1:HH:mm}-{2:HH:mm})", timeSlot.TimeSlotName, timeSlot.FromTime, timeSlot.ToTime) : "");
                    emailTemplate = emailTemplate.Replace("{Old Pickup Timeslot ID}", order.PickupTimeSlotId?.ToString());
                    timeSlot = await db.TimeSlots.FirstOrDefaultAsync(x => x.Id == order.DeliveryTimeSlotId);
                    emailTemplate = emailTemplate.Replace("{Old Delivery Timeslot Name}", timeSlot != null ? string.Format("{0}({1:HH:mm}-{2:HH:mm})", timeSlot.TimeSlotName, timeSlot.FromTime, timeSlot.ToTime) : "");
                    emailTemplate = emailTemplate.Replace("{Old Delivery Timeslot ID}", order.DeliveryTimeSlotId?.ToString());
                    emailTemplate = emailTemplate.Replace("{Old From Address}", order.FromAddress);
                    emailTemplate = emailTemplate.Replace("{Old To Address}", order.ToAddress);
                    emailTemplate = emailTemplate.Replace("{Old From Postal Code}", order.FromZipCode);
                    emailTemplate = emailTemplate.Replace("{Old To Postal Code}", order.ToZipCode);
                    emailTemplate = emailTemplate.Replace("{Old Remark}", order.Remark);
                    emailTemplate = emailTemplate.Replace("{Old Status text}", EnumHelpers.GetDisplayValue((OrderStatuses)order.Status));
                    emailTemplate = emailTemplate.Replace("{Old Status}", order.Status.ToString());

                    var size = await db.Sizes.FirstOrDefaultAsync(x => x.Id == model.SizeId);
                    emailTemplate = emailTemplate.Replace("{Size}", string.Format("<span style='color: #{0}'>{1}</span>", model.SizeId != order.SizeId ? "153cd7" : "000", size.SizeName));
                    emailTemplate = emailTemplate.Replace("{Bin Code}", string.Format("<span style='color: #{0}'>{1}</span>", model.BinCode != order.BinCode ? "153cd7" : "000", model.BinCode));
                    emailTemplate = emailTemplate.Replace("{From Name}", string.Format("<span style='color: #{0}'>{1}</span>", model.FromName != order.FromName ? "153cd7" : "000", model.FromName));
                    emailTemplate = emailTemplate.Replace("{From Mobile}", string.Format("<span style='color: #{0}'>{1}</span>", model.FromMobilePhone != order.FromMobilePhone ? "153cd7" : "000", model.FromMobilePhone));
                    emailTemplate = emailTemplate.Replace("{To Name}", string.Format("<span style='color: #{0}'>{1}</span>", model.ToName != order.ToName ? "153cd7" : "000", model.ToName));
                    emailTemplate = emailTemplate.Replace("{To Mobile}", string.Format("<span style='color: #{0}'>{1}</span>", model.ToMobilePhone != order.ToMobilePhone ? "153cd7" : "000", model.ToMobilePhone));
                    emailTemplate = emailTemplate.Replace("{Pickup Date}", string.Format("<span style='color: #{0}'>{1}</span>", model.PickupDate != order.PickupDate ? "153cd7" : "000", model.PickupDate?.ToString("dd/MM/yyyy")));
                    emailTemplate = emailTemplate.Replace("{Delivery Date}", string.Format("<span style='color: #{0}'>{1}</span>", model.DeliveryDate != order.DeliveryDate ? "153cd7" : "000", model.DeliveryDate?.ToString("dd/MM/yyyy")));
                    timeSlot = await db.TimeSlots.FirstOrDefaultAsync(x => x.Id == model.PickupTimeSlotId);
                    emailTemplate = emailTemplate.Replace("{Pickup Timeslot Name}", string.Format("<span style='color: #{0}'>{1}</span>", model.PickupTimeSlotId != order.PickupTimeSlotId ? "153cd7" : "000", timeSlot != null ? string.Format("{0}({1:HH:mm}-{2:HH:mm})", timeSlot.TimeSlotName, timeSlot.FromTime, timeSlot.ToTime) : ""));
                    emailTemplate = emailTemplate.Replace("{Pickup Timeslot ID}", string.Format("<span style='color: #{0}'>{1}</span>", model.PickupTimeSlotId != order.PickupTimeSlotId ? "153cd7" : "000", model.PickupTimeSlotId?.ToString()));
                    timeSlot = await db.TimeSlots.FirstOrDefaultAsync(x => x.Id == model.DeliveryTimeSlotId);
                    emailTemplate = emailTemplate.Replace("{Delivery Timeslot Name}", string.Format("<span style='color: #{0}'>{1}</span>", model.DeliveryTimeSlotId != order.DeliveryTimeSlotId ? "153cd7" : "000", timeSlot != null ? string.Format("{0}({1:HH:mm}-{2:HH:mm})", timeSlot.TimeSlotName, timeSlot.FromTime, timeSlot.ToTime) : ""));
                    emailTemplate = emailTemplate.Replace("{Delivery Timeslot ID}", string.Format("<span style='color: #{0}'>{1}</span>", model.DeliveryTimeSlotId != order.DeliveryTimeSlotId ? "153cd7" : "000", model.DeliveryTimeSlotId?.ToString()));
                    emailTemplate = emailTemplate.Replace("{From Address}", string.Format("<span style='color: #{0}'>{1}</span>", model.FromAddress != order.FromAddress ? "153cd7" : "000", model.FromAddress));
                    emailTemplate = emailTemplate.Replace("{To Address}", string.Format("<span style='color: #{0}'>{1}</span>", model.ToAddress != order.ToAddress ? "153cd7" : "000", model.ToAddress));
                    emailTemplate = emailTemplate.Replace("{From Postal Code}", string.Format("<span style='color: #{0}'>{1}</span>", model.FromZipCode != order.FromZipCode ? "153cd7" : "000", model.FromZipCode));
                    emailTemplate = emailTemplate.Replace("{To Postal Code}", string.Format("<span style='color: #{0}'>{1}</span>", model.ToZipCode != order.ToZipCode ? "153cd7" : "000", model.ToZipCode));
                    emailTemplate = emailTemplate.Replace("{Remark}", string.Format("<span style='color: #{0}'>{1}</span>", model.Remark != order.Remark ? "153cd7" : "000", model.Remark));
                    emailTemplate = emailTemplate.Replace("{Status text}", string.Format("<span style='color: #{0}'>{1}</span>", (int)model.Status != order.Status ? "153cd7" : "000", EnumHelpers.GetDisplayValue(model.Status)));
                    emailTemplate = emailTemplate.Replace("{Status}", string.Format("<span style='color: #{0}'>{1}</span>", (int)model.Status != order.Status ? "153cd7" : "000", ((int)model.Status).ToString()));

                    var client = new System.Net.Mail.SmtpClient(SettingProvider.SmtpHostAddress,
                        SettingProvider.SmtpHostPort)
                    {
                        DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false
                    };

                    // Create the credentials:
                    var credentials = new System.Net.NetworkCredential(SettingProvider.SmtpUsername,
                        SettingProvider.SmtpPassword);

                    client.EnableSsl = SettingProvider.SmtpSecured;
                    client.Credentials = credentials;

                    // Create the message:
                    var mailAddressFrom = new MailAddress(SettingProvider.SmtpSenderEmail,
                        SettingProvider.SmtpSenderName);
                    var mailAddressTo = new MailAddress(email);
                    var mail = new MailMessage(mailAddressFrom, mailAddressTo)
                    {
                        Subject = "Order Information Change Request",
                        Body = emailTemplate,
                        IsBodyHtml = true
                    };

                    // Send
                    await client.SendMailAsync(mail);
                }

                // Save
                var track = new EmailTracking()
                {
                    EmailType = "Send Order Change Request",
                    OrderId = order.Id,
                    Information = JsonConvert.SerializeObject(model),
                    SentBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    SentOn = DateTime.Now
                };
                db.EmailTrackings.Add(track);

                await db.SaveChangesAsync();

                return Ok(new
                {
                    Code = 0
                });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(new
                {
                    Code = 500,
                    Message = exc.Message
                });
            }
        }

        [Route("warehouse")]
        [HttpPost]
        public async Task<IHttpActionResult> Warehouse()
        {
            var totalItems = await db.Orders
                .Where(x => x.Status == (int)OrderStatuses.ItemInDepot && !x.IsDeleted)
                .CountAsync();
            var totalParcels = await db.Orders
                .Where(x => x.Status == (int)OrderStatuses.ItemInDepot && !x.IsDeleted && x.ProductType.ProductTypeName.ToLower() == "parcel")
                .CountAsync();
            var totalDocuments = await db.Orders
                .Where(x => x.Status == (int)OrderStatuses.ItemInDepot && !x.IsDeleted && x.ProductType.ProductTypeName.ToLower() == "document")
                .CountAsync();

            var orders = await db.Orders
                .Where(x => x.Status == (int)OrderStatuses.ItemInDepot && !x.IsDeleted)
                .GroupBy(x => x.BinCode)
                .Select(g => new
                {
                    BinCode = g.Key,
                    Orders = g.Select(x => new
                    {
                        x.ConsignmentNumber
                    })
                })
                .OrderBy(x => x.BinCode)
                .ToListAsync();
            return Ok(new
            {
                Code = 0,
                TotalItems = totalItems,
                TotalParcels = totalParcels,
                TotalDocuments = totalDocuments,
                Orders = orders
            });
        }

        [Route("faildeliveries")]
        [HttpPost]
        public async Task<IHttpActionResult> FailedDeliveries()
        {
            var failedOrders = await db.Orders
                .Where(x => x.Status == (int)OrderStatuses.DeliveryFailed && !x.IsDeleted)
                .Select(x => new
                {
                    x.ConsignmentNumber,
                    Proceeded = db.OrderTrackings
                        .Where(t => t.ConsignmentNumber == x.ConsignmentNumber)
                        .OrderByDescending(t => t.CreatedOn)
                        .Select(t => t.Status)
                        .FirstOrDefault() == (int)OrderStatuses.ItemInDepot,
                    Sent = db.EmailTrackings
                        .Any(t => t.OrderId == x.Id && t.EmailType.ToLower() == "send failed item to operators")
                })
                .ToListAsync();
            return Ok(new
            {
                Code = 0,
                Result = failedOrders
            });
        }

        [Route("putfailedinto/{consignmentNumber}")]
        [HttpPost]
        public async Task<IHttpActionResult> PutFailedInto(string consignmentNumber)
        {
            if (consignmentNumber == null)
            {
                consignmentNumber = "";
            }

            var order = await db.Orders
                .FirstOrDefaultAsync(x => x.ConsignmentNumber.ToLower() == consignmentNumber.ToLower() && !x.IsDeleted);

            if (order == null)
            {
                return Ok(new
                {
                    Code = 404,
                    Message = "Cannot find order corresponding to this consignment number"
                });
            }

            if (order.Status != (int)OrderStatuses.DeliveryFailed)
            {
                return Ok(new
                {
                    Code = 403,
                    Message = "Item is not in the correct status"
                });
            }

            // Not insert duplicate
            var track = await db.OrderTrackings.Where(x => x.ConsignmentNumber == order.ConsignmentNumber)
                .OrderByDescending(x => x.CreatedOn).FirstOrDefaultAsync();
            if (track != null && track.Status == (int)OrderStatuses.ItemInDepot)
            {
                return Ok(new
                {
                    Code = 0
                });
            }

            // Add track
            track = new OrderTracking
            {
                ConsignmentNumber = order.ConsignmentNumber,
                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                CreatedOn = DateTime.Now,
                PickupTimeSlotId = order.PickupTimeSlotId,
                PickupDate = order.PickupDate,
                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                DeliveryDate = order.DeliveryDate,
                BinCode = order.BinCode,
                Status = (int)OrderStatuses.ItemInDepot
            };
            db.OrderTrackings.Add(track);

            try
            {
                await db.SaveChangesAsync();
                return Ok(new
                {
                    Code = 0
                });
            }
            catch (Exception exc)
            {
                return Ok(new
                {
                    Code = 500,
                    Message = exc.Message
                });
            }
        }

        [Route("sendfailed")]
        [HttpPost]
        public async Task<IHttpActionResult> SendFailed()
        {
            var failedOrders = await db.Orders
                .Where(x => x.Status == (int)OrderStatuses.DeliveryFailed && !x.IsDeleted &&
                            db.OrderTrackings
                                .Where(t => t.ConsignmentNumber == x.ConsignmentNumber)
                                .OrderByDescending(t => t.CreatedOn)
                                .Select(t => t.Status)
                                .FirstOrDefault() == (int)OrderStatuses.ItemInDepot)
                .ToListAsync();

            var failedOrderIds = failedOrders.Select(x => x.Id).ToList();
            var tracks = await db.EmailTrackings
                .Where(x => x.EmailType.ToLower() == "send failed item to operators" && x.OrderId.HasValue && failedOrderIds.Contains(x.OrderId.Value))
                .ToListAsync();

            // Send email with information
            var emails = (SettingProvider.OperatorsEmail ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                foreach (var email in emails)
                {
                    // Load template
                    var emailTemplate = string.Join("\n",
                        File.ReadAllLines(
                            HttpContext.Current.Server.MapPath("~/Templates/email_inventory_failed_delivery.html"),
                            Encoding.UTF8));

                    emailTemplate = emailTemplate.Replace("{Consignment Numbers}", string.Join("<br />", failedOrders.Where(x => tracks.All(t => t.OrderId != x.Id)).Select(x => x.ConsignmentNumber)));
                    emailTemplate = emailTemplate.Replace("{Sent Consignment Numbers}", string.Join("<br />", failedOrders.Where(x => tracks.Any(t => t.OrderId == x.Id)).Select(x => x.ConsignmentNumber)));

                    var client = new System.Net.Mail.SmtpClient(SettingProvider.SmtpHostAddress,
                        SettingProvider.SmtpHostPort)
                    {
                        DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false
                    };

                    // Create the credentials:
                    var credentials = new System.Net.NetworkCredential(SettingProvider.SmtpUsername,
                        SettingProvider.SmtpPassword);

                    client.EnableSsl = SettingProvider.SmtpSecured;
                    client.Credentials = credentials;

                    // Create the message:
                    var mailAddressFrom = new MailAddress(SettingProvider.SmtpSenderEmail,
                        SettingProvider.SmtpSenderName);
                    var mailAddressTo = new MailAddress(email);
                    var mail = new MailMessage(mailAddressFrom, mailAddressTo)
                    {
                        Subject = "Inventory Failed Deliveries in Warehouse",
                        Body = emailTemplate,
                        IsBodyHtml = true
                    };

                    // Send
                    await client.SendMailAsync(mail);
                }

                // Save
                foreach (var order in failedOrders)
                {
                    var track = new EmailTracking()
                    {
                        EmailType = "Send failed item to Operators",
                        OrderId = order.Id,
                        SentBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        SentOn = DateTime.Now
                    };
                    db.EmailTrackings.Add(track);
                }

                await db.SaveChangesAsync();

                return Ok(new
                {
                    Code = 0
                });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(new
                {
                    Code = 500,
                    Message = exc.Message
                });
            }
        }

        [Route("statistics/{from}/{to}")]
        [HttpPost]
        public async Task<IHttpActionResult> Statistics(string from, string to)
        {
            var now = DateTime.Now;
            var fromDate = now.AddDays(-(int)now.DayOfWeek);
            var toDate = fromDate.AddDays(6);

            if (!DateTime.TryParseExact(from, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out fromDate))
            {
                fromDate = now.AddDays(-(int)now.DayOfWeek);
            }
            if (!DateTime.TryParseExact(to, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out toDate))
            {
                toDate = fromDate.AddDays(6);
            }

            var result = await db.Orders
                .Where(x => !x.IsDeleted && DbFunctions.TruncateTime(x.CreatedOn) >= fromDate && DbFunctions.TruncateTime(x.CreatedOn) <= toDate)
                .GroupBy(x => x.Status)
                .Select(g => new
                {
                    Status = g.Key,
                    Orders = g.GroupBy(x => DbFunctions.TruncateTime(x.CreatedOn))
                            .OrderBy(x => x.Key)
                            .Select(x => new
                            {
                                Date = x.Key.Value,
                                Count = x.Count()
                            })
                })
                .ToListAsync();

            return Ok(new
            {
                Code = 0,
                FromDate = fromDate.Date,
                ToDate = toDate.Date,
                Result = result
            });
        }

        [Route("job/{jobId}")]
        [HttpPost]
        public async Task<IHttpActionResult> Job(long jobId)
        {
            var job = await db.Jobs.FirstOrDefaultAsync(x => x.Id == jobId && !x.IsSpecial);
            if (job == null)
            {
                return Ok(new
                {
                    Code = 404,
                    Message = "Cannot find job with this ID."
                });
            }

            var result = new List<dynamic>();

            var route = JsonConvert.DeserializeObject<JobViewModel>(job.Routes);
            foreach (var location in route.Locations)
            {
                var oids = (string.IsNullOrEmpty(location.OrderId) ? "" : location.OrderId).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i));
                var consignmentNumbers = db.Orders.Where(x => !x.IsDeleted && oids.Contains(x.Id))
                    .Select(x => x.ConsignmentNumber).ToList();
                foreach (var number in consignmentNumbers)
                {
                    result.Add(new
                    {
                        ConsignmentNumber = number,
                        Type = location.Type
                    });
                }
            }

            result = result.Distinct().ToList();

            return Ok(new
            {
                Code = 0,
                Result = result
            });
        }

        [Route("uploadSignature")]
        [HttpPost]
        public async Task<IHttpActionResult> UploadSignature()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
            if (file != null && file.ContentLength > 0)
            {
                // Check folder
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(PathHelper.InventoryUploadBaseDirectory + PathHelper.SignaturesDirectory)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(PathHelper.InventoryUploadBaseDirectory + PathHelper.SignaturesDirectory));
                }

                var resultFileName = $"{Path.GetRandomFileName()}.png";
                var resultFileUrl = PathHelper.InventoryUploadBaseDirectory + PathHelper.SignaturesDirectory + resultFileName;
                var resultFilePath = HttpContext.Current.Server.MapPath(resultFileUrl);
                file.SaveAs(resultFilePath);

                var sizeInKilobytes = file.ContentLength / 1024;
                var sizeText = sizeInKilobytes.ToString() + " KB";

                return Ok(new
                {
                    Code = 0,
                    Path = resultFileUrl
                });
            }

            return Ok(new
            {
                Code = 400,
                Message = "There is no upload file."
            });
        }

        [Route("acknowledge")]
        [HttpPost]
        public async Task<IHttpActionResult> Acknowledge(AcknowledgeModel model)
        {
            // Send email with information
            var emails = (SettingProvider.OperatorsEmail ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                foreach (var email in emails)
                {
                    // Load template
                    var emailTemplate = string.Join("\n",
                        File.ReadAllLines(
                            HttpContext.Current.Server.MapPath("~/Templates/email_inventory_acknowledge.html"),
                            Encoding.UTF8));

                    var parts = model.Info.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length < 2)
                    {
                        return Ok(new
                        {
                            Code = 500,
                            Message = "Invalid parameter(s)"
                        });
                    }

                    emailTemplate = emailTemplate.Replace("{Consignment Numbers}", string.Join("<br />", model.ConsignmentNumbers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)));
                    emailTemplate = emailTemplate.Replace("{Driver}", parts[1]);

                    var client = new System.Net.Mail.SmtpClient(SettingProvider.SmtpHostAddress,
                        SettingProvider.SmtpHostPort)
                    {
                        DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false
                    };

                    // Create the credentials:
                    var credentials = new System.Net.NetworkCredential(SettingProvider.SmtpUsername,
                        SettingProvider.SmtpPassword);

                    client.EnableSsl = SettingProvider.SmtpSecured;
                    client.Credentials = credentials;

                    // Create the message:
                    var mailAddressFrom = new MailAddress(SettingProvider.SmtpSenderEmail,
                        SettingProvider.SmtpSenderName);
                    var mailAddressTo = new MailAddress(email);
                    var mail = new MailMessage(mailAddressFrom, mailAddressTo)
                    {
                        Subject = "Inventory Driver Acknowledgement",
                        Body = emailTemplate,
                        Attachments =
                        {
                            new Attachment(HttpContext.Current.Server.MapPath(parts[0]))
                        },
                        IsBodyHtml = true
                    };

                    // Send
                    await client.SendMailAsync(mail);
                }

                return Ok(new
                {
                    Code = 0
                });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Ok(new
                {
                    Code = 500,
                    Message = exc.Message
                });
            }
        }

        [Route("dispatchinfo/{consignmentNumber}")]
        [HttpPost]
        public async Task<IHttpActionResult> DispatchInfo(string consignmentNumber)
        {
            var order = await db.Orders.FirstOrDefaultAsync(x => x.ConsignmentNumber.ToLower() == consignmentNumber.ToLower());
            if (order == null)
            {
                return Ok(new
                {
                    Code = 404,
                    Message = "Cannot find order with this Consignment Number."
                });
            }

            var job = await db.Jobs.FirstOrDefaultAsync(x => !x.IsDeleted && ((!x.IsSpecial && (x.Routes.Contains("\"" + order.Id + "\"") ||
                x.Routes.Contains("\"" + order.Id + ",") || x.Routes.Contains("," + order.Id + ",") || x.Routes.Contains("," + order.Id + "\"")))
                || (x.IsSpecial && ("," + x.Routes + ",").Contains("," + order.Id + ","))));

            if (job == null)
            {
                return Ok(new
                {
                    Code = 0,
                    RouteTo = "",
                    Driver = ""
                });
            }

            var name = "";
            var vehicleNumber = "";
            if (job.DriverType == (int)AccountTypes.Contractor)
            {
                var person = await db.Contractors.FirstOrDefaultAsync(x => x.Id == job.DriverId);
                name = person?.ContractorName ?? "";
                vehicleNumber = person?.VehicleNumber ?? "";
            }
            else
            {
                name = await db.Staffs.Where(x => x.Id == job.DriverId).Select(x => x.StaffName).FirstOrDefaultAsync();
            }

            return Ok(new
            {
                Code = 0,
                RouteTo = job.Vehicle?.VehicleNumber ?? vehicleNumber,
                Driver = name
            });
        }
    }

    public class AcknowledgeModel
    {
        public string Info { get; set; }

        public string ConsignmentNumbers { get; set; }
    }
}
