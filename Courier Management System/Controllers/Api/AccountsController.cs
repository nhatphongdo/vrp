﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Courier_Management_System.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/accounts")]
    public class AccountsController : ApiController
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountsController()
        {
        }

        public AccountsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    Code = 100,
                    Message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage))
                });
            }

            var result = await AccountHelper.Authenticate(model);
            return Ok(result);
            //case SignInStatus.LockedOut:
            //        return Ok(new
            //        {
            //            Code = 400,
            //            Message = "Your account is locked. Please contact to administrator to resolve."
            //        });
            //    case SignInStatus.RequiresVerification:
            //        return Ok(new
            //        {
            //            Code = 300,
            //            Message = "Your account needs to be verified. Please check your mailbox or phone to verify account or contact to administrator for help."
            //        });
        }

        [Route("register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    Code = 100,
                    Message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage))
                });
            }

            var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                // Add user to role
                result = await UserManager.AddToRoleAsync(user.Id, model.AccountType == AccountTypes.Staff ? AccountHelper.StaffDriverRole : AccountHelper.ContractorDriverRole);
                if (!result.Succeeded)
                {
                    // Rollback
                    result = await UserManager.DeleteAsync(user);
                    return Ok(new
                    {
                        Code = 500,
                        Message = string.Join(" | ", result.Errors)
                    });
                }

                // Save user information
                if (model.AccountType == AccountTypes.Staff)
                {
                    // Add to Role
                    var staff = new Staff()
                    {
                        CreatedOn = DateTime.Now,
                        CreatedBy = HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : model.UserName,
                        DrivingLicense = model.DrivingLicense,
                        ICNumber = model.ICNumber,
                        IsDeleted = false,
                        StaffAddress = model.Address,
                        StaffMobile = model.MobileNumber,
                        StaffCountry = model.CountryCode,
                        StaffName = model.Name,
                        UpdatedBy = HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : model.UserName,
                        UpdatedOn = DateTime.Now,
                        UserName = model.UserName
                    };
                    db.Staffs.Add(staff);
                }
                else if (model.AccountType == AccountTypes.Contractor)
                {
                    var contractor = new Contractor()
                    {
                        CreatedOn = DateTime.Now,
                        CreatedBy = HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : model.UserName,
                        UserName = model.UserName,
                        ContractorAddress = model.Address,
                        ContractorCountry = model.CountryCode,
                        ContractorName = model.Name,
                        ContractorMobile = model.MobileNumber,
                        DrivingLicense = model.DrivingLicense,
                        ICNumber = model.ICNumber,
                        VehicleNumber = model.VehicleNumber,
                        IsDeleted = false,
                        UpdatedBy = HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : model.UserName,
                        UpdatedOn = DateTime.Now
                    };
                    db.Contractors.Add(contractor);
                }

                try
                {
                    db.SaveChanges();
                    var loginResult = await AccountHelper.Authenticate(new LoginViewModel() { UserName = model.UserName, Password = model.Password });

                    if (loginResult.Code == 500)
                    {
                        result = await UserManager.DeleteAsync(user);
                    }
                    return Ok(loginResult);
                }
                catch (Exception exc)
                {
                    // Rollback
                    result = await UserManager.RemoveFromRoleAsync(user.Id, model.AccountType == AccountTypes.Staff ? AccountHelper.StaffDriverRole : AccountHelper.ContractorDriverRole);
                    result = await UserManager.DeleteAsync(user);
                    LogHelper.Log(exc);
                    return Ok(new
                    {
                        Code = 500,
                        Message = "Errors occur in processing request. Please contact to administrator to resolve."
                    });
                }
            }

            return Ok(new
            {
                Code = 500,
                Message = string.Join(" | ", result.Errors)
            });
        }

        [Route("forgotpassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    Code = 100,
                    Message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage))
                });
            }

            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                // Don't reveal that the user does not exist or is not confirmed
                return Ok(new
                {
                    Code = 400,
                    Message = "Cannot find user with this username"
                });
            }

            // Send an email with this link
            var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var callbackUrl = Url.Link("Default", new
            {
                controller = "Account",
                action = "ResetPassword",
                userId = user.Id,
                code = code
            });
            await UserManager.SendEmailAsync(user.Id, "Reset Password", $"Please reset your password by clicking <a href=\"{callbackUrl}\">here</a>");

            return Ok(new
            {
                Code = 0,
                Message = "Please check your mail box to reset your password"
            });
        }

        [Route("checkUserName/{username}")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> CheckUserName(string username)
        {
            var result = await db.AspNetUsers.FirstOrDefaultAsync(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (result != null)
            {
                // Suggest another username
                var suggestion = new List<string>();
                var index = 0;
                do
                {
                    ++index;
                    var newUsername = $"{username}_{index}";
                    result = await db.AspNetUsers.FirstOrDefaultAsync(x => x.UserName.Equals(newUsername, StringComparison.OrdinalIgnoreCase));
                } while (result != null);
                suggestion.Add($"{username}_{index}");

                return Ok(new
                {
                    Existed = true,
                    Suggestion = suggestion
                });
            }

            return Ok(new
            {
                Existed = false
            });
        }

        [Route("profile")]
        [HttpPost]
        public async Task<IHttpActionResult> Profile()
        {
            var roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            var profile = await AccountHelper.Profile(User.Identity.GetUserName(), roles);
            return Ok(profile);
        }

        [Route("update")]
        [HttpPost]
        public async Task<IHttpActionResult> Update(UpdateViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    Code = 100,
                    Message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage))
                });
            }

            var userId = User.Identity.GetUserId();
            var roles = await UserManager.GetRolesAsync(userId);
            var result = await AccountHelper.Update(userId, User.Identity.GetUserName(), roles, model);
            return Ok(result);
        }

        [Route("upload")]
        [HttpPost]
        public async Task<IHttpActionResult> Upload()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
            if (file != null && file.ContentLength > 0)
            {
                // Check folder
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(PathHelper.DriverUploadBaseDirectory)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(PathHelper.DriverUploadBaseDirectory));
                }

                try
                {
                    var resultFileName = $"{Path.GetRandomFileName()}{Path.GetExtension(file.FileName)}";
                    var resultFileUrl = PathHelper.DriverUploadBaseDirectory + resultFileName;
                    var resultFilePath = HttpContext.Current.Server.MapPath(resultFileUrl);
                    file.SaveAs(resultFilePath);

                    var name = file.FileName;
                    var url = Url.Content(resultFileUrl).ToAbsoluteUrl();
                    var sizeInKilobytes = file.ContentLength / 1024;
                    var sizeText = sizeInKilobytes.ToString() + " KB";

                    return Ok(new
                    {
                        Code = 0,
                        Path = url
                    });
                }
                catch (Exception exc)
                {
                    LogHelper.Log(exc);
                    return Ok(new
                    {
                        Code = 500,
                        Message = exc.Message
                    });
                }
            }

            return Ok(new
            {
                Code = 400,
                Message = "There is no upload file."
            });
        }

        [Route("notificationToken")]
        [HttpPost]
        public async Task<IHttpActionResult> NotificationToken(NotificationTokenViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    Code = 100,
                    Message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage))
                });
            }

            var roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            var result = await AccountHelper.UpdateNotificationToken(User.Identity.GetUserName(), roles, model);
            return Ok(result);
        }

        [Route("uploadcustomer")]
        [HttpPost]
        public async Task<IHttpActionResult> UploadCustomer()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
            if (file != null && file.ContentLength > 0)
            {
                // Check folder
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(PathHelper.CustomerUploadBaseDirectory)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(PathHelper.CustomerUploadBaseDirectory));
                }

                var resultFileName = $"{Path.GetRandomFileName()}{Path.GetExtension(file.FileName)}";
                var resultFileUrl = PathHelper.CustomerUploadBaseDirectory + resultFileName;
                var resultFilePath = HttpContext.Current.Server.MapPath(resultFileUrl);
                file.SaveAs(resultFilePath);

                var name = file.FileName;
                var url = Url.Content(resultFileUrl).ToAbsoluteUrl();
                var sizeInKilobytes = file.ContentLength / 1024;
                var sizeText = sizeInKilobytes.ToString() + " KB";

                return Ok(new
                {
                    Code = 0,
                    Path = url
                });
            }

            return Ok(new
            {
                Code = 400,
                Message = "There is no upload file."
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
            base.Dispose(disposing);
        }

    }
}