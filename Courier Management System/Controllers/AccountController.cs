﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Braintree;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Courier_Management_System.Controllers
{
    public class AccountController : Controller
    {
        #region Fields

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        #endregion

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated && !string.IsNullOrEmpty(GetAccessToken()))
            {
                return RedirectToLocal(returnUrl, GetAccessToken());
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await AccountHelper.Authenticate(model);
            if (result.Code == 0)
            {
                Response.SetCookie(new HttpCookie("CdsAccessToken", result.Token));
                return RedirectToLocal(returnUrl, result.Token);
            }
            else
            {
                ModelState.AddModelError("", result.Message);
                return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }

            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent : model.RememberMe, rememberBrowser : model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl, "");
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent : false, rememberBrowser : false);
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Profile", "Account");
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult CustomerRegister()
        {
            var db = new CourierDBEntities();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted)
                                     .Select(
                                             x => new SelectListItem()
                                                  {
                                                      Text = x.PaymentTypeName,
                                                      Value = x.Id.ToString()
                                                  })
                                     .ToList();
            ViewBag.CountryCodes = db.Countries
                                     .Select(
                                             x => new SelectListItem()
                                                  {
                                                      Text = x.CountryName + " (" + x.CountryCode + ")",
                                                      Value = x.CountryCode
                                                  })
                                     .ToList();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CustomerRegister(AddCustomerViewModel model)
        {
            var db = new CourierDBEntities();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted)
                                     .Select(
                                             x => new SelectListItem()
                                                  {
                                                      Text = x.PaymentTypeName,
                                                      Value = x.Id.ToString()
                                                  })
                                     .ToList();
            ViewBag.CountryCodes = db.Countries
                                     .Select(
                                             x => new SelectListItem()
                                                  {
                                                      Text = x.CountryName + "(" + x.CountryCode + ")",
                                                      Value = x.CountryCode
                                                  })
                                     .ToList();
            if (ModelState.IsValid)
            {
                // Check unique Company Prefix
                if (!string.IsNullOrWhiteSpace(model.CompanyPrefix))
                {
                    var found = db.Customers.Any(x => x.CompanyPrefix.ToLower() == model.CompanyPrefix.ToLower());
                    if (found)
                    {
                        ViewBag.ErrorMessages = new string[] { "Company Prefix already existed. Please choose the other." };
                        return View(model);
                    }
                }

                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    result = await UserManager.AddToRoleAsync(user.Id, AccountHelper.CustomerRole);

                    if (!result.Succeeded)
                    {
                        // Rollback
                        result = await UserManager.DeleteAsync(user);
                        ViewBag.ErrorMessages = result.Errors;
                        return View(model);
                    }

                    var customer = new Customer()
                                   {
                                       UserName = model.UserName,
                                       PaymentTypeId = model.PaymentType,
                                       Credit = 0,
                                       CustomerAddress = model.Address,
                                       CustomerCompany = model.Company,
                                       CustomerName = model.Name,
                                       CustomerCountry = model.CountryCode,
                                       CustomerPhone = model.MobileNumber,
                                       CustomerZipCode = model.ZipCode,
                                       IsActive = true,
                                       Salesperson = "",
                                       DefaultPromoCode = "",
                                       CompanyPrefix = model.CompanyPrefix,
                                       PricePlan = null,
                                       IsDeleted = false,
                                       CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                       CreatedOn = DateTime.Now,
                                       UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                       UpdatedOn = DateTime.Now
                                   };

                    db.Customers.Add(customer);

                    try
                    {
                        db.SaveChanges();

                        //// Send an email with this link
                        //var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        //var callbackUrl = Url.Action("ConfirmEmail", "Account", new
                        //{
                        //    userId = user.Id,
                        //    code = code
                        //}, protocol: Request.Url.Scheme);

                        var emailTemplate = string.Join("\n", System.IO.File.ReadAllLines(Server.MapPath("~/Templates/register_confirm.html"), Encoding.UTF8));
                        emailTemplate = emailTemplate.Replace("{CustomerName}", user.UserName);
                        await UserManager.SendEmailAsync(user.Id, "Welcome to Roadbull", emailTemplate);

                        await SignInManager.SignInAsync(user, isPersistent : false, rememberBrowser : false);

                        return RedirectToAction("Book", "Order");
                    }
                    catch (Exception exc)
                    {
                        // Rollback
                        result = await UserManager.RemoveFromRoleAsync(user.Id, AccountHelper.CustomerRole);
                        result = await UserManager.DeleteAsync(user);
                        ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                        LogHelper.Log(exc);
                    }
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.UserName);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    ViewBag.ErrorMessage = "We cannot find user corresponding to this account.";
                }
                else
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action(
                                                 "ResetPassword",
                                                 "Account",
                                                 new
                                                 {
                                                     userId = user.Id,
                                                     code = code
                                                 },
                                                 protocol : Request.Url.Scheme);

                    try
                    {
                        var emailTemplate = string.Join("\n", System.IO.File.ReadAllLines(Server.MapPath("~/Templates/reset_password.html"), Encoding.UTF8));
                        emailTemplate = emailTemplate.Replace("{ResetPasswordLink}", callbackUrl);
                        await UserManager.SendEmailAsync(user.Id, "Reset Password", emailTemplate);
                        return RedirectToAction("ForgotPasswordConfirmation", "Account");
                    }
                    catch (Exception exc)
                    {
                        LogHelper.Log(exc);
                        ViewBag.ErrorMessage = exc.Message;
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(
                                       provider,
                                       Url.Action(
                                                  "ExternalLoginCallback",
                                                  "Account",
                                                  new
                                                  {
                                                      ReturnUrl = returnUrl
                                                  }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }

            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }

            return RedirectToAction(
                                    "VerifyCode",
                                    new
                                    {
                                        Provider = model.SelectedProvider,
                                        ReturnUrl = model.ReturnUrl,
                                        RememberMe = model.RememberMe
                                    });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent : false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl, "");
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction(
                                            "SendCode",
                                            new
                                            {
                                                ReturnUrl = returnUrl,
                                                RememberMe = false
                                            });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }

                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent : false, rememberBrowser : false);
                        return RedirectToLocal(returnUrl, "");
                    }
                }

                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [Authorize]
        public ActionResult LogOff()
        {
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut();

            Response.SetCookie(
                               new HttpCookie("CdsAccessToken")
                               {
                                   Expires = DateTime.Now.Subtract(TimeSpan.FromDays(1)),
                                   Value = ""
                               });

            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

                var loginResult = await AccountHelper.Authenticate(
                                                                   new LoginViewModel()
                                                                   {
                                                                       UserName = user.UserName,
                                                                       Password = model.NewPassword,
                                                                       RememberMe = true
                                                                   });
                if (loginResult.Code == 0)
                {
                    Response.SetCookie(new HttpCookie("CdsAccessToken", loginResult.Token));
                    return RedirectToLocal(returnUrl, loginResult.Token);
                }
                else
                {
                    ModelState.AddModelError("", loginResult.Message);
                    return View(model);
                }
            }

            AddErrors(result);
            return View(model);
        }

        [Authorize]
        public ActionResult Profile()
        {
            // Load customer's profile
            var db = new CourierDBEntities();
            ViewBag.CountryCodes = db.Countries.Select(
                                                       x => new SelectListItem()
                                                            {
                                                                Text = x.CountryName + " (" + x.CountryCode + ")",
                                                                Value = x.CountryCode
                                                            }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.PaymentTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            ViewBag.PaymentHistory = db.TopupPayments
                                       .Where(x => x.Status != (int) PaymentStatus.Undefined && x.Payer.ToLower() == username.ToLower())
                                       .Join(
                                             db.PaymentAccounts,
                                             x => x.PaymentAccountId,
                                             y => y.Id,
                                             (x, y) => new PaymentHistoryModel()
                                                       {
                                                           Id = x.Id,
                                                           TransactionDate = y.AccountType == (int) PaymentAccountTypes.Bank ? y.TransactionOn : x.ApprovedOn,
                                                           TransactionAmount = x.TransferAmount,
                                                           AccountType = (PaymentAccountTypes) y.AccountType,
                                                           Status = (PaymentStatus) x.Status
                                                       })
                                       .OrderByDescending(x => x.TransactionDate)
                                       .Take(100)
                                       .ToList();

            var model = (from customer in db.Customers
                         from user in db.AspNetUsers
                         where !customer.IsDeleted && customer.UserName.Equals(username, StringComparison.OrdinalIgnoreCase) &&
                               user.UserName.Equals(customer.UserName, StringComparison.OrdinalIgnoreCase)
                         select new
                                {
                                    Name = customer.CustomerName,
                                    UserName = customer.UserName,
                                    Address = customer.CustomerAddress,
                                    Email = user.Email,
                                    PaymentType = customer.PaymentTypeId,
                                    MobileNumber = customer.CustomerPhone,
                                    Company = customer.CustomerCompany,
                                    Country = customer.CustomerCountry,
                                    Credit = customer.Credit,
                                    IsActive = customer.IsActive,
                                    ZipCode = customer.CustomerZipCode,
                                    Salesperson = customer.Salesperson,
                                    PricePlan = customer.PricePlan == null ? PricePlans.Undefined : (PricePlans) customer.PricePlan,
                                    DefaultPromoCode = customer.DefaultPromoCode,
                                    CompanyPrefix = customer.CompanyPrefix
                                }).FirstOrDefault();
            if (model == null)
            {
                ViewBag.ErrorMessage = "You don't have permission to access this page's information.";
                return View();
            }

            var paymentAccounts = db.PaymentAccounts.Where(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase)).ToList();
            ViewBag.PaymentAccounts = paymentAccounts;

            ViewBag.PaymentAccount = TempData["PaymentAccount"];
            ViewBag.ReopenAdd = TempData["ReopenAdd"];
            ViewBag.PaymentAccountMessage = TempData["ErrorMessage"];

            var addressParts = (model.Address ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var address = "";
            var unitNumber = "";
            var buildingName = "";
            if (addressParts.Length >= 3)
            {
                buildingName = addressParts[addressParts.Length - 1].Trim();
                unitNumber = addressParts[addressParts.Length - 2].Trim();
                address = string.Join(",", addressParts.Take(addressParts.Length - 2)).Trim();
            }
            else if (addressParts.Length >= 2)
            {
                unitNumber = addressParts[addressParts.Length - 1].Trim();
                address = string.Join(",", addressParts.Take(addressParts.Length - 1)).Trim();
            }
            else
            {
                address = model.Address.Trim();
            }
            return View(
                        new EditCustomerViewModel()
                        {
                            Name = model.Name,
                            UserName = model.UserName,
                            CountryCode = model.Country,
                            Address = address,
                            UnitNumber = unitNumber,
                            BuildingName = buildingName,
                            Company = model.Company,
                            Credit = model.Credit,
                            Email = model.Email,
                            IsActive = model.IsActive,
                            MobileNumber = model.MobileNumber,
                            ZipCode = model.ZipCode,
                            PaymentType = model.PaymentType,
                            CompanyPrefix = model.CompanyPrefix,
                            CurrentPassword = "",
                            NewPassword = "",
                            ConfirmPassword = ""
                        });
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile(EditCustomerViewModel model)
        {
            // Load customer's profile
            var db = new CourierDBEntities();
            ViewBag.CountryCodes = db.Countries.Select(
                                                       x => new SelectListItem()
                                                            {
                                                                Text = x.CountryName + " (" + x.CountryCode + ")",
                                                                Value = x.CountryCode
                                                            }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.PaymentTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            ViewBag.PaymentHistory = db.TopupPayments
                                       .Where(x => x.Status != (int) PaymentStatus.Undefined && x.Payer.ToLower() == username.ToLower())
                                       .Join(
                                             db.PaymentAccounts,
                                             x => x.PaymentAccountId,
                                             y => y.Id,
                                             (x, y) => new PaymentHistoryModel()
                                                       {
                                                           Id = x.Id,
                                                           TransactionDate = y.AccountType == (int) PaymentAccountTypes.Bank ? y.TransactionOn : x.ApprovedOn,
                                                           TransactionAmount = x.TransferAmount,
                                                           AccountType = (PaymentAccountTypes) y.AccountType,
                                                           Status = (PaymentStatus) x.Status
                                                       })
                                       .OrderByDescending(x => x.TransactionDate)
                                       .Take(100)
                                       .ToList();

            var paymentAccounts = db.PaymentAccounts.Where(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase)).ToList();
            ViewBag.PaymentAccounts = paymentAccounts;

            if (ModelState.IsValid)
            {
                // Update email and password
                IdentityResult result;
                var user = UserManager.FindByName(username);
                if (user == null)
                {
                    ViewBag.ErrorMessage = $"Cannot find user with this User Name '{username}'.";
                    model.CurrentPassword = "";
                    model.NewPassword = "";
                    model.ConfirmPassword = "";
                    return View(model);
                }

                user.Email = model.Email;
                result = UserManager.Update(user);
                if (!result.Succeeded)
                {
                    ViewBag.ErrorMessage = result.Errors.FirstOrDefault();
                    model.CurrentPassword = "";
                    model.NewPassword = "";
                    model.ConfirmPassword = "";
                    return View(model);
                }

                if (!string.IsNullOrEmpty(model.CurrentPassword) && !string.IsNullOrEmpty(model.NewPassword))
                {
                    result = UserManager.ChangePassword(user.Id, model.CurrentPassword, model.NewPassword);
                    if (!result.Succeeded)
                    {
                        ViewBag.ErrorMessage = result.Errors.FirstOrDefault();
                        model.CurrentPassword = "";
                        model.NewPassword = "";
                        model.ConfirmPassword = "";
                        return View(model);
                    }
                }

                model.CurrentPassword = "";
                model.NewPassword = "";
                model.ConfirmPassword = "";

                var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                if (customer == null)
                {
                    ViewBag.ErrorMessage = "You don't have permission to access this page's information.";
                    return View(model);
                }

                model.Credit = customer.Credit;

                customer.CustomerAddress = model.Address + (string.IsNullOrEmpty(model.UnitNumber) ? "" : ", " + model.UnitNumber) + (string.IsNullOrEmpty(model.BuildingName) ? "" : ", " + model.BuildingName);
                customer.CustomerCompany = model.Company;
                customer.CustomerName = model.Name;
                customer.CustomerCountry = model.CountryCode;
                customer.CustomerPhone = model.MobileNumber;
                customer.CustomerZipCode = model.ZipCode;
                customer.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                customer.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    ViewBag.SuccessMessage = "You updated your account information successfully.";
                    return View(model);
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessage = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PaymentAccounts(PaymentAccountViewModel model)
        {
            TempData["PaymentAccount"] = model;

            if (ModelState.IsValid)
            {
                try
                {
                    // Save to Paypal
                    var paypalAccessToken = PaypalServices.GetAccessToken();
                    if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                    {
                        // Not token
                        TempData["ErrorMessage"] = "We cannot process your request right now. Please try again after few minutes.";
                        TempData["ReopenAdd"] = true;
                    }
                    else
                    {
                        var db = new CourierDBEntities();

                        if (model.AccountType == (int) PaymentAccountTypes.PaypalAccount)
                        {
                            var account = new PaymentAccount()
                                          {
                                              AccountType = model.AccountType,
                                              CardNumber = model.CardNumber,
                                              Currency = model.Currency,
                                              FirstName = model.FirstName,
                                              LastName = model.LastName,
                                              IsDeleted = false,
                                              Owner = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              CreatedOn = DateTime.Now,
                                              CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              UpdatedOn = DateTime.Now,
                                              UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                                          };

                            db.PaymentAccounts.Add(account);
                            try
                            {
                                db.SaveChanges();
                                TempData["ReopenAdd"] = false;
                            }
                            catch (Exception exc)
                            {
                                LogHelper.Log(exc);
                                TempData["ErrorMessage"] = "Errors occur in processing request. Please contact to Customer Care for support.";
                                TempData["ReopenAdd"] = true;
                            }
                        }
                        else
                        {
                            var result = PaypalServices.SaveCreditCard(
                                                                       paypalAccessToken,
                                                                       model.AccountType == (int) PaymentAccountTypes.VisaCard ? "visa" : "mastercard",
                                                                       model.CardNumber,
                                                                       model.ExpireMonth.GetValueOrDefault(),
                                                                       model.ExpireYear.GetValueOrDefault(),
                                                                       model.FirstName,
                                                                       model.LastName,
                                                                       model.CardCVV);

                            if (result.state.Equals("ok", StringComparison.OrdinalIgnoreCase))
                            {
                                // Save to DB
                                var account = new PaymentAccount()
                                              {
                                                  AccountType = model.AccountType,
                                                  CardNumber = result.number,
                                                  Currency = model.Currency,
                                                  PaypalCardId = result.id,
                                                  FirstName = model.FirstName,
                                                  LastName = model.LastName,
                                                  IsDeleted = false,
                                                  Owner = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                  CreatedOn = DateTime.Now,
                                                  CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                  UpdatedOn = DateTime.Now,
                                                  UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                                              };

                                db.PaymentAccounts.Add(account);
                                try
                                {
                                    db.SaveChanges();
                                    TempData["ReopenAdd"] = false;
                                }
                                catch (Exception exc)
                                {
                                    LogHelper.Log(exc);
                                    TempData["ErrorMessage"] = "Errors occur in processing request. Please contact to Customer Care for support.";
                                    TempData["ReopenAdd"] = true;
                                }
                            }
                            else
                            {
                                TempData["ErrorMessage"] = "Errors occur in processing request. Please contact to Customer Care for support.";
                                TempData["ReopenAdd"] = true;
                            }
                        }
                    }
                }
                catch (PayPal.PaymentsException exc)
                {
                    LogHelper.Log(exc);
                    TempData["ErrorMessage"] = exc.Details.message + "<br />" +
                                               (exc.Details.details != null ? string.Join("<br />", exc.Details.details.Select(x => x.issue)) : "");
                    TempData["ReopenAdd"] = true;
                }
            }
            else
            {
                TempData["ErrorMessage"] = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                TempData["ReopenAdd"] = true;
            }

            return RedirectToAction("Profile", "Account");
        }

        public ActionResult RemovePaymentAccount(long? id)
        {
            if (id.HasValue)
            {
                var db = new CourierDBEntities();

                var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                var paymentAccount = db.PaymentAccounts.FirstOrDefault(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Id == id);
                if (paymentAccount != null)
                {
                    try
                    {
                        paymentAccount.IsDeleted = true;
                        db.SaveChanges();
                    }
                    catch (Exception exc)
                    {
                        LogHelper.Log(exc);
                    }
                }
            }

            return RedirectToAction("Profile", "Account");
        }

        [Authorize]
        public ActionResult TopupPayment(int topUpAmount)
        {
            var db = new CourierDBEntities();
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var paymentAccounts = db.PaymentAccounts.Where(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase)).ToList();
            ViewBag.PaymentAccounts = paymentAccounts;

            ViewBag.TopupAmount = topUpAmount;

            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            ViewBag.SuccessMessage = TempData["SuccessMessage"];

            ViewBag.BrainTreeClientToken = PaypalServices.GetBrainTreeClientToken();

            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TopupPayment(int topUpAmount, long? paymentAccountId, int? paymentAccountType, FormCollection formCollection)
        {
            var db = new CourierDBEntities();
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => x.UserName.ToLower() == username.ToLower());
            if (customer == null)
            {
                ViewBag.ErrorMessage = "You don't have permissions to access this section.";
                return View();
            }

            var paymentAccounts = db.PaymentAccounts.Where(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase)).ToList();
            ViewBag.PaymentAccounts = paymentAccounts;

            ViewBag.TopupAmount = topUpAmount;

            ViewBag.BrainTreeClientToken = PaypalServices.GetBrainTreeClientToken();

            try
            {
                if (paymentAccountType != (int) PaymentAccountTypes.Bank && paymentAccountType != (int) PaymentAccountTypes.CreditCard && paymentAccountType != (int) PaymentAccountTypes.PaypalAccount)
                {
                    ViewBag.ErrorMessage = "We don't support this payment method. Please choose one payment method above.";
                    return View();
                }

                var nonce = formCollection["payment-method-nonce"];
                var paymentAccount = new PaymentAccount
                                     {
                                         Owner = username,
                                         AccountType = paymentAccountType.Value,
                                         PaypalCardId = nonce,
                                         IsDeleted = false,
                                         CreatedOn = DateTime.Now,
                                         CreatedBy = username,
                                         UpdatedOn = DateTime.Now,
                                         UpdatedBy = username
                                     };
                db.PaymentAccounts.Add(paymentAccount);
                db.SaveChanges();

                // Save payment
                var payment = new TopupPayment()
                              {
                                  Payer = username,
                                  IsSuccessful = false,
                                  PaymentAccountId = paymentAccount.Id,
                                  PaymentAmount = topUpAmount,
                                  CreatedOn = DateTime.Now,
                                  UpdatedOn = DateTime.Now
                              };

                db.TopupPayments.Add(payment);
                db.SaveChanges();

                if (paymentAccountType == (int) PaymentAccountTypes.Bank)
                {
                    payment.Status = (int) PaymentStatus.Pending;
                    db.SaveChanges();

                    // Move to next step
                    return RedirectToAction("PaymentConfirm", new { id = payment.Id });
                }
                else
                {
                    //var paymentAccount = db.PaymentAccounts.FirstOrDefault(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Id == paymentAccountId);
                    //if (paymentAccount == null)
                    //{
                    //    ViewBag.ErrorMessage = "You don't have this payment account. Please try with the other account.";
                    //    return View();
                    //}

                    // Call BrainTree to pay
                    var result = PaypalServices.PayWithBrainTree(payment.PaymentAmount, nonce);

                    if (result.IsSuccess())
                    {
                        // Topup
                        customer.Credit += payment.PaymentAmount;
                        customer.UpdatedOn = DateTime.Now;
                        customer.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";

                        payment.IsSuccessful = true;
                        payment.TransferAmount = payment.PaymentAmount;
                        payment.Status = (int) PaymentStatus.Approved;
                        payment.ApprovedOn = DateTime.Now;
                        payment.UpdatedOn = DateTime.Now;
                        db.SaveChanges();

                        ViewBag.SuccessMessage = "<p style='font-size: 20px; font-weight: bold'>Your transaction is successful.</p>Your Roadbull account has been topped up.";
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "We cannot process your payment right now. Please try again after few minutes.";
                        if (result.Errors != null && result.Errors.DeepCount > 0)
                        {
                            ViewBag.ErrorMessage = string.Join("<br />", result.Errors.DeepAll().Select(x => x.Message));
                        }
                        else if (result.Transaction != null)
                        {
                            if (result.Transaction.Status == TransactionStatus.PROCESSOR_DECLINED)
                            {
                                ViewBag.ErrorMessage = string.IsNullOrEmpty(result.Transaction.ProcessorResponseText)
                                                           ? result.Transaction.AdditionalProcessorResponse
                                                           : result.Transaction.ProcessorResponseText;
                            }
                            else if (result.Transaction.Status == TransactionStatus.SETTLEMENT_DECLINED)
                            {
                                ViewBag.ErrorMessage = result.Transaction.ProcessorSettlementResponseText;
                            }
                            else if (result.Transaction.Status == TransactionStatus.GATEWAY_REJECTED)
                            {
                                ViewBag.ErrorMessage = result.Transaction.GatewayRejectionReason;
                            }
                        }

                        return View();
                    }
                }

                //// Call Paypal to pay
                //var paypalAccessToken = PaypalServices.GetAccessToken();
                //if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                //{
                //    // Not token
                //    ViewBag.ErrorMessage = "We cannot process your payment right now. Please try again after few minutes.";
                //    return View();
                //}

                //if (paymentAccount.AccountType == (int) PaymentAccountTypes.PaypalAccount)
                //{
                //    // This is paypal
                //    var url = PaypalServices.InitTopupPayWithPaypal(Request, Url, payment.Id, paypalAccessToken,
                //        payment.PaymentAmount, $"Pay for top-up {topUpAmount.ToString("c")} on Roadbull website.");
                //    return Redirect(url);
                //}
                //else
                //{
                //    // This is credit card
                //    PaypalServices.PayWithCreditCard(paypalAccessToken,
                //        paymentAccount.PaypalCardId, payment.PaymentAmount,
                //        $"Pay for top-up {topUpAmount.ToString("c")} on Roadbull website.",
                //        paymentAccount.Currency);

                //    payment.IsSuccessful = true;
                //    db.SaveChanges();

                //    ViewBag.SuccessMessage = "Your Order has been processed successfully. Thank you for using Roadbull.";
                //    return View();
                //}
            }
            catch (PayPal.PaymentsException exc)
            {
                LogHelper.Log(exc);
                ViewBag.ErrorMessage = exc.Details.message + "<br />" +
                                       (exc.Details.details != null ? string.Join("<br />", exc.Details.details.Select(x => x.issue)) : "");
                return View();
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                ViewBag.ErrorMessage = "We cannot process your payment right now. Please try again after few minutes.";
                return View();
            }
        }

        public ActionResult PaypalPayment(string success, long? payment, int? topUpAmount, string paymentId, string payerID)
        {
            var db = new CourierDBEntities();
            var paymentObj = db.Payments.FirstOrDefault(x => x.Id == payment);
            if (paymentObj == null)
            {
                TempData["ErrorMessage"] = "We cannot process your payment right now. Please try again after few minutes.";
                return RedirectToAction(
                                        "TopupPayment",
                                        "Account",
                                        new
                                        {
                                            topUpAmount = topUpAmount ?? 0
                                        });
            }

            if (success == null || success.ToLower() != "true")
            {
                // Not success
                TempData["ErrorMessage"] = "You declined to process your Order or payment process is failed. Please try again after few minutes.";
                return RedirectToAction(
                                        "TopupPayment",
                                        "Account",
                                        new
                                        {
                                            topUpAmount = paymentObj.PaymentAmount
                                        });
            }

            try
            {
                // Call Paypal to pay
                var paypalAccessToken = PaypalServices.GetAccessToken();
                if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                {
                    // Not token
                    TempData["PaymentError"] = "We cannot process your payment right now. Please try again after few minutes.";
                    return RedirectToAction(
                                            "TopupPayment",
                                            "Account",
                                            new
                                            {
                                                topUpAmount = paymentObj.PaymentAmount
                                            });
                }

                PaypalServices.PayWithPaypal(paypalAccessToken, paymentId, payerID);

                paymentObj.IsSuccessful = true;
                db.SaveChanges();

                TempData["SuccessMessage"] = "Your Payment has been processed successfully. Thank you for using Roadbull.";
                return RedirectToAction(
                                        "TopupPayment",
                                        "Account",
                                        new
                                        {
                                            topUpAmount = paymentObj.PaymentAmount
                                        });
            }
            catch (PayPal.PaymentsException exc)
            {
                LogHelper.Log(exc);
                TempData["SuccessMessage"] = exc.Details.message + "<br />" +
                                             (exc.Details.details != null ? string.Join("<br />", exc.Details.details.Select(x => x.issue)) : "");
                return RedirectToAction(
                                        "TopupPayment",
                                        "Account",
                                        new
                                        {
                                            topUpAmount = paymentObj.PaymentAmount
                                        });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                TempData["SuccessMessage"] = "We cannot process your payment right now. Please try again after few minutes.";
                return RedirectToAction(
                                        "TopupPayment",
                                        "Account",
                                        new
                                        {
                                            topUpAmount = paymentObj.PaymentAmount
                                        });
            }
        }

        [Authorize]
        public ActionResult PaymentConfirm(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var db = new CourierDBEntities();
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var payment = db.TopupPayments.FirstOrDefault(x => x.Id == id);
            if (payment == null)
            {
                return HttpNotFound();
            }

            var paymentAccount = db.PaymentAccounts.FirstOrDefault(x => x.Id == payment.PaymentAccountId && !x.IsDeleted);
            if (paymentAccount == null)
            {
                return HttpNotFound();
            }

            if (payment.Payer.ToLower() != username.ToLower())
            {
                ViewBag.ErrorMessage = "You don't have permission to confirm this transaction.";
                return View();
            }

            var customer = db.Customers.FirstOrDefault(x => x.UserName.ToLower() == username.ToLower());
            if (customer == null)
            {
                ViewBag.ErrorMessage = "You don't have permissions to access this section.";
                return View();
            }

            if (payment.IsSuccessful || payment.Status == (int) PaymentStatus.Approved || payment.Status == (int) PaymentStatus.Declined)
            {
                ViewBag.ErrorMessage = "You already confirmed this transaction.";
                return View();
            }

            return View(
                        new PaymentViaBankModel()
                        {
                            CompanyName = customer.CustomerCompany,
                            MobileNumber = customer.CustomerPhone,
                            TransactionAmount = payment.PaymentAmount,
                            TransactionDate = paymentAccount.TransactionOn?.ToString("dd/MM/yyyy") ?? DateTime.Now.ToString("dd/MM/yyyy"),
                            Username = username,
                            TransactionNumber = paymentAccount.TransactionNumber,
                            TransactionTime = paymentAccount.TransactionOn?.ToString("HH:mm") ?? DateTime.Now.ToString("HH:mm"),
                        });
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PaymentConfirm(long? id, PaymentViaBankModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var db = new CourierDBEntities();
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var payment = db.TopupPayments.FirstOrDefault(x => x.Id == id);
            if (payment == null)
            {
                return HttpNotFound();
            }

            var paymentAccount = db.PaymentAccounts.FirstOrDefault(x => x.Id == payment.PaymentAccountId && !x.IsDeleted);
            if (paymentAccount == null)
            {
                return HttpNotFound();
            }

            if (payment.Payer.ToLower() != username.ToLower())
            {
                ViewBag.ErrorMessage = "You don't have permission to confirm this transaction.";
                return View();
            }

            var customer = db.Customers.FirstOrDefault(x => x.UserName.ToLower() == username.ToLower());
            if (customer == null)
            {
                ViewBag.ErrorMessage = "You don't have permissions to access this section.";
                return View();
            }

            if (payment.IsSuccessful || payment.Status == (int) PaymentStatus.Approved || payment.Status == (int) PaymentStatus.Declined)
            {
                ViewBag.ErrorMessage = "You already confirmed this transaction.";
                return View();
            }

            if (ModelState.IsValid)
            {
                // Validate date
                DateTime date;
                if (!DateTime.TryParseExact(model.TransactionDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out date))
                {
                    ViewBag.ErrorMessage = "Transferred Date has invalid format.";
                    return View(model);
                }

                DateTime time;
                if (!DateTime.TryParseExact(model.TransactionTime, "H:mm", CultureInfo.CurrentCulture, DateTimeStyles.None, out time))
                {
                    ViewBag.ErrorMessage = "Transferred Time has invalid format.";
                    return View(model);
                }

                // Save info
                paymentAccount.CompanyName = model.CompanyName;
                paymentAccount.TransactionNumber = model.TransactionNumber;
                paymentAccount.MobileNumber = model.MobileNumber;
                paymentAccount.TransactionOn = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, 0);
                paymentAccount.UpdatedOn = DateTime.Now;
                paymentAccount.UpdatedBy = username;

                payment.TransferAmount = model.TransactionAmount;
                payment.UpdatedOn = DateTime.Now;
                try
                {
                    db.SaveChanges();
                    ViewBag.SuccessMessage =
                        "<p style='font-size: 20px; font-weight: bold'>We have received your Account Top-up Request.</p>You will be noticed via email once your transaction has been verified and your account is topped up.";
                }
                catch (Exception exc)
                {
                    LogHelper.Log(exc);
                    ViewBag.ErrorMessage = "We cannot process your confirmation right now. Please try again after few minutes.";
                }

                // Send email to Operators
                var emails = (SettingProvider.OperatorsEmail ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    foreach (var email in emails)
                    {
                        // Load template
                        var emailTemplate = string.Join("\n", System.IO.File.ReadAllLines(Server.MapPath("~/Templates/email_payment_submit.html"), Encoding.UTF8));

                        emailTemplate = emailTemplate.Replace("{Customer}", customer.CustomerName);
                        emailTemplate = emailTemplate.Replace("{Company}", model.CompanyName);
                        emailTemplate = emailTemplate.Replace("{Amount}", model.TransactionAmount.ToString("C"));

                        var client = new System.Net.Mail.SmtpClient(SettingProvider.SmtpHostAddress, SettingProvider.SmtpHostPort)
                                     {
                                         DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                                         UseDefaultCredentials = false
                                     };

                        // Create the credentials:
                        var credentials = new System.Net.NetworkCredential(SettingProvider.SmtpUsername, SettingProvider.SmtpPassword);

                        client.EnableSsl = SettingProvider.SmtpSecured;
                        client.Credentials = credentials;

                        // Create the message:
                        var mailAddressFrom = new MailAddress(SettingProvider.SmtpSenderEmail, SettingProvider.SmtpSenderName);
                        var mailAddressTo = new MailAddress(email);
                        var mail = new MailMessage(mailAddressFrom, mailAddressTo)
                                   {
                                       Subject = "Top-up Payment Received",
                                       Body = emailTemplate,
                                       IsBodyHtml = true
                                   };

                        // Send
                        await client.SendMailAsync(mail);
                    }
                }
                catch (Exception exc)
                {
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessage = ModelState.Values.FirstOrDefault(x => x.Errors.Count > 0)?.Errors.FirstOrDefault()?.ErrorMessage;
            }

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl, string token)
        {
            if (!string.IsNullOrEmpty(returnUrl))
            {
                if (!string.IsNullOrEmpty(token))
                {
                    // Attach access token into url
                    if (returnUrl.Contains("?"))
                    {
                        returnUrl += "&token=" + token;
                    }
                    else
                    {
                        returnUrl += "?token=" + token;
                    }
                }
                return Redirect(returnUrl);
            }

            return RedirectToAction(
                                    "Home",
                                    "Order",
                                    new
                                    {
                                        token = token
                                    });
        }

        public static string GetAccessToken()
        {
            var token = System.Web.HttpContext.Current.Request.Cookies["CdsAccessToken"]?.Value ?? "";

            return token;
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }

            public string RedirectUri { get; set; }

            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion
    }
}
