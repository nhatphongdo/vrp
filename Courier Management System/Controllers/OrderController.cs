﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using Braintree;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using Excel;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.Threading.Tasks;
using CsvHelper;
using System.Data;
using FileHelpers;

namespace Courier_Management_System.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private CourierDBEntities db = new CourierDBEntities();

        public OrderController()
        {
            db.Database.CommandTimeout = 600;
        }

        public ActionResult Home(DateTime? startDate, DateTime? endDate, string vendor)
        {
            if (startDate == null)
            {
                startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }
            if (endDate == null)
            {
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            }
            ViewBag.StartDate = startDate.Value;
            ViewBag.EndDate = endDate.Value;
            ViewBag.Vendor = vendor;

            endDate = endDate.Value.AddDays(1);
            var username = this.User.Identity.Name;

            // Get list of vendors of current merchant
            if (User.IsInRole(AccountHelper.MerchantRole))
            {
                var vendors = db.Customers.Where(x => !x.IsDeleted && x.IsActive && x.MerchantUserName.Equals(username, StringComparison.OrdinalIgnoreCase)).ToList();
                ViewBag.Vendors = vendors;

                if (vendors.Any(x => x.UserName.Equals(vendor, StringComparison.OrdinalIgnoreCase)))
                {
                    // Avoid merchant view other merchant's vendors info
                    ViewBag.ConfirmedOrders =
                        db.Orders.Count(
                                        x =>
                                            !x.IsDeleted && x.Customer.UserName.Equals(vendor, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.OrderConfirmed && x.CreatedOn >= startDate &&
                                            x.CreatedOn < endDate);
                    ViewBag.DeliveredOrders =
                        db.Orders.Count(
                                        x =>
                                            !x.IsDeleted && x.Customer.UserName.Equals(vendor, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.ItemDelivered && x.CreatedOn >= startDate &&
                                            x.CreatedOn < endDate);

                    ViewBag.ItemInDepot =
                        db.Orders.Count(
                                        x =>
                                            !x.IsDeleted && x.Customer.UserName.Equals(vendor, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.ItemInDepot && x.CreatedOn >= startDate &&
                                            x.CreatedOn < endDate);
                    ViewBag.ItemOutDepot =
                        db.Orders.Count(
                                        x =>
                                            !x.IsDeleted && x.Customer.UserName.Equals(vendor, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.ItemOutDepot && x.CreatedOn >= startDate &&
                                            x.CreatedOn < endDate);
                    ViewBag.FailedDelivery =
                        db.Orders.Count(
                                        x =>
                                            !x.IsDeleted && x.Customer.UserName.Equals(vendor, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.DeliveryFailed && x.CreatedOn >= startDate &&
                                            x.CreatedOn < endDate);

                    return View();
                }
            }

            ViewBag.ConfirmedOrders =
                db.Orders.Count(
                                x =>
                                    !x.IsDeleted && x.Customer.UserName.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.OrderConfirmed && x.CreatedOn >= startDate &&
                                    x.CreatedOn < endDate);
            ViewBag.DeliveredOrders =
                db.Orders.Count(
                                x =>
                                    !x.IsDeleted && x.Customer.UserName.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.ItemDelivered && x.CreatedOn >= startDate &&
                                    x.CreatedOn < endDate);

            ViewBag.ItemInDepot =
                db.Orders.Count(
                                x =>
                                    !x.IsDeleted && x.Customer.UserName.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.ItemInDepot && x.CreatedOn >= startDate &&
                                    x.CreatedOn < endDate);
            ViewBag.ItemOutDepot =
                db.Orders.Count(
                                x =>
                                    !x.IsDeleted && x.Customer.UserName.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.ItemOutDepot && x.CreatedOn >= startDate &&
                                    x.CreatedOn < endDate);
            ViewBag.FailedDelivery =
                db.Orders.Count(
                                x =>
                                    !x.IsDeleted && x.Customer.UserName.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Status == (int)OrderStatuses.DeliveryFailed && x.CreatedOn >= startDate &&
                                    x.CreatedOn < endDate);

            return View();
        }

        public ActionResult Book()
        {
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).ToList();
            ViewBag.TimeSlots = db.TimeSlots.Where(x => !x.IsDeleted)
                                  .Select(
                                          x => new
                                          {
                                              x.Id,
                                              x.FromTime,
                                              x.ToTime,
                                              x.IsAllowPreOrder
                                          }).ToList();

            var username = this.User.Identity.Name;
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false)
            {
                ViewBag.ErrorMessage = "Your account is not verified or activated to book an order.";
                customer = null;
            }

            ViewBag.Customer = customer;

            if (TempData["BookModel"] != null && (TempData["BookModel"] as BookOrderViewModel) != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
                return View(TempData["BookModel"] as BookOrderViewModel);
            }

            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            var model = new BookOrderViewModel();
            model.FromName = customer?.CustomerName;
            var addressParts = (customer?.CustomerAddress ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var address = "";
            var unitNumber = "";
            var buildingName = "";
            if (addressParts.Length >= 3)
            {
                buildingName = addressParts[addressParts.Length - 1].Trim();
                unitNumber = addressParts[addressParts.Length - 2].Trim();
                address = string.Join(",", addressParts.Take(addressParts.Length - 2)).Trim();
            }
            else if (addressParts.Length >= 2)
            {
                unitNumber = addressParts[addressParts.Length - 1].Trim();
                address = string.Join(",", addressParts.Take(addressParts.Length - 1)).Trim();
            }
            else
            {
                address = (customer?.CustomerAddress ?? "").Trim();
            }
            model.FromAddress = address;
            model.FromUnitNumber = unitNumber;
            model.FromBuildingName = buildingName;
            model.FromZipCode = customer?.CustomerZipCode;
            model.FromMobilePhone = customer?.CustomerPhone;
            model.CustomerId = customer?.Id;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Book(BookOrderViewModel model)
        {
            var username = this.User.Identity.Name;
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false)
            {
                ViewBag.ErrorMessage = "Your account is not verified or activated to book an order.";
                customer = null;
            }

            ViewBag.Customer = customer;

            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).ToList();
            ViewBag.TimeSlots = db.TimeSlots.Where(x => !x.IsDeleted)
                                  .Select(
                                          x => new
                                          {
                                              x.Id,
                                              x.FromTime,
                                              x.ToTime,
                                              x.IsAllowPreOrder
                                          }).ToList();

            if (ModelState.IsValid)
            {
                if (!db.ZipCodes.Any(x => x.PostalCode.Equals(model.FromZipCode, StringComparison.OrdinalIgnoreCase)))
                {
                    ViewBag.ErrorMessage = "From Postal Code is not valid.";
                    return View(model);
                }

                if (!db.ZipCodes.Any(x => x.PostalCode.Equals(model.ToZipCode, StringComparison.OrdinalIgnoreCase)))
                {
                    ViewBag.ErrorMessage = "To Postal Code is not valid.";
                    return View(model);
                }

                TempData["BookModel"] = model;
                return RedirectToAction("Confirm");
            }

            ViewBag.ErrorMessage = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();

            return View(model);
        }

        public ActionResult Confirm()
        {
            var model = TempData["BookModel"] as BookOrderViewModel;
            if (model == null)
            {
                TempData["ErrorMessage"] = "Cannot process with this order. Please try to book order again.";
                return RedirectToAction("Book", "Order");
            }

            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.IsActive && x.Id == model.CustomerId);
            if (customer == null)
            {
                TempData["ErrorMessage"] = "Cannot retrieve your information. Please try to login before booking.";
                return RedirectToAction("Book", "Order");
            }

            ViewBag.Customer = customer;

            //var paymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
            //{
            //    Text = x.PaymentTypeName,
            //    Value = x.Id.ToString()
            //}).ToList();
            //ViewBag.PaymentTypes = paymentTypes;

            ViewBag.ProductType = db.ProductTypes.FirstOrDefault(x => x.Id == model.ProductTypeId);
            ViewBag.Size = db.Sizes.FirstOrDefault(x => x.Id == model.SizeId);
            ViewBag.Service = db.Services.FirstOrDefault(x => x.Id == model.ServiceId);
            if (ViewBag.Service != null && !ViewBag.Service.IsPickupDateAllow)
            {
                model.PickupDate = DateTime.Now.Date;
            }
            if (ViewBag.Service != null && !ViewBag.Service.IsDeliveryDateAllow)
            {
                model.DeliveryDate = model.PickupDate;
            }
            ViewBag.PickupTime = db.TimeSlots.FirstOrDefault(x => x.Id == model.PickupTimeSlotId);
            ViewBag.DeliveryTime = db.TimeSlots.FirstOrDefault(x => x.Id == model.DeliveryTimeSlotId);

            var price = OrderHelper.CalculatePrice(
                                                   new PriceRequestViewModel()
                                                   {
                                                       ProductTypeId = model.ProductTypeId,
                                                       CustomerId = model.CustomerId,
                                                       ServiceId = model.ServiceId,
                                                       SizeId = model.SizeId,
                                                       AddOnCode = ""
                                                   });

            // Calculate price without imposing minimum amount
            var total = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion + price.Surcharge;
            ViewBag.TotalPrice = total; //price.Total;
            ViewBag.Promotion = price.Promotion;

            model.Price = total;

            var now = DateTime.Now;
            ViewBag.Surcharges = db.Charges.Where(
                                                  x => !x.IsDeleted && x.ChargeAmount > 0 &&
                                                       (x.ValidFrom.HasValue == false || x.ValidFrom.Value <= now) &&
                                                       (x.ValidTo.HasValue == false || x.ValidTo.Value >= now) &&
                                                       (x.ServiceId.HasValue == false || x.ServiceId.Value == model.ServiceId) &&
                                                       (x.SizeId.HasValue == false || x.SizeId.Value == model.SizeId))
                                   .ToList();
            ViewBag.CodOptions = db.CodOptions.Where(x => !x.IsDeleted).ToList();

            ViewBag.ErrorMessage = TempData["ErrorMessage"];

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Confirm(BookOrderViewModel model)
        {
            var paymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                               x => new SelectListItem()
                                                                               {
                                                                                   Text = x.PaymentTypeName,
                                                                                   Value = x.Id.ToString()
                                                                               }).ToList();
            //ViewBag.PaymentTypes = paymentTypes;

            var now = DateTime.Now;
            ViewBag.Surcharges = db.Charges.Where(
                                                  x => !x.IsDeleted && x.ChargeAmount > 0 &&
                                                       (x.ValidFrom.HasValue == false || x.ValidFrom.Value <= now) &&
                                                       (x.ValidTo.HasValue == false || x.ValidTo.Value >= now) &&
                                                       (x.ServiceId.HasValue == false || x.ServiceId.Value == model.ServiceId) &&
                                                       (x.SizeId.HasValue == false || x.SizeId.Value == model.SizeId))
                                   .ToList();
            ViewBag.CodOptions = db.CodOptions.Where(x => !x.IsDeleted).ToList();

            ViewBag.ProductType = db.ProductTypes.FirstOrDefault(x => x.Id == model.ProductTypeId);
            ViewBag.Size = db.Sizes.FirstOrDefault(x => x.Id == model.SizeId);
            ViewBag.Service = db.Services.FirstOrDefault(x => x.Id == model.ServiceId);
            ViewBag.TotalPrice = model.Price;
            ViewBag.Promotion = 0;

            if (ModelState.IsValid)
            {
                var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.IsActive && x.Id == model.CustomerId);
                if (customer == null || (Request["submitAction"] ?? "").ToLower() == "back")
                {
                    TempData["BookModel"] = model;
                    TempData["ErrorMessage"] = customer == null ? "Cannot retrieve your information. Please try to login before booking." : null;
                    return RedirectToAction("Book", "Order");
                }

                ViewBag.Customer = customer;

                // Recalculate price to avoid intented changes from user
                var price = OrderHelper.CalculatePrice(
                                                       new PriceRequestViewModel()
                                                       {
                                                           ProductTypeId = model.ProductTypeId,
                                                           ServiceId = model.ServiceId,
                                                           CustomerId = model.CustomerId,
                                                           SizeId = model.SizeId,
                                                           AddOnCode = model.PromoCode + "," + Request["vas"],
                                                           CodAmounts = Request["codAmount"],
                                                           Cod = Request["cod"],
                                                           IsExchange = (Request["isExchange"] ?? "").ToLower() == "on"
                                                       });
                var total = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion + price.Surcharge;

                var order = new Order()
                {
                    FromZipCode = model.FromZipCode,
                    FromName = model.FromName,
                    FromAddress =
                                    model.FromAddress + (string.IsNullOrEmpty(model.FromUnitNumber) ? "" : ", " + model.FromUnitNumber) +
                                    (string.IsNullOrEmpty(model.FromBuildingName) ? "" : ", " + model.FromBuildingName),
                    FromMobilePhone = model.FromMobilePhone,
                    ToName = model.ToName,
                    ToZipCode = model.ToZipCode,
                    ToAddress = model.ToAddress + (string.IsNullOrEmpty(model.ToUnitNumber) ? "" : ", " + model.ToUnitNumber) + (string.IsNullOrEmpty(model.ToBuildingName) ? "" : ", " + model.ToBuildingName),
                    ToMobilePhone = model.ToMobilePhone,
                    PaymentTypeId = customer.PaymentTypeId,
                    ServiceId = model.ServiceId,
                    ProductTypeId = model.ProductTypeId,
                    Remark = model.Remark,
                    Price = total, //price.Total,
                    Status = (int)OrderStatuses.OrderSubmitted,
                    CustomerId = model.CustomerId,
                    SizeId = model.SizeId,
                    PickupTimeSlotId = model.PickupTimeSlotId,
                    PickupDate = model.PickupDate,
                    DeliveryTimeSlotId = model.DeliveryTimeSlotId,
                    DeliveryDate = model.DeliveryDate,
                    PromoCode = (model.PromoCode + "," + Request["vas"]).Trim(','),
                    IsExchange = (Request["isExchange"] ?? "").ToLower() == "on",
                    IsDeleted = false,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };

                var service = db.Services.FirstOrDefault(x => x.Id == model.ServiceId);
                if (service != null && !service.IsPickupDateAllow)
                {
                    order.PickupDate = DateTime.Now.Date;
                }
                if (service != null && !service.IsDeliveryDateAllow)
                {
                    order.DeliveryDate = order.PickupDate;
                }

                order.BinCode = OrderHelper.GenerateBinCode(order);

                db.Orders.Add(order);

                try
                {
                    db.SaveChanges();

                    order.ConsignmentNumber = OrderHelper.GenerateConsignmentNumber(order.Id);
                    order.OrderNumber = customer.CompanyPrefix + order.ConsignmentNumber;

                    // Build QR code
                    var services = db.Services.Where(x => !x.IsDeleted).Select(
                                                                               x => new SelectListItem()
                                                                               {
                                                                                   Text = x.ServiceName,
                                                                                   Value = x.Id.ToString()
                                                                               }).ToList();
                    var sizes = db.Sizes.Where(x => !x.IsDeleted).Select(
                                                                         x => new SelectListItem()
                                                                         {
                                                                             Text = x.SizeName,
                                                                             Value = x.Id.ToString()
                                                                         }).ToList();
                    var productTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                       x => new SelectListItem()
                                                                                       {
                                                                                           Text = x.ProductTypeName,
                                                                                           Value = x.Id.ToString()
                                                                                       }).ToList();
                    // Build QR Code
                    var bitmap = OrderHelper.GenerateQrCode(paymentTypes, services, productTypes, sizes, order);
                    order.QrCodePath = Url.Content(OrderHelper.SaveQrCode(order.ConsignmentNumber, bitmap));
                    // Build Bar Code
                    var barCode = OrderHelper.GenerateBarCode(order.ConsignmentNumber);
                    var barCodePath = OrderHelper.SaveBarCode(order.ConsignmentNumber, barCode);

                    var track = new OrderTracking
                    {
                        ConsignmentNumber = order.ConsignmentNumber,
                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        CreatedOn = DateTime.Now,
                        PickupTimeSlotId = order.PickupTimeSlotId,
                        PickupDate = order.PickupDate,
                        DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                        DeliveryDate = order.DeliveryDate,
                        BinCode = order.BinCode,
                        Status = order.Status
                    };
                    db.OrderTrackings.Add(track);

                    // Get Cart ID
                    var cartId = Request.Cookies["Cart"] == null ? "" : Request.Cookies["Cart"].Value;
                    Guid cartGuid = Guid.Empty;
                    Guid.TryParse(cartId, out cartGuid);
                    var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == cartGuid);
                    if (mapDN == null)
                    {
                        mapDN = new ConsignmentNumber()
                        {
                            ConsignmentNumbers = order.ConsignmentNumber,
                            Id = Guid.NewGuid(),
                            CreatedOn = DateTime.Now
                        };
                        db.ConsignmentNumbers.Add(mapDN);
                    }
                    else
                    {
                        mapDN.ConsignmentNumbers += "," + order.ConsignmentNumber;
                    }

                    db.SaveChanges();

                    // Add COD Options
                    var cod = (Request["cod"] ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    var codAmounts = (Request["codAmount"] ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (var i = 0; i < cod.Length; i++)
                    {
                        int id = 0;
                        if (!int.TryParse(cod[i], out id))
                        {
                            continue;
                        }

                        decimal amount = 0;
                        if (i < codAmounts.Length)
                        {
                            decimal.TryParse(codAmounts[i], out amount);
                        }

                        var codOption = db.CodOptions.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                        if (codOption == null)
                        {
                            continue;
                        }

                        var codOptionOfOrder = new CodOptionOfOrder()
                        {
                            Amount = amount,
                            CodOptionId = codOption.Id,
                            CollectedAmount = 0,
                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            CreatedOn = DateTime.Now,
                            IsCollected = false,
                            IsDeleted = false,
                            OrderId = order.Id,
                            UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            UpdatedOn = DateTime.Now
                        };
                        db.CodOptionOfOrders.Add(codOptionOfOrder);
                    }

                    db.SaveChanges();

                    return RedirectToAction(
                                            "Cart",
                                            "Order",
                                            new
                                            {
                                                id = mapDN.Id.ToString()
                                            });
                }
                catch (Exception exc)
                {
                    // Rollback
                    LogHelper.Log(exc);
                    TempData["BookModel"] = model;
                    TempData["ErrorMessage"] = "Errors occur in processing request. Please contact to Customer Care for support.";
                    return RedirectToAction("Book", "Order");
                }
            }

            TempData["BookModel"] = model;
            TempData["ErrorMessage"] = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
            return RedirectToAction("Book", "Order");
        }

        public ActionResult Cart(string id)
        {
            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);

                if (mapDN == null)
                {
                    ViewBag.ErrorMessage = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                    return View();
                }

                // Check payment
                var orderIds = mapDN.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var orderIdsLength = orderIds.Length;
                var payment = db.Payments.FirstOrDefault(x => x.IsSuccessful && x.OrdersOfPayments.Count(o => orderIds.Contains(o.ConsignmentNumber.ToLower())) == orderIdsLength);
                if (payment != null)
                {
                    Response.Cookies.Remove("Cart");
                    var cookie = new HttpCookie("Cart") { Expires = DateTime.Now.AddDays(-1) };
                    Response.Cookies.Add(cookie);

                    // Already paid
                    return RedirectToAction(
                                            "Tracking",
                                            "Order",
                                            new
                                            {
                                                id = id
                                            });
                }

                ViewBag.CartId = mapDN.Id.ToString();
                ViewBag.Timeslots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();

                var aCookie = new HttpCookie("Cart", mapDN.Id.ToString()) { Expires = DateTime.Now.AddDays(1) };
                Response.Cookies.Add(aCookie);

                var orders = db.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
                return View(orders);
            }
            else
            {
                ViewBag.ErrorMessage = "We cannot find information of your Shopping Cart. Please try to book other orders or contact to Customer Support for help.";
            }

            return View();
        }

        [HttpPost]
        [ActionName("Cart")]
        public async Task<ActionResult> Cart_Post(string id)
        {
            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);

                if (mapDN == null)
                {
                    ViewBag.ErrorMessage = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                    return View();
                }

                // Check payment
                var orderIds = mapDN.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var orderIdsLength = orderIds.Length;
                var oldPayment = db.Payments.FirstOrDefault(x => x.IsSuccessful && x.OrdersOfPayments.Count(o => orderIds.Contains(o.ConsignmentNumber.ToLower())) == orderIdsLength);
                if (oldPayment != null)
                {
                    Response.Cookies.Remove("Cart");
                    var cookie = new HttpCookie("Cart") { Expires = DateTime.Now.AddDays(-1) };
                    Response.Cookies.Add(cookie);

                    // Already paid
                    return RedirectToAction(
                                            "Tracking",
                                            "Order",
                                            new
                                            {
                                                id = id
                                            });
                }

                ViewBag.CartId = mapDN.Id.ToString();
                ViewBag.Timeslots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();

                var orders = db.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();

                if (orders.Count == 0)
                {
                    return View();
                }

                var paymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                   {
                                                                                       Text = x.PaymentTypeName,
                                                                                       Value = x.Id.ToString()
                                                                                   }).ToList();
                var customerId = orders.FirstOrDefault().CustomerId;
                var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.IsActive && x.Id == customerId);
                if (customer == null)
                {
                    TempData["ErrorMessage"] = "Cannot retrieve your information. Please try to login before booking.";
                    return RedirectToAction("Book", "Order");
                }

                var total = orders.Sum(x => x.Price);
                total = Math.Max(SettingProvider.MinimumOrderAmount, total);
                if (paymentTypes.FirstOrDefault(x => x.Value == customer.PaymentTypeId.ToString())?.Text?.ToLower().Contains("postpaid") == true)
                {
                    // Check balance
                    if (customer.Credit < total)
                    {
                        ViewBag.ErrorMessage = "Sorry you have used up your credit limit. Please pay your outstanding amount due before placing order. Thank you.";
                        return View(orders);
                    }

                    // For Post Paid
                    foreach (var order in orders)
                    {
                        order.Status = (int)OrderStatuses.OrderConfirmed;
                        order.ConfirmedOn = DateTime.Now;
                    }

                    customer.Credit -= total;
                    // Save payment
                    var payment = new Payment()
                    {
                        Payer = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        AccountType = (int)PaymentTypes.PostPaid,
                        IsSuccessful = true,
                        PaymentAmount = total,
                        CreatedOn = DateTime.Now
                    };
                    foreach (var order in orders)
                    {
                        var orderInPayment = new OrdersOfPayment()
                        {
                            ConsignmentNumber = order.ConsignmentNumber,
                        };
                        payment.OrdersOfPayments.Add(orderInPayment);
                    }

                    db.Payments.Add(payment);

                    db.SaveChanges();

                    OrderHelper.SendOrderEmail(Url, orders, mapDN.Id.ToString());
                    await OrderHelper.SendOrderSms(Url, orders, mapDN.Id.ToString());

                    // Remove cart cookie
                    Response.Cookies.Remove("Cart");
                    var aCookie = new HttpCookie("Cart") { Expires = DateTime.Now.AddDays(-1) };
                    Response.Cookies.Add(aCookie);

                    TempData["SuccessMessage"] = "Your Order has been processed successfully. Thank you for using Roadbull.";
                    return RedirectToAction(
                                            "Tracking",
                                            "Order",
                                            new
                                            {
                                                id = mapDN.Id.ToString()
                                            });
                }
                else
                {
                    // For Pre Paid
                    return RedirectToAction(
                                            "PaymentAccounts",
                                            "Order",
                                            new
                                            {
                                                id = mapDN.Id.ToString()
                                            });
                }
            }
            else
            {
                ViewBag.ErrorMessage = "We cannot find information of your Shopping Cart. Please try to book other orders or contact to Customer Support for help.";
            }

            return View();
        }

        [HttpPost]
        public ActionResult RemoveCartItem(string id, string consignmentNumber, string action)
        {
            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);
                if (mapDN != null)
                {
                    var ids = mapDN.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    mapDN.ConsignmentNumbers = string.Join(",", ids.Where(x => !x.Equals(consignmentNumber.ToLower()))).ToUpper();

                    // Remove order
                    var order = db.Orders.FirstOrDefault(x => !x.IsDeleted && x.ConsignmentNumber.ToLower() == consignmentNumber.ToLower());
                    if (order != null)
                    {
                        order.IsDeleted = true;
                    }

                    if (order != null && !string.IsNullOrEmpty(action) && action.ToLower() == "edit")
                    {
                        // Generate model
                        var model = new BookOrderViewModel()
                        {
                            FromZipCode = order.FromZipCode,
                            FromName = order.FromName,
                            FromMobilePhone = order.FromMobilePhone,
                            ToName = order.ToName,
                            ToZipCode = order.ToZipCode,
                            ToMobilePhone = order.ToMobilePhone,
                            ServiceId = order.ServiceId,
                            ProductTypeId = order.ProductTypeId,
                            Remark = order.Remark,
                            CustomerId = order.CustomerId,
                            SizeId = order.SizeId,
                            PickupTimeSlotId = order.PickupTimeSlotId,
                            PickupDate = order.PickupDate,
                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                            DeliveryDate = order.DeliveryDate,
                            PromoCode = order.PromoCode,
                        };

                        var addressParts = (order.FromAddress ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        var address = "";
                        var unitNumber = "";
                        var buildingName = "";
                        if (addressParts.Length >= 3)
                        {
                            buildingName = addressParts[addressParts.Length - 1].Trim();
                            unitNumber = addressParts[addressParts.Length - 2].Trim();
                            address = string.Join(",", addressParts.Take(addressParts.Length - 2)).Trim();
                        }
                        else if (addressParts.Length >= 2)
                        {
                            unitNumber = addressParts[addressParts.Length - 1].Trim();
                            address = string.Join(",", addressParts.Take(addressParts.Length - 1)).Trim();
                        }
                        else
                        {
                            address = order.FromAddress.Trim();
                        }
                        model.FromAddress = address;
                        model.FromUnitNumber = unitNumber;
                        model.FromBuildingName = buildingName;

                        addressParts = (order.ToAddress ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        address = "";
                        unitNumber = "";
                        buildingName = "";
                        if (addressParts.Length >= 3)
                        {
                            buildingName = addressParts[addressParts.Length - 1].Trim();
                            unitNumber = addressParts[addressParts.Length - 2].Trim();
                            address = string.Join(",", addressParts.Take(addressParts.Length - 2)).Trim();
                        }
                        else if (addressParts.Length >= 2)
                        {
                            unitNumber = addressParts[addressParts.Length - 1].Trim();
                            address = string.Join(",", addressParts.Take(addressParts.Length - 1)).Trim();
                        }
                        else
                        {
                            address = order.ToAddress.Trim();
                        }
                        model.ToAddress = address;
                        model.ToUnitNumber = unitNumber;
                        model.ToBuildingName = buildingName;


                        db.SaveChanges();

                        TempData["BookModel"] = model;
                        return RedirectToAction("Book", "Order");
                    }

                    db.SaveChanges();
                }
            }

            return RedirectToAction(
                                    "Cart",
                                    "Order",
                                    new
                                    {
                                        id = id
                                    });
        }

        [AllowAnonymous]
        public ActionResult Tracking(string id, string check = "false")
        {
            if (TempData["SuccessMessage"] != null)
            {
                ViewBag.SuccessMessage = TempData["SuccessMessage"];
            }

            if (string.IsNullOrEmpty(id))
            {
                ViewBag.ErrorMessage = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                return View();
            }

            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);

                if (mapDN == null)
                {
                    ViewBag.ErrorMessage = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                    return View();
                }

                ViewBag.CNI = id;

                id = mapDN.ConsignmentNumbers;
            }
            else if (id.ToLower().StartsWith("g"))
            {
                // Get consignment numbers corresponding to group code
                ViewBag.CNI = id;
                check = "true";
                id = string.Join(",", db.Orders.Where(x => x.GroupTrackingNumber.ToLower() == id.ToLower()).Select(x => x.ConsignmentNumber));
            }
            else if (id.ToLower().StartsWith("rb_"))
            {
                // Get consignment numbers corresponding to order number
                ViewBag.CNI = id;
                check = "true";

                if (id.Substring(3).Contains("_"))
                {
                    // There is another "_", using RB_[Prefix]_[Invoice]
                    var parts = id.Substring(3).Split(new char[] { '_' });
                    if (parts.Length == 2)
                    {
                        // Get invoice number
                        long invoiceNumber = 0;
                        if (long.TryParse(parts[1], out invoiceNumber))
                        {
                            id = string.Join(
                                             ",",
                                             db.Payments.Where(x => x.IsSuccessful && x.Id == invoiceNumber)
                                               .SelectMany(x => x.OrdersOfPayments.Select(o => o.ConsignmentNumber))
                                               .ToList());
                        }
                    }
                }
                else
                {
                    var orderNumber = id.Substring(3);
                    id = string.Join(",", db.Orders.Where(x => x.OrderNumber.ToLower() == orderNumber.ToLower()).Select(x => x.ConsignmentNumber));
                }
            }
            else
            {
                ViewBag.CNI = id;
            }

            if (string.IsNullOrEmpty(check))
            {
                check = "false";
            }

            ViewBag.Check = check.ToLower() == "true";

            // Check payment
            var orderIds = id.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orderIdsLength = orderIds.Length;
            var payment = db.Payments.FirstOrDefault(x => x.IsSuccessful && x.OrdersOfPayments.Count(o => orderIds.Contains(o.ConsignmentNumber.ToLower())) == orderIdsLength);
            if (check.ToLower() != "true" && payment == null)
            {
                // Not paid
                return RedirectToAction(
                                        "PaymentAccounts",
                                        "Order",
                                        new
                                        {
                                            id = id
                                        });
            }

            var orders = db.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
            ViewBag.Orders = orders;
            var orderTrackings = db.OrderTrackings.Where(x => orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
            ViewBag.OrderTrackings = orderTrackings;

            return View();
        }

        public ActionResult ExportTracking(string id, string check = "false")
        {
            if (TempData["SuccessMessage"] != null)
            {
                ViewBag.SuccessMessage = TempData["SuccessMessage"];
            }

            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);

                if (mapDN == null)
                {
                    ViewBag.ErrorMessage = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                    return View();
                }

                ViewBag.CNI = id;

                id = mapDN.ConsignmentNumbers;
            }
            else if (id.ToLower().StartsWith("g"))
            {
                // Get consignment numbers corresponding to group code
                ViewBag.CNI = id;
                id = string.Join(",", db.Orders.Where(x => x.GroupTrackingNumber.ToLower() == id.ToLower()).Select(x => x.ConsignmentNumber));
            }
            else if (id.ToLower().StartsWith("rb_"))
            {
                // Get consignment numbers corresponding to order number
                ViewBag.CNI = id;

                if (id.Substring(3).Contains("_"))
                {
                    // There is another "_", using RB_[Prefix]_[Invoice]
                    var parts = id.Substring(3).Split(new char[] { '_' });
                    if (parts.Length == 2)
                    {
                        // Get invoice number
                        long invoiceNumber = 0;
                        if (long.TryParse(parts[1], out invoiceNumber))
                        {
                            id = string.Join(
                                             ",",
                                             db.Payments.Where(x => x.IsSuccessful && x.Id == invoiceNumber)
                                               .SelectMany(x => x.OrdersOfPayments.Select(o => o.ConsignmentNumber))
                                               .ToList());
                        }
                    }
                }
                else
                {
                    var orderNumber = id.Substring(3);
                    id = string.Join(",", db.Orders.Where(x => x.OrderNumber.ToLower() == orderNumber.ToLower()).Select(x => x.ConsignmentNumber));
                }
            }
            else
            {
                ViewBag.CNI = id;
            }

            if (string.IsNullOrEmpty(id))
            {
                ViewBag.ErrorMessage = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                return View();
            }

            if (string.IsNullOrEmpty(check))
            {
                check = "false";
            }

            var orderIds = id.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orders = db.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
            var timeslots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();

            var folderPath = Server.MapPath("~/Temp/");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            var fileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".xlsx";
            var filePath = folderPath + fileName;
            var outputFile = new FileInfo(filePath);

            var package = new ExcelPackage(outputFile);
            // Add the sheet
            var ws = package.Workbook.Worksheets.Add("Orders");
            ws.View.ShowGridLines = false;

            // Headers
            if (check.ToLower() == "true")
            {
                ws.Cells["A1"].Value = "Number";
                ws.Cells["B1"].Value = "Consignment Number";
                ws.Cells["C1"].Value = "GroupTracking Number";
                ws.Cells["D1"].Value = "Fulfillment Number";
                ws.Cells["E1"].Value = "Order Number";
                ws.Cells["F1"].Value = "Status";
                ws.Cells["G1"].Value = "Tracking Link";
                ws.Cells["H1"].Value = "Customer Company";
                ws.Cells["I1"].Value = "Pickup Date";
                ws.Cells["J1"].Value = "To Name";
                ws.Cells["K1"].Value = "To Zip Code";
                ws.Cells["L1"].Value = "To Address";
                ws.Cells["M1"].Value = "To Mobile Number";
                ws.Cells["N1"].Value = "Delivery Date";
                ws.Cells["O1"].Value = "Delivery Slot";
                ws.Cells["P1"].Value = "Delivery EPOD";
                ws.Cells["Q1"].Value = "Delivery EPOD Name";
                ws.Cells["R1"].Value = "Delivery EPOD Time";
                ws.Cells["S1"].Value = "Remarks";
                ws.Cells["A1:S1"].Style.Font.Bold = true;
            }
            else
            {
                ws.Cells["A1"].Value = "Number";
                ws.Cells["B1"].Value = "Consignment Number";
                ws.Cells["C1"].Value = "GroupTracking Number";
                ws.Cells["D1"].Value = "Fulfillment Number";
                ws.Cells["E1"].Value = "Order Tracking Number";
                ws.Cells["F1"].Value = "Status";
                ws.Cells["G1"].Value = "Tracking Link";
                ws.Cells["H1"].Value = "Remarks";
                ws.Cells["I1"].Value = "Customer Company";
                ws.Cells["J1"].Value = "From Name";
                ws.Cells["K1"].Value = "From Zip Code";
                ws.Cells["L1"].Value = "From Address";
                ws.Cells["M1"].Value = "Pickup Date";
                ws.Cells["N1"].Value = "Pickup Slot";
                ws.Cells["O1"].Value = "To Name";
                ws.Cells["P1"].Value = "To Zip Code";
                ws.Cells["Q1"].Value = "To Address";
                ws.Cells["R1"].Value = "To Mobile Number";
                ws.Cells["S1"].Value = "Delivery Date";
                ws.Cells["T1"].Value = "Delivery Slot";
                ws.Cells["U1"].Value = "Service";
                ws.Cells["V1"].Value = "Size";
                ws.Cells["W1"].Value = "Invoice Grand Total";
                ws.Cells["X1"].Value = "Pickup EPOD";
                ws.Cells["Y1"].Value = "Pickup EPOD Name";
                ws.Cells["Z1"].Value = "Pickup EPOD Time";
                ws.Cells["AA1"].Value = "Delivery EPOD";
                ws.Cells["AB1"].Value = "Delivery EPOD Name";
                ws.Cells["AC1"].Value = "Delivery EPOD Time";
                ws.Cells["A1:AC1"].Style.Font.Bold = true;
            }

            // Data
            var i = 0;
            foreach (var item in orders)
            {
                ws.Cells[i + 2, 1].Value = i + 1;
                ws.Cells[i + 2, 2].Value = item.ConsignmentNumber;
                ws.Cells[i + 2, 3].Value = item.GroupTrackingNumber;
                ws.Cells[i + 2, 4].Value = item.FullfilmentNumber;
                ws.Cells[i + 2, 5].Value = "RB_" + item.OrderNumber;
                if (item.Status == (int)OrderStatuses.SpecialOrderForDelivery)
                {
                    ws.Cells[i + 2, 6].Value = "Pending Dispatch for Delivery";
                }
                else if (item.Status == (int)OrderStatuses.SpecialOrderForPickup)
                {
                    ws.Cells[i + 2, 6].Value = "Pending Dispatch for Pickup";
                }
                else
                {
                    ws.Cells[i + 2, 6].Value = EnumHelpers.GetDisplayValue((OrderStatuses)item.Status);
                }
                if (check.ToLower() == "true")
                {
                    ws.Cells[i + 2, 7].Hyperlink = new Uri(
                                                           Url.Action(
                                                                      "Tracking",
                                                                      "Order",
                                                                      new
                                                                      {
                                                                          // id = item.ConsignmentNumber
                                                                          id = "RB_" + item.OrderNumber
                                                                      },
                                                                      Request.Url.Scheme));
                    ws.Cells[i + 2, 8].Value = item.Customer.CustomerCompany;
                    ws.Cells[i + 2, 9].Value = item.PickupDate?.ToString("dd/MM/yyyy");
                    ws.Cells[i + 2, 10].Value = item.ToName;
                    ws.Cells[i + 2, 11].Value = item.ToZipCode;
                    ws.Cells[i + 2, 12].Value = item.ToAddress;
                    ws.Cells[i + 2, 13].Value = item.ToMobilePhone;
                    ws.Cells[i + 2, 14].Value = item.DeliveryDate?.ToString("dd/MM/yyyy");
                    ws.Cells[i + 2, 15].Value = timeslots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.TimeSlotName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(item.ToSignature))
                    {
                        var parts = item.ToSignature.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        if (parts.Length > 1)
                        {
                            ws.Cells[i + 2, 16].Hyperlink = new Uri(parts[0].Trim());
                            ws.Cells[i + 2, 17].Value = parts[1].Trim();
                        }
                    }
                    ws.Cells[i + 2, 18].Value = item.DeliveredOn?.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[i + 2, 19].Value = item.Remark;
                }
                else
                {
                    ws.Cells[i + 2, 7].Hyperlink = new Uri(
                                                           Url.Action(
                                                                      "Tracking",
                                                                      "Order",
                                                                      new
                                                                      {
                                                                          //id = item.ConsignmentNumber
                                                                          id = "RB_" + item.OrderNumber
                                                                      },
                                                                      Request.Url.Scheme));

                    ws.Cells[i + 2, 8].Value = item.Remark;
                    ws.Cells[i + 2, 9].Value = item.Customer.CustomerCompany;
                    ws.Cells[i + 2, 10].Value = item.FromName;
                    ws.Cells[i + 2, 11].Value = item.FromZipCode;
                    ws.Cells[i + 2, 12].Value = item.FromAddress;
                    ws.Cells[i + 2, 13].Value = item.PickupDate?.ToString("dd/MM/yyyy");
                    ws.Cells[i + 2, 14].Value = timeslots.Where(x => x.Id == item.PickupTimeSlotId).Select(x => x.TimeSlotName).FirstOrDefault();
                    ws.Cells[i + 2, 15].Value = item.ToName;
                    ws.Cells[i + 2, 16].Value = item.ToZipCode;
                    ws.Cells[i + 2, 17].Value = item.ToAddress;
                    ws.Cells[i + 2, 18].Value = item.ToMobilePhone;
                    ws.Cells[i + 2, 19].Value = item.DeliveryDate?.ToString("dd/MM/yyyy");
                    ws.Cells[i + 2, 20].Value = timeslots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.TimeSlotName).FirstOrDefault();
                    ws.Cells[i + 2, 21].Value = item.Service.ServiceName;
                    ws.Cells[i + 2, 22].Value = item.Size.SizeName;
                    ws.Cells[i + 2, 23].Value = item.Price;
                    if (!string.IsNullOrEmpty(item.FromSignature))
                    {
                        var parts = item.FromSignature.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        if (parts.Length > 1)
                        {
                            ws.Cells[i + 2, 24].Hyperlink = new Uri(parts[0].Trim());
                            ws.Cells[i + 2, 25].Value = parts[1].Trim();
                        }
                    }
                    ws.Cells[i + 2, 26].Value = item.CollectedOn?.ToString("dd/MM/yyyy HH:mm:ss");
                    if (!string.IsNullOrEmpty(item.ToSignature))
                    {
                        var parts = item.ToSignature.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        if (parts.Length > 1)
                        {
                            ws.Cells[i + 2, 27].Hyperlink = new Uri(parts[0].Trim());
                            ws.Cells[i + 2, 28].Value = parts[1].Trim();
                        }
                    }
                    ws.Cells[i + 2, 29].Value = item.DeliveredOn?.ToString("dd/MM/yyyy HH:mm:ss");
                }

                ++i;
            }

            ws.Column(1).AutoFit();
            ws.Column(2).AutoFit();
            ws.Column(3).AutoFit();
            ws.Column(4).AutoFit();
            ws.Column(5).AutoFit();
            ws.Column(6).AutoFit();
            ws.Column(7).AutoFit();
            ws.Column(8).AutoFit();
            ws.Column(9).AutoFit();
            ws.Column(10).AutoFit();
            ws.Column(11).AutoFit();
            ws.Column(12).AutoFit();
            ws.Column(13).AutoFit();
            ws.Column(14).AutoFit();
            ws.Column(15).AutoFit();
            ws.Column(16).AutoFit();
            ws.Column(17).AutoFit();
            ws.Column(18).AutoFit();
            ws.Column(19).AutoFit();
            ws.Column(20).AutoFit();
            ws.Column(21).AutoFit();
            ws.Column(22).AutoFit();
            ws.Column(23).AutoFit();
            ws.Column(24).AutoFit();
            ws.Column(25).AutoFit();
            ws.Column(26).AutoFit();
            ws.Column(26).AutoFit();
            ws.Column(27).AutoFit();
            ws.Column(28).AutoFit();
            ws.Column(29).AutoFit();
            package.Save();

            return File(filePath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        public ActionResult PaymentAccounts(string id)
        {
            Guid guid = Guid.Empty;
            Guid.TryParse(id, out guid);
            var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);

            if (mapDN == null)
            {
                TempData["ErrorMessage"] = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                return RedirectToAction("Book", "Order");
            }

            var orderIds = mapDN.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orders = db.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
            if (orders.Count == 0)
            {
                TempData["ErrorMessage"] = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                return RedirectToAction("Book", "Order");
            }

            ViewBag.ConsignmentNumbers = id;

            // Check payment
            var orderIdsLength = orderIds.Length;
            var payment = db.Payments.FirstOrDefault(x => x.IsSuccessful && x.OrdersOfPayments.Count(o => orderIds.Contains(o.ConsignmentNumber.ToLower())) == orderIdsLength);
            if (payment != null)
            {
                // Already paid
                return RedirectToAction(
                                        "Tracking",
                                        "Order",
                                        new
                                        {
                                            id = id
                                        });
            }

            // Load all payment accounts
            ViewBag.PaymentError = TempData["PaymentError"];
            var username = User.Identity.Name;
            var paymentAccounts = db.PaymentAccounts.Where(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase)).ToList();
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            ViewBag.Customer = customer;

            ViewBag.BrainTreeClientToken = PaypalServices.GetBrainTreeClientToken();

            return View(paymentAccounts);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PaymentAccounts(string id, PaymentAccountViewModel model)
        {
            Guid guid = Guid.Empty;
            Guid.TryParse(id, out guid);
            var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);

            if (mapDN == null)
            {
                TempData["ErrorMessage"] = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                return RedirectToAction("Book", "Order");
            }

            ViewBag.ConsignmentNumbers = id;
            ViewBag.PaymentAccount = model;
            ViewBag.BrainTreeClientToken = PaypalServices.GetBrainTreeClientToken();

            // Check payment
            var orderIds = mapDN.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orderIdsLength = orderIds.Length;
            var payment = db.Payments.FirstOrDefault(x => x.IsSuccessful && x.OrdersOfPayments.Count(o => orderIds.Contains(o.ConsignmentNumber.ToLower())) == orderIdsLength);
            if (payment != null)
            {
                // Already paid
                return RedirectToAction(
                                        "Tracking",
                                        "Order",
                                        new
                                        {
                                            id = id
                                        });
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // Save to Paypal
                    var paypalAccessToken = PaypalServices.GetAccessToken();
                    if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                    {
                        // Not token
                        ViewBag.ErrorMessage = "We cannot process your request right now. Please try again after few minutes.";
                        ViewBag.ReopenAdd = true;
                    }
                    else
                    {
                        if (model.AccountType == (int)PaymentAccountTypes.PaypalAccount)
                        {
                            var account = new PaymentAccount()
                            {
                                AccountType = model.AccountType,
                                CardNumber = model.CardNumber,
                                Currency = model.Currency,
                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                IsDeleted = false,
                                Owner = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                UpdatedOn = DateTime.Now,
                                UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                            };

                            db.PaymentAccounts.Add(account);
                            try
                            {
                                db.SaveChanges();
                                ViewBag.ReopenAdd = false;
                            }
                            catch (Exception exc)
                            {
                                LogHelper.Log(exc);
                                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to Customer Care for support.";
                                ViewBag.ReopenAdd = true;
                            }
                        }
                        else
                        {
                            var result = PaypalServices.SaveCreditCard(
                                                                       paypalAccessToken,
                                                                       model.AccountType == (int)PaymentAccountTypes.VisaCard ? "visa" : "mastercard",
                                                                       model.CardNumber,
                                                                       model.ExpireMonth.GetValueOrDefault(),
                                                                       model.ExpireYear.GetValueOrDefault(),
                                                                       model.FirstName,
                                                                       model.LastName,
                                                                       model.CardCVV);

                            if (result.state.Equals("ok", StringComparison.OrdinalIgnoreCase))
                            {
                                // Save to DB
                                var account = new PaymentAccount()
                                {
                                    AccountType = model.AccountType,
                                    CardNumber = result.number,
                                    Currency = model.Currency,
                                    PaypalCardId = result.id,
                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    IsDeleted = false,
                                    Owner = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                    UpdatedOn = DateTime.Now,
                                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                                };

                                db.PaymentAccounts.Add(account);
                                try
                                {
                                    db.SaveChanges();
                                    ViewBag.ReopenAdd = false;
                                }
                                catch (Exception exc)
                                {
                                    LogHelper.Log(exc);
                                    ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to Customer Care for support.";
                                    ViewBag.ReopenAdd = true;
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to Customer Care for support.";
                                ViewBag.ReopenAdd = true;
                            }
                        }
                    }
                }
                catch (PayPal.PaymentsException exc)
                {
                    LogHelper.Log(exc);
                    ViewBag.ErrorMessage = exc.Details.message + "<br />" +
                                           (exc.Details.details != null ? string.Join("<br />", exc.Details.details.Select(x => x.issue)) : "");
                    ViewBag.ReopenAdd = true;
                }
            }
            else
            {
                ViewBag.ErrorMessage = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                ViewBag.ReopenAdd = true;
            }

            // Load all payment accounts
            var username = User.Identity.Name;
            var paymentAccounts = db.PaymentAccounts.Where(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase)).ToList();
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            ViewBag.Customer = customer;

            return View(paymentAccounts);
        }

        public ActionResult RemovePaymentAccount(string id, long? aid)
        {
            if (aid.HasValue)
            {
                var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                var paymentAccount = db.PaymentAccounts.FirstOrDefault(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Id == aid);
                if (paymentAccount != null)
                {
                    try
                    {
                        paymentAccount.IsDeleted = true;
                        db.SaveChanges();
                    }
                    catch (Exception exc)
                    {
                        LogHelper.Log(exc);
                    }
                }
            }

            return RedirectToAction(
                                    "PaymentAccounts",
                                    "Order",
                                    new
                                    {
                                        id = id
                                    });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Payment(string id, long? paymentAccountId, FormCollection formCollection)
        {
            Guid guid = Guid.Empty;
            Guid.TryParse(id, out guid);
            var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);

            if (mapDN == null)
            {
                TempData["ErrorMessage"] = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                return RedirectToAction("Book", "Order");
            }

            var orderIds = mapDN.ConsignmentNumbers.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var orders = db.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
            if (orders.Count == 0)
            {
                TempData["ErrorMessage"] = "We cannot find any order with these Consignment Number(s). Please verify the Consignment Number(s) or contact to Customer Support for help.";
                return RedirectToAction("Book", "Order");
            }

            // Check payment
            var orderIdsLength = orderIds.Length;
            var oldPayment = db.Payments.FirstOrDefault(x => x.IsSuccessful && x.OrdersOfPayments.Count(o => orderIds.Contains(o.ConsignmentNumber.ToLower())) == orderIdsLength);
            if (oldPayment != null)
            {
                // Already paid
                return RedirectToAction(
                                        "Tracking",
                                        "Order",
                                        new
                                        {
                                            id = id
                                        });
            }

            var total = orders.Sum(x => x.Price);
            total = Math.Max(SettingProvider.MinimumOrderAmount, total);

            try
            {
                var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                if (paymentAccountId == 0)
                {
                    // This is for Credit
                    // Check balance
                    var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                    if (customer.Credit < total)
                    {
                        TempData["PaymentError"] = "You don't have enough Credit to make payment. Please purchase more credit or use other method.";
                        return RedirectToAction(
                                                "PaymentAccounts",
                                                "Order",
                                                new
                                                {
                                                    id = id
                                                });
                    }
                    else
                    {
                        customer.Credit -= total;
                        // Add to payment
                        var creditPayment = new Payment()
                        {
                            Payer = username,
                            AccountType = (int)PaymentTypes.PrePaid,
                            IsSuccessful = true,
                            PaymentAmount = total,
                            CreatedOn = DateTime.Now
                        };
                        foreach (var consignmentNumber in mapDN.ConsignmentNumbers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            var orderInPayment = new OrdersOfPayment()
                            {
                                ConsignmentNumber = consignmentNumber,
                            };
                            creditPayment.OrdersOfPayments.Add(orderInPayment);
                        }

                        db.Payments.Add(creditPayment);

                        foreach (var order in orders)
                        {
                            order.Status = (int)OrderStatuses.OrderConfirmed;
                            order.ConfirmedOn = DateTime.Now;

                            var track = new OrderTracking
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                PickupTimeSlotId = order.PickupTimeSlotId,
                                PickupDate = order.PickupDate,
                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                DeliveryDate = order.DeliveryDate,
                                BinCode = order.BinCode,
                                Status = order.Status
                            };
                            db.OrderTrackings.Add(track);
                        }

                        db.SaveChanges();

                        OrderHelper.SendOrderEmail(Url, orders, id);
                        await OrderHelper.SendOrderSms(Url, orders, id);

                        Response.Cookies.Remove("Cart");
                        var aCookie = new HttpCookie("Cart") { Expires = DateTime.Now.AddDays(-1) };
                        Response.Cookies.Add(aCookie);

                        TempData["SuccessMessage"] = "Your Order has been processed successfully. Thank you for using Roadbull.";
                        return RedirectToAction(
                                                "Tracking",
                                                "Order",
                                                new
                                                {
                                                    id = id
                                                });
                    }
                }

                //var paymentAccount = db.PaymentAccounts.FirstOrDefault(x => !x.IsDeleted && x.Owner.Equals(username, StringComparison.OrdinalIgnoreCase) && x.Id == paymentAccountId);
                //if (paymentAccount == null)
                //{
                //    TempData["PaymentError"] = "You don't have this payment account. Please try with the other account.";
                //    return RedirectToAction("PaymentAccounts", "Order", new
                //    {
                //        id = id
                //    });
                //}

                // Create payment account to save data
                var nonce = formCollection["payment_method_nonce"];
                var paymentAccount = new PaymentAccount
                {
                    Owner = username,
                    AccountType = (int)PaymentAccountTypes.BrainTree,
                    PaypalCardId = nonce,
                    IsDeleted = false,
                    CreatedOn = DateTime.Now,
                    CreatedBy = username,
                    UpdatedOn = DateTime.Now,
                    UpdatedBy = username
                };
                db.PaymentAccounts.Add(paymentAccount);
                db.SaveChanges();

                // Add charge for Paypal
                total += (decimal)(3.9 / 100) * total + (decimal)0.5;
                total = Math.Round(total, 2);

                // Save payment
                var payment = new Payment()
                {
                    Payer = username,
                    IsSuccessful = false,
                    AccountType = (int)PaymentTypes.PostPaid,
                    PaymentAccountId = paymentAccount.Id,
                    PaymentAmount = total,
                    CreatedOn = DateTime.Now
                };
                foreach (var consignmentNumber in mapDN.ConsignmentNumbers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var orderInPayment = new OrdersOfPayment()
                    {
                        ConsignmentNumber = consignmentNumber,
                    };
                    payment.OrdersOfPayments.Add(orderInPayment);
                }

                db.Payments.Add(payment);
                db.SaveChanges();

                // Call BrainTree to pay
                var result = PaypalServices.PayWithBrainTree(payment.PaymentAmount, nonce);

                if (result.IsSuccess())
                {
                    payment.IsSuccessful = true;

                    foreach (var order in orders)
                    {
                        order.Status = (int)OrderStatuses.OrderConfirmed;
                        order.ConfirmedOn = DateTime.Now;

                        var track = new OrderTracking
                        {
                            ConsignmentNumber = order.ConsignmentNumber,
                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            CreatedOn = DateTime.Now,
                            PickupTimeSlotId = order.PickupTimeSlotId,
                            PickupDate = order.PickupDate,
                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                            DeliveryDate = order.DeliveryDate,
                            BinCode = order.BinCode,
                            Status = order.Status
                        };
                        db.OrderTrackings.Add(track);
                    }

                    db.SaveChanges();

                    OrderHelper.SendOrderEmail(Url, orders, id);
                    await OrderHelper.SendOrderSms(Url, orders.ToList(), id);


                    Response.Cookies.Remove("Cart");
                    var bCookie = new HttpCookie("Cart") { Expires = DateTime.Now.AddDays(-1) };
                    Response.Cookies.Add(bCookie);

                    TempData["SuccessMessage"] =
                        "Your Order has been processed successfully. Thank you for using Roadbull.";
                    return RedirectToAction(
                                            "Tracking",
                                            "Order",
                                            new
                                            {
                                                id = id
                                            });
                }
                else
                {
                    TempData["PaymentError"] = "We cannot process your payment right now. Please try again after few minutes.";
                    if (result.Errors != null && result.Errors.Count > 0)
                    {
                        TempData["PaymentError"] = string.Join("<br />", result.Errors.DeepAll().Select(x => x.Message));
                    }
                    else if (result.Transaction != null)
                    {
                        if (result.Transaction.Status == TransactionStatus.PROCESSOR_DECLINED)
                        {
                            TempData["PaymentError"] = string.IsNullOrEmpty(result.Transaction.ProcessorResponseText)
                                                           ? result.Transaction.AdditionalProcessorResponse
                                                           : result.Transaction.ProcessorResponseText;
                        }
                        else if (result.Transaction.Status == TransactionStatus.SETTLEMENT_DECLINED)
                        {
                            TempData["PaymentError"] = result.Transaction.ProcessorSettlementResponseText;
                        }
                        else if (result.Transaction.Status == TransactionStatus.GATEWAY_REJECTED)
                        {
                            TempData["PaymentError"] = result.Transaction.GatewayRejectionReason;
                        }
                    }

                    return RedirectToAction(
                                            "PaymentAccounts",
                                            "Order",
                                            new
                                            {
                                                id = id
                                            });
                }

                // Call Paypal to pay
                //var paypalAccessToken = PaypalServices.GetAccessToken();
                //if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                //{
                //    // Not token
                //    TempData["PaymentError"] = "We cannot process your payment right now. Please try again after few minutes.";
                //    return RedirectToAction("PaymentAccounts", "Order", new
                //    {
                //        id = id
                //    });
                //}

                //var consignmentNumbers = orders.Select(x => "#" + x.ConsignmentNumber).Take(10).ToList();

                //if (paymentAccount.AccountType == (int) PaymentAccountTypes.PaypalAccount)
                //{
                //    // This is paypal
                //    var url = PaypalServices.InitPayWithPaypal(Request, Url, payment.Id, id, paypalAccessToken,
                //        payment.PaymentAmount, $"Pay for order {string.Join(", ", consignmentNumbers)} on Roadbull website.");
                //    return Redirect(url);
                //}
                //else
                //{
                //    // This is credit card
                //    PaypalServices.PayWithCreditCard(paypalAccessToken,
                //        paymentAccount.PaypalCardId, payment.PaymentAmount,
                //        $"Pay for order {string.Join(", ", consignmentNumbers)} on Roadbull website.",
                //        paymentAccount.Currency);

                //    payment.IsSuccessful = true;

                //    foreach (var order in orders)
                //    {
                //        order.Status = (int) OrderStatuses.OrderConfirmed;
                //        order.ConfirmedOn = DateTime.Now;

                //        var track = new OrderTracking
                //        {
                //            ConsignmentNumber = order.ConsignmentNumber,
                //            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                //            CreatedOn = DateTime.Now,
                //            PickupTimeSlotId = order.PickupTimeSlotId,
                //            PickupDate = order.PickupDate,
                //            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                //            DeliveryDate = order.DeliveryDate,
                //            BinCode = order.BinCode,
                //            Status = order.Status
                //        };
                //        db.OrderTrackings.Add(track);
                //    }

                //    db.SaveChanges();

                //    OrderHelper.SendOrderEmail(Url, orders, id);

                //    Response.Cookies.Remove("Cart");
                //    var aCookie = new HttpCookie("Cart") { Expires = DateTime.Now.AddDays(-1) };
                //    Response.Cookies.Add(aCookie);

                //    TempData["SuccessMessage"] = "Your Order has been processed successfully. Thank you for using Roadbull.";
                //    return RedirectToAction("Tracking", "Order", new
                //    {
                //        id = id
                //    });
                //}
            }
            //catch (PayPal.PaymentsException exc)
            //{
            //    LogHelper.Log(exc);
            //    TempData["PaymentError"] = exc.Details.message + "<br />" +
            //        (exc.Details.details != null ? string.Join("<br />", exc.Details.details.Select(x => x.issue)) : "");
            //    return RedirectToAction("PaymentAccounts", "Order", new
            //    {
            //        id = id
            //    });
            //}
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                TempData["PaymentError"] = "We cannot process your payment right now. Please try again after few minutes.";
                return RedirectToAction(
                                        "PaymentAccounts",
                                        "Order",
                                        new
                                        {
                                            id = id
                                        });
            }
        }

        public async Task<ActionResult> PaypalPayment(string success, long? payment, string cni, string paymentId, string payerID)
        {
            var paymentObj = db.Payments.FirstOrDefault(x => x.Id == payment);
            if (paymentObj == null)
            {
                TempData["ErrorMessage"] = "We cannot process your payment right now. Please try again after few minutes.";
                return RedirectToAction("Book", "Order");
            }

            if (success == null || success.ToLower() != "true")
            {
                // Not success
                TempData["ErrorMessage"] = "You declined to process your Order or payment process is failed. Please try again after few minutes.";
                return RedirectToAction(
                                        "PaymentAccounts",
                                        "Order",
                                        new
                                        {
                                            id = cni
                                        });
            }

            try
            {
                // Call Paypal to pay
                var paypalAccessToken = PaypalServices.GetAccessToken();
                if (!paypalAccessToken.ToLower().StartsWith("bearer"))
                {
                    // Not token
                    TempData["PaymentError"] = "We cannot process your payment right now. Please try again after few minutes.";
                    return RedirectToAction(
                                            "PaymentAccounts",
                                            "Order",
                                            new
                                            {
                                                id = cni
                                            });
                }

                PaypalServices.PayWithPaypal(paypalAccessToken, paymentId, payerID);

                paymentObj.IsSuccessful = true;

                var orderIds = paymentObj.OrdersOfPayments.Select(x => x.ConsignmentNumber.ToLower()).ToList();
                var orders = db.Orders.Where(x => orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();

                foreach (var order in orders)
                {
                    order.Status = (int)OrderStatuses.OrderConfirmed;
                    order.ConfirmedOn = DateTime.Now;

                    var track = new OrderTracking
                    {
                        ConsignmentNumber = order.ConsignmentNumber,
                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        CreatedOn = DateTime.Now,
                        PickupTimeSlotId = order.PickupTimeSlotId,
                        PickupDate = order.PickupDate,
                        DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                        DeliveryDate = order.DeliveryDate,
                        BinCode = order.BinCode,
                        Status = order.Status
                    };
                    db.OrderTrackings.Add(track);
                }

                db.SaveChanges();

                OrderHelper.SendOrderEmail(Url, orders, cni);
                await OrderHelper.SendOrderSms(Url, orders, cni);


                Response.Cookies.Remove("Cart");
                var aCookie = new HttpCookie("Cart") { Expires = DateTime.Now.AddDays(-1) };
                Response.Cookies.Add(aCookie);

                TempData["SuccessMessage"] = "Your Order has been processed successfully. Thank you for using Roadbull.";
                return RedirectToAction(
                                        "Tracking",
                                        "Order",
                                        new
                                        {
                                            id = cni
                                        });
            }
            catch (PayPal.PaymentsException exc)
            {
                LogHelper.Log(exc);
                TempData["PaymentError"] = exc.Details.message + "<br />" +
                                           (exc.Details.details != null ? string.Join("<br />", exc.Details.details.Select(x => x.issue)) : "");
                return RedirectToAction(
                                        "PaymentAccounts",
                                        "Order",
                                        new
                                        {
                                            id = cni
                                        });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                TempData["PaymentError"] = "We cannot process your payment right now. Please try again after few minutes.";
                return RedirectToAction(
                                        "PaymentAccounts",
                                        "Order",
                                        new
                                        {
                                            id = cni
                                        });
            }
        }

        public ActionResult PrintCode(string id, int? startNumber)
        {
            // Check if id is UUID
            Guid guid;
            if (Guid.TryParse(id, out guid))
            {
                var mapDN = db.ConsignmentNumbers.FirstOrDefault(x => x.Id == guid);
                if (mapDN != null)
                {
                    id = mapDN.ConsignmentNumbers;
                }
            }
            else if (id.ToLower().StartsWith("g"))
            {
                // Get consignment numbers corresponding to group code
                id = string.Join(",", db.Orders.Where(x => x.GroupTrackingNumber.ToLower() == id.ToLower()).Select(x => x.ConsignmentNumber));
            }

            var orderIds = id.ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            //var orders = db.Orders.Where(x => orderIds.Contains(x.ConsignmentNumber.ToLower())).ToList();
            var orders = db.Orders.Where(x => orderIds.Contains(x.ConsignmentNumber.ToLower())).OrderBy(x => x.Id).ToList();

            ViewBag.StartNumber = ((startNumber > 0 ? startNumber : 1) - 1) % 8;

            ViewBag.Orders = orders;
            if (orders.Count == 0)
            {
                ViewBag.ErrorMessage = "There is no order corresponding to these Consignment Number(s).";
            }
            return View();
        }

        public ActionResult History(string searchTerm = "", int page = 1)
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            var customerId = customer?.Id;
            var searchConsignment = string.IsNullOrEmpty(searchTerm) || customer == null
                                        ? ""
                                        : db.Orders.Where(
                                                          x => !x.IsDeleted && x.CustomerId == customerId &&
                                                               (x.ConsignmentNumber.ToLower() == searchTerm.ToLower() ||
                                                                x.GroupTrackingNumber.ToLower() == searchTerm.ToLower() ||
                                                                x.OrderNumber.ToLower() == searchTerm.ToLower() ||
                                                                ("rb_" + x.OrderNumber.ToLower()) == searchTerm.ToLower()))
                                            .Select(x => x.ConsignmentNumber)
                                            .FirstOrDefault();
            var payments = (from payment in db.Payments
                            where payment.IsSuccessful && payment.Payer.Equals(username, StringComparison.OrdinalIgnoreCase)
                            orderby payment.CreatedOn descending
                            select new HistoryPayment()
                            {
                                AccountType = payment.AccountType,
                                CreatedOn = payment.CreatedOn,
                                PaymentAccount = payment.PaymentAccount,
                                PaymentAmount = payment.PaymentAmount,
                                Id = payment.Id,
                                CNI = db.ConsignmentNumbers.Where(x => payment.OrdersOfPayments.All(o => x.ConsignmentNumbers.Contains(o.ConsignmentNumber))).Select(x => x.Id.ToString()).FirstOrDefault(),
                                Orders = db.Orders.Where(
                                                                x =>
                                                                    !x.IsDeleted && !string.IsNullOrEmpty(x.ConsignmentNumber)
                                                                    && payment.OrdersOfPayments.Select(o => o.ConsignmentNumber.ToLower()).Contains(x.ConsignmentNumber.ToLower()) &&
                                                                    x.CustomerId == customerId && (searchConsignment == "" || x.ConsignmentNumber == searchConsignment)).OrderBy(x => x.Id).ToList(),
                                HasOrder = db.Orders.Any(
                                                                x =>
                                                                    !x.IsDeleted && !string.IsNullOrEmpty(x.ConsignmentNumber)
                                                                    && payment.OrdersOfPayments.Select(o => o.ConsignmentNumber.ToLower()).Contains(x.ConsignmentNumber.ToLower()) &&
                                                                    x.CustomerId == customerId && (searchConsignment == "" || x.ConsignmentNumber == searchConsignment))
                            })
                .Where(x => x.HasOrder);

            ViewBag.Page = page;
            ViewBag.TotalItems = payments.Count();
            ViewBag.TotalPages = ViewBag.TotalItems / 10;
            if (ViewBag.TotalItems % 10 > 0)
            {
                ViewBag.TotalPages = ViewBag.TotalPages + 1;
            }

            var result = payments.Skip((page - 1) * 10).Take(10).ToList();
            foreach (var item in result)
            {
                if (string.IsNullOrEmpty(item.CNI))
                {
                    var query = db.ConsignmentNumbers.AsQueryable();
                    query = item.Orders.Aggregate(query, (current, order) => current.Where(x => ("," + x.ConsignmentNumbers + ",").Contains("," + order.ConsignmentNumber + ",")));
                    item.CNI = query.Select(x => x.Id.ToString()).FirstOrDefault();
                }
            }

            return View(result);
        }

        public ActionResult Export(string id)
        {
            long numId = 0;
            long.TryParse(id, out numId);
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var payment = db.Payments.Where(x => x.IsSuccessful && x.Id == numId && x.Payer.Equals(username, StringComparison.OrdinalIgnoreCase))
                            .Select(
                                    p => new HistoryPayment()
                                    {
                                        AccountType = p.AccountType,
                                        CreatedOn = p.CreatedOn,
                                        PaymentAccount = p.PaymentAccount,
                                        PaymentAmount = p.PaymentAmount,
                                        Id = p.Id,
                                        Orders =
                                                 db.Orders.Where(
                                                                 x =>
                                                                     !x.IsDeleted && !string.IsNullOrEmpty(x.ConsignmentNumber)
                                                                     && p.OrdersOfPayments.Select(o => o.ConsignmentNumber.ToLower()).Contains(x.ConsignmentNumber.ToLower())).ToList(),
                                        PayerUserName = p.Payer,
                                        Payer = db.Customers.FirstOrDefault(x => x.UserName.Equals(p.Payer, StringComparison.OrdinalIgnoreCase))
                                    })
                            .FirstOrDefault();
            if (payment == null)
            {
                return HttpNotFound();
            }

            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).ToList();

            var folderPath = Server.MapPath("~/Temp/");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            var fileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".xlsx";
            var filePath = folderPath + fileName;
            var outputFile = new FileInfo(filePath);

            var package = new ExcelPackage(outputFile);
            // Add the sheet
            var ws = package.Workbook.Worksheets.Add("Orders");
            var timeslots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();
            ws.View.ShowGridLines = false;

            // Headers
            ws.Cells["A1"].Value = "Number";
            ws.Cells["B1"].Value = "Consignment Number";
            ws.Cells["C1"].Value = "GroupTracking Number";
            ws.Cells["D1"].Value = "Fulfillment Number";
            ws.Cells["E1"].Value = "Order Tracking Number";
            ws.Cells["F1"].Value = "Tracking Link";
            ws.Cells["G1"].Value = "Remarks";
            ws.Cells["H1"].Value = "Status";
            ws.Cells["I1"].Value = "Customer Company";
            ws.Cells["J1"].Value = "From Name";
            ws.Cells["K1"].Value = "From Zipcode";
            ws.Cells["L1"].Value = "From Address";
            ws.Cells["M1"].Value = "Pickup Date";
            ws.Cells["N1"].Value = "Pickup Slot";
            ws.Cells["O1"].Value = "To Name";
            ws.Cells["P1"].Value = "To Zipcode";
            ws.Cells["Q1"].Value = "To Address";
            ws.Cells["R1"].Value = "To MobileNumber";
            ws.Cells["S1"].Value = "Delivery Date";
            ws.Cells["T1"].Value = "Delivery Slot";
            ws.Cells["U1"].Value = "Service";
            ws.Cells["V1"].Value = "Size";
            ws.Cells["W1"].Value = "Invoice Grand Total";


            ws.Cells["A1:W1"].Style.Font.Bold = true;

            // Data
            var i = 0;
            foreach (var item in payment.Orders)
            {
                ws.Cells[i + 2, 1].Value = i + 1;
                ws.Cells[i + 2, 2].Value = item.ConsignmentNumber;
                ws.Cells[i + 2, 3].Value = item.GroupTrackingNumber;
                ws.Cells[i + 2, 4].Value = item.FullfilmentNumber;
                ws.Cells[i + 2, 5].Value = "RB_" + item.OrderNumber;
                ws.Cells[i + 2, 6].Value = Url.Action(
                                                      "Tracking",
                                                      "Order",
                                                      new
                                                      {
                                                          id = "RB_" + item.OrderNumber
                                                          // id = item.ConsignmentNumber
                                                      },
                                                      Request.Url.Scheme);
                ws.Cells[i + 2, 7].Value = item.Remark;

                if (item.Status == (int)OrderStatuses.SpecialOrderForDelivery)
                {
                    ws.Cells[i + 2, 8].Value = "Pending Dispatch for Delivery";
                }
                else if (item.Status == (int)OrderStatuses.SpecialOrderForPickup)
                {
                    ws.Cells[i + 2, 8].Value = "Pending Dispatch for Pickup";
                }
                else
                {
                    ws.Cells[i + 2, 8].Value = EnumHelpers.GetDisplayValue((OrderStatuses)item.Status);
                }
                ws.Cells[i + 2, 9].Value = item.Customer.CustomerCompany;
                ws.Cells[i + 2, 10].Value = item.FromName;
                ws.Cells[i + 2, 11].Value = item.FromZipCode;
                ws.Cells[i + 2, 12].Value = item.FromAddress;
                ws.Cells[i + 2, 13].Value = item.PickupDate?.ToString("dd/MM/yyyy");
                ws.Cells[i + 2, 14].Value = timeslots.Where(x => x.Id == item.PickupTimeSlotId).Select(x => x.TimeSlotName).FirstOrDefault();
                ws.Cells[i + 2, 15].Value = item.ToName;
                ws.Cells[i + 2, 16].Value = item.ToZipCode;
                ws.Cells[i + 2, 17].Value = item.ToAddress;
                ws.Cells[i + 2, 18].Value = item.ToMobilePhone;
                ws.Cells[i + 2, 19].Value = item.DeliveryDate?.ToString("dd/MM/yyyy");
                ws.Cells[i + 2, 20].Value = timeslots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.TimeSlotName).FirstOrDefault();
                ws.Cells[i + 2, 21].Value = item.Service.ServiceName;
                ws.Cells[i + 2, 22].Value = item.Size.SizeName;
                ws.Cells[i + 2, 23].Value = item.Price;

                ++i;
            }

            ws.Column(1).AutoFit();
            ws.Column(2).AutoFit();
            ws.Column(3).AutoFit();
            ws.Column(4).AutoFit();
            ws.Column(5).AutoFit();
            ws.Column(6).AutoFit();
            ws.Column(7).AutoFit();
            ws.Column(8).AutoFit();
            ws.Column(9).AutoFit();
            ws.Column(10).AutoFit();
            ws.Column(11).AutoFit();
            ws.Column(12).AutoFit();
            ws.Column(13).AutoFit();
            ws.Column(14).AutoFit();
            ws.Column(15).AutoFit();
            ws.Column(16).AutoFit();
            ws.Column(17).AutoFit();
            ws.Column(18).AutoFit();
            ws.Column(19).AutoFit();
            ws.Column(20).AutoFit();
            ws.Column(21).AutoFit();
            ws.Column(22).AutoFit();
            ws.Column(23).AutoFit();
            package.Save();

            return File(filePath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        public ActionResult Invoice(string id)
        {
            var isAdmin = User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.OrdersInvoicesReportPrintInvoice);
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            long numId;
            long.TryParse(id, out numId);
            var payment = db.Payments.Where(x => x.IsSuccessful && x.Id == numId && (x.Payer.Equals(username, StringComparison.OrdinalIgnoreCase) || isAdmin))
                            .Select(
                                    p => new HistoryPayment()
                                    {
                                        AccountType = p.AccountType,
                                        CreatedOn = p.CreatedOn,
                                        PaymentAccount = p.PaymentAccount,
                                        PaymentAmount = p.PaymentAmount,
                                        Id = p.Id,
                                        Orders =
                                                 db.Orders.Where(
                                                                 x =>
                                                                     !x.IsDeleted && !string.IsNullOrEmpty(x.ConsignmentNumber)
                                                                     && p.OrdersOfPayments.Select(o => o.ConsignmentNumber.ToLower()).Contains(x.ConsignmentNumber.ToLower())).ToList(),
                                        PayerUserName = p.Payer,
                                        Payer = db.Customers.FirstOrDefault(x => x.UserName.Equals(p.Payer, StringComparison.OrdinalIgnoreCase))
                                    })
                            .FirstOrDefault();
            if (payment == null)
            {
                ViewBag.ErrorMessage = "There is no invoice corresponding to this request.";
            }
            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).ToList();
            return View(payment);
        }

        public ActionResult Bulk()
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false)
            {
                ViewBag.ErrorMessage = "Your account is not verified or activated to book an order.";
                customer = null;
            }
            ViewBag.Customer = customer;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Bulk(FormCollection form)
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null)
            {
                ViewBag.ErrorMessage = "Cannot retrieve your information. Please try to login before booking.";
                return View();
            }

            try
            {
                var hasFile = false;
                var result = new List<Order>();
                foreach (var fileKey in Request.Files.AllKeys)
                {
                    var file = Request.Files[fileKey];
                    if (file.ContentLength > 0)
                    {
                        hasFile = true;
                        List<Order> list = null;
                        if (fileKey.Equals("StdDocumentUploadFile", StringComparison.OrdinalIgnoreCase))
                        {
                            list = ReadOrders(file, SettingProvider.StandardDocumentServiceId);
                        }
                        else if (fileKey.Equals("StdParcelUploadFile", StringComparison.OrdinalIgnoreCase))
                        {
                            list = ReadOrders(file, SettingProvider.StandardParcelServiceId);
                        }
                        else if (fileKey.Equals("ScheduleParcelUploadFile", StringComparison.OrdinalIgnoreCase))
                        {
                            list = ReadOrders(file, SettingProvider.ScheduleParcelServiceId);
                        }

                        if (list != null)
                        {
                            result.AddRange(list);
                        }
                        else if (ViewBag.ErrorMessage != null)
                        {
                            return View();
                        }
                    }
                }

                if (hasFile)
                {
                    TempData["OrderList"] = result;
                    return RedirectToAction("BulkConfirm", "Order");
                }
                else
                {
                    // There is no file
                    ViewBag.ErrorMessage = "There is no file selected. Please upload file to corresponding service type.";
                }
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Issues happen when importing files. Please try again or contact to Customer Care for support.";
            }

            return View();
        }

        public ActionResult BulkConfirm()
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            ViewBag.Customer = customer;
            ViewBag.TimeSlots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();
            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).ToList();
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).ToList();
            ViewBag.CodOptions = db.CodOptions.Where(x => !x.IsDeleted).ToList();

            var offDayStrs = (SettingProvider.OutOfServiceDays ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var offDays = new List<DateTime>();
            foreach (var day in offDayStrs)
            {
                offDays.Add(DateTime.ParseExact(day, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None));
            }

            ViewBag.OffDays = offDays;

            var list = TempData["OrderList"] as List<Order>;
            if (list == null)
            {
                return RedirectToAction("Bulk", "Order");
            }

            if (customer != null && customer.UseGroupPrice)
            {
                for (var i = 0; i < list.Count; i++)
                {
                    list[i].Id = i + 1;
                }

                var groupedOrders = list.GroupBy(
                                                 x => new
                                                 {
                                                     x.FromAddress,
                                                     x.ToAddress,
                                                     x.ToZipCode,
                                                     x.ToMobilePhone,
                                                     x.DeliveryDate,
                                                     x.DeliveryTimeSlotId
                                                 }).ToList();
                foreach (var group in groupedOrders)
                {
                    if (group.Count() < 2)
                    {
                        continue;
                    }

                    var firstOrder = group.FirstOrDefault();
                    if (firstOrder != null)
                    {
                        var maxPrice = group.Max(x => x.Price);
                        // Associate all orders in the same group with same group tracking number generated from this
                        foreach (var order in group)
                        {
                            if (customer.UseGroupPrice)
                            {
                                list.FirstOrDefault(x => x.Id == order.Id).Price = 0;
                            }
                            list.FirstOrDefault(x => x.Id == order.Id).GroupTrackingNumber = firstOrder.Id.ToString();
                        }

                        if (customer.UseGroupPrice)
                        {
                            list.FirstOrDefault(x => x.Id == firstOrder.Id).Price = maxPrice;
                        }
                    }
                }
            }

            return View(list.OrderBy(x => x.GroupTrackingNumber).ThenByDescending(x => x.Price).ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BulkConfirm(List<Order> model)
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null)
            {
                ViewBag.ErrorMessage = "Cannot retrieve your information. Please try to login before booking.";
                return View();
            }

            ViewBag.Customer = customer;
            var timeSlots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();
            ViewBag.TimeSlots = timeSlots;
            var serviceList = db.Services.Where(x => !x.IsDeleted).ToList();
            ViewBag.Services = serviceList;
            var sizeList = db.Sizes.Where(x => !x.IsDeleted).ToList();
            ViewBag.Sizes = sizeList;
            var productTypeList = db.ProductTypes.Where(x => !x.IsDeleted).ToList();
            ViewBag.ProductTypes = productTypeList;
            var offDayStrs = (SettingProvider.OutOfServiceDays ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var offDays = new List<DateTime>();
            foreach (var day in offDayStrs)
            {
                offDays.Add(DateTime.ParseExact(day, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None));
            }

            ViewBag.OffDays = offDays;
            ViewBag.CodOptions = db.CodOptions.Where(x => !x.IsDeleted).ToList();

            try
            {
                var paymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                   {
                                                                                       Text = x.PaymentTypeName,
                                                                                       Value = x.Id.ToString()
                                                                                   }).ToList();
                var services = serviceList.Select(
                                                  x => new SelectListItem()
                                                  {
                                                      Text = x.ServiceName,
                                                      Value = x.Id.ToString()
                                                  }).ToList();
                var sizes = sizeList.Select(
                                            x => new SelectListItem()
                                            {
                                                Text = x.SizeName,
                                                Value = x.Id.ToString()
                                            }).ToList();
                var productTypes = productTypeList.Select(
                                                          x => new SelectListItem()
                                                          {
                                                              Text = x.ProductTypeName,
                                                              Value = x.Id.ToString()
                                                          }).ToList();

                var correctOrders = new List<Order>();
                var consignmentNumbers = new List<string>();
                var index = 0;
                var selectedOrder = new Order();
                foreach (var order in model)
                {
                    if (!string.IsNullOrEmpty(Validate(order, serviceList, timeSlots, productTypeList, offDays, model)))
                    {
                        continue;
                    }

                    // Recalculate to prevent hijacking
                    var price = OrderHelper.CalculatePrice(
                                                           new PriceRequestViewModel()
                                                           {
                                                               ProductTypeId = order.ProductTypeId,
                                                               ServiceId = order.ServiceId,
                                                               CustomerId = order.CustomerId,
                                                               SizeId = order.SizeId
                                                           });

                    if (db.Orders.Any(x => x.OrderNumber == order.OrderNumber && x.ConfirmedOn == null) && !string.IsNullOrEmpty(order.OrderNumber))
                    {
                        selectedOrder = db.Orders.FirstOrDefault(x => x.OrderNumber == order.OrderNumber);
                        selectedOrder.Status = (int)OrderStatuses.OrderSubmitted;
                        selectedOrder.IsDeleted = false;
                        selectedOrder.CreatedOn = DateTime.Now;
                        selectedOrder.CreatedBy = username;
                        selectedOrder.UpdatedOn = DateTime.Now;
                        selectedOrder.UpdatedBy = username;
                        selectedOrder.Price = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion + price.Surcharge;
                        selectedOrder.BinCode = OrderHelper.GenerateBinCode(order);
                        selectedOrder.FromAddress = order.FromAddress;
                        selectedOrder.ToAddress = order.ToAddress;
                        selectedOrder.ToZipCode = order.ToZipCode;
                        selectedOrder.ToMobilePhone = order.ToMobilePhone;
                        selectedOrder.DeliveryDate = order.DeliveryDate;
                        selectedOrder.DeliveryTimeSlotId = order.DeliveryTimeSlotId;
                        selectedOrder.GroupTrackingNumber = null;
                        db.SaveChanges();

                        consignmentNumbers.Add(selectedOrder.ConsignmentNumber);
                        correctOrders.Add(selectedOrder);
                    }
                    else
                    {
                        order.Status = (int)OrderStatuses.OrderSubmitted;
                        order.IsDeleted = false;
                        order.CreatedOn = DateTime.Now;
                        order.CreatedBy = username;
                        order.UpdatedOn = DateTime.Now;
                        order.UpdatedBy = username;
                        order.Price = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion + price.Surcharge;
                        order.BinCode = OrderHelper.GenerateBinCode(order);

                        db.Orders.Add(order);

                        db.SaveChanges();

                        order.ConsignmentNumber = OrderHelper.GenerateConsignmentNumber(order.Id);
                        if (string.IsNullOrWhiteSpace(order.OrderNumber))
                        {
                            order.OrderNumber = customer.CompanyPrefix + order.ConsignmentNumber;
                        }

                        // Build QR code
                        var bitmap = OrderHelper.GenerateQrCode(paymentTypes, services, productTypes, sizes, order);
                        order.QrCodePath = Url.Content(OrderHelper.SaveQrCode(order.ConsignmentNumber, bitmap));
                        // Build Bar Code
                        var barCode = OrderHelper.GenerateBarCode(order.ConsignmentNumber);
                        var barCodePath = OrderHelper.SaveBarCode(order.ConsignmentNumber, barCode);

                        consignmentNumbers.Add(order.ConsignmentNumber);
                        correctOrders.Add(order);
                    }

                    // Add Cod
                    var count = 0;
                    if (int.TryParse(Request["[" + index + "].CodOptions.Count"], out count))
                    {
                        for (var j = 0; j < count; j++)
                        {
                            decimal amount = 0;
                            var codOptionId = 0;
                            if (decimal.TryParse(Request["[" + index + "].CodOptions[" + j + "].Amount"], out amount) &&
                                int.TryParse(Request["[" + index + "].CodOptions[" + j + "].CodOptionId"], out codOptionId))
                            {
                                if (db.CodOptionOfOrders.Any(x => x.OrderId == selectedOrder.Id))
                                {
                                    var selectedCodOptionOfOrder = db.CodOptionOfOrders.FirstOrDefault(x => x.OrderId == selectedOrder.Id);

                                    selectedCodOptionOfOrder.Amount = amount;
                                    selectedCodOptionOfOrder.CodOptionId = codOptionId;
                                    selectedCodOptionOfOrder.CreatedBy = username;
                                    selectedCodOptionOfOrder.CreatedOn = DateTime.Now;
                                    selectedCodOptionOfOrder.CollectedAmount = 0;
                                    selectedCodOptionOfOrder.IsCollected = false;
                                    selectedCodOptionOfOrder.IsDeleted = false;
                                    selectedCodOptionOfOrder.UpdatedBy = username;
                                    selectedCodOptionOfOrder.UpdatedOn = DateTime.Now;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    var cod = new CodOptionOfOrder()
                                    {
                                        Amount = amount,
                                        CodOptionId = codOptionId,
                                        CollectedAmount = 0,
                                        CreatedBy = username,
                                        CreatedOn = DateTime.Now,
                                        IsCollected = false,
                                        IsDeleted = false,
                                        OrderId = order.Id,
                                        UpdatedBy = username,
                                        UpdatedOn = DateTime.Now
                                    };
                                    db.CodOptionOfOrders.Add(cod);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    if (db.OrderTrackings.Any(x => x.ConsignmentNumber == selectedOrder.ConsignmentNumber && x.ConsignmentNumber != null))
                    {
                        var selectedOrderTracking = db.OrderTrackings.FirstOrDefault(x => x.ConsignmentNumber == selectedOrder.ConsignmentNumber);

                        selectedOrderTracking.ConsignmentNumber = selectedOrder.ConsignmentNumber;
                        selectedOrderTracking.PickupTimeSlotId = selectedOrder.PickupTimeSlotId;
                        selectedOrderTracking.PickupDate = selectedOrder.PickupDate;
                        selectedOrderTracking.DeliveryTimeSlotId = selectedOrder.DeliveryTimeSlotId;
                        selectedOrderTracking.DeliveryDate = selectedOrder.DeliveryDate;
                        selectedOrderTracking.BinCode = selectedOrder.BinCode;
                        selectedOrderTracking.Status = selectedOrder.Status;
                        selectedOrderTracking.CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        selectedOrderTracking.CreatedOn = DateTime.Now;
                        db.SaveChanges();

                    }
                    else
                    {
                        var track = new OrderTracking
                        {
                            ConsignmentNumber = order.ConsignmentNumber,
                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            CreatedOn = DateTime.Now,
                            PickupTimeSlotId = order.PickupTimeSlotId,
                            PickupDate = order.PickupDate,
                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                            DeliveryDate = order.DeliveryDate,
                            BinCode = order.BinCode,
                            Status = order.Status
                        };
                        db.OrderTrackings.Add(track);
                        db.SaveChanges();
                    }

                    ++index;
                }

                decimal total = 0;

                // Generate group tracking number from:
                // Pickup address, Delivery address, Delivery postalcode, Delivery date, Delivery time slot, Delivery phone number
                var groupedOrders = correctOrders.GroupBy(
                                                          x => new
                                                          {
                                                              x.FromAddress,
                                                              x.ToAddress,
                                                              x.ToZipCode,
                                                              x.ToMobilePhone,
                                                              x.DeliveryDate,
                                                              x.DeliveryTimeSlotId
                                                          }).ToList();
                foreach (var group in groupedOrders)
                {
                    if (group.Count() < 2)
                    {
                        total += group.Count() > 0 ? group.FirstOrDefault().Price : 0;
                        continue;
                    }

                    var firstOrder = group.FirstOrDefault();
                    if (firstOrder != null)
                    {
                        var maxPrice = group.Max(x => x.Price);
                        if (customer != null && customer.UseGroupPrice)
                        {
                            total += maxPrice;
                        }
                        else
                        {
                            total += group.Sum(x => x.Price);
                        }
                        // Associate all orders in the same group with same group tracking number generated from this
                        var groupTrackingNumber = OrderHelper.GenerateGroupTrackingNumber(firstOrder.Id);
                        foreach (var order in group)
                        {
                            correctOrders.FirstOrDefault(x => x.Id == order.Id).GroupTrackingNumber = groupTrackingNumber;
                            if (customer != null && customer.UseGroupPrice)
                            {
                                correctOrders.FirstOrDefault(x => x.Id == order.Id).Price = 0;
                            }
                        }

                        if (customer != null && customer.UseGroupPrice)
                        {
                            correctOrders.FirstOrDefault(x => x.Id == firstOrder.Id).Price = maxPrice;
                        }
                    }
                }

                db.SaveChanges();

                if (consignmentNumbers.Count == 0)
                {
                    // All failed
                    ViewBag.ErrorMessage = "All records are failed to import. Please check data and try again.";
                    return View(model);
                }
                var mapCN = new ConsignmentNumber();
                string consignmentNumbersList = string.Join(",", consignmentNumbers);
                if (!db.ConsignmentNumbers.Any(x => x.ConsignmentNumbers == consignmentNumbersList))
                {  // Save Consignment Number to temp table
                    mapCN.ConsignmentNumbers = consignmentNumbersList;
                    mapCN.Id = Guid.NewGuid();
                    mapCN.CreatedOn = DateTime.Now;
                    db.ConsignmentNumbers.Add(mapCN);
                    db.SaveChanges();
                }
                else
                {
                    mapCN = db.ConsignmentNumbers.FirstOrDefault(x => x.ConsignmentNumbers == consignmentNumbersList);
                    mapCN.CreatedOn = DateTime.Now;
                    db.SaveChanges();
                }


                total = Math.Max(SettingProvider.MinimumOrderAmount, total);

                if (paymentTypes.FirstOrDefault(x => x.Value == customer.PaymentTypeId.ToString())?.Text?.ToLower().Contains("postpaid") == true)
                {
                    // For Post Paid
                    // Check balance
                    if (customer.Credit < total)
                    {
                        ViewBag.ErrorMessage = "Sorry you have used up your credit limit. Please pay your outstanding amount due before placing order. Thank you.";
                        return View(model);
                    }

                    foreach (var order in correctOrders)
                    {
                        order.Status = (int)OrderStatuses.OrderConfirmed;
                        order.ConfirmedOn = DateTime.Now;
                        var track = new OrderTracking
                        {
                            ConsignmentNumber = order.ConsignmentNumber,
                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            CreatedOn = DateTime.Now,
                            PickupTimeSlotId = order.PickupTimeSlotId,
                            PickupDate = order.PickupDate,
                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                            DeliveryDate = order.DeliveryDate,
                            BinCode = order.BinCode,
                            Status = order.Status
                        };
                        db.OrderTrackings.Add(track);
                    }

                    customer.Credit -= total;
                    // Save payment
                    var payment = new Payment()
                    {
                        Payer = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        AccountType = (int)PaymentTypes.PostPaid,
                        IsSuccessful = true,
                        PaymentAmount = total,
                        CreatedOn = DateTime.Now
                    };
                    foreach (var consignmentNumber in consignmentNumbers)
                    {
                        var orderInPayment = new OrdersOfPayment()
                        {
                            ConsignmentNumber = consignmentNumber,
                        };
                        payment.OrdersOfPayments.Add(orderInPayment);
                    }

                    db.Payments.Add(payment);

                    db.SaveChanges();

                    OrderHelper.SendOrderEmail(Url, correctOrders, mapCN.Id.ToString());
                    await OrderHelper.SendOrderSms(Url, correctOrders, mapCN.Id.ToString());

                    TempData["SuccessMessage"] = string.Format("Your Orders ({0}/{1} uploaded) has been processed successfully. Thank you for using Roadbull.", correctOrders.Count, model.Count);
                    return RedirectToAction(
                                            "Tracking",
                                            "Order",
                                            new
                                            {
                                                id = mapCN.Id.ToString()
                                            });
                }
                else
                {
                    // For Pre Paid
                    return RedirectToAction(
                                            "PaymentAccounts",
                                            "Order",
                                            new
                                            {
                                                id = mapCN.Id.ToString()
                                            });
                }
            }
            catch (Exception exc)
            {
                // Rollback
                LogHelper.Log(exc);
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to Customer Care for support.";
                return View(model);
            }
        }

        public ActionResult BulkTracking()
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false || !User.IsInRole(AccountHelper.VendorRole))
            {
                ViewBag.ErrorMessage = "Your account is not verified or activated to track multiple orders.";
                customer = null;
            }
            ViewBag.Customer = customer;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BulkTracking(FormCollection form)
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || !User.IsInRole(AccountHelper.VendorRole))
            {
                ViewBag.ErrorMessage = "Cannot retrieve your information. Please try to login before tracking.";
                return View();
            }

            ViewBag.Customer = customer;

            try
            {
                var list = new List<string>();
                var file = Request.Files["UploadFile"];
                if (file != null && file.ContentLength > 0)
                {
                    IExcelDataReader excelReader;

                    if (file.FileName.ToLower().EndsWith(".xls"))
                    {
                        // Load data from Excel 2003 file
                        excelReader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
                    }
                    else if (file.FileName.ToLower().EndsWith(".xlsx"))
                    {
                        // Load data from Excel 2007 file
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
                    }
                    else
                    {
                        excelReader = null;
                    }

                    if (excelReader != null)
                    {
                        var rowCount = 0;
                        try
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            var data = excelReader.AsDataSet();

                            if (data.Tables.Count == 0)
                            {
                                // There is no table in sheet
                                ViewBag.ErrorMessage =
                                    "Import file does not have correct format. There is no data sheet. Please use template format.";
                                excelReader.Close();
                            }
                            else
                            {
                                var ordersTable = data.Tables[0];

                                for (var i = 0; i < ordersTable.Rows.Count; i++)
                                {
                                    // Process each row
                                    var row = ordersTable.Rows[i];

                                    ++rowCount;
                                    list.Add(row[0].ToString());
                                }

                                excelReader.Close();
                            }
                        }
                        catch (Exception exc)
                        {
                            excelReader.Close();
                            LogHelper.Log(exc);
                            if (exc is InvalidCastException || exc is NullReferenceException)
                            {
                                ViewBag.ErrorMessage =
                                    $"Import file does not have correct format. Please use template format. Error at row '{rowCount}'.<br />More information: {exc.ToString()}";
                            }
                            else
                            {
                                ViewBag.ErrorMessage =
                                    "There is error in processing. Please contact to Customer Care for support with these information: <br />" +
                                    exc.Message + "<br />" + exc.StackTrace;
                            }
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Import file does not have correct format. Please use template format.";
                    }
                }
                else if (!string.IsNullOrEmpty(form["InputList"]))
                {
                    list.AddRange(form["InputList"].Split(new char[] { ',', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries));
                }
                else
                {
                    ViewBag.ErrorMessage = "There is no file uploaded or input Consignment Numbers.";
                }

                if (ViewBag.ErrorMessage != null)
                {
                    return View();
                }

                // Create tracking group
                var cn = string.Join(",", list);
                var group = db.ConsignmentNumbers.FirstOrDefault(x => x.ConsignmentNumbers.Equals(cn, StringComparison.OrdinalIgnoreCase));
                if (group == null)
                {
                    group = new ConsignmentNumber()
                    {
                        Id = Guid.NewGuid(),
                        ConsignmentNumbers = cn,
                        CreatedOn = DateTime.Now
                    };
                    db.ConsignmentNumbers.Add(group);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception exc)
                    {
                        ViewBag.ErrorMessage = "Issues happen when importing data. Please try again or contact to Customer Care for support.";
                        return View();
                    }
                }

                return RedirectToAction("Tracking", new { id = group.Id.ToString(), check = "true" });
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Issues happen when importing data. Please try again or contact to Customer Care for support.";
            }

            return View();
        }

        public ActionResult Marketplace()
        {
            var service = db.Services.FirstOrDefault(x => x.Id == SettingProvider.StandardParcelServiceId);
            if (service == null)
            {
                ViewBag.ErrorMessage = "Cannot get information of service. Please retry after a few minutes";
            }

            ViewBag.Service = service;

            var timeSlots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();

            ViewBag.TimeSlots = timeSlots;

            var pickupTimeSlots = new List<System.Web.Mvc.SelectListItem>();
            if (service != null && !string.IsNullOrEmpty(service.AvailablePickupTimeSlots))
            {
                pickupTimeSlots.AddRange(
                                         from item in service.AvailablePickupTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                         select timeSlots.FirstOrDefault(x => x.Id.ToString() == item)
                                         into timeSlot
                                         orderby timeSlot.FromTime
                                         where timeSlot != null
                                         select new System.Web.Mvc.SelectListItem()
                                         {
                                             Text = timeSlot.TimeSlotName + " (" + timeSlot.FromTime.ToString("HH:mm") + " - " +
                                                           timeSlot.ToTime.ToString("HH:mm") + ")",
                                             Value = timeSlot.Id.ToString()
                                         });
            }
            ViewBag.PickupTimeSlots = pickupTimeSlots;

            var username = this.User.Identity.Name;
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false)
            {
                ViewBag.ErrorMessage = "Your account is not verified or activated to book an order.";
                customer = null;
            }

            ViewBag.Customer = customer;

            ViewBag.FromName = customer?.CustomerName;
            var addressParts = (customer?.CustomerAddress ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var address = "";
            var unitNumber = "";
            var buildingName = "";
            if (addressParts.Length >= 3)
            {
                buildingName = addressParts[addressParts.Length - 1].Trim();
                unitNumber = addressParts[addressParts.Length - 2].Trim();
                address = string.Join(",", addressParts.Take(addressParts.Length - 2)).Trim();
            }
            else if (addressParts.Length >= 2)
            {
                unitNumber = addressParts[addressParts.Length - 1].Trim();
                address = string.Join(",", addressParts.Take(addressParts.Length - 1)).Trim();
            }
            else
            {
                address = (customer?.CustomerAddress ?? "").Trim();
            }
            ViewBag.FromAddress = address;
            ViewBag.FromUnitNumber = unitNumber;
            ViewBag.FromBuildingName = buildingName;
            ViewBag.FromZipCode = customer?.CustomerZipCode;
            ViewBag.FromMobilePhone = customer?.CustomerPhone;
            ViewBag.CustomerId = customer?.Id;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Marketplace(FormCollection form)
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null)
            {
                ViewBag.ErrorMessage = "Cannot retrieve your information. Please try to login before booking.";
                return View();
            }

            // Validation
            int productTypeId;
            if (string.IsNullOrEmpty(form["ProductTypeId"]) || !int.TryParse(form["ProductTypeId"], out productTypeId))
            {
                ViewBag.ErrorMessage = "Invalid Service. Please try again or contact to our Customer Care for support.";
                return View();
            }

            int serviceId;
            if (string.IsNullOrEmpty(form["ServiceId"]) || !int.TryParse(form["ServiceId"], out serviceId))
            {
                ViewBag.ErrorMessage = "Invalid Service. Please try again or contact to our Customer Care for support.";
                return View();
            }

            int serviceDay;
            if (string.IsNullOrEmpty(form["SelectService"]) || !int.TryParse(form["SelectService"], out serviceDay))
            {
                ViewBag.ErrorMessage = "Invalid Service. Please try again or contact to our Customer Care for support.";
                return View();
            }

            DateTime pickupDate;
            if (string.IsNullOrEmpty(form["PickupDate"]) ||
                !DateTime.TryParseExact(form["PickupDate"], "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out pickupDate))
            {
                ViewBag.ErrorMessage = "Invalid Pickup Date. Please try again or contact to our Customer Care for support.";
                return View();
            }

            int pickupTimeSlotId;
            if (string.IsNullOrEmpty(form["PickupTimeSlotId"]) || !int.TryParse(form["PickupTimeSlotId"], out pickupTimeSlotId))
            {
                ViewBag.ErrorMessage = "Invalid Pickup Time. Please try again or contact to our Customer Care for support.";
                return View();
            }

            var fromName = form["FromName"];
            if (string.IsNullOrEmpty(fromName))
            {
                ViewBag.ErrorMessage = "Invalid Sender's Name. Please try again or contact to our Customer Care for support.";
                return View();
            }

            var fromZipCode = form["FromZipCode"];
            if (string.IsNullOrEmpty(fromZipCode))
            {
                ViewBag.ErrorMessage = "Invalid Sender's Postal Code. Please try again or contact to our Customer Care for support.";
                return View();
            }

            var fromAddress = form["FromAddress"];
            if (string.IsNullOrEmpty(fromAddress))
            {
                ViewBag.ErrorMessage = "Invalid Sender's Address. Please try again or contact to our Customer Care for support.";
                return View();
            }

            var fromUnitNumber = form["FromUnitNumber"];
            var fromBuildingName = form["FromBuildingName"];
            var fromMobilePhone = form["FromMobilePhone"];
            if (string.IsNullOrEmpty(fromMobilePhone))
            {
                ViewBag.ErrorMessage = "Invalid Sender's Mobile Phone. Please try again or contact to our Customer Care for support.";
                return View();
            }

            string template = form["TemplatePicker"];
            if (string.IsNullOrEmpty(template))
            {
                ViewBag.ErrorMessage = "Invalid Template. Please try again or contact to our Customer Care for support.";
                return View();
            }

            try
            {
                var file = Request.Files["UploadFile"];
                if (file != null && file.ContentLength > 0)
                {
                    List<Order> list = null;
                    list = ReadMarketplaceOrders(
                                                 file,
                                                 productTypeId,
                                                 serviceId,
                                                 serviceDay,
                                                 pickupDate,
                                                 pickupTimeSlotId,
                                                 fromName,
                                                 fromZipCode,
                                                 fromAddress,
                                                 fromUnitNumber,
                                                 fromBuildingName,
                                                 fromMobilePhone,
                                                 template);

                    if (list == null)
                    {
                        return View();
                    }

                    TempData["OrderList"] = list;
                    return RedirectToAction("BulkConfirm", "Order");
                }
                else
                {
                    // There is no file
                    ViewBag.ErrorMessage = "There is no file selected. Please upload file to corresponding service type.";
                }
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Issues happen when importing files. Please try again or contact to Customer Care for support.";
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult ExtraRequest(string id)
        {
            var request = db.ExtraRequests.FirstOrDefault(x => x.Code.ToLower() == id.ToLower() && !x.IsDeleted);
            if (request == null)
            {
                ViewBag.Error = "There is no request with this identifier. Please check the link again.";
                return View();
            }

            if (request.IsVisited)
            {
                ViewBag.Error = "This request is already submitted.";
                return View();
            }

            if (request.RequestType == (int)ExtraRequestType.DeliveryRequest &&
                (request.Order.Status == (int)OrderStatuses.ItemDelivered ||
                 request.Order.Status == (int)OrderStatuses.DeliveryFailed ||
                 request.Order.Status == (int)OrderStatuses.SpecialOrderForDelivery))
            {
                ViewBag.Error = "This request is expired.";
                return View();
            }

            if (request.RequestType == (int)ExtraRequestType.ReDeliveryRequest &&
                (!request.Order.DeliveredOn.HasValue || DateTime.Now > request.Order.DeliveredOn.Value.AddDays(2)))
            {
                ViewBag.Error = "This request is expired.";
                return View();
            }

            if (request.RequestType == (int)ExtraRequestType.RePickupRequest &&
                (!request.Order.CollectedOn.HasValue || DateTime.Now > request.Order.CollectedOn.Value.AddDays(2)))
            {
                ViewBag.Error = "This request is expired.";
                return View();
            }

            ViewBag.FromDate = null;
            if (request.RequestType == (int)ExtraRequestType.ReDeliveryRequest)
            {
                ViewBag.FromDate = request.Order.DeliveredOn;
            }
            else if (request.RequestType == (int)ExtraRequestType.ReDeliveryRequest)
            {
                ViewBag.FromDate = request.Order.CollectedOn;
            }
            return View(request);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ExtraRequest(string id, FormCollection form)
        {
            var request = db.ExtraRequests.FirstOrDefault(x => x.Code.ToLower() == id.ToLower() && !x.IsDeleted);
            if (request == null)
            {
                ViewBag.Error = "There is no request with this identifier. Please check the link again.";
                return View();
            }

            if (request.IsVisited)
            {
                ViewBag.Error = "This request is already submitted.";
                return View();
            }

            if (request.RequestType == (int)ExtraRequestType.DeliveryRequest &&
                (request.Order.Status == (int)OrderStatuses.ItemDelivered ||
                 request.Order.Status == (int)OrderStatuses.DeliveryFailed ||
                 request.Order.Status == (int)OrderStatuses.SpecialOrderForDelivery))
            {
                ViewBag.Error = "This request is expired.";
                return View();
            }

            if (request.RequestType == (int)ExtraRequestType.ReDeliveryRequest &&
                (!request.Order.DeliveredOn.HasValue || DateTime.Now > request.Order.DeliveredOn.Value.AddDays(2)))
            {
                ViewBag.Error = "This request is expired.";
                return View();
            }

            if (request.RequestType == (int)ExtraRequestType.RePickupRequest &&
                (!request.Order.CollectedOn.HasValue || DateTime.Now > request.Order.CollectedOn.Value.AddDays(2)))
            {
                ViewBag.Error = "This request is expired.";
                return View();
            }

            request.IsVisited = true;
            request.RequestContent = (form["Request"] ?? "").Trim(',');
            request.RequestDate = null;
            DateTime date;
            if (!string.IsNullOrEmpty(form["RequestDate"]))
            {
                if (DateTime.TryParseExact(
                                           form["RequestDate"],
                                           "dd/MM/yyyy",
                                           CultureInfo.CurrentCulture,
                                           DateTimeStyles.None,
                                           out date))
                {
                    request.RequestDate = date;
                }
            }
            request.UpdatedOn = DateTime.Now;
            request.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            db.SaveChanges();

            ViewBag.FromDate = null;
            if (request.RequestType == (int)ExtraRequestType.ReDeliveryRequest)
            {
                ViewBag.FromDate = request.Order.DeliveredOn;
            }
            else if (request.RequestType == (int)ExtraRequestType.ReDeliveryRequest)
            {
                ViewBag.FromDate = request.Order.CollectedOn;
            }
            ViewBag.Success = "You already submitted your request successfully.";
            return View(request);
        }

        private List<Order> ReadOrders(HttpPostedFileBase file, int serviceId)
        {
            IExcelDataReader excelReader;

            if (file.FileName.ToLower().EndsWith(".xls"))
            {
                // Load data from Excel 2003 file
                excelReader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
            }
            else if (file.FileName.ToLower().EndsWith(".xlsx"))
            {
                // Load data from Excel 2007 file
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
            }
            else
            {
                excelReader = null;
            }

            if (excelReader != null)
            {
                var rowCount = 0;
                try
                {
                    excelReader.IsFirstRowAsColumnNames = true;
                    var data = excelReader.AsDataSet();

                    if (data.Tables.Count == 0)
                    {
                        // There is no table in sheet
                        ViewBag.ErrorMessage = "Import file does not have correct format. There is no data sheet. Please use template format.";
                        excelReader.Close();
                        return null;
                    }
                    else
                    {
                        var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
                        if (customer == null)
                        {
                            ViewBag.ErrorMessage = "Cannot retrieve your information. Please try to login before booking.";
                            return null;
                        }

                        var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == serviceId);
                        if (service == null)
                        {
                            ViewBag.ErrorMessage = "Cannot file corresponding service. Please contact to Customer Care for support.";
                            return null;
                        }

                        var size = db.Sizes.FirstOrDefault(x => !x.IsDeleted && x.ProductTypeId == service.ProductTypeId);
                        if (size == null)
                        {
                            ViewBag.ErrorMessage = "Cannot file corresponding service. Please contact to Customer Care for support.";
                            return null;
                        }

                        var timeSlots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();

                        var result = new List<Order>();
                        var ordersTable = data.Tables[0];

                        for (var i = 0; i < ordersTable.Rows.Count; i++)
                        {
                            // Process each row
                            var row = ordersTable.Rows[i];
                            if (row[0] == null || string.IsNullOrWhiteSpace(row[0].ToString()))
                            {
                                continue;
                            }

                            if (serviceId != SettingProvider.StandardDocumentServiceId)
                            {
                                var sizeText = row[9].ToString().ToLower();
                                size = db.Sizes.FirstOrDefault(x => !x.IsDeleted && x.ProductTypeId == service.ProductTypeId && x.SizeName.ToLower().StartsWith(sizeText));
                                if (size == null)
                                {
                                    ViewBag.ErrorMessage = "Cannot file corresponding service. Please contact to Customer Care for support.";
                                    return null;
                                }
                            }

                            var price = OrderHelper.CalculatePrice(
                                                                   new PriceRequestViewModel()
                                                                   {
                                                                       ProductTypeId = service.ProductTypeId,
                                                                       ServiceId = service.Id,
                                                                       CustomerId = customer.Id,
                                                                       SizeId = size.Id
                                                                   });

                            var timeColumn = 9;
                            if (serviceId != SettingProvider.StandardDocumentServiceId)
                            {
                                timeColumn = 10;
                            }
                            ++rowCount;
                            var order = new Order()
                            {
                                FromName = row[1].ToString(),
                                FromZipCode = row[2].ToString(),
                                FromAddress = row[3].ToString(),
                                FromMobilePhone = row[4].ToString(),
                                ToName = row[5].ToString(),
                                ToZipCode = row[6].ToString(),
                                ToAddress = row[7].ToString(),
                                ToMobilePhone = row[8].ToString(),
                                ServiceId = service.Id,
                                ProductTypeId = service.ProductTypeId,
                                CustomerId = customer.Id,
                                PaymentTypeId = customer.PaymentTypeId,
                                SizeId = size.Id,
                                Price = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion + price.Surcharge, //price.Total,
                                PickupDate = row[timeColumn] as DateTime?,
                                DeliveryDate = row.ItemArray.Count() == timeColumn + 2 ? null : row[timeColumn + 2] as DateTime?,
                                FullfilmentNumber = row[timeColumn + 4].ToString(),
                                OrderNumber = string.IsNullOrWhiteSpace(row[timeColumn + 5].ToString()) ? "" : customer.CompanyPrefix + row[timeColumn + 5].ToString(),
                                Remark = row[timeColumn + 6].ToString(),
                                Location = row[timeColumn + 7].ToString(),
                                SKU = row[timeColumn + 8].ToString(),
                                Quantity = row[timeColumn + 9].ToString(),
                                IsExchange = row.ItemArray.Length > timeColumn + 10 && row[timeColumn + 10] != null && row[timeColumn + 10].ToString().ToLower() == "yes"
                            };

                            foreach (
                                var timeSlot in
                                timeSlots.Where(
                                                timeSlot =>
                                                    row[timeColumn + 1].ToString()
                                                                       .Equals($"{timeSlot.TimeSlotName} ({timeSlot.FromTime.ToString("HH:mm")} - {timeSlot.ToTime.ToString("HH:mm")})", StringComparison.OrdinalIgnoreCase))
                            )
                            {
                                order.PickupTimeSlotId = timeSlot.Id;
                            }

                            if (row.ItemArray.Count() > timeColumn + 3)
                            {
                                foreach (
                                    var timeSlot in
                                    timeSlots.Where(
                                                    timeSlot =>
                                                        row[timeColumn + 3].ToString()
                                                                           .Equals(
                                                                                   $"{timeSlot.TimeSlotName} ({timeSlot.FromTime.ToString("HH:mm")} - {timeSlot.ToTime.ToString("HH:mm")})",
                                                                                   StringComparison.OrdinalIgnoreCase)))
                                {
                                    order.DeliveryTimeSlotId = timeSlot.Id;
                                }
                            }

                            if (row.ItemArray.Length > timeColumn + 11 && row[timeColumn + 11] != null && !string.IsNullOrEmpty(row[timeColumn + 11].ToString().Trim()) && (double)row[timeColumn + 11] > 0)
                            {
                                // COD Cash
                                var codOption = db.CodOptions.FirstOrDefault(x => !x.IsDeleted && x.Type == (int)CodOptionTypes.Cash);
                                if (codOption != null)
                                {
                                    var cod = new CodOptionOfOrder()
                                    {
                                        Amount = (decimal)(double)row[timeColumn + 11],
                                        CodOptionId = codOption.Id,
                                    };
                                    order.CodOptionOfOrders.Add(cod);
                                }
                            }

                            if (row.ItemArray.Length > timeColumn + 12 && row[timeColumn + 12] != null && !string.IsNullOrEmpty(row[timeColumn + 12].ToString().Trim()) && (double)row[timeColumn + 12] > 0)
                            {
                                // COD Cheque
                                var codOption = db.CodOptions.FirstOrDefault(x => !x.IsDeleted && x.Type == (int)CodOptionTypes.Cheque);
                                if (codOption != null)
                                {
                                    var cod = new CodOptionOfOrder()
                                    {
                                        Amount = (decimal)(double)row[timeColumn + 12],
                                        CodOptionId = codOption.Id,
                                    };
                                    order.CodOptionOfOrders.Add(cod);
                                }
                            }

                            result.Add(order);
                        }

                        excelReader.Close();
                        return result;
                    }
                }
                catch (Exception exc)
                {
                    excelReader.Close();
                    LogHelper.Log(exc);
                    if (exc is InvalidCastException || exc is NullReferenceException)
                    {
                        ViewBag.ErrorMessage = $"Import file does not have correct format. Please use template format. Error at row '{rowCount}'.<br />More information: {exc.ToString()}";
                        return null;
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "There is error in processing. Please contact to Customer Care for support with these information: <br />" +
                                               exc.Message + "<br />" + exc.StackTrace;
                        return null;
                    }
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Import file does not have correct format. Please use template format.";
                return null;
            }
        }

        /// <summary>
        /// This function will render the Reports view
        /// </summary>
        /// <returns></returns>
        //[ValidateAntiForgeryToken]
        public ActionResult Reports()
        {
            var username = this.User.Identity.Name;
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false)
            {
                ViewBag.ErrorMessage = "Your account is not verified or activated to book an order.";
            }

            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult ExportToExcel(DateTime? StartDate, DateTime? EndDate)
        {
            var username = this.User.Identity.Name;
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false)
            {
                return RedirectToAction("Reports");
            }

            var startDate = DateTime.MinValue;
            var endDate = DateTime.MaxValue;

            if (StartDate.HasValue && StartDate.Value != DateTime.MinValue)
            {
                startDate = StartDate.Value;
            }

            if (EndDate.HasValue && EndDate.Value != DateTime.MinValue)
            {
                endDate = EndDate.Value.AddDays(1);
            }

            var dataReport = db.Orders.Where(x => x.CustomerId == customer.Id && x.PickupDate >= startDate && x.PickupDate < endDate).ToList();

            var folderPath = Server.MapPath("~/Temp/");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            var fileName = "DeliverySummary_" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".xlsx";
            var filePath = folderPath + fileName;
            var outputFile = new FileInfo(filePath);

            var package = new ExcelPackage(outputFile);
            // Add the sheet
            var ws = package.Workbook.Worksheets.Add("DeliverySummary");
            var timeslots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();
            ws.View.ShowGridLines = true;

            // Headers
            ws.Cells["A1"].Value = "Status";
            ws.Cells["B1"].Value = "Last Updated";
            ws.Cells["C1"].Value = "Consignment Number";
            ws.Cells["D1"].Value = "Order Number";
            ws.Cells["E1"].Value = "COD";
            ws.Cells["F1"].Value = "COD Collected";
            ws.Cells["G1"].Value = "Customer Remark";
            ws.Cells["H1"].Value = "Exchange?";
            ws.Cells["I1"].Value = "Pickup Date";
            ws.Cells["J1"].Value = "To Name";
            ws.Cells["K1"].Value = "To Zip Code";
            ws.Cells["L1"].Value = "To Address";
            ws.Cells["M1"].Value = "To Mobile Phone";
            ws.Cells["N1"].Value = "Delivery Date";
            ws.Cells["O1"].Value = "Delivery Time Slot";
            ws.Cells["P1"].Value = "Size";
            ws.Cells["Q1"].Value = "Price";
            ws.Cells["R1"].Value = "Created On";
            ws.Cells["S1"].Value = "EPOD (Electronic Proof of Delivery)";

            ws.Cells["A1:S1"].Style.Font.Bold = true;

            //Fill Data
            for (int i = 0; i < dataReport.Count; i++)
            {
                var item = dataReport[i];

                if (item.Status == (int)OrderStatuses.SpecialOrderForDelivery)
                {
                    ws.Cells[i + 2, 1].Value = "Pending Dispatch for Delivery";
                }
                else if (item.Status == (int)OrderStatuses.SpecialOrderForPickup)
                {
                    ws.Cells[i + 2, 1].Value = "Pending Dispatch for Pickup";
                }
                else
                {
                    ws.Cells[i + 2, 1].Value = EnumHelpers.GetDisplayValue((OrderStatuses)item.Status);
                }

                ws.Cells[i + 2, 2].Value = item.UpdatedOn.ToString("dd/MM/yyyy");
                ws.Cells[i + 2, 3].Value = item.ConsignmentNumber;
                ws.Cells[i + 2, 4].Value = item.OrderNumber;
                ws.Cells[i + 2, 5].Value = ""; //item.FullfilmentNumber;
                ws.Cells[i + 2, 6].Value = ""; //"RB_" + item.OrderNumber;
                ws.Cells[i + 2, 7].Value = item.Remark;
                ws.Cells[i + 2, 8].Value = item.IsExchange ? "Yes" : "No";
                ws.Cells[i + 2, 9].Value = item.PickupDate?.ToString("dd/MM/yyyy");
                ws.Cells[i + 2, 10].Value = item.ToName;
                ws.Cells[i + 2, 11].Value = item.ToZipCode;
                ws.Cells[i + 2, 12].Value = item.ToAddress;
                ws.Cells[i + 2, 13].Value = item.ToMobilePhone;
                ws.Cells[i + 2, 14].Value = item.DeliveryDate?.ToString("dd/MM/yyyy");
                ws.Cells[i + 2, 15].Value = timeslots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.TimeSlotName).FirstOrDefault();
                ws.Cells[i + 2, 16].Value = item.Size.SizeName;
                ws.Cells[i + 2, 17].Value = item.Price.ToString("C");
                ws.Cells[i + 2, 18].Value = item.CreatedOn.ToString("dd/MM/yyyy");

                if (item.Status == (int)OrderStatuses.ItemCollected || item.Status == (int)OrderStatuses.PickupFailed)
                {
                    ws.Cells[i + 2, 19].Value = string.IsNullOrEmpty(item.FromSignature) || string.IsNullOrEmpty(item.FromSignature.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault())
                                                    ? ""
                                                    : string.Format(
                                                                    Url.Content(item.FromSignature.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()));
                }
                else
                {
                    ws.Cells[i + 2, 19].Value = string.IsNullOrEmpty(item.ToSignature) || string.IsNullOrEmpty(item.ToSignature.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault())
                                                  ? ""
                                                  : string.Format(
                                                                  //"{0}://{1}{2}{3}",
                                                                  //Request.Url.Scheme,
                                                                  //Request.Url.Host,
                                                                  //Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                                                                  Url.Content(item.ToSignature.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()));

                }
            }

            ws.Column(1).AutoFit();
            ws.Column(2).AutoFit();
            ws.Column(3).AutoFit();
            ws.Column(4).AutoFit();
            ws.Column(5).AutoFit();
            ws.Column(6).AutoFit();
            ws.Column(7).AutoFit();
            ws.Column(8).AutoFit();
            ws.Column(9).AutoFit();
            ws.Column(10).AutoFit();
            ws.Column(11).AutoFit();
            ws.Column(12).AutoFit();
            ws.Column(13).AutoFit();
            ws.Column(14).AutoFit();
            ws.Column(15).AutoFit();
            ws.Column(16).AutoFit();
            ws.Column(17).AutoFit();
            ws.Column(18).AutoFit();
            ws.Column(19).AutoFit();
            package.Save();

            return File(filePath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        private List<Order> ReadMarketplaceOrders(
            HttpPostedFileBase file,
            int productTypeId,
            int serviceId,
            int serviceDay,
            DateTime pickupDate,
            int pickupTimeSlotId,
            string fromName,
            string fromZipCode,
            string fromAddress,
            string fromUnitNumber,
            string fromBuildingName,
            string fromMobilePhone,
            string template)
        {
            var username = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null)
            {
                ViewBag.ErrorMessage = "Cannot retrieve your information. Please try to login before booking.";
                return null;
            }

            var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == serviceId);
            if (service == null)
            {
                ViewBag.ErrorMessage = "Cannot file corresponding service. Please contact to Customer Care for support.";
                return null;
            }

            var size = db.Sizes.FirstOrDefault(x => !x.IsDeleted && x.ProductTypeId == productTypeId);
            if (size == null)
            {
                ViewBag.ErrorMessage = "Cannot file corresponding service. Please contact to Customer Care for support.";
                return null;
            }

            if (string.IsNullOrEmpty(template))
            {
                ViewBag.ErrorMessage = "Cannot find template. Please contact to Customer Care for support.";
                return null;
            }

            var offDayStrs = (SettingProvider.OutOfServiceDays ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var offDays = new List<DateTime>();
            foreach (var day in offDayStrs)
            {
                offDays.Add(DateTime.ParseExact(day, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None));
            }

            var deliveryDate = pickupDate.AddDays(Math.Min(2, serviceDay));
            while (offDays.Any(x => x.Day == deliveryDate.Day && x.Month == deliveryDate.Month && x.Year == deliveryDate.Year)
                   || deliveryDate.DayOfWeek == DayOfWeek.Sunday)
            {
                deliveryDate = deliveryDate.AddDays(1);
            }

            var price = OrderHelper.CalculatePrice(
                                                   new PriceRequestViewModel()
                                                   {
                                                       ProductTypeId = productTypeId,
                                                       ServiceId = serviceId,
                                                       CustomerId = customer.Id,
                                                       SizeId = size.Id
                                                   });

            var deliveryTimeSlotIdStr = string.IsNullOrEmpty(service.AvailableDeliveryTimeSlots) ? "" : service.AvailableDeliveryTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
            int deliveryTimeSlotId;
            if (!int.TryParse(deliveryTimeSlotIdStr, out deliveryTimeSlotId))
            {
                deliveryTimeSlotId = pickupTimeSlotId;
            }

            var textReader = new StreamReader(file.InputStream);
            var csv = new CsvReader(textReader);

            var result = new List<Order>();

            if (template.ToLower() == "icommerce")
            {
                DataTable records = null;
                IExcelDataReader excelReader;

                if (file.FileName.ToLower().EndsWith(".xls"))
                {
                    // Load data from Excel 2003 file
                    excelReader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
                }
                else if (file.FileName.ToLower().EndsWith(".xlsx"))
                {
                    // Load data from Excel 2007 file
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
                }
                else
                {
                    excelReader = null;
                }

                if (excelReader != null)
                {
                    excelReader.IsFirstRowAsColumnNames = true;
                    var data = excelReader.AsDataSet();

                    if (data.Tables.Count > 0)
                    {
                        records = data.Tables[0];
                    }

                    excelReader.Dispose();
                }

                Order order = null;
                for (int i = 0; i < records.Rows.Count; i++)
                {
                    if (i >= 2)
                    {
                        if (records.Rows[i][25].ToString().Length > 0)
                        {
                            if (records.Rows[i][14].ToString().Length > 0)
                            {
                                order = new Order();
                                order.FromName = fromName;
                                order.FromZipCode = fromZipCode;
                                order.FromAddress = fromAddress + (string.IsNullOrEmpty(fromUnitNumber) ? "" : ", " + fromUnitNumber) +
                                                    (string.IsNullOrEmpty(fromBuildingName) ? "" : ", " + fromBuildingName);
                                order.FromMobilePhone = fromMobilePhone;
                                order.ToName = records.Rows[i][17].ToString();
                                order.ToZipCode = records.Rows[i][21].ToString();
                                order.ToAddress = records.Rows[i][20].ToString();
                                order.ToMobilePhone = records.Rows[i][18].ToString();
                                order.ServiceId = serviceId;
                                order.ProductTypeId = productTypeId;
                                order.CustomerId = customer.Id;
                                order.PaymentTypeId = customer.PaymentTypeId;
                                order.SizeId = size.Id;
                                order.Price = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion + price.Surcharge; //price.Total,
                                order.PickupDate = pickupDate;
                                order.PickupTimeSlotId = pickupTimeSlotId;
                                order.DeliveryDate = deliveryDate;
                                order.DeliveryTimeSlotId = deliveryTimeSlotId;
                                order.OrderNumber = string.IsNullOrWhiteSpace(records.Rows[i][16].ToString()) ? "" : customer.CompanyPrefix + records.Rows[i][16].ToString();
                                order.Quantity = records.Rows[i][26].ToString();
                                order.Remark = records.Rows[i][25].ToString();
                                result.Add(order);
                            }
                            else
                            {
                                if (order != null)
                                {
                                    order.Quantity = order.Quantity + "; " + records.Rows[i][26].ToString();
                                    order.Remark = order.Remark + "; " + records.Rows[i][25].ToString();
                                }
                            }
                        }
                    }
                }
            }
            else if (template.ToLower() == "groupon")
            {
                csv.Configuration.RegisterClassMap<GrouponModelMap>();
                var records = csv.GetRecords<GrouponModel>().ToList();
                foreach (var record in records)
                {
                    var order = new Order()
                    {
                        FromName = fromName,
                        FromZipCode = fromZipCode,
                        FromAddress = fromAddress + (string.IsNullOrEmpty(fromUnitNumber) ? "" : ", " + fromUnitNumber) +
                                                  (string.IsNullOrEmpty(fromBuildingName) ? "" : ", " + fromBuildingName),
                        FromMobilePhone = fromMobilePhone,
                        ToName = record.ShipmentAddressName,
                        ToZipCode = record.ShipmentAddressPostalCode,
                        ToAddress = record.ShipmentAddressStreet,
                        ToMobilePhone = record.CustomerPhone,
                        ServiceId = serviceId,
                        ProductTypeId = productTypeId,
                        CustomerId = customer.Id,
                        PaymentTypeId = customer.PaymentTypeId,
                        SizeId = size.Id,
                        Price = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion + price.Surcharge, //price.Total,
                        PickupDate = pickupDate,
                        PickupTimeSlotId = pickupTimeSlotId,
                        DeliveryDate = deliveryDate,
                        DeliveryTimeSlotId = deliveryTimeSlotId,
                        FullfilmentNumber = record.FulfillmentLineItemId,
                        //      OrderNumber = string.IsNullOrWhiteSpace(record.GrouponNumber) ? "" : customer.CompanyPrefix + record.GrouponNumber,

                        SKU = record.GrouponSku,
                        Quantity = record.QuantityRequested,
                        Remark = record.ItemName
                    };
                    result.Add(order);
                }
            }
            else if (template.ToLower() == "qoo10")
            {
                csv.Configuration.RegisterClassMap<Qoo10ModelMap>();
                var records = csv.GetRecords<Qoo10Model>().ToList();
                foreach (var record in records)
                {
                    var order = new Order()
                    {
                        FromName = fromName,
                        FromZipCode = fromZipCode,
                        FromAddress = fromAddress + (string.IsNullOrEmpty(fromUnitNumber) ? "" : ", " + fromUnitNumber) +
                                                  (string.IsNullOrEmpty(fromBuildingName) ? "" : ", " + fromBuildingName),
                        FromMobilePhone = fromMobilePhone,
                        ToName = record.Recipient,
                        ToZipCode = record.PostalCode,
                        ToAddress = record.Address,
                        ToMobilePhone = record.RecipientMobilePhone,
                        ServiceId = serviceId,
                        ProductTypeId = productTypeId,
                        CustomerId = customer.Id,
                        PaymentTypeId = customer.PaymentTypeId,
                        SizeId = size.Id,
                        Price = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion +
                                            price.Surcharge, //price.Total,
                        PickupDate = pickupDate,
                        PickupTimeSlotId = pickupTimeSlotId,
                        DeliveryDate = deliveryDate,
                        DeliveryTimeSlotId = deliveryTimeSlotId,
                        OrderNumber = string.IsNullOrWhiteSpace(record.OrderNumber) ? "" : customer.CompanyPrefix + record.OrderNumber,
                        Quantity = record.Quantity,
                        Remark = record.Item
                    };
                    result.Add(order);
                }
            }
            else if (template.ToLower() == "mls")
            {
                csv.Configuration.RegisterClassMap<MaxcellentsModelMap>();
                var records = csv.GetRecords<MaxcellentsModel>().ToList();
                foreach (var record in records)
                {
                    var order = new Order()
                    {
                        FromName = fromName,
                        FromZipCode = fromZipCode,
                        FromAddress = fromAddress + (string.IsNullOrEmpty(fromUnitNumber) ? "" : ", " + fromUnitNumber) +
                                                  (string.IsNullOrEmpty(fromBuildingName) ? "" : ", " + fromBuildingName),
                        FromMobilePhone = fromMobilePhone,
                        ToName = record.Name,
                        ToZipCode = record.PostalCode,
                        ToAddress = record.Address,
                        ToMobilePhone = record.Contact,
                        ServiceId = serviceId,
                        ProductTypeId = productTypeId,
                        CustomerId = customer.Id,
                        PaymentTypeId = customer.PaymentTypeId,
                        SizeId = size.Id,
                        Price = price.BasePrice + price.AddOnPrice + price.DefaultPromotion - price.Promotion +
                                            price.Surcharge, //price.Total,
                        PickupDate = pickupDate,
                        PickupTimeSlotId = pickupTimeSlotId,
                        DeliveryDate = deliveryDate,
                        DeliveryTimeSlotId = deliveryTimeSlotId,
                        OrderNumber = string.IsNullOrWhiteSpace(record.Tracking) ? "" : customer.CompanyPrefix + record.Tracking,
                        Remark = record.Remarks,
                        Location = record.Location,
                        SKU = record.SKU,
                        Quantity = record.Quantity,
                        IsExchange = record.ExchangeService?.ToLower() == "yes"
                    };
                    result.Add(order);
                }
            }
            else
            {
                return null;
            }

            return result;
        }

        public static string Validate(Order order, List<Service> services, List<TimeSlot> timeSlots, List<ProductType> productTypes, List<DateTime> offDays, List<Order> orders)
        {
            try
            {
                var db = new CourierDBEntities();
                var mobileRegex = new Regex("[3689][0-9]{7}");

                if (string.IsNullOrEmpty(order.FromName))
                {
                    return "From Name must be provided";
                }

                if (string.IsNullOrEmpty(order.FromZipCode))
                {
                    return "From Postal Code must be provided";
                }

                if (!db.ZipCodes.Any(x => x.PostalCode == order.FromZipCode))
                {
                    return "From Postal Code does not exist in our database";
                }

                if (string.IsNullOrEmpty(order.FromAddress))
                {
                    return "From Address must be provided";
                }

                if (string.IsNullOrEmpty(order.FromMobilePhone))
                {
                    return "From Mobile Phone must be provided";
                }

                if (!mobileRegex.IsMatch(order.FromMobilePhone))
                {
                    return "From Mobile Phone is not a valid mobile number";
                }

                if (string.IsNullOrEmpty(order.ToName))
                {
                    return "To Name must be provided";
                }

                if (string.IsNullOrEmpty(order.ToZipCode))
                {
                    return "To Postal Code must be provided";
                }

                if (!db.ZipCodes.Any(x => x.PostalCode == order.ToZipCode))
                {
                    return "To Postal Code does not exist in our database";
                }

                if (string.IsNullOrEmpty(order.ToAddress))
                {
                    return "To Address must be provided";
                }

                if (string.IsNullOrEmpty(order.ToMobilePhone))
                {
                    return "To Mobile Phone must be provided";
                }

                if (!mobileRegex.IsMatch(order.ToMobilePhone))
                {
                    return "To Mobile Phone is not a valid mobile number";
                }

                var service = services.FirstOrDefault(x => x.Id == order.ServiceId);
                if (service == null)
                {
                    return "Cannot find corresponding Service";
                }

                if (service.IsPickupDateAllow && !string.IsNullOrEmpty(service.AvailablePickupDateRange))
                {
                    // Check Pickup Date
                    if (!order.PickupDate.HasValue)
                    {
                        return "Pickup Date must be provided";
                    }

                    var range = service.AvailablePickupDateRange.Split(
                                                                       new char[] { ',' },
                                                                       StringSplitOptions.RemoveEmptyEntries);
                    if (range.Length == 2)
                    {
                        var minValue = 0;
                        var maxValue = 0;
                        int.TryParse(range[0], out minValue);
                        int.TryParse(range[1], out maxValue);
                        var minDate = DateTime.Now.Date.AddDays(minValue);
                        var weekends = CountWeekendDays(DateTime.Now.Date, minDate, offDays);
                        minDate = minDate.AddDays(weekends);
                        var maxDate = DateTime.Now.Date.AddDays(maxValue);
                        weekends = CountWeekendDays(DateTime.Now.Date, maxDate, offDays);
                        maxDate = maxDate.AddDays(weekends);
                        if (order.PickupDate.Value < minDate || order.PickupDate.Value > maxDate)
                        {
                            return
                                $"Pickup Date is out of range. It should be in '{minDate.ToString("dd/MM/yyyy")}' to '{maxDate.ToString("dd/MM/yyyy")}'";
                        }
                        if (order.PickupDate.Value.DayOfWeek == DayOfWeek.Sunday)
                        {
                            return "Pickup Date is an out-of-service day";
                        }

                        foreach (var day in offDays)
                        {
                            if (day.Day == order.PickupDate.Value.Day && day.Month == order.PickupDate.Value.Month && day.Year == order.PickupDate.Value.Year)
                            {
                                return "Pickup Date is an out-of-service day";
                            }
                        }
                    }
                }

                if (service.IsPickupTimeAllow && !string.IsNullOrEmpty(service.AvailablePickupTimeSlots))
                {
                    // Check Pickup Time
                    if (!order.PickupTimeSlotId.HasValue)
                    {
                        return "Pickup Time must be provided";
                    }

                    var slots = service.AvailablePickupTimeSlots.Split(
                                                                       new char[] { ',' },
                                                                       StringSplitOptions.RemoveEmptyEntries);
                    if (!slots.Contains(order.PickupTimeSlotId.Value.ToString()))
                    {
                        return "Pickup Time is out of range";
                    }

                    var timeSlot = timeSlots.FirstOrDefault(x => x.Id == order.PickupTimeSlotId);
                    if (timeSlot == null)
                    {
                        return "Cannot find corresponding Time Slot";
                    }

                    if (order.PickupDate.HasValue && order.PickupDate.Value == DateTime.Now.Date)
                    {
                        // Today
                        if (DateTime.Now.TimeOfDay.TotalMinutes + SettingProvider.CloseWindowTime > timeSlot.FromTime.TimeOfDay.TotalMinutes && timeSlot.IsAllowPreOrder)
                        {
                            return "Too late to choose this time for pickup";
                        }
                        else if (DateTime.Now.TimeOfDay.TotalMinutes > timeSlot.ToTime.TimeOfDay.TotalMinutes)
                        {
                            return "Too late to choose this time for pickup";
                        }
                    }
                }

                if (service.IsDeliveryDateAllow && !string.IsNullOrEmpty(service.AvailableDeliveryDateRange))
                {
                    // Check Delivery Date
                    if (!order.DeliveryDate.HasValue)
                    {
                        return "Delivery Date must be provided";
                    }

                    var range = service.AvailableDeliveryDateRange.Split(
                                                                         new char[] { ',' },
                                                                         StringSplitOptions.RemoveEmptyEntries);
                    if (range.Length == 2)
                    {
                        var pickupDate = order.PickupDate ?? DateTime.Now.Date;
                        var minValue = 0;
                        var maxValue = 0;
                        int.TryParse(range[0], out minValue);
                        int.TryParse(range[1], out maxValue);
                        var minDate = pickupDate.AddDays(minValue);
                        var weekends = CountWeekendDays(pickupDate, minDate, offDays);
                        minDate = minDate.AddDays(weekends);
                        var maxDate = pickupDate.AddDays(maxValue);
                        weekends = CountWeekendDays(pickupDate, maxDate, offDays);
                        maxDate = maxDate.AddDays(weekends);
                        if (order.DeliveryDate.Value < minDate || order.DeliveryDate.Value > maxDate)
                        {
                            return
                                $"Delivery Date is out of range. It should be in '{minDate.ToString("dd/MM/yyyy")}' to '{maxDate.ToString("dd/MM/yyyy")}'";
                        }
                        if (order.DeliveryDate.Value.DayOfWeek == DayOfWeek.Sunday)
                        {
                            return "Delivery Date is an out-of-service day";
                        }

                        foreach (var day in offDays)
                        {
                            if (day.Day == order.DeliveryDate.Value.Day && day.Month == order.DeliveryDate.Value.Month && day.Year == order.DeliveryDate.Value.Year)
                            {
                                return "Delivery Date is an out-of-service day";
                            }
                        }
                    }
                }

                if (service.IsDeliveryTimeAllow && !string.IsNullOrEmpty(service.AvailableDeliveryTimeSlots))
                {
                    // Check Delivery Time
                    if (!order.DeliveryTimeSlotId.HasValue)
                    {
                        return "Delivery Time must be provided";
                    }

                    var slots = service.AvailableDeliveryTimeSlots.Split(
                                                                         new char[] { ',' },
                                                                         StringSplitOptions.RemoveEmptyEntries);
                    if (!slots.Contains(order.DeliveryTimeSlotId.Value.ToString()))
                    {
                        return "Delivery Time is out of range";
                    }

                    var timeSlot = timeSlots.FirstOrDefault(x => x.Id == order.DeliveryTimeSlotId);
                    if (timeSlot == null)
                    {
                        return "Cannot find corresponding Time Slot";
                    }

                    // Check with Pickup Time
                    if (order.PickupDate.HasValue && order.DeliveryDate.HasValue &&
                        order.PickupDate.Value == order.DeliveryDate.Value)
                    {
                        var pickTimeSlot = timeSlots.FirstOrDefault(x => x.Id == order.PickupTimeSlotId);
                        if (pickTimeSlot != null)
                        {
                            var productType = productTypes.FirstOrDefault(x => x.Id == order.ProductTypeId);
                            if (productType == null)
                            {
                                return "Cannot find corresponding Product Type";
                            }

                            if (productType.ProductTypeName.ToLower().Contains("document"))
                            {
                                if (timeSlot.FromTime.TimeOfDay.TotalMinutes < pickTimeSlot.ToTime.TimeOfDay.TotalMinutes)
                                {
                                    return "Delivery time must be after Pickup time at least 1 slot";
                                }
                            }
                            else
                            {
                                var nextTimeSlotIdx = timeSlots.IndexOf(pickTimeSlot) + 1;
                                if (nextTimeSlotIdx < 0 || nextTimeSlotIdx >= timeSlots.Count)
                                {
                                    return "Delivery time must be after Pickup time at least 2 slots";
                                }

                                var nextTimeSlot = timeSlots[nextTimeSlotIdx];

                                if (timeSlot.FromTime.TimeOfDay.TotalMinutes <
                                    nextTimeSlot.ToTime.TimeOfDay.TotalMinutes)
                                {
                                    return "Delivery time must be after Pickup time at least 2 slots";
                                }
                            }
                        }
                    }
                }

                // Validate fulfillment number
                if (!string.IsNullOrWhiteSpace(order.OrderNumber))
                {
                    var another = db.Orders.Any(x => x.OrderNumber.ToLower() == order.OrderNumber.ToLower() && x.ConfirmedOn != null);
                    if (another)
                    {
                        return "This Order Number already existed. Please use another number (without prefix).";
                    }

                    if (orders.Count(x => x.OrderNumber != null && x.OrderNumber.ToLower() == order.OrderNumber.ToLower()) > 1)
                    {
                        return "This Order Number is already used many times. Please use other numbers (without prefix) for other items.";
                    }
                }

                return string.Empty;
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }

        private static int CountWeekendDays(DateTime fromDate, DateTime toDate, List<DateTime> offDays)
        {
            var result = 0;
            for (var date = fromDate.Date; date <= toDate.Date; date = date.AddDays(1))
            {
                if (date.DayOfWeek == DayOfWeek.Sunday)
                {
                    ++result;
                }
                else
                {
                    // Off-days
                    foreach (var day in offDays)
                    {
                        if (day.Day == date.Day && day.Month == date.Month && day.Year == date.Year)
                        {
                            ++result;
                        }
                    }
                }
            }

            return result;
        }

        //Export order status tracking start
        public ActionResult ExportOrderTracking()
        {
            var username = this.User.Identity.Name;
            var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (customer == null || customer.IsActive == false)
            {
                return RedirectToAction("Home");
            }
           
            

            var result = (from order in db.Orders
                          where order.CustomerId == customer.Id &&
                                !order.IsDeleted
                          select new OrderCsvViewModel()
                          {
                              Id = order.Id,
                              OrderNumber = order.OrderNumber,
                              Status = order.Status,
                              CreatedOn = order.CreatedOn,
                              ConfirmedOn = order.ConfirmedOn,
                              CollectedOn = order.CollectedOn,
                              DeliveryOn = order.DeliveredOn,
                              CanceledOn = order.CanceledOn,
                              ProceededOn = order.ProceededOn,
                              ProceededOutOn = order.ProceededOutOn
                          }).ToList();

            foreach (var item in result)
            {
                item.OrderNumber = OrderHelper.ToCustomerOrderNumber(item.OrderNumber, customer.CompanyPrefix);
            }
            var folderPath = Server.MapPath("~/Temp/");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            var fileName = customer.UserName + "_OrderTracking_" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".csv";
            var filePath = folderPath + fileName;

            var engine = new FileHelperEngine(typeof(OrderCsvViewModel));
            engine.HeaderText = "Id,OrderNumber,Status,CreatedOn,ConfirmedOn,CollectedOn,DeliveryOn,CanceledOn,ProceededOn,ProceededOutOn,";
            engine.WriteFile(filePath, result);

            return File(filePath, "text/csv", fileName);
        }
        //Export order status tracking end


    }
}
