﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Courier_Management_System
{
    /// <summary>
    /// Map services. This is implemented as reference, should not you because of quota limitation of Google Maps Service API.
    /// Instead we should use Google Maps client services to obtain the same result.
    /// </summary>
    public class MapServices
    {
        public const string GeocodingServiceByAddress = "https://maps.googleapis.com/maps/api/geocode/json?key={0}&address={1}&components=country:SG";
        public const string GeocodingServiceByAddressComponent = "https://maps.googleapis.com/maps/api/geocode/json?key={0}&address={1}&components=postal_code:{2}|country:SG";


        #region Google map services
        /// <summary>
        /// Query to Map service to get result
        /// </summary>
        public static string GetServiceResult(string url)
        {
            var uri = new Uri(url);
            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;
            var response = (HttpWebResponse) request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream == null)
            {
                return string.Empty;
            }
            var reader = new StreamReader(stream);
            var output = reader.ReadToEnd();
            response.Close();

            return output;
        }

        /// <summary>
        /// Get geocoding (latitude and longitude) of an address; Postal Code is optional, used in case there are same address in many region
        /// </summary>
        public static double[] GetGeocodingOfAddress(string key, string address, string postalCode = "")
        {
            var tries = 0;
            do
            {
                // Remove # part
                while (address.Contains("#"))
                {
                    var index = address.IndexOf('#');
                    var nextSpace = address.IndexOf(' ', index);
                    if (nextSpace < 0)
                    {
                        nextSpace = address.Length - 1;
                    }
                    address = address.Remove(index, nextSpace - index);
                }

                // Add Singapore
                if (!address.ToLower().Trim().EndsWith("singapore"))
                {
                    address += ", Singapore";
                }
                string json;
                if (string.IsNullOrEmpty(postalCode))
                {
                    json = GetServiceResult(string.Format(GeocodingServiceByAddress, key, address));
                }
                else
                {
                    json = GetServiceResult(string.Format(GeocodingServiceByAddressComponent, key, address, postalCode));
                }

                // Wait a little bit to avoid flood Map system
                System.Threading.Thread.Sleep(100);

                if (!string.IsNullOrEmpty(json))
                {
                    dynamic result = JsonConvert.DeserializeObject(json);
                    if (result.status == "OK")
                    {
                        return new double[]
                        {
                            result.results[0].geometry.location.lat,
                            result.results[0].geometry.location.lng
                        };
                    }
                }

                ++tries;
            } while (tries < 10);

            return null;
        }

        #endregion
    }
}
