﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web.Mvc;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "SettingsDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.SettingsShowSettings)]
        public ActionResult List()
        {
            var currentNames = db.Settings.Select(x => x.Name).ToList();
            var type = typeof(SettingProvider);
            var result = type.GetProperties(BindingFlags.Public | BindingFlags.Static).Select(x => x).ToList();
            foreach (var item in result)
            {
                if (!currentNames.Contains(item.Name))
                {
                    // Not exists
                    if (item.PropertyType == typeof(int) || item.PropertyType == typeof(decimal) || item.PropertyType == typeof(double))
                    {
                        item.SetValue(null, 0);
                    }
                    else if (item.PropertyType == typeof(string))
                    {
                        item.SetValue(null, "");
                    }
                    else if (item.PropertyType == typeof(bool))
                    {
                        item.SetValue(null, "True");
                    }
                    else
                    {
                        item.SetValue(null, Activator.CreateInstance(item.PropertyType));
                    }
                }
            }

            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.SettingsShowSettings)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.SettingsEditSetting)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = db.Settings.FirstOrDefault(x => x.Id == id);

            if (model == null)
            {
                return HttpNotFound();
            }

            if (model.Type == (int) PropertyTypes.DateArray && !string.IsNullOrEmpty(model.Value))
            {
                var dateStrs = model.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var dates = new List<DateTime>();
                foreach (var str in dateStrs)
                {
                    DateTime dateValue;
                    if (DateTime.TryParseExact(str, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateValue))
                    {
                        dates.Add(dateValue);
                    }
                }
                ViewData["SelectedDates"] = dates.ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.SettingsEditSetting)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, [ModelBinder(typeof(DevExpressEditorsBinder))] Setting model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var setting = db.Settings.FirstOrDefault(x => x.Id == id);
                if (setting == null)
                {
                    return HttpNotFound();
                }

                setting.Value = model.Value;
                setting.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                setting.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    SettingProvider.ClearCache();
                    return RedirectToAction("List", "Settings");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            if (model.Type == (int) PropertyTypes.DateArray && !string.IsNullOrEmpty(model.Value))
            {
                var dateStrs = model.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var dates = new List<DateTime>();
                foreach (var str in dateStrs)
                {
                    DateTime dateValue;
                    if (DateTime.TryParseExact(str, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateValue))
                    {
                        dates.Add(dateValue);
                    }
                }
                ViewData["SelectedDates"] = dates.ToArray();
            }

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<Setting> GetSettings()
        {
            var names = SettingProvider.GetProperties();
            var settings = from setting in DB.Settings
                           where names.Contains(setting.Name)
                           select setting;

            return settings;
        }

        #endregion
    }
}