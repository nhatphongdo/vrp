﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class PaymentTypesController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "PaymentTypesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowPaymentTypes)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowPaymentTypes)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddPaymentType)]
        public ActionResult Add()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddPaymentType)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditPaymentTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var type = new PaymentType()
                {
                    PaymentTypeName = model.Name,
                    Description = HttpUtility.HtmlDecode(model.Description),
                    IsDeleted = false,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };

                db.PaymentTypes.Add(type);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "PaymentTypes");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersEditPaymentType)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from type in db.PaymentTypes
                         where !type.IsDeleted && type.Id == id
                         select new AddEditPaymentTypeViewModel()
                         {
                             Name = type.PaymentTypeName,
                             Description = type.Description
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersEditPaymentType)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditPaymentTypeViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var type = db.PaymentTypes.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (type == null)
                {
                    return HttpNotFound();
                }

                type.PaymentTypeName = model.Name;
                type.Description = HttpUtility.HtmlDecode(model.Description);
                type.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                type.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "PaymentTypes");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersDeletePaymentType)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var type = db.PaymentTypes.FirstOrDefault(x => x.Id == id);
                if (type != null)
                {
                    type.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<PaymentTypeViewModel> GetPaymentTypes()
        {
            var types = (from type in DB.PaymentTypes
                         where !type.IsDeleted
                         select new PaymentTypeViewModel()
                         {
                             Id = type.Id,
                             Name = type.PaymentTypeName,
                             Description = type.Description,
                             CreatedOn = type.CreatedOn,
                             UpdatedOn = type.UpdatedOn
                         });

            return types;
        }

        #endregion
    }
}