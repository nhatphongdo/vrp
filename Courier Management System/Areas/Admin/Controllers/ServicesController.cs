﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web.Mvc;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class ServicesController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "ServicesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowServices)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowServices)]
        public ActionResult ListViewPartial()
        {
            ViewBag.TimeSlots = db.TimeSlots.Where(x => !x.IsDeleted).ToList();
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddService)]
        public ActionResult Add()
        {
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.ProductTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();

            var item = new AddEditServiceViewModel()
                       {
                           AvailablePickupTimeSlots = db.TimeSlots.Where(x => !x.IsDeleted).Select(
                                                                                                   x => new TimeSlotOfService()
                                                                                                        {
                                                                                                            Id = x.Id,
                                                                                                            Name = x.TimeSlotName,
                                                                                                            FromTime = x.FromTime,
                                                                                                            ToTime = x.ToTime,
                                                                                                            IsSelected = false
                                                                                                        }).ToList(),
                           AvailableDeliveryTimeSlots = db.TimeSlots.Where(x => !x.IsDeleted).Select(
                                                                                                     x => new TimeSlotOfService()
                                                                                                          {
                                                                                                              Id = x.Id,
                                                                                                              Name = x.TimeSlotName,
                                                                                                              FromTime = x.FromTime,
                                                                                                              ToTime = x.ToTime,
                                                                                                              IsSelected = false
                                                                                                          }).ToList(),
                       };

            return View(item);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddService)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add([ModelBinder(typeof(DevExpressEditorsBinder))] AddEditServiceViewModel model)
        {
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.ProductTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            if (ModelState.IsValid)
            {
                var service = new Service()
                              {
                                  ServiceName = model.Name,
                                  Description = HttpUtility.HtmlDecode(model.Description),
                                  Cost = model.Cost,
                                  IsPickupTimeAllow = model.IsPickupTimeAllow,
                                  IsPickupDateAllow = model.IsPickupDateAllow,
                                  IsDeliveryTimeAllow = model.IsDeliveryTimeAllow,
                                  IsDeliveryDateAllow = model.IsDeliveryDateAllow,
                                  AvailablePickupTimeSlots = string.Join(",", model.AvailablePickupTimeSlots.Where(x => x.IsSelected).Select(x => x.Id)),
                                  AvailablePickupDateRange = model.AvailablePickupDateRange,
                                  AvailableDeliveryTimeSlots = string.Join(",", model.AvailableDeliveryTimeSlots.Where(x => x.IsSelected).Select(x => x.Id)),
                                  AvailableDeliveryDateRange = model.AvailableDeliveryDateRange,
                                  ProductTypeId = model.ProductTypeId,
                                  IsActive = model.IsActive,
                                  IsAppliedDefaultPromoCode = model.IsAppliedDefaultPromoCode,
                                  LimitToSizes = string.Join(",", model.LimitToSizes),
                                  IsDeleted = false,
                                  CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                  CreatedOn = DateTime.Now,
                                  UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                  UpdatedOn = DateTime.Now
                              };

                db.Services.Add(service);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Services");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditService)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.ProductTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            var model = (from service in db.Services
                         where !service.IsDeleted && service.Id == id
                         select service).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            var pickupTimeSlots = model.AvailablePickupTimeSlots?.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) ?? new string[] { };
            var deliveryTimeSlots = model.AvailableDeliveryTimeSlots?.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) ?? new string[] { };

            var item = new AddEditServiceViewModel()
                       {
                           Name = model.ServiceName,
                           Description = model.Description,
                           Cost = model.Cost,
                           IsPickupTimeAllow = model.IsPickupTimeAllow,
                           IsPickupDateAllow = model.IsPickupDateAllow,
                           IsDeliveryTimeAllow = model.IsDeliveryTimeAllow,
                           IsDeliveryDateAllow = model.IsDeliveryDateAllow,
                           LimitToSizes = string.IsNullOrEmpty(model.LimitToSizes) ? new string[] { } : model.LimitToSizes.Split(','),
                           AvailablePickupTimeSlots = db.TimeSlots.Where(x => !x.IsDeleted).Select(
                                                                                                   x => new TimeSlotOfService()
                                                                                                        {
                                                                                                            Id = x.Id,
                                                                                                            Name = x.TimeSlotName,
                                                                                                            FromTime = x.FromTime,
                                                                                                            ToTime = x.ToTime,
                                                                                                            IsSelected = pickupTimeSlots.Contains(x.Id.ToString())
                                                                                                        }).ToList(),
                           AvailablePickupDateRange = model.AvailablePickupDateRange,
                           AvailableDeliveryTimeSlots = db.TimeSlots.Where(x => !x.IsDeleted).Select(
                                                                                                     x => new TimeSlotOfService()
                                                                                                          {
                                                                                                              Id = x.Id,
                                                                                                              Name = x.TimeSlotName,
                                                                                                              FromTime = x.FromTime,
                                                                                                              ToTime = x.ToTime,
                                                                                                              IsSelected = deliveryTimeSlots.Contains(x.Id.ToString())
                                                                                                          }).ToList(),
                           AvailableDeliveryDateRange = model.AvailableDeliveryDateRange,
                           ProductTypeId = model.ProductTypeId,
                           IsActive = model.IsActive,
                           IsAppliedDefaultPromoCode = model.IsAppliedDefaultPromoCode
                       };

            return View(item);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditService)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, [ModelBinder(typeof(DevExpressEditorsBinder))] AddEditServiceViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.ProductTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            if (ModelState.IsValid)
            {
                var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (service == null)
                {
                    return HttpNotFound();
                }

                service.ServiceName = model.Name;
                service.Description = HttpUtility.HtmlDecode(model.Description);
                service.Cost = model.Cost;
                service.IsPickupTimeAllow = model.IsPickupTimeAllow;
                service.IsPickupDateAllow = model.IsPickupDateAllow;
                service.IsDeliveryTimeAllow = model.IsDeliveryTimeAllow;
                service.IsDeliveryDateAllow = model.IsDeliveryDateAllow;
                service.AvailablePickupTimeSlots = string.Join(",", model.AvailablePickupTimeSlots.Where(x => x.IsSelected).Select(x => x.Id));
                service.AvailablePickupDateRange = model.AvailablePickupDateRange;
                service.AvailableDeliveryTimeSlots = string.Join(",", model.AvailableDeliveryTimeSlots.Where(x => x.IsSelected).Select(x => x.Id));
                service.AvailableDeliveryDateRange = model.AvailableDeliveryDateRange;
                service.LimitToSizes = string.Join(",", model.LimitToSizes);
                service.ProductTypeId = model.ProductTypeId;
                service.IsActive = model.IsActive;
                service.IsAppliedDefaultPromoCode = model.IsAppliedDefaultPromoCode;
                service.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                service.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Services");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsDeleteService)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var service = db.Services.FirstOrDefault(x => x.Id == id);
                if (service != null)
                {
                    service.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<ServiceViewModel> GetServices()
        {
            var services = (from service in DB.Services
                            where !service.IsDeleted
                            select new ServiceViewModel()
                                   {
                                       Id = service.Id,
                                       Name = service.ServiceName,
                                       Description = service.Description,
                                       Cost = service.Cost,
                                       ProductTypeId = service.ProductTypeId,
                                       PickupTime = (service.IsPickupTimeAllow ? service.AvailablePickupTimeSlots : "") + "|" + (service.IsPickupDateAllow ? service.AvailablePickupDateRange : ""),
                                       DeliveryTime = (service.IsDeliveryTimeAllow ? service.AvailableDeliveryTimeSlots : "") + "|" + (service.IsDeliveryDateAllow ? service.AvailableDeliveryDateRange : ""),
                                       LimitToSizes = DB.Sizes.Where(x => (string.IsNullOrEmpty(service.LimitToSizes) ? "" : service.LimitToSizes + ",").Contains(x.Id.ToString() + ",")).ToList(),
                                       IsActive = service.IsActive,
                                       IsAppliedDefaultPromoCode = service.IsAppliedDefaultPromoCode,
                                       CreatedOn = service.CreatedOn,
                                       UpdatedOn = service.UpdatedOn
                                   });

            return services;
        }

        #endregion
    }
}
