﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private CourierDBEntities db = new CourierDBEntities();

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.BasicDashboard + "," + AccountHelper.AdvancedDashboard)]
        public ActionResult Index()
        {
            // Number of this month's order
            var beginDay = DateTime.Now.Date;
            var endDay = beginDay.AddDays(1);
            var beginMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var endMonth = beginMonth.AddMonths(1);
            ViewBag.DayOrders = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.OrderConfirmed && x.CreatedOn >= beginDay && x.CreatedOn < endDay);
            ViewBag.DayBulletOrders = db.Orders.Count(
                                                      x =>
                                                          !x.IsDeleted && x.Status == (int) OrderStatuses.OrderConfirmed && x.CreatedOn >= beginDay &&
                                                          x.CreatedOn < endDay && x.Service.ServiceName.ToLower().Contains("bullet"));
            ViewBag.MonthOrders = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.OrderConfirmed && x.CreatedOn >= beginMonth && x.CreatedOn < endMonth);
            ViewBag.ConfirmedOrders = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.OrderConfirmed);
            ViewBag.CollectedOrders = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.ItemCollected);
            ViewBag.DeliveredOrders = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.ItemDelivered);
            ViewBag.ItemsInDepot = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.ItemInDepot);
            ViewBag.ItemsOutDepot = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.ItemOutDepot);
            ViewBag.RejectedOrders = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.OrderRejected);
            ViewBag.CanceledOrders = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.OrderCanceled);
            ViewBag.FailedPickup = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.PickupFailed);
            ViewBag.FailedDelivery = db.Orders.Count(x => !x.IsDeleted && x.Status == (int) OrderStatuses.DeliveryFailed);
            ViewBag.MonthOrderPrice = db.Orders.Where(x => !x.IsDeleted && x.Status > (int) OrderStatuses.OrderSubmitted && x.CreatedOn >= beginMonth && x.CreatedOn < endMonth).Select(x => x.Price).ToList().Sum();
            ViewBag.DayOrderPrice = db.Orders.Where(x => !x.IsDeleted && x.Status > (int) OrderStatuses.OrderSubmitted && x.CreatedOn >= beginDay && x.CreatedOn < endDay).Select(x => x.Price).ToList().Sum();
            ViewBag.MonthRevenue = db.Payments.Where(x => x.IsSuccessful && x.CreatedOn >= beginMonth && x.CreatedOn < endMonth).Select(x => x.PaymentAmount).ToList().Sum();
            ViewBag.DayRevenue = db.Payments.Where(x => x.IsSuccessful && x.CreatedOn >= beginDay && x.CreatedOn < endDay).Select(x => x.PaymentAmount).ToList().Sum();
            ViewBag.TotalUsers = db.Customers.Count(x => !x.IsDeleted);
            ViewBag.MonthUsers = db.Customers.Count(x => !x.IsDeleted && x.CreatedOn >= beginMonth && x.CreatedOn < endMonth);
            ViewBag.PostPaidUsers = db.Customers.Count(x => !x.IsDeleted && x.PaymentType.PaymentTypeName.ToLower().Contains("postpaid"));
            ViewBag.PrePaidUsers = db.Customers.Count(x => !x.IsDeleted && x.PaymentType.PaymentTypeName.ToLower().Contains("prepaid"));
            ViewBag.PostPaidBalance = db.Customers.Where(x => !x.IsDeleted && x.PaymentType.PaymentTypeName.ToLower().Contains("postpaid")).Sum(x => x.Credit);
            ViewBag.PrePaidBalance = db.Customers.Where(x => !x.IsDeleted && x.PaymentType.PaymentTypeName.ToLower().Contains("prepaid")).Sum(x => x.Credit);
            ViewBag.PendingPayment = db.TopupPayments.Count(x => x.Status == (int) PaymentStatus.Pending);

            return View();
        }
    }
}
