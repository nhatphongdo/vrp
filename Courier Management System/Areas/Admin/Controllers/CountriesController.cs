﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web;
using DevExpress.Web.Mvc;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class CountriesController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "CountriesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CountriesShowCountries)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CountriesShowCountries)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        public ActionResult PhotoUpload()
        {
            var uploadControl = UploadControlExtension.GetUploadedFiles("uploadControl", PathHelper.UploadSettings, (sender, args) =>
            {
                // Check folder
                if (!Directory.Exists(Server.MapPath(PathHelper.CountryUploadBaseDirectory)))
                {
                    Directory.CreateDirectory(Server.MapPath(PathHelper.CountryUploadBaseDirectory));
                }

                var resultFileName = $"{Path.GetRandomFileName()}_{args.UploadedFile.FileName}";
                var resultFileUrl = PathHelper.CountryUploadBaseDirectory + resultFileName;
                var resultFilePath = Server.MapPath(resultFileUrl);
                args.UploadedFile.SaveAs(resultFilePath);

                var name = args.UploadedFile.FileName;
                var url = Url.Content(resultFileUrl);
                var sizeInKilobytes = args.UploadedFile.ContentLength / 1024;
                var sizeText = sizeInKilobytes.ToString() + " KB";
                args.CallbackData = $"{name}|{url}|{sizeText}";
            });
            return null;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CountriesAddCountry)]
        public ActionResult Add()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CountriesAddCountry)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditCountryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var country = new Country()
                {
                    CountryCode = model.CountryCode,
                    CountryName = model.Name,
                    CountryImage = model.CountryImage,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };

                db.Countries.Add(country);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Countries");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CountriesEditCountry)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from country in db.Countries
                         where country.Id == id
                         select new AddEditCountryViewModel()
                         {
                             CountryCode = country.CountryCode,
                             Name = country.CountryName,
                             CountryImage = country.CountryImage
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CountriesEditCountry)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditCountryViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var country = db.Countries.FirstOrDefault(x => x.Id == id);
                if (country == null)
                {
                    return HttpNotFound();
                }

                country.CountryCode = model.CountryCode;
                country.CountryName = model.Name;
                country.CountryImage = model.CountryImage;
                country.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                country.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Countries");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CountriesDeleteCountry)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var country = db.Countries.FirstOrDefault(x => x.Id == id);
                if (country != null)
                {
                    db.Countries.Remove(country);
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<CountryViewModel> GetCountries()
        {
            var countries = (from country in DB.Countries
                select new CountryViewModel()
                {
                    Id = country.Id,
                    CountryCode = country.CountryCode,
                    Name = country.CountryName,
                    CountryImage = country.CountryImage,
                    CreatedOn = country.CreatedOn,
                    UpdatedOn = country.UpdatedOn
                });

            return countries;
        }

        #endregion
    }
}