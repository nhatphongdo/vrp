﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DevExpress.Web.Mvc;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class DriversController : Controller
    {
        #region Fields

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public DriversController()
        {
        }

        public DriversController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "DriversDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversShowDrivers)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversAddDriver)]
        public ActionResult Add()
        {
            ViewBag.CountryCodes = db.Countries.Select(x => new SelectListItem()
                                                            {
                                                                Text = x.CountryName + " (" + x.CountryCode + ")",
                                                                Value = x.CountryCode
                                                            }).ToList();
            ViewBag.PaySchemes = db.PaySchemes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.SchemeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversAddDriver)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddDriverViewModel model)
        {
            ViewBag.CountryCodes = db.Countries.Select(x => new SelectListItem()
                                                            {
                                                                Text = x.CountryName + " (" + x.CountryCode + ")",
                                                                Value = x.CountryCode
                                                            }).ToList();
            ViewBag.PaySchemes = db.PaySchemes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.SchemeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();

            if (ModelState.IsValid)
            {
                // Create account
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    // Add user to role
                    result = await UserManager.AddToRoleAsync(user.Id, model.AccountType == AccountTypes.Staff ? AccountHelper.StaffDriverRole : AccountHelper.ContractorDriverRole);
                    if (!result.Succeeded)
                    {
                        // Rollback
                        result = await UserManager.DeleteAsync(user);
                        ViewBag.ErrorMessages = result.Errors;
                        return View(model);
                    }

                    // Update according to type
                    if (model.AccountType == AccountTypes.Staff)
                    {
                        var staff = new Staff()
                                    {
                                        UserName = model.UserName,
                                        StaffName = model.Name,
                                        StaffAddress = model.Address,
                                        StaffCountry = model.CountryCode,
                                        StaffMobile = model.MobileNumber,
                                        StaffIdentityNumber = model.IdentityNumber,
                                        StaffPhoto = model.Photo,
                                        DrivingLicense = model.DrivingLicense,
                                        ICNumber = model.ICNumber,
                                        Rating = model.Rating,
                                        Remark = model.Remark,
                                        IsActive = model.IsActive,
                                        PayRateId = model.PayRateScheme,
                                        IsDeleted = false,
                                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                        CreatedOn = DateTime.Now,
                                        UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                        UpdatedOn = DateTime.Now
                                    };

                        db.Staffs.Add(staff);
                    }
                    else
                    {
                        var contractor = new Contractor()
                                         {
                                             UserName = model.UserName,
                                             ContractorName = model.Name,
                                             ContractorAddress = model.Address,
                                             ContractorCountry = model.CountryCode,
                                             ContractorMobile = model.MobileNumber,
                                             ContractorPhoto = model.Photo,
                                             DrivingLicense = model.DrivingLicense,
                                             VehicleType = model.VehicleType,
                                             VehicleNumber = model.VehicleNumber,
                                             ICNumber = model.ICNumber,
                                             Rating = model.Rating,
                                             Remark = model.Remark,
                                             IsActive = model.IsActive,
                                             PayRateId = model.PayRateScheme,
                                             IsDeleted = false,
                                             CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                             CreatedOn = DateTime.Now,
                                             UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                             UpdatedOn = DateTime.Now
                                         };

                        db.Contractors.Add(contractor);
                    }

                    try
                    {
                        db.SaveChanges();
                        return RedirectToAction("List", "Drivers");
                    }
                    catch (Exception exc)
                    {
                        // Rollback
                        result = await UserManager.RemoveFromRoleAsync(user.Id, model.AccountType == AccountTypes.Staff ? AccountHelper.StaffDriverRole : AccountHelper.ContractorDriverRole);
                        result = await UserManager.DeleteAsync(user);
                        ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                        LogHelper.Log(exc);
                    }
                }
                else
                {
                    ViewBag.ErrorMessages = result.Errors;
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversEditDriver)]
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            ViewBag.CountryCodes = db.Countries.Select(x => new SelectListItem()
                                                            {
                                                                Text = x.CountryName + " (" + x.CountryCode + ")",
                                                                Value = x.CountryCode
                                                            }).ToList();
            ViewBag.PaySchemes = db.PaySchemes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.SchemeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();

            // Strip text to get real ID
            var parts = id.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 1)
            {
                EditDriverViewModel model;
                var realId = 0;
                int.TryParse(parts[1], out realId);
                if (parts[0].Equals("staff", StringComparison.OrdinalIgnoreCase))
                {
                    // This is staff
                    model = (from staff in db.Staffs
                             from user in db.AspNetUsers
                             where staff.Id == realId && !staff.IsDeleted &&
                                   user.UserName.Equals(staff.UserName, StringComparison.OrdinalIgnoreCase)
                             select new EditDriverViewModel()
                                    {
                                        Name = staff.StaffName,
                                        UserName = staff.UserName,
                                        Address = staff.StaffAddress,
                                        Photo = staff.StaffPhoto,
                                        CountryCode = staff.StaffCountry,
                                        MobileNumber = staff.StaffMobile,
                                        AccountType = AccountTypes.Staff,
                                        DrivingLicense = staff.DrivingLicense,
                                        Email = user.Email,
                                        ICNumber = staff.ICNumber,
                                        IdentityNumber = staff.StaffIdentityNumber,
                                        Rating = staff.Rating,
                                        Remark = staff.Remark,
                                        PayRateScheme = staff.PayRateId,
                                        IsActive = staff.IsActive
                                    }).FirstOrDefault();
                }
                else
                {
                    // This is contractor
                    model = (from contractor in db.Contractors
                             from user in db.AspNetUsers
                             where contractor.Id == realId && !contractor.IsDeleted &&
                                   user.UserName.Equals(contractor.UserName, StringComparison.OrdinalIgnoreCase)
                             select new EditDriverViewModel()
                                    {
                                        Name = contractor.ContractorName,
                                        UserName = contractor.UserName,
                                        Address = contractor.ContractorAddress,
                                        Photo = contractor.ContractorPhoto,
                                        CountryCode = contractor.ContractorCountry,
                                        MobileNumber = contractor.ContractorMobile,
                                        AccountType = AccountTypes.Contractor,
                                        DrivingLicense = contractor.DrivingLicense,
                                        Email = user.Email,
                                        ICNumber = contractor.ICNumber,
                                        VehicleNumber = contractor.VehicleNumber,
                                        VehicleType = contractor.VehicleType,
                                        Rating = contractor.Rating,
                                        Remark = contractor.Remark,
                                        IsActive = contractor.IsActive,
                                        PayRateScheme = contractor.PayRateId
                                    }).FirstOrDefault();
                }

                if (model != null)
                {
                    return View(model);
                }
            }
            return HttpNotFound();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversEditDriver)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string id, EditDriverViewModel model)
        {
            ViewBag.CountryCodes = db.Countries.Select(x => new SelectListItem()
                                                            {
                                                                Text = x.CountryName + " (" + x.CountryCode + ")",
                                                                Value = x.CountryCode
                                                            }).ToList();
            ViewBag.PaySchemes = db.PaySchemes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.SchemeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();

            if (ModelState.IsValid)
            {
                // Strip text to get real ID
                var parts = id.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length > 1)
                {
                    var realId = 0;
                    int.TryParse(parts[1], out realId);

                    // Update email and password
                    IdentityResult result;
                    var user = await UserManager.FindByNameAsync(model.UserName);
                    if (user == null)
                    {
                        ViewBag.ErrorMessages = new string[] { $"Cannot find user with this User Name '{model.UserName}'." };
                        return View(model);
                    }
                    user.Email = model.Email;
                    result = await UserManager.UpdateAsync(user);
                    if (!result.Succeeded)
                    {
                        ViewBag.ErrorMessages = result.Errors;
                        return View(model);
                    }

                    if (!string.IsNullOrEmpty(model.CurrentPassword) && !string.IsNullOrEmpty(model.NewPassword))
                    {
                        result = await UserManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                        if (!result.Succeeded)
                        {
                            ViewBag.ErrorMessages = result.Errors;
                            return View(model);
                        }
                    }

                    // Update according to type
                    if (parts[0].Equals("staff", StringComparison.OrdinalIgnoreCase))
                    {
                        var staff = db.Staffs.FirstOrDefault(x => x.Id == realId && !x.IsDeleted);
                        if (staff != null)
                        {
                            staff.StaffName = model.Name;
                            staff.StaffAddress = model.Address;
                            staff.StaffCountry = model.CountryCode;
                            staff.StaffMobile = model.MobileNumber;
                            staff.StaffIdentityNumber = model.IdentityNumber;
                            staff.StaffPhoto = model.Photo;
                            staff.ICNumber = model.ICNumber;
                            staff.DrivingLicense = model.DrivingLicense;
                            staff.Rating = model.Rating;
                            staff.Remark = model.Remark;
                            staff.IsActive = model.IsActive;
                            staff.PayRateId = model.PayRateScheme;
                            staff.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            staff.UpdatedOn = DateTime.Now;
                        }
                        else
                        {
                            return HttpNotFound();
                        }
                    }
                    else
                    {
                        var contractor = db.Contractors.FirstOrDefault(x => x.Id == realId && !x.IsDeleted);
                        if (contractor != null)
                        {
                            contractor.ContractorName = model.Name;
                            contractor.ContractorAddress = model.Address;
                            contractor.ContractorCountry = model.CountryCode;
                            contractor.ContractorMobile = model.MobileNumber;
                            contractor.ContractorPhoto = model.Photo;
                            contractor.DrivingLicense = model.DrivingLicense;
                            contractor.VehicleType = model.VehicleType;
                            contractor.VehicleNumber = model.VehicleNumber;
                            contractor.ICNumber = model.ICNumber;
                            contractor.Rating = model.Rating;
                            contractor.Remark = model.Remark;
                            contractor.IsActive = model.IsActive;
                            contractor.PayRateId = model.PayRateScheme;
                            contractor.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            contractor.UpdatedOn = DateTime.Now;
                        }
                        else
                        {
                            return HttpNotFound();
                        }
                    }

                    try
                    {
                        db.SaveChanges();
                        return RedirectToAction("List", "Drivers");
                    }
                    catch (Exception exc)
                    {
                        // Rollback
                        ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                        LogHelper.Log(exc);
                    }
                }
                else
                {
                    return HttpNotFound();
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversShowDrivers)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        public ActionResult PhotoUpload()
        {
            var uploadControl = UploadControlExtension.GetUploadedFiles("uploadControl",
                                                                        PathHelper.UploadSettings,
                                                                        (sender, args) =>
                                                                        {
                                                                            // Check folder
                                                                            if (!Directory.Exists(Server.MapPath(PathHelper.DriverUploadBaseDirectory)))
                                                                            {
                                                                                Directory.CreateDirectory(Server.MapPath(PathHelper.DriverUploadBaseDirectory));
                                                                            }

                                                                            var resultFileName = $"{Path.GetRandomFileName()}{Path.GetExtension(args.UploadedFile.FileName)}";
                                                                            var resultFileUrl = PathHelper.DriverUploadBaseDirectory + resultFileName;
                                                                            var resultFilePath = Server.MapPath(resultFileUrl);
                                                                            args.UploadedFile.SaveAs(resultFilePath);

                                                                            var name = args.UploadedFile.FileName;
                                                                            var url = Url.Content(resultFileUrl);
                                                                            var sizeInKilobytes = args.UploadedFile.ContentLength / 1024;
                                                                            var sizeText = sizeInKilobytes.ToString() + " KB";
                                                                            args.CallbackData = $"{name}|{url}|{sizeText}";
                                                                        });
            return null;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversDeleteDriver)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(string id)
        {
            // Strip text to get real ID
            var parts = id.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 1)
            {
                var realId = 0;
                int.TryParse(parts[1], out realId);
                if (parts[0].Equals("staff", StringComparison.OrdinalIgnoreCase))
                {
                    // This is staff
                    var staff = db.Staffs.FirstOrDefault(x => x.Id == realId);
                    if (staff != null)
                    {
                        staff.IsDeleted = true;
                    }
                }
                else
                {
                    // This is contractor
                    var contractor = db.Contractors.FirstOrDefault(x => x.Id == realId);
                    if (contractor != null)
                    {
                        contractor.IsDeleted = true;
                    }
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                    LogHelper.Log(exc);
                }
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            base.Dispose(disposing);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversExportDrivers)]
        [HttpPost]
        public ActionResult Export(string type)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetGridViewExportSettings(), GetDrivers().ToList(), "Drivers-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetGridViewExportSettings(), GetDrivers().ToList(), "Drivers-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetGridViewExportSettings(), GetDrivers().ToList(), "Drivers-" + DateTime.Now.ToString("yyyyMMdd"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewDrivers";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add("Name");
            settings.Columns.Add("Address");
            settings.Columns.Add("CountryCode");
            settings.Columns.Add("MobileNumber");
            settings.Columns.Add(c =>
                                 {
                                     c.FieldName = "Photo";
                                     c.ColumnType = MVCxGridViewColumnType.BinaryImage;
                                     BinaryImageEditProperties properties = (BinaryImageEditProperties) c.PropertiesEdit;
                                     properties.ImageWidth = 120;
                                     properties.ImageHeight = 80;
                                     properties.ExportImageSettings.Width = 90;
                                     properties.ExportImageSettings.Height = 60;
                                 });
            settings.Columns.Add("UserName");
            settings.Columns.Add("Email");
            settings.Columns.Add("DrivingLicense");
            settings.Columns.Add("VehicleType");
            settings.Columns.Add("VehicleNumber");
            settings.Columns.Add("ICNumber");
            settings.Columns.Add("IdentityNumber").Caption = "Staff Number";
            var column = settings.Columns.Add("AccountType", MVCxGridViewColumnType.ComboBox);
            var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = EnumHelper.GetSelectList(typeof(AccountTypes));
            comboBoxProperties.TextField = "Text";
            comboBoxProperties.ValueField = "Value";
            comboBoxProperties.ValueType = typeof(int);
            settings.Columns.Add("Rating");
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversShowPayScheme)]
        public ActionResult PaySchemes()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversShowPayScheme)]
        public ActionResult PaySchemesViewPartial()
        {
            return PartialView("_PaySchemesViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversDeletePayScheme)]
        [HttpPost]
        public ActionResult PaySchemesViewPartialDelete(int? id)
        {
            var payscheme = db.PaySchemes.FirstOrDefault(x => x.Id == id);
            if (payscheme != null)
            {
                payscheme.IsDeleted = true;
                foreach (var rate in payscheme.PayRates)
                {
                    rate.IsDeleted = true;
                }
            }

            try
            {
                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_PaySchemesViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversAddPayScheme)]
        public ActionResult AddPayScheme()
        {
            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                {
                                                                                    Text = x.ServiceName,
                                                                                    Value = x.Id.ToString()
                                                                                }).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                          {
                                                                              Text = x.SizeName,
                                                                              Value = x.Id.ToString()
                                                                          }).ToList();
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversAddPayScheme)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPayScheme(FormCollection form)
        {
            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                {
                                                                                    Text = x.ServiceName,
                                                                                    Value = x.Id.ToString()
                                                                                }).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                          {
                                                                              Text = x.SizeName,
                                                                              Value = x.Id.ToString()
                                                                          }).ToList();

            if (string.IsNullOrEmpty(form["SchemeName"]))
            {
                ViewBag.ErrorMessages = new string[] { "Scheme Name is required." };
                return View();
            }

            var payscheme = new PayScheme()
                            {
                                SchemeName = form["SchemeName"],
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                UpdatedOn = DateTime.Now,
                                IsDeleted = false,
                                PayRates = new List<PayRate>()
                            };

            foreach (var key in form.AllKeys)
            {
                if (!key.ToLower().StartsWith("service_"))
                {
                    continue;
                }
                var fakeId = key.Substring("service_".Length);

                if (string.IsNullOrEmpty(form[key]) || string.IsNullOrEmpty(form["Size_" + fakeId]) || string.IsNullOrEmpty(form["Amount_" + fakeId]))
                {
                    continue;
                }

                var serviceId = 0;
                var sizeId = 0;
                var amount = (decimal) 0;
                if (int.TryParse(form[key], out serviceId) && int.TryParse(form["Size_" + fakeId], out sizeId) && decimal.TryParse(form["Amount_" + fakeId], out amount))
                {
                    payscheme.PayRates.Add(new PayRate()
                                           {
                                               Amount = amount,
                                               CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                               CreatedOn = DateTime.Now,
                                               UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                               UpdatedOn = DateTime.Now,
                                               IsDeleted = false,
                                               ServiceId = serviceId,
                                               SizeId = sizeId
                                           });
                }
            }

            if (payscheme.PayRates.Count == 0)
            {
                ViewBag.ErrorMessages = new string[] { "You need to add at least 1 valid pay rate." };
                return View(payscheme);
            }

            db.PaySchemes.Add(payscheme);
            try
            {
                db.SaveChanges();
                return RedirectToAction("PaySchemes", "Drivers");
            }
            catch (Exception exc)
            {
                // Rollback
                ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                LogHelper.Log(exc);
            }

            return View(payscheme);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversEditPayScheme)]
        public ActionResult EditPayScheme(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                {
                                                                                    Text = x.ServiceName,
                                                                                    Value = x.Id.ToString()
                                                                                }).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                          {
                                                                              Text = x.SizeName,
                                                                              Value = x.Id.ToString()
                                                                          }).ToList();

            var payscheme = db.PaySchemes.FirstOrDefault(x => x.Id == id && !x.IsDeleted);
            if (payscheme == null)
            {
                return HttpNotFound();
            }

            return View(payscheme);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversEditPayScheme)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPayScheme(int? id, FormCollection form)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                {
                                                                                    Text = x.ServiceName,
                                                                                    Value = x.Id.ToString()
                                                                                }).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                          {
                                                                              Text = x.SizeName,
                                                                              Value = x.Id.ToString()
                                                                          }).ToList();

            var payscheme = db.PaySchemes.FirstOrDefault(x => x.Id == id && !x.IsDeleted);
            if (payscheme == null)
            {
                return HttpNotFound();
            }

            if (string.IsNullOrEmpty(form["SchemeName"]))
            {
                ViewBag.ErrorMessages = new string[] { "Scheme Name is required." };
                return View();
            }

            payscheme.SchemeName = form["SchemeName"];
            payscheme.UpdatedOn = DateTime.Now;
            payscheme.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            payscheme.PayRates.ForEach(x => x.IsDeleted = true);

            foreach (var key in form.AllKeys)
            {
                if (!key.ToLower().StartsWith("service_"))
                {
                    continue;
                }
                var fakeId = key.Substring("service_".Length);

                if (string.IsNullOrEmpty(form[key]) || string.IsNullOrEmpty(form["Size_" + fakeId]) || string.IsNullOrEmpty(form["Amount_" + fakeId]))
                {
                    continue;
                }

                var serviceId = 0;
                var sizeId = 0;
                var amount = (decimal) 0;
                if (int.TryParse(form[key], out serviceId) && int.TryParse(form["Size_" + fakeId], out sizeId) && decimal.TryParse(form["Amount_" + fakeId], out amount))
                {
                    var rate = payscheme.PayRates.FirstOrDefault(x => x.Id.ToString() == fakeId);
                    if (rate != null)
                    {
                        rate.IsDeleted = false;
                        rate.Amount = amount;
                        rate.ServiceId = serviceId;
                        rate.SizeId = sizeId;
                        rate.UpdatedOn = DateTime.Now;
                        rate.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    }
                    else
                    {
                        payscheme.PayRates.Add(new PayRate()
                                               {
                                                   Amount = amount,
                                                   CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                   CreatedOn = DateTime.Now,
                                                   UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                   UpdatedOn = DateTime.Now,
                                                   IsDeleted = false,
                                                   ServiceId = serviceId,
                                                   SizeId = sizeId
                                               });
                    }
                }
            }

            if (payscheme.PayRates.Count(x => !x.IsDeleted) == 0)
            {
                ViewBag.ErrorMessages = new string[] { "You need to add at least 1 valid pay rate." };
                return View(payscheme);
            }

            try
            {
                db.SaveChanges();
                return RedirectToAction("PaySchemes", "Drivers");
            }
            catch (Exception exc)
            {
                // Rollback
                ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                LogHelper.Log(exc);
            }

            return View(payscheme);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversExportPaySchemes)]
        [HttpPost]
        public ActionResult ExportPaySchemes(string type)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetPaySchemesGridViewExportSettings(), GetExportPaySchemes(), "Payschemes-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetPaySchemesGridViewExportSettings(), GetExportPaySchemes(), "Payschemes-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetPaySchemesGridViewExportSettings(), GetExportPaySchemes(), "Payschemes-" + DateTime.Now.ToString("yyyyMMdd"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetPaySchemesGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewPaySchemes";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add("SchemeName");
            settings.Columns.Add("PayRate");
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversShowDriverHistory)]
        public ActionResult History(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            ViewBag.DriverId = id;
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;

            // Strip text to get real ID
            var parts = id.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 1)
            {
                var realId = 0;
                int.TryParse(parts[1], out realId);
                if (parts[0].Equals("staff", StringComparison.OrdinalIgnoreCase))
                {
                    // This is staff
                    var driver = db.Staffs.FirstOrDefault(x => !x.IsDeleted && x.Id == realId);
                    if (driver == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.Username = driver.UserName;
                    ViewBag.DriverName = driver.StaffName;
                }
                else
                {
                    // This is contractor
                    var driver = db.Contractors.FirstOrDefault(x => !x.IsDeleted && x.Id == realId);
                    if (driver == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.Username = driver.UserName;
                    ViewBag.DriverName = driver.ContractorName;
                }
            }
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversShowDriverHistory)]
        public ActionResult HistoryListViewPartial(DateTime? start, DateTime? end, string username, DateTime? startDate, DateTime? endDate)
        {
            ViewBag.Username = username;
            ViewBag.StartDate = start ?? startDate;
            ViewBag.EndDate = end ?? endDate;
            return PartialView("_HistoryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversDeleteDriverHistory)]
        [HttpPost]
        public ActionResult HistoryListViewPartialDelete(long? id)
        {
            var history = db.JobHistories.FirstOrDefault(x => x.Id == id);
            if (history != null)
            {
                history.IsDeleted = true;
            }

            try
            {
                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_HistoryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversExportDriverHistory)]
        [HttpPost]
        public ActionResult ExportHistory(string type, string username, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetHistoriesGridViewExportSettings(), GetDriverJobHistory(username, startDate, endDate).ToList(), "DriverHistory-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetHistoriesGridViewExportSettings(), GetDriverJobHistory(username, startDate, endDate).ToList(), "DriverHistory-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetHistoriesGridViewExportSettings(), GetDriverJobHistory(username, startDate, endDate).ToList(), "DriverHistory-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetHistoriesGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewHistories";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add("JobId");
            settings.Columns.Add("ConsignmentNumber");
            settings.Columns.Add("Size");
            settings.Columns.Add("BinCode");
            settings.Columns.Add("AssignedOn");
            var column = settings.Columns.Add("CompletedOn");
            column.Caption = "Completed On Date";
            column.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
            column = settings.Columns.Add("CompletedOn");
            column.Caption = "Completed On Time";
            column.PropertiesEdit.DisplayFormatString = "HH:mm:ss";
            settings.Columns.Add("JobName");
            settings.Columns.Add("JobStatus");
            column = settings.Columns.Add("Payment");
            column.PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("Rating");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversShowDriverPayslip)]
        public ActionResult Payslip(string id, string start, string end)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            var username = "";
            // Strip text to get real ID
            var parts = id.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 1)
            {
                var realId = 0;
                int.TryParse(parts[1], out realId);
                if (parts[0].Equals("staff", StringComparison.OrdinalIgnoreCase))
                {
                    // This is staff
                    var driver = db.Staffs.FirstOrDefault(x => !x.IsDeleted && x.Id == realId);
                    if (driver == null)
                    {
                        return HttpNotFound();
                    }
                    username = driver.UserName;
                    ViewBag.DriverName = driver.StaffName;
                }
                else
                {
                    // This is contractor
                    var driver = db.Contractors.FirstOrDefault(x => !x.IsDeleted && x.Id == realId);
                    if (driver == null)
                    {
                        return HttpNotFound();
                    }
                    username = driver.UserName;
                    ViewBag.DriverName = driver.ContractorName;
                }
            }

            ViewBag.Username = username;

            DateTime startDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date;
            DateTime.TryParseExact(start, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out startDate);
            DateTime.TryParseExact(end, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDate);

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            var histories = GetDriverJobHistory(username, startDate, endDate);
            var payslip = histories
                .Where(x => x.JobStatus == "Successful")
                .GroupBy(x => new
                              {
                                  Date = DbFunctions.TruncateTime(x.CompletedOn),
                                  JobId = x.JobId
                              })
                .Select(g => new PayslipModel()
                             {
                                 Date = g.Key.Date,
                                 JobId = g.Key.JobId,
                                 Amount = g.Sum(x => x.Payment),
                                 Deduction = g.Sum(x => x.DeductionAmount)
                             })
                .OrderBy(x => x.Date)
                .ToList();

            ViewBag.Payslip = payslip;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DriversEmailDriverPayslip)]
        [HttpPost]
        [ActionName("Payslip")]
        public ActionResult Payslip_Post(string id, string start, string end)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            var username = "";
            // Strip text to get real ID
            var parts = id.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 1)
            {
                var realId = 0;
                int.TryParse(parts[1], out realId);
                if (parts[0].Equals("staff", StringComparison.OrdinalIgnoreCase))
                {
                    // This is staff
                    var driver = db.Staffs.FirstOrDefault(x => !x.IsDeleted && x.Id == realId);
                    if (driver == null)
                    {
                        return HttpNotFound();
                    }
                    username = driver.UserName;
                    ViewBag.DriverName = driver.StaffName;
                }
                else
                {
                    // This is contractor
                    var driver = db.Contractors.FirstOrDefault(x => !x.IsDeleted && x.Id == realId);
                    if (driver == null)
                    {
                        return HttpNotFound();
                    }
                    username = driver.UserName;
                    ViewBag.DriverName = driver.ContractorName;
                }
            }

            ViewBag.Username = username;

            DateTime startDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date;
            DateTime.TryParseExact(start, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out startDate);
            DateTime.TryParseExact(end, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDate);

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            var histories = GetDriverJobHistory(username, startDate, endDate);
            var payslip = histories
                .Where(x => x.JobStatus == "Successful")
                .GroupBy(x => new
                              {
                                  Date = DbFunctions.TruncateTime(x.CompletedOn),
                                  JobId = x.JobId
                              })
                .Select(g => new PayslipModel()
                             {
                                 Date = g.Key.Date,
                                 JobId = g.Key.JobId,
                                 Amount = g.Sum(x => x.Payment),
                                 Deduction = g.Sum(x => x.DeductionAmount)
                             })
                .OrderBy(x => x.Date)
                .ToList();

            var result = ViewEngines.Engines.FindView(ControllerContext, "Payslip", null);

            using (var output = new StringWriter())
            {
                var viewContext = new ViewContext(ControllerContext,
                                                  result.View,
                                                  new ViewDataDictionary()
                                                  {
                                                      { "Payslip", payslip },
                                                      { "DriverName", ViewBag.DriverName },
                                                      { "Username", ViewBag.Username },
                                                      { "StartDate", ViewBag.StartDate },
                                                      { "EndDate", ViewBag.EndDate }
                                                  },
                                                  ControllerContext.Controller.TempData,
                                                  output);
                result.View.Render(viewContext, output);
                result.ViewEngine.ReleaseView(ControllerContext, result.View);

                var email = output.ToString();
                var index = email.IndexOf("<form", StringComparison.OrdinalIgnoreCase);
                if (index >= 0)
                {
                    email = email.Substring(0, index) + email.Substring(email.IndexOf("/form>", index + 1, StringComparison.OrdinalIgnoreCase) + "/form>".Length);
                }
                AccountHelper.SendEmailSync(username, "Payslip", email);
            }

            ViewBag.Payslip = payslip;
            ViewBag.Message = "Email is already sent.";
            return View();
        }

        #region Data queries

        public static IQueryable<DriverViewModel> GetDrivers()
        {
            var staffs = (from person in DB.Staffs
                          from user in DB.AspNetUsers
                          where !person.IsDeleted && user.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)
                          select new DriverViewModel()
                                 {
                                     Id = "Staff_" + person.Id,
                                     Name = person.StaffName,
                                     CreatedOn = person.CreatedOn,
                                     UserName = person.UserName,
                                     Address = person.StaffAddress,
                                     Photo = person.StaffPhoto,
                                     UpdatedOn = person.UpdatedOn,
                                     CountryCode = person.StaffCountry,
                                     MobileNumber = person.StaffMobile,
                                     VehicleType = "",
                                     DrivingLicense = person.DrivingLicense,
                                     Email = user.Email,
                                     VehicleNumber = "",
                                     AccountType = AccountTypes.Staff,
                                     Rating = person.Rating,
                                     ICNumber = person.ICNumber,
                                     IdentityNumber = person.StaffIdentityNumber,
                                     NotificationKey = person.NotificationKey,
                                     IsActive = person.IsActive
                                 });

            var contractors = (from person in DB.Contractors
                               from user in DB.AspNetUsers
                               where !person.IsDeleted && user.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)
                               select new DriverViewModel()
                                      {
                                          Id = "Contractor_" + person.Id,
                                          Name = person.ContractorName,
                                          CreatedOn = person.CreatedOn,
                                          UserName = person.UserName,
                                          Address = person.ContractorAddress,
                                          Photo = person.ContractorPhoto,
                                          UpdatedOn = person.UpdatedOn,
                                          CountryCode = person.ContractorCountry,
                                          MobileNumber = person.ContractorMobile,
                                          VehicleType = person.VehicleType,
                                          DrivingLicense = person.DrivingLicense,
                                          Email = user.Email,
                                          VehicleNumber = person.VehicleNumber,
                                          AccountType = AccountTypes.Contractor,
                                          Rating = person.Rating,
                                          ICNumber = person.ICNumber,
                                          IdentityNumber = "",
                                          NotificationKey = person.NotificationKey,
                                          IsActive = person.IsActive
                                      });

            return staffs.Union(contractors);
        }

        public static IQueryable<DriverViewModel> GetStaffs()
        {
            var staffs = (from person in DB.Staffs
                          from user in DB.AspNetUsers
                          where !person.IsDeleted && person.IsActive && user.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)
                          select new DriverViewModel()
                                 {
                                     Id = "Staff_" + person.Id,
                                     Name = person.StaffName,
                                     CreatedOn = person.CreatedOn,
                                     UserName = person.UserName,
                                     Address = person.StaffAddress,
                                     Photo = person.StaffPhoto,
                                     UpdatedOn = person.UpdatedOn,
                                     CountryCode = person.StaffCountry,
                                     MobileNumber = person.StaffMobile,
                                     VehicleType = "",
                                     DrivingLicense = person.DrivingLicense,
                                     Email = user.Email,
                                     VehicleNumber = "",
                                     AccountType = AccountTypes.Staff,
                                     Rating = person.Rating,
                                     ICNumber = person.ICNumber,
                                     IdentityNumber = person.StaffIdentityNumber,
                                     NotificationKey = person.NotificationKey,
                                     IsActive = person.IsActive
                                 });

            return staffs;
        }

        public static IQueryable<DriverViewModel> GetContractors()
        {
            var contractors = (from person in DB.Contractors
                               from user in DB.AspNetUsers
                               where !person.IsDeleted && person.IsActive && user.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)
                               select new DriverViewModel()
                                      {
                                          Id = "Contractor_" + person.Id,
                                          Name = person.ContractorName,
                                          CreatedOn = person.CreatedOn,
                                          UserName = person.UserName,
                                          Address = person.ContractorAddress,
                                          Photo = person.ContractorPhoto,
                                          UpdatedOn = person.UpdatedOn,
                                          CountryCode = person.ContractorCountry,
                                          MobileNumber = person.ContractorMobile,
                                          VehicleType = person.VehicleType,
                                          DrivingLicense = person.DrivingLicense,
                                          Email = user.Email,
                                          VehicleNumber = person.VehicleNumber,
                                          AccountType = AccountTypes.Contractor,
                                          Rating = person.Rating,
                                          ICNumber = person.ICNumber,
                                          IdentityNumber = "",
                                          NotificationKey = person.NotificationKey,
                                          IsActive = person.IsActive
                                      });

            return contractors;
        }

        public static IQueryable<PayScheme> GetPaySchemes()
        {
            return DB.PaySchemes.Where(x => !x.IsDeleted);
        }

        public static List<PaySchemeExport> GetExportPaySchemes()
        {
            return DB.PaySchemes.Where(x => !x.IsDeleted)
                     .ToList()
                     .Select(x => new PaySchemeExport
                                  {
                                      SchemeName = x.SchemeName,
                                      PayRate = string.Join("\n", x.PayRates.Where(r => !r.IsDeleted).Select(rate => rate.Service.ServiceName + " " + rate.Size.SizeName + ": " + rate.Amount.ToString("C"))),
                                      CreatedOn = x.CreatedOn
                                  })
                     .ToList();
        }

        public static IQueryable<DriverJobHistoryModel> GetDriverJobHistory(string driverUsername, DateTime? start, DateTime? end)
        {
            if (driverUsername == null)
            {
                driverUsername = "";
            }
            if (!start.HasValue || !end.HasValue)
            {
                start = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
                end = DateTime.Now.Date;
            }

            var list = from history in DB.JobHistories
                       join order in DB.Orders on history.ConsignmentNumber.ToLower() equals order.ConsignmentNumber.ToLower() into orderGroup
                       where !history.IsDeleted && history.CompletedBy.ToLower() == driverUsername.ToLower() &&
                             DbFunctions.TruncateTime(history.CompletedOn) >= start && DbFunctions.TruncateTime(history.CompletedOn) <= end
                       select new DriverJobHistoryModel()
                              {
                                  Id = history.Id,
                                  ConsignmentNumber = history.ConsignmentNumber,
                                  BinCode = orderGroup.Select(x => x.BinCode).FirstOrDefault(),
                                  Size = orderGroup.Select(x => x.Size.SizeName).FirstOrDefault(),
                                  AssignedOn = history.Job.TakenOn,
                                  CompletedOn = history.CompletedOn,
                                  JobId = history.JobId,
                                  JobName = history.IsPickup ? "Pickup" : "Delivery",
                                  JobStatus = history.IsSuccessful ? "Successful" : "Failed",
                                  Payment = history.Payment,
                                  DeductionAmount = history.DeductionAmount,
                                  Rating = history.Rating
                              };
            return list;
        }

        public class PaySchemeExport
        {
            public string SchemeName { get; set; }

            public string PayRate { get; set; }

            public DateTime CreatedOn { get; set; }
        }

        #endregion
    }
}