﻿using System;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        #region Fields

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public UsersController()
        {
        }

        public UsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "UsersDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersShowUsers)]
        public ActionResult List()
        {
            ViewBag.Roles = db.AspNetRoles.ToList();

            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersShowUsers + "," + AccountHelper.UsersEditUser)]
        public ActionResult ListViewPartial(string id, string command, string role)
        {
            ViewBag.Roles = db.AspNetRoles.ToList();
            ViewBag.FilterRole = role;

            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(command))
            {
                return PartialView("_ListViewPartial");
            }

            var profile = db.UserProfiles.FirstOrDefault(x => x.UserName.Equals(id, StringComparison.OrdinalIgnoreCase));
            if (profile == null)
            {
                profile = new UserProfile()
                          {
                              CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                              CreatedOn = DateTime.Now,
                              UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                              UpdatedOn = DateTime.Now,
                              UserName = id,
                          };
                db.UserProfiles.Add(profile);
            }

            if (command.Equals("active", StringComparison.OrdinalIgnoreCase))
            {
                profile.IsActive = true;
            }
            else if (command.Equals("deactive", StringComparison.OrdinalIgnoreCase))
            {
                profile.IsActive = false;
            }

            db.SaveChanges();

            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersShowRoles)]
        public ActionResult Roles()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersShowRoles)]
        public ActionResult RoleListViewPartial()
        {
            return PartialView("_RoleListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersAddRole)]
        public ActionResult AddRole()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersAddRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddRole(AddEditRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Create role
                var role = new AspNetRole()
                           {
                               Name = model.Name,
                               Id = Guid.NewGuid().ToString()
                           };

                db.AspNetRoles.Add(role);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Roles", "Users");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersEditRole)]
        public ActionResult EditRole(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            var model = (from role in db.AspNetRoles
                         where role.Id.Equals(id, StringComparison.OrdinalIgnoreCase)
                         select new AddEditRoleViewModel()
                                {
                                    Name = role.Name
                                }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersEditRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditRole(string id, AddEditRoleViewModel model)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var role = db.AspNetRoles.FirstOrDefault(x => x.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
                if (role == null)
                {
                    return HttpNotFound();
                }

                role.Name = model.Name;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Roles", "Users");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersDeleteRole)]
        [HttpPost]
        public ActionResult RoleListViewPartialDelete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            try
            {
                var userCount = db.AspNetUsers.Count(x => x.AspNetRoles.Any(r => r.Id.Equals(id, StringComparison.OrdinalIgnoreCase)));
                if (userCount > 0)
                {
                    // Inform
                    ViewBag.WarningMessage = "You cannot delete this role because some users belong to it!";
                    return PartialView("_RoleListViewPartial");
                }

                var role = db.AspNetRoles.FirstOrDefault(x => x.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
                if (role != null)
                {
                    db.AspNetRoles.Remove(role);
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_RoleListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersEditUser)]
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            var model = (from user in db.AspNetUsers
                         where user.Id.Equals(id, StringComparison.OrdinalIgnoreCase)
                         select user).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            var roles = UserManager.GetRoles(model.Id);

            var editUser = new EditUserViewModel()
                           {
                               Email = model.Email,
                               Roles = db.AspNetRoles.Select(
                                                             x => new UserRoleViewModel()
                                                                  {
                                                                      Name = x.Name,
                                                                      IsSelected = roles.Any(r => r.Equals(x.Name, StringComparison.OrdinalIgnoreCase))
                                                                  })
                                         .ToList()
                                         .Where(x => Array.IndexOf(AccountHelper.RoleNames, x.Name) >= 0)
                                         .OrderBy(x => Array.IndexOf(AccountHelper.RoleNames, x.Name))
                                         .ToList()
                           };

            return View(editUser);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersEditUser)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string id, EditUserViewModel model)
        {
            if (string.IsNullOrEmpty(id))
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                IdentityResult result;
                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    ViewBag.ErrorMessages = new string[] { $"Cannot find user with this Id '{id}'." };
                    return View(model);
                }

                user.Email = model.Email;
                result = await UserManager.UpdateAsync(user);
                if (!result.Succeeded)
                {
                    ViewBag.ErrorMessages = result.Errors;
                    return View(model);
                }

                // Remove all roles                
                var roles = await UserManager.GetRolesAsync(id);
                result = await UserManager.RemoveFromRolesAsync(id, roles.ToArray());
                if (!result.Succeeded)
                {
                    ViewBag.ErrorMessages = result.Errors;
                    return View(model);
                }

                // Re add roles
                result = await UserManager.AddToRolesAsync(id, model.Roles.Where(x => x.IsSelected).Select(x => x.Name).ToArray());
                if (!result.Succeeded)
                {
                    ViewBag.ErrorMessages = result.Errors;
                    return View(model);
                }

                return RedirectToAction("List", "Users");
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersAccessRights)]
        public ActionResult AccessRights()
        {
            ApplicationDbInitializer.InitializeIdentityForEF(new ApplicationDbContext());
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.UsersAccessRights)]
        public ActionResult AccessRightsViewPartial(string id, string command, string role, bool? isChecked)
        {
            ViewBag.Roles = db.AspNetRoles.ToList()
                              .Where(x => Array.IndexOf(AccountHelper.RoleNames, x.Name) >= 0)
                              .OrderBy(x => Array.IndexOf(AccountHelper.RoleNames, x.Name))
                              .ToList();
            
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(role) && isChecked.HasValue)
            {
                var user = UserManager.FindById(id);
                if (user != null)
                {
                    if (isChecked.Value)
                    {
                        UserManager.AddToRole(id, role);
                    }
                    else
                    {
                        UserManager.RemoveFromRole(id, role);
                    }
                }
            }

            return PartialView("_AccessRightsViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<UserViewModel> GetUsers(string role = "")
        {
            var users = (from person in DB.AspNetUsers
                         where string.IsNullOrEmpty(role) || person.AspNetRoles.Any(x => x.Name.Equals(role, StringComparison.OrdinalIgnoreCase))
                         select new UserViewModel()
                                {
                                    Id = person.Id,
                                    UserName = person.UserName,
                                    Email = person.Email,
                                    Roles = person.AspNetRoles.Select(x => x.Name).ToList(),
                                    IsActive = !DB.UserProfiles.Any(x => x.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)) ||
                                               DB.UserProfiles.Where(x => x.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)).Select(x => x.IsActive).FirstOrDefault()
                                });

            return users;
        }

        public static IQueryable<RoleViewModel> GetRoles()
        {
            return (from role in DB.AspNetRoles
                    select new RoleViewModel()
                           {
                               Name = role.Name,
                               Id = role.Id
                           });
        }

        public static IQueryable<UserViewModel> GetAccessRights()
        {
            var users = (from person in DB.AspNetUsers
                         select new UserViewModel()
                         {
                             Id = person.Id,
                             UserName = person.UserName,
                             Email = person.Email,
                             Roles = person.AspNetRoles.Select(x => x.Name).ToList()
                         });

            return users;
        }

        #endregion
    }
}
