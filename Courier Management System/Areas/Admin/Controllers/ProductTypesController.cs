﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class ProductTypesController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "ProductTypesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowProductTypes)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowProductTypes)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddProductType)]
        public ActionResult Add()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddProductType)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditProductTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var type = new ProductType()
                {
                    ProductTypeName = model.Name,
                    Description = HttpUtility.HtmlDecode(model.Description),
                    BinCode = model.BinCode,
                    IsDeleted = false,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };

                db.ProductTypes.Add(type);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "ProductTypes");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditProductType)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from type in db.ProductTypes
                         where !type.IsDeleted && type.Id == id
                         select new AddEditProductTypeViewModel()
                         {
                             Name = type.ProductTypeName,
                             Description = type.Description,
                             BinCode = type.BinCode,
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditProductType)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditProductTypeViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var type = db.ProductTypes.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (type == null)
                {
                    return HttpNotFound();
                }

                type.ProductTypeName = model.Name;
                type.Description = HttpUtility.HtmlDecode(model.Description);
                type.BinCode = model.BinCode;
                type.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                type.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "ProductTypes");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsDeleteProductType)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var type = db.ProductTypes.FirstOrDefault(x => x.Id == id);
                if (type != null)
                {
                    type.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<ProductTypeViewModel> GetProductTypes()
        {
            var types = (from type in DB.ProductTypes
                         where !type.IsDeleted
                         select new ProductTypeViewModel()
                         {
                             Id = type.Id,
                             Name = type.ProductTypeName,
                             Description = type.Description,
                             BinCode = type.BinCode,
                             CreatedOn = type.CreatedOn,
                             UpdatedOn = type.UpdatedOn
                         });

            return types;
        }

        #endregion
    }
}