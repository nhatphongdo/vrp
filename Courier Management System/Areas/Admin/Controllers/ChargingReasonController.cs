﻿using Courier_Management_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    public class ChargingReasonController : Controller
    {
        // GET: Admin/ChargingReason
        public ActionResult Index()
        {
            return View();
        }

        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        private const string DatabaseDataContextKey = "CharginReasonsDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    var context = new CourierDBEntities();
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = context;
                }
                return (CourierDBEntities)System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        public static List<ChargingReasonViewModel> GetChargingReasons()
        {
            return (from reason in DB.ChargingReasons
                    select new ChargingReasonViewModel()
                    {
                        Id = reason.Id,
                        Description = reason.Description,
                        PaymentCategory = reason.PaymentCategory
                    }).ToList();
        }

        #endregion
    }
}