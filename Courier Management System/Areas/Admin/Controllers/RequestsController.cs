﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web;
using DevExpress.Web.Mvc;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class RequestsController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        private const string DatabaseDataContextKey = "RequestsDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowRePickupRequests)]
        public ActionResult Repickup()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowRePickupRequests + "," + AccountHelper.OrdersEditRePickupRequest)]
        public ActionResult RepickupViewPartial(long? id, string command)
        {
            if (id.HasValue && !string.IsNullOrEmpty(command) && command.ToLower() == "apply")
            {
                if (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.OrdersEditRePickupRequest))
                {
                    var request = db.ExtraRequests.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                    if (request != null && request.RequestDate.HasValue)
                    {
                        var order = db.Orders.FirstOrDefault(x => !x.IsDeleted && x.Id == request.OrderId);
                        if (order != null && order.Status == (int) OrderStatuses.PickupFailed && request.RequestDate < order.DeliveryDate)
                        {
                            var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == order.ServiceId);
                            if (service != null)
                            {
                                //var pickupTimeIds = service.AvailablePickupTimeSlots.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                //var pickupTimeslots = db.TimeSlots.Where(x => !x.IsDeleted && pickupTimeIds.Contains(x.Id.ToString()))
                                //                        .OrderBy(x => x.FromTime)
                                //                        .ToList();

                                //// Find proper timeslot
                                //var timeslotId = 0;
                                //foreach (var time in pickupTimeslots)
                                //{
                                //    if (time.FromTime.TimeOfDay <= request.RequestDate.Value.TimeOfDay && time.ToTime.TimeOfDay >= request.RequestDate.Value.TimeOfDay)
                                //    {
                                //        timeslotId = time.Id;
                                //    }
                                //}
                                //if (timeslotId != 0)
                                {
                                    //order.PickupTimeSlotId = timeslotId;
                                    order.PickupDate = request.RequestDate.Value.Date;
                                    order.Status = (int) OrderStatuses.OrderConfirmed;
                                    order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                                    order.UpdatedOn = DateTime.Now;
                                    order.BinCode = OrderHelper.GenerateBinCode(order);

                                    // Save to history
                                    var track = new OrderTracking
                                                {
                                                    ConsignmentNumber = order.ConsignmentNumber,
                                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                    CreatedOn = DateTime.Now,
                                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                                    PickupDate = order.PickupDate,
                                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                                    DeliveryDate = order.DeliveryDate,
                                                    Status = (int) OrderStatuses.RePickup,
                                                    BinCode = order.BinCode
                                                };
                                    db.OrderTrackings.Add(track);

                                    try
                                    {
                                        db.SaveChanges();
                                    }
                                    catch (Exception exc)
                                    {
                                        // Rollback
                                        ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                                        LogHelper.Log(exc);
                                    }
                                }
                                //else
                                //{
                                //    ViewBag.ErrorMessage = "Cannot find proper Time Slot of this Request Time";
                                //}
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "Order has invalid status or Order's Delivery Date is sooner than Request Date.";
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "No Request Date to apply";
                    }
                }
            }
            return PartialView("_RepickupViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersDeleteRePickupRequest)]
        public ActionResult RepickupViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var request = db.ExtraRequests.FirstOrDefault(x => x.Id == id);
                if (request != null)
                {
                    request.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_RepickupViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersExportRePickupRequests)]
        [HttpPost]
        public ActionResult RepickupExport(string type)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetGridViewExportSettings(), GetPickupRequests().ToList(), "PickupRequests-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetGridViewExportSettings(), GetPickupRequests().ToList(), "PickupRequests-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetGridViewExportSettings(), GetPickupRequests().ToList(), "PickupRequests-" + DateTime.Now.ToString("yyyyMMdd"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewOrders";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "OrderId";
            settings.Columns.Add("ConsignmentNumber");
            settings.Columns.Add("BinCode");
            settings.Columns.Add("FailedPickupDate");
            settings.Columns.Add("RequestDate");
            settings.Columns.Add("PickupRequest");
            settings.Columns.Add("PickupDate").Caption = "Rescheduled Date";
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "PickupTimeSlotId";
                                     column.Caption = "Rescheduled Time Slot";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = TimeSlotsController.GetTimeSlots().ToList().Select(x => new
                                                                                                                             {
                                                                                                                                 Id = x.Id,
                                                                                                                                 Name = x.Name,
                                                                                                                                 FromTime = x.FromTime.ToString("HH:mm"),
                                                                                                                                 ToTime = x.ToTime.ToString("HH:mm")
                                                                                                                             }).ToList();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.TextFormatString = "{0} ({1} - {2})";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                     comboBoxProperties.Columns.Add("Name", "Time Slot Name", Unit.Pixel(200));
                                     comboBoxProperties.Columns.Add("FromTime");
                                     comboBoxProperties.Columns.Add("ToTime");
                                 });
            settings.Columns.Add("CustomerName");
            settings.Columns.Add("FromName");
            settings.Columns.Add("FromZipCode");
            settings.Columns.Add("FromDistrictCode");
            settings.Columns.Add("FromZoneCode");
            settings.Columns.Add("FromAddress");
            settings.Columns.Add("FromMobilePhone");
            settings.Columns.Add("ToName");
            settings.Columns.Add("ToZipCode");
            settings.Columns.Add("ToDistrictCode");
            settings.Columns.Add("ToZoneCode");
            settings.Columns.Add("ToAddress");
            settings.Columns.Add("ToMobilePhone");
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "ServiceId";
                                     column.Caption = "Service";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetServices();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "ProductTypeId";
                                     column.Caption = "Product Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetProductTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "SizeId";
                                     column.Caption = "Size";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetSizes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "PaymentTypeId";
                                     column.Caption = "Payment Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetPaymentTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            var col = settings.Columns.Add("Price");
            col.PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowReDeliveryRequests)]
        public ActionResult Redelivery()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowReDeliveryRequests + "," + AccountHelper.OrdersEditReDeliveryRequest)]
        public ActionResult RedeliveryViewPartial(long? id, string command)
        {
            if (id.HasValue && !string.IsNullOrEmpty(command) && command.ToLower() == "apply")
            {
                if (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.OrdersEditReDeliveryRequest))
                {
                    var request = db.ExtraRequests.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                    if (request != null && request.RequestDate.HasValue)
                    {
                        var order = db.Orders.FirstOrDefault(x => !x.IsDeleted && x.Id == request.OrderId);
                        if (order != null && order.Status == (int) OrderStatuses.DeliveryFailed)
                        {
                            var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == order.ServiceId);
                            if (service != null)
                            {
                                //var deliveryTimeIds = service.AvailableDeliveryTimeSlots.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                //var deliveryTimeslots = db.TimeSlots.Where(x => !x.IsDeleted && deliveryTimeIds.Contains(x.Id.ToString()))
                                //                          .OrderBy(x => x.FromTime)
                                //                          .ToList();

                                //// Find proper timeslot
                                //var timeslotId = 0;
                                //foreach (var time in deliveryTimeslots)
                                //{
                                //    if (time.FromTime.TimeOfDay <= request.RequestDate.Value.TimeOfDay && time.ToTime.TimeOfDay >= request.RequestDate.Value.TimeOfDay)
                                //    {
                                //        timeslotId = time.Id;
                                //    }
                                //}
                                //if (timeslotId != 0)
                                {
                                    //order.DeliveryTimeSlotId = timeslotId;
                                    order.DeliveryDate = request.RequestDate.Value.Date;
                                    order.Status = (int) OrderStatuses.ItemInDepot;
                                    order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                                    order.UpdatedOn = DateTime.Now;
                                    order.BinCode = OrderHelper.GenerateBinCode(order);

                                    // Save to history
                                    var track = new OrderTracking
                                                {
                                                    ConsignmentNumber = order.ConsignmentNumber,
                                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                    CreatedOn = DateTime.Now,
                                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                                    PickupDate = order.PickupDate,
                                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                                    DeliveryDate = order.DeliveryDate,
                                                    Status = (int) OrderStatuses.ReDelivery,
                                                    BinCode = order.BinCode
                                                };
                                    db.OrderTrackings.Add(track);

                                    try
                                    {
                                        db.SaveChanges();

                                        // Send notification
                                        OrderHelper.SendOrderRedeliverySms(Url, order);
                                    }
                                    catch (Exception exc)
                                    {
                                        // Rollback
                                        ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                                        LogHelper.Log(exc);
                                    }
                                }
                                //else
                                //{
                                //    ViewBag.ErrorMessage = "Cannot find proper Time Slot of this Request Time.";
                                //}
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "Order has invalid status.";
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "No Request Date to apply";
                    }
                }
            }
            return PartialView("_RedeliveryViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersDeleteReDeliveryRequest)]
        public ActionResult RedeliveryViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var request = db.ExtraRequests.FirstOrDefault(x => x.Id == id);
                if (request != null)
                {
                    request.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_RedeliveryViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersExportReDeliveryRequests)]
        [HttpPost]
        public ActionResult RedeliveryExport(string type)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetDeliveryGridViewExportSettings(), GetDeliveryRequests().ToList(), "DeliveryRequests-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetDeliveryGridViewExportSettings(), GetDeliveryRequests().ToList(), "DeliveryRequests-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetDeliveryGridViewExportSettings(), GetDeliveryRequests().ToList(), "DeliveryRequests-" + DateTime.Now.ToString("yyyyMMdd"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetDeliveryGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewOrders";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "OrderId";
            settings.Columns.Add("ConsignmentNumber");
            settings.Columns.Add("BinCode");
            settings.Columns.Add("FailedDeliveryDate");
            settings.Columns.Add("RequestDate");
            settings.Columns.Add("DeliveryRequest");
            settings.Columns.Add("DeliveryDate").Caption = "Rescheduled Date";
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "DeliveryTimeSlotId";
                                     column.Caption = "Rescheduled Time Slot";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = TimeSlotsController.GetTimeSlots().ToList().Select(x => new
                                                                                                                             {
                                                                                                                                 Id = x.Id,
                                                                                                                                 Name = x.Name,
                                                                                                                                 FromTime = x.FromTime.ToString("HH:mm"),
                                                                                                                                 ToTime = x.ToTime.ToString("HH:mm")
                                                                                                                             }).ToList();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.TextFormatString = "{0} ({1} - {2})";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                     comboBoxProperties.Columns.Add("Name", "Time Slot Name", Unit.Pixel(200));
                                     comboBoxProperties.Columns.Add("FromTime");
                                     comboBoxProperties.Columns.Add("ToTime");
                                 });
            settings.Columns.Add("CustomerName");
            settings.Columns.Add("FromName");
            settings.Columns.Add("FromZipCode");
            settings.Columns.Add("FromDistrictCode");
            settings.Columns.Add("FromZoneCode");
            settings.Columns.Add("FromAddress");
            settings.Columns.Add("FromMobilePhone");
            settings.Columns.Add("ToName");
            settings.Columns.Add("ToZipCode");
            settings.Columns.Add("ToDistrictCode");
            settings.Columns.Add("ToZoneCode");
            settings.Columns.Add("ToAddress");
            settings.Columns.Add("ToMobilePhone");
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "ServiceId";
                                     column.Caption = "Service";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetServices();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "ProductTypeId";
                                     column.Caption = "Product Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetProductTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "SizeId";
                                     column.Caption = "Size";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetSizes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(column =>
                                 {
                                     column.FieldName = "PaymentTypeId";
                                     column.Caption = "Payment Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetPaymentTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            var col = settings.Columns.Add("Price");
            col.PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        public static IQueryable<RequestViewModel> GetPickupRequests()
        {
            var requests = from request in DB.ExtraRequests
                           join zipCode in DB.ZipCodes on request.Order.FromZipCode equals zipCode.PostalCode into fromGroup
                           join zipCode in DB.ZipCodes on request.Order.ToZipCode equals zipCode.PostalCode into toGroup
                           where !request.IsDeleted && request.RequestType == (int) ExtraRequestType.RePickupRequest
                           select new RequestViewModel()
                                  {
                                      Id = request.Id,
                                      Status = (OrderStatuses) request.Order.Status,
                                      OrderId = request.OrderId,
                                      ConsignmentNumber = request.Order.ConsignmentNumber,
                                      BinCode = request.Order.BinCode,
                                      FailedPickupDate = request.Order.CollectedOn,
                                      PickupRequest = request.RequestContent,
                                      OperatorRemark = request.Order.OperatorRemark,
                                      RequestDate = request.RequestDate,
                                      PickupDate = request.Order.Status == (int) OrderStatuses.OrderConfirmed
                                                       ? request.Order.PickupDate
                                                       : null,
                                      PickupTimeSlotId = request.Order.Status == (int) OrderStatuses.OrderConfirmed
                                                             ? request.Order.PickupTimeSlotId
                                                             : null,
                                      CustomerCompany = request.Order.Customer.CustomerCompany,
                                      FromName = request.Order.FromName,
                                      FromAddress = request.Order.FromAddress,
                                      FromZipCode = request.Order.FromZipCode,
                                      FromMobilePhone = request.Order.FromMobilePhone,
                                      FromDistrictCode = fromGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                      FromZoneCode = fromGroup.Select(x => x.Zone).FirstOrDefault(),
                                      ToName = request.Order.ToName,
                                      ToAddress = request.Order.ToAddress,
                                      ToZipCode = request.Order.ToZipCode,
                                      ToDistrictCode = toGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                      ToZoneCode = toGroup.Select(x => x.Zone).FirstOrDefault(),
                                      ToMobilePhone = request.Order.ToMobilePhone,
                                      DeliveryDate = request.Order.DeliveryDate,
                                      DeliveryTimeSlotId = request.Order.DeliveryTimeSlotId,
                                      ServiceId = request.Order.ServiceId,
                                      PaymentTypeId = request.Order.PaymentTypeId,
                                      ProductTypeId = request.Order.ProductTypeId,
                                      Price = request.Order.Price,
                                      PromoCode = request.Order.PromoCode,
                                      CreatedOn = request.CreatedOn
                                  };

            return requests;
        }

        public static IQueryable<RequestViewModel> GetDeliveryRequests()
        {
            var requests = from request in DB.ExtraRequests
                           join zipCode in DB.ZipCodes on request.Order.FromZipCode equals zipCode.PostalCode into fromGroup
                           join zipCode in DB.ZipCodes on request.Order.ToZipCode equals zipCode.PostalCode into toGroup
                           where !request.IsDeleted && request.RequestType == (int) ExtraRequestType.ReDeliveryRequest
                           select new RequestViewModel()
                                  {
                                      Id = request.Id,
                                      OrderId = request.OrderId,
                                      Status = (OrderStatuses) request.Order.Status,
                                      ConsignmentNumber = request.Order.ConsignmentNumber,
                                      BinCode = request.Order.BinCode,
                                      FailedDeliveryDate = request.Order.DeliveredOn,
                                      DeliveryRequest = request.RequestContent,
                                      OperatorRemark = request.Order.OperatorRemark,
                                      RequestDate = request.RequestDate,
                                      DeliveryDate = request.Order.Status == (int) OrderStatuses.ItemInDepot
                                                         ? request.Order.DeliveryDate
                                                         : null,
                                      DeliveryTimeSlotId = request.Order.Status == (int) OrderStatuses.ItemInDepot
                                                               ? request.Order.DeliveryTimeSlotId
                                                               : null,
                                      CustomerCompany = request.Order.Customer.CustomerCompany,
                                      FromName = request.Order.FromName,
                                      FromAddress = request.Order.FromAddress,
                                      FromZipCode = request.Order.FromZipCode,
                                      FromMobilePhone = request.Order.FromMobilePhone,
                                      FromDistrictCode = fromGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                      FromZoneCode = fromGroup.Select(x => x.Zone).FirstOrDefault(),
                                      ToName = request.Order.ToName,
                                      ToAddress = request.Order.ToAddress,
                                      ToZipCode = request.Order.ToZipCode,
                                      ToDistrictCode = toGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                      ToZoneCode = toGroup.Select(x => x.Zone).FirstOrDefault(),
                                      ToMobilePhone = request.Order.ToMobilePhone,
                                      PickupDate = request.Order.PickupDate,
                                      PickupTimeSlotId = request.Order.PickupTimeSlotId,
                                      ServiceId = request.Order.ServiceId,
                                      PaymentTypeId = request.Order.PaymentTypeId,
                                      ProductTypeId = request.Order.ProductTypeId,
                                      Price = request.Order.Price,
                                      PromoCode = request.Order.PromoCode,
                                      CreatedOn = request.CreatedOn
                                  };

            return requests;
        }
    }
}