﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class TimeSlotsController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "TimeSlotsDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowTimeSlots)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowTimeSlots)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddTimeSlot)]
        public ActionResult Add()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddTimeSlot)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditTimeSlotViewModel model)
        {
            if (ModelState.IsValid)
            {
                var time = new TimeSlot()
                           {
                               TimeSlotName = model.Name,
                               BinCode = model.BinCode,
                               FromTime = ConvertHourToDateTime(model.FromTime),
                               ToTime = ConvertHourToDateTime(model.ToTime),
                               IsAllowPreOrder = model.IsAllowPreOrder,
                               IsDeleted = false,
                               CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                               CreatedOn = DateTime.Now,
                               UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                               UpdatedOn = DateTime.Now
                           };

                db.TimeSlots.Add(time);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "TimeSlots");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditTimeSlot)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from time in db.TimeSlots
                         where !time.IsDeleted && time.Id == id
                         select time).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(
                        new AddEditTimeSlotViewModel()
                        {
                            Name = model.TimeSlotName,
                            BinCode = model.BinCode,
                            FromTime = ConvertDateTimeToHour(model.FromTime),
                            ToTime = ConvertDateTimeToHour(model.ToTime),
                            IsAllowPreOrder = model.IsAllowPreOrder
                        });
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditTimeSlot)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditTimeSlotViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var time = db.TimeSlots.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (time == null)
                {
                    return HttpNotFound();
                }

                time.TimeSlotName = model.Name;
                time.BinCode = model.BinCode;
                time.FromTime = ConvertHourToDateTime(model.FromTime);
                time.ToTime = ConvertHourToDateTime(model.ToTime);
                time.IsAllowPreOrder = model.IsAllowPreOrder;
                time.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                time.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "TimeSlots");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsDeleteTimeSlot)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var time = db.TimeSlots.FirstOrDefault(x => x.Id == id);
                if (time != null)
                {
                    time.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static DateTime ConvertHourToDateTime(string input)
        {
            var parts = input.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                var hour = parts.Length > 0 ? int.Parse(parts[0]) : 0;
                var minute = parts.Length > 1 ? int.Parse(parts[1]) : 0;
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hour, minute, 0);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }

        public static string ConvertDateTimeToHour(DateTime input)
        {
            return $"{input.Hour:00}:{input.Minute:00}";
        }

        #region Data queries

        public static IQueryable<TimeSlotViewModel> GetTimeSlots()
        {
            var times = (from time in DB.TimeSlots
                         where !time.IsDeleted
                         select new TimeSlotViewModel()
                                {
                                    Id = time.Id,
                                    BinCode = time.BinCode,
                                    Name = time.TimeSlotName,
                                    FromTime = time.FromTime,
                                    ToTime = time.ToTime,
                                    IsAllowPreOrder = time.IsAllowPreOrder,
                                    CreatedOn = time.CreatedOn,
                                    UpdatedOn = time.UpdatedOn
                                });

            return times;
        }

        #endregion
    }
}
