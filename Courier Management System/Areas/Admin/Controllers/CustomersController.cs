﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using DevExpress.Web.Mvc.Internal;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        #region Fields

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CustomersController()
        {
        }

        public CustomersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "CustomersDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities)System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowCustomers)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowCustomers)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        public ActionResult PhotoUpload()
        {
            var uploadControl = UploadControlExtension.GetUploadedFiles(
                                                                        "uploadControl",
                                                                        PathHelper.UploadSettings,
                                                                        (sender, args) =>
                                                                        {
                                                                            // Check folder
                                                                            if (!Directory.Exists(Server.MapPath(PathHelper.CustomerUploadBaseDirectory)))
                                                                            {
                                                                                Directory.CreateDirectory(Server.MapPath(PathHelper.CustomerUploadBaseDirectory));
                                                                            }

                                                                            var resultFileName = $"{Path.GetRandomFileName()}{Path.GetExtension(args.UploadedFile.FileName)}";
                                                                            var resultFileUrl = PathHelper.CustomerUploadBaseDirectory + resultFileName;
                                                                            var resultFilePath = Server.MapPath(resultFileUrl);
                                                                            args.UploadedFile.SaveAs(resultFilePath);

                                                                            var name = args.UploadedFile.FileName;
                                                                            var url = Url.Content(resultFileUrl);
                                                                            var sizeInKilobytes = args.UploadedFile.ContentLength / 1024;
                                                                            var sizeText = sizeInKilobytes.ToString() + " KB";
                                                                            args.CallbackData = $"{name}|{url}|{sizeText}";
                                                                        });
            return null;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddCustomer)]
        public ActionResult Add()
        {
            ViewBag.CountryCodes = db.Countries.Select(
                                                       x => new SelectListItem()
                                                       {
                                                           Text = x.CountryName + " (" + x.CountryCode + ")",
                                                           Value = x.CountryCode
                                                       }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                   {
                                                                                       Text = x.PaymentTypeName,
                                                                                       Value = x.Id.ToString()
                                                                                   }).ToList();
            ViewBag.PricePlans = db.CustomerPricePlans.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.PlanName,
                                                                                        Value = x.Id.ToString()
                                                                                     }).ToList();
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddCustomer)]
        public async Task<ActionResult> Add([ModelBinder(typeof(DevExpressEditorsBinder))] AddCustomerViewModel model)
        {
            ViewBag.CountryCodes = db.Countries.Select(
                                                       x => new SelectListItem()
                                                       {
                                                           Text = x.CountryName + " (" + x.CountryCode + ")",
                                                           Value = x.CountryCode
                                                       }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                   {
                                                                                       Text = x.PaymentTypeName,
                                                                                       Value = x.Id.ToString()
                                                                                   }).ToList();

            ViewBag.PricePlans = db.CustomerPricePlans.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.PlanName,
                                                                                        Value = x.Id.ToString()
                                                                                     }).ToList();
            if (ModelState.IsValid)
            {
                // Check unique prefix
                if (!string.IsNullOrWhiteSpace(model.CompanyPrefix))
                {
                    var found = db.Customers.Any(x => x.CompanyPrefix.ToLower() == model.CompanyPrefix.ToLower());
                    if (found)
                    {
                        ViewBag.ErrorMessages = new string[] { "Company Prefix already existed. Please choose the other." };
                        return View(model);
                    }
                }

                // Create account
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    // Add user to role
                    result = await UserManager.AddToRoleAsync(user.Id, AccountHelper.CustomerRole);
                    if (!result.Succeeded)
                    {
                        // Rollback
                        result = await UserManager.DeleteAsync(user);
                        ViewBag.ErrorMessages = result.Errors;
                        return View(model);
                    }

                    var customer = new Customer()
                    {
                        UserName = model.UserName,
                        PaymentTypeId = model.PaymentType,
                        Credit = model.Credit,
                        CustomerAddress = model.Address,
                        CustomerCompany = model.Company,
                        CustomerName = model.Name,
                        CustomerCountry = model.CountryCode,
                        CustomerPhone = model.MobileNumber,
                        CustomerZipCode = model.ZipCode,
                        IsActive = true,
                        Salesperson = model.Salesperson,
                        DefaultPromoCode = model.DefaultPromoCode == null ? "" : string.Join(",", model.DefaultPromoCode),
                        PricePlan = model.PricePlan == 0 ? null : (int?)model.PricePlan,
                        Remark = model.Remark,
                        UseGroupPrice = model.UseGroupPrice,
                        CompanyPrefix = model.CompanyPrefix,
                        IsDeleted = false,
                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        CreatedOn = DateTime.Now,
                        UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                        UpdatedOn = DateTime.Now
                    };

                    db.Customers.Add(customer);

                    try
                    {
                        db.SaveChanges();
                        return RedirectToAction("List", "Customers");
                    }
                    catch (Exception exc)
                    {
                        // Rollback
                        result = await UserManager.RemoveFromRoleAsync(user.Id, AccountHelper.CustomerRole);
                        result = await UserManager.DeleteAsync(user);
                        ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                        LogHelper.Log(exc);
                    }
                }
                else
                {
                    ViewBag.ErrorMessages = result.Errors;
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersEditCustomer)]
        public ActionResult Edit(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.CountryCodes = db.Countries.Select(
                                                       x => new SelectListItem()
                                                       {
                                                           Text = x.CountryName + " (" + x.CountryCode + ")",
                                                           Value = x.CountryCode
                                                       }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                   {
                                                                                       Text = x.PaymentTypeName,
                                                                                       Value = x.Id.ToString()
                                                                                   }).ToList();

            ViewBag.PricePlans = db.CustomerPricePlans.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.PlanName,
                                                                                        Value = x.Id.ToString()
                                                                                     }).ToList();

            var model = (from customer in db.Customers
                         from user in db.AspNetUsers
                         where !customer.IsDeleted && customer.Id == id &&
                               user.UserName.Equals(customer.UserName, StringComparison.OrdinalIgnoreCase)
                         select new
                         {
                             Name = customer.CustomerName,
                             UserName = customer.UserName,
                             Address = customer.CustomerAddress,
                             Email = user.Email,
                             PaymentType = customer.PaymentTypeId,
                             CountryCode = customer.CustomerCountry,
                             MobileNumber = customer.CustomerPhone,
                             Company = customer.CustomerCompany,
                             Credit = customer.Credit,
                             IsActive = customer.IsActive,
                             ZipCode = customer.CustomerZipCode,
                             Salesperson = customer.Salesperson,
                             PricePlan = customer.PricePlan,
                             DefaultPromoCode = customer.DefaultPromoCode,
                             Remark = customer.Remark,
                             UseGroupPrice = customer.UseGroupPrice,
                             CompanyPrefix = customer.CompanyPrefix
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(
                        new EditCustomerViewModel()
                        {
                            Name = model.Name,
                            UserName = model.UserName,
                            Address = model.Address,
                            Email = model.Email,
                            PaymentType = model.PaymentType,
                            MobileNumber = model.MobileNumber,
                            CountryCode = model.CountryCode,
                            Company = model.Company,
                            Credit = model.Credit,
                            IsActive = model.IsActive,
                            ZipCode = model.ZipCode,
                            Salesperson = model.Salesperson,
                            PricePlan = model.PricePlan,
                            Remark = model.Remark,
                            DefaultPromoCode = (string.IsNullOrEmpty(model.DefaultPromoCode) ? "" : model.DefaultPromoCode).Split(','),
                            UseGroupPrice = model.UseGroupPrice,
                            CompanyPrefix = model.CompanyPrefix
                        });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersEditCustomer)]
        public async Task<ActionResult> Edit(long? id, [ModelBinder(typeof(DevExpressEditorsBinder))] EditCustomerViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.CountryCodes = db.Countries.Select(
                                                       x => new SelectListItem()
                                                       {
                                                           Text = x.CountryName + " (" + x.CountryCode + ")",
                                                           Value = x.CountryCode
                                                       }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                   {
                                                                                       Text = x.PaymentTypeName,
                                                                                       Value = x.Id.ToString()
                                                                                   }).ToList();

            if (ModelState.IsValid)
            {
                // Check unique prefix
                if (!string.IsNullOrWhiteSpace(model.CompanyPrefix))
                {
                    var found = db.Customers.Any(x => x.Id != id && x.CompanyPrefix.ToLower() == model.CompanyPrefix.ToLower());
                    if (found)
                    {
                        ViewBag.ErrorMessages = new string[] { "Company Prefix already existed. Please choose the other." };
                        return View(model);
                    }
                }

                // Create account
                // Update email and password
                IdentityResult result;
                var user = await UserManager.FindByNameAsync(model.UserName);
                if (user == null)
                {
                    ViewBag.ErrorMessages = new string[] { $"Cannot find user with this User Name '{model.UserName}'." };
                    return View(model);
                }

                user.Email = model.Email;
                result = await UserManager.UpdateAsync(user);
                if (!result.Succeeded)
                {
                    ViewBag.ErrorMessages = result.Errors;
                    return View(model);
                }

                if (!string.IsNullOrEmpty(model.CurrentPassword) && !string.IsNullOrEmpty(model.NewPassword))
                {
                    result = await UserManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                    if (!result.Succeeded)
                    {
                        ViewBag.ErrorMessages = result.Errors;
                        return View(model);
                    }
                }

                var customer = db.Customers.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (customer == null)
                {
                    return HttpNotFound();
                }

                customer.UserName = model.UserName;
                customer.PaymentTypeId = model.PaymentType;
                customer.Credit = model.Credit;
                customer.CustomerAddress = model.Address;
                customer.CustomerCompany = model.Company;
                customer.CustomerName = model.Name;
                customer.CustomerCountry = model.CountryCode;
                customer.CustomerPhone = model.MobileNumber;
                customer.CustomerZipCode = model.ZipCode;
                customer.IsActive = model.IsActive;
                customer.DefaultPromoCode = model.DefaultPromoCode == null ? "" : string.Join(",", model.DefaultPromoCode);
                customer.PricePlan = model.PricePlan == 0 ? null : (int?)model.PricePlan;
                customer.Salesperson = model.Salesperson;
                customer.Remark = model.Remark;
                customer.UseGroupPrice = model.UseGroupPrice;
                customer.CompanyPrefix = model.CompanyPrefix;
                customer.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                customer.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Customers");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersDeleteCustomer)]
        public ActionResult ListViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var customer = db.Customers.FirstOrDefault(x => x.Id == id);
                if (customer != null)
                {
                    customer.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        [HttpPost]
        public ActionResult FindSalesPersons(string q, int? page)
        {
            if (!page.HasValue)
            {
                page = 1;
            }

            q = q.ToLower();
            var list = (db.AspNetUsers
                          .Where(x => x.UserName.ToLower().Contains(q) || x.Email.ToLower().Contains(q))
                          .OrderBy(x => x.UserName)
                          .Skip((page.Value - 1) * 10).Take(10)
                          .Select(
                                  x => new
                                  {
                                      x.UserName,
                                      x.Email,
                                      id = x.UserName
                                  })
                          .ToList());

            return Json(list);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            base.Dispose(disposing);
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersExportCustomers)]
        public ActionResult Export(string type)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetGridViewExportSettings(), GetCustomers().ToList(), "Customers-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetGridViewExportSettings(), GetCustomers().ToList(), "Customers-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetGridViewExportSettings(), GetCustomers().ToList(), "Customers-" + DateTime.Now.ToString("yyyyMMdd"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewCustomers";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add("Name");
            settings.Columns.Add("Company");
            settings.Columns.Add("CompanyPrefix");
            settings.Columns.Add("ZipCode");
            settings.Columns.Add("Address");
            settings.Columns.Add("CountryCode");
            settings.Columns.Add("MobileNumber");
            settings.Columns.Add("UserName");
            settings.Columns.Add("Email");
            var column = settings.Columns.Add("PaymentType", MVCxGridViewColumnType.ComboBox);
            var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = GetPaymentTypes();
            comboBoxProperties.TextField = "Name";
            comboBoxProperties.ValueField = "Id";
            comboBoxProperties.ValueType = typeof(int);
            settings.Columns.Add("Credit").PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("Salesperson");
            settings.Columns.Add("Remark");
            settings.Columns.Add("UseGroupPrice", MVCxGridViewColumnType.CheckBox);
            settings.Columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowPaymentStatus)]
        public ActionResult PaymentStatus()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowPaymentStatus + "," + AccountHelper.CustomersEditPaymentStatus)]
        public ActionResult PaymentStatusListViewPartial(long? id, string command, DateTime? start, DateTime? end, DateTime? startDate, DateTime? endDate)
        {
            ViewBag.StartDate = start ?? startDate;
            ViewBag.EndDate = end ?? endDate;

            if (id.HasValue && (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.CustomersEditPaymentStatus)))
            {
                var payment = db.TopupPayments.FirstOrDefault(x => x.Id == id);
                if (payment != null)
                {
                    var customer = db.Customers.FirstOrDefault(x => x.UserName.ToLower() == payment.Payer.ToLower() && !x.IsDeleted && x.IsActive);
                    if (customer == null)
                    {
                        ViewBag.ErrorMessage = "Cannot find corresponding Customer of this topup payment.";
                        return PartialView("_PaymentStatusListViewPartial");
                    }

                    switch (command.ToLower())
                    {
                        case "approve":
                            payment.Status = (int)Helpers.PaymentStatus.Approved;
                            payment.ApprovedOn = DateTime.Now;
                            payment.ApprovedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            payment.IsSuccessful = true;
                            break;
                        case "decline":
                            payment.Status = (int)Helpers.PaymentStatus.Declined;
                            payment.ApprovedOn = DateTime.Now;
                            payment.ApprovedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            payment.IsSuccessful = false;
                            break;
                        default:
                            break;
                    }

                    // Topup
                    if (payment.Status == (int)Helpers.PaymentStatus.Approved)
                    {
                        customer.Credit += payment.TransferAmount;
                        customer.UpdatedOn = DateTime.Now;
                        customer.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    }

                    try
                    {
                        db.SaveChanges();

                        // Send email
                        var emailTemplate = string.Join(
                                                        "\n",
                                                        System.IO.File.ReadAllLines(
                                                                                    Server.MapPath(
                                                                                                   payment.Status == (int)Helpers.PaymentStatus.Approved
                                                                                                       ? "~/Templates/email_payment_approve.html"
                                                                                                       : "~/Templates/email_payment_decline.html"),
                                                                                    Encoding.UTF8));


                        var result = AccountHelper.SendEmailSync(payment.Payer, "Topup", emailTemplate);
                    }
                    catch (Exception exc)
                    {
                        LogHelper.Log(exc);
                    }
                }
            }

            return PartialView("_PaymentStatusListViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersExportPaymentStatus)]
        public ActionResult ExportPaymentStatus(string type, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetPaymentStatusGridViewExportSettings(), GetPaymentStatuses(startDate, endDate).ToList(), "TopupPayments-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetPaymentStatusGridViewExportSettings(), GetPaymentStatuses(startDate, endDate).ToList(), "TopupPayments-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetPaymentStatusGridViewExportSettings(), GetPaymentStatuses(startDate, endDate).ToList(), "TopupPayments-" + DateTime.Now.ToString("yyyyMMdd"));
                default:
                    return new EmptyResult();
            }
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowRefundStatus)]
        public ActionResult RefundOrExtraCharge()
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = startDate.AddMonths(1).AddDays(-1);
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowRefundStatus + "," + AccountHelper.CustomersEditRefundStatus)]
        public ActionResult RefundOrExtraChargeListViewPartial(long? id, string command, int? paymentCategory, DateTime? startDate, DateTime? endDate)
        {
            DateTime now = DateTime.Now;
            startDate = startDate ?? new DateTime(now.Year, now.Month, 1);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate ?? startDate?.AddMonths(1).AddDays(-1);

            if (id.HasValue && (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.CustomersEditRefundStatus)))
            {
                var refundOrExtraChargePayment = db.RefundOrExtraCharges.FirstOrDefault(x => x.Id == id);
                if (refundOrExtraChargePayment != null)
                {
                    var customer = db.Customers.FirstOrDefault(x => x.Id == refundOrExtraChargePayment.CustomerId && !x.IsDeleted && x.IsActive);
                    if (customer == null)
                    {
                        ViewBag.ErrorMessage = "Cannot find corresponding customer of this payment.";
                        return PartialView("_RefundOrExtraChargeListViewPartial");
                    }

                    switch (command.ToLower())
                    {
                        case "approve":
                            refundOrExtraChargePayment.ApprovalStatus = (int)Helpers.PaymentStatus.Approved;
                            refundOrExtraChargePayment.UpdatedOn = DateTime.Now;
                            refundOrExtraChargePayment.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            break;
                        case "decline":
                            refundOrExtraChargePayment.ApprovalStatus = (int)Helpers.PaymentStatus.Declined;
                            refundOrExtraChargePayment.UpdatedOn = DateTime.Now;
                            refundOrExtraChargePayment.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            break;
                        default:
                            break;
                    }

                    // Refund
                    if (refundOrExtraChargePayment.ApprovalStatus == (int)Helpers.PaymentStatus.Approved)
                    {
                        if (paymentCategory == (int)PaymentCategory.Refund)
                            customer.Credit += refundOrExtraChargePayment.Amount;
                        else if (paymentCategory == (int)PaymentCategory.ExtraCharge)
                            customer.Credit -= refundOrExtraChargePayment.Amount;

                        customer.UpdatedOn = DateTime.Now;
                        customer.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    }

                    try
                    {
                        db.SaveChanges();

                        // Send email
                        var emailTemplate = "";
                        if (paymentCategory == (int)PaymentCategory.Refund)
                        {
                            emailTemplate = string.Join(
                                                        "\n",
                                                        System.IO.File.ReadAllLines(
                                                                                    Server.MapPath(
                                                                                                   refundOrExtraChargePayment.ApprovalStatus == (int)Helpers.PaymentStatus.Approved
                                                                                                       ? "~/Templates/email_refund_approve.html"
                                                                                                       : "~/Templates/email_refund_decline.html"),
                                                                                    Encoding.UTF8));
                            AccountHelper.SendEmailSync(refundOrExtraChargePayment.Customer.UserName, "Refund", emailTemplate);
                        }
                        else if (paymentCategory == (int)PaymentCategory.ExtraCharge && refundOrExtraChargePayment.ApprovalStatus == (int)Helpers.PaymentStatus.Approved)
                        {
                            emailTemplate = string.Join(
                                                        "\n",
                                                        System.IO.File.ReadAllLines(
                                                                                    Server.MapPath("~/Templates/email_extracharge_approve.html"),
                                                                                    Encoding.UTF8));
                            emailTemplate = string.Format(emailTemplate, refundOrExtraChargePayment.ConsignmentNumber, refundOrExtraChargePayment.ChargingReason.Description);
                            AccountHelper.SendEmailSync(refundOrExtraChargePayment.Customer.UserName, "Extra charge", emailTemplate);
                        }
                    }
                    catch (Exception exc)
                    {
                        LogHelper.Log(exc);
                    }
                }
            }

            return PartialView("_RefundOrExtraChargeListViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersExportRefundStatus)]
        public ActionResult ExportRefundOrExtraCharge(string type, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetRefundStatusGridViewExportSettings(), GetRefundOrExtraChargeStatuses(startDate, endDate).ToList(), "RefundOrExtraCharge-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetRefundStatusGridViewExportSettings(), GetRefundOrExtraChargeStatuses(startDate, endDate).ToList(), "RefundOrExtraCharge-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetRefundStatusGridViewExportSettings(), GetRefundOrExtraChargeStatuses(startDate, endDate).ToList(), "RefundOrExtraCharge-" + DateTime.Now.ToString("yyyyMMdd"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetRefundStatusGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewRefundOrExtraCharge";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add("CompanyName");

            var column = settings.Columns.Add("PaymentCategory", MVCxGridViewColumnType.ComboBox);
            column.Caption = "Refund or Extra charge";
            var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = EnumHelpers.ConvertEnumToSelectList<PaymentCategory>();
            comboBoxProperties.TextField = "Text";
            comboBoxProperties.ValueField = "Value";
            comboBoxProperties.ValueType = typeof(int);

            column = settings.Columns.Add("Reason", MVCxGridViewColumnType.ComboBox);
            column.Caption = "Reason";
            comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = ChargingReasonController.GetChargingReasons();
            comboBoxProperties.TextField = "Description";
            comboBoxProperties.ValueField = "Id";
            comboBoxProperties.ValueType = typeof(int);

            settings.Columns.Add("UserName");
            settings.Columns.Add("CustomerName");
            settings.Columns.Add("SalesPerson");
            settings.Columns.Add("ConsignmentNumber");
            settings.Columns.Add("Amount").PropertiesEdit.DisplayFormatString = "c";

            column = settings.Columns.Add("ApprovalStatus", MVCxGridViewColumnType.ComboBox);
            column.Caption = "Approval Status";
            comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = EnumHelpers.ConvertEnumToSelectList<PaymentStatus>();
            comboBoxProperties.TextField = "Text";
            comboBoxProperties.ValueField = "Value";
            comboBoxProperties.ValueType = typeof(int);

            settings.Columns.Add("CreatedOn", "Request CreatedOn");
            settings.Columns.Add("CreatedBy", "Request CreatedBy");
            settings.Columns.Add("UpdatedOn", "Status UpdatedOn");
            settings.Columns.Add("UpdatedBy", "Status UpdatedBy");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddRefund)]
        public ActionResult AddRefundOrExtraCharge()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddRefund)]
        public ActionResult AddRefundOrExtraCharge([ModelBinder(typeof(DevExpressEditorsBinder))] AddRefundViewModel model)
        {
            ViewBag.ChargingReasons = GetRefundOrExtraChargeReasonsAsList(((int)model.PaymentCategory).ToString());
            model.Customer = db.Customers.Where(c => c.Id == model.CustomerId).FirstOrDefault();
            if (ModelState.IsValid)
            {
                // Check unique refund
                var found = db.RefundOrExtraCharges.Any(x => x.CustomerId == model.CustomerId && x.ConsignmentNumber == model.ConsignmentNumber && x.PaymentCategory == (int)model.PaymentCategory);
                if (found)
                {
                    ViewBag.ErrorMessages = new string[] { "Refund/Extra charge already done for this customer !." };
                    return View(model);
                }

                // Check if consignment number belongs to selected customer
                var isConsignmentNoBelongstoSelectedCustomer = DB.Orders.Any(x => !x.IsDeleted && x.CustomerId == model.CustomerId && x.ConsignmentNumber == model.ConsignmentNumber);
                if (!isConsignmentNoBelongstoSelectedCustomer)
                {
                    ViewBag.ErrorMessages = new string[] { "Could not find consignment number corresponding to selected customer !." };
                    return View(model);
                }

                if (!db.Orders.Any(o => o.ConsignmentNumber.ToLower() == model.ConsignmentNumber.ToLower()))
                {
                    // Not Found
                    ViewBag.ErrorMessages = new string[] { "Consignment Number (" + model.ConsignmentNumber + ") is invalid." };
                    return View(model);
                }

                var refund = new RefundOrExtraCharge()
                {
                    ApprovalStatus = (int)Helpers.PaymentStatus.Pending,
                    ConsignmentNumber = model.ConsignmentNumber,
                    CustomerId = model.CustomerId,
                    Amount = model.Amount,
                    Reason = model.Reason,
                    PaymentCategory = (int)model.PaymentCategory,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };
                db.RefundOrExtraCharges.Add(refund);

                try
                {
                    db.SaveChanges();
                    ViewBag.SuccessMessage = "Refund/Extra charge added successfully.";
                    ViewBag.ChargingReasons = null;
                    ModelState.Clear();
                    return View();
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddRefund)]
        public ActionResult AddRefundOrExtraChargeBulk()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddRefund)]
        [MultipleButton(Name = "action", Argument = "AddRefundOrExtraChargeBulk")]
        public ActionResult AddRefundOrExtraChargeBulk([ModelBinder(typeof(DevExpressEditorsBinder))] AddRefundOrExtraChargeBulk model)
        {
            ViewBag.ChargingReasons = GetRefundOrExtraChargeReasonsAsList(((int)model.PaymentCategory).ToString());
            model.Customer = db.Customers.Where(c => c.Id == model.CustomerId).FirstOrDefault();
            if (ModelState.IsValid)
            {
                // Validate Consignment Numbers
                var arrWorkings = (model.ConsignmentNumbers ?? "").Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None).Select(s => s.Trim()).ToList();
                var arrConsignNumbers = new List<string>();

                foreach (var singleLine in arrWorkings)
                {
                    if (string.IsNullOrEmpty(singleLine.Trim()))
                    {
                        continue;
                    }

                    var arrSingle = singleLine.Split('|').Select(s => s.Trim()).ToList();
                    foreach (var item in arrSingle)
                    {
                        if (string.IsNullOrEmpty(item.Trim()))
                        {
                            continue;
                        }
                        arrConsignNumbers.Add(item);
                    }
                }

                var errors = new List<string>();
                ViewBag.ErrorMessages = errors;

                foreach (var singleConsignNumber in arrConsignNumbers)
                {
                    if (!DB.Orders.Any(o => o.ConsignmentNumber.ToLower() == singleConsignNumber.ToLower()))
                    {
                        // Not Found
                        errors.Add("Consignment Number (" + singleConsignNumber + ") is invalid.");
                    }
                    if (!DB.Orders.Any(x => !x.IsDeleted && x.CustomerId == model.CustomerId && x.ConsignmentNumber == singleConsignNumber))
                    {
                        // Not Found
                        errors.Add("Could not find consignment number corresponding to selected customer !. (" + singleConsignNumber + ") ");
                    }
                }

                if (errors.Count > 0)
                {
                    // Not valid consignment numbers found
                    HtmlHelper.ClientValidationEnabled = false;
                    return View(model);
                }
                bool copyModelAmount = false;
                if (model.Amount.HasValue && model.Amount != 0)
                {
                    copyModelAmount = true;
                }
                var orders = db.Orders.Where(o => arrConsignNumbers.Contains(o.ConsignmentNumber));
                var bulkData = new List<AddRefundViewModel>();
                foreach (var item in arrConsignNumbers)
                {
                    var singleRefundOrExtraCharge = new AddRefundViewModel();
                    singleRefundOrExtraCharge.CustomerId = model.CustomerId;
                    singleRefundOrExtraCharge.Amount = copyModelAmount ? model.Amount.Value : orders.Where(o => o.ConsignmentNumber == item).FirstOrDefault().Price;
                    singleRefundOrExtraCharge.ExistingPrice = orders.Where(o => o.ConsignmentNumber == item).FirstOrDefault().Price;
                    singleRefundOrExtraCharge.Reason = model.Reason;
                    singleRefundOrExtraCharge.ConsignmentNumber = item;
                    singleRefundOrExtraCharge.PaymentCategory = model.PaymentCategory;
                    bulkData.Add(singleRefundOrExtraCharge);
                }
                model.BulkRefundOrExtraCharges = bulkData;
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddRefund)]
        [MultipleButton(Name = "action", Argument = "SubmitRefundOrExtraChargeBulk")]
        public ActionResult SubmitRefundOrExtraChargeBulk([ModelBinder(typeof(DevExpressEditorsBinder))] AddRefundOrExtraChargeBulk model)
        {
            List<string> errors = new List<string>();
            try
            {
                ViewBag.ChargingReasons = GetRefundOrExtraChargeReasonsAsList(((int)model.PaymentCategory).ToString());
                model.Customer = db.Customers.Where(c => c.Id == model.CustomerId).FirstOrDefault();
                //Entity Framework bulk insertion
                //https://code.msdn.microsoft.com/Entity-Framework-Batch-994cd739
                List<RefundOrExtraCharge> bulkRefundOrExtraCharge = new List<RefundOrExtraCharge>();
                foreach (var item in model.BulkRefundOrExtraCharges)
                {
                    // Check unique refund
                    var found = DB.RefundOrExtraCharges.Any(x => x.CustomerId == model.CustomerId && x.ConsignmentNumber == item.ConsignmentNumber && x.PaymentCategory == (int)model.PaymentCategory);
                    // Check if consignment number belongs to selected customer
                    var isConsignmentNoBelongstoSelectedCustomer = DB.Orders.Any(x => !x.IsDeleted && x.CustomerId == model.CustomerId && x.ConsignmentNumber == item.ConsignmentNumber);
                    if (found)
                    {
                        errors.Add("Refund/Extra charge already done !. (" + item.ConsignmentNumber + ") ");
                    }
                    else if (!isConsignmentNoBelongstoSelectedCustomer)
                    {
                        errors.Add("Could not find consignment number corresponding to selected customer !. (" + item.ConsignmentNumber + ") ");
                    }
                    else
                    {
                        var refundOrExtraCharge = new RefundOrExtraCharge()
                        {
                            ApprovalStatus = (int)Helpers.PaymentStatus.Pending,
                            ConsignmentNumber = item.ConsignmentNumber,
                            CustomerId = model.CustomerId,
                            Amount = item.Amount,
                            Reason = model.Reason,
                            PaymentCategory = (int)model.PaymentCategory,
                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            CreatedOn = DateTime.Now,
                            UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                            UpdatedOn = DateTime.Now
                        };
                        bulkRefundOrExtraCharge.Add(refundOrExtraCharge);
                    }
                }
                if (bulkRefundOrExtraCharge.Count() > 0)
                {
                    DB.RefundOrExtraCharges.AddRange(bulkRefundOrExtraCharge);
                    DB.SaveChanges();
                    ViewBag.SuccessMessage = "Refund/Extra charge added successfully.";
                }
            }
            catch (Exception exc)
            {
                errors.Add("Errors occur in processing request. Please contact administrator to resolve.");
                LogHelper.Log(exc);
            }
            ViewBag.ErrorMessages = errors;
            return View("AddRefundOrExtraChargeBulk", model);
        }

        private GridViewSettings GetPaymentStatusGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewPaymentStatus";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add("CompanyName");
            settings.Columns.Add("Username");
            settings.Columns.Add("TransactionNumber");
            settings.Columns.Add("TransactionAmount").PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("TransactionDate");
            settings.Columns.Add("TransactionTime");
            settings.Columns.Add("MobileNumber");
            var column = settings.Columns.Add("AccountType", MVCxGridViewColumnType.ComboBox);
            column.Caption = "Payment Mode";
            var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = EnumHelpers.ConvertEnumToSelectList<PaymentAccountTypes>();
            comboBoxProperties.TextField = "Text";
            comboBoxProperties.ValueField = "Value";
            comboBoxProperties.ValueType = typeof(int);
            column = settings.Columns.Add("Status", MVCxGridViewColumnType.ComboBox);
            comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = EnumHelpers.ConvertEnumToSelectList<PaymentStatus>();
            comboBoxProperties.TextField = "Text";
            comboBoxProperties.ValueField = "Value";
            comboBoxProperties.ValueType = typeof(int);
            settings.Columns.Add("ApprovalDate");
            settings.Columns.Add("ApprovalTime");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        #region Data queries
        public JsonResult GetRefundOrExtraChargeReasons(string paymentCategory)
        {
            var reasons = GetRefundOrExtraChargeReasonsAsList(paymentCategory);
            return Json(new SelectList(reasons, "Value", "Text"));
        }

        private static List<SelectListItem> GetRefundOrExtraChargeReasonsAsList(string paymentCategory)
        {
            List<SelectListItem> reasons = new List<SelectListItem>();
            var refundOrExtraChargeReasons = DB.ChargingReasons.ToList();

            if (paymentCategory == ((int)PaymentCategory.Refund).ToString())
            {
                var refundReasons = from a in refundOrExtraChargeReasons.Where(x => x.PaymentCategory == (int)PaymentCategory.Refund)
                                    select new SelectListItem
                                    {
                                        Text = a.Description,
                                        Value = a.Id.ToString()
                                    };
                reasons = refundReasons.ToList();

            }
            else if (paymentCategory == ((int)PaymentCategory.ExtraCharge).ToString())
            {
                var extraChargeReasons = from a in refundOrExtraChargeReasons.Where(x => x.PaymentCategory == (int)PaymentCategory.ExtraCharge)
                                         select new SelectListItem
                                         {
                                             Text = a.Description,
                                             Value = a.Id.ToString()
                                         };
                reasons = extraChargeReasons.ToList();
            }
            return reasons;
        }

        public static IQueryable<CustomerViewModel> GetCustomers()
        { 
            var customers = (from person in DB.Customers
                             from user in DB.AspNetUsers
                             where !person.IsDeleted && user.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)
                             //from customerPricePlan in DB.CustomerPricePlans
                             //where !customerPricePlan.IsDeleted && person.PricePlan == customerPricePlan.Id
                             select new CustomerViewModel()
                             {
                                 Id = person.Id,
                                 Name = person.CustomerName,
                                 CreatedOn = person.CreatedOn,
                                 UserName = person.UserName,
                                 Photo = person.CustomerPhoto,
                                 UpdatedOn = person.UpdatedOn,
                                 Address = person.CustomerAddress,
                                 CountryCode = person.CustomerCountry,
                                 MobileNumber = person.CustomerPhone,
                                 Email = user.Email,
                                 PaymentType = person.PaymentTypeId,
                                 Company = person.CustomerCompany,
                                 Credit = person.Credit,
                                 ZipCode = person.CustomerZipCode,
                                 IsActive = person.IsActive,
                                 Salesperson = person.Salesperson,
                                 Remark = person.Remark,
                                 PricePlan = person.PricePlan,
                                 DefaultPromoCode = person.DefaultPromoCode,
                                 NotificationKey = person.NotificationKey,
                                 UseGroupPrice = person.UseGroupPrice,
                                 CompanyPrefix = person.CompanyPrefix
                             });

            return customers;
        }

        public static List<PaymentTypeViewModel> GetPaymentTypes()
        {
            return (from type in DB.PaymentTypes
                    where !type.IsDeleted
                    select new PaymentTypeViewModel()
                    {
                        Name = type.PaymentTypeName,
                        Id = type.Id
                    }).ToList();
        }

        public static List<PricePlanViewModel> GetPricePlans()
        {
            return (from type in DB.CustomerPricePlans
                    where !type.IsDeleted
                    select new PricePlanViewModel()
                    {
                        PlanName = type.PlanName,
                        Id = type.Id
                    }).ToList();
        }

        public static List<PricePlanViewModel> GetAllPricePlans()
        {
            return (from type in DB.CustomerPricePlans
                    select new PricePlanViewModel()
                    {
                        PlanName = type.PlanName,
                        Id = type.Id
                    }).ToList();
        }

        public static List<SelectListItem> GetCharges()
        {
            return (from charge in DB.Charges
                    where !charge.IsDeleted && !string.IsNullOrEmpty(charge.ChargeCode)
                    select charge)
                .ToList()
                .Select(
                        x => new SelectListItem
                        {
                            Value = x.ChargeCode,
                            Text = x.ChargeCode + " - " + (x.IsChargeInPercentage ? x.ChargeAmount.ToString("P0") : x.ChargeAmount.ToString("C"))
                        })
                .ToList();
        }

        public static IQueryable<PaymentStatusModel> GetPaymentStatuses(DateTime? start, DateTime? end)
        {
            if (!start.HasValue || !end.HasValue)
            {
                start = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
                end = DateTime.Now.Date;
            }

            var result = DB.TopupPayments
                           .Join(
                                 DB.PaymentAccounts,
                                 payment => payment.PaymentAccountId,
                                 account => account.Id,
                                 (payment, account) => new
                                 {
                                     Id = payment.Id,
                                     Status = (PaymentStatus)payment.Status,
                                     TransactionAmount = payment.TransferAmount,
                                     MobileNumber = account.MobileNumber,
                                     TransactionNumber = account.TransactionNumber,
                                     CompanyName = account.CompanyName,
                                     TransactionOn = account.TransactionOn ?? account.CreatedOn,
                                     AccountType = (PaymentAccountTypes)account.AccountType,
                                     ApprovalOn = payment.ApprovedOn,
                                     Username = account.Owner
                                 })
                           .Where(
                                  x => (x.AccountType == PaymentAccountTypes.Bank && DbFunctions.TruncateTime(x.TransactionOn) >= start && DbFunctions.TruncateTime(x.TransactionOn) <= end) ||
                                       (x.AccountType != PaymentAccountTypes.Bank && DbFunctions.TruncateTime(x.ApprovalOn) >= start && DbFunctions.TruncateTime(x.ApprovalOn) <= end))
                           .Select(
                                   x => new PaymentStatusModel()
                                   {
                                       Id = x.Id,
                                       Status = x.Status,
                                       TransactionNumber = x.TransactionNumber,
                                       TransactionAmount = x.TransactionAmount,
                                       MobileNumber = x.MobileNumber,
                                       CompanyName = x.CompanyName,
                                       AccountType = x.AccountType,
                                       Username = x.Username,
                                       ApprovalDate = x.ApprovalOn,
                                       ApprovalTime = x.ApprovalOn,
                                       TransactionDate = x.AccountType == PaymentAccountTypes.Bank ? x.TransactionOn : x.ApprovalOn,
                                       TransactionTime = x.AccountType == PaymentAccountTypes.Bank ? x.TransactionOn : x.ApprovalOn,
                                   });
            return result;
        }

        [HttpPost]
        public string GetPromoCode(int? pricePlanId)
        {
            var PricePlan = db.CustomerPricePlans.Where(x=> x.Id == pricePlanId).Select(x=>x.DefaultPromoCode).FirstOrDefault();
            return PricePlan;
        }

        public static IQueryable<RefundOrExtraChargeViewModel> GetRefundOrExtraChargeStatuses(DateTime? startDate, DateTime? endDate)
        {
            DateTime now = DateTime.Now;
            if (startDate == null)
                startDate = new DateTime(now.Year, now.Month, 1);
            if (endDate == null)
                endDate = startDate.Value.AddMonths(1).AddDays(-1);

            var result = from refund in DB.RefundOrExtraCharges
                         where DbFunctions.TruncateTime(refund.CreatedOn) >= startDate && DbFunctions.TruncateTime(refund.CreatedOn) <= endDate
                         select new RefundOrExtraChargeViewModel
                         {
                             Id = refund.Id,
                             ConsignmentNumber = refund.ConsignmentNumber,
                             ApprovalStatus = refund.ApprovalStatus,
                             CreatedBy = refund.CreatedBy,
                             CreatedOn = refund.CreatedOn,
                             UserName = refund.Customer.UserName,
                             CustomerName = refund.Customer.CustomerName,
                             Amount = refund.Amount,
                             Reason = refund.Reason,
                             SalesPerson = refund.Customer.Salesperson,
                             UpdatedBy = refund.UpdatedBy,
                             UpdatedOn = refund.UpdatedOn,
                             CustomerId = refund.CustomerId,
                             CompanyName = refund.Customer.CustomerCompany,
                             PaymentCategory = refund.PaymentCategory,
                             ChargingReason = refund.ChargingReason
                         };
            return result;
        }

        #endregion
    }
}