﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using System.Collections.Generic;
using DevExpress.Web.Mvc;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class PricePlansController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "PricePlansDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowPricePlans)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersShowPricePlans)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddPricePlan)]
        public ActionResult Add()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersAddPricePlan)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add([ModelBinder(typeof(DevExpressEditorsBinder))]AddEditPricePlanViewModel model)
        {
            if (ModelState.IsValid)
            {
                var plan = new CustomerPricePlan()
                {
                    PlanName = model.PlanName,
                    //LimitValue = model.LimitValue,
                    //RebateAmount = model.RebateAmount / 100.0, // In percentage
                    //Comparison = (int) model.Comparison,
                    //UnitType = (int) model.UnitType,
                    //PeriodType = (int) model.PeriodType,
                    IsDeleted = false,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now,
                    DefaultPromoCode = model.DefaultPromoCode == null ? "" : string.Join(",", model.DefaultPromoCode),
                };

                db.CustomerPricePlans.Add(plan);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "PricePlans");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersEditPricePlan)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from plan in db.CustomerPricePlans
                         where !plan.IsDeleted && plan.Id == id
                         select new
                         {
                             //Comparison = (ComparisonTypes) plan.Comparison,
                             //RebateAmount = plan.RebateAmount * 100.0,
                             //UnitType = (UnitTypes) plan.UnitType,
                             //PeriodType = (PeriodTypes) plan.PeriodType,
                             PlanName = plan.PlanName,
                             //LimitValue = plan.LimitValue
                             DefaultPromoCode = plan.DefaultPromoCode
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(new AddEditPricePlanViewModel()
            {
                PlanName = model.PlanName,
                DefaultPromoCode = (string.IsNullOrEmpty(model.DefaultPromoCode) ? "" : model.DefaultPromoCode).Split(',')
            });
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersEditPricePlan)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, [ModelBinder(typeof(DevExpressEditorsBinder))]AddEditPricePlanViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var plan = db.CustomerPricePlans.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (plan == null)
                {
                    return HttpNotFound();
                }

                plan.PlanName = model.PlanName;
                //plan.Comparison = (int) model.Comparison;
                //plan.LimitValue = model.LimitValue;
                //plan.RebateAmount = model.RebateAmount / 100.0; // In percentage
                //plan.UnitType = (int) model.UnitType;
                //plan.PeriodType = (int) model.PeriodType;
                plan.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                plan.UpdatedOn = DateTime.Now;
                plan.DefaultPromoCode = model.DefaultPromoCode == null ? "" : string.Join(",", model.DefaultPromoCode);
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "PricePlans");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.CustomersDeletePricePlan)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var plan = db.CustomerPricePlans.FirstOrDefault(x => x.Id == id);
                if (plan != null)
                {
                    plan.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<PricePlanViewModel> GetCustomerPricePlans()
        {
            var services = (from plan in DB.CustomerPricePlans
                            where !plan.IsDeleted
                            select new PricePlanViewModel()
                            {
                                Id = plan.Id,
                                PlanName = plan.PlanName,
                                LimitValue = plan.LimitValue,
                                RebateAmount = plan.RebateAmount,
                                UnitType = (UnitTypes)plan.UnitType,
                                Comparison = (ComparisonTypes)plan.Comparison,
                                PeriodType = (PeriodTypes)plan.PeriodType,
                                CreatedOn = plan.CreatedOn,
                                UpdatedOn = plan.UpdatedOn,
                                DefaultPromoCode =  plan.DefaultPromoCode
                            });
            

            return services;
        }

        public static List<SelectListItem> GetCharges()
        {
            return (from charge in DB.Charges
                    where !charge.IsDeleted && !string.IsNullOrEmpty(charge.ChargeCode)
                    select charge)
                .ToList()
                .Select(
                        x => new SelectListItem
                        {
                            Value = x.ChargeCode,
                            Text = x.ChargeCode + " - " + (x.IsChargeInPercentage ? x.ChargeAmount.ToString("P0") : x.ChargeAmount.ToString("C"))
                        })
                .ToList();
        }

        #endregion
    }
}