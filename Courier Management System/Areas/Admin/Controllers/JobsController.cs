﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Data.WcfLinq.Helpers;
using Excel;
using Newtonsoft.Json;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class JobsController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "JobsDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    var context = new CourierDBEntities();
                    context.Database.CommandTimeout = 600;
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = context;
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        public JobsController()
        {
            db.Database.CommandTimeout = 600;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowTodayJobs)]
        public ActionResult List()
        {
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowTodayJobs)]
        public ActionResult ListViewPartial(long? id, string action, string driver)
        {
            var result = AssignDriver(id, action, driver, User.Identity.IsAuthenticated ? User.Identity.Name : "");
            if (id.HasValue && !result)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
            }

            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchDeleteJob)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var job = db.Jobs.FirstOrDefault(x => x.Id == id);
                if (job != null)
                {
                    job.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchImportJobs)]
        [HttpPost]
        public ActionResult Import()
        {
            var file = Request.Files.Get("importFile");

            if (!string.IsNullOrEmpty(file?.FileName))
            {
                IExcelDataReader excelReader;

                if (file.FileName.ToLower().EndsWith(".xls"))
                {
                    // Load data from Excel 2003 file
                    excelReader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
                }
                else if (file.FileName.ToLower().EndsWith(".xlsx"))
                {
                    // Load data from Excel 2007 file
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
                }
                else
                {
                    excelReader = null;
                }

                if (excelReader != null)
                {
                    var rowCount = 0;
                    try
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        var data = excelReader.AsDataSet();

                        if (data.Tables.Count == 0)
                        {
                            // There is no table in sheet
                            TempData["ErrorMessage"] = "Import file does not have correct format. There is no data sheet. Please use file exported from VRP system.";
                        }
                        else
                        {
                            // Find table with name "Locations"
                            var locationTable = data.Tables[0];

                            // Find old jobs
                            var jobListIds = new System.Collections.Generic.List<int>();
                            for (var i = 0; i < locationTable.Rows.Count; i++)
                            {
                                var row = locationTable.Rows[i];
                                var vehicleId = int.Parse(row[0].ToString());
                                jobListIds.Add(vehicleId);
                            }

                            var startDate = DateTime.Now.Date;
                            var endDate = startDate.AddDays(1);
                            var jobs = db.Jobs.Where(
                                                     x => !x.IsDeleted && x.CreatedOn >= startDate && x.CreatedOn < endDate &&
                                                          jobListIds.Contains(x.VehicleId.Value) && !x.CompletedOn.HasValue).ToList();
                            if (jobs.Count > 0)
                            {
                                TempData["ErrorMessage"] = "There are same records in current database. Please check carefully before import.";
                            }
                            else
                            {
                                for (var i = 0; i < locationTable.Rows.Count; i++)
                                {
                                    // Process each row
                                    var row = locationTable.Rows[i];

                                    var route = JsonConvert.DeserializeObject<JobViewModel>(row[1].ToString());

                                    ++rowCount;
                                    var job = new Job()
                                              {
                                                  Routes = row[1].ToString(),
                                                  IsDeleted = false,
                                                  VehicleId = int.Parse(row[0].ToString()),
                                                  TotalPickups = route.Locations.Where(x => x.Type == 0).SelectMany(x => x.OrderId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)).Count(),
                                                  TotalDeliveries = route.Locations.Where(x => x.Type == 1).SelectMany(x => x.OrderId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)).Count(),
                                                  TotalDistances = route.Route.Distances.Last(),
                                                  TotalDrivingTime = route.Route.DrivingTimes.Last(),
                                                  TotalWorkingTime = route.Route.WorkingTimes.Last(),
                                                  IsSpecial = false,
                                                  CreatedOn = DateTime.Now,
                                                  CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                  UpdatedOn = DateTime.Now,
                                                  UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                                              };
                                    db.Jobs.Add(job);
                                }

                                db.SaveChanges();
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        LogHelper.Log(exc);
                        if (exc is InvalidCastException || exc is NullReferenceException)
                        {
                            TempData["ErrorMessage"] = $"Import file does not have correct format. Please use file exported from VRP system. Error at row '{rowCount}'.<br />More information: {exc.ToString()}";
                        }
                        else
                        {
                            TempData["ErrorMessage"] = "There is error in processing. Please contact to administrator for support with these information: <br />" +
                                                       exc.Message + "<br />" + exc.StackTrace;
                        }
                    }
                    finally
                    {
                        excelReader.Close();
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Import file does not have correct format. Please use file exported from VRP system.";
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Import file does not selected.";
            }

            return RedirectToAction("List", "Jobs");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchPrintPlan)]
        public ActionResult PrintPlan(int? sortType)
        {
            var startDate = DateTime.Now.Date;
            var endDate = startDate.AddDays(1);

            startDate = DateTime.MinValue.Date;
            endDate = DateTime.MaxValue.Date;

            var list = (from job in DB.Jobs
                        where !job.IsDeleted && !job.CompletedOn.HasValue && !job.IsSpecial &&
                              DbFunctions.TruncateTime(job.CreatedOn) >= startDate && DbFunctions.TruncateTime(job.CreatedOn) < endDate
                        select job).ToList();

            var jobs = new System.Collections.Generic.List<JobReportViewModel>();
            foreach (var item in list)
            {
                var driverName = "";
                if (item.DriverType == (int) AccountTypes.Staff)
                {
                    var staff = DB.Staffs.FirstOrDefault(x => x.Id == item.DriverId);
                    driverName = staff?.StaffName;
                }
                else
                {
                    var contractor = DB.Contractors.FirstOrDefault(x => x.Id == item.DriverId);
                    driverName = contractor?.ContractorName;
                }
                var route = JsonConvert.DeserializeObject<JobViewModel>(item.Routes);

                // Get order list
                var orderIds = route.Locations.Where(x => !string.IsNullOrEmpty(x.OrderId)).Select(x => x.OrderId).Distinct().ToList();
                var ids = new List<long>();
                foreach (var orderId in orderIds)
                {
                    ids.AddRange(orderId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i)));
                }

                ids = ids.Distinct().ToList();
                var orders = db.Orders.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
                foreach (var location in route.Locations)
                {
                    var oids = (string.IsNullOrEmpty(location.OrderId) ? "" : location.OrderId).Split(
                                                                                                      new char[] { ',' },
                                                                                                      StringSplitOptions.RemoveEmptyEntries);
                    location.Orders = orders.Where(x => oids.Contains(x.Id.ToString())).ToList();
                    if (location.Orders.Count > 0)
                    {
                        location.ConsignmentNumbers = location.Orders.Select(x => x.ConsignmentNumber).ToList();
                        if (location.Type == 0)
                        {
                            // Pickup
                            location.CustomerName = location.Orders.FirstOrDefault().FromName;
                            location.CustomerAddress = location.Orders.FirstOrDefault().FromAddress;
                            location.CustomerMobilePhone = location.Orders.FirstOrDefault().FromMobilePhone;
                            location.CustomerCompany = location.Orders.FirstOrDefault().Customer.CustomerCompany;
                            location.IsDone = (location.Orders.FirstOrDefault().Status != (int) OrderStatuses.OrderConfirmed);
                        }
                        else
                        {
                            // Delivery
                            location.CustomerName = location.Orders.FirstOrDefault().ToName;
                            location.CustomerAddress = location.Orders.FirstOrDefault().ToAddress;
                            location.CustomerMobilePhone = location.Orders.FirstOrDefault().ToMobilePhone;
                            location.IsDone = (location.Orders.FirstOrDefault().Status != (int) OrderStatuses.ItemOutDepot);
                        }
                    }
                }

                // Parse path
                route.Route.FootSteps = JsonConvert.DeserializeObject<System.Collections.Generic.List<RouteStepViewModel>>(route.Route.Path);

                jobs.Add(
                         new JobReportViewModel()
                         {
                             Id = item.Id,
                             VehicleId = item.VehicleId.HasValue ? item.VehicleId.Value : 0,
                             VehicleNumber = item.Vehicle.VehicleNumber,
                             Routes = route,
                             TakenOn = item.TakenOn,
                             TakenBy = driverName,
                             TotalPickups = item.TotalPickups,
                             TotalDeliveries = item.TotalDeliveries,
                             TotalDistances = item.TotalDistances,
                             TotalDrivingTime = item.TotalDrivingTime,
                             TotalWorkingTime = item.TotalWorkingTime,
                             CreatedOn = item.CreatedOn
                         });
            }

            ViewBag.Jobs = jobs;
            ViewBag.SortType = sortType;

            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchPrintSpecialPlan)]
        public ActionResult PrintSpecialPlan()
        {
            ViewBag.Jobs = GetSpecialJobs().ToList();

            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowSpecialJobs)]
        public ActionResult Special()
        {
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowSpecialJobs)]
        public ActionResult SpecialListViewPartial(long? id, string action, string driver)
        {
            var result = AssignDriverForSpecial(id, action, driver, User.Identity.IsAuthenticated ? User.Identity.Name : "");
            if (id.HasValue && !result)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
            }

            return PartialView("_SpecialListViewPartial");
        }

        public ActionResult OrderDetail(string consignmentNumber)
        {
            var order = db.Orders.FirstOrDefault(x => !x.IsDeleted && x.ConsignmentNumber.Equals(consignmentNumber, StringComparison.OrdinalIgnoreCase));
            return PartialView("_OrderDetail", order);
        }
        
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchDeleteJob)]
        [HttpPost]
        public ActionResult SpecialListViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var job = db.Jobs.FirstOrDefault(x => x.Id == id);
                if (job != null)
                {
                    job.IsDeleted = true;
                    job.UpdatedOn = DateTime.Now;
                    job.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_SpecialListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchDeleteJob)]
        [HttpPost]
        public ActionResult SpecialListViewPartialMultiDelete(string jobsSelected)
        {
            if(string.IsNullOrEmpty(jobsSelected))
            {
                return RedirectToAction("Special");
            }

            try
            {
                var arrayJobs = jobsSelected.Split(',');
                foreach(var item in arrayJobs)
                {
                    var job = db.Jobs.FirstOrDefault(x => x.Id.ToString() == item);

                    if (job != null)
                    {
                        job.IsDeleted = true;
                        job.UpdatedOn = DateTime.Now;
                        job.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        db.SaveChanges();
                    }
                }               
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return RedirectToAction("Special");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchDriverHistory)]
        public ActionResult History()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchDriverHistory)]
        public ActionResult HistoryListViewPartial(string order)
        {
            ViewBag.SearchOrder = order;
            return PartialView("_HistoryListViewPartial");
        }

        public static bool AssignDriver(long? id, string action, string driver, string username)
        {
            if (id.HasValue)
            {
                var job = DB.Jobs.FirstOrDefault(x => x.Id == id);

                if (job != null && !string.IsNullOrEmpty(action))
                {
                    switch (action.ToLower())
                    {
                        case "assign":
                            if (!string.IsNullOrEmpty(driver))
                            {
                                var parts = driver.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                                if (parts.Length != 2)
                                {
                                    return false;
                                }
                                else
                                {
                                    try
                                    {
                                        job.DriverId = int.Parse(parts[1]);
                                        job.DriverType = parts[0].Equals("Staff", StringComparison.OrdinalIgnoreCase)
                                                             ? (int) AccountTypes.Staff
                                                             : (int) AccountTypes.Contractor;
                                        job.TakenOn = DateTime.Now;
                                        job.TakenBy = username;
                                        job.UpdatedOn = DateTime.Now;
                                        job.UpdatedBy = username;

                                        var driverName = parts[0].Equals("Staff", StringComparison.OrdinalIgnoreCase)
                                                             ? DB.Staffs.Where(x => x.Id == job.DriverId).Select(x => x.UserName).FirstOrDefault()
                                                             : DB.Contractors.Where(x => x.Id == job.DriverId).Select(x => x.UserName).FirstOrDefault();

                                        // Change status of order
                                        var route = JsonConvert.DeserializeObject<JobViewModel>(job.Routes);
                                        foreach (var location in route.Locations)
                                        {
                                            var orderIds = (string.IsNullOrEmpty(location.OrderId) ? "" : location.OrderId).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i));
                                            var orders = DB.Orders.Where(x => !x.IsDeleted && orderIds.Contains(x.Id));
                                            foreach (var order in orders)
                                            {
                                                if (order.Status == (int) OrderStatuses.ItemInDepot)
                                                {
                                                    order.Status = (int) OrderStatuses.ItemOutDepot;
                                                    order.ProceededOutOn = DateTime.Now;
                                                    order.ProceededOutBy = username;
                                                }
                                                else if (order.Status == (int) OrderStatuses.OrderConfirmed)
                                                {
                                                    order.Status = (int) OrderStatuses.ItemHasBeingPicked;
                                                }

                                                // Add to job history
                                                var jobHistory = new JobHistory()
                                                                 {
                                                                     ConsignmentNumber = order.ConsignmentNumber,
                                                                     JobId = job.Id,
                                                                     IsDeleted = false,
                                                                     CompletedOn = null,
                                                                     CompletedBy = driverName,
                                                                     IsSuccessful = false,
                                                                     CreatedBy = username,
                                                                     CreatedOn = DateTime.Now,
                                                                     UpdatedBy = username,
                                                                     UpdatedOn = DateTime.Now,
                                                                     IsPickup = order.Status == (int) OrderStatuses.ItemHasBeingPicked,
                                                                 };
                                                DB.JobHistories.Add(jobHistory);

                                                // Track change
                                                var track = new OrderTracking
                                                            {
                                                                ConsignmentNumber = order.ConsignmentNumber,
                                                                CreatedBy = username,
                                                                CreatedOn = DateTime.Now,
                                                                PickupTimeSlotId = order.PickupTimeSlotId,
                                                                PickupDate = order.PickupDate,
                                                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                                                DeliveryDate = order.DeliveryDate,
                                                                BinCode = order.BinCode,
                                                                Status = order.Status
                                                            };
                                                DB.OrderTrackings.Add(track);
                                            }
                                        }

                                        DB.SaveChanges();
                                        return true;
                                    }
                                    catch (Exception exc)
                                    {
                                        LogHelper.Log(exc);
                                    }
                                }
                            }

                            break;
                    }
                }
            }

            return false;
        }

        public static bool AssignDriverForSpecial(long? id, string action, string driver, string username)
        {
            if (id.HasValue)
            {
                var job = DB.Jobs.FirstOrDefault(x => x.Id == id);

                if (job != null && !string.IsNullOrEmpty(action))
                {
                    switch (action.ToLower())
                    {
                        case "assign":
                            if (!string.IsNullOrEmpty(driver))
                            {
                                var parts = driver.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                                if (parts.Length != 2)
                                {
                                    return false;
                                }
                                else
                                {
                                    try
                                    {
                                        job.DriverId = int.Parse(parts[1]);
                                        job.DriverType = parts[0].Equals("Staff", StringComparison.OrdinalIgnoreCase)
                                                             ? (int) AccountTypes.Staff
                                                             : (int) AccountTypes.Contractor;
                                        job.TakenOn = DateTime.Now;
                                        job.TakenBy = username;
                                        job.UpdatedOn = DateTime.Now;
                                        job.UpdatedBy = username;

                                        var driverName = parts[0].Equals("Staff", StringComparison.OrdinalIgnoreCase)
                                                             ? DB.Staffs.Where(x => x.Id == job.DriverId).Select(x => x.UserName).FirstOrDefault()
                                                             : DB.Contractors.Where(x => x.Id == job.DriverId).Select(x => x.UserName).FirstOrDefault();

                                        // Change status of Delivery order
                                        var ids = (job.Routes ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i));
                                        var orders = DB.Orders.Where(x => !x.IsDeleted && ids.Contains(x.Id));
                                        foreach (var order in orders)
                                        {
                                            if (order.Status == (int) OrderStatuses.SpecialOrderForDelivery)
                                            {
                                                order.Status = (int) OrderStatuses.ItemOutDepot;
                                                order.ProceededOutOn = DateTime.Now;
                                                order.ProceededOutBy = username;
                                            }
                                            else if (order.Status == (int) OrderStatuses.SpecialOrderForPickup)
                                            {
                                                order.Status = (int) OrderStatuses.ItemHasBeingPicked;
                                            }

                                            // Add to job history
                                            var jobHistory = new JobHistory()
                                                             {
                                                                 ConsignmentNumber = order.ConsignmentNumber,
                                                                 JobId = job.Id,
                                                                 IsDeleted = false,
                                                                 CompletedOn = null,
                                                                 CompletedBy = driverName,
                                                                 IsSuccessful = false,
                                                                 CreatedBy = username,
                                                                 CreatedOn = DateTime.Now,
                                                                 UpdatedBy = username,
                                                                 UpdatedOn = DateTime.Now,
                                                                 IsPickup = order.Status == (int) OrderStatuses.ItemHasBeingPicked,
                                                             };
                                            DB.JobHistories.Add(jobHistory);

                                            // Track change
                                            var track = new OrderTracking
                                                        {
                                                            ConsignmentNumber = order.ConsignmentNumber,
                                                            CreatedBy = username,
                                                            CreatedOn = DateTime.Now,
                                                            PickupTimeSlotId = order.PickupTimeSlotId,
                                                            PickupDate = order.PickupDate,
                                                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                                            DeliveryDate = order.DeliveryDate,
                                                            BinCode = order.BinCode,
                                                            Status = order.Status
                                                        };
                                            DB.OrderTrackings.Add(track);
                                        }

                                        DB.SaveChanges();
                                        return true;
                                    }
                                    catch (Exception exc)
                                    {
                                        LogHelper.Log(exc);
                                    }
                                }
                            }

                            break;
                    }
                }
            }

            return false;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchDriverHistory)]
        public ActionResult BuildJobHistory()
        {
            return RedirectToAction("List");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowTodayJobs)]
        public ActionResult OrdersList(string id)
        {
            ViewBag.Id = id;
            return PartialView("_OrdersList");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowTodayJobs)]
        public ActionResult OrdersListGrid(string id)
        {
            ViewBag.Id = id;
            var job = db.Jobs.FirstOrDefault(x => !x.IsDeleted && x.Id.ToString() == id);
            if (job != null)
            {
                if (!string.IsNullOrEmpty(job.Routes))
                {
                    var route = JsonConvert.DeserializeObject<JobViewModel>(job.Routes);
                    var ids = new List<long>();
                    foreach (var step in route.Route.Steps)
                    {
                        var orderId = route.Locations[step - 1].OrderId;
                        if (!string.IsNullOrEmpty(orderId))
                        {
                            ids.AddRange(orderId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i)));
                        }
                    }

                    ids = ids.Distinct().ToList();
                    var orders = db.Orders.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();                    
                    return PartialView("_OrdersListGrid", orders);
                }
            }

            return PartialView("_OrdersListGrid");
        }

        public static DetailJobViewModel GetDetailJobs(string id)
        {
            var result = new DetailJobViewModel();
            var job = DB.Jobs.FirstOrDefault(x => !x.IsDeleted && x.Id.ToString() == id);
            if (job != null)
            {
                if (!string.IsNullOrEmpty(job.Routes))
                {
                    var route = JsonConvert.DeserializeObject<JobViewModel>(job.Routes);
                    var ids = new List<long>();
                    foreach (var step in route.Route.Steps)
                    {
                        var orderId = route.Locations[step - 1].OrderId;
                        if (!string.IsNullOrEmpty(orderId))
                        {
                            ids.AddRange(orderId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => long.Parse(i)));
                        }
                    }

                    ids = ids.Distinct().ToList();
                    var orders = DB.Orders.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();

                    var total = orders.Count();
                    var completed = 0;
                    var failed = 0;
                    var inProgress = 0;
                    var others = 0;

                    foreach(var item in orders)
                    {
                        if(item.Status == (int)OrderStatuses.ItemCollected || item.Status == (int)OrderStatuses.ItemDelivered)
                        {
                            completed++;
                        }
                        else if(item.Status == (int)OrderStatuses.DeliveryFailed || item.Status == (int)OrderStatuses.PickupFailed)
                        {
                            failed++;
                        }
                        else if(item.Status == (int)OrderStatuses.ItemOutDepot || item.Status == (int)OrderStatuses.ItemHasBeingPicked)
                        {
                            inProgress++;
                        }
                        else
                        {
                            others++;
                        }
                    }

                    result.TotalOrders = total;
                    result.Completed = completed;                    
                    result.Failed = failed;
                    result.InProgress = inProgress;
                    result.Others = others;

                    return result;
                }
            }

            return result;
        }

        #region Data

        public static IQueryable<DriverReportViewModel> GetJobs()
        {
            var startDate = DateTime.Now.Date;
            var endDate = startDate.AddDays(1);

            // Allow to show all, remove these lines if just want to show today jobs
            startDate = DateTime.MinValue.Date;
            endDate = DateTime.MaxValue.Date;

            var staffs = (from job in DB.Jobs
                          from vehicle in DB.Vehicles
                          join people in DB.Staffs on job.DriverId equals people.Id into peopleGroup
                          from person in peopleGroup.DefaultIfEmpty()
                          where !job.IsDeleted && job.DriverType == (int) AccountTypes.Staff && job.IsSpecial == false &&
                                DbFunctions.TruncateTime(job.CreatedOn) >= startDate && DbFunctions.TruncateTime(job.CreatedOn) < endDate
                                && vehicle.Id == job.VehicleId
                          select new DriverReportViewModel()
                                 {
                                     Id = job.Id.ToString(),
                                     Name = person.StaffName,
                                     UserName = person.UserName,
                                     CountryCode = person.StaffCountry,
                                     MobileNumber = person.StaffMobile,
                                     VehicleType = vehicle.VehicleType,
                                     VehicleNumber = vehicle.VehicleNumber,
                                     ICNumber = person.ICNumber,
                                     TakenOn = job.TakenOn,
                                     AccountType = AccountTypes.Staff,
                                     Routes = job.Routes,
                                     CompletedOn = job.CompletedOn,
                                     TotalPickups = job.TotalPickups,
                                     TotalDeliveries = job.TotalDeliveries,
                                     TasksCompleted = job.TasksCompleted
                                 });

            var contractors = (from job in DB.Jobs
                               from vehicle in DB.Vehicles
                               join people in DB.Contractors on job.DriverId equals people.Id into peopleGroup
                               from person in peopleGroup.DefaultIfEmpty()
                               where !job.IsDeleted && job.DriverType == (int) AccountTypes.Contractor && job.IsSpecial == false &&
                                     DbFunctions.TruncateTime(job.CreatedOn) >= startDate && DbFunctions.TruncateTime(job.CreatedOn) <= endDate
                                     && vehicle.Id == job.VehicleId
                               select new DriverReportViewModel()
                                      {
                                          Id = job.Id.ToString(),
                                          Name = person.ContractorName,
                                          UserName = person.UserName,
                                          CountryCode = person.ContractorCountry,
                                          MobileNumber = person.ContractorMobile,
                                          VehicleType = vehicle.VehicleType,
                                          VehicleNumber = vehicle.VehicleNumber,
                                          ICNumber = person.ICNumber,
                                          TakenOn = job.TakenOn,
                                          AccountType = AccountTypes.Contractor,
                                          Routes = job.Routes,
                                          CompletedOn = job.CompletedOn,
                                          TotalPickups = job.TotalPickups,
                                          TotalDeliveries = job.TotalDeliveries,
                                          TasksCompleted = job.TasksCompleted,                                          
                                      });
            var result = staffs.Union(contractors).OrderByDescending(x => x.Id);

            return result;
        }

        public static IQueryable<DriverReportViewModel> GetSpecialJobs()
        {
            var startDate = DateTime.Now.Date;
            var endDate = startDate.AddDays(1);

            startDate = DateTime.MinValue.Date;
            endDate = DateTime.MaxValue.Date;

            var staffs = (from job in DB.Jobs
                          join people in DB.Staffs on job.DriverId equals people.Id into peopleGroup
                          from person in peopleGroup.DefaultIfEmpty()
                          where !job.IsDeleted && job.DriverType == (int) AccountTypes.Staff && !job.CompletedOn.HasValue && job.IsSpecial &&
                                DbFunctions.TruncateTime(job.CreatedOn) >= startDate && DbFunctions.TruncateTime(job.CreatedOn) <= endDate
                          select new
                                 {
                                     Id = job.Id.ToString(),
                                     Name = person.StaffName,
                                     UserName = person.UserName,
                                     CountryCode = person.StaffCountry,
                                     MobileNumber = person.StaffMobile,
                                     ICNumber = person.ICNumber,
                                     TakenOn = job.TakenOn,
                                     AccountType = AccountTypes.Staff,
                                     Routes = job.Routes
                                 });

            var contractors = (from job in DB.Jobs
                               join people in DB.Contractors on job.DriverId equals people.Id into peopleGroup
                               from person in peopleGroup.DefaultIfEmpty()
                               where !job.IsDeleted && job.DriverType == (int) AccountTypes.Contractor && !job.CompletedOn.HasValue && job.IsSpecial &&
                                     DbFunctions.TruncateTime(job.CreatedOn) >= startDate && DbFunctions.TruncateTime(job.CreatedOn) <= endDate
                               select new
                                      {
                                          Id = job.Id.ToString(),
                                          Name = person.ContractorName,
                                          UserName = person.UserName,
                                          CountryCode = person.ContractorCountry,
                                          MobileNumber = person.ContractorMobile,
                                          ICNumber = person.ICNumber,
                                          TakenOn = job.TakenOn,
                                          AccountType = AccountTypes.Contractor,
                                          Routes = job.Routes,
                                      });

            var result = staffs.Union(contractors)
                               .Select(
                                       job => new DriverReportViewModel
                                              {
                                                  Id = job.Id,
                                                  Name = job.Name,
                                                  UserName = job.UserName,
                                                  CountryCode = job.CountryCode,
                                                  MobileNumber = job.MobileNumber,
                                                  ICNumber = job.ICNumber,
                                                  TakenOn = job.TakenOn,
                                                  AccountType = job.AccountType,
                                                  Routes = "",
                                                  Orders = DB.Orders.Where(x => !x.IsDeleted && ("," + job.Routes + ",").Contains("," + x.Id.ToString() + ",")).ToList()
                                              });

            return result;
        }

        public static IQueryable<DriverReportViewModel> GetHistoryJobs(string order)
        {
            if (string.IsNullOrEmpty(order))
            {
                order = "";
            }
            // Get id from consignment number
            var orderId = DB.Orders.Where(x => x.ConsignmentNumber.ToLower() == order.ToLower())
                            .Select(x => x.Id)
                            .FirstOrDefault();

            var staffs = (from job in DB.Jobs
                          where
                          orderId == 0 || job.Routes.Contains("\"" + orderId + "\"") || job.Routes.Contains("\"" + orderId + ",") || job.Routes.Contains("," + orderId + ",") || job.Routes.Contains("," + orderId + "\"")
                          join vehicle in DB.Vehicles on job.VehicleId equals vehicle.Id into vehicleGroup
                          from car in vehicleGroup.DefaultIfEmpty()
                          join people in DB.Staffs on job.DriverId equals people.Id into peopleGroup
                          from person in peopleGroup.DefaultIfEmpty()
                          where !job.IsDeleted && job.DriverType == (int) AccountTypes.Staff && job.IsSpecial == false
                          select new
                                 {
                                     Id = job.Id.ToString(),
                                     Name = person.StaffName,
                                     UserName = person.UserName,
                                     CountryCode = person.StaffCountry,
                                     MobileNumber = person.StaffMobile,
                                     VehicleType = car.VehicleType,
                                     VehicleNumber = car.VehicleNumber,
                                     ICNumber = person.ICNumber,
                                     TakenOn = job.TakenOn,
                                     AccountType = AccountTypes.Staff,
                                     Routes = job.Routes,
                                     IsSpecial = job.IsSpecial
                                 });

            var contractors = (from job in DB.Jobs
                               where
                               orderId == 0 || job.Routes.Contains("\"" + orderId + "\"") || job.Routes.Contains("\"" + orderId + ",") || job.Routes.Contains("," + orderId + ",")
                               || job.Routes.Contains("," + orderId + "\"")
                               join vehicle in DB.Vehicles on job.VehicleId equals vehicle.Id into vehicleGroup
                               from car in vehicleGroup.DefaultIfEmpty()
                               join people in DB.Contractors on job.DriverId equals people.Id into peopleGroup
                               from person in peopleGroup.DefaultIfEmpty()
                               where !job.IsDeleted && job.DriverType == (int) AccountTypes.Contractor && job.IsSpecial == false
                               select new
                                      {
                                          Id = job.Id.ToString(),
                                          Name = person.ContractorName,
                                          UserName = person.UserName,
                                          CountryCode = person.ContractorCountry,
                                          MobileNumber = person.ContractorMobile,
                                          VehicleType = car.VehicleType,
                                          VehicleNumber = car.VehicleNumber,
                                          ICNumber = person.ICNumber,
                                          TakenOn = job.TakenOn,
                                          AccountType = AccountTypes.Contractor,
                                          Routes = job.Routes,
                                          IsSpecial = job.IsSpecial
                                      });

            var staffsSpecial = (from job in DB.Jobs
                                 where orderId == 0 || ("," + job.Routes + ",").Contains("," + orderId + ",")
                                 join vehicle in DB.Vehicles on job.VehicleId equals vehicle.Id into vehicleGroup
                                 from car in vehicleGroup.DefaultIfEmpty()
                                 join people in DB.Staffs on job.DriverId equals people.Id into peopleGroup
                                 from person in peopleGroup.DefaultIfEmpty()
                                 where !job.IsDeleted && job.DriverType == (int) AccountTypes.Staff && job.IsSpecial == true
                                 select new
                                        {
                                            Id = job.Id.ToString(),
                                            Name = person.StaffName,
                                            UserName = person.UserName,
                                            CountryCode = person.StaffCountry,
                                            MobileNumber = person.StaffMobile,
                                            VehicleType = car.VehicleType,
                                            VehicleNumber = car.VehicleNumber,
                                            ICNumber = person.ICNumber,
                                            TakenOn = job.TakenOn,
                                            AccountType = AccountTypes.Staff,
                                            Routes = job.Routes,
                                            IsSpecial = job.IsSpecial
                                        });

            var contractorsSpecial = (from job in DB.Jobs
                                      where orderId == 0 || ("," + job.Routes + ",").Contains("," + orderId + ",")
                                      join vehicle in DB.Vehicles on job.VehicleId equals vehicle.Id into vehicleGroup
                                      from car in vehicleGroup.DefaultIfEmpty()
                                      join people in DB.Contractors on job.DriverId equals people.Id into peopleGroup
                                      from person in peopleGroup.DefaultIfEmpty()
                                      where !job.IsDeleted && job.DriverType == (int) AccountTypes.Contractor && job.IsSpecial == true
                                      select new
                                             {
                                                 Id = job.Id.ToString(),
                                                 Name = person.ContractorName,
                                                 UserName = person.UserName,
                                                 CountryCode = person.ContractorCountry,
                                                 MobileNumber = person.ContractorMobile,
                                                 VehicleType = car.VehicleType,
                                                 VehicleNumber = car.VehicleNumber,
                                                 ICNumber = person.ICNumber,
                                                 TakenOn = job.TakenOn,
                                                 AccountType = AccountTypes.Contractor,
                                                 Routes = job.Routes,
                                                 IsSpecial = job.IsSpecial
                                             });

            var result = staffs.Union(contractors).Union(staffsSpecial).Union(contractorsSpecial)
                               .Select(
                                       x => new DriverReportViewModel()
                                            {
                                                Id = x.Id,
                                                Name = x.Name,
                                                UserName = x.UserName,
                                                CountryCode = x.CountryCode,
                                                MobileNumber = x.MobileNumber,
                                                VehicleType = x.VehicleType,
                                                VehicleNumber = x.VehicleNumber,
                                                ICNumber = x.ICNumber,
                                                TakenOn = x.TakenOn,
                                                AccountType = x.AccountType,
                                                Routes = x.Routes,
                                                Orders = DB.Orders.Where(i => !i.IsDeleted && ("," + (x.IsSpecial ? x.Routes : "") + ",").Contains("," + i.Id.ToString() + ",")).ToList()
                                            });
            return result;
        }

        #endregion
    }
}
