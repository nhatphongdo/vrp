﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class VehiclesController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "VehiclesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.VehiclesShowVehicles)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.VehiclesShowVehicles + "," + AccountHelper.VehiclesEditVehicle)]
        public ActionResult ListViewPartial(int? id, bool? isActive)
        {
            if (id.HasValue && isActive.HasValue)
            {
                var vehicle = DB.Vehicles.FirstOrDefault(x => !x.IsDeleted && x.Id == id.Value);
                if (vehicle != null)
                {
                    vehicle.IsActive = isActive.Value;
                    DB.SaveChanges();
                }
            }
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.VehiclesAddVehicle)]
        public ActionResult Add()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.VehiclesAddVehicle)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditVehicleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var vehicle = new Vehicle()
                              {
                                  VehicleType = model.VehicleType,
                                  VehicleNumber = model.VehicleNumber,
                                  Capacity = model.Capacity,
                                  Color = model.Color,
                                  MileAge = model.MileAge,
                                  WorkingTimeLimit = model.WorkingTimeLimit.Hour + model.WorkingTimeLimit.Minute / 60.0,
                                  WorkStartTime = model.WorkStartTime.Hour + model.WorkStartTime.Minute / 60.0,
                                  DrivingTimeLimit = model.DrivingTimeLimit.Hour + model.DrivingTimeLimit.Minute / 60.0,
                                  DistanceLimit = model.DistanceLimit,
                                  IsActive = model.IsActive,
                                  CostPerKilometer = model.CostPerKilometer,
                                  FixedCostPerTrip = model.FixedCostPerTrip,
                                  IsDeleted = false,
                                  CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                  CreatedOn = DateTime.Now,
                                  UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                  UpdatedOn = DateTime.Now
                              };

                db.Vehicles.Add(vehicle);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Vehicles");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.VehiclesEditVehicle)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from vehicle in db.Vehicles
                         where !vehicle.IsDeleted && vehicle.Id == id
                         select new AddEditVehicleViewModel()
                                {
                                    Capacity = vehicle.Capacity,
                                    VehicleNumber = vehicle.VehicleNumber,
                                    VehicleType = vehicle.VehicleType,
                                    MileAge = vehicle.MileAge,
                                    Color = vehicle.Color,
                                    CostPerKilometer = vehicle.CostPerKilometer,
                                    FixedCostPerTrip = vehicle.FixedCostPerTrip,
                                    IsActive = vehicle.IsActive,
                                    DistanceLimit = vehicle.DistanceLimit,
                                    WorkStartTime = DbFunctions.CreateDateTime(1, 1, 1, (int) vehicle.WorkStartTime, 60 * (int) (vehicle.WorkStartTime - (int) vehicle.WorkStartTime), 0).Value,
                                    DrivingTimeLimit = DbFunctions.CreateDateTime(1, 1, 1, (int) vehicle.DrivingTimeLimit, 60 * (int) (vehicle.DrivingTimeLimit - (int) vehicle.DrivingTimeLimit), 0).Value,
                                    WorkingTimeLimit = DbFunctions.CreateDateTime(1, 1, 1, (int) vehicle.WorkingTimeLimit, 60 * (int) (vehicle.WorkingTimeLimit - (int) vehicle.WorkingTimeLimit), 0).Value,
                                }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.VehiclesEditVehicle)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditVehicleViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var vehicle = db.Vehicles.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (vehicle == null)
                {
                    return HttpNotFound();
                }

                vehicle.Capacity = model.Capacity;
                vehicle.Color = model.Color;
                vehicle.MileAge = model.MileAge;
                vehicle.VehicleNumber = model.VehicleNumber;
                vehicle.VehicleType = model.VehicleType;
                vehicle.WorkingTimeLimit = model.WorkingTimeLimit.Hour + model.WorkingTimeLimit.Minute / 60.0;
                vehicle.WorkStartTime = model.WorkStartTime.Hour + model.WorkStartTime.Minute / 60.0;
                vehicle.DrivingTimeLimit = model.DrivingTimeLimit.Hour + model.DrivingTimeLimit.Minute / 60.0;
                vehicle.DistanceLimit = model.DistanceLimit;
                vehicle.IsActive = model.IsActive;
                vehicle.CostPerKilometer = model.CostPerKilometer;
                vehicle.FixedCostPerTrip = model.FixedCostPerTrip;
                vehicle.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                vehicle.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Vehicles");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.VehiclesDeleteVehicle)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var vehicle = db.Vehicles.FirstOrDefault(x => x.Id == id);
                if (vehicle != null)
                {
                    vehicle.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<VehicleViewModel> GetVehicles()
        {
            var vehicles = (from vehicle in DB.Vehicles
                            where !vehicle.IsDeleted
                            select new VehicleViewModel()
                                   {
                                       Id = vehicle.Id,
                                       Color = vehicle.Color,
                                       MileAge = vehicle.MileAge,
                                       VehicleNumber = vehicle.VehicleNumber,
                                       VehicleType = vehicle.VehicleType,
                                       CreatedOn = vehicle.CreatedOn,
                                       UpdatedOn = vehicle.UpdatedOn,
                                       Capacity = vehicle.Capacity,
                                       CostPerKilometer = vehicle.CostPerKilometer,
                                       FixedCostPerTrip = vehicle.FixedCostPerTrip,
                                       IsActive = vehicle.IsActive,
                                       DistanceLimit = vehicle.DistanceLimit,
                                       WorkStartTime = DbFunctions.CreateDateTime(1, 1, 1, (int) vehicle.WorkStartTime, 60 * (int) (vehicle.WorkStartTime - (int) vehicle.WorkStartTime), 0).Value,
                                       DrivingTimeLimit = DbFunctions.CreateDateTime(1, 1, 1, (int) vehicle.DrivingTimeLimit, 60 * (int) (vehicle.DrivingTimeLimit - (int) vehicle.DrivingTimeLimit), 0).Value,
                                       WorkingTimeLimit = DbFunctions.CreateDateTime(1, 1, 1, (int) vehicle.WorkingTimeLimit, 60 * (int) (vehicle.WorkingTimeLimit - (int) vehicle.WorkingTimeLimit), 0).Value,
                                   });

            return vehicles;
        }

        #endregion
    }
}
