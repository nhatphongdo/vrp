﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using Courier_Management_System.Controllers;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using System.Web.Mvc.Html;
using DevExpress.Data.Filtering;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Data.Linq;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "ReportsDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities)System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrderEvents)]
        public ActionResult Orders()
        {
            ViewBag.OrderStatus = (int)OrderStatuses.OrderConfirmed;
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrderEvents)]
        public ActionResult OrdersListViewPartial(int? status, DateTime? start, DateTime? end, DateTime? startDate, DateTime? endDate)
        {
            ViewBag.OrderStatus = status;
            ViewBag.StartDate = start ?? startDate;
            ViewBag.EndDate = end ?? endDate;
            return PartialView("_OrdersListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowInvoicesReport)]
        public ActionResult Invoices()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowInvoicesReport)]
        public ActionResult InvoicesListViewPartial(DateTime? start, DateTime? end, long? id, DateTime? startDate, DateTime? endDate)
        {
            if (id.HasValue)
            {
                // Send invoice to user
                var payment = db.Payments.FirstOrDefault(x => x.Id == id);
                if (payment != null)
                {
                    var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(payment.Payer));
                    if (customer != null)
                    {
                        // Get Invoice content
                        try
                        {
                            var routeData = new RouteData();
                            routeData.Values.Add("id", id);
                            var controller = ViewRenderer.CreateController<OrderController>(routeData);
                            var model = ((ViewResult)controller.Invoice(id.ToString())).Model;
                            var emailTemplate = ViewRenderer.RenderView("~/views/order/invoice.cshtml", model, controller.ControllerContext);

                            var link = Url.Action(
                                                  "Invoice",
                                                  "Order",
                                                  new
                                                  {
                                                      id = id
                                                  },
                                                  Request.Url.Scheme);

                            var result = AccountHelper.SendEmailSync(payment.Payer, "Invoice", emailTemplate);
                            if (result.Code == 0)
                            {
                                payment.InvoiceSentOn = DateTime.Now;
                                payment.InvoiceSent = true;
                                db.SaveChanges();
                            }
                        }
                        catch (Exception exc)
                        {
                            LogHelper.Log(exc);
                        }
                    }
                }
            }

            ViewBag.StartDate = start ?? startDate;
            ViewBag.EndDate = end ?? endDate;
            return PartialView("_InvoicesListViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersExportInvoicesReport)]
        public ActionResult ExportInvoices(string type, DateTime? startDate, DateTime? endDate)
        {
            var invoices = GetInvoices(startDate, endDate).ToList()
                                                          .Select(
                                                                  x => new
                                                                  {
                                                                      x.Id,
                                                                      x.Payer,
                                                                      x.AccountType,
                                                                      x.CreatedOn,
                                                                      x.InvoiceSent,
                                                                      x.InvoiceSentOn,
                                                                      OrdersText = string.Join(",", x.Orders),
                                                                      x.PaymentAccountId,
                                                                      x.PaymentAccount,
                                                                      x.PaymentAmount,
                                                                      x.Customer
                                                                  });
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetGridViewExportSettings(), invoices.ToList(), "Invoices-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetGridViewExportSettings(), invoices.ToList(), "Invoices-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetGridViewExportSettings(), invoices.ToList(), "Invoices-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewInvoices";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("Payer");
            settings.Columns.Add("Customer.CustomerCompany");
            var column = settings.Columns.Add("AccountType", MVCxGridViewColumnType.ComboBox);
            var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = EnumHelpers.ConvertEnumToSelectList<PaymentTypes>();
            comboBoxProperties.TextField = "Text";
            comboBoxProperties.ValueField = "Value";
            comboBoxProperties.ValueType = typeof(int);
            settings.Columns.Add("PaymentAmount").PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("OrdersText");
            settings.Columns.Add("InvoiceSent");
            settings.Columns.Add("InvoiceSentOn").Caption = "Last Invoice Sent";
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowJobsReport)]
        public ActionResult Jobs()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchShowJobsReport)]
        public ActionResult JobsListViewPartial(DateTime? start, DateTime? end)
        {
            ViewBag.StartDate = start;
            ViewBag.EndDate = end;
            return PartialView("_JobsListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.DispatchTrackDriversLocation)]
        public ActionResult TrackLocations()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetLocations()
        {
            var locations = db.TrackLocations
                              .GroupBy(x => x.CreatedBy)
                              .Select(
                                      g => new
                                      {
                                          Driver = g.Key,
                                          Location = g.OrderByDescending(x => x.CreatedOn)
                                                           .Select(
                                                                   x => new
                                                                   {
                                                                       x.ConsignmentNumber,
                                                                       x.Latitude,
                                                                       x.Longitude,
                                                                       x.CreatedOn,
                                                                       x.Job.Vehicle.VehicleNumber,
                                                                       x.Job.DriverType,
                                                                       DriverName = x.CreatedBy,
                                                                       Tasks = x.Job.TotalPickups + x.Job.TotalDeliveries,
                                                                       x.Job.TasksCompleted
                                                                   })
                                                           .FirstOrDefault()
                                      })
                              .ToList();

            return Json(locations);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowSalesReport)]
        public ActionResult Sales()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowSalesReport)]
        public ActionResult SalesListViewPartial(DateTime? start, DateTime? end, long? id, DateTime? startDate, DateTime? endDate)
        {
            if (id.HasValue)
            {
                // Send invoice to user
                var payment = db.Payments.FirstOrDefault(x => x.Id == id);
                if (payment != null)
                {
                    var customer = db.Customers.FirstOrDefault(x => x.UserName.Equals(payment.Payer));
                    if (customer != null)
                    {
                        // Get Invoice content
                        try
                        {
                            var routeData = new RouteData();
                            routeData.Values.Add("id", id);
                            var controller = ViewRenderer.CreateController<OrderController>(routeData);
                            var model = ((ViewResult)controller.Invoice(id.ToString())).Model;
                            var emailTemplate = ViewRenderer.RenderView("~/views/order/invoice.cshtml", model, controller.ControllerContext);

                            var link = Url.Action(
                                                  "Invoice",
                                                  "Order",
                                                  new
                                                  {
                                                      id = id
                                                  },
                                                  Request.Url.Scheme);

                            var result = AccountHelper.SendEmailSync(payment.Payer, "Invoice", emailTemplate);
                            if (result.Code == 0)
                            {
                                payment.InvoiceSentOn = DateTime.Now;
                                payment.InvoiceSent = true;
                                db.SaveChanges();
                            }
                        }
                        catch (Exception exc)
                        {
                            LogHelper.Log(exc);
                        }
                    }
                }
            }

            ViewBag.StartDate = start ?? startDate;
            ViewBag.EndDate = end ?? endDate;
            return PartialView("_SalesListViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersExportSalesReport)]
        public ActionResult ExportSales(string type, DateTime? startDate, DateTime? endDate)
        {
            var sales = GetSales(startDate, endDate).ToList()
                                                    .Select(
                                                            x => new
                                                            {
                                                                x.Id,
                                                                x.Payer,
                                                                x.AccountType,
                                                                x.CreatedOn,
                                                                x.InvoiceSent,
                                                                x.InvoiceSentOn,
                                                                OrdersText = string.Join(",", x.Orders),
                                                                x.PaymentAccountId,
                                                                x.PaymentAccount,
                                                                x.PaymentAmount,
                                                                x.Remark,
                                                                x.Customer
                                                            });
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetSalesGridViewExportSettings(), sales.ToList(), "Sales-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetSalesGridViewExportSettings(), sales.ToList(), "Sales-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetSalesGridViewExportSettings(), sales.ToList(), "Sales-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetSalesGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewInvoices";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("Customer.Salesperson");
            settings.Columns.Add("Customer.CustomerName");
            settings.Columns.Add("Customer.CustomerCompany");
            settings.Columns.Add("Customer.PaymentType.PaymentTypeName");
            var column = settings.Columns.Add("AccountType", MVCxGridViewColumnType.ComboBox);
            var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
            comboBoxProperties.DataSource = EnumHelpers.ConvertEnumToSelectList<PaymentTypes>();
            comboBoxProperties.TextField = "Text";
            comboBoxProperties.ValueField = "Value";
            comboBoxProperties.ValueType = typeof(int);
            settings.Columns.Add("PaymentAmount").PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("OrdersText");
            settings.Columns.Add("Customer.Credit").PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("Remark");
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowManagementReport)]
        public ActionResult Management(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null)
            {
                startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }
            if (endDate == null)
            {
                endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            }
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            endDate = endDate.Value.AddDays(1);

            ViewBag.TotalOrders = db.Orders.Count(x => !x.IsDeleted && x.Status != (int)OrderStatuses.OrderSubmitted && x.Status != (int)OrderStatuses.OrderRejected && x.CreatedOn >= startDate && x.CreatedOn < endDate);
            ViewBag.TotalDelivered = db.Orders.Count(x => !x.IsDeleted && x.Status == (int)OrderStatuses.ItemDelivered && x.CreatedOn >= startDate && x.CreatedOn < endDate);
            ViewBag.TotalFailed = db.Orders.Count(x => !x.IsDeleted && x.Status == (int)OrderStatuses.DeliveryFailed && x.CreatedOn >= startDate && x.CreatedOn < endDate);
            ViewBag.TotalCancelled = db.Orders.Count(x => !x.IsDeleted && x.Status == (int)OrderStatuses.OrderCanceled && x.CreatedOn >= startDate && x.CreatedOn < endDate);
            ViewBag.TotalRevenue = db.Payments.Where(x => x.IsSuccessful && x.CreatedOn >= startDate && x.CreatedOn < endDate).Select(x => x.PaymentAmount).ToList().Sum();
            var lastMonthStart = startDate.Value.AddMonths(-1);
            var lastMonthEnd = endDate.Value.AddMonths(-1);
            ViewBag.LastRevenue = db.Payments.Where(x => x.IsSuccessful && x.CreatedOn >= lastMonthStart && x.CreatedOn < lastMonthEnd).Select(x => x.PaymentAmount).ToList().Sum();

            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsOpsDeliverySummary)]
        public ActionResult OPSDeliverySummary()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsOpsDeliverySummary)]
        public ActionResult OPSDeliverySummaryListViewPartial(DateTime? start, DateTime? end)
        {
            ViewBag.StartDate = start ?? DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = end ?? DateTime.Now.Date;
            return PartialView("_OPSDeliverySummaryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportOpsDeliverySummary)]
        public ActionResult ExportOPSDeliverySummary(string type, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(
                                                         GetOPSDeliverySummaryGridViewExportSettings(),
                                                         GetOPSDeliverySummary(startDate, endDate).ToList(),
                                                         "OPSDeliverySummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(
                                                          GetOPSDeliverySummaryGridViewExportSettings(),
                                                          GetOPSDeliverySummary(startDate, endDate).ToList(),
                                                          "OPSDeliverySummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(
                                                         GetOPSDeliverySummaryGridViewExportSettings(),
                                                         GetOPSDeliverySummary(startDate, endDate).ToList(),
                                                         "OPSDeliverySummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    ViewBag.StartDate = startDate ?? DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
                    ViewBag.EndDate = endDate ?? DateTime.Now.Date;
                    return View("OPSDeliverySummary");
                    //return new EmptyResult();
            }
        }

        private GridViewSettings GetOPSDeliverySummaryGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewReport";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("PostalCode");
            settings.Columns.Add("Quantity");
            settings.Columns.Add("A");
            settings.Columns.Add("B");
            settings.Columns.Add("C");
            settings.Columns.Add("D");
            settings.Columns.Add("Z");
            settings.Columns.Add("MD");
            settings.Columns.Add("AD");
            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;
            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsOpsPickupSummary)]
        public ActionResult OPSPickUpSummary()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsOpsPickupSummary)]
        public ActionResult OPSPickUpSummaryListViewPartial(DateTime? start, DateTime? end)
        {
            ViewBag.StartDate = start ?? DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = end ?? DateTime.Now.Date;
            return PartialView("_OPSPickUpSummaryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportOpsPickupSummary)]
        public ActionResult ExportOPSPickUpSummary(string type, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetOPSPickUpSummaryGridViewExportSettings(), GetOPSPickUpSummary(startDate, endDate).ToList(), "OPSPickUpSummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetOPSPickUpSummaryGridViewExportSettings(), GetOPSPickUpSummary(startDate, endDate).ToList(), "OPSPickUpSummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetOPSPickUpSummaryGridViewExportSettings(), GetOPSPickUpSummary(startDate, endDate).ToList(), "OPSPickUpSummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    ViewBag.StartDate = startDate ?? DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
                    ViewBag.EndDate = endDate ?? DateTime.Now.Date;
                    return View("OPSPickUpSummary");
                    //return new EmptyResult();
            }
        }

        private GridViewSettings GetOPSPickUpSummaryGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewReport";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("PostalCode");
            settings.Columns.Add("Quantity");
            settings.Columns.Add("A");
            settings.Columns.Add("B");
            settings.Columns.Add("C");
            settings.Columns.Add("D");
            settings.Columns.Add("Z");
            settings.Columns.Add("MD");
            settings.Columns.Add("AD");
            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;
            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsBillingSummary)]
        public ActionResult BillingSummary()
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = startDate.AddMonths(1).AddDays(-1);
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsBillingSummary)]
        public ActionResult BillingSummaryListViewPartial(DateTime? startDate, DateTime? endDate)
        {
            DateTime now = DateTime.Now;
            startDate = startDate ?? new DateTime(now.Year, now.Month, 1);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate ?? startDate?.AddMonths(1).AddDays(-1);
            return PartialView("_BillingSummaryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportBillingSummary)]
        public ActionResult ExportBillingSummary(string type, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetBillingSummaryGridViewExportSettings(), GetBillingSummary(startDate, endDate).ToList(), "BillingSummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetBillingSummaryGridViewExportSettings(), GetBillingSummary(startDate, endDate).ToList(), "BillingSummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetBillingSummaryGridViewExportSettings(), GetBillingSummary(startDate, endDate).ToList(), "BillingSummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetBillingSummaryGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewReport";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("SalesPerson");
            settings.Columns.Add("CustomerName");
            settings.Columns.Add("CompanyName");
            settings.Columns.Add("AccountType");
            settings.Columns.Add("PaymentType");
            settings.Columns.Add("PricePlan");
            settings.Columns.Add("BillingAmount");
            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;
            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsBillingReport)]
        public ActionResult Billing()
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = startDate.AddMonths(1).AddDays(-1);
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsBillingReport)]
        public ActionResult BillingListViewPartial(string cod, string codCollected, DateTime? startDate, DateTime? endDate)
        {
            ViewBag.COD = cod ?? Request["COD"];
            ViewBag.CODCollected = codCollected ?? Request["CODCollected"];
            DateTime now = DateTime.Now;
            startDate = startDate ?? new DateTime(now.Year, now.Month, 1);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate ?? startDate?.AddMonths(1).AddDays(-1);
            return PartialView("_BillingListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsSalesDeliveryReport)]
        public ActionResult SalesDelivery()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsSalesDeliveryReport)]
        public ActionResult SalesDeliveryListViewPartial(DateTime? start, DateTime? end)
        {
            ViewBag.StartDate = start ?? DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = end ?? DateTime.Now.Date;
            return PartialView("_SalesDeliveryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportBillingReport)]
        public ActionResult ExportBillingReport(string type, DateTime? startDate, DateTime? endDate)
        {
            DateTime now = DateTime.Now;
            startDate = startDate ?? new DateTime(now.Year, now.Month, 1);
            endDate = endDate ?? startDate?.AddMonths(1).AddDays(-1);

            var conditions = CriteriaOperator.Parse(Request["FilterExpression"]);
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(
                                                         GetBillingReportGridViewExportSettings(),
                                                         GetExportedBillingDetails(Request["COD"], Request["CODCollected"], startDate, endDate, conditions).ToList(),
                                                         "BillingReport-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(
                                                          GetBillingReportGridViewExportSettings(),
                                                          GetExportedBillingDetails(Request["COD"], Request["CODCollected"], startDate, endDate, conditions).ToList(),
                                                          "BillingReport-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(
                                                         GetBillingReportGridViewExportSettings(),
                                                         GetExportedBillingDetails(Request["COD"], Request["CODCollected"], startDate, endDate, conditions).ToList(),
                                                         "BillingReport-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetBillingReportGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewOrders";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "Status";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = EnumHelper.GetSelectList(typeof(OrderStatuses));
                                     comboBoxProperties.TextField = "Text";
                                     comboBoxProperties.ValueField = "Value";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add("ConsignmentNumber");
            settings.Columns.Add("OrderNumber");
            settings.Columns.Add("Cod").Caption = "COD";
            settings.Columns.Add("CodCollected").Caption = "COD Collected";
            settings.Columns.Add("Remark").Caption = "Customer Remark";
            settings.Columns.Add("IsExchange").Caption = "Exchange?";
            settings.Columns.Add("CustomerCompany");
            settings.Columns.Add("FromZipCode");
            settings.Columns.Add("FromAddress");
            settings.Columns.Add("PickupDate");
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "PickupTimeSlotId";
                                     column.Caption = "Pickup Time Slot";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;
                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = TimeSlotsController.GetTimeSlots().ToList().Select(
                                                                                                                        x => new
                                                                                                                        {
                                                                                                                            Id = x.Id,
                                                                                                                            Name = x.Name,
                                                                                                                            FromTime = x.FromTime.ToString("HH:mm"),
                                                                                                                            ToTime = x.ToTime.ToString("HH:mm")
                                                                                                                        }).ToList();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.TextFormatString = "{0} ({1} - {2})";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                     comboBoxProperties.Columns.Add("Name", "Time Slot Name", Unit.Pixel(200));
                                     comboBoxProperties.Columns.Add("FromTime");
                                     comboBoxProperties.Columns.Add("ToTime");
                                 });

            settings.Columns.Add("ToName");
            settings.Columns.Add("ToZipCode");
            settings.Columns.Add("ToAddress");
            settings.Columns.Add("ToMobilePhone");
            settings.Columns.Add("DeliveryDate");
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "DeliveryTimeSlotId";
                                     column.Caption = "Delivery Time Slot";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = TimeSlotsController.GetTimeSlots().ToList().Select(
                                                                                                                        x => new
                                                                                                                        {
                                                                                                                            Id = x.Id,
                                                                                                                            Name = x.Name,
                                                                                                                            FromTime = x.FromTime.ToString("HH:mm"),
                                                                                                                            ToTime = x.ToTime.ToString("HH:mm")
                                                                                                                        }).ToList();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.TextFormatString = "{0} ({1} - {2})";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                     comboBoxProperties.Columns.Add("Name", "Time Slot Name", Unit.Pixel(200));
                                     comboBoxProperties.Columns.Add("FromTime");
                                     comboBoxProperties.Columns.Add("ToTime");
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "ServiceId";
                                     column.Caption = "Service";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetServices();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "SizeId";
                                     column.Caption = "Size";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = OrdersController.GetSizes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            var col = settings.Columns.Add("Price");
            col.PropertiesEdit.DisplayFormatString = "C";
            settings.Columns.Add("CreatedOn");
            settings.Columns.Add("FullfilmentNumber");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportSalesDeliveryReport)]
        public ActionResult ExportSalesDelivery(string type, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetSalesDeliveryGridViewExportSettings(), GetSalesDelivery(startDate, endDate).ToList(), "SalesDelivery-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetSalesDeliveryGridViewExportSettings(), GetSalesDelivery(startDate, endDate).ToList(), "SalesDelivery-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetSalesDeliveryGridViewExportSettings(), GetSalesDelivery(startDate, endDate).ToList(), "SalesDelivery-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetSalesDeliveryGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewReport";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("SalesPerson");
            settings.Columns.Add("CompanyName");
            var col = settings.Columns.Add("TotalDeliveried");
            col.PropertiesEdit.DisplayFormatString = "N0";
            settings.Columns.Add("PaymentType");
            col = settings.Columns.Add("CreditBalance");
            col.PropertiesEdit.DisplayFormatString = "C";
            settings.Columns.Add("PricePlan");
            settings.Columns.Add("UpdatedOn");
            settings.Columns.Add("CreatedOn");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsSalesServiceReport)]
        public ActionResult SalesService()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsSalesServiceReport)]
        public ActionResult SalesServiceListViewPartial(DateTime? start, DateTime? end)
        {
            ViewBag.StartDate = start ?? DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = end ?? DateTime.Now.Date;
            return PartialView("_SalesServiceListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportSalesServiceReport)]
        public ActionResult ExportSalesService(string type, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetSalesServiceGridViewExportSettings(), GetSalesService(startDate, endDate).ToList(), "SalesService-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetSalesServiceGridViewExportSettings(), GetSalesService(startDate, endDate).ToList(), "SalesService-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetSalesServiceGridViewExportSettings(), GetSalesService(startDate, endDate).ToList(), "SalesService-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetSalesServiceGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewReport";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("Service").Width = Unit.Pixel(200);
            settings.Columns.AddBand(
                                     row =>
                                     {
                                         row.Caption = "Document";
                                         row.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                                         var col = row.Columns.Add("DocumentCount");
                                         col.Caption = "Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "N0";
                                         col = row.Columns.Add("DocumentCountPercent");
                                         col.Caption = "% Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                         col = row.Columns.Add("DocumentPrice");
                                         col.Caption = "Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "C";
                                         col = row.Columns.Add("DocumentPricePercent");
                                         col.Caption = "% Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                     });
            settings.Columns.AddBand(
                                     row =>
                                     {
                                         row.Caption = "Extra Large";
                                         row.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                                         var col = row.Columns.Add("ExtraLargeCount");
                                         col.Caption = "Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "N0";
                                         col = row.Columns.Add("ExtraLargeCountPercent");
                                         col.Caption = "% Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                         col = row.Columns.Add("ExtraLargePrice");
                                         col.Caption = "Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "C";
                                         col = row.Columns.Add("ExtraLargePricePercent");
                                         col.Caption = "% Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                     });
            settings.Columns.AddBand(
                                     row =>
                                     {
                                         row.Caption = "Large";
                                         row.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                                         var col = row.Columns.Add("LargeCount");
                                         col.Caption = "Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "N0";
                                         col = row.Columns.Add("LargeCountPercent");
                                         col.Caption = "% Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                         col = row.Columns.Add("LargePrice");
                                         col.Caption = "Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "C";
                                         col = row.Columns.Add("LargePricePercent");
                                         col.Caption = "% Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                     });
            settings.Columns.AddBand(
                                     row =>
                                     {
                                         row.Caption = "Medium";
                                         row.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                                         var col = row.Columns.Add("MediumCount");
                                         col.Caption = "Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "N0";
                                         col = row.Columns.Add("MediumCountPercent");
                                         col.Caption = "% Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                         col = row.Columns.Add("MediumPrice");
                                         col.Caption = "Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "C";
                                         col = row.Columns.Add("MediumPricePercent");
                                         col.Caption = "% Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                     });
            settings.Columns.AddBand(
                                     row =>
                                     {
                                         row.Caption = "Small";
                                         row.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                                         var col = row.Columns.Add("SmallCount");
                                         col.Caption = "Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "N0";
                                         col = row.Columns.Add("SmallCountPercent");
                                         col.Caption = "% Count";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                         col = row.Columns.Add("SmallPrice");
                                         col.Caption = "Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "C";
                                         col = row.Columns.Add("SmallPricePercent");
                                         col.Caption = "% Price";
                                         col.Width = Unit.Pixel(150);
                                         col.PropertiesEdit.DisplayFormatString = "P3";
                                     });

            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "DocumentCount").DisplayFormat = "N0";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "DocumentCountPercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "DocumentPrice").DisplayFormat = "C";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "DocumentPricePercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "ExtraLargeCount").DisplayFormat = "N0";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "ExtraLargeCountPercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "ExtraLargePrice").DisplayFormat = "C";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "ExtraLargePricePercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "LargeCount").DisplayFormat = "N0";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "LargeCountPercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "LargePrice").DisplayFormat = "C";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "LargePricePercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "MediumCount").DisplayFormat = "N0";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "MediumCountPercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "MediumPrice").DisplayFormat = "C";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "MediumPricePercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SmallCount").DisplayFormat = "N0";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SmallCountPercent").DisplayFormat = "P3";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SmallPrice").DisplayFormat = "C";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SmallPricePercent").DisplayFormat = "P3";

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsFailedDeliveryReport)]
        public ActionResult FailedDelivery()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1));
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsFailedDeliveryReport)]
        public ActionResult FailedDeliveryListViewPartial(long? customer, DateTime? start, DateTime? end)
        {
            ViewBag.Customer = customer;
            ViewBag.StartDate = start ?? DateTime.Now.Subtract(TimeSpan.FromDays(1));
            ViewBag.EndDate = end ?? DateTime.Now.Date;
            return PartialView("_FailedDeliveryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportFailedDeliveryReport)]
        public ActionResult ExportFailedDelivery(string type, long? customerId, DateTime? startDate, DateTime? endDate)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(
                                                         GetFailedDeliveryGridViewExportSettings(),
                                                         GetFailedDelivery(customerId, startDate, endDate).ToList(),
                                                         "FailedDelivery-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(
                                                          GetFailedDeliveryGridViewExportSettings(),
                                                          GetFailedDelivery(customerId, startDate, endDate).ToList(),
                                                          "FailedDelivery-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(
                                                         GetFailedDeliveryGridViewExportSettings(),
                                                         GetFailedDelivery(customerId, startDate, endDate).ToList(),
                                                         "FailedDelivery-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetFailedDeliveryGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewReport";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("Status");
            var col = settings.Columns.Add("Count");
            col.PropertiesEdit.DisplayFormatString = "N0";
            col = settings.Columns.Add("Percent");
            col.PropertiesEdit.DisplayFormatString = "P3";

            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "Count").DisplayFormat = "N0";
            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "Percent").DisplayFormat = "P";

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsJobHistoryReport)]
        public ActionResult JobsHistory()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsJobHistoryReport)]
        public ActionResult JobsHistoryListViewPartial()
        {
            return PartialView("_JobsHistoryListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsCodReport)]
        public ActionResult Cod()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsCodReport)]
        public ActionResult CodListViewPartial(DateTime? start, DateTime? end)
        {
            ViewBag.StartDate = start ?? DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            ViewBag.EndDate = end ?? DateTime.Now.Date;
            return PartialView("_CodListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ReportsExportCodReport)]
        public ActionResult ExportCod(string type, DateTime? startDate, DateTime? endDate)
        {
            var data = GetCod(startDate, endDate)
                .ToList()
                .Select(
                        item => new
                        {
                            item.CustomerName,
                            item.UserName,
                            item.CashAmount,
                            item.ChequeAmount,
                            CollectedCash = string.Join("\n", item.CollectedCash.Select(x => x.CollectedAmount.ToString("C") + " on " + x.CollectedOn.ToString("dd MMM, yyyy"))),
                            CollectedCheque = string.Join("\n", item.CollectedCheque.Select(x => x.CollectedAmount.ToString("C") + " on " + x.CollectedOn.ToString("dd MMM, yyyy")))
                        })
                .ToList();

            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(
                                                         GetCodGridViewExportSettings(),
                                                         data,
                                                         "Cod-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(
                                                          GetCodGridViewExportSettings(),
                                                          data,
                                                          "Cod-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(
                                                         GetCodGridViewExportSettings(),
                                                         data,
                                                         "Cod-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetCodGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewCod";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("CustomerName");
            settings.Columns.Add("UserName");
            var col = settings.Columns.Add("CashAmount");
            col.PropertiesEdit.DisplayFormatString = "C";
            col = settings.Columns.Add("ChequeAmount");
            col.PropertiesEdit.DisplayFormatString = "C";
            settings.Columns.Add("CollectedCash");
            settings.Columns.Add("CollectedCheque");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        #region Data queries

        public static IQueryable<OrderViewModel> GetOrders(int? statusValue, DateTime? start, DateTime? end)
        {
            var orders = OrdersController.GetOrders();

            if (!statusValue.HasValue)
            {
                return orders;
            }

            var status = (OrderStatuses)statusValue;

            switch (status)
            {
                case OrderStatuses.DeliveryFailed:
                case OrderStatuses.ItemDelivered:
                    if (start.HasValue && end.HasValue)
                    {
                        orders = orders.Where(x => x.Status == status && DbFunctions.TruncateTime(x.DeliveredOn) >= start && DbFunctions.TruncateTime(x.DeliveredOn) <= end);
                    }
                    break;
                case OrderStatuses.ItemCollected:
                case OrderStatuses.PickupFailed:
                    if (start.HasValue && end.HasValue)
                    {
                        orders = orders.Where(x => x.Status == status && DbFunctions.TruncateTime(x.CollectedOn) >= start && DbFunctions.TruncateTime(x.CollectedOn) <= end);
                    }
                    break;
                case OrderStatuses.OrderSubmitted:
                    if (start.HasValue && end.HasValue)
                    {
                        orders = orders.Where(x => x.Status == status && DbFunctions.TruncateTime(x.CreatedOn) >= start && DbFunctions.TruncateTime(x.CreatedOn) <= end);
                    }
                    break;
                case OrderStatuses.OrderConfirmed:
                case OrderStatuses.OrderRejected:
                    if (start.HasValue && end.HasValue)
                    {
                        orders = orders.Where(x => x.Status == status && DbFunctions.TruncateTime(x.ConfirmedOn) >= start && DbFunctions.TruncateTime(x.ConfirmedOn) <= end);
                    }
                    break;
                case OrderStatuses.OrderCanceled:
                    if (start.HasValue && end.HasValue)
                    {
                        orders = orders.Where(x => x.Status == status && DbFunctions.TruncateTime(x.CanceledOn) >= start && DbFunctions.TruncateTime(x.CanceledOn) <= end);
                    }
                    break;
                case OrderStatuses.ItemInDepot:
                    if (start.HasValue && end.HasValue)
                    {
                        orders = orders.Where(x => x.Status == status && DbFunctions.TruncateTime(x.ProceededOn) >= start && DbFunctions.TruncateTime(x.ProceededOn) <= end);
                    }
                    break;
                case OrderStatuses.ItemOutDepot:
                    if (start.HasValue && end.HasValue)
                    {
                        orders = orders.Where(x => x.Status == status && DbFunctions.TruncateTime(x.ProceededOutOn) >= start && DbFunctions.TruncateTime(x.ProceededOutOn) <= end);
                    }
                    break;
            }

            return orders;
        }

        public static IQueryable<dynamic> GetInvoices(DateTime? start, DateTime? end)
        {
            if (!start.HasValue || !end.HasValue)
            {
                start = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
                end = DateTime.Now.Date;
            }

            var payments = (from payment in DB.Payments
                            from customer in DB.Customers
                            where payment.IsSuccessful && DbFunctions.TruncateTime(payment.CreatedOn) >= start && DbFunctions.TruncateTime(payment.CreatedOn) <= end
                                  && customer.UserName.Equals(payment.Payer, StringComparison.OrdinalIgnoreCase)
                            select new
                            {
                                payment.Id,
                                payment.Payer,
                                payment.AccountType,
                                payment.CreatedOn,
                                payment.InvoiceSent,
                                payment.InvoiceSentOn,
                                Orders = payment.OrdersOfPayments.Select(o => o.ConsignmentNumber),
                                payment.PaymentAccountId,
                                payment.PaymentAccount,
                                payment.PaymentAmount,
                                Customer = customer
                            });

            return payments;
        }

        public static IQueryable<dynamic> GetSales(DateTime? start, DateTime? end)
        {
            if (!start.HasValue || !end.HasValue)
            {
                start = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
                end = DateTime.Now.Date;
            }

            var payments = (from payment in DB.Payments
                            from customer in DB.Customers
                            where payment.IsSuccessful && DbFunctions.TruncateTime(payment.CreatedOn) >= start && DbFunctions.TruncateTime(payment.CreatedOn) <= end
                                  && customer.UserName.Equals(payment.Payer, StringComparison.OrdinalIgnoreCase)
                            select new
                            {
                                payment.Id,
                                payment.Payer,
                                payment.AccountType,
                                payment.CreatedOn,
                                payment.InvoiceSent,
                                payment.InvoiceSentOn,
                                Orders = payment.OrdersOfPayments.Select(o => o.ConsignmentNumber),
                                payment.PaymentAccountId,
                                payment.PaymentAccount,
                                payment.PaymentAmount,
                                payment.Remark,
                                Customer = customer
                            });

            return payments;
        }

        public static IQueryable<DriverReportViewModel> GetDrivers(DateTime? start, DateTime? end)
        {
            var contractors = (from job in DB.Jobs
                               from person in DB.Contractors
                               from user in DB.AspNetUsers
                               where !job.IsDeleted && job.DriverType == (int)AccountTypes.Contractor && person.Id == job.DriverId &&
                                     DbFunctions.TruncateTime(job.CreatedOn) >= start && DbFunctions.TruncateTime(job.CreatedOn) <= end &&
                                     !person.IsDeleted && user.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)
                               select new DriverReportViewModel()
                               {
                                   Id = "Contractor_" + person.Id,
                                   Name = person.ContractorName,
                                   UserName = person.UserName,
                                   CountryCode = person.ContractorCountry,
                                   MobileNumber = person.ContractorMobile,
                                   VehicleType = person.VehicleType,
                                   DrivingLicense = person.DrivingLicense,
                                   Email = user.Email,
                                   VehicleNumber = person.VehicleNumber,
                                   AccountType = AccountTypes.Contractor,
                                   ICNumber = person.ICNumber,
                                   TakenOn = job.TakenOn,
                                   CompletedOn = job.CompletedOn,
                                   TotalDeliveries = job.TotalDeliveries,
                                   TotalPickups = job.TotalPickups,
                                   TotalDistances = job.TotalDistances,
                                   TotalDrivingTime = job.TotalDrivingTime,
                                   TotalWorkingTime = job.TotalWorkingTime,
                                   JobId = job.Id,
                                   IsSpecialJob = job.IsSpecial
                               });

            var staffs = (from job in DB.Jobs
                          from person in DB.Staffs
                          from user in DB.AspNetUsers
                          join vehicle in DB.Vehicles on job.VehicleId equals vehicle.Id into vehicleGroup
                          where !job.IsDeleted && job.DriverType == (int)AccountTypes.Staff && person.Id == job.DriverId &&
                                DbFunctions.TruncateTime(job.CreatedOn) >= start && DbFunctions.TruncateTime(job.CreatedOn) <= end &&
                                !person.IsDeleted && user.UserName.Equals(person.UserName, StringComparison.OrdinalIgnoreCase)
                          select new DriverReportViewModel()
                          {
                              Id = "Staff_" + person.Id,
                              Name = person.StaffName,
                              UserName = person.UserName,
                              CountryCode = person.StaffCountry,
                              MobileNumber = person.StaffMobile,
                              VehicleType = vehicleGroup.Select(v => v.VehicleType).FirstOrDefault(),
                              DrivingLicense = person.DrivingLicense,
                              Email = user.Email,
                              VehicleNumber = vehicleGroup.Select(v => v.VehicleNumber).FirstOrDefault(),
                              AccountType = AccountTypes.Staff,
                              ICNumber = person.ICNumber,
                              TakenOn = job.TakenOn,
                              CompletedOn = job.CompletedOn,
                              TotalDeliveries = job.TotalDeliveries,
                              TotalPickups = job.TotalPickups,
                              TotalDistances = job.TotalDistances,
                              TotalDrivingTime = job.TotalDrivingTime,
                              TotalWorkingTime = job.TotalWorkingTime,
                              JobId = job.Id,
                              IsSpecialJob = job.IsSpecial
                          });

            return contractors.Union(staffs);
        }

        public static IQueryable<dynamic> GetOPSDeliverySummary(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null)
                startDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            if (endDate == null)
                endDate = DateTime.Now.Date;

            var orders = (from order in DB.Orders
                          join deliveryTimeSlot in DB.TimeSlots on order.DeliveryTimeSlotId equals deliveryTimeSlot.Id
                          where !order.IsDeleted
                                && DbFunctions.TruncateTime(order.DeliveryDate) >= startDate
                                && DbFunctions.TruncateTime(order.DeliveryDate) <= endDate
                          select new
                          {
                              PostalCode = order.ToZipCode,
                              TimeSlot = deliveryTimeSlot.BinCode,
                          });

            var pivotedOrders = orders.GroupBy(s => s.PostalCode.Substring(0, 2)).OrderBy(x => x.Key)
                                      .Select(
                                              o => new
                                              {
                                                  Key = o.Key,
                                                  PostalCode = o.Key,
                                                  Quantity = o.Count(),
                                                  A = o.Count(x => x.TimeSlot == "A"),
                                                  B = o.Count(x => x.TimeSlot == "B"),
                                                  C = o.Count(x => x.TimeSlot == "C"),
                                                  D = o.Count(x => x.TimeSlot == "D"),
                                                  Z = o.Count(x => x.TimeSlot == "Z"),
                                                  MD = o.Count(x => x.TimeSlot == "MD"),
                                                  AD = o.Count(x => x.TimeSlot == "AD"),
                                              });

            return pivotedOrders;
        }

        public static IQueryable<dynamic> GetOPSPickUpSummary(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null)
                startDate = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
            if (endDate == null)
                endDate = DateTime.Now.Date;

            var orders = (from order in DB.Orders
                          join pickUpTimeSlot in DB.TimeSlots on order.PickupTimeSlotId equals pickUpTimeSlot.Id
                          where !order.IsDeleted
                                && DbFunctions.TruncateTime(order.PickupDate) >= startDate
                                && DbFunctions.TruncateTime(order.PickupDate) <= endDate
                          select new
                          {
                              PostalCode = order.FromZipCode,
                              TimeSlot = pickUpTimeSlot.BinCode,
                          });

            var pivotedOrders = orders.GroupBy(s => s.PostalCode.Substring(0, 2)).OrderBy(x => x.Key)
                                      .Select(
                                              o => new
                                              {
                                                  Key = o.Key,
                                                  PostalCode = o.Key,
                                                  Quantity = o.Count(),
                                                  A = o.Count(x => x.TimeSlot == "A"),
                                                  B = o.Count(x => x.TimeSlot == "B"),
                                                  C = o.Count(x => x.TimeSlot == "C"),
                                                  D = o.Count(x => x.TimeSlot == "D"),
                                                  Z = o.Count(x => x.TimeSlot == "Z"),
                                                  MD = o.Count(x => x.TimeSlot == "MD"),
                                                  AD = o.Count(x => x.TimeSlot == "AD"),
                                              });

            return pivotedOrders;
        }

        public static IQueryable<dynamic> GetSalesDelivery(DateTime? startDate, DateTime? endDate)
        {
            var report = (from customer in DB.Customers
                          join pricePlan in DB.CustomerPricePlans on customer.PricePlan equals pricePlan.Id into pricePlanGroup
                          where !customer.IsDeleted
                          select new
                          {
                              Id = customer.Id,
                              SalesPerson = customer.Salesperson,
                              CompanyName = customer.CustomerCompany,
                              TotalDeliveried =
                                     customer.Orders.Count(
                                                           o =>
                                                               !o.IsDeleted && o.Status == (int)OrderStatuses.ItemDelivered && DbFunctions.TruncateTime(o.CreatedOn) >= startDate
                                                               && DbFunctions.TruncateTime(o.CreatedOn) <= endDate),
                              PaymentType = customer.PaymentType.PaymentTypeName,
                              CreditBalance = customer.Credit,
                              PricePlan = pricePlanGroup.Select(p => p.PlanName).FirstOrDefault(),
                              CreatedOn = customer.CreatedOn,
                              UpdatedOn = customer.UpdatedOn
                          });

            return report;
        }

        public static IEnumerable<dynamic> GetSalesService(DateTime? startDate, DateTime? endDate)
        {
            var report = (from order in DB.Orders
                          where !order.IsDeleted && DbFunctions.TruncateTime(order.CreatedOn) >= startDate && DbFunctions.TruncateTime(order.CreatedOn) <= endDate
                          group order by new
                          {
                              Service = order.Service.ServiceName,
                              Size = order.Size.SizeName,
                              Status = order.Status
                          }
                          into orderGroup
                          select new
                          {
                              Service = orderGroup.Key.Service,
                              Size = orderGroup.Key.Size,
                              Status = (OrderStatuses)orderGroup.Key.Status,
                              Count = orderGroup.Count(),
                              Price = orderGroup.Sum(x => x.Price)
                          })
                .Concat(
                        from order in DB.Orders
                        where
                        !order.IsDeleted && DbFunctions.TruncateTime(order.CreatedOn) >= startDate && DbFunctions.TruncateTime(order.CreatedOn) <= endDate && ("," + order.PromoCode.ToLower() + ",").Contains(",qrc,")
                        group order by new
                        {
                            Status = order.Status,
                            Size = order.Size.SizeName
                        }
                        into orderGroup
                        select new
                        {
                            Service = "QRC",
                            Size = orderGroup.Key.Size,
                            Status = (OrderStatuses)orderGroup.Key.Status,
                            Count = orderGroup.Count(),
                            Price = orderGroup.Sum(x => x.Price)
                        }
                       )
                .Concat(
                        from order in DB.Orders
                        where
                        !order.IsDeleted && DbFunctions.TruncateTime(order.CreatedOn) >= startDate && DbFunctions.TruncateTime(order.CreatedOn) <= endDate
                        && order.CodOptionOfOrders.Count(c => !c.IsDeleted && !c.CodOption.IsDeleted) > 0
                        group order by new
                        {
                            Status = order.Status,
                            Size = order.Size.SizeName
                        }
                        into orderGroup
                        select new
                        {
                            Service = "COD",
                            Size = orderGroup.Key.Size,
                            Status = (OrderStatuses)orderGroup.Key.Status,
                            Count = orderGroup.Count(),
                            Price = orderGroup.Sum(x => x.Price)
                        }
                       )
                .Concat(
                        from order in DB.Orders
                        where !order.IsDeleted && DbFunctions.TruncateTime(order.CreatedOn) >= startDate && DbFunctions.TruncateTime(order.CreatedOn) <= endDate && order.IsExchange
                        group order by new
                        {
                            Status = order.Status,
                            Size = order.Size.SizeName
                        }
                        into orderGroup
                        select new
                        {
                            Service = "Exchange",
                            Size = orderGroup.Key.Size,
                            Status = (OrderStatuses)orderGroup.Key.Status,
                            Count = orderGroup.Count(),
                            Price = orderGroup.Sum(x => x.Price)
                        }
                       ).ToList();

            var result = new List<dynamic>();
            foreach (var group in report.GroupBy(x => x.Service))
            {
                result.Add(
                           new
                           {
                               Service = group.Key,
                               DocumentCount = group.Where(x => x.Size == "Document").Sum(x => x.Count),
                               DocumentCountPercent =
                               report.Where(x => x.Size == "Document").Sum(x => x.Count) == 0
                                   ? 0
                                   : (float)group.Where(x => x.Size == "Document").Sum(x => x.Count) / report.Where(x => x.Size == "Document").Sum(x => x.Count),
                               DocumentPrice = group.Where(x => x.Size == "Document").Sum(x => x.Price),
                               DocumentPricePercent =
                               report.Where(x => x.Size == "Document").Sum(x => x.Price) == 0 ? 0 : group.Where(x => x.Size == "Document").Sum(x => x.Price) / report.Where(x => x.Size == "Document").Sum(x => x.Price),
                               ExtraLargeCount = group.Where(x => x.Size == "Extra Large").Sum(x => x.Count),
                               ExtraLargeCountPercent =
                               report.Where(x => x.Size == "Extra Large").Sum(x => x.Count) == 0
                                   ? 0
                                   : (float)group.Where(x => x.Size == "Extra Large").Sum(x => x.Count) / report.Where(x => x.Size == "Extra Large").Sum(x => x.Count),
                               ExtraLargePrice = group.Where(x => x.Size == "Extra Large").Sum(x => x.Price),
                               ExtraLargePricePercent =
                               report.Where(x => x.Size == "Extra Large").Sum(x => x.Price) == 0
                                   ? 0
                                   : group.Where(x => x.Size == "Extra Large").Sum(x => x.Price) / report.Where(x => x.Size == "Extra Large").Sum(x => x.Price),
                               LargeCount = group.Where(x => x.Size == "Large").Sum(x => x.Count),
                               LargeCountPercent =
                               report.Where(x => x.Size == "Large").Sum(x => x.Count) == 0 ? 0 : (float)group.Where(x => x.Size == "Large").Sum(x => x.Count) / report.Where(x => x.Size == "Large").Sum(x => x.Count),
                               LargePrice = group.Where(x => x.Size == "Large").Sum(x => x.Price),
                               LargePricePercent =
                               report.Where(x => x.Size == "Large").Sum(x => x.Price) == 0 ? 0 : group.Where(x => x.Size == "Large").Sum(x => x.Price) / report.Where(x => x.Size == "Large").Sum(x => x.Price),
                               MediumCount = group.Where(x => x.Size == "Medium").Sum(x => x.Count),
                               MediumCountPercent =
                               report.Where(x => x.Size == "Medium").Sum(x => x.Count) == 0 ? 0 : (float)group.Where(x => x.Size == "Medium").Sum(x => x.Count) / report.Where(x => x.Size == "Medium").Sum(x => x.Count),
                               MediumPrice = group.Where(x => x.Size == "Medium").Sum(x => x.Price),
                               MediumPricePercent =
                               report.Where(x => x.Size == "Medium").Sum(x => x.Price) == 0 ? 0 : group.Where(x => x.Size == "Medium").Sum(x => x.Price) / report.Where(x => x.Size == "Medium").Sum(x => x.Price),
                               SmallCount = group.Where(x => x.Size == "Small").Sum(x => x.Count),
                               SmallCountPercent =
                               report.Where(x => x.Size == "Small").Sum(x => x.Count) == 0 ? 0 : (float)group.Where(x => x.Size == "Small").Sum(x => x.Count) / report.Where(x => x.Size == "Small").Sum(x => x.Count),
                               SmallPrice = group.Where(x => x.Size == "Small").Sum(x => x.Price),
                               SmallPricePercent =
                               report.Where(x => x.Size == "Small").Sum(x => x.Price) == 0 ? 0 : group.Where(x => x.Size == "Small").Sum(x => x.Price) / report.Where(x => x.Size == "Small").Sum(x => x.Price),
                               TotalCount = group.Sum(x => x.Count),
                               TotalCountPercent = report.Sum(x => x.Count) == 0 ? 0 : (float)group.Sum(x => x.Count) / report.Sum(x => x.Count),
                               TotalPrice = group.Sum(x => x.Price),
                               TotalPricePercent = report.Sum(x => x.Price) == 0 ? 0 : group.Sum(x => x.Price) / report.Sum(x => x.Price),
                           });
            }

            return result;
        }

        public static IEnumerable<dynamic> GetFailedDelivery(long? customer, DateTime? startDate, DateTime? endDate)
        {
            var report = (from order in DB.Orders
                          join status in DB.OrderTrackings on order.ConsignmentNumber equals status.ConsignmentNumber into statusGroup
                          where !order.IsDeleted && DbFunctions.TruncateTime(order.CreatedOn) >= startDate && DbFunctions.TruncateTime(order.CreatedOn) <= endDate
                                && (customer.HasValue == false || order.CustomerId == customer)
                          select new
                          {
                              ItemDeliveredCount = statusGroup.Count(x => x.Status == (int)OrderStatuses.ItemDelivered),
                              ReturnToSenderCount = statusGroup.Count(x => x.Status == (int)OrderStatuses.ReturnToSender),
                              ItemFailedDeliveryCount = statusGroup.Count(x => x.Status == (int)OrderStatuses.DeliveryFailed),
                              Status = order.Status,
                          });

            var itemDeliveried = report.Count(x => x.ItemDeliveredCount > 0 && x.ItemFailedDeliveryCount == 0);
            var returnToSender = report.Count(x => x.ReturnToSenderCount > 0);
            var failedOneTime = report.Count(x => x.ItemFailedDeliveryCount == 1);
            var failedTwoTimes = report.Count(x => x.ItemFailedDeliveryCount >= 2);
            var itemRedeliveried = report.Count(x => x.ItemDeliveredCount > 0 && x.ItemFailedDeliveryCount > 0);
            var total = report.Count();

            return new List<dynamic>()
                   {
                       new
                       {
                           Status = "Item is Delivered",
                           Count = itemDeliveried,
                           Percent = total == 0 ? 0 : (float) itemDeliveried / total
                       },
                       new
                       {
                           Status = "Return to Sender",
                           Count = returnToSender,
                           Percent = total == 0 ? 0 : (float) returnToSender / total
                       },
                       new
                       {
                           Status = "Failed 1st Delivery",
                           Count = failedOneTime,
                           Percent = total == 0 ? 0 : (float) failedOneTime / total
                       },
                       new
                       {
                           Status = "Failed 2nd Delivery",
                           Count = failedTwoTimes,
                           Percent = total == 0 ? 0 : (float) failedTwoTimes / total
                       },
                       new
                       {
                           Status = "Item is Redelivered",
                           Count = itemRedeliveried,
                           Percent = total == 0 ? 0 : (float) itemRedeliveried / total
                       }
                   };
        }

        public static IQueryable<dynamic> GetJobsHistory()
        {
            var result = DB.JobHistories.Where(x => !x.IsDeleted)
                           .Select(
                                   x => new
                                   {
                                       CompletedOn = x.CompletedOn,
                                       ConsignmentNumber = x.ConsignmentNumber,
                                       CreatedBy = x.CreatedBy,
                                       DeductionAmount = x.DeductionAmount,
                                       Id = x.Id,
                                       IsPickup = x.IsPickup,
                                       IsSuccessful = x.IsSuccessful,
                                       JobId = x.JobId,
                                       PayRateId = x.PayRateId,
                                       Payment = x.Payment,
                                       Rating = x.Rating,
                                       CompletedBy = DB.Staffs.Where(s => s.UserName.Equals(x.CompletedBy, StringComparison.OrdinalIgnoreCase)).Select(s => s.StaffName).FirstOrDefault() ??
                                                          DB.Contractors.Where(s => s.UserName.Equals(x.CompletedBy, StringComparison.OrdinalIgnoreCase)).Select(s => s.ContractorName).FirstOrDefault()
                                   });

            return result;
        }

        public static IQueryable<CodReportViewModel> GetCod(DateTime? start, DateTime? end)
        {
            if (!start.HasValue || !end.HasValue)
            {
                start = DateTime.Now.Subtract(TimeSpan.FromDays(1)).Date;
                end = DateTime.Now.Date;
            }

            var result = (from cod in DB.CodOptionOfOrders
                          where !cod.IsDeleted && DbFunctions.TruncateTime(cod.CreatedOn) >= start && DbFunctions.TruncateTime(cod.CreatedOn) <= end
                          select new
                          {
                              CustomerName = cod.Order.Customer.CustomerName,
                              UserName = cod.Order.Customer.UserName,
                              Amount = cod.Amount,
                              CollectedAmount = cod.CollectedAmount,
                              IsCollected = cod.IsCollected,
                              Type = cod.CodOption.Type,
                              CollectedOn = cod.UpdatedOn
                          })
                .GroupBy(x => x.UserName)
                .Select(
                        x => new CodReportViewModel
                        {
                            UserName = x.Key,
                            CustomerName = x.Select(i => i.CustomerName).FirstOrDefault(),
                            CashAmount = x.Where(i => i.Type == (int)CodOptionTypes.Cash).Select(i => i.Amount).DefaultIfEmpty().Sum(),
                            ChequeAmount = x.Where(i => i.Type == (int)CodOptionTypes.Cheque).Select(i => i.Amount).DefaultIfEmpty().Sum(),
                            CollectedCash = x.Where(i => i.Type == (int)CodOptionTypes.Cash && i.IsCollected)
                                                  .OrderBy(i => i.CollectedOn)
                                                  .Select(
                                                          i => new CodCollectViewModel
                                                          {
                                                              CollectedAmount = i.CollectedAmount,
                                                              CollectedOn = i.CollectedOn
                                                          }),
                            CollectedCheque = x.Where(i => i.Type == (int)CodOptionTypes.Cheque && i.IsCollected)
                                                    .OrderBy(i => i.CollectedOn)
                                                    .Select(
                                                            i => new CodCollectViewModel
                                                            {
                                                                CollectedAmount = i.CollectedAmount,
                                                                CollectedOn = i.CollectedOn
                                                            })
                        });

            return result;
        }

        public static IQueryable<OrderViewModel> GetBillingDetails(string cod = "", string codCollected = "", DateTime? startDate = null, DateTime? endDate = null)
        {
            DateTime now = DateTime.Now;
            if (startDate == null)
                startDate = new DateTime(now.Year, now.Month, 1);
            if (endDate == null)
                endDate = startDate.Value.AddMonths(1).AddDays(-1);

            cod = cod ?? "";
            codCollected = codCollected ?? "";
            var types = new List<int>();
            var collectedTypes = new List<int>();
            foreach (var t in cod.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                CodOptionTypes type;
                if (Enum.TryParse(t, true, out type))
                {
                    types.Add((int)type);
                }
            }
            foreach (var t in codCollected.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                CodOptionTypes type;
                if (Enum.TryParse(t, true, out type))
                {
                    collectedTypes.Add((int)type);
                }
            }

            var result = from order in DB.Orders
                         where
                         !order.IsDeleted &&
                         DbFunctions.TruncateTime(order.PickupDate) >= startDate
                         && DbFunctions.TruncateTime(order.PickupDate) <= endDate &&
                         ((order.Status != (int)OrderStatuses.OrderConfirmed) &&
                          (order.Status != (int)OrderStatuses.OrderSubmitted) &&
                          (order.Status != (int)OrderStatuses.ItemHasBeingPicked) &&
                          (order.Status != (int)OrderStatuses.OrderCanceled) &&
                          (order.Status != (int)OrderStatuses.PickupFailed) &&
                          (order.Status != (int)OrderStatuses.OrderRejected)) &&
                         (types.Count == 0 ||
                          order.CodOptionOfOrders.Where(c => !c.IsDeleted && types.Contains(c.CodOption.Type)).Select(c => c.CodOption.Type).Distinct().Count() == types.Count) &&
                         (collectedTypes.Count == 0 ||
                          order.CodOptionOfOrders.Where(c => !c.IsDeleted && c.IsCollected && collectedTypes.Contains(c.CodOption.Type)).Select(c => c.CodOption.Type).Distinct().Count() ==
                          collectedTypes.Count)
                         join zipCode in DB.ZipCodes on order.FromZipCode equals zipCode.PostalCode into fromGroup
                         join zipCode in DB.ZipCodes on order.ToZipCode equals zipCode.PostalCode into toGroup
                         select new OrderViewModel()
                         {
                             CreatedOn = order.CreatedOn,
                             Id = order.Id,
                             BinCode = order.BinCode,
                             CanceledOn = order.CanceledOn,
                             UpdatedOn = order.UpdatedOn,
                             ServiceId = order.ServiceId,
                             PaymentTypeId = order.PaymentTypeId,
                             ProductTypeId = order.ProductTypeId,
                             CollectedBy = order.CollectedBy,
                             CollectedOn = order.CollectedOn,
                             ConfirmedBy = order.ConfirmedBy,
                             ConfirmedOn = order.ConfirmedOn,
                             ConsignmentNumber = order.ConsignmentNumber,
                             CustomerId = order.CustomerId,
                             CustomerName = order.Customer.CustomerName,
                             CustomerCompany = order.Customer.CustomerCompany,
                             DeliveredBy = order.DeliveredBy,
                             DeliveredOn = order.DeliveredOn,
                             FromAddress = order.FromAddress,
                             FromName = order.FromName,
                             FromZipCode = order.FromZipCode,
                             FromMobilePhone = order.FromMobilePhone,
                             FromSignature = order.FromSignature,
                             Price = order.Price,
                             ProceededOn = order.ProceededOn,
                             ProceededBy = order.ProceededBy,
                             ProceededOutOn = order.ProceededOutOn,
                             ProceededOutBy = order.ProceededOutBy,
                             RejectNote = order.RejectNote,
                             Remark = order.Remark,
                             Status = (OrderStatuses)order.Status,
                             ToAddress = order.ToAddress,
                             ToName = order.ToName,
                             ToZipCode = order.ToZipCode,
                             ToMobilePhone = order.ToMobilePhone,
                             ToSignature = order.ToSignature,
                             QrCodePath = order.QrCodePath,
                             PromoCode = order.PromoCode,
                             SizeId = order.SizeId,
                             PickupTimeSlotId = order.PickupTimeSlotId,
                             PickupDate = order.PickupDate,
                             DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                             DeliveryDate = order.DeliveryDate,
                             FromDistrictCode = fromGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                             FromZoneCode = fromGroup.Select(x => x.Zone).FirstOrDefault(),
                             ToDistrictCode = toGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                             ToZoneCode = toGroup.Select(x => x.Zone).FirstOrDefault(),
                             OperatorRemark = order.OperatorRemark,
                             OrderNumber = order.OrderNumber,
                             RequestDate = order.Status == (int)OrderStatuses.PickupFailed
                                                      ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                      : order.Status == (int)OrderStatuses.DeliveryFailed
                                                          ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.ReDeliveryRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                          : null,
                             RequestRemark = order.Status == (int)OrderStatuses.PickupFailed
                                                        ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault()
                                                        : order.Status == (int)OrderStatuses.DeliveryFailed
                                                            ? order.ExtraRequests.Where(x => x.RequestType == (int)ExtraRequestType.ReDeliveryRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault()
                                                            : "",
                             IsExchange = order.IsExchange,
                             TotalCodAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted).Sum(x => x.Amount),
                             TotalCodCollectedAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted && x.IsCollected).Sum(x => x.CollectedAmount),
                         };

            return result;
        }

        public static List<ExportOrderViewModel> GetExportedBillingDetails(string cod = "", string codCollected = "", DateTime? startDate = null, DateTime? endDate = null, CriteriaOperator conditions = null)
        {
            var filteredBillingDetails = GetBillingDetails(cod, codCollected, startDate, endDate).AppendWhere(new CriteriaToExpressionConverter(), conditions) as IQueryable<OrderViewModel>;
            var result = from order in filteredBillingDetails.ToList()
                         select new ExportOrderViewModel()
                         {
                             CreatedOn = order.CreatedOn,
                             Id = order.Id,
                             BinCode = order.BinCode,
                             CanceledOn = order.CanceledOn,
                             UpdatedOn = order.UpdatedOn,
                             ServiceId = order.ServiceId,
                             PaymentTypeId = order.PaymentTypeId,
                             ProductTypeId = order.ProductTypeId,
                             CollectedBy = order.CollectedBy,
                             CollectedOn = order.CollectedOn,
                             ConfirmedBy = order.ConfirmedBy,
                             ConfirmedOn = order.ConfirmedOn,
                             ConsignmentNumber = order.ConsignmentNumber,
                             CustomerId = order.CustomerId,
                             CustomerName = order.CustomerName,
                             CustomerCompany = order.CustomerCompany,
                             DeliveredBy = order.DeliveredBy,
                             DeliveredOn = order.DeliveredOn,
                             FromAddress = order.FromAddress,
                             FromName = order.FromName,
                             FromZipCode = order.FromZipCode,
                             FromMobilePhone = order.FromMobilePhone,
                             FromSignature = order.FromSignature,
                             Price = order.Price,
                             ProceededOn = order.ProceededOn,
                             ProceededBy = order.ProceededBy,
                             ProceededOutOn = order.ProceededOutOn,
                             ProceededOutBy = order.ProceededOutBy,
                             RejectNote = order.RejectNote,
                             Remark = order.Remark,
                             Status = (OrderStatuses)order.Status,
                             ToAddress = order.ToAddress,
                             ToName = order.ToName,
                             ToZipCode = order.ToZipCode,
                             ToMobilePhone = order.ToMobilePhone,
                             ToSignature = order.ToSignature,
                             QrCodePath = order.QrCodePath,
                             PromoCode = order.PromoCode,
                             SizeId = order.SizeId,
                             PickupTimeSlotId = order.PickupTimeSlotId,
                             PickupDate = order.PickupDate,
                             DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                             DeliveryDate = order.DeliveryDate,
                             FromDistrictCode = order.FromDistrictCode,
                             FromZoneCode = order.FromZoneCode,
                             ToDistrictCode = order.ToDistrictCode,
                             ToZoneCode = order.ToZoneCode,
                             RequestDate = order.RequestDate,
                             RequestRemark = order.RequestRemark,
                             OperatorRemark = order.OperatorRemark,
                             Cod = string.Join(
                                                      "\n",
                                                      DB.CodOptionOfOrders
                                                        .Where(x => x.OrderId == order.Id && !x.IsDeleted && !x.CodOption.IsDeleted)
                                                        .ToList()
                                                        .Select(x => ((CodOptionTypes)x.CodOption.Type).ToString() + "\n" + x.Amount.ToString("c"))),
                             CodCollected = string.Join(
                                                               "\n",
                                                               DB.CodOptionOfOrders
                                                                 .Where(x => x.OrderId == order.Id && !x.IsDeleted && !x.CodOption.IsDeleted && x.IsCollected)
                                                                 .ToList()
                                                                 .Select(x => ((CodOptionTypes)x.CodOption.Type).ToString() + "\n" + x.CollectedAmount.ToString("c"))),
                             IsExchange = order.IsExchange,
                             TotalCodAmount = order.TotalCodAmount,
                             TotalCodCollectedAmount = order.TotalCodCollectedAmount,
                             OrderNumber = order.OrderNumber
                         };

            return result.ToList();
        }

        public static IQueryable<dynamic> GetBillingSummary(DateTime? startDate, DateTime? endDate)
        {
            DateTime now = DateTime.Now;
            if (startDate == null)
            {
                startDate = new DateTime(now.Year, now.Month, 1);
            }

            if (endDate == null)
            {
                endDate = startDate.Value.AddMonths(1).AddDays(-1);
            }

            var result = from order in DB.Orders
                         where
                             !order.IsDeleted &&
                             DbFunctions.TruncateTime(order.PickupDate) >= startDate
                             && DbFunctions.TruncateTime(order.PickupDate) <= endDate &&
                             ((order.Status != (int)OrderStatuses.OrderConfirmed) &&
                              (order.Status != (int)OrderStatuses.OrderSubmitted) &&
                              (order.Status != (int)OrderStatuses.ItemHasBeingPicked) &&
                              (order.Status != (int)OrderStatuses.OrderCanceled) &&
                              (order.Status != (int)OrderStatuses.PickupFailed) &&
                              (order.Status != (int)OrderStatuses.OrderRejected))
                         join customer in DB.Customers on order.CustomerId equals customer.Id
                         group order by new { customer.CustomerCompany, customer.CustomerName, customer.UserName, customer.Salesperson, customer.PaymentTypeId, customer.PricePlan } 
                         into rp
                         select new
                         {
                             Id = rp.Key.CustomerName,
                             SalesPerson = rp.Key.Salesperson,
                             CompanyName = rp.Key.CustomerCompany,
                             PricePlan = rp.Key.PricePlan,
                             CustomerName = rp.Key.CustomerName,
                             PaymentType = rp.Key.PaymentTypeId,
                             BillingAmount = rp.Sum(x => x.Price)
                         };

            return result;
        }

        #endregion
    }
}
