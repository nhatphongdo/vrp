﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Web.Mvc;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class NotificationsController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "NotificationsDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsForDrivers)]
        public ActionResult Drivers()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsForDrivers)]
        public ActionResult DriversListViewPartial()
        {
            return PartialView("_DriversListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsSendToDrivers)]
        public ActionResult Send()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsSendToDrivers)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Send([ModelBinder(typeof(DevExpressEditorsBinder))] SendNotificationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Request["Send"] != null)
                {
                    var result = SendNotification(model.Token, model.Message);
                    if (result.StartsWith("id", StringComparison.OrdinalIgnoreCase))
                    {
                        var notification = new Notification()
                                           {
                                               Message = model.Message,
                                               IsDeleted = false,
                                               SendBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                               SendOn = DateTime.Now,
                                               SendTo = model.ToUser,
                                               SendToToken = model.Token,
                                               ToDriver = model.IsDriver,
                                               UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                               UpdatedOn = DateTime.Now
                                           };

                        db.Notifications.Add(notification);

                        try
                        {
                            db.SaveChanges();
                            return RedirectToAction("Drivers", "Notifications");
                        }
                        catch (Exception exc)
                        {
                            ViewBag.ErrorMessages = new[]
                                                        { "Errors occur in processing request. Please contact to administrator to resolve." };
                            LogHelper.Log(exc);
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessages = new[]
                                                    { "Errors occur in processing request. Please contact to administrator to resolve.", result };
                    }
                }
                else if (Request["SendAll"] != null)
                {
                    var total = 0;
                    var count = 0;
                    var fails = new List<string>();
                    foreach (var driver in DriversController.GetDrivers().Where(x => x.IsActive))
                    {
                        ++total;
                        var result = SendNotification(driver.NotificationKey, model.Message);
                        if (result.StartsWith("id", StringComparison.OrdinalIgnoreCase))
                        {
                            var notification = new Notification()
                                               {
                                                   Message = model.Message,
                                                   IsDeleted = false,
                                                   SendBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                   SendOn = DateTime.Now,
                                                   SendTo = driver.UserName,
                                                   SendToToken = driver.NotificationKey,
                                                   ToDriver = model.IsDriver,
                                                   UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                   UpdatedOn = DateTime.Now
                                               };

                            db.Notifications.Add(notification);

                            try
                            {
                                db.SaveChanges();
                                ++count;
                            }
                            catch (Exception exc)
                            {
                                fails.Add($"Notification is sent successfully but cannot log information into database. Username is: {driver.UserName}.");
                                LogHelper.Log(exc);
                            }
                        }
                        else
                        {
                            fails.Add($"Notification is sent failed. Username is: {driver.UserName}.");
                        }
                    }

                    fails.Insert(0, $"Sent {count} / {total} notifications successfully. Please check error(s) if any as below:");
                    ViewBag.ErrorMessages = fails.ToArray();
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsForUsers)]
        public ActionResult Users()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsForUsers)]
        public ActionResult UsersListViewPartial()
        {
            return PartialView("_UsersListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsSendToUsers)]
        public ActionResult SendUser()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.NotificationsSendToUsers)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendUser([ModelBinder(typeof(DevExpressEditorsBinder))] SendNotificationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Request["Send"] != null)
                {
                    var result = SendNotification(model.Token, model.Message);
                    if (result.StartsWith("id", StringComparison.OrdinalIgnoreCase))
                    {
                        var notification = new Notification()
                                           {
                                               Message = model.Message,
                                               IsDeleted = false,
                                               SendBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                               SendOn = DateTime.Now,
                                               SendTo = model.ToUser,
                                               SendToToken = model.Token,
                                               ToDriver = model.IsDriver,
                                               UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                               UpdatedOn = DateTime.Now
                                           };

                        db.Notifications.Add(notification);

                        try
                        {
                            db.SaveChanges();
                            return RedirectToAction("Users", "Notifications");
                        }
                        catch (Exception exc)
                        {
                            ViewBag.ErrorMessages = new[]
                                                        { "Errors occur in processing request. Please contact to administrator to resolve." };
                            LogHelper.Log(exc);
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessages = new[]
                                                    { "Errors occur in processing request. Please contact to administrator to resolve.", result };
                    }
                }
                else if (Request["SendAll"] != null)
                {
                    var total = 0;
                    var count = 0;
                    var fails = new List<string>();
                    foreach (var customer in CustomersController.GetCustomers().Where(x => x.IsActive))
                    {
                        ++total;
                        var result = SendNotification(customer.NotificationKey, model.Message);
                        if (result.StartsWith("id", StringComparison.OrdinalIgnoreCase))
                        {
                            var notification = new Notification()
                                               {
                                                   Message = model.Message,
                                                   IsDeleted = false,
                                                   SendBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                   SendOn = DateTime.Now,
                                                   SendTo = customer.UserName,
                                                   SendToToken = customer.NotificationKey,
                                                   ToDriver = model.IsDriver,
                                                   UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                   UpdatedOn = DateTime.Now
                                               };

                            db.Notifications.Add(notification);

                            try
                            {
                                db.SaveChanges();
                                ++count;
                            }
                            catch (Exception exc)
                            {
                                fails.Add($"Notification is sent successfully but cannot log information into database. Username is: {customer.UserName}.");
                                LogHelper.Log(exc);
                            }
                        }
                        else
                        {
                            fails.Add($"Notification is sent failed. Username is: {customer.UserName}.");
                        }
                    }

                    fails.Insert(0, $"Sent {count} / {total} notifications successfully. Please check error(s) if any as below:");
                    ViewBag.ErrorMessages = fails.ToArray();
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<NotificationViewModel> GetDriverNotifications()
        {
            var notifications = (from notification in DB.Notifications
                                 where !notification.IsDeleted && notification.ToDriver
                                 select new NotificationViewModel()
                                        {
                                            Id = notification.Id,
                                            Message = notification.Message,
                                            SendTo = notification.SendTo,
                                            SentOn = notification.SendOn,
                                            SendBy = notification.SendBy
                                        });

            return notifications;
        }

        public static IQueryable<NotificationViewModel> GetUserNotifications()
        {
            var notifications = (from notification in DB.Notifications
                                 where !notification.IsDeleted && !notification.ToDriver
                                 select new NotificationViewModel()
                                        {
                                            Id = notification.Id,
                                            Message = notification.Message,
                                            SendTo = notification.SendTo,
                                            SentOn = notification.SendOn,
                                            SendBy = notification.SendBy
                                        });

            return notifications;
        }

        #endregion

        public string SendNotification(string deviceId, string message)
        {
            var value = message;
            var tRequest = WebRequest.Create("https://gcm-http.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add($"Authorization: key={SettingProvider.GcmServerKey}");

            var postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&data.time=" + System.DateTime.Now.ToString() + "&registration_id=" + deviceId + "";
            var byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            var dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            var tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            var tReader = new StreamReader(dataStream);

            var sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            return sResponseFromServer;
        }
    }
}
