﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [CdsAuthorize]
    public class HomeController : Controller
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated && !string.IsNullOrEmpty(GetAccessToken()))
            {
                return RedirectToLocal(returnUrl, GetAccessToken());
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await AccountHelper.Authenticate(model);
            if (result.Code == 0)
            {
                Response.SetCookie(new HttpCookie("CdsAccessToken", result.Token));                
                return RedirectToLocal(returnUrl, result.Token);
            }
            else
            {
                ModelState.AddModelError("", result.Message);
                return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult AccessDenied()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl, string token)
        {
            if (!string.IsNullOrEmpty(returnUrl))
            {
                if (!string.IsNullOrEmpty(token))
                {
                    // Attach access token into url
                    if (returnUrl.Contains("?"))
                    {
                        returnUrl += "&token=" + token;
                    }
                    else
                    {
                        returnUrl += "?token=" + token;
                    }
                }
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Dashboard", new
            {
                token = token
            });
        }

        public static string GetAccessToken()
        {
            var token = System.Web.HttpContext.Current.Request.Cookies["CdsAccessToken"]?.Value ?? "";

            return token;
        }

        #endregion
    }
}