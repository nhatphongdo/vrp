﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;
using DevExpress.Data.Filtering;
using DevExpress.Data.Linq;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using Newtonsoft.Json;
using System.Data.Entity;
using Courier_Management_System.Services.Implement;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        private const string DatabaseDataContextKey = "OrdersDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    var context = new CourierDBEntities();
                    context.Database.CommandTimeout = 600;
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = context;
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        public OrdersController()
        {
            db.Database.CommandTimeout = 600;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrders)]
        public ActionResult List()
        {
            ViewBag.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(30)).Date;
            ViewBag.EndDate = DateTime.Now.Date;
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrders + "," + AccountHelper.OrdersEditOrder)]
        public ActionResult ListViewPartial(long? id, string command, string note, string cod, string codCollected, DateTime? start, DateTime? end, DateTime? startDate, DateTime? endDate)
        {
            ViewBag.COD = cod ?? Request["COD"];
            ViewBag.CODCollected = codCollected ?? Request["CODCollected"];
            ViewBag.StartDate = start ?? startDate;
            ViewBag.EndDate = end ?? endDate;

            if (!id.HasValue || string.IsNullOrEmpty(command))
            {
                return PartialView("_ListViewPartial");
            }

            if (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.OrdersEditOrder))
            {
                ProcessCommand(id, command, note);
            }

            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrders + "," + AccountHelper.OrdersEditOrder + "," + AccountHelper.EditOrderPrice)]
        public ActionResult EditOrderNew()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrders + "," + AccountHelper.OrdersEditOrder + "," + AccountHelper.EditOrderPrice)]
        public ActionResult ListViewPartialNew(long? id, string command, string note)
        {
            if (!id.HasValue || string.IsNullOrEmpty(command))
            {
                return PartialView("_ListViewPartialNew");
            }

            if (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.OrdersEditOrder) || User.IsInRole(AccountHelper.EditOrderPrice))
            {
                ProcessCommand(id, command, note);
            }

            return PartialView("_ListViewPartialNew");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersAddOrder)]
        public ActionResult Add()
        {
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.ProductTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.PaymentTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersAddOrder)]
        public async Task<ActionResult> Add(AddEditOrderViewModel model)
        {
            var services = db.Services.Where(x => !x.IsDeleted).Select(
                                                                       x => new SelectListItem()
                                                                            {
                                                                                Text = x.ServiceName,
                                                                                Value = x.Id.ToString()
                                                                            }).ToList();
            var sizes = db.Sizes.Where(x => !x.IsDeleted).Select(
                                                                 x => new SelectListItem()
                                                                      {
                                                                          Text = x.SizeName,
                                                                          Value = x.Id.ToString()
                                                                      }).ToList();

            var productTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                               x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.ProductTypeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();
            ViewBag.ProductTypes = productTypes;
            var paymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                               x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.PaymentTypeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();
            ViewBag.PaymentTypes = paymentTypes;

            if (model.CustomerId.HasValue)
            {
                model.Customer = db.Customers.FirstOrDefault(x => x.Id == model.CustomerId.Value);
            }

            if (ModelState.IsValid)
            {
                var order = new Order()
                            {
                                FromZipCode = model.FromZipCode,
                                FromName = model.FromName,
                                FromAddress = model.FromAddress,
                                FromMobilePhone = model.FromMobilePhone,
                                ToName = model.ToName,
                                ToZipCode = model.ToZipCode,
                                ToAddress = model.ToAddress,
                                ToMobilePhone = model.ToMobilePhone,
                                PaymentTypeId = model.PaymentTypeId,
                                ServiceId = model.ServiceId,
                                ProductTypeId = model.ProductTypeId,
                                Remark = model.Remark,
                                Price = model.Price,
                                Status = (int) model.Status,
                                CustomerId = model.CustomerId,
                                SizeId = model.SizeId,
                                PickupTimeSlotId = model.PickupTimeSlotId,
                                PickupDate = model.PickupDate,
                                DeliveryTimeSlotId = model.DeliveryTimeSlotId,
                                DeliveryDate = model.DeliveryDate,
                                PromoCode = model.PromoCode,
                                OperatorRemark = model.OperatorRemark,
                                IsDeleted = false,
                                IsExchange = model.IsExchange,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                UpdatedOn = DateTime.Now
                            };

                var service = db.Services.FirstOrDefault(x => x.Id == model.ServiceId);
                if (service != null && !service.IsPickupDateAllow)
                {
                    order.PickupDate = DateTime.Now.Date;
                }
                if (service != null && !service.IsDeliveryDateAllow)
                {
                    order.DeliveryDate = order.PickupDate;
                }

                order.BinCode = OrderHelper.GenerateBinCode(order);

                if (order.Status == (int) OrderStatuses.DeliveryFailed || order.Status == (int) OrderStatuses.ItemDelivered)
                {
                    order.DeliveredOn = DateTime.Now;
                    order.DeliveredBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.PickupFailed || order.Status == (int) OrderStatuses.ItemCollected)
                {
                    order.CollectedOn = DateTime.Now;
                    order.CollectedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.OrderConfirmed || order.Status == (int) OrderStatuses.OrderRejected
                    || order.Status == (int) OrderStatuses.SpecialOrderForPickup || order.Status == (int) OrderStatuses.ItemHasBeingPicked)
                {
                    order.ConfirmedOn = DateTime.Now;
                    order.ConfirmedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.OrderCanceled)
                {
                    order.CanceledOn = DateTime.Now;
                    order.CanceledBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.ItemInDepot)
                {
                    order.ProceededOn = DateTime.Now;
                    order.ProceededBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.ItemOutDepot)
                {
                    order.ProceededOutOn = DateTime.Now;
                    order.ProceededOutBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }

                db.Orders.Add(order);

                try
                {
                    db.SaveChanges();

                    order.ConsignmentNumber = OrderHelper.GenerateConsignmentNumber(order.Id);

                    // Build QR code
                    var bitmap = OrderHelper.GenerateQrCode(paymentTypes, services, productTypes, sizes, order);
                    order.QrCodePath = Url.Content(OrderHelper.SaveQrCode(order.ConsignmentNumber, bitmap));
                    //Build BarCode
                    var barCode = OrderHelper.GenerateBarCode(order.ConsignmentNumber);
                    var barCodePath = OrderHelper.SaveBarCode(order.ConsignmentNumber, barCode);

                    var track = new OrderTracking
                                {
                                    ConsignmentNumber = order.ConsignmentNumber,
                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                    CreatedOn = DateTime.Now,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    BinCode = order.BinCode,
                                    Status = order.Status
                                };
                    db.OrderTrackings.Add(track);

                    // Update request
                    ExtraRequest pickupRequest = null;
                    ExtraRequest deliveryRequest = null;
                    if (!string.IsNullOrEmpty(model.PickupRequest))
                    {
                        pickupRequest = new ExtraRequest()
                                        {
                                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                            OrderId = order.Id,
                                            RequestType = (int) ExtraRequestType.PickupRequest,
                                            CreatedOn = DateTime.Now,
                                            IsDeleted = false,
                                            IsVisited = true,
                                            RequestContent = model.PickupRequest,
                                            UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                            UpdatedOn = DateTime.Now
                                        };
                        db.ExtraRequests.Add(pickupRequest);
                    }
                    if (!string.IsNullOrEmpty(model.DeliveryRequest))
                    {
                        deliveryRequest = new ExtraRequest()
                                          {
                                              CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              OrderId = order.Id,
                                              RequestType = (int) ExtraRequestType.DeliveryRequest,
                                              CreatedOn = DateTime.Now,
                                              IsDeleted = false,
                                              IsVisited = true,
                                              RequestContent = model.DeliveryRequest,
                                              UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              UpdatedOn = DateTime.Now
                                          };
                        db.ExtraRequests.Add(deliveryRequest);
                    }

                    db.SaveChanges();

                    if (pickupRequest != null)
                    {
                        pickupRequest.Code = OrderHelper.GenerateExtraRequestNumber(pickupRequest.Id);
                    }
                    if (deliveryRequest != null)
                    {
                        deliveryRequest.Code = OrderHelper.GenerateExtraRequestNumber(deliveryRequest.Id);
                    }
                    db.SaveChanges();

                    return RedirectToAction("List", "Orders");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersEditOrder + "," + AccountHelper.EditOrderPrice)]
        public ActionResult Edit(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.ProductTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();
            ViewBag.PaymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                                   x => new SelectListItem()
                                                                                        {
                                                                                            Text = x.PaymentTypeName,
                                                                                            Value = x.Id.ToString()
                                                                                        }).ToList();

            var model = (from order in db.Orders
                         where !order.IsDeleted && order.Id == id
                         select new AddEditOrderViewModel()
                                {
                                    FromName = order.FromName,
                                    FromZipCode = order.FromZipCode,
                                    FromAddress = order.FromAddress,
                                    FromMobilePhone = order.FromMobilePhone,
                                    ToName = order.ToName,
                                    ToZipCode = order.ToZipCode,
                                    ToAddress = order.ToAddress,
                                    ToMobilePhone = order.ToMobilePhone,
                                    PaymentTypeId = order.PaymentTypeId,
                                    ProductTypeId = order.ProductTypeId,
                                    Remark = order.Remark,
                                    Status = (OrderStatuses) order.Status,
                                    ServiceId = order.ServiceId,
                                    CustomerId = order.CustomerId,
                                    Customer = order.Customer,
                                    Price = order.Price,
                                    RejectNote = order.RejectNote,
                                    SizeId = order.SizeId,
                                    PromoCode = order.PromoCode,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    OperatorRemark = order.OperatorRemark,
                                    PickupRequest = order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.PickupRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault(),
                                    DeliveryRequest = order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.DeliveryRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault(),
                                    IsExchange = order.IsExchange,
                                    CodOptions = order.CodOptionOfOrders
                                                      .Where(x => !x.IsDeleted)
                                                      .Select(
                                                              x => new CodOptionModel()
                                                                   {
                                                                       Id = x.Id,
                                                                       IsCollected = x.IsCollected,
                                                                       Type = x.CodOption.Type,
                                                                       Name = x.CodOption.Description,
                                                                       Amount = x.Amount,
                                                                       CollectedAmount = x.CollectedAmount
                                                                   })
                                                      .ToList()
                                }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            //if (User.IsInRole(AccountHelper.OrdersEditOrder))
            //{
            //    // Clear attributes are not valid for Rule
            //    model.PaymentTypeId = 0;
            //    model.ProductTypeId = 0;
            //    model.Remark = "";
            //    model.CustomerId = 0;
            //    model.Customer = null;
            //    model.RejectNote = "";
            //    model.SizeId = 0;
            //    model.PromoCode = "";
            //}

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersEditOrder + "," + AccountHelper.EditOrderPrice)]
        public async Task<ActionResult> Edit(long? id, AddEditOrderViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var services = db.Services.Where(x => !x.IsDeleted).Select(
                                                                       x => new SelectListItem()
                                                                            {
                                                                                Text = x.ServiceName,
                                                                                Value = x.Id.ToString()
                                                                            }).ToList();
            var sizes = db.Sizes.Where(x => !x.IsDeleted).Select(
                                                                 x => new SelectListItem()
                                                                      {
                                                                          Text = x.SizeName,
                                                                          Value = x.Id.ToString()
                                                                      }).ToList();

            var productTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(
                                                                               x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.ProductTypeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();
            ViewBag.ProductTypes = productTypes;
            var paymentTypes = db.PaymentTypes.Where(x => !x.IsDeleted).Select(
                                                                               x => new SelectListItem()
                                                                                    {
                                                                                        Text = x.PaymentTypeName,
                                                                                        Value = x.Id.ToString()
                                                                                    }).ToList();
            ViewBag.PaymentTypes = paymentTypes;

            if (model.CustomerId.HasValue)
            {
                model.Customer = db.Customers.FirstOrDefault(x => x.Id == model.CustomerId.Value);
            }

            if (ModelState.IsValid)
            {
                var order = db.Orders.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (order == null)
                {
                    return HttpNotFound();
                }

                order.FromName = model.FromName;
                order.FromZipCode = model.FromZipCode;
                order.FromAddress = model.FromAddress;
                order.ToName = model.ToName;
                order.ToZipCode = model.ToZipCode;
                order.ToAddress = model.ToAddress;
                order.ToMobilePhone = model.ToMobilePhone;
                order.PickupTimeSlotId = model.PickupTimeSlotId;
                order.PickupDate = model.PickupDate;
                order.DeliveryTimeSlotId = model.DeliveryTimeSlotId;
                order.DeliveryDate = model.DeliveryDate;
                order.OperatorRemark = model.OperatorRemark;
                order.IsExchange = model.IsExchange;

                var pickupRequest = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.PickupRequest && !x.IsDeleted);
                if (pickupRequest == null)
                {
                    pickupRequest = new ExtraRequest()
                                    {
                                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                        OrderId = order.Id,
                                        RequestType = (int) ExtraRequestType.PickupRequest,
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false,
                                        IsVisited = true
                                    };
                    db.ExtraRequests.Add(pickupRequest);
                }
                pickupRequest.RequestContent = model.PickupRequest;
                pickupRequest.UpdatedOn = DateTime.Now;
                pickupRequest.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";

                var deliveryRequest = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.DeliveryRequest && !x.IsDeleted);
                if (deliveryRequest == null)
                {
                    deliveryRequest = new ExtraRequest()
                                      {
                                          CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          OrderId = order.Id,
                                          RequestType = (int) ExtraRequestType.DeliveryRequest,
                                          CreatedOn = DateTime.Now,
                                          IsDeleted = false,
                                          IsVisited = true
                                      };
                    db.ExtraRequests.Add(deliveryRequest);
                }
                deliveryRequest.RequestContent = model.DeliveryRequest;
                deliveryRequest.UpdatedOn = DateTime.Now;
                deliveryRequest.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";

                order.FromMobilePhone = model.FromMobilePhone;
                order.PaymentTypeId = model.PaymentTypeId;
                order.ServiceId = model.ServiceId;
                order.ProductTypeId = model.ProductTypeId;
                order.Remark = model.Remark;
                if (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.EditOrderPrice))
                {
                    order.Price = model.Price;
                }
                order.Status = (int) model.Status;
                order.RejectNote = model.RejectNote;
                order.CustomerId = model.CustomerId;
                order.SizeId = model.SizeId;
                order.PromoCode = model.PromoCode;

                // Update COD
                if (model.CodOptions != null)
                {
                    foreach (var cod in model.CodOptions)
                    {
                        var codOption = db.CodOptionOfOrders.FirstOrDefault(x => !x.IsDeleted && x.Id == cod.Id);
                        if (codOption != null)
                        {
                            codOption.Amount = cod.Amount;
                            codOption.CollectedAmount = cod.CollectedAmount;
                            codOption.UpdatedOn = DateTime.Now;
                            codOption.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        }
                    }
                }

                order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                order.UpdatedOn = DateTime.Now;

                if (order.Status == (int) OrderStatuses.DeliveryFailed || order.Status == (int) OrderStatuses.ItemDelivered)
                {
                    order.DeliveredOn = DateTime.Now;
                    order.DeliveredBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.PickupFailed || order.Status == (int) OrderStatuses.ItemCollected)
                {
                    order.CollectedOn = DateTime.Now;
                    order.CollectedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.OrderConfirmed || order.Status == (int) OrderStatuses.OrderRejected
                    || order.Status == (int) OrderStatuses.SpecialOrderForPickup || order.Status == (int) OrderStatuses.ItemHasBeingPicked)
                {
                    order.ConfirmedOn = DateTime.Now;
                    order.ConfirmedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.OrderCanceled)
                {
                    order.CanceledOn = DateTime.Now;
                    order.CanceledBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.ItemInDepot)
                {
                    order.ProceededOn = DateTime.Now;
                    order.ProceededBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }
                if (order.Status == (int) OrderStatuses.ItemOutDepot)
                {
                    order.ProceededOutOn = DateTime.Now;
                    order.ProceededOutBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }

                var service = db.Services.FirstOrDefault(x => x.Id == order.ServiceId);
                if (service != null && !service.IsPickupDateAllow)
                {
                    order.PickupDate = DateTime.Now.Date;
                }
                if (service != null && !service.IsDeliveryDateAllow)
                {
                    order.DeliveryDate = order.PickupDate;
                }
                order.BinCode = OrderHelper.GenerateBinCode(order);

                // Build QR code
                //var bitmap = OrderHelper.GenerateQrCode(paymentTypes, services, productTypes, sizes, order);
                //order.QrCodePath = Url.Content(OrderHelper.SaveQrCode(order.ConsignmentNumber, bitmap));
                // Build BarCode
                //var barCode = OrderHelper.GenerateBarCode(order.ConsignmentNumber);
                //var barCodePath = OrderHelper.SaveBarCode(order.ConsignmentNumber, barCode);

                var track = new OrderTracking
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                PickupTimeSlotId = order.PickupTimeSlotId,
                                PickupDate = order.PickupDate,
                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                DeliveryDate = order.DeliveryDate,
                                BinCode = order.BinCode,
                                Status = order.Status
                            };
                db.OrderTrackings.Add(track);

                try
                {
                    db.SaveChanges();

                    if (string.IsNullOrEmpty(pickupRequest.Code))
                    {
                        pickupRequest.Code = OrderHelper.GenerateExtraRequestNumber(pickupRequest.Id);
                    }
                    if (string.IsNullOrEmpty(deliveryRequest.Code))
                    {
                        deliveryRequest.Code = OrderHelper.GenerateExtraRequestNumber(deliveryRequest.Id);
                    }
                    db.SaveChanges();

                    return RedirectToAction("List", "Orders");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersDeleteOrder)]
        public ActionResult ListViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var order = db.Orders.FirstOrDefault(x => x.Id == id);
                if (order != null)
                {
                    order.IsDeleted = true;
                    order.UpdatedOn = DateTime.Now;
                    order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        [HttpPost]
        public ActionResult FindCustomers(string q, int? page)
        {
            if (!page.HasValue)
            {
                page = 1;
            }

            q = q.ToLower();
            var list = db.Customers
                         .Where(x => !x.IsDeleted && x.IsActive && x.CustomerName.ToLower().Contains(q) || x.CustomerCompany.ToLower().Contains(q) || x.CustomerAddress.ToLower().Contains(q))
                         .OrderBy(x => x.CustomerName)
                         .Skip((page.Value - 1) * 10).Take(10)
                         .Select(
                                 x => new
                                      {
                                          x.CustomerName,
                                          x.CustomerCompany,
                                          x.CustomerAddress,
                                          x.UserName,
                                          x.Salesperson,
                                          id = x.Id
                                      })
                         .ToList();

            return Json(list);
        }

        [HttpPost]
        public ActionResult LoadServiceSelectItems(int? productTypeId, int? selectedId)
        {
            if (!productTypeId.HasValue)
            {
                return Content("");
            }

            var list = db.Services.Where(x => !x.IsDeleted && x.ProductTypeId == productTypeId.Value).ToList();

            return Content(string.Join("", list.Select(x => $"<option value='{x.Id}'" + (x.Id == selectedId ? "selected='selected'" : "") + $">{x.ServiceName}</option>")));
        }

        [HttpPost]
        public ActionResult LoadSizeSelectItems(int? productTypeId, int? selectedId)
        {
            if (!productTypeId.HasValue)
            {
                return Content("");
            }

            var list = db.Sizes.Where(x => !x.IsDeleted && x.ProductTypeId == productTypeId.Value).ToList();

            return Content(string.Join("", list.Select(x => $"<option value='{x.Id}'" + (x.Id == selectedId ? "selected='selected'" : "") + $">{x.SizeName}</option>")));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            base.Dispose(disposing);
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersExportOrders)]
        public ActionResult Export(string type, DateTime? startDate, DateTime? endDate)
        {
            var conditions = CriteriaOperator.Parse(Request["FilterExpression"]);

            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(
                                                         GetGridViewExportSettings(),
                                                         GetExportedOrders(conditions, Request["COD"], Request["CODCollected"], startDate, endDate),
                                                         "Orders-" + DateTime.Now.ToString("yyyyMMdd"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(
                                                          GetGridViewExportSettings(),
                                                          GetExportedOrders(conditions, Request["COD"], Request["CODCollected"], startDate, endDate),
                                                          "Orders-" + DateTime.Now.ToString("yyyyMMdd"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(
                                                         GetGridViewExportSettings(),
                                                         GetExportedOrders(conditions, Request["COD"], Request["CODCollected"], startDate, endDate),
                                                         "Orders-" + DateTime.Now.ToString("yyyyMMdd"));
                case "vrp":
                    return GridViewExtension.ExportToXlsx(GetVrpGridViewExportSettings(), GetVrp(GetOrders("", "", startDate, endDate), conditions), "ExportVRP-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "autovrp":
                    if (!Directory.Exists(Server.MapPath("~/Temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Temp"));
                    }
                    var fileName = DateTime.Now.Ticks.ToString() + ".xlsx";
                    var stream = new FileStream(Server.MapPath("~/Temp/" + fileName), FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    GridViewExtension.WriteXlsx(GetVrpGridViewExportSettings(), GetVrp(GetOrders("", "", startDate, endDate), conditions), stream);
                    stream.Flush();
                    stream.Close();
                    return Redirect(SettingProvider.VrpLink + "?file=" + fileName);
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewOrders";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "Status";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = EnumHelper.GetSelectList(typeof(OrderStatuses));
                                     comboBoxProperties.TextField = "Text";
                                     comboBoxProperties.ValueField = "Value";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add("ConsignmentNumber");
            settings.Columns.Add("BinCode");
            settings.Columns.Add("OrderNumber");
            settings.Columns.Add("Cod").Caption = "COD";
            settings.Columns.Add("CodCollected").Caption = "COD Collected";
            settings.Columns.Add("Remark").Caption = "Customer Remark";
            settings.Columns.Add("IsExchange").Caption = "Exchange?";
            settings.Columns.Add("CustomerName");
            settings.Columns.Add("CustomerCompany");
            settings.Columns.Add("FromName");
            settings.Columns.Add("FromZipCode");
            settings.Columns.Add("FromDistrictCode");
            settings.Columns.Add("FromZoneCode");
            settings.Columns.Add("FromAddress");
            settings.Columns.Add("FromMobilePhone");
            settings.Columns.Add("PickupDate");
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "PickupTimeSlotId";
                                     column.Caption = "Pickup Time Slot";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;
                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = TimeSlotsController.GetTimeSlots().ToList().Select(
                                                                                                                        x => new
                                                                                                                             {
                                                                                                                                 Id = x.Id,
                                                                                                                                 Name = x.Name,
                                                                                                                                 FromTime = x.FromTime.ToString("HH:mm"),
                                                                                                                                 ToTime = x.ToTime.ToString("HH:mm")
                                                                                                                             }).ToList();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.TextFormatString = "{0} ({1} - {2})";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                     comboBoxProperties.Columns.Add("Name", "Time Slot Name", Unit.Pixel(200));
                                     comboBoxProperties.Columns.Add("FromTime");
                                     comboBoxProperties.Columns.Add("ToTime");
                                 });

            settings.Columns.Add("ToName");
            settings.Columns.Add("ToZipCode");
            settings.Columns.Add("ToDistrictCode");
            settings.Columns.Add("ToZoneCode");
            settings.Columns.Add("ToAddress");
            settings.Columns.Add("ToMobilePhone");
            settings.Columns.Add("DeliveryDate");
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "DeliveryTimeSlotId";
                                     column.Caption = "Delivery Time Slot";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = TimeSlotsController.GetTimeSlots().ToList().Select(
                                                                                                                        x => new
                                                                                                                             {
                                                                                                                                 Id = x.Id,
                                                                                                                                 Name = x.Name,
                                                                                                                                 FromTime = x.FromTime.ToString("HH:mm"),
                                                                                                                                 ToTime = x.ToTime.ToString("HH:mm")
                                                                                                                             }).ToList();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.TextFormatString = "{0} ({1} - {2})";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                     comboBoxProperties.Columns.Add("Name", "Time Slot Name", Unit.Pixel(200));
                                     comboBoxProperties.Columns.Add("FromTime");
                                     comboBoxProperties.Columns.Add("ToTime");
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "ServiceId";
                                     column.Caption = "Service";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetServices();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "ProductTypeId";
                                     column.Caption = "Product Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetProductTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "SizeId";
                                     column.Caption = "Size";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetSizes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "PaymentTypeId";
                                     column.Caption = "Payment Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetPaymentTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            var col = settings.Columns.Add("Price");
            col.PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("CreatedOn");
            settings.Columns.Add("FullfilmentNumber");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowTodayOrders)]
        public ActionResult Today()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowTodayOrders + "," + AccountHelper.OrdersEditOrder)]
        public ActionResult TodayListViewPartial(long? id, string command, string note, string cod, string codCollected)
        {
            ViewBag.COD = cod ?? Request["COD"];
            ViewBag.CODCollected = codCollected ?? Request["CODCollected"];

            if (!id.HasValue || string.IsNullOrEmpty(command))
            {
                return PartialView("_TodayListViewPartial");
            }

            if (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.OrdersEditOrder))
            {
                ProcessCommand(id, command, note);
            }

            return PartialView("_TodayListViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersExportTodayOrders)]
        public ActionResult ExportToday(string type)
        {
            var conditions = CriteriaOperator.Parse(Request["FilterExpression"]);

            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetGridViewExportSettings(), GetExportedTodayOrders(Request["COD"], Request["CODCollected"]), "TodayOrders-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetGridViewExportSettings(), GetExportedTodayOrders(Request["COD"], Request["CODCollected"]), "TodayOrders-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetGridViewExportSettings(), GetExportedTodayOrders(Request["COD"], Request["CODCollected"]), "TodayOrders-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "vrp":
                    return GridViewExtension.ExportToXlsx(GetVrpGridViewExportSettings(), GetVrp(GetTodayOrders(), conditions), "ExportVRP-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "autovrp":
                    if (!Directory.Exists(Server.MapPath("~/Temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Temp"));
                    }
                    var fileName = DateTime.Now.Ticks.ToString() + ".xlsx";
                    var stream = new FileStream(Server.MapPath("~/Temp/" + fileName), FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    GridViewExtension.WriteXlsx(GetVrpGridViewExportSettings(), GetVrp(GetTodayOrders(), conditions), stream);
                    stream.Flush();
                    stream.Close();
                    return Redirect(SettingProvider.VrpLink + "?file=" + fileName);
                default:
                    return new EmptyResult();
            }
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersDeleteTodayOrder)]
        public ActionResult TodayListViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var order = db.Orders.FirstOrDefault(x => x.Id == id);
                if (order != null)
                {
                    order.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_TodayListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowFailedOrders)]
        public ActionResult Failed()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowFailedOrders + "," + AccountHelper.OrdersEditFailedOrder)]
        public ActionResult FailedListViewPartial(long? id, string command, string note)
        {
            if (!id.HasValue || string.IsNullOrEmpty(command))
            {
                return PartialView("_FailedListViewPartial");
            }

            if (User.IsInRole(AccountHelper.AdministratorRole) || User.IsInRole(AccountHelper.OrdersEditFailedOrder))
            {
                ProcessCommand(id, command, note);
            }

            return PartialView("_FailedListViewPartial");
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersExportFailedOrders)]
        public ActionResult ExportFailed(string type)
        {
            switch (type.ToLower())
            {
                case "xls":
                    return GridViewExtension.ExportToXls(GetFailedGridViewExportSettings(), GetFailedOrders().ToList(), "FailedOrders-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "xlsx":
                    return GridViewExtension.ExportToXlsx(GetFailedGridViewExportSettings(), GetFailedOrders().ToList(), "FailedOrders-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                case "pdf":
                    return GridViewExtension.ExportToPdf(GetFailedGridViewExportSettings(), GetFailedOrders().ToList(), "FailedOrders-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                default:
                    return new EmptyResult();
            }
        }

        private GridViewSettings GetFailedGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewOrders";
            settings.Width = Unit.Percentage(100);

            settings.KeyFieldName = "Id";
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "Status";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = EnumHelper.GetSelectList(typeof(OrderStatuses));
                                     comboBoxProperties.TextField = "Text";
                                     comboBoxProperties.ValueField = "Value";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add("ConsignmentNumber");
            settings.Columns.Add("BinCode");
            settings.Columns.Add("RequestDate").Caption = "Request";
            settings.Columns.Add("RequestRemark").Caption = "Remark";
            settings.Columns.Add("CustomerName");
            settings.Columns.Add("FromName");
            settings.Columns.Add("FromZipCode");
            settings.Columns.Add("FromDistrictCode");
            settings.Columns.Add("FromZoneCode");
            settings.Columns.Add("FromAddress");
            settings.Columns.Add("FromMobilePhone");
            settings.Columns.Add("ToName");
            settings.Columns.Add("ToZipCode");
            settings.Columns.Add("ToDistrictCode");
            settings.Columns.Add("ToZoneCode");
            settings.Columns.Add("ToAddress");
            settings.Columns.Add("ToMobilePhone");
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "ServiceId";
                                     column.Caption = "Service";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetServices();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "ProductTypeId";
                                     column.Caption = "Product Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetProductTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "SizeId";
                                     column.Caption = "Size";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetSizes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            settings.Columns.Add(
                                 column =>
                                 {
                                     column.FieldName = "PaymentTypeId";
                                     column.Caption = "Payment Type";
                                     column.ColumnType = MVCxGridViewColumnType.ComboBox;

                                     var comboBoxProperties = column.PropertiesEdit as ComboBoxProperties;
                                     comboBoxProperties.DataSource = GetPaymentTypes();
                                     comboBoxProperties.TextField = "Name";
                                     comboBoxProperties.ValueField = "Id";
                                     comboBoxProperties.ValueType = typeof(int);
                                 });
            var col = settings.Columns.Add("Price");
            col.PropertiesEdit.DisplayFormatString = "c";
            settings.Columns.Add("CreatedOn");
            settings.Columns.Add("FullfilmentNumber");

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        [HttpPost]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersDeleteFailedOrder)]
        public ActionResult FailedListViewPartialDelete(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var order = db.Orders.FirstOrDefault(x => x.Id == id);
                if (order != null)
                {
                    order.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_FailedListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersEditFailedOrder)]
        public ActionResult EditFailed(long? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var order = (from o in db.Orders
                         where !o.IsDeleted && o.Id == id
                         select o).FirstOrDefault();

            if (order == null)
            {
                return HttpNotFound();
            }

            var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == order.ServiceId);
            if (service == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsPickupDateAllow = service.IsPickupDateAllow;
            ViewBag.IsPickupTimeAllow = service.IsPickupTimeAllow;
            ViewBag.IsDeliveryDateAllow = service.IsDeliveryDateAllow;
            ViewBag.IsDeliveryTimeAllow = service.IsDeliveryTimeAllow;
            ViewBag.OrderStatus = order.Status;

            var pickupTimeIds = service.AvailablePickupTimeSlots.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var deliveryTimeIds = service.AvailableDeliveryTimeSlots.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            ViewBag.PickupTimeslots = db.TimeSlots.Where(x => !x.IsDeleted && pickupTimeIds.Contains(x.Id.ToString()))
                                        .OrderBy(x => x.FromTime)
                                        .ToList()
                                        .Select(
                                                x => new SelectListItem()
                                                     {
                                                         Value = x.Id.ToString(),
                                                         Text = x.TimeSlotName + " (" + x.FromTime.ToString("HH:mm") + " - " + x.ToTime.ToString("HH:mm") + ")"
                                                     })
                                        .ToList();
            ViewBag.DeliveryTimeslots = db.TimeSlots.Where(x => !x.IsDeleted && deliveryTimeIds.Contains(x.Id.ToString()))
                                          .OrderBy(x => x.FromTime)
                                          .ToList()
                                          .Select(
                                                  x => new SelectListItem()
                                                       {
                                                           Value = x.Id.ToString(),
                                                           Text = x.TimeSlotName + " (" + x.FromTime.ToString("HH:mm") + " - " + x.ToTime.ToString("HH:mm") + ")"
                                                       })
                                          .ToList();

            return View(
                        new AddEditOrderViewModel()
                        {
                            ServiceId = order.ServiceId,
                            Status = (OrderStatuses) order.Status,
                            PickupTimeSlotId = order.PickupTimeSlotId,
                            PickupDate = order.PickupDate,
                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                            DeliveryDate = order.DeliveryDate,
                        });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersEditFailedOrder)]
        public async Task<ActionResult> EditFailed(long? id, AddEditOrderViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var order = db.Orders.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
            if (order == null)
            {
                return HttpNotFound();
            }

            var service = db.Services.FirstOrDefault(x => !x.IsDeleted && x.Id == order.ServiceId);
            if (service == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsPickupDateAllow = service.IsPickupDateAllow;
            ViewBag.IsPickupTimeAllow = service.IsPickupTimeAllow;
            ViewBag.IsDeliveryDateAllow = service.IsDeliveryDateAllow;
            ViewBag.IsDeliveryTimeAllow = service.IsDeliveryTimeAllow;
            ViewBag.OrderStatus = order.Status;

            var pickupTimeIds = service.AvailablePickupTimeSlots.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var deliveryTimeIds = service.AvailableDeliveryTimeSlots.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            ViewBag.PickupTimeslots = db.TimeSlots.Where(x => !x.IsDeleted && pickupTimeIds.Contains(x.Id.ToString()))
                                        .OrderBy(x => x.FromTime)
                                        .ToList()
                                        .Select(
                                                x => new SelectListItem()
                                                     {
                                                         Value = x.Id.ToString(),
                                                         Text = x.TimeSlotName + " (" + x.FromTime.ToString("HH:mm") + " - " + x.ToTime.ToString("HH:mm") + ")"
                                                     })
                                        .ToList();
            ViewBag.DeliveryTimeslots = db.TimeSlots.Where(x => !x.IsDeleted && deliveryTimeIds.Contains(x.Id.ToString()))
                                          .OrderBy(x => x.FromTime)
                                          .ToList()
                                          .Select(
                                                  x => new SelectListItem()
                                                       {
                                                           Value = x.Id.ToString(),
                                                           Text = x.TimeSlotName + " (" + x.FromTime.ToString("HH:mm") + " - " + x.ToTime.ToString("HH:mm") + ")"
                                                       })
                                          .ToList();

            if (!string.IsNullOrEmpty(Request.Form["Action"]) && Request.Form["Action"].Equals("ReturnToSender", StringComparison.OrdinalIgnoreCase))
            {
                if (order.Status == (int) OrderStatuses.DeliveryFailed)
                {
                    order.Status = (int) OrderStatuses.ReturnToSender;
                    order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    order.UpdatedOn = DateTime.Now;

                    // Save to history
                    var track = new OrderTracking
                                {
                                    ConsignmentNumber = order.ConsignmentNumber,
                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                    CreatedOn = DateTime.Now,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    Status = order.Status
                                };
                    db.OrderTrackings.Add(track);

                    try
                    {
                        db.SaveChanges();
                        return RedirectToAction("Failed", "Orders");
                    }
                    catch (Exception exc)
                    {
                        // Rollback
                        ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                        LogHelper.Log(exc);
                    }
                }
            }
            else
            {
                if (!service.IsPickupDateAllow)
                {
                    order.PickupDate = DateTime.Now.Date;
                }
                if (!service.IsDeliveryDateAllow)
                {
                    order.DeliveryDate = order.PickupDate;
                }

                OrderTracking track = null;
                if (order.Status == (int) OrderStatuses.PickupFailed)
                {
                    order.PickupTimeSlotId = model.PickupTimeSlotId;
                    order.PickupDate = model.PickupDate;
                    order.DeliveryTimeSlotId = model.DeliveryTimeSlotId;
                    order.DeliveryDate = model.DeliveryDate;
                    order.Status = (int) OrderStatuses.OrderConfirmed;
                    order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    order.UpdatedOn = DateTime.Now;
                    order.BinCode = OrderHelper.GenerateBinCode(order);

                    // Save to history
                    track = new OrderTracking
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                PickupTimeSlotId = order.PickupTimeSlotId,
                                PickupDate = order.PickupDate,
                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                DeliveryDate = order.DeliveryDate,
                                Status = (int) OrderStatuses.RePickup
                            };
                }
                else if (order.Status == (int) OrderStatuses.DeliveryFailed)
                {
                    order.DeliveryTimeSlotId = model.DeliveryTimeSlotId;
                    order.DeliveryDate = model.DeliveryDate;
                    order.Status = (int) OrderStatuses.ItemInDepot;
                    order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    order.UpdatedOn = DateTime.Now;
                    order.BinCode = OrderHelper.GenerateBinCode(order);

                    // Save to history
                    track = new OrderTracking
                            {
                                ConsignmentNumber = order.ConsignmentNumber,
                                CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                CreatedOn = DateTime.Now,
                                PickupTimeSlotId = order.PickupTimeSlotId,
                                PickupDate = order.PickupDate,
                                DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                DeliveryDate = order.DeliveryDate,
                                Status = (int) OrderStatuses.ReDelivery
                            };
                }

                if (track != null)
                {
                    track.BinCode = order.BinCode;
                    db.OrderTrackings.Add(track);
                }

                try
                {
                    db.SaveChanges();

                    // Send notification
                    if (order.Status == (int) OrderStatuses.ItemInDepot)
                    {
                        // Re-Delivery
                        OrderHelper.SendOrderRedeliverySms(Url, order);
                    }

                    return RedirectToAction("Failed", "Orders");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }

            return View(model);
        }

        private GridViewSettings GetVrpGridViewExportSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "gridViewVrpOrders";
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add(column => { column.Caption = "Location ID"; });
            settings.Columns.Add("Id").Caption = "Order ID";
            settings.Columns.Add("Name");
            settings.Columns.Add("PostalCode", MVCxGridViewColumnType.TextBox);
            settings.Columns.Add("Address");
            settings.Columns.Add("Latitude");
            settings.Columns.Add("Longitude");
            settings.Columns.Add("MustBeVisited");
            settings.Columns.Add("ServiceTime", MVCxGridViewColumnType.TimeEdit);
            settings.Columns.Add("PickupAmount");
            settings.Columns.Add("DeliveryAmount");
            settings.Columns.Add("Profit");
            settings.Columns.Add("ProductType");
            settings.Columns.Add("StartTime", MVCxGridViewColumnType.TimeEdit);
            settings.Columns.Add("EndTime", MVCxGridViewColumnType.TimeEdit);

            settings.SettingsDetail.ExportMode = GridViewDetailExportMode.None;

            return settings;
        }

        private bool ProcessCommand(long? id, string command, string note)
        {
            try
            {
                ExtraRequest request = null;
                var order = db.Orders.FirstOrDefault(x => x.Id == id);
                if (order != null)
                {
                    // History
                    var track = new OrderTracking
                                {
                                    ConsignmentNumber = order.ConsignmentNumber,
                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                    CreatedOn = DateTime.Now,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    BinCode = order.BinCode
                                };

                    switch (command.ToLower())
                    {
                        case "confirm":
                            order.Status = (int) OrderStatuses.OrderConfirmed;
                            order.ConfirmedOn = DateTime.Now;
                            order.ConfirmedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.OrderConfirmed;
                            break;
                        case "reject":
                            order.Status = (int) OrderStatuses.OrderRejected;
                            order.ConfirmedOn = DateTime.Now;
                            order.ConfirmedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            order.RejectNote = note;
                            track.Status = (int) OrderStatuses.OrderRejected;
                            break;
                        case "collect":
                            order.Status = (int) OrderStatuses.ItemCollected;
                            order.CollectedOn = DateTime.Now;
                            order.CollectedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.ItemCollected;

                            // Generate delivery ticket
                            request = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.DeliveryRequest && !x.IsDeleted);
                            if (request == null)
                            {
                                request = new ExtraRequest
                                          {
                                              CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              OrderId = order.Id,
                                              RequestType = (int) ExtraRequestType.DeliveryRequest,
                                              RequestContent = "",
                                              CreatedOn = DateTime.Now,
                                              IsDeleted = false,
                                              IsVisited = false,
                                              UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              UpdatedOn = DateTime.Now
                                          };
                                db.ExtraRequests.Add(request);
                            }

                            break;
                        case "failpickup":
                            order.Status = (int) OrderStatuses.PickupFailed;
                            order.CollectedOn = DateTime.Now;
                            order.CollectedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.PickupFailed;

                            // Generate failed ticket
                            request = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.RePickupRequest && !x.IsDeleted);
                            if (request == null)
                            {
                                request = new ExtraRequest
                                          {
                                              CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              OrderId = order.Id,
                                              RequestType = (int) ExtraRequestType.RePickupRequest,
                                              RequestContent = "",
                                              CreatedOn = DateTime.Now,
                                              IsDeleted = false,
                                              IsVisited = false,
                                              UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              UpdatedOn = DateTime.Now
                                          };
                                db.ExtraRequests.Add(request);
                            }

                            break;
                        case "cancel":
                            order.Status = (int) OrderStatuses.OrderCanceled;
                            order.CanceledOn = DateTime.Now;
                            order.CanceledBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.OrderCanceled;
                            break;
                        case "goin":
                            order.Status = (int) OrderStatuses.ItemInDepot;
                            order.ProceededOn = DateTime.Now;
                            order.ProceededBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.ItemInDepot;
                            break;
                        case "goout":
                            order.Status = (int) OrderStatuses.ItemOutDepot;
                            order.ProceededOutOn = DateTime.Now;
                            order.ProceededOutBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.ItemOutDepot;
                            break;
                        case "deliver":
                            order.Status = (int) OrderStatuses.ItemDelivered;
                            order.DeliveredOn = DateTime.Now;
                            order.DeliveredBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.ItemDelivered;
                            break;
                        case "faildeliver":
                            order.Status = (int) OrderStatuses.DeliveryFailed;
                            order.DeliveredOn = DateTime.Now;
                            order.DeliveredBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                            track.Status = (int) OrderStatuses.DeliveryFailed;

                            // Generate failed ticket
                            request = db.ExtraRequests.FirstOrDefault(x => x.OrderId == order.Id && x.RequestType == (int) ExtraRequestType.ReDeliveryRequest && !x.IsDeleted);
                            if (request == null)
                            {
                                request = new ExtraRequest
                                          {
                                              CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              OrderId = order.Id,
                                              RequestType = (int) ExtraRequestType.ReDeliveryRequest,
                                              RequestContent = "",
                                              CreatedOn = DateTime.Now,
                                              IsDeleted = false,
                                              IsVisited = false,
                                              UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                              UpdatedOn = DateTime.Now
                                          };
                                db.ExtraRequests.Add(request);
                            }

                            break;
                        case "sendjob":
                            // Find job
                            if (order.Status == (int) OrderStatuses.OrderConfirmed)
                            {
                                order.Status = (int) OrderStatuses.SpecialOrderForPickup;
                                track.Status = (int) OrderStatuses.SpecialOrderForPickup;
                            }
                            else if (order.Status == (int) OrderStatuses.ItemInDepot)
                            {
                                order.Status = (int) OrderStatuses.SpecialOrderForDelivery;
                                track.Status = (int) OrderStatuses.SpecialOrderForDelivery;
                            }

                            var job = db.Jobs.FirstOrDefault(x => !x.IsDeleted && x.IsSpecial && x.Routes == order.Id.ToString() && !x.TakenOn.HasValue);
                            if (job == null)
                            {
                                job = new Job()
                                      {
                                          Routes = order.Id.ToString(),
                                          IsDeleted = false,
                                          IsSpecial = true,
                                          CreatedOn = DateTime.Now,
                                          CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          UpdatedOn = DateTime.Now,
                                          UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                                      };
                                db.Jobs.Add(job);
                            }

                            break;
                    }

                    db.OrderTrackings.Add(track);

                    db.SaveChanges();

                    if (request != null)
                    {
                        request.Code = OrderHelper.GenerateExtraRequestNumber(request.Id);
                        db.SaveChanges();
                    }

                    // Send notification
                    if (order.Status == (int) OrderStatuses.ItemCollected)
                    {
                        OrderHelper.SendOrderCollectedSms(Url, order, request);
                    }
                    else if (order.Status == (int) OrderStatuses.PickupFailed)
                    {
                        OrderHelper.SendFailedPickupEmail(Url, order, request);
                        OrderHelper.SendFailedPickupSms(Url, order, request);
                    }
                    else if (order.Status == (int) OrderStatuses.ItemDelivered)
                    {
                        OrderHelper.SendOrderDeliveredSms(Url, order);
                    }
                    else if (order.Status == (int) OrderStatuses.DeliveryFailed)
                    {
                        OrderHelper.SendFailedDeliveryEmail(Url, order, request);
                        OrderHelper.SendFailedDeliverySms(Url, order, request);
                    }

                    return true;
                }

                return false;
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return false;
        }

        /*
        * Start Enhancement 3 Sep 2016
        * BulkUpdate
        * 
        */

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersBulkUpdate)]
        public ActionResult BulkUpdate()
        {
            var model = new BulkUpdateViewModel();
            HtmlHelper.ClientValidationEnabled = false;
            ViewBag.CustomerName =  DB.Customers.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
                                                                                                    {
                                                                                                      Text = x.CustomerCompany + (!string.IsNullOrEmpty(x.CompanyPrefix) ?  " (" + x.CompanyPrefix + ") " : ""),
                                                                                                      Value = x.Id.ToString()
                                                                                                    }).ToList();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersBulkUpdate)]
        public async Task<ActionResult> BulkUpdate(BulkUpdateViewModel model)
        {
            // Validate Consignment Numbers
            var arrWorkings = (model.ConsignmentNumbers ?? "").Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None).Select(s => s.Trim()).ToList();
            var arrConsignNumbers = new List<string>();
            if (model.UpdateUsingField == Convert.ToInt32(UpdateUsingField.OrderNumber))
            {
                var companyPrefix = db.Customers.Where(x => x.Id == model.CustomerName).Select(x => x.CompanyPrefix).FirstOrDefault();
                foreach (var singleLine in arrWorkings)
                {
                    if (string.IsNullOrEmpty(singleLine.Trim()))
                    {
                        continue;
                    }

                    var arrSingle = singleLine.Split('|').Select(s => s.Trim()).ToList();
                    if (arrSingle.Count == 1)
                    {
                        string orderNumber = companyPrefix + arrSingle[0];
                        var consignmentNumber = DB.Orders.Where(x => x.OrderNumber == orderNumber).Select(x => x.ConsignmentNumber).FirstOrDefault();
                        arrConsignNumbers.Add(consignmentNumber);
                    }
                    else if (arrSingle.Count > 1)
                    {
                        string orderNumber = companyPrefix + arrSingle[1];
                        var consignmentNumber = DB.Orders.Where(x => x.OrderNumber == orderNumber).Select(x => x.ConsignmentNumber).FirstOrDefault();
                        arrConsignNumbers.Add(consignmentNumber.ToString());
                    }
                }
            }
            else
            {
                foreach (var singleLine in arrWorkings)
                {
                    if (string.IsNullOrEmpty(singleLine.Trim()))
                    {
                        continue;
                    }

                    var arrSingle = singleLine.Split('|').Select(s => s.Trim()).ToList();
                    if (arrSingle.Count == 1)
                    {
                        arrConsignNumbers.Add(arrSingle[0]);
                    }
                    else if (arrSingle.Count > 1)
                    {
                        arrConsignNumbers.Add(arrSingle[1]);
                    }
                }
            }
            var errors = new List<string>();
            ViewBag.ErrorMessages = errors;

            foreach (var singleConsignNumber in arrConsignNumbers)
            {
                if (!db.Orders.Any(o => o.ConsignmentNumber.ToLower() == singleConsignNumber.ToLower()))
                {
                    // Not Found
                    errors.Add("Consignment Number (" + singleConsignNumber + ") is invalid.");
                }
            }

            if (errors.Count > 0)
            {
                // Not valid consignment numbers found
                HtmlHelper.ClientValidationEnabled = false;
                return View(model);
            }

            // Validate Action
            switch (model.UpdateField)
            {
                case 1: // Status
                case 10: // Date & Time
                case 20: // Allocate to drivers
                case 21:
                    break;
                case 99:
                    // Bulk VRP
                    break;
                default:
                    errors.Add("Invalid Update Field Number (" + model.UpdateField + ").");
                    return View(model);
            }

            var rowsAffected = new List<string>();
            var dbOrders = db.Orders.Where(o => arrConsignNumbers.Contains(o.ConsignmentNumber)).ToList();
            foreach (var order in dbOrders)
            {
                switch (model.UpdateField)
                {
                    case 1:
                        // Status
                        switch (model.Status)
                        {
                            case OrderStatuses.OrderConfirmed:
                                if (ProcessCommand(order.Id, "Confirm", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.ItemCollected:
                                if (ProcessCommand(order.Id, "Collect", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.ItemDelivered:
                                if (ProcessCommand(order.Id, "Deliver", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.ItemInDepot:
                                if (ProcessCommand(order.Id, "GoIn", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.ItemOutDepot:
                                if (ProcessCommand(order.Id, "GoOut", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.OrderCanceled:
                                if (ProcessCommand(order.Id, "Cancel", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.OrderRejected:
                                if (ProcessCommand(order.Id, "Reject", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.SpecialOrderForDelivery:
                            case OrderStatuses.SpecialOrderForPickup:
                                if (ProcessCommand(order.Id, "SendJob", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.ReturnToSender:
                                if (order.Status == (int) OrderStatuses.DeliveryFailed)
                                {
                                    order.Status = (int) OrderStatuses.ReturnToSender;
                                    order.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                                    order.UpdatedOn = DateTime.Now;

                                    // Save to history
                                    var track = new OrderTracking
                                                {
                                                    ConsignmentNumber = order.ConsignmentNumber,
                                                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                                    CreatedOn = DateTime.Now,
                                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                                    PickupDate = order.PickupDate,
                                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                                    DeliveryDate = order.DeliveryDate,
                                                    Status = order.Status
                                                };
                                    db.OrderTrackings.Add(track);

                                    try
                                    {
                                        db.SaveChanges();
                                        rowsAffected.Add(order.ConsignmentNumber);
                                    }
                                    catch (Exception exc)
                                    {
                                        // Rollback
                                        ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                                        LogHelper.Log(exc);
                                    }
                                }
                                break;
                            case OrderStatuses.PickupFailed:
                                if (ProcessCommand(order.Id, "FailPickup", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                            case OrderStatuses.DeliveryFailed:
                                if (ProcessCommand(order.Id, "FailDeliver", ""))
                                {
                                    rowsAffected.Add(order.ConsignmentNumber);
                                }
                                break;
                        }

                        break;

                    case 10:
                        // Update Date & Time
                        if (model.PickupDate.HasValue)
                        {
                            order.PickupDate = model.PickupDate;
                        }
                        if (model.DeliveryDate.HasValue)
                        {
                            if (model.DeliveryDate.Value.Date < order.PickupDate.Value.Date)
                            {
                                continue;
                            }

                            order.DeliveryDate = model.DeliveryDate;
                        }

                        if (model.PickupTimeSlotId.HasValue)
                        {
                            var ids = order.Service.AvailablePickupTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (!ids.Contains(model.PickupTimeSlotId.Value.ToString()))
                            {
                                continue;
                            }

                            var timeslot = db.TimeSlots.FirstOrDefault(x => x.Id == model.PickupTimeSlotId.Value);
                            if (timeslot == null)
                            {
                                continue;
                            }

                            if (order.PickupDate.Value.Date == DateTime.Now.Date)
                            {
                                if (timeslot.IsAllowPreOrder)
                                {
                                    if (DateTime.Now.TimeOfDay >= timeslot.FromTime.TimeOfDay.Subtract(TimeSpan.FromMinutes(SettingProvider.CloseWindowTime)) ||
                                        DateTime.Now.TimeOfDay > timeslot.ToTime.TimeOfDay)
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    if (DateTime.Now.TimeOfDay < timeslot.FromTime.TimeOfDay || DateTime.Now.TimeOfDay > timeslot.ToTime.TimeOfDay)
                                    {
                                        continue;
                                    }
                                }
                            }

                            order.PickupTimeSlotId = timeslot.Id;
                        }

                        if (model.DeliveryTimeSlotId.HasValue)
                        {
                            var ids = order.Service.AvailableDeliveryTimeSlots.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (!ids.Contains(model.DeliveryTimeSlotId.Value.ToString()))
                            {
                                continue;
                            }

                            var timeslot = db.TimeSlots.FirstOrDefault(x => x.Id == model.DeliveryTimeSlotId.Value);
                            if (timeslot == null)
                            {
                                continue;
                            }

                            if (order.PickupDate.Value.Date == order.DeliveryDate.Value.Date && timeslot.IsAllowPreOrder)
                            {
                                var isDocument = order.ProductType.ProductTypeName.ToLower().Contains("document");
                                var time = isDocument
                                               ? db.TimeSlots.Where(x => x.Id == order.PickupTimeSlotId.Value).Select(x => x.FromTime).FirstOrDefault()
                                               : db.TimeSlots.Where(x => x.Id == order.PickupTimeSlotId.Value).Select(x => x.ToTime).FirstOrDefault();
                                if (timeslot.FromTime.TimeOfDay < time.TimeOfDay)
                                {
                                    continue;
                                }
                            }

                            order.DeliveryTimeSlotId = timeslot.Id;
                        }

                        // Update Bin Code
                        order.BinCode = OrderHelper.GenerateBinCode(order);

                        try
                        {
                            db.SaveChanges();
                            rowsAffected.Add(order.ConsignmentNumber);
                        }
                        catch (Exception exc)
                        {
                            LogHelper.Log(exc);
                        }

                        break;

                    case 20:
                        // Check if already there's a job
                        if (order.Status != (int) OrderStatuses.SpecialOrderForPickup && order.Status != (int) OrderStatuses.SpecialOrderForDelivery)
                        {
                            // Wrong status, try to send job first
                            ProcessCommand(order.Id, "SendJob", "");
                        }
                        var job = db.Jobs.FirstOrDefault(x => !x.IsDeleted && x.IsSpecial && x.Routes == order.Id.ToString() && !x.TakenOn.HasValue);
                        if (job != null)
                        {
                            if (JobsController.AssignDriverForSpecial(job.Id, "Assign", model.DriverId, User.Identity.IsAuthenticated ? User.Identity.Name : ""))
                            {
                                rowsAffected.Add(order.ConsignmentNumber);
                            }
                        }

                        break;
                }
            }

            if (model.UpdateField == 21)
            {
                // Create job of all these orders
                string nullString = null;
                var locations = new List<dynamic>
                                {
                                    new
                                    {
                                        OrderId = nullString,
                                        CustomerName = "Depot",
                                        Type = 2
                                    }
                                };
                var steps = new List<int> { 1 };
                var depot = db.ZipCodes.FirstOrDefault(x => x.PostalCode.ToLower() == SettingProvider.DepotPostalCode);
                var paths = new List<dynamic>
                            {
                                new
                                {
                                    Latitude = depot?.Latitude ?? 1.3044735,
                                    Longitude = depot?.Longitude ?? 103.7958841
                                }
                            };
                var groups = dbOrders.Where(x => x.Status == (int) OrderStatuses.OrderConfirmed).GroupBy(
                                                                                                         x =>
                                                                                                             new
                                                                                                             {
                                                                                                                 ZipCode = x.FromZipCode,
                                                                                                                 CustomerId = x.CustomerId
                                                                                                             })
                                     .Union(
                                            dbOrders.Where(x => x.Status == (int) OrderStatuses.ItemInDepot).GroupBy(
                                                                                                                     x => new
                                                                                                                          {
                                                                                                                              ZipCode = x.ToZipCode,
                                                                                                                              CustomerId = x.CustomerId
                                                                                                                          }))
                                     .OrderBy(x => x.Select(i => i.BinCode).FirstOrDefault());
                foreach (var group in groups)
                {
                    locations.Add(
                                  new
                                  {
                                      OrderId = string.Join(",", group.Select(x => x.Id)),
                                      CustomerName = group.Select(x => x.Customer.CustomerName).FirstOrDefault(),
                                      Type = group.Select(x => x.Status).FirstOrDefault() == (int) OrderStatuses.OrderConfirmed ? 0 : 1
                                  });
                    steps.Add(steps[steps.Count - 1] + 1);

                    var postalCode = db.ZipCodes.FirstOrDefault(x => x.PostalCode.ToLower() == group.Key.ZipCode.ToLower());
                    paths.Add(
                              new
                              {
                                  Latitude = postalCode?.Latitude ?? 1.3044735,
                                  Longitude = postalCode?.Longitude ?? 103.7958841
                              });
                }

                steps.Add(1);

                var routes = new
                             {
                                 Locations = locations,
                                 Route = new
                                         {
                                             Stops = locations.Count,
                                             NetProfit = 0,
                                             Steps = steps,
                                             Distances = steps,
                                             Loads = steps,
                                             Profits = steps,
                                             DrivingTimes = steps,
                                             Arrivals = steps,
                                             Departures = steps,
                                             WorkingTimes = steps,
                                             Path = JsonConvert.SerializeObject(paths)
                                         }
                             };

                var job = new Job()
                          {
                              Routes = JsonConvert.SerializeObject(routes),
                              IsDeleted = false,
                              VehicleId = db.Vehicles.Where(x => !x.IsDeleted && x.IsActive).Select(x => x.Id).FirstOrDefault(),
                              TotalPickups = locations.Where(x => x.Type == 0).SelectMany(x => ((string) x.OrderId).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)).Count(),
                              TotalDeliveries = locations.Where(x => x.Type == 1).SelectMany(x => ((string) x.OrderId).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)).Count(),
                              TotalDistances = 0,
                              TotalDrivingTime = 0,
                              TotalWorkingTime = 0,
                              IsSpecial = false,
                              CreatedOn = DateTime.Now,
                              CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                              UpdatedOn = DateTime.Now,
                              UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                          };
                db.Jobs.Add(job);

                try
                {
                    db.SaveChanges();

                    if (JobsController.AssignDriver(job.Id, "Assign", model.DriverId, User.Identity.IsAuthenticated ? User.Identity.Name : ""))
                    {
                        rowsAffected.AddRange(dbOrders.Select(x => x.ConsignmentNumber));
                    }
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else if (model.UpdateField == 99)
            {
                // Generate VRP records for inputted orders
                var tempOrders = from order in DB.Orders
                                 where arrConsignNumbers.Contains(order.ConsignmentNumber)
                                 join zipCode in DB.ZipCodes on order.FromZipCode equals zipCode.PostalCode into fromGroup
                                 join zipCode in DB.ZipCodes on order.ToZipCode equals zipCode.PostalCode into toGroup
                                 select new OrderViewModel()
                                        {
                                            CreatedOn = order.CreatedOn,
                                            Id = order.Id,
                                            BinCode = order.BinCode,
                                            CanceledOn = order.CanceledOn,
                                            UpdatedOn = order.UpdatedOn,
                                            ServiceId = order.ServiceId,
                                            PaymentTypeId = order.PaymentTypeId,
                                            ProductTypeId = order.ProductTypeId,
                                            CollectedBy = order.CollectedBy,
                                            CollectedOn = order.CollectedOn,
                                            ConfirmedBy = order.ConfirmedBy,
                                            ConfirmedOn = order.ConfirmedOn,
                                            ConsignmentNumber = order.ConsignmentNumber,
                                            CustomerId = order.CustomerId,
                                            CustomerName = order.Customer.CustomerName,
                                            CustomerCompany = order.Customer.CustomerCompany,
                                            DeliveredBy = order.DeliveredBy,
                                            DeliveredOn = order.DeliveredOn,
                                            FromAddress = order.FromAddress,
                                            FromName = order.FromName,
                                            FromZipCode = order.FromZipCode,
                                            FromMobilePhone = order.FromMobilePhone,
                                            FromSignature = order.FromSignature,
                                            Price = order.Price,
                                            ProceededOn = order.ProceededOn,
                                            ProceededBy = order.ProceededBy,
                                            ProceededOutOn = order.ProceededOutOn,
                                            ProceededOutBy = order.ProceededOutBy,
                                            RejectNote = order.RejectNote,
                                            Remark = order.Remark,
                                            Status = (OrderStatuses) order.Status,
                                            ToAddress = order.ToAddress,
                                            ToName = order.ToName,
                                            ToZipCode = order.ToZipCode,
                                            ToMobilePhone = order.ToMobilePhone,
                                            ToSignature = order.ToSignature,
                                            QrCodePath = order.QrCodePath,
                                            PromoCode = order.PromoCode,
                                            SizeId = order.SizeId,
                                            PickupTimeSlotId = order.PickupTimeSlotId,
                                            PickupDate = order.PickupDate,
                                            DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                            DeliveryDate = order.DeliveryDate,
                                            FromDistrictCode = fromGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                            FromZoneCode = fromGroup.Select(x => x.Zone).FirstOrDefault(),
                                            ToDistrictCode = toGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                            ToZoneCode = toGroup.Select(x => x.Zone).FirstOrDefault(),
                                            OperatorRemark = order.OperatorRemark,
                                            OrderNumber = order.OrderNumber,
                                            RequestDate = order.Status == (int) OrderStatuses.PickupFailed
                                                              ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                              : order.Status == (int) OrderStatuses.DeliveryFailed
                                                                  ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.ReDeliveryRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                                  : null,
                                            RequestRemark = order.Status == (int) OrderStatuses.PickupFailed
                                                                ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault()
                                                                : order.Status == (int) OrderStatuses.DeliveryFailed
                                                                    ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.ReDeliveryRequest && !x.IsDeleted)
                                                                           .Select(x => x.RequestContent)
                                                                           .FirstOrDefault()
                                                                    : "",
                                            IsExchange = order.IsExchange,
                                            TotalCodAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted).Sum(x => x.Amount),
                                            TotalCodCollectedAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted && x.IsCollected).Sum(x => x.CollectedAmount),
                                        };


                var vrpData = GetVrp(tempOrders, CriteriaOperator.Parse(""));
                return GridViewExtension.ExportToXlsx(GetVrpGridViewExportSettings(), vrpData, "ExportVRP-" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
            }

            model.TotalRecordsAffected = rowsAffected.Count;
            ViewBag.RowsAffected = rowsAffected;
            ViewBag.Rows = arrConsignNumbers;
            ViewBag.ProcessSuccess = true;

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersBulkExportVrp)]
        public ActionResult BulkExportVRP()
        {
            return View();
        }

        #region BulkPrintQR Actions

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersBulkPrintQr)]
        public ActionResult BulkPrintQRLabel()
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            ViewBag.PickUpStartDate = startDate;
            ViewBag.PickUpEndDate = startDate.AddMonths(1).AddDays(-1);
            ViewBag.DeliveryStartDate = startDate;
            ViewBag.DeliveryEndDate = startDate.AddMonths(1).AddDays(-1);
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersBulkPrintQr)]
        public ActionResult BulkPrintQRLabelListViewPartial(DateTime? pickupStartDate, DateTime? pickupEndDate, DateTime? deliveryStartDate, DateTime? deliveryEndDate)
        {
            DateTime now = DateTime.Now;

            pickupStartDate = pickupStartDate ?? new DateTime(now.Year, now.Month, 1);
            ViewBag.PickUpStartDate = pickupStartDate;
            ViewBag.PickUpEndDate = pickupEndDate ?? pickupEndDate?.AddMonths(1).AddDays(-1);

            deliveryStartDate = deliveryStartDate ?? new DateTime(now.Year, now.Month, 1);
            ViewBag.DeliveryStartDate = deliveryStartDate;
            ViewBag.DeliveryEndDate = deliveryEndDate ?? deliveryEndDate?.AddMonths(1).AddDays(-1);
            return PartialView("_BulkPrintQRLabelListViewPartial");
        }

        public ActionResult PrintBulkCode(string orderIdsstr, int? startPosition)
        {
            var orderService = new OrderService();
            var orderList = orderService.GetOrdersListByIdArray(orderIdsstr, DB).ToList();

            ViewBag.StartPossition = ((startPosition > 0 ? startPosition : 1) - 1) % 8;

            ViewBag.BulkPrintOrders = orderList;
            if (orderList.Count == 0)
            {
                ViewBag.ErrorMessage = "There is no order corresponding to these Consignment Number(s).";
            }
            return View();
        }

        #endregion

        public static List<VrpViewModel> GetVrp(List<string> consignmentNumbers)
        {
            var orders = DB.Orders.Where(o => consignmentNumbers.Contains(o.ConsignmentNumber)).ToList();
            var sizes = DB.Sizes.Where(x => !x.IsDeleted).ToList();
            var productTypes = DB.ProductTypes.Where(x => !x.IsDeleted).ToList();
            var timeSlots = DB.TimeSlots.Where(x => !x.IsDeleted).ToList();

            // Pickup
            var pickup = orders.Where(x => x.Status == (int) OrderStatuses.OrderConfirmed).GroupBy(
                                                                                                   x => new
                                                                                                        {
                                                                                                            x.FromName,
                                                                                                            x.FromAddress,
                                                                                                            FromZipCode = x.FromZipCode.Trim(),
                                                                                                            x.PickupTimeSlotId,
                                                                                                            x.ProductTypeId
                                                                                                        })
                               .Select(
                                       g => new
                                            {
                                                g.Key.FromName,
                                                g.Key.FromAddress,
                                                g.Key.FromZipCode,
                                                g.Key.PickupTimeSlotId,
                                                g.Key.ProductTypeId,
                                                Orders = g.ToList()
                                            })
                               .ToList();
            var vrp = (from item in pickup
                       join zipCode in DB.ZipCodes on item.FromZipCode equals zipCode.PostalCode into fromGroup
                       from fromZipCode in fromGroup.DefaultIfEmpty()
                       let startTime = timeSlots.Where(x => x.Id == item.PickupTimeSlotId).Select(x => x.FromTime).FirstOrDefault()
                       let endTime = timeSlots.Where(x => x.Id == item.PickupTimeSlotId).Select(x => x.ToTime).FirstOrDefault()
                       select new VrpViewModel()
                              {
                                  Id = string.Join(",", item.Orders.Select(x => x.Id)),
                                  Name = item.FromName,
                                  Address = item.FromAddress,
                                  PostalCode = item.FromZipCode,
                                  Latitude = fromZipCode.Latitude,
                                  Longitude = fromZipCode.Longitude,
                                  PickupAmount = item.Orders.Count, //sizes.Where(x => x.Id == item.SizeId).Select(x => x.ToLength).FirstOrDefault(),
                                  DeliveryAmount = 0,
                                  ServiceTime = new DateTime(0).AddMinutes(SettingProvider.ServiceTime),
                                  StartTime = item.PickupTimeSlotId.HasValue ? startTime : new DateTime(0),
                                  EndTime = item.PickupTimeSlotId.HasValue ? endTime : new DateTime(0).AddSeconds(86400 - 1),
                                  ProductType = productTypes.Where(x => x.Id == item.ProductTypeId).Select(x => x.ProductTypeName).FirstOrDefault(),
                                  MustBeVisited = "Must be visited",
                                  Profit = 0
                              }).ToList();

            // Delivery
            var delivery = orders.Where(x => x.Status == (int) OrderStatuses.ItemInDepot).GroupBy(
                                                                                                  x => new
                                                                                                       {
                                                                                                           x.ToName,
                                                                                                           x.ToAddress,
                                                                                                           ToZipCode = x.ToZipCode.Trim(),
                                                                                                           x.DeliveryTimeSlotId,
                                                                                                           x.ProductTypeId
                                                                                                       })
                                 .Select(
                                         g => new
                                              {
                                                  g.Key.ToName,
                                                  g.Key.ToAddress,
                                                  g.Key.ToZipCode,
                                                  g.Key.DeliveryTimeSlotId,
                                                  g.Key.ProductTypeId,
                                                  Orders = g.ToList()
                                              })
                                 .ToList();
            vrp.AddRange(
                         from item in delivery
                         join zipCode in DB.ZipCodes on item.ToZipCode equals zipCode.PostalCode into toGroup
                         from toZipCode in toGroup.DefaultIfEmpty()
                         let startTime = timeSlots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.FromTime).FirstOrDefault()
                         let endTime = timeSlots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.ToTime).FirstOrDefault()
                         select new VrpViewModel()
                                {
                                    Id = string.Join(",", item.Orders.Select(x => x.Id)),
                                    Name = item.ToName,
                                    Address = item.ToAddress,
                                    PostalCode = item.ToZipCode,
                                    Latitude = toZipCode.Latitude,
                                    Longitude = toZipCode.Longitude,
                                    PickupAmount = 0,
                                    DeliveryAmount = item.Orders.Count, //sizes.Where(x => x.Id == item.SizeId).Select(x => x.ToLength).FirstOrDefault(),
                                    ServiceTime = new DateTime(0).AddMinutes(SettingProvider.ServiceTime),
                                    StartTime = item.DeliveryTimeSlotId.HasValue ? startTime : new DateTime(0),
                                    EndTime = item.DeliveryTimeSlotId.HasValue ? endTime : new DateTime(0).AddSeconds(86400 - 1),
                                    ProductType = productTypes.Where(x => x.Id == item.ProductTypeId).Select(x => x.ProductTypeName).FirstOrDefault(),
                                    MustBeVisited = "Must be visited",
                                    Profit = 0
                                });

            return vrp;
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrderDetail)]
        public ActionResult OrderDetails(string consignmentNumber)
        {
            ViewBag.ConsignmentNumber = consignmentNumber;
            return PartialView("_OrderDetails");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.OrdersShowOrderDetail)]
        public ActionResult OrderDetailsGrid(string consignmentNumber)
        {
            ViewBag.ConsignmentNumber = consignmentNumber;
            var result = from orderDetails in DB.USP_GetOrderDetails(consignmentNumber)
                         select new OrderViewModel
                                {
                                    ConsignmentNumber = consignmentNumber,
                                    DriverOrOPSName = orderDetails.DriverOrOpsName,
                                    DriverOrOPSMobile = orderDetails.DriverOrOpsMobile,
                                    DriverOrOPSTimestamp = orderDetails.DriverOrOpsTimeStamp.Value.Year.ToString() == "1900" ? null : orderDetails.DriverOrOpsTimeStamp
                                };
            return PartialView("_OrderDetailsGrid", result.ToList());
        }

        /*
                [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ManagerRole)]
                /*
                         * End Start Enhancement 3 Sep 2016
                        */

        #region Data queries

        public static IQueryable<OrderViewModel> GetOrders(string cod = "", string codCollected = "", DateTime? start = null, DateTime? end = null)
        {
            cod = cod ?? "";
            codCollected = codCollected ?? "";

            var types = new List<int>();
            var collectedTypes = new List<int>();
            foreach (var t in cod.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                CodOptionTypes type;
                if (Enum.TryParse(t, true, out type))
                {
                    types.Add((int) type);
                }
            }
            foreach (var t in codCollected.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                CodOptionTypes type;
                if (Enum.TryParse(t, true, out type))
                {
                    collectedTypes.Add((int) type);
                }
            }

            var result = from order in DB.Orders
                         where
                         !order.IsDeleted &&
                         (types.Count == 0 ||
                          order.CodOptionOfOrders.Where(c => !c.IsDeleted && types.Contains(c.CodOption.Type)).Select(x => x.CodOption.Type).Count() != 0) &&
                         (collectedTypes.Count == 0 ||
                          order.CodOptionOfOrders.Where(c => !c.IsDeleted && c.IsCollected && collectedTypes.Contains(c.CodOption.Type)).Select(c => c.CodOption.Type).Distinct().Count() != 0)
                         join zipCode in DB.ZipCodes on order.FromZipCode equals zipCode.PostalCode into fromGroup
                         join zipCode in DB.ZipCodes on order.ToZipCode equals zipCode.PostalCode into toGroup
                         select new OrderViewModel()
                                {
                                    CreatedOn = order.CreatedOn,
                                    Id = order.Id,
                                    BinCode = order.BinCode,
                                    CanceledOn = order.CanceledOn,
                                    UpdatedOn = order.UpdatedOn,
                                    ServiceId = order.ServiceId,
                                    PaymentTypeId = order.PaymentTypeId,
                                    ProductTypeId = order.ProductTypeId,
                                    CollectedBy = order.CollectedBy,
                                    CollectedOn = order.CollectedOn,
                                    ConfirmedBy = order.ConfirmedBy,
                                    ConfirmedOn = order.ConfirmedOn,
                                    ConsignmentNumber = order.ConsignmentNumber,
                                    CustomerId = order.CustomerId,
                                    CustomerName = order.Customer.CustomerName,
                                    CustomerCompany = order.Customer.CustomerCompany,
                                    DeliveredBy = order.DeliveredBy,
                                    DeliveredOn = order.DeliveredOn,
                                    FromAddress = order.FromAddress,
                                    FromName = order.FromName,
                                    FromZipCode = order.FromZipCode,
                                    FromMobilePhone = order.FromMobilePhone,
                                    FromSignature = order.FromSignature,
                                    Price = order.Price,
                                    ProceededOn = order.ProceededOn,
                                    ProceededBy = order.ProceededBy,
                                    ProceededOutOn = order.ProceededOutOn,
                                    ProceededOutBy = order.ProceededOutBy,
                                    RejectNote = order.RejectNote,
                                    Remark = order.Remark,
                                    Status = (OrderStatuses) order.Status,
                                    ToAddress = order.ToAddress,
                                    ToName = order.ToName,
                                    ToZipCode = order.ToZipCode,
                                    ToMobilePhone = order.ToMobilePhone,
                                    ToSignature = order.ToSignature,
                                    QrCodePath = order.QrCodePath,
                                    PromoCode = order.PromoCode,
                                    SizeId = order.SizeId,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    FromDistrictCode = fromGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                    FromZoneCode = fromGroup.Select(x => x.Zone).FirstOrDefault(),
                                    ToDistrictCode = toGroup.Select(x => x.DistrictCode).FirstOrDefault(),
                                    ToZoneCode = toGroup.Select(x => x.Zone).FirstOrDefault(),
                                    OperatorRemark = order.OperatorRemark,
                                    OrderNumber = order.OrderNumber,
                                    RequestDate = order.Status == (int) OrderStatuses.PickupFailed
                                                      ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                      : order.Status == (int) OrderStatuses.DeliveryFailed
                                                          ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.ReDeliveryRequest && !x.IsDeleted).Select(x => x.RequestDate).FirstOrDefault()
                                                          : null,
                                    RequestRemark = order.Status == (int) OrderStatuses.PickupFailed
                                                        ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.RePickupRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault()
                                                        : order.Status == (int) OrderStatuses.DeliveryFailed
                                                            ? order.ExtraRequests.Where(x => x.RequestType == (int) ExtraRequestType.ReDeliveryRequest && !x.IsDeleted).Select(x => x.RequestContent).FirstOrDefault()
                                                            : "",
                                    IsExchange = order.IsExchange,
                                    TotalCodAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted).Sum(x => x.Amount),
                                    TotalCodCollectedAmount = order.CodOptionOfOrders.Where(x => !x.IsDeleted && x.IsCollected).Sum(x => x.CollectedAmount),
                                };

            if (start.HasValue && end.HasValue)
            {
                result = result.Where(x => DbFunctions.TruncateTime(x.CreatedOn) >= start && DbFunctions.TruncateTime(x.CreatedOn) <= end);
            }

            return result;
        }

        public static IQueryable<OrderViewModel> GetDataForShowOrders(string cod = "", string codCollected = "")
        {
            if (string.IsNullOrEmpty(cod))
            {
                cod = "";
            }
            else
            {
                cod += ",";
            }
            if (string.IsNullOrEmpty(codCollected))
            {
                codCollected = "";
            }
            else
            {
                codCollected += ",";
            }

            return DB.OrdersViews.Where(
                                        x => ((cod != "" && x.CodTypes == cod) || cod == "") &&
                                             ((codCollected != "" && x.CodCollectedTypes == codCollected) || codCollected == ""))
                     .Select(
                             item => new OrderViewModel()
                                     {
                                         CreatedOn = item.CreatedOn,
                                         Id = item.Id,
                                         BinCode = item.BinCode,
                                         CanceledOn = item.CanceledOn,
                                         UpdatedOn = item.UpdatedOn,
                                         ServiceId = item.ServiceId,
                                         PaymentTypeId = item.PaymentTypeId,
                                         ProductTypeId = item.ProductTypeId,
                                         CollectedBy = item.CollectedBy,
                                         CollectedOn = item.CollectedOn,
                                         ConfirmedBy = item.ConfirmedBy,
                                         ConfirmedOn = item.ConfirmedOn,
                                         ConsignmentNumber = item.ConsignmentNumber,
                                         CustomerId = item.CustomerId,
                                         CustomerName = item.CustomerName,
                                         CustomerCompany = item.CustomerCompany,
                                         DeliveredBy = item.DeliveredBy,
                                         DeliveredOn = item.DeliveredOn,
                                         FromAddress = item.FromAddress,
                                         FromName = item.FromName,
                                         FromZipCode = item.FromZipCode,
                                         FromMobilePhone = item.FromMobilePhone,
                                         FromSignature = item.FromSignature,
                                         Price = item.Price,
                                         ProceededOn = item.ProceededOn,
                                         ProceededBy = item.ProceededBy,
                                         ProceededOutOn = item.ProceededOutOn,
                                         ProceededOutBy = item.ProceededOutBy,
                                         RejectNote = item.RejectNote,
                                         Remark = item.Remark,
                                         Status = (OrderStatuses) item.Status,
                                         ToAddress = item.ToAddress,
                                         ToName = item.ToName,
                                         ToZipCode = item.ToZipCode,
                                         ToMobilePhone = item.ToMobilePhone,
                                         ToSignature = item.ToSignature,
                                         QrCodePath = item.QrCodePath,
                                         PromoCode = item.PromoCode,
                                         SizeId = item.SizeId,
                                         PickupTimeSlotId = item.PickupTimeSlotId,
                                         PickupDate = item.PickupDate,
                                         DeliveryTimeSlotId = item.DeliveryTimeSlotId,
                                         DeliveryDate = item.DeliveryDate,
                                         FromDistrictCode = item.FromDistrictCode,
                                         FromZoneCode = item.FromZoneCode,
                                         ToDistrictCode = item.ToDistrictCode,
                                         ToZoneCode = item.ToZoneCode,
                                         OperatorRemark = item.OperatorRemark,
                                         OrderNumber = item.OrderNumber,
                                         RequestDate = item.Status == (int) OrderStatuses.PickupFailed
                                                           ? item.RPRRequestDate
                                                           : item.Status == (int) OrderStatuses.DeliveryFailed
                                                               ? item.RDRRequestDate
                                                               : null,
                                         RequestRemark = item.Status == (int) OrderStatuses.PickupFailed
                                                             ? item.RPRRequestContent
                                                             : item.Status == (int) OrderStatuses.DeliveryFailed
                                                                 ? item.RDRRequestContent
                                                                 : "",
                                         IsExchange = item.IsExchange,
                                         TotalCodAmount = item.TotalCodAmount,
                                         TotalCodCollectedAmount = item.TotalCodCollectedAmount,
                                         //CODOption = GetCodOptions(item.Id),
                                         DriverOrOPSName = item.DriverOrOpsName,
                                         DriverOrOPSMobile = item.DriverOrOpsMobile,
                                         DriverOrOPSTimestamp = item.DriverOrOpsTimeStamp.Value.Year.ToString() == "1900" ? null : item.DriverOrOpsTimeStamp
                                     });

            //var spResult = DB.USP_FetchAllOrders(cod, codCollected)
            //                 .Select(
            //                         item => new OrderViewModel()
            //                                 {
            //                                     CreatedOn = item.CreatedOn,
            //                                     Id = item.Id,
            //                                     BinCode = item.BinCode,
            //                                     CanceledOn = item.CanceledOn,
            //                                     UpdatedOn = item.UpdatedOn,
            //                                     ServiceId = item.ServiceId,
            //                                     PaymentTypeId = item.PaymentTypeId,
            //                                     ProductTypeId = item.ProductTypeId,
            //                                     CollectedBy = item.CollectedBy,
            //                                     CollectedOn = item.CollectedOn,
            //                                     ConfirmedBy = item.ConfirmedBy,
            //                                     ConfirmedOn = item.ConfirmedOn,
            //                                     ConsignmentNumber = item.ConsignmentNumber,
            //                                     CustomerId = item.CustomerId,
            //                                     CustomerName = item.CustomerName,
            //                                     CustomerCompany = item.CustomerCompany,
            //                                     DeliveredBy = item.DeliveredBy,
            //                                     DeliveredOn = item.DeliveredOn,
            //                                     FromAddress = item.FromAddress,
            //                                     FromName = item.FromName,
            //                                     FromZipCode = item.FromZipCode,
            //                                     FromMobilePhone = item.FromMobilePhone,
            //                                     FromSignature = item.FromSignature,
            //                                     Price = item.Price,
            //                                     ProceededOn = item.ProceededOn,
            //                                     ProceededBy = item.ProceededBy,
            //                                     ProceededOutOn = item.ProceededOutOn,
            //                                     ProceededOutBy = item.ProceededOutBy,
            //                                     RejectNote = item.RejectNote,
            //                                     Remark = item.Remark,
            //                                     Status = (OrderStatuses) item.Status,
            //                                     ToAddress = item.ToAddress,
            //                                     ToName = item.ToName,
            //                                     ToZipCode = item.ToZipCode,
            //                                     ToMobilePhone = item.ToMobilePhone,
            //                                     ToSignature = item.ToSignature,
            //                                     QrCodePath = item.QrCodePath,
            //                                     PromoCode = item.PromoCode,
            //                                     SizeId = item.SizeId,
            //                                     PickupTimeSlotId = item.PickupTimeSlotId,
            //                                     PickupDate = item.PickupDate,
            //                                     DeliveryTimeSlotId = item.DeliveryTimeSlotId,
            //                                     DeliveryDate = item.DeliveryDate,
            //                                     FromDistrictCode = item.FromDistrictCode,
            //                                     FromZoneCode = item.FromZoneCode,
            //                                     ToDistrictCode = item.ToDistrictCode,
            //                                     ToZoneCode = item.ToZoneCode,
            //                                     OperatorRemark = item.OperatorRemark,
            //                                     OrderNumber = item.OrderNumber,
            //                                     RequestDate = item.Status == (int) OrderStatuses.PickupFailed
            //                                                       ? item.RPRRequestDate
            //                                                       : item.Status == (int) OrderStatuses.DeliveryFailed
            //                                                           ? item.RDRRequestDate
            //                                                           : null,
            //                                     RequestRemark = item.Status == (int) OrderStatuses.PickupFailed
            //                                                         ? item.RPRRequestContent
            //                                                         : item.Status == (int) OrderStatuses.DeliveryFailed
            //                                                             ? item.RDRRequestContent
            //                                                             : "",
            //                                     IsExchange = item.IsExchange,
            //                                     TotalCodAmount = item.TotalCodAmount,
            //                                     TotalCodCollectedAmount = item.TotalCodCollectedAmount,
            //                                     //CODOption = GetCodOptions(item.Id),
            //                                     DriverOrOPSName = item.DriverOrOpsName,
            //                                     DriverOrOPSMobile = item.DriverOrOpsMobile,
            //                                     DriverOrOPSTimestamp = item.DriverOrOpsTimeStamp.ToString() == "1/1/1900 12:00:00 AM" ? null : item.DriverOrOpsTimeStamp
            //                                 });
            //return spResult.Take(10).AsQueryable();
        }

        public static List<ExportOrderViewModel> GetExportedOrders(CriteriaOperator conditions, string cod = "", string codCollected = "", DateTime? startDate = null, DateTime? endDate = null)
        {
            var result = from order in (GetOrders(cod, codCollected, startDate, endDate).AppendWhere(new CriteriaToExpressionConverter(), conditions) as IQueryable<OrderViewModel>).ToList()
                         select new ExportOrderViewModel()
                                {
                                    CreatedOn = order.CreatedOn,
                                    Id = order.Id,
                                    BinCode = order.BinCode,
                                    CanceledOn = order.CanceledOn,
                                    UpdatedOn = order.UpdatedOn,
                                    ServiceId = order.ServiceId,
                                    PaymentTypeId = order.PaymentTypeId,
                                    ProductTypeId = order.ProductTypeId,
                                    CollectedBy = order.CollectedBy,
                                    CollectedOn = order.CollectedOn,
                                    ConfirmedBy = order.ConfirmedBy,
                                    ConfirmedOn = order.ConfirmedOn,
                                    ConsignmentNumber = order.ConsignmentNumber,
                                    CustomerId = order.CustomerId,
                                    CustomerName = order.CustomerName,
                                    CustomerCompany = order.CustomerCompany,
                                    DeliveredBy = order.DeliveredBy,
                                    DeliveredOn = order.DeliveredOn,
                                    FromAddress = order.FromAddress,
                                    FromName = order.FromName,
                                    FromZipCode = order.FromZipCode,
                                    FromMobilePhone = order.FromMobilePhone,
                                    FromSignature = order.FromSignature,
                                    Price = order.Price,
                                    ProceededOn = order.ProceededOn,
                                    ProceededBy = order.ProceededBy,
                                    ProceededOutOn = order.ProceededOutOn,
                                    ProceededOutBy = order.ProceededOutBy,
                                    RejectNote = order.RejectNote,
                                    Remark = order.Remark,
                                    Status = (OrderStatuses) order.Status,
                                    ToAddress = order.ToAddress,
                                    ToName = order.ToName,
                                    ToZipCode = order.ToZipCode,
                                    ToMobilePhone = order.ToMobilePhone,
                                    ToSignature = order.ToSignature,
                                    QrCodePath = order.QrCodePath,
                                    PromoCode = order.PromoCode,
                                    SizeId = order.SizeId,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    FromDistrictCode = order.FromDistrictCode,
                                    FromZoneCode = order.FromZoneCode,
                                    ToDistrictCode = order.ToDistrictCode,
                                    ToZoneCode = order.ToZoneCode,
                                    RequestDate = order.RequestDate,
                                    RequestRemark = order.RequestRemark,
                                    OperatorRemark = order.OperatorRemark,
                                    Cod = string.Join(
                                                      "\n",
                                                      DB.CodOptionOfOrders
                                                        .Where(x => x.OrderId == order.Id && !x.IsDeleted && !x.CodOption.IsDeleted)
                                                        .ToList()
                                                        .Select(x => ((CodOptionTypes) x.CodOption.Type).ToString() + "\n" + x.Amount.ToString("c"))),
                                    CodCollected = string.Join(
                                                               "\n",
                                                               DB.CodOptionOfOrders
                                                                 .Where(x => x.OrderId == order.Id && !x.IsDeleted && !x.CodOption.IsDeleted && x.IsCollected)
                                                                 .ToList()
                                                                 .Select(x => ((CodOptionTypes) x.CodOption.Type).ToString() + "\n" + x.CollectedAmount.ToString("c"))),
                                    IsExchange = order.IsExchange,
                                    TotalCodAmount = order.TotalCodAmount,
                                    TotalCodCollectedAmount = order.TotalCodCollectedAmount,
                                    OrderNumber = order.OrderNumber
                                };

            return result.ToList();
        }

        public static IQueryable<OrderViewModel> GetTodayOrders(string cod = "", string codCollected = "")
        {
            var beginDate = DateTime.Now.Date;
            var endDate = beginDate.AddDays(1);
            return GetOrders(cod, codCollected).Where(
                                                      order =>
                                                          order.Status != OrderStatuses.DeliveryFailed && order.Status != OrderStatuses.ItemDelivered && order.Status != OrderStatuses.OrderCanceled
                                                          && order.Status != OrderStatuses.OrderRejected && order.Status != OrderStatuses.PickupFailed && order.Status != OrderStatuses.SpecialOrderForPickup
                                                          && order.Status != OrderStatuses.SpecialOrderForDelivery && order.Status != OrderStatuses.ItemHasBeingPicked &&
                                                          ((order.PickupDate >= beginDate && order.PickupDate < endDate) ||
                                                           (order.DeliveryDate >= beginDate && order.DeliveryDate < endDate)));
        }

        public static List<ExportOrderViewModel> GetExportedTodayOrders(string cod = "", string codCollected = "")
        {
            var result = from order in GetTodayOrders(cod, codCollected).ToList()
                         select new ExportOrderViewModel()
                                {
                                    CreatedOn = order.CreatedOn,
                                    Id = order.Id,
                                    BinCode = order.BinCode,
                                    CanceledOn = order.CanceledOn,
                                    UpdatedOn = order.UpdatedOn,
                                    ServiceId = order.ServiceId,
                                    PaymentTypeId = order.PaymentTypeId,
                                    ProductTypeId = order.ProductTypeId,
                                    CollectedBy = order.CollectedBy,
                                    CollectedOn = order.CollectedOn,
                                    ConfirmedBy = order.ConfirmedBy,
                                    ConfirmedOn = order.ConfirmedOn,
                                    ConsignmentNumber = order.ConsignmentNumber,
                                    CustomerId = order.CustomerId,
                                    CustomerName = order.CustomerName,
                                    CustomerCompany = order.CustomerCompany,
                                    DeliveredBy = order.DeliveredBy,
                                    DeliveredOn = order.DeliveredOn,
                                    FromAddress = order.FromAddress,
                                    FromName = order.FromName,
                                    FromZipCode = order.FromZipCode,
                                    FromMobilePhone = order.FromMobilePhone,
                                    FromSignature = order.FromSignature,
                                    Price = order.Price,
                                    ProceededOn = order.ProceededOn,
                                    ProceededBy = order.ProceededBy,
                                    ProceededOutOn = order.ProceededOutOn,
                                    ProceededOutBy = order.ProceededOutBy,
                                    RejectNote = order.RejectNote,
                                    Remark = order.Remark,
                                    Status = (OrderStatuses) order.Status,
                                    ToAddress = order.ToAddress,
                                    ToName = order.ToName,
                                    ToZipCode = order.ToZipCode,
                                    ToMobilePhone = order.ToMobilePhone,
                                    ToSignature = order.ToSignature,
                                    QrCodePath = order.QrCodePath,
                                    PromoCode = order.PromoCode,
                                    SizeId = order.SizeId,
                                    PickupTimeSlotId = order.PickupTimeSlotId,
                                    PickupDate = order.PickupDate,
                                    DeliveryTimeSlotId = order.DeliveryTimeSlotId,
                                    DeliveryDate = order.DeliveryDate,
                                    FromDistrictCode = order.FromDistrictCode,
                                    FromZoneCode = order.FromZoneCode,
                                    ToDistrictCode = order.ToDistrictCode,
                                    ToZoneCode = order.ToZoneCode,
                                    RequestDate = order.RequestDate,
                                    RequestRemark = order.RequestRemark,
                                    OperatorRemark = order.OperatorRemark,
                                    Cod = string.Join(
                                                      "\n",
                                                      DB.CodOptionOfOrders
                                                        .Where(x => x.OrderId == order.Id && !x.IsDeleted && !x.CodOption.IsDeleted)
                                                        .ToList()
                                                        .Select(x => ((CodOptionTypes) x.CodOption.Type).ToString() + "\n" + x.Amount.ToString("c"))),
                                    CodCollected = string.Join(
                                                               "\n",
                                                               DB.CodOptionOfOrders
                                                                 .Where(x => x.OrderId == order.Id && !x.IsDeleted && !x.CodOption.IsDeleted && x.IsCollected)
                                                                 .ToList()
                                                                 .Select(x => ((CodOptionTypes) x.CodOption.Type).ToString() + "\n" + x.CollectedAmount.ToString("c"))),
                                    IsExchange = order.IsExchange,
                                    TotalCodAmount = order.TotalCodAmount,
                                    TotalCodCollectedAmount = order.TotalCodCollectedAmount,
                                };

            return result.ToList();
        }

        public static IQueryable<OrderViewModel> GetFailedOrders()
        {
            return GetOrders().Where(order => order.Status == OrderStatuses.DeliveryFailed || order.Status == OrderStatuses.PickupFailed);
        }

        public static List<VrpViewModel> GetVrp(IQueryable<OrderViewModel> source, CriteriaOperator conditions)
        {
            var orders = (source.AppendWhere(new CriteriaToExpressionConverter(), conditions) as IQueryable<OrderViewModel>).ToList();
            var sizes = DB.Sizes.Where(x => !x.IsDeleted).ToList();
            var productTypes = DB.ProductTypes.Where(x => !x.IsDeleted).ToList();
            var timeSlots = DB.TimeSlots.Where(x => !x.IsDeleted).ToList();

            // Pickup
            var pickup = orders.Where(x => x.Status == OrderStatuses.OrderConfirmed).GroupBy(
                                                                                             x => new
                                                                                                  {
                                                                                                      x.FromName,
                                                                                                      x.FromAddress,
                                                                                                      FromZipCode = x.FromZipCode.Trim(),
                                                                                                      x.PickupTimeSlotId,
                                                                                                      x.ProductTypeId
                                                                                                  })
                               .Select(
                                       g => new
                                            {
                                                g.Key.FromName,
                                                g.Key.FromAddress,
                                                g.Key.FromZipCode,
                                                g.Key.PickupTimeSlotId,
                                                g.Key.ProductTypeId,
                                                Orders = g.ToList()
                                            })
                               .ToList();
            var vrp = (from item in pickup
                       join zipCode in DB.ZipCodes on item.FromZipCode equals zipCode.PostalCode into fromGroup
                       from fromZipCode in fromGroup.DefaultIfEmpty()
                       let startTime = timeSlots.Where(x => x.Id == item.PickupTimeSlotId).Select(x => x.FromTime).FirstOrDefault()
                       let endTime = timeSlots.Where(x => x.Id == item.PickupTimeSlotId).Select(x => x.ToTime).FirstOrDefault()
                       select new VrpViewModel()
                              {
                                  Id = string.Join(",", item.Orders.Select(x => x.Id)),
                                  Name = item.FromName,
                                  Address = item.FromAddress,
                                  PostalCode = item.FromZipCode,
                                  Latitude = fromZipCode.Latitude,
                                  Longitude = fromZipCode.Longitude,
                                  PickupAmount = item.Orders.Count, //sizes.Where(x => x.Id == item.SizeId).Select(x => x.ToLength).FirstOrDefault(),
                                  DeliveryAmount = 0,
                                  ServiceTime = new DateTime(0).AddMinutes(SettingProvider.ServiceTime),
                                  StartTime = item.PickupTimeSlotId.HasValue ? startTime : new DateTime(0),
                                  EndTime = item.PickupTimeSlotId.HasValue ? endTime : new DateTime(0).AddSeconds(86400 - 1),
                                  ProductType = productTypes.Where(x => x.Id == item.ProductTypeId).Select(x => x.ProductTypeName).FirstOrDefault(),
                                  MustBeVisited = "Must be visited",
                                  Profit = 0
                              }).ToList();

            // Delivery
            var delivery = orders.Where(x => x.Status == OrderStatuses.ItemInDepot).GroupBy(
                                                                                            x => new
                                                                                                 {
                                                                                                     x.ToName,
                                                                                                     x.ToAddress,
                                                                                                     ToZipCode = x.ToZipCode.Trim(),
                                                                                                     x.DeliveryTimeSlotId,
                                                                                                     x.ProductTypeId
                                                                                                 })
                                 .Select(
                                         g => new
                                              {
                                                  g.Key.ToName,
                                                  g.Key.ToAddress,
                                                  g.Key.ToZipCode,
                                                  g.Key.DeliveryTimeSlotId,
                                                  g.Key.ProductTypeId,
                                                  Orders = g.ToList()
                                              })
                                 .ToList();
            vrp.AddRange(
                         from item in delivery
                         join zipCode in DB.ZipCodes on item.ToZipCode equals zipCode.PostalCode into toGroup
                         from toZipCode in toGroup.DefaultIfEmpty()
                         let startTime = timeSlots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.FromTime).FirstOrDefault()
                         let endTime = timeSlots.Where(x => x.Id == item.DeliveryTimeSlotId).Select(x => x.ToTime).FirstOrDefault()
                         select new VrpViewModel()
                                {
                                    Id = string.Join(",", item.Orders.Select(x => x.Id)),
                                    Name = item.ToName,
                                    Address = item.ToAddress,
                                    PostalCode = item.ToZipCode,
                                    Latitude = toZipCode.Latitude,
                                    Longitude = toZipCode.Longitude,
                                    PickupAmount = 0,
                                    DeliveryAmount = item.Orders.Count, //sizes.Where(x => x.Id == item.SizeId).Select(x => x.ToLength).FirstOrDefault(),
                                    ServiceTime = new DateTime(0).AddMinutes(SettingProvider.ServiceTime),
                                    StartTime = item.DeliveryTimeSlotId.HasValue ? startTime : new DateTime(0),
                                    EndTime = item.DeliveryTimeSlotId.HasValue ? endTime : new DateTime(0).AddSeconds(86400 - 1),
                                    ProductType = productTypes.Where(x => x.Id == item.ProductTypeId).Select(x => x.ProductTypeName).FirstOrDefault(),
                                    MustBeVisited = "Must be visited",
                                    Profit = 0
                                });

            return vrp;
        }

        public static List<PaymentTypeViewModel> GetPaymentTypes()
        {
            return (from type in DB.PaymentTypes
                    where !type.IsDeleted
                    select new PaymentTypeViewModel()
                           {
                               Name = type.PaymentTypeName,
                               Id = type.Id
                           }).ToList();
        }

        public static List<ProductTypeViewModel> GetProductTypes()
        {
            return (from type in DB.ProductTypes
                    where !type.IsDeleted
                    select new ProductTypeViewModel()
                           {
                               Name = type.ProductTypeName,
                               Id = type.Id
                           }).ToList();
        }

        public static List<ServiceViewModel> GetServices()
        {
            return (from service in DB.Services
                    where !service.IsDeleted
                    select new ServiceViewModel()
                           {
                               Name = service.ServiceName,
                               Id = service.Id
                           }).ToList();
        }

        public static List<SizeViewModel> GetSizes()
        {
            return (from size in DB.Sizes
                    where !size.IsDeleted
                    select new SizeViewModel()
                           {
                               Name = size.SizeName,
                               Id = size.Id
                           }).ToList();
        }

        public static List<CodOptionModel> GetCodOptions(long orderId)
        {
            var cod = DB.CodOptionOfOrders
                        .Where(x => x.OrderId == orderId && !x.IsDeleted)
                        .Select(
                                x => new CodOptionModel()
                                     {
                                         Amount = x.Amount,
                                         Type = x.CodOption.Type,
                                         CollectedAmount = x.CollectedAmount,
                                         IsCollected = x.IsCollected
                                     })
                        .ToList();
            return cod;
        }

        #endregion
    }
}
