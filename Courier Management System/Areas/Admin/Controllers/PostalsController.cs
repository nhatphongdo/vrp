﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class PostalsController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "ZipCodesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesShowPostals)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesShowPostals)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesAddPostal)]
        public ActionResult Add()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesAddPostal)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditZipCodeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var zipCode = new ZipCode()
                              {
                                  BuildingName = model.BuildingName,
                                  BuildingType = model.BuildingType,
                                  Latitude = model.Latitude,
                                  Longitude = model.Longitude,
                                  DistrictCode = model.DistrictCode,
                                  Zone = model.Zone,
                                  PostalCode = model.PostalCode,
                                  StreetName = model.StreetName,
                                  UnitNumber = model.UnitNumber,
                                  CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                  CreatedOn = DateTime.Now,
                                  UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                  UpdatedOn = DateTime.Now
                              };

                db.ZipCodes.Add(zipCode);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Postals");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesEditPostal)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from code in db.ZipCodes
                         where code.Id == id
                         select new AddEditZipCodeViewModel()
                                {
                                    Latitude = code.Latitude,
                                    Longitude = code.Longitude,
                                    StreetName = code.StreetName,
                                    PostalCode = code.PostalCode,
                                    BuildingType = code.BuildingType,
                                    UnitNumber = code.UnitNumber,
                                    BuildingName = code.BuildingName,
                                    DistrictCode = code.DistrictCode,
                                    Zone = code.Zone
                                }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesEditPostal)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditZipCodeViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var code = db.ZipCodes.FirstOrDefault(x => x.Id == id);
                if (code == null)
                {
                    return HttpNotFound();
                }

                code.BuildingName = model.BuildingName;
                code.BuildingType = model.BuildingType;
                code.Latitude = model.Latitude;
                code.Longitude = model.Longitude;
                code.PostalCode = model.PostalCode;
                code.StreetName = model.StreetName;
                code.UnitNumber = model.UnitNumber;
                code.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                code.UpdatedOn = DateTime.Now;
                code.DistrictCode = model.DistrictCode;
                code.Zone = model.Zone;
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Postals");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Import()
        {
            return null;
            //var file = Request.Files.Get("importFile");

            //if (!string.IsNullOrEmpty(file?.FileName))
            //{
            //    IExcelDataReader excelReader;

            //    if (file.FileName.ToLower().EndsWith(".xls"))
            //    {
            //        // Load data from Excel 2003 file
            //        excelReader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
            //    }
            //    else if (file.FileName.ToLower().EndsWith(".xlsx"))
            //    {
            //        // Load data from Excel 2007 file
            //        excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
            //    }
            //    else
            //    {
            //        excelReader = null;
            //    }

            //    if (excelReader != null)
            //    {
            //        try
            //        {
            //            excelReader.IsFirstRowAsColumnNames = true;
            //            var data = excelReader.AsDataSet();

            //            if (data.Tables.Count == 0)
            //            {
            //                // There is no table in sheet
            //                TempData["ErrorMessage"] = "Import file does not have correct format.";
            //            }
            //            else
            //            {
            //                var table = data.Tables[0];
            //                for (var i = 0; i < table.Rows.Count; i++)
            //                {
            //                    // Process each row
            //                    var row = table.Rows[i];

            //                    var postalCode = new PostalCode()
            //                    {
            //                        PostalCode1 = row[0].ToString(),
            //                        BuildingName = row[1].ToString(),
            //                        UnitNumber = row[2].ToString(),
            //                        StreetName = row[3].ToString(),
            //                        BuildingType = row[4].ToString(),
            //                        Longitude = row[5].ToString() != "" ? row[5] as double? : null,
            //                        Latitude = row[6].ToString() != "" ? row[6] as double? : null,
            //                        CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
            //                        CreatedOn = DateTime.Now,
            //                        UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
            //                        UpdatedOn = DateTime.Now
            //                    };
            //                    context.PostalCodes.Add(postalCode);
            //                    context.SaveChanges();
            //                }
            //            }
            //        }
            //        catch (Exception exc)
            //        {
            //            if (exc is InvalidCastException || exc is NullReferenceException)
            //            {
            //                TempData["ErrorMessage"] = "Import file does not have correct format.";
            //            }
            //            else
            //            {
            //                TempData["ErrorMessage"] = "There is error in processing. Please contact to administrator for support with these information: <br />" +
            //                    exc.Message + "<br />" + exc.StackTrace;
            //            }
            //        }
            //        finally
            //        {
            //            excelReader.Close();
            //        }
            //    }
            //    else
            //    {
            //        TempData["ErrorMessage"] = "Import file does not have correct format.";
            //    }
            //}
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesDeletePostal)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            try
            {
                var code = db.ZipCodes.FirstOrDefault(x => x.Id == id);
                if (code != null)
                {
                    db.ZipCodes.Remove(code);
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesUpdateGeolocation)]
        public ActionResult Update()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PostalCodesUpdateGeolocation)]
        [HttpPost]
        public ActionResult Update(FormCollection formCollection)
        {
            // Default: AIzaSyA4u-ZTqs9Hs8d-XmhMI4eJPx6vLgQn47U
            if (string.IsNullOrEmpty(formCollection["ServerKey"]))
            {
                ViewBag.ErrorMessages = new[] { "You must input Server Key to start retrieving Geolocation." };
                return View();
            }

            var startIndex = 0;
            if (!string.IsNullOrEmpty(formCollection["StartFrom"]) && !int.TryParse(formCollection["StartFrom"], out startIndex))
            {
                ViewBag.ErrorMessages = new[] { "The Start From Index must be a number." };
                return View();
            }

            var numberItems = 0;
            if (!string.IsNullOrEmpty(formCollection["NumberItems"]) && !int.TryParse(formCollection["NumberItems"], out numberItems))
            {
                ViewBag.ErrorMessages = new[] { "The Number of Items must be a number." };
                return View();
            }

            if (numberItems <= 0 || numberItems > 100000)
            {
                // Limit to 100,000 items / day
                numberItems = 10000;
            }

            var db = new CourierDBEntities();
            var postalCodes = db.ZipCodes.OrderBy(x => x.Id).Skip(startIndex).Take(numberItems).ToList();
            var counter = 0;
            foreach (var postalCode in postalCodes)
            {
                var address = $"{postalCode.UnitNumber} {postalCode.StreetName}";
                var result = MapServices.GetGeocodingOfAddress(formCollection["ServerKey"], address);
                if (result == null)
                {
                    // Error, stop now
                    ViewBag.ErrorMessages = new[] { "Import process is failed at item #" + (startIndex + counter) + "<br />Detail error: Cannot retrieve geolocation from Google service." };
                    break;
                }

                try
                {
                    // Save result to DB
                    postalCode.Latitude = result[0];
                    postalCode.Longitude = result[1];
                    postalCode.UpdatedBy = "Geolocation";
                    postalCode.UpdatedOn = DateTime.Now;

                    if (!string.IsNullOrEmpty(postalCode.PostalCode))
                    {
                        var zoneStr = postalCode.PostalCode.Substring(0, 2);
                        var zoneIndex = 0;
                        if (int.TryParse(zoneStr, out zoneIndex))
                        {
                            // Set Zone
                            if ((zoneIndex >= 1 && zoneIndex <= 8) || (zoneIndex >= 17 && zoneIndex <= 27))
                            {
                                postalCode.Zone = "C";
                            }
                            else if ((zoneIndex >= 9 && zoneIndex <= 16) || (zoneIndex >= 58 && zoneIndex <= 64))
                            {
                                postalCode.Zone = "W";
                            }
                            else if ((zoneIndex >= 28 && zoneIndex <= 33) || (zoneIndex >= 53 && zoneIndex <= 57)
                                     || (zoneIndex >= 77 && zoneIndex <= 80) || zoneIndex == 82)
                            {
                                postalCode.Zone = "NE";
                            }
                            else if ((zoneIndex >= 65 && zoneIndex <= 73) || zoneIndex == 75 || zoneIndex == 76)
                            {
                                postalCode.Zone = "N";
                            }
                            else if ((zoneIndex >= 34 && zoneIndex <= 52) || zoneIndex == 81)
                            {
                                postalCode.Zone = "E";
                            }

                            // Set District
                            if (zoneIndex >= 1 && zoneIndex <= 6)
                            {
                                postalCode.DistrictCode = "01";
                            }
                            else if (zoneIndex <= 8)
                            {
                                postalCode.DistrictCode = "02";
                            }
                            else if (zoneIndex <= 10)
                            {
                                postalCode.DistrictCode = "04";
                            }
                            else if (zoneIndex <= 13)
                            {
                                postalCode.DistrictCode = "05";
                            }
                            else if (zoneIndex <= 16)
                            {
                                postalCode.DistrictCode = "03";
                            }
                            else if (zoneIndex <= 17)
                            {
                                postalCode.DistrictCode = "06";
                            }
                            else if (zoneIndex <= 19)
                            {
                                postalCode.DistrictCode = "07";
                            }
                            else if (zoneIndex <= 21)
                            {
                                postalCode.DistrictCode = "08";
                            }
                            else if (zoneIndex <= 23)
                            {
                                postalCode.DistrictCode = "09";
                            }
                            else if (zoneIndex <= 27)
                            {
                                postalCode.DistrictCode = "10";
                            }
                            else if (zoneIndex <= 30)
                            {
                                postalCode.DistrictCode = "11";
                            }
                            else if (zoneIndex <= 33)
                            {
                                postalCode.DistrictCode = "12";
                            }
                            else if (zoneIndex <= 37)
                            {
                                postalCode.DistrictCode = "13";
                            }
                            else if (zoneIndex <= 41)
                            {
                                postalCode.DistrictCode = "14";
                            }
                            else if (zoneIndex <= 45)
                            {
                                postalCode.DistrictCode = "15";
                            }
                            else if (zoneIndex <= 48)
                            {
                                postalCode.DistrictCode = "16";
                            }
                            else if (zoneIndex <= 50 || zoneIndex == 81)
                            {
                                postalCode.DistrictCode = "17";
                            }
                            else if (zoneIndex <= 52)
                            {
                                postalCode.DistrictCode = "18";
                            }
                            else if (zoneIndex <= 55 || zoneIndex == 82)
                            {
                                postalCode.DistrictCode = "19";
                            }
                            else if (zoneIndex <= 57)
                            {
                                postalCode.DistrictCode = "20";
                            }
                            else if (zoneIndex <= 59)
                            {
                                postalCode.DistrictCode = "21";
                            }
                            else if (zoneIndex <= 64)
                            {
                                postalCode.DistrictCode = "22";
                            }
                            else if (zoneIndex <= 68)
                            {
                                postalCode.DistrictCode = "23";
                            }
                            else if (zoneIndex <= 71)
                            {
                                postalCode.DistrictCode = "24";
                            }
                            else if (zoneIndex <= 73)
                            {
                                postalCode.DistrictCode = "25";
                            }
                            else if (zoneIndex <= 76)
                            {
                                postalCode.DistrictCode = "27";
                            }
                            else if (zoneIndex <= 78)
                            {
                                postalCode.DistrictCode = "26";
                            }
                            else if (zoneIndex <= 80)
                            {
                                postalCode.DistrictCode = "28";
                            }
                        }
                    }

                    db.SaveChanges();

                    ++counter;
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Import process is failed at item #" + (startIndex + counter) + "<br />Detail error: " + exc.ToString() };
                    break;
                }
            }

            ViewBag.Counter = startIndex + counter;

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<ZipCodeViewModel> GetPostals()
        {
            var codes = (from code in DB.ZipCodes
                         select new ZipCodeViewModel()
                                {
                                    Id = code.Id,
                                    BuildingName = code.BuildingName,
                                    BuildingType = code.BuildingType,
                                    Latitude = code.Latitude,
                                    Longitude = code.Longitude,
                                    PostalCode = code.PostalCode,
                                    UnitNumber = code.UnitNumber,
                                    StreetName = code.StreetName,
                                    CreatedOn = code.CreatedOn,
                                    UpdatedOn = code.UpdatedOn
                                });

            return codes;
        }

        #endregion
    }
}
