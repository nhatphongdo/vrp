﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class PricingController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "PricingDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowAddOnPrices)]
        public ActionResult List()
        {
            return View(GetPricing());
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowAddOnPrices)]
        [HttpPost]
        public ActionResult UpdatePrice(int serviceId, int productTypeId, int sizeId, decimal cost)
        {
            var price = db.Pricings.FirstOrDefault(x => x.ServiceId == serviceId && x.ProductTypeId == productTypeId && x.SizeId == sizeId);
            if (price == null)
            {
                price = new Pricing()
                        {
                            ServiceId = serviceId,
                            ProductTypeId = productTypeId,
                            SizeId = sizeId,
                            CreatedOn = DateTime.Now,
                            CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : ""
                        };
                db.Pricings.Add(price);
            }
            price.ChargedCost = cost;
            price.UpdatedOn = DateTime.Now;
            price.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
            try
            {
                db.SaveChanges();
                return Json(
                            new
                            {
                                Error = false
                            });
            }
            catch (Exception exc)
            {
                LogHelper.Log(exc);
                return Json(
                            new
                            {
                                Error = true,
                                ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve."
                            });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        private static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }

        private List<Pricing> GetPricing()
        {
            var prices = db.Pricings.ToList();
            ViewBag.Services = db.Services.Where(x => !x.IsDeleted).ToList();
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).ToList();
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted).ToList();

            return prices;
        }

        #endregion
    }
}
