﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class SizesController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "SizesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowSizes)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsShowSizes)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddSize)]
        public ActionResult Add()
        {
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
            {
                Text = x.ProductTypeName,
                Value = x.Id.ToString()
            }).ToList();
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsAddSize)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditSizeViewModel model)
        {
            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
            {
                Text = x.ProductTypeName,
                Value = x.Id.ToString()
            }).ToList();
            if (ModelState.IsValid)
            {
                var size = new Size()
                {
                    SizeName = model.Name,
                    Description = HttpUtility.HtmlDecode(model.Description),
                    ProductTypeId = model.ProductTypeId,
                    FromWeight = model.FromWeight,
                    ToWeight = model.ToWeight,
                    FromLength = model.FromLength,
                    ToLength = model.ToLength,
                    IsDeleted = false,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };

                db.Sizes.Add(size);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Sizes");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditSize)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
            {
                Text = x.ProductTypeName,
                Value = x.Id.ToString()
            }).ToList();
            var model = (from size in db.Sizes
                         where !size.IsDeleted && size.Id == id
                         select new AddEditSizeViewModel()
                         {
                             Name = size.SizeName,
                             Description = size.Description,
                             ProductTypeId = size.ProductTypeId,
                             FromWeight = size.FromWeight,
                             ToWeight = size.ToWeight,
                             FromLength = size.FromLength,
                             ToLength = size.ToLength
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsEditSize)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditSizeViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ProductTypes = db.ProductTypes.Where(x => !x.IsDeleted).Select(x => new SelectListItem()
            {
                Text = x.ProductTypeName,
                Value = x.Id.ToString()
            }).ToList();
            if (ModelState.IsValid)
            {
                var size = db.Sizes.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (size == null)
                {
                    return HttpNotFound();
                }

                size.SizeName = model.Name;
                size.Description = HttpUtility.HtmlDecode(model.Description);
                size.ProductTypeId = model.ProductTypeId;
                size.FromWeight = model.FromWeight;
                size.ToWeight = model.ToWeight;
                size.FromLength = model.FromLength;
                size.ToLength = model.ToLength;
                size.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                size.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Sizes");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.ProductsDeleteSize)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var size = db.Sizes.FirstOrDefault(x => x.Id == id);
                if (size != null)
                {
                    size.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Data queries

        public static IQueryable<SizeViewModel> GetSizes()
        {
            var sizes = (from size in DB.Sizes
                         where !size.IsDeleted
                         select new SizeViewModel()
                         {
                             Id = size.Id,
                             Name = size.SizeName,
                             ProductTypeId = size.ProductTypeId,
                             Description = size.Description,
                             FromWeight = size.FromWeight,
                             ToWeight = size.ToWeight,
                             FromLength = size.FromLength,
                             ToLength = size.ToLength,
                             CreatedOn = size.CreatedOn,
                             UpdatedOn = size.UpdatedOn
                         });

            return sizes;
        }

        #endregion
    }
}