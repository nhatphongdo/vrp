﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Courier_Management_System.Helpers;
using Courier_Management_System.Models;

namespace Courier_Management_System.Areas.Admin.Controllers
{
    [Authorize]
    public class ChargesController : Controller
    {
        #region Fields

        private CourierDBEntities db = new CourierDBEntities();

        const string DatabaseDataContextKey = "ChargesDatabaseDataContext";

        public static CourierDBEntities DB
        {
            get
            {
                if (System.Web.HttpContext.Current.Items[DatabaseDataContextKey] == null)
                {
                    System.Web.HttpContext.Current.Items[DatabaseDataContextKey] = new CourierDBEntities();
                }
                return (CourierDBEntities) System.Web.HttpContext.Current.Items[DatabaseDataContextKey];
            }
        }

        #endregion

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsShowCharges)]
        public ActionResult List()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsShowCharges)]
        public ActionResult ListViewPartial()
        {
            return PartialView("_ListViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsAddCharge)]
        public ActionResult Add()
        {
            ViewBag.Services = db.Services.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.ServiceName,
                    Value = x.Id.ToString()
                })
                .ToList();
            ViewBag.Services.Insert(0, new SelectListItem()
            {
                Text = "No Service is selected",
                Value = ""
            });
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.SizeName,
                    Value = x.Id.ToString()
                })
                .ToList();
            ViewBag.Sizes.Insert(0, new SelectListItem()
            {
                Text = "No Size is selected",
                Value = ""
            });
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsAddCharge)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddEditChargeViewModel model)
        {
            ViewBag.Services = db.Services.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.ServiceName,
                    Value = x.Id.ToString()
                }).ToList();
            ViewBag.Services.Insert(0, new SelectListItem()
            {
                Text = "No Service is selected",
                Value = ""
            });
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.SizeName,
                    Value = x.Id.ToString()
                })
                .ToList();
            ViewBag.Sizes.Insert(0, new SelectListItem()
            {
                Text = "No Size is selected",
                Value = ""
            });

            if (ModelState.IsValid)
            {
                var charge = new Charge()
                {
                    ChargeCode = model.ChargeCode,
                    Description = HttpUtility.HtmlDecode(model.Description),
                    ChargeAmount = model.IsChargeInPercentage ? model.ChargeAmount / 100 : model.ChargeAmount,
                    IsChargeInPercentage = model.IsChargeInPercentage,
                    ValidFrom = model.ValidFrom,
                    ValidTo = model.ValidTo,
                    ServiceId = model.ServiceId,
                    SizeId = model.SizeId,
                    IsDeleted = false,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };

                db.Charges.Add(charge);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Charges");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsEditCharge)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.Services = db.Services.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.ServiceName,
                    Value = x.Id.ToString()
                }).ToList();
            ViewBag.Services.Insert(0, new SelectListItem()
            {
                Text = "No Service is selected",
                Value = ""
            });
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.SizeName,
                    Value = x.Id.ToString()
                })
                .ToList();
            ViewBag.Sizes.Insert(0, new SelectListItem()
            {
                Text = "No Size is selected",
                Value = ""
            });

            var model = (from charge in db.Charges
                         where !charge.IsDeleted && charge.Id == id
                         select new AddEditChargeViewModel()
                         {
                             ChargeCode = charge.ChargeCode,
                             Description = charge.Description,
                             ChargeAmount = charge.IsChargeInPercentage ? charge.ChargeAmount * 100 : charge.ChargeAmount,
                             IsChargeInPercentage = charge.IsChargeInPercentage,
                             ValidFrom = charge.ValidFrom,
                             ValidTo = charge.ValidTo,
                             ServiceId = charge.ServiceId,
                             SizeId = charge.SizeId
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsEditCharge)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, AddEditChargeViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.Services = db.Services.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.ServiceName,
                    Value = x.Id.ToString()
                }).ToList();
            ViewBag.Services.Insert(0, new SelectListItem()
            {
                Text = "No Service is selected",
                Value = ""
            });
            ViewBag.Sizes = db.Sizes.Where(x => !x.IsDeleted)
                .Select(x => new SelectListItem()
                {
                    Text = x.SizeName,
                    Value = x.Id.ToString()
                })
                .ToList();
            ViewBag.Sizes.Insert(0, new SelectListItem()
            {
                Text = "No Size is selected",
                Value = ""
            });

            if (ModelState.IsValid)
            {
                var charge = db.Charges.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (charge == null)
                {
                    return HttpNotFound();
                }

                charge.Description = HttpUtility.HtmlDecode(model.Description);
                charge.ChargeAmount = model.IsChargeInPercentage ? model.ChargeAmount / 100 : model.ChargeAmount;
                charge.IsChargeInPercentage = model.IsChargeInPercentage;
                charge.ValidFrom = model.ValidFrom;
                charge.ValidTo = model.ValidTo;
                charge.ServiceId = model.ServiceId;
                charge.SizeId = model.SizeId;
                charge.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                charge.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("List", "Charges");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsDeleteCharge)]
        [HttpPost]
        public ActionResult ListViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var charge = db.Charges.FirstOrDefault(x => x.Id == id);
                if (charge != null)
                {
                    charge.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_ListViewPartial");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsShowCodOptions)]
        public ActionResult CodOptions()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsShowCodOptions)]
        public ActionResult CodOptionsViewPartial()
        {
            return PartialView("_CodOptionsViewPartial");
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsAddCodOption)]
        public ActionResult AddCodOption()
        {
            return View();
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsAddCodOption)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddCodOption(AddEditCodOptionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var codOption = new CodOption()
                {
                    Description = HttpUtility.HtmlDecode(model.Description),
                    Type = model.Type,
                    ChargeAmount = model.IsChargeInPercentage ? model.ChargeAmount / 100 : model.ChargeAmount,
                    IsChargeInPercentage = model.IsChargeInPercentage,
                    IsDeleted = false,
                    CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    CreatedOn = DateTime.Now,
                    UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                    UpdatedOn = DateTime.Now
                };

                db.CodOptions.Add(codOption);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("CodOptions", "Charges");
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsEditCodOption)]
        public ActionResult EditCodOption(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var model = (from codOption in db.CodOptions
                         where !codOption.IsDeleted && codOption.Id == id
                         select new AddEditCodOptionViewModel()
                         {
                             Description = codOption.Description,
                             Type = codOption.Type,
                             ChargeAmount = codOption.IsChargeInPercentage ? codOption.ChargeAmount * 100 : codOption.ChargeAmount,
                             IsChargeInPercentage = codOption.IsChargeInPercentage,
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsEditCodOption)]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCodOption(int? id, AddEditCodOptionViewModel model)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var codOption = db.CodOptions.FirstOrDefault(x => !x.IsDeleted && x.Id == id);
                if (codOption == null)
                {
                    return HttpNotFound();
                }

                codOption.Description = HttpUtility.HtmlDecode(model.Description);
                codOption.Type = model.Type;
                codOption.ChargeAmount = model.IsChargeInPercentage ? model.ChargeAmount / 100 : model.ChargeAmount;
                codOption.IsChargeInPercentage = model.IsChargeInPercentage;
                codOption.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                codOption.UpdatedOn = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("CodOptions", "Charges");
                }
                catch (Exception exc)
                {
                    // Rollback
                    ViewBag.ErrorMessages = new[] { "Errors occur in processing request. Please contact to administrator to resolve." };
                    LogHelper.Log(exc);
                }
            }
            else
            {
                ViewBag.ErrorMessages = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return View(model);
        }

        [CdsAuthorize(Roles = AccountHelper.AdministratorRole + "," + AccountHelper.PromotionsDeleteCodOption)]
        [HttpPost]
        public ActionResult CodOptionsViewPartialDelete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            try
            {
                var codOption = db.CodOptions.FirstOrDefault(x => x.Id == id);
                if (codOption != null)
                {
                    codOption.IsDeleted = true;
                }

                db.SaveChanges();
            }
            catch (Exception exc)
            {
                ViewBag.ErrorMessage = "Errors occur in processing request. Please contact to administrator to resolve.";
                LogHelper.Log(exc);
            }

            return PartialView("_CodOptionsViewPartial");
        }

        #region Data queries

        public static IQueryable<ChargeViewModel> GetCharges()
        {
            var charges = (from charge in DB.Charges
                           where !charge.IsDeleted
                           select new ChargeViewModel()
                           {
                               Id = charge.Id,
                               ChargeAmount = charge.ChargeAmount,
                               ChargeCode = charge.ChargeCode,
                               Description = charge.Description,
                               IsChargeInPercentage = charge.IsChargeInPercentage,
                               ValidFrom = charge.ValidFrom,
                               ValidTo = charge.ValidTo,
                               ServiceId = charge.ServiceId,
                               SizeId = charge.SizeId,
                               CreatedOn = charge.CreatedOn,
                               UpdatedOn = charge.UpdatedOn
                           });

            return charges;
        }

        public static IQueryable<CodOption> GetCodOptions()
        {
            var codOptions = from codOption in DB.CodOptions
                             where !codOption.IsDeleted
                             select codOption;

            return codOptions;
        }

        #endregion
    }
}