﻿using System.Web.Mvc;

namespace Courier_Management_System.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new
                {
                    controller = "Home",
                    action = "Login",
                    id = UrlParameter.Optional,
                    namespaces = new[] { "Courier_Management_System.Areas.Admin.Controllers" }
                }
            );
        }
    }
}