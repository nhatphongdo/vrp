﻿using System.Web;
using System.Web.Optimization;

namespace Courier_Management_System
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/scripts/jquery-migrate.js",
                        "~/scripts/script.js",
                        "~/scripts/custom.js",
                        "~/scripts/jquery.timepicker.min.js",
                        "~/scripts/jquery-ui.js",
                        "~/scripts/lib/bootstrap-datepicker.js",
                        "~/scripts/lib/pikaday.js",
                        "~/scripts/lib/jquery.ptTimeSelect.js",
                        "~/scripts/lib/moment.min.js",
                        "~/scripts/lib/site.js",
                        "~/scripts/lib/jquery.datepair.js",
                        "~/Content/Admin/js/plugins/iCheck/icheck.min.js",
                        "~/scripts/select/js/dropdownlist.js",
                        "~/Scripts/jquery.bootstrap-touchspin.min.js",
                        "~/Scripts/bootstrap-timepicker.min.js",
                        "~/Scripts/Menu/prefix-free.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/styles/css").Include(
                      "~/scripts/lib/bootstrap-datepicker.css",
                      "~/scripts/lib/jquery.ptTimeSelect.css",
                      "~/Content/Admin/js/plugins/iCheck/square/blue.css",
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-nav-wizard.css",
                      "~/Content/jquery.bootstrap-touchspin.min.css",
                      "~/Content/bootstrap-timepicker.min.css",
                      "~/Content/fonts.css",
                      "~/Content/Skin.css",
                      "~/Content/form/style.css",
                      "~/Content/Menu/style.css").Include("~/Content/Menu/iconic.css", new CssRewriteUrlTransform())) ;


            bundles.Add(new ScriptBundle("~/bundles/jqueryadmin").Include(
                      "~/Scripts/jquery-{version}.js",
                      "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                      "~/Content/Admin/js/plugins/jQueryUI/jquery-ui.min.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/raphael-min.js",
                      "~/Content/Admin/js/plugins/chartjs/chart.min.js",
                      "~/Content/Admin/js/plugins/morris/morris.min.js",
                      "~/Content/Admin/js/plugins/sparkline/jquery.sparkline.min.js",
                      "~/Content/Admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                      "~/Content/Admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                      "~/Content/Admin/js/plugins/knob/jquery.knob.js",
                      "~/Scripts/moment.min.js",
                      "~/Content/Admin/js/plugins/daterangepicker/daterangepicker.js",
                      "~/Content/Admin/js/plugins/datepicker/bootstrap-datepicker.js",
                      "~/Content/Admin/js/plugins/timepicker/bootstrap-timepicker.min.js",
                      "~/Content/Admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                      "~/Content/Admin/js/plugins/slimScroll/jquery.slimscroll.min.js",
                      "~/Content/Admin/js/plugins/fastclick/fastclick.js",
                      "~/Content/Admin/js/plugins/iCheck/icheck.min.js",
                      "~/Content/Admin/js/plugins/select2/select2.full.min.js",
                      "~/Content/Admin/js/plugins/bootstrap-slider/bootstrap-slider.min.js",
                      "~/Scripts/sweetalert.min.js",
                      "~/Scripts/jquery.bootstrap-touchspin.min.js",
                      "~/Content/Admin/js/app.js"));

            bundles.Add(new StyleBundle("~/styles/admin").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Admin/css/AdminLTE.min.css",
                      "~/Content/Admin/css/skins/skin-red.min.css",
                      "~/Content/Admin/js/plugins/iCheck/square/blue.css",
                      "~/Content/Admin/js/plugins/morris/morris.css",
                      "~/Content/Admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
                      "~/Content/Admin/js/plugins/datepicker/datepicker3.css",
                      "~/Content/Admin/js/plugins/timepicker/bootstrap-timepicker.min.css",
                      "~/Content/Admin/js/plugins/daterangepicker/daterangepicker-bs3.css",
                      "~/Content/Admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",
                      "~/Content/Admin/js/plugins/select2/select2.min.css",
                      "~/Content/Admin/js/plugins/bootstrap-slider/bootstrap-slider.min.css",
                      "~/Content/sweetalert.css",
                      "~/Content/jquery.bootstrap-touchspin.min.css"));
        }
    }
}
