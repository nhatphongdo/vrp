﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courier_Management_System.Models
{
    public class HistoryPayment
    {
        public int? AccountType { get; set; }

        public DateTime CreatedOn { get; set; }

        public PaymentAccount PaymentAccount { get; set; }

        public decimal PaymentAmount { get; set; }

        public long Id { get; set; }

        public List<Order> Orders { get; set; }

        public string PayerUserName { get; set; }

        public Customer Payer { get; set; }

        public string CNI { get; set; }

        public bool HasOrder { get; set; }
    }
}
