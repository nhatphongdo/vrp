﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Courier_Management_System.Helpers;
using FileHelpers;

namespace Courier_Management_System.Models
{
    public class AddEditOrderViewModel
    {
        public string ConsignmentNumber { get; set; }

        [Display(Name = "Customer")]
        public long? CustomerId { get; set; }

        public Customer Customer { get; set; }

        [Required(ErrorMessage = "The Sender's Name is required")]
        [Display(Name = "Pickup From Who?")]
        public string FromName { get; set; }

        [Required]
        [Display(Name = "Sender's Zip Code")]
        public string FromZipCode { get; set; }

        [Required(ErrorMessage = "The Sender's Address is required")]
        [Display(Name = "Pickup From Where?")]
        public string FromAddress { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Sender's Mobile phone")]
        public string FromMobilePhone { get; set; }

        [Required(ErrorMessage = "The Receiver's Name is required")]
        [Display(Name = "Deliver To Who?")]
        public string ToName { get; set; }

        [Required]
        [Display(Name = "Receiver's Zip Code")]
        public string ToZipCode { get; set; }

        [Required(ErrorMessage = "The Receiver's Address is required")]
        [Display(Name = "Deliver To Where?")]
        public string ToAddress { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Receiver's Mobile phone")]
        public string ToMobilePhone { get; set; }

        [Required]
        [Display(Name = "Payment Type")]
        public int PaymentTypeId { get; set; }

        [Required]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Service")]
        public int ServiceId { get; set; }

        [Required]
        [Display(Name = "Product Type")]
        public int ProductTypeId { get; set; }

        [Required]
        [Display(Name = "Size")]
        public int SizeId { get; set; }

        [Display(Name = "When do you want to pickup (time)?")]
        public int? PickupTimeSlotId { get; set; }

        [Display(Name = "When do you want to pickup (date)?")]
        public DateTime? PickupDate { get; set; }

        [Display(Name = "When do you want to deliver (time)?")]
        public int? DeliveryTimeSlotId { get; set; }

        [Display(Name = "When do you want to deliver (date)?")]
        public DateTime? DeliveryDate { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Required]
        [Display(Name = "Status")]
        public OrderStatuses Status { get; set; }

        [Display(Name = "Reject Note")]
        public string RejectNote { get; set; }

        [Display(Name = "Promo Code")]
        public string PromoCode { get; set; }

        [Display(Name = "Ops Remark")]
        public string OperatorRemark { get; set; }

        [Display(Name = "Pickup Request")]
        public string PickupRequest { get; set; }

        [Display(Name = "Delivery Request")]
        public string DeliveryRequest { get; set; }

        [Display(Name = "COD Options")]
        public List<CodOptionModel> CodOptions { get; set; }

        [Display(Name = "Is Exchange")]
        public bool IsExchange { get; set; }
    }

    public class OrderViewModel
    {
        public long Id { get; set; }

        public string ConsignmentNumber { get; set; }

        public string BinCode { get; set; }

        public long? CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerCompany { get; set; }

        public string FromName { get; set; }

        public string FromZipCode { get; set; }

        public string FromDistrictCode { get; set; }

        public string FromZoneCode { get; set; }

        public string FromAddress { get; set; }

        public string FromMobilePhone { get; set; }

        public string FromSignature { get; set; }

        public string ToName { get; set; }

        public string ToZipCode { get; set; }

        public string ToDistrictCode { get; set; }

        public string ToZoneCode { get; set; }

        public string ToAddress { get; set; }

        public string ToMobilePhone { get; set; }

        public string ToSignature { get; set; }

        public int PaymentTypeId { get; set; }

        public decimal Price { get; set; }

        public int ServiceId { get; set; }

        public int ProductTypeId { get; set; }

        public int SizeId { get; set; }

        public int? PickupTimeSlotId { get; set; }

        public DateTime? PickupDate { get; set; }

        public int? DeliveryTimeSlotId { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string Remark { get; set; }

        public OrderStatuses Status { get; set; }

        public string QrCodePath { get; set; }

        public string PromoCode { get; set; }

        public DateTime? ConfirmedOn { get; set; }

        public string ConfirmedBy { get; set; }

        public string ConfirmedContact { get; set; }

        public string RejectNote { get; set; }

        public DateTime? CollectedOn { get; set; }

        public string CollectedBy { get; set; }

        public string CollectedContact { get; set; }

        public DateTime? ProceededOn { get; set; }

        public string ProceededBy { get; set; }

        public string ProceededContact { get; set; }

        public DateTime? ProceededOutOn { get; set; }

        public string ProceededOutContact { get; set; }

        public string ProceededOutBy { get; set; }

        public DateTime? DeliveredOn { get; set; }

        public string DeliveredBy { get; set; }

        public string DeliveredContact { get; set; }

        public DateTime? CanceledOn { get; set; }

        public string CanceledContact { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string FullfilmentNumber { get; set; }

        public string GroupTrackingNumber { get; set; }

        public string OperatorRemark { get; set; }

        public string RequestRemark { get; set; }

        public DateTime? RequestDate { get; set; }

        public bool IsExchange { get; set; }

        public string[] CodCollectedOptions { get; set; }

        public decimal? TotalCodAmount { get; set; }

        public decimal? TotalCodCollectedAmount { get; set; }

        public string OrderNumber { get; set; }

        public string DriverOrOPSName { get; set; }

        public string DriverOrOPSMobile { get; set; }

        public DateTime? DriverOrOPSTimestamp { get; set; }

        public string BeingPickedBy { get; set; }

        public DateTime? BeingPickedOn { get; set; }

        public string BeingPickedContact { get; set; }

        public string SpecialPickupBy { get; set; }

        public string SpecialPickupContact { get; set; }

        public DateTime? SpecialPickupOn { get; set; }

        public string SpecialDeliveryBy { get; set; }

        public string SpecialDeliveryContact { get; set; }

        public DateTime? SpecialDeliveryOn { get; set; }

        public string RePickupBy { get; set; }

        public DateTime? RePickupOn { get; set; }

        public string RePickupContact { get; set; }

        public string ReDeliveryBy { get; set; }

        public string ReDeliveryContact { get; set; }

        public DateTime? ReDeliveryOn { get; set; }

        public string ReturnToSenderBy { get; set; }

        public string ReturnToSenderContact { get; set; }

        public DateTime? ReturnToSenderOn { get; set; }

        public List<CodOptionModel> CODOption { get; set; }
    }

    public class ExportOrderViewModel
    {
        public long Id { get; set; }

        public string ConsignmentNumber { get; set; }

        public string BinCode { get; set; }

        public long? CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerCompany { get; set; }

        public string FromName { get; set; }

        public string FromZipCode { get; set; }

        public string FromDistrictCode { get; set; }

        public string FromZoneCode { get; set; }

        public string FromAddress { get; set; }

        public string FromMobilePhone { get; set; }

        public string FromSignature { get; set; }

        public string ToName { get; set; }

        public string ToZipCode { get; set; }

        public string ToDistrictCode { get; set; }

        public string ToZoneCode { get; set; }

        public string ToAddress { get; set; }

        public string ToMobilePhone { get; set; }

        public string ToSignature { get; set; }

        public int PaymentTypeId { get; set; }

        public decimal Price { get; set; }

        public int ServiceId { get; set; }

        public int ProductTypeId { get; set; }

        public int SizeId { get; set; }

        public int? PickupTimeSlotId { get; set; }

        public DateTime? PickupDate { get; set; }

        public int? DeliveryTimeSlotId { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string Remark { get; set; }

        public OrderStatuses Status { get; set; }

        public string QrCodePath { get; set; }

        public string PromoCode { get; set; }

        public DateTime? ConfirmedOn { get; set; }

        public string ConfirmedBy { get; set; }

        public string RejectNote { get; set; }

        public DateTime? CollectedOn { get; set; }

        public string CollectedBy { get; set; }

        public DateTime? ProceededOn { get; set; }

        public string ProceededBy { get; set; }

        public DateTime? ProceededOutOn { get; set; }

        public string ProceededOutBy { get; set; }

        public DateTime? DeliveredOn { get; set; }

        public string DeliveredBy { get; set; }

        public DateTime? CanceledOn { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string FullfilmentNumber { get; set; }

        public string GroupTrackingNumber { get; set; }

        public string OperatorRemark { get; set; }

        public string RequestRemark { get; set; }

        public DateTime? RequestDate { get; set; }

        public bool IsExchange { get; set; }

        public string Cod { get; set; }

        public string OrderNumber { get; set; }

        public string CodCollected { get; set; }

        public decimal? TotalCodAmount { get; set; }

        public decimal? TotalCodCollectedAmount { get; set; }
    }

    public class SizeRequestViewModel
    {
        public int ProductTypeId { get; set; }
    }

    public class ServiceRequestViewModel
    {
        public int ProductTypeId { get; set; }

        public int SizeId { get; set; }
    }

    public class PriceRequestViewModel
    {
        public int ProductTypeId { get; set; }

        public int ServiceId { get; set; }

        public int SizeId { get; set; }

        public long? CustomerId { get; set; }

        /// <summary>
        /// Code for promotion or sur-charge
        /// </summary>
        public string AddOnCode { get; set; }

        public string Cod { get; set; }

        public string CodAmounts { get; set; }

        public bool IsExchange { get; set; }
    }

    public class OrderArriveConfirmViewModel
    {
        public long JobId { get; set; }

        public string ConsignmentNumber { get; set; }

        public bool IsPickup { get; set; }

        public bool IsSuccessful { get; set; }

        public string ProofPath { get; set; }

        public int Rating { get; set; }
    }

    public class BookOrderViewModel
    {
        [Required(ErrorMessage = "The Sender's Name is required")]
        [Display(Name = "Pickup From Who?")]
        public string FromName { get; set; }

        [Required]
        [Display(Name = "Sender's Zip Code")]
        public string FromZipCode { get; set; }

        [Required(ErrorMessage = "The Sender's Address is required")]
        [Display(Name = "Pickup From Where?")]
        public string FromAddress { get; set; }

        [Display(Name = "Unit Number")]
        public string FromUnitNumber { get; set; }

        [Display(Name = "Building Name")]
        public string FromBuildingName { get; set; }

        [Required]
        [RegularExpression(@"[3689][0-9]{7}", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Sender's Mobile phone")]
        public string FromMobilePhone { get; set; }

        [Required(ErrorMessage = "The Receiver's Name is required")]
        [Display(Name = "Deliver To Who?")]
        public string ToName { get; set; }

        [Required]
        [Display(Name = "Receiver's Zip Code")]
        public string ToZipCode { get; set; }

        [Required(ErrorMessage = "The Receiver's Address is required")]
        [Display(Name = "Deliver To Where?")]
        public string ToAddress { get; set; }

        [Display(Name = "Unit Number")]
        public string ToUnitNumber { get; set; }

        [Display(Name = "Building Name")]
        public string ToBuildingName { get; set; }

        [Required]
        [RegularExpression(@"[3689][0-9]{7}", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Receiver's Mobile phone")]
        public string ToMobilePhone { get; set; }

        [Required]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Service")]
        public int ServiceId { get; set; }

        [Required]
        [Display(Name = "Product Type")]
        public int ProductTypeId { get; set; }

        [Required]
        [Display(Name = "Size")]
        public int SizeId { get; set; }

        [Display(Name = "When do you want to pickup (time)?")]
        public int? PickupTimeSlotId { get; set; }

        [Display(Name = "When do you want to pickup (date)?")]
        public DateTime? PickupDate { get; set; }

        [Display(Name = "When do you want to deliver (time)?")]
        public int? DeliveryTimeSlotId { get; set; }

        [Display(Name = "When do you want to deliver (date)?")]
        public DateTime? DeliveryDate { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Display(Name = "Promo Code")]
        public string PromoCode { get; set; }

        public long? CustomerId { get; set; }

        public int PaymentTypeId { get; set; }

        public long PaymentAccountId { get; set; }
    }

    public class VrpViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string PostalCode { get; set; }

        public string Address { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string MustBeVisited { get; set; }

        public string ProductType { get; set; }

        public double PickupAmount { get; set; }

        public double DeliveryAmount { get; set; }

        public decimal Profit { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public DateTime ServiceTime { get; set; }
    }

    public class ApiBookOrderViewModel
    {
        [Required(ErrorMessage = "The Sender's Name is required")]
        [Display(Name = "Pickup From Who?")]
        public string FromName { get; set; }

        [Required]
        [Display(Name = "Sender's Zip Code")]
        public string FromZipCode { get; set; }

        [Required(ErrorMessage = "The Sender's Address is required")]
        [Display(Name = "Pickup From Where?")]
        public string FromAddress { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Sender's Mobile phone")]
        public string FromMobilePhone { get; set; }

        [Required(ErrorMessage = "The Receiver's Name is required")]
        [Display(Name = "Deliver To Who?")]
        public string ToName { get; set; }

        [Required]
        [Display(Name = "Receiver's Zip Code")]
        public string ToZipCode { get; set; }

        [Required(ErrorMessage = "The Receiver's Address is required")]
        [Display(Name = "Deliver To Where?")]
        public string ToAddress { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Receiver's Mobile phone")]
        public string ToMobilePhone { get; set; }

        [Required]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Service")]
        public int ServiceId { get; set; }

        [Required]
        [Display(Name = "Product Type")]
        public int ProductTypeId { get; set; }

        [Required]
        [Display(Name = "Size")]
        public int SizeId { get; set; }

        [Display(Name = "When do you want to pickup (time)?")]
        public int? PickupTimeSlotId { get; set; }

        [Display(Name = "When do you want to pickup (date)?")]
        public string PickupDate { get; set; }

        [Display(Name = "When do you want to deliver (time)?")]
        public int? DeliveryTimeSlotId { get; set; }

        [Display(Name = "When do you want to deliver (date)?")]
        public string DeliveryDate { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Display(Name = "Promo Code")]
        public string PromoCode { get; set; }

        public long? CustomerId { get; set; }

        public int PaymentTypeId { get; set; }

        public long PaymentAccountId { get; set; }

        public string OrderNumber { get; set; }
    }

    public class CheckoutViewModel
    {
        public string OrderId { get; set; }

        public string PaymentMethodNonce { get; set; }
    }

    [Serializable]
    public class CodOptionModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Type { get; set; }

        public decimal Amount { get; set; }

        public decimal CollectedAmount { get; set; }

        public bool IsCollected { get; set; }
    }

    public class OrdersRemarkViewModel
    {
        public string ConsignmentNumbers { get; set; }
    }

    /*
     * Enhancement 8 SEP 2016
     * 
     */

    public class BulkUpdateViewModel
    {
        [Display(Name = "Scanned Consignments")]
        [Required]
        public string ConsignmentNumbers { get; set; }

        [Required]
        public int? UpdateField { get; set; }
	public int UpdateUsingField { get; set; }
        public int? TotalRecordsAffected { get; set; }

        public string FromName { get; set; }

        public string FromZipCode { get; set; }

        public string FromAddress { get; set; }

        public string FromMobilePhone { get; set; }

        public string ToName { get; set; }

        public string ToZipCode { get; set; }

        public string ToAddress { get; set; }

        public string ToMobilePhone { get; set; }

        public OrderStatuses Status { get; set; }

        [Display(Name = "Assign Driver")]
        public string DriverId { get; set; }

        public DateTime? PickupDate { get; set; }

        public int? PickupTimeSlotId { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public int? DeliveryTimeSlotId { get; set; }
	[Display(Name ="Customer Name")]
        public int CustomerName { get; set; }
    }

    /*
         * End Enhancement 8 SEP 2016
         */

    [DelimitedRecord(",")]
    public class OrderCsvViewModel
    {
        public long Id { get; set; }
        public string OrderNumber { get; set; }
        public int Status { get; set; }
        public DateTime? CollectedOn { get; set; }
        public DateTime? ConfirmedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DeliveryOn { get; set; }
        public DateTime? ProceededOn { get; set; }
        public DateTime? ProceededOutOn { get; set; }
        public DateTime? CanceledOn { get; set; }
    }
}
