﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class AddEditZipCodeViewModel
    {
        [Required]
        [Display(Name = "Postal Code")]
        public string PostalCode
        {
            get; set;
        }

        [Display(Name = "Building Name")]
        public string BuildingName
        {
            get; set;
        }

        [Display(Name = "Street Number")]
        public string UnitNumber
        {
            get; set;
        }

        [Display(Name = "Street Name")]
        public string StreetName
        {
            get; set;
        }

        [Display(Name = "Building Type")]
        public string BuildingType
        {
            get; set;
        }

        [Display(Name = "Latitude")]
        public double? Latitude
        {
            get; set;
        }

        [Display(Name = "Longitude")]
        public double? Longitude
        {
            get; set;
        }
        [Display(Name = "DistrictCode")]
        public string DistrictCode
        {
            get; set;
        }

        [Display(Name = "Zone")]
        public string Zone
        {
            get; set;
        }
    }

    public class ZipCodeViewModel
    {
        public int Id
        {
            get; set;
        }

        public string PostalCode
        {
            get; set;
        }

        public string BuildingName
        {
            get; set;
        }

        [Display(Name = "Street Number")]
        public string UnitNumber
        {
            get; set;
        }

        public string StreetName
        {
            get; set;
        }

        public string BuildingType
        {
            get; set;
        }

        public double? Longitude
        {
            get; set;
        }

        public double? Latitude
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }

        
        public string DistrictCode
        {
            get; set;
        }

       
        public string Zone
        {
            get; set;
        }
    }

}
