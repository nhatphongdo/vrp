﻿using System;
using System.ComponentModel.DataAnnotations;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Models
{
    public class AddDriverViewModel
    {
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        [RegularExpression(@"[0-9+]*", ErrorMessage = "The Country Code field is not valid.")]
        [Display(Name = "Country Code")]
        public string CountryCode { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name = "Staff Number")]
        public string IdentityNumber { get; set; }

        [Display(Name = "Photo")]
        public string Photo { get; set; }

        [Display(Name = "Account Type")]
        public AccountTypes AccountType { get; set; }

        [Display(Name = "IC Number")]
        public string ICNumber { get; set; }

        [Required]
        [Display(Name = "Driving License")]
        public string DrivingLicense { get; set; }

        [Display(Name = "Vehicle Number")]
        public string VehicleNumber { get; set; }

        [Display(Name = "Vehicle Type")]
        public string VehicleType { get; set; }

        [Display(Name = "Rating")]
        public int Rating { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Pay Rate Scheme")]
        public int? PayRateScheme { get; set; }
    }

    public class EditDriverViewModel
    {
        public string UserName { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Current Password")]
        public string CurrentPassword { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        [RegularExpression(@"[0-9+]*", ErrorMessage = "The Country Code field is not valid.")]
        [Display(Name = "Country Code")]
        public string CountryCode { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name = "Staff Number")]
        public string IdentityNumber { get; set; }

        [Display(Name = "Photo")]
        public string Photo { get; set; }

        [Display(Name = "Account Type")]
        public AccountTypes AccountType { get; set; }

        [Display(Name = "IC Number")]
        public string ICNumber { get; set; }

        [Required]
        [Display(Name = "Driving License")]
        public string DrivingLicense { get; set; }

        [Display(Name = "Vehicle Number")]
        public string VehicleNumber { get; set; }

        [Display(Name = "Vehicle Type")]
        public string VehicleType { get; set; }

        [Display(Name = "Rating")]
        public int Rating { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Pay Rate Scheme")]
        public int? PayRateScheme { get; set; }
    }

    public class DriverViewModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string CountryCode { get; set; }

        public string MobileNumber { get; set; }

        public string IdentityNumber { get; set; }

        public string Photo { get; set; }

        public AccountTypes AccountType { get; set; }

        public string ICNumber { get; set; }

        public string DrivingLicense { get; set; }

        public string VehicleNumber { get; set; }

        public string VehicleType { get; set; }

        public int Rating { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string NotificationKey { get; set; }

        public bool IsActive { get; set; }
    }

    public class DriverJobHistoryModel
    {
        public long Id { get; set; }

        public long JobId { get; set; }

        public string ConsignmentNumber { get; set; }

        public string Size { get; set; }

        public string BinCode { get; set; }

        public DateTime? AssignedOn { get; set; }

        public DateTime? CompletedOn { get; set; }
        
        public string JobName { get; set; }

        public string JobStatus { get; set; }

        public decimal Payment { get; set; }

        public decimal DeductionAmount { get; set; }

        public int Rating { get; set; }
    }

    public class PayslipModel
    {
        public DateTime? Date { get; set; }

        public long JobId { get; set; }

        public decimal Amount { get; set; }

        public decimal Deduction { get; set; }
    }
}