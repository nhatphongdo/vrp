﻿using System;
using System.ComponentModel.DataAnnotations;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Models
{
    public class AddEditPricePlanViewModel
    {
        [Required]
        [Display(Name = "Customer Plan Name")]
        public string PlanName
        {
            get; set;
        }

        //[Display(Name = "Rebate Amount")]
        //public double RebateAmount
        //{
        //    get; set;
        //}

        //[Display(Name = "Comparison Type")]
        //public ComparisonTypes Comparison
        //{
        //    get; set;
        //}

        //[Display(Name = "Limit Value")]
        //public decimal LimitValue
        //{
        //    get; set;
        //}

        //[Display(Name = "Unit Type")]
        //public UnitTypes UnitType
        //{
        //    get; set;
        //}

        //[Display(Name = "Period Type")]
        //public PeriodTypes PeriodType
        //{
        //    get; set;
        //}

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Default Promo Code")]
        public string[] DefaultPromoCode
        {
            get; set;
        }
    }

    public class PricePlanViewModel
    {
        public int Id
        {
            get; set;
        }

        public string PlanName
        {
            get; set;
        }

        public double RebateAmount
        {
            get; set;
        }

        public ComparisonTypes Comparison
        {
            get; set;
        }

        public decimal LimitValue
        {
            get; set;
        }

        public UnitTypes UnitType
        {
            get; set;
        }

        public PeriodTypes PeriodType
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }

        public string DefaultPromoCode
        {
            get; set;
        }
    }
}
