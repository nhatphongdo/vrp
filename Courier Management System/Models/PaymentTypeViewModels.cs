﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courier_Management_System.Models
{
    public class AddEditPaymentTypeViewModel
    {
        [Required]
        [Display(Name = "Payment Type Name")]
        public string Name
        {
            get; set;
        }

        [Display(Name = "Description")]
        public string Description
        {
            get; set;
        }
    }

    public class PaymentTypeViewModel
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Description
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }
    }
}
