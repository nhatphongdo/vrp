﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CsvHelper.Configuration;

namespace Courier_Management_System.Models
{
    public class MaxcellentsModel
    {
        public string Tracking { get; set; }

        public string ServiceType { get; set; }

        public string CodService { get; set; }

        public string CodAmount { get; set; }

        public string ExchangeService { get; set; }

        public string PickupService { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string Contact { get; set; }

        public string Email { get; set; }

        public string ItemValue { get; set; }

        public string Remarks { get; set; }

        public string Location { get; set; }

        public string SKU { get; set; }

        public string Quantity { get; set; }
    }

    public sealed class MaxcellentsModelMap : CsvClassMap<MaxcellentsModel>
    {
        public MaxcellentsModelMap()
        {
            Map(m => m.Tracking).Index(0);
            Map(m => m.CodService).Index(2);
            Map(m => m.CodAmount).Index(3);
            Map(m => m.ExchangeService).Index(4);
            Map(m => m.Name).Index(6);
            Map(m => m.Address).Index(7);
            Map(m => m.PostalCode).Index(8);
            Map(m => m.Contact).Index(9);
            Map(m => m.ItemValue).Index(11);
            Map(m => m.Remarks).Index(12);
            Map(m => m.Location).Index(13);
            Map(m => m.SKU).Index(14);
            Map(m => m.Quantity).Index(15);
        }
    }
}
