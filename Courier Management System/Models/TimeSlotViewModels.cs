﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class AddEditTimeSlotViewModel
    {
        [Required]
        [Display(Name = "Time Slot Name")]
        public string Name
        {
            get; set;
        }

        [Required]
        [Display(Name = "Bin Code")]
        public string BinCode
        {
            get; set;
        }

        [Required]
        [Display(Name = "From Time")]
        public string FromTime
        {
            get; set;
        }

        [Required]
        [Display(Name = "To Time")]
        public string ToTime
        {
            get; set;
        }

        [Required]
        [Display(Name = "Is allow to pre-order")]
        public bool IsAllowPreOrder
        {
            get; set;
        }
    }

    public class TimeSlotViewModel
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string BinCode
        {
            get; set;
        }

        public DateTime FromTime
        {
            get; set;
        }

        public DateTime ToTime
        {
            get; set;
        }

        public bool IsAllowPreOrder
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }
    }
}
