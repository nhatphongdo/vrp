﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class AddEditChargeViewModel
    {
        [Display(Name = "Charge Code")]
        public string ChargeCode { get; set; }

        public string Description { get; set; }

        [Display(Name = "Valid From")]
        public DateTime? ValidFrom { get; set; }

        [Display(Name = "Valid Until")]
        public DateTime? ValidTo { get; set; }

        [Required]
        [Display(Name = "Charge Amount")]
        public decimal ChargeAmount { get; set; }

        [Required]
        [Display(Name = "Is Charge Amount in Percentage?")]
        public bool IsChargeInPercentage { get; set; }

        [Display(Name = "Service")]
        public int? ServiceId { get; set; }

        [Display(Name = "Size")]
        public int? SizeId { get; set; }
    }

    public class ChargeViewModel
    {
        public int Id { get; set; }

        public string ChargeCode { get; set; }

        public string Description { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public decimal ChargeAmount { get; set; }

        public bool IsChargeInPercentage { get; set; }

        public int? ServiceId { get; set; }

        public int? SizeId { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }

    public class AddEditCodOptionViewModel
    {
        [Required]
        public string Description { get; set; }

        [Required]
        public int Type { get; set; }

        [Required]
        [Display(Name = "Charge Amount")]
        public decimal ChargeAmount { get; set; }

        [Required]
        [Display(Name = "Is Charge Amount in Percentage?")]
        public bool IsChargeInPercentage { get; set; }
    }
}