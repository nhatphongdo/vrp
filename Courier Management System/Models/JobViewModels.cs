﻿using System;
using System.Collections.Generic;

namespace Courier_Management_System.Models
{
    public class JobLocationViewModel
    {
        public string OrderId
        {
            get; set;
        }

        public string CustomerName
        {
            get; set;
        }

        public string CustomerAddress
        {
            get; set;
        }

        public string CustomerMobilePhone
        {
            get; set;
        }

        public string CustomerCompany
        {
            get; set;
        }

        public List<string> ConsignmentNumbers
        {
            get; set;
        }

        public int Type
        {
            get; set;
        }

        public bool IsDone
        {
            get; set;
        }

        public string SizeName
        {
            get; set;
        }

        public List<Order> Orders
        {
            get; set;
        }
    }

    public class RouteStepViewModel
    {
        public double Latitude
        {
            get; set;
        }

        public double Longitude
        {
            get; set;
        }
    }

    public class JobRouteViewModel
    {
        public int Stops
        {
            get; set;
        }

        public decimal NetProfit
        {
            get; set;
        }

        public List<int> Steps
        {
            get; set;
        }

        public List<double> Distances
        {
            get; set;
        }

        public List<double> Loads
        {
            get; set;
        }

        public List<decimal> Profits
        {
            get; set;
        }

        public List<double> DrivingTimes
        {
            get; set;
        }

        public List<double> Arrivals
        {
            get; set;
        }

        public List<double> Departures
        {
            get; set;
        }

        public List<double> WorkingTimes
        {
            get; set;
        }

        public string Path
        {
            get; set;
        }

        public List<RouteStepViewModel> FootSteps
        {
            get; set;
        }
    }

    public class JobViewModel
    {
        public List<JobLocationViewModel> Locations
        {
            get; set;
        }

        public JobRouteViewModel Route
        {
            get; set;
        }
    }

    public class JobReportViewModel
    {
        public long Id
        {
            get; set;
        }

        public int VehicleId
        {
            get; set;
        }

        public string VehicleNumber
        {
            get; set;
        }

        public JobViewModel Routes
        {
            get; set;
        }

        public DateTime? TakenOn
        {
            get; set;
        }

        public string TakenBy
        {
            get; set;
        }

        public int TotalPickups
        {
            get; set;
        }

        public int TotalDeliveries
        {
            get; set;
        }

        public double TotalDistances
        {
            get; set;
        }

        public double TotalDrivingTime
        {
            get; set;
        }

        public double TotalWorkingTime
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }
    }

    public class JobHistorySummaryViewModel
    {
        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public bool IsPickup { get; set; }
    }
}
