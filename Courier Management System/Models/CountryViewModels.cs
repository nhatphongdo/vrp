﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class AddEditCountryViewModel
    {
        [Required]
        [Display(Name = "Country Code")]
        public string CountryCode
        {
            get; set;
        }

        [Required]
        [Display(Name = "Country Name")]
        public string Name
        {
            get; set;
        }

        [Display(Name = "Country Image")]
        public string CountryImage
        {
            get; set;
        }
    }

    public class CountryViewModel
    {
        public int Id
        {
            get; set;
        }

        public string CountryCode
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string CountryImage
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }
    }
}
