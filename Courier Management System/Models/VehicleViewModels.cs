﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class AddEditVehicleViewModel
    {
        [Required]
        [Display(Name = "Vehicle Number")]
        public string VehicleNumber
        {
            get; set;
        }

        [Display(Name = "Vehicle Type")]
        public string VehicleType
        {
            get; set;
        }

        [Required]
        [Display(Name = "Capacity")]
        public double Capacity
        {
            get; set;
        }

        [Display(Name = "Mile Age")]
        public double MileAge
        {
            get; set;
        }

        [Display(Name = "Fixed cost per trip")]
        public decimal FixedCostPerTrip
        {
            get; set;
        }

        [Display(Name = "Cost per kilometer")]
        public decimal CostPerKilometer
        {
            get; set;
        }

        [Display(Name = "Distance limit")]
        public double DistanceLimit
        {
            get; set;
        }

        [Display(Name = "Work start time")]
        public DateTime WorkStartTime
        {
            get; set;
        }

        [Display(Name = "Working time limit")]
        public DateTime WorkingTimeLimit
        {
            get; set;
        }

        [Display(Name = "Driving time limit")]
        public DateTime DrivingTimeLimit
        {
            get; set;
        }

        [Display(Name = "Is active")]
        public bool IsActive
        {
            get; set;
        }

        [Display(Name = "Color")]
        public string Color
        {
            get; set;
        }
    }

    public class VehicleViewModel
    {
        public int Id
        {
            get; set;
        }

        public string VehicleNumber
        {
            get; set;
        }

        public string VehicleType
        {
            get; set;
        }

        public double Capacity
        {
            get; set;
        }

        public double MileAge
        {
            get; set;
        }

        public string Color
        {
            get; set;
        }

        public decimal FixedCostPerTrip
        {
            get; set;
        }

        public decimal CostPerKilometer
        {
            get; set;
        }

        public double DistanceLimit
        {
            get; set;
        }

        public DateTime WorkStartTime
        {
            get; set;
        }

        public DateTime WorkingTimeLimit
        {
            get; set;
        }

        public DateTime DrivingTimeLimit
        {
            get; set;
        }

        public bool IsActive
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }
    }
}
