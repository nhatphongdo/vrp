﻿using System;
using System.Collections.Generic;
using System.Linq;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Models
{
    public class DriverReportViewModel
    {
        public string Id
        {
            get; set;
        }

        public string UserName
        {
            get; set;
        }

        public string Email
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string CountryCode
        {
            get; set;
        }

        public string MobileNumber
        {
            get; set;
        }

        public AccountTypes AccountType
        {
            get; set;
        }

        public string ICNumber
        {
            get; set;
        }

        public string DrivingLicense
        {
            get; set;
        }

        public string VehicleNumber
        {
            get; set;
        }

        public string VehicleType
        {
            get; set;
        }

        public DateTime? TakenOn
        {
            get; set;
        }

        public DateTime? CompletedOn
        {
            get; set;
        }

        public int TotalDeliveries
        {
            get; set;
        }

        public int TotalPickups
        {
            get; set;
        }

        public double TotalDistances
        {
            get; set;
        }

        public double TotalDrivingTime
        {
            get; set;
        }

        public double TotalWorkingTime
        {
            get; set;
        }

        public string Routes
        {
            get; set;
        }

        public List<Order> Orders
        {
            get; set;
        }

        public long JobId { get; set; }

        public bool IsSpecialJob { get; set; }

        public int TasksCompleted { get; set; }

        public DetailJobViewModel DetailJob { get; set; }
    }
    public class DetailJobViewModel
    {
        public int TotalOrders { get; set; }
        public int Completed { get; set; }
        public int Failed { get; set; }
        public int InProgress { get; set; }
        public int Others { get; set; }
    }

}
