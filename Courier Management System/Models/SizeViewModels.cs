﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class AddEditSizeViewModel
    {
        [Required]
        [Display(Name = "Size Name")]
        public string Name
        {
            get; set;
        }

        [Display(Name = "Description")]
        public string Description
        {
            get; set;
        }

        [Required]
        [Display(Name = "Product Type")]
        public int ProductTypeId
        {
            get; set;
        }

        [Display(Name = "From Weight")]
        public double FromWeight
        {
            get; set;
        }

        [Display(Name = "To Weight")]
        public double ToWeight
        {
            get; set;
        }

        [Display(Name = "From Length")]
        public double FromLength
        {
            get; set;
        }

        [Display(Name = "To Length")]
        public double ToLength
        {
            get; set;
        }
    }

    public class SizeViewModel
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Description
        {
            get; set;
        }

        public int ProductTypeId
        {
            get; set;
        }

        public double FromWeight
        {
            get; set;
        }

        public double ToWeight
        {
            get; set;
        }

        public double FromLength
        {
            get; set;
        }

        public double ToLength
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }
    }
}
