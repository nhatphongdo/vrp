﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration;

namespace Courier_Management_System.Models
{
    public class Qoo10Model
    {
        public string ShippingStatus { get; set; }

        public string OrderNumber { get; set; }

        public string CartNumber { get; set; }

        public string DeliveryCompany { get; set; }

        public string TrackingNumber { get; set; }

        public string ShippingDate { get; set; }

        public string OrderDate { get; set; }

        public string PaymentComplete { get; set; }

        public string DesiredShippingDate { get; set; }

        public string EstimatedShippingDate { get; set; }

        public string DeliveredDate { get; set; }

        public string ShippingMethod { get; set; }

        public string ItemCode { get; set; }

        public string Item { get; set; }

        public string Quantity { get; set; }

        public string Options { get; set; }

        public string OptionCode { get; set; }

        public string Gift { get; set; }

        public string Recipient { get; set; }

        public string RecipientPhonetic { get; set; }

        public string RecipientPhoneNumber { get; set; }

        public string RecipientMobilePhone { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string Nation { get; set; }

        public string PaymentOfShippingRate { get; set; }

        public string PaymentNation { get; set; }

        public string Currency { get; set; }

        public string Payment { get; set; }

        public string SellPrice { get; set; }

        public string Discount { get; set; }

        public string TotalPrice { get; set; }

        public string SettlePrice { get; set; }

        public string Customer { get; set; }

        public string CustomerPhonetic { get; set; }

        public string MemoToSeller { get; set; }

        public string CustomerPhoneNumber { get; set; }

        public string CustomerMobilePhone { get; set; }

        public string SellerCode { get; set; }

        public string JanCode { get; set; }

        public string StandardNumber { get; set; }

        public string GiftSender { get; set; }

    }

    public sealed class Qoo10ModelMap : CsvClassMap<Qoo10Model>
    {
        public Qoo10ModelMap()
        {
            Map(m => m.OrderNumber).Index(1);
            Map(m => m.Item).Index(13);
            Map(m => m.Quantity).Index(14);
            Map(m => m.Recipient).Index(18);
            Map(m => m.RecipientMobilePhone).Index(21);
            Map(m => m.Address).Index(22);
            Map(m => m.PostalCode).Index(23);
        }
    }
}
