﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Models
{
    public class PaymentAccountViewModel
    {
        public int AccountType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CardNumber { get; set; }
        public string CardCVV { get; set; }
        public int? ExpireMonth { get; set; }
        public int? ExpireYear { get; set; }
        public string Currency { get; set; }
    }

    public class PaymentViaBankModel
    {
        [Display(Name = "Company Name")]
        [Required]
        public string CompanyName { get; set; }

        [Display(Name = "User Name")]
        [Required]
        public string Username { get; set; }

        [Display(Name = "Transaction Reference No.")]
        [Required]
        public string TransactionNumber { get; set; }

        [Display(Name = "Transferred Amount")]
        [Required]
        public decimal TransactionAmount { get; set; }

        [Display(Name = "Transferred Date")]
        [Required]
        public string TransactionDate { get; set; }

        [Display(Name = "Transferred Time")]
        [Required]
        public string TransactionTime { get; set; }

        [Display(Name = "Mobile Number")]
        [Required]
        [RegularExpression(@"[3689][0-9]{7}", ErrorMessage = "The Mobile Number is not a valid mobile number.")]
        public string MobileNumber { get; set; }
    }

    public class PaymentHistoryModel
    {
        public long Id { get; set; }

        public DateTime? TransactionDate { get; set; }

        public decimal TransactionAmount { get; set; }

        public PaymentAccountTypes AccountType { get; set; }

        public PaymentStatus Status { get; set; }
    }

    public class PaymentStatusModel
    {
        public long Id { get; set; }

        public string CompanyName { get; set; }

        public string Username { get; set; }

        public string TransactionNumber { get; set; }

        public decimal TransactionAmount { get; set; }

        public DateTime? TransactionDate { get; set; }

        public DateTime? TransactionTime { get; set; }

        public string MobileNumber { get; set; }

        public PaymentAccountTypes AccountType { get; set; }

        public PaymentStatus Status { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public DateTime? ApprovalTime { get; set; }
    }
}