﻿using System;
using CsvHelper.Configuration;

namespace Courier_Management_System.Models
{
    public class GrouponModel
    {
        public string FulfillmentLineItemId { get; set; }

        public string GrouponNumber { get; set; }

        public string OrderDate { get; set; }

        public string MerchantSkuItem { get; set; }

        public string QuantityRequested { get; set; }

        public string ShipmentMethodRequested { get; set; }

        public string ShipmentAddressName { get; set; }

        public string ShipmentAddressStreet { get; set; }

        public string ShipmentAddressStreet2 { get; set; }

        public string ShipmentAddressCity { get; set; }

        public string ShipmentAddressStat { get; set; }

        public string ShipmentAddressPostalCode { get; set; }

        public string ShipmentAddressCountry { get; set; }

        public string Gift { get; set; }

        public string GiftMessage { get; set; }

        public string QuantityShipped { get; set; }

        public string ShipmentCarrier { get; set; }

        public string ShipmentMethod { get; set; }

        public string ShipmentTrackingNumber { get; set; }

        public string ShipDate { get; set; }

        public string GrouponSku { get; set; }

        public string CustomFieldValue { get; set; }

        public string Permalink { get; set; }

        public string ItemName { get; set; }

        public string VendorId { get; set; }

        public string SalesForceDealOptionId { get; set; }

        public string GrouponCost { get; set; }

        public string BillingAddressName { get; set; }

        public string BillingAddressStreet { get; set; }

        public string BillingAddressCity { get; set; }

        public string BillingAddressStat { get; set; }

        public string BillingAddressPostalCode { get; set; }

        public string BillingAddressCountry { get; set; }

        public string PurchaseOrderNumber { get; set; }

        public string ProductWeight { get; set; }

        public string ProductWeightUnit { get; set; }

        public string ProductLength { get; set; }

        public string ProductWidth { get; set; }

        public string ProductHeight { get; set; }

        public string ProductDimensionUnit { get; set; }

        public string CustomerPhone { get; set; }

        public string IncoTerms { get; set; }

        public string HtsCode { get; set; }

        public string ThreePlName { get; set; }

        public string ThreePlWarehouseLocation { get; set; }

        public string KittingDetails { get; set; }

        public string SellPrice { get; set; }

        public string DealOpportunityId { get; set; }

        public string ShipmentStrategy { get; set; }

        public string FulfillmentMethod { get; set; }

        public string CountryOfOrigin { get; set; }

        public string MerchantPermalink { get; set; }

        public string FeatureStartDate { get; set; }

        public string FeatureEndDate { get; set; }

        public string BomSku { get; set; }
    }

    public sealed class GrouponModelMap : CsvClassMap<GrouponModel>
    {
        public GrouponModelMap()
        {
            Map(m => m.FulfillmentLineItemId).Index(0);
            Map(m => m.GrouponNumber).Index(1);
            Map(m => m.QuantityRequested).Index(4);
            Map(m => m.ShipmentAddressName).Index(6);
            Map(m => m.ShipmentAddressStreet).Index(7);
            Map(m => m.ShipmentAddressStreet2).Index(8);
            Map(m => m.ShipmentAddressPostalCode).Index(11);
            Map(m => m.GrouponSku).Index(20);
            Map(m => m.ItemName).Index(23);
            Map(m => m.CustomerPhone).Index(40);
        }
    }

}
