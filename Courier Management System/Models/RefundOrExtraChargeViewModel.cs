﻿using Courier_Management_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class RefundOrExtraChargeViewModel
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string UserName { get; set; }
        public string SalesPerson { get; set; }
        public string ConsignmentNumber { get; set; }
        public int Reason { get; set; }
        public decimal Amount { get; set; }
        public int ApprovalStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string CustomerName { get; set; }
        public string CompanyName { get; set; }
        public int PaymentCategory { get; set; }

        public virtual ChargingReason ChargingReason { get; set; }
        public virtual Customer Customer { get; set; }
    }

    public class AddRefundViewModel
    {
        [Required]
        [Display(Name = "Customer")]
        public long CustomerId { get; set; }

        public Customer Customer { get; set; }

        //[Required]
        //[Display(Name = "Sales Person")]
        //public string SalesPerson { get; set; }

        [Required]
        [Display(Name = "Consignment number")]
        public string ConsignmentNumber { get; set; }

        [Required]
        [Display(Name = "Refund/Extra charge reason")]
        public int Reason { get; set; }

        public decimal ExistingPrice { get; set; }

        [Required]
        [Display(Name = "Refund/Extra charge amount")]
        public decimal Amount { get; set; }

        [Required]
        public decimal ApprovalStatus { get; set; }

        [Range(1, 2, ErrorMessage = "Please select payment category")]
        public PaymentCategory PaymentCategory { get; set; }
    }

    public class AddRefundOrExtraChargeBulk
    {
        [Required(ErrorMessage = "Please enter consignment numbers")]
        [Display(Name = "Consignment numbers")]
        public string ConsignmentNumbers { get; set; }

        [Required]
        [Display(Name = "Customer")]
        public long CustomerId { get; set; }

        public Customer Customer { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select reason")]
        [Display(Name = "Refund/Extra charge reason")]
        public int Reason { get; set; }

        //[Required]
        public decimal? Amount { get; set; }

        //[Required]
        public decimal ApprovalStatus { get; set; }

        [Range(1, 2, ErrorMessage = "Please select payment category")]
        public PaymentCategory PaymentCategory { get; set; }

        public List<AddRefundViewModel> BulkRefundOrExtraCharges { get; set; }
    }

    public class ChargingReasonViewModel
    {
        public int Id { get; set; }
        public int PaymentCategory { get; set; }
        public string Description { get; set; }
    }
}