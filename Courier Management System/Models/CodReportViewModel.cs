﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Courier_Management_System.Models
{
    public class CodReportViewModel
    {
        public string UserName { get; set; }

        public string CustomerName { get; set; }

        public decimal CashAmount { get; set; }

        public decimal ChequeAmount { get; set; }

        public IEnumerable<CodCollectViewModel> CollectedCash { get; set; }

        public IEnumerable<CodCollectViewModel> CollectedCheque { get; set; }
    }

    public class CodCollectViewModel
    {
        public decimal CollectedAmount { get; set; }

        public DateTime CollectedOn { get; set; }
    }
}