﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class UserViewModel
    {
        public string Id
        {
            get; set;
        }

        public string UserName
        {
            get; set;
        }

        public string Email
        {
            get; set;
        }

        public List<string> Roles
        {
            get; set;
        }

        public bool IsActive
        {
            get; set;
        }
    }

    public class UserRoleViewModel
    {
        public string Name
        {
            get; set;
        }

        public bool IsSelected
        {
            get; set;
        }
    }

    public class EditUserViewModel
    {
        public string Email
        {
            get; set;
        }

        public List<UserRoleViewModel> Roles
        {
            get; set;
        }
    }

    public class RoleViewModel
    {
        public string Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }

    public class AddEditRoleViewModel
    {
        [Required]
        [Display(Name = "Role Name")]
        public string Name
        {
            get; set;
        }
    }
}
