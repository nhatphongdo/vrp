﻿using System;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Models
{
    public class RequestViewModel
    {
        public long Id { get; set; }

        public long OrderId { get; set; }

        public string ConsignmentNumber { get; set; }

        public string BinCode { get; set; }

        public long? CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerCompany { get; set; }

        public string FromName { get; set; }

        public string FromZipCode { get; set; }

        public string FromDistrictCode { get; set; }

        public string FromZoneCode { get; set; }

        public string FromAddress { get; set; }

        public string FromMobilePhone { get; set; }

        public string FromSignature { get; set; }

        public string ToName { get; set; }

        public string ToZipCode { get; set; }

        public string ToDistrictCode { get; set; }

        public string ToZoneCode { get; set; }

        public string ToAddress { get; set; }

        public string ToMobilePhone { get; set; }

        public string ToSignature { get; set; }

        public int PaymentTypeId { get; set; }

        public decimal Price { get; set; }

        public int ServiceId { get; set; }

        public int ProductTypeId { get; set; }

        public int SizeId { get; set; }

        public int? PickupTimeSlotId { get; set; }

        public DateTime? PickupDate { get; set; }

        public int? DeliveryTimeSlotId { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string Remark { get; set; }

        public OrderStatuses Status { get; set; }

        public string QrCodePath { get; set; }

        public string PromoCode { get; set; }

        public DateTime? ConfirmedOn { get; set; }

        public string ConfirmedBy { get; set; }

        public string RejectNote { get; set; }

        public DateTime? CollectedOn { get; set; }

        public string CollectedBy { get; set; }

        public DateTime? ProceededOn { get; set; }

        public string ProceededBy { get; set; }

        public DateTime? ProceededOutOn { get; set; }

        public string ProceededOutBy { get; set; }

        public DateTime? DeliveredOn { get; set; }

        public string DeliveredBy { get; set; }

        public DateTime? CanceledOn { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string FullfilmentNumber { get; set; }

        public string GroupTrackingNumber { get; set; }

        public string OperatorRemark { get; set; }

        public string PickupRequest { get; set; }

        public string DeliveryRequest { get; set; }

        public DateTime? FailedPickupDate { get; set; }

        public DateTime? FailedDeliveryDate { get; set; }

        public DateTime? RequestDate { get; set; }
    }
}