﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class SendNotificationViewModel
    {
        public string Token
        {
            get; set;
        }

        public string ToUser
        {
            get; set;
        }

        [Required]
        public string Message
        {
            get; set;
        }

        [Required]
        public bool IsDriver
        {
            get; set;
        }
    }

    public class NotificationViewModel
    {
        public long Id
        {
            get; set;
        }

        public string Message
        {
            get; set;
        }

        public DateTime SentOn
        {
            get; set;
        }

        public string SendBy
        {
            get; set;
        }

        public string SendTo
        {
            get; set;
        }
    }
}
