﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Courier_Management_System.Models
{
    public class TimeSlotOfService
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public DateTime FromTime
        {
            get; set;
        }

        public DateTime ToTime
        {
            get; set;
        }

        public bool IsSelected
        {
            get; set;
        }
    }

    public class AddEditServiceViewModel
    {
        [Required]
        [Display(Name = "Service Name")]
        public string Name
        {
            get; set;
        }

        [Display(Name = "Description")]
        public string Description
        {
            get; set;
        }

        [Required]
        [Display(Name = "Cost")]
        public decimal Cost
        {
            get; set;
        }

        [Required]
        [Display(Name = "Allow customer to select pickup time?")]
        public bool IsPickupTimeAllow
        {
            get; set;
        }

        [Required]
        [Display(Name = "Allow customer to select pickup date?")]
        public bool IsPickupDateAllow
        {
            get; set;
        }

        [Required]
        [Display(Name = "Allow customer to select delivery time?")]
        public bool IsDeliveryTimeAllow
        {
            get; set;
        }

        [Required]
        [Display(Name = "Allow customer to select delivery time?")]
        public bool IsDeliveryDateAllow
        {
            get; set;
        }

        [Display(Name = "Pickup Time Slots")]
        public List<TimeSlotOfService> AvailablePickupTimeSlots
        {
            get; set;
        }

        [Display(Name = "Pickup Date Range (from Transaction Date)")]
        public string AvailablePickupDateRange
        {
            get; set;
        }

        [Display(Name = "Delivery Time Slots")]
        public List<TimeSlotOfService> AvailableDeliveryTimeSlots
        {
            get; set;
        }

        [Display(Name = "Delivery Date Range (from Pickup Date)")]
        public string AvailableDeliveryDateRange
        {
            get; set;
        }

        [Required]
        [Display(Name = "Available for Product Type")]
        public int ProductTypeId
        {
            get; set;
        }

        [Required]
        [Display(Name = "Is Default Promo Code Applied")]
        public bool IsAppliedDefaultPromoCode
        {
            get; set;
        }

        [Required]
        [Display(Name = "Is Active")]
        public bool IsActive
        {
            get; set;
        }

        [Display(Name = "Limit to these sizes only")]
        public string[] LimitToSizes
        {
            get; set;
        }
    }

    public class ServiceViewModel
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Description
        {
            get; set;
        }

        public decimal Cost
        {
            get; set;
        }

        public string PickupTime
        {
            get; set;
        }

        public string DeliveryTime
        {
            get; set;
        }

        public int ProductTypeId
        {
            get; set;
        }

        public bool IsAppliedDefaultPromoCode
        {
            get; set;
        }

        public bool IsActive
        {
            get; set;
        }

        public List<Size> LimitToSizes
        {
            get; set;
        }

        public DateTime CreatedOn
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }
    }
}
