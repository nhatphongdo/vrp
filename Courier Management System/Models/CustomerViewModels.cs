﻿using System;
using System.ComponentModel.DataAnnotations;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Models
{
    public class AddCustomerViewModel
    {
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [Display(Name = "Company")]
        public string Company { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Postal Code")]
        public string ZipCode { get; set; }

        [Required]
        [RegularExpression(@"[0-9+]*", ErrorMessage = "The Country Code field is not valid.")]
        [Display(Name = "Country Code")]
        public string CountryCode { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Required]
        [Display(Name = "Payment Type")]
        public int PaymentType { get; set; }

        [Display(Name = "Credit")]
        public decimal Credit { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Sales Person")]
        public string Salesperson { get; set; }

        [Display(Name = "Price Plan")]
        public int? PricePlan { get; set; }

        [Display(Name = "Default Promo Code")]
        public string[] DefaultPromoCode { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Display(Name = "Use Group Price")]
        public bool UseGroupPrice { get; set; }

        [Display(Name = "Company Prefix")]
        public string CompanyPrefix { get; set; }
    }

    public class EditCustomerViewModel
    {
        public string UserName { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Current Password")]
        public string CurrentPassword { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [Display(Name = "Company")]
        public string Company { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Unit Number")]
        public string UnitNumber { get; set; }

        [Display(Name = "Building Name")]
        public string BuildingName { get; set; }

        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [Required]
        [RegularExpression(@"[0-9+]*", ErrorMessage = "The Country Code field is not valid.")]
        [Display(Name = "Country Code")]
        public string CountryCode { get; set; }

        [Required]
        [RegularExpression(@"[0-9+\-\. ]*", ErrorMessage = "The Mobile Number field is not a valid mobile number.")]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name = "Payment Type")]
        public int PaymentType { get; set; }

        [Display(Name = "Credit")]
        public decimal Credit { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Sales Person")]
        public string Salesperson { get; set; }

        [Display(Name = "Price Plan")]
        public int? PricePlan { get; set; }

        [Display(Name = "Default Promo Code")]
        public string[] DefaultPromoCode { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [Display(Name = "Use Group Price")]
        public bool UseGroupPrice { get; set; }

        [Display(Name = "Company Prefix")]
        public string CompanyPrefix { get; set; }
    }

    public class CustomerViewModel
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Photo { get; set; }

        public string CountryCode { get; set; }

        public string MobileNumber { get; set; }

        public string Company { get; set; }

        public string ZipCode { get; set; }

        public int PaymentType { get; set; }

        public decimal Credit { get; set; }

        public bool IsActive { get; set; }

        public string Salesperson { get; set; }

        public int? PricePlan { get; set; }

        public string DefaultPromoCode { get; set; }

        public string NotificationKey { get; set; }

        public string Remark { get; set; }

        public string CompanyPrefix { get; set; }

        public bool UseGroupPrice { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}