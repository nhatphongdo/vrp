﻿
namespace Courier_Management_System.Models
{
    public class TrackLocationViewModel
    {
        public string ConsignmentNumber
        {
            get; set;
        }

        public long JobId
        {
            get; set;
        }

        public double Latitude
        {
            get; set;
        }

        public double Longitude
        {
            get; set;
        }
    }
}
