﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Courier_Management_System.Helpers;

namespace Courier_Management_System.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email
        {
            get; set;
        }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl
        {
            get; set;
        }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider
        {
            get; set;
        }
        public ICollection<System.Web.Mvc.SelectListItem> Providers
        {
            get; set;
        }
        public string ReturnUrl
        {
            get; set;
        }
        public bool RememberMe
        {
            get; set;
        }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider
        {
            get; set;
        }

        [Required]
        [Display(Name = "Code")]
        public string Code
        {
            get; set;
        }
        public string ReturnUrl
        {
            get; set;
        }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser
        {
            get; set;
        }

        public bool RememberMe
        {
            get; set;
        }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName
        {
            get; set;
        }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName
        {
            get; set;
        }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password
        {
            get; set;
        }

        [Display(Name = "Remember me?")]
        public bool RememberMe
        {
            get; set;
        }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName
        {
            get; set;
        }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email
        {
            get; set;
        }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password
        {
            get; set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword
        {
            get; set;
        }

        [Display(Name = "Full Name")]
        public string Name
        {
            get; set;
        }

        [Display(Name = "Country Code")]
        public string CountryCode
        {
            get; set;
        }

        [Display(Name = "Mobile Number")]
        public string MobileNumber
        {
            get; set;
        }

        [Display(Name = "Address")]
        public string Address
        {
            get; set;
        }

        [Display(Name = "IC Number")]
        public string ICNumber
        {
            get; set;
        }

        [Display(Name = "Account Type")]
        public AccountTypes AccountType
        {
            get; set;
        }

        [Display(Name = "Driving License")]
        public string DrivingLicense
        {
            get; set;
        }

        [Display(Name = "Vehicle Number")]
        public string VehicleNumber
        {
            get; set;
        }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName
        {
            get; set;
        }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password
        {
            get; set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword
        {
            get; set;
        }

        public string Code
        {
            get; set;
        }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName
        {
            get; set;
        }
    }

    public class UpdateViewModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password
        {
            get; set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword
        {
            get; set;
        }

        [Display(Name = "Full Name")]
        public string Name
        {
            get; set;
        }

        [Display(Name = "Email")]
        public string Email
        {
            get; set;
        }

        [Display(Name = "Country Code")]
        public string CountryCode
        {
            get; set;
        }

        [Display(Name = "Mobile Number")]
        public string MobileNumber
        {
            get; set;
        }

        [Display(Name = "Photo")]
        public string Photo
        {
            get; set;
        }

        [Display(Name = "Company")]
        public string Company
        {
            get; set;
        }

        [Display(Name = "Postal Code")]
        public string PostalCode
        {
            get; set;
        }

        [Display(Name = "Address")]
        public string Address
        {
            get; set;
        }
    }

    public class NotificationTokenViewModel
    {
        public string Token
        {
            get; set;
        }

        public PhoneTypes PhoneType
        {
            get; set;
        }
    }
}
