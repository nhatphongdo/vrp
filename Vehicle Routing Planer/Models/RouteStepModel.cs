﻿using System.Collections.Generic;

namespace Vehicle_Routing_Planer.Models
{
    public class RouteStepModel
    {
        public string StartAddress
        {
            get; set;
        }

        public LocationModel StartLocation
        {
            get; set;
        }

        public string EndAddress
        {
            get; set;
        }

        public LocationModel EndLocation
        {
            get; set;
        }

        public ValueTextModel Distance
        {
            get; set;
        }

        public ValueTextModel Duration
        {
            get; set;
        }

        public List<object> Steps
        {
            get; set;
        }
    }
}
