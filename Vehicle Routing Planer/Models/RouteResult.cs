﻿using System.Collections.Generic;

namespace Vehicle_Routing_Planer.Models
{
    public class RouteResult
    {
        public VehicleViewModel Vehicle
        {
            get; set;
        }

        public int VehicleIndex
        {
            get; set;
        }

        public int Stops
        {
            get; set;
        }

        public decimal NetProfit
        {
            get; set;
        }

        public List<int> Steps
        {
            get; set;
        }

        public List<double> Distances
        {
            get; set;
        }

        public List<double> Loads
        {
            get; set;
        }

        public List<decimal> Profits
        {
            get; set;
        }

        public List<double> DrivingTimes
        {
            get; set;
        }

        public List<double> Arrivals
        {
            get; set;
        }

        public List<double> Departures
        {
            get; set;
        }

        public List<double> WorkingTimes
        {
            get; set;
        }

        public string Path
        {
            get; set;
        }
    }
}
