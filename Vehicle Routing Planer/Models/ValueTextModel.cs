﻿namespace Vehicle_Routing_Planer.Models
{
    public class ValueTextModel
    {
        public double Value
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }
    }
}
