﻿
using System;

namespace Vehicle_Routing_Planer.Models
{
    public class HistoryModel
    {
        public int Id
        {
            get; set;
        }

        public int PlanId
        {
            get; set;
        }

        public Plan Plan
        {
            get; set;
        }

        public string Result
        {
            get; set;
        }

        public DateTime UpdatedOn
        {
            get; set;
        }
    }
}
