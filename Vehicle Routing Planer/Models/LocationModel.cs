﻿namespace Vehicle_Routing_Planer.Models
{
    public class LocationModel
    {
        public double Longitude
        {
            get; set;
        }

        public double Latitude
        {
            get; set;
        }
    }
}
