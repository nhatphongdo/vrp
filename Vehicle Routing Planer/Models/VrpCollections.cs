using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Vehicle_Routing_Planer.Models
{
    public static class VrpCollections
    {
        public static List<SelectListItem> ComputationMethods = new List<SelectListItem>()
        {
            //new SelectListItem() { Text = "Manual entry", Value = "0" },
            //new SelectListItem() { Text = "Euclidian distances", Value = "1" },
            //new SelectListItem() { Text = "Rounded Euclidian distances", Value = "2" },
            //new SelectListItem() { Text = "Geodesic approximation", Value = "3" },
            //new SelectListItem() { Text = "Rectilinear (Manhattan) distances", Value = "4" },
            new SelectListItem() { Text = "Google Maps", Value = "5" },
            new SelectListItem() { Text = "Bing Maps", Value = "6" },
            new SelectListItem() { Text = "Open Street Map", Value = "7" }
        };

        public static List<SelectListItem> RouteTypes = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Shortest", Value = "0" },
            new SelectListItem() { Text = "Fastest", Value = "1" },
            new SelectListItem() { Text = "Fastest - Real time traffic", Value = "2" }
        };

        public static List<SelectListItem> YesNoOptions = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Yes", Value = "true" },
            new SelectListItem() { Text = "No", Value = "false" }
        };

        public static List<SelectListItem> TimeWindowTypes = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Hard", Value = "0" },
            new SelectListItem() { Text = "Soft", Value = "1" }
        };

        public static List<SelectListItem> MustBeVisitedOptions = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Must be visited", Value = "1" },
            new SelectListItem() { Text = "May be visited", Value = "0" },
            new SelectListItem() { Text = "Don't visit", Value = "-1" }
        };

        //public static List<SelectListItem> ServiceTypes = new List<SelectListItem>()
        //{
        //    new SelectListItem() { Text = "Pickup", Value = "0" },
        //    new SelectListItem() { Text = "Delivery", Value = "1" }
        //};

        public static List<SelectListItem> ProductTypes = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Document", Value = "0" },
            new SelectListItem() { Text = "Parcel", Value = "1" }
        };

        public static int GetIntValue(List<SelectListItem> list, string text)
        {
            var value = list.Where(x => x.Text.Equals(text, StringComparison.OrdinalIgnoreCase))
                    .Select(x => x.Value)
                    .FirstOrDefault();

            var result = 0;
            int.TryParse(value, out result);
            return result;
        }

        public static string GetText(List<SelectListItem> list, int value)
        {
            return list.Where(x => x.Value.Equals(value.ToString(), StringComparison.OrdinalIgnoreCase))
                    .Select(x => x.Text)
                    .FirstOrDefault();
        }
    }
}