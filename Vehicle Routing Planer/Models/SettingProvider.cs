﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Vehicle_Routing_Planer.App_Start;

namespace Vehicle_Routing_Planer.Models
{
    public static class SettingProvider
    {
        public static string GoogleApiKeyName = "GoogleApiKey";
        public static string GoogleServerApiKeyName = "GoogleServerApiKey";
        public static string BingMapApiKeyName = "BingMapApiKey";
        public static string CpuTimeLimitName = "CpuTimeLimit";
        public static string MaxRetriesQueryGoogleMapsName = "MaxRetriesQueryGoogleMaps";
        public static string CourierSystemServiceBaseName = "CourierSystemServiceBase";
        public static string OSMServiceBaseName = "OSMServiceBase";

        public static List<string> GetProperties()
        {
            // Get properties through Reflections
            var type = typeof(SettingProvider);
            var result = type.GetFields(BindingFlags.Public | BindingFlags.Static).Select(prop => prop.GetValue(null).ToString()).ToList();

            return result;
        }

        public static string GetValue(string name)
        {
            using (var context = new VrpDataEntities())
            {
                return context.Settings.Where(x => x.Name == name)
                    .Select(x => x.Value)
                    .FirstOrDefault();
            }
        }

        public static void ClearCache()
        {
            InMemoryCache.ClearCache();
        }

        public static string GoogleApi
        {
            get
            {
                return InMemoryCache.GetOrSet<string>(GoogleApiKeyName, () =>
                {
                    using (var context = new VrpDataEntities())
                    {
                        return context.Settings.Where(x => x.Name == GoogleApiKeyName)
                            .Select(x => x.Value)
                            .FirstOrDefault() ?? "";
                    }
                });
            }
        }

        public static string GoogleServerApi
        {
            get
            {
                return InMemoryCache.GetOrSet<string>(GoogleServerApiKeyName, () =>
                {
                    using (var context = new VrpDataEntities())
                    {
                        return context.Settings.Where(x => x.Name == GoogleServerApiKeyName)
                            .Select(x => x.Value)
                            .FirstOrDefault() ?? "";
                    }
                });
            }
        }

        public static string BingMapApiKey
        {
            get
            {
                return InMemoryCache.GetOrSet<string>(BingMapApiKeyName, () =>
                {
                    using (var context = new VrpDataEntities())
                    {
                        return context.Settings.Where(x => x.Name == BingMapApiKeyName)
                            .Select(x => x.Value)
                            .FirstOrDefault() ?? "";
                    }
                });
            }
        }

        public static int CpuTimeLimit
        {
            get
            {
                var str = InMemoryCache.GetOrSet<string>(CpuTimeLimitName, () =>
                {
                    using (var context = new VrpDataEntities())
                    {
                        var value = context.Settings.Where(x => x.Name == CpuTimeLimitName)
                            .Select(x => x.Value)
                            .FirstOrDefault();

                        return value;
                    }
                });

                var result = 0;
                int.TryParse(str, out result);

                return result;
            }
        }

        public static int MaxRetriesQueryGoogleMaps
        {
            get
            {
                var str = InMemoryCache.GetOrSet<string>(MaxRetriesQueryGoogleMapsName, () =>
                {
                    using (var context = new VrpDataEntities())
                    {
                        var value = context.Settings.Where(x => x.Name == MaxRetriesQueryGoogleMapsName)
                            .Select(x => x.Value)
                            .FirstOrDefault();
                        
                        return value;
                    }
                });

                var result = 0;
                int.TryParse(str, out result);

                return result;
            }
        }

        public static string CourierSystemServiceBase
        {
            get
            {
                return InMemoryCache.GetOrSet<string>(CourierSystemServiceBaseName, () =>
                {
                    using (var context = new VrpDataEntities())
                    {
                        return context.Settings.Where(x => x.Name == CourierSystemServiceBaseName)
                            .Select(x => x.Value)
                            .FirstOrDefault() ?? "";
                    }
                });
            }
        }

        public static string OSMServiceBase
        {
            get
            {
                return InMemoryCache.GetOrSet<string>(OSMServiceBaseName, () =>
                {
                    using (var context = new VrpDataEntities())
                    {
                        return context.Settings.Where(x => x.Name == OSMServiceBaseName)
                            .Select(x => x.Value)
                            .FirstOrDefault() ?? "";
                    }
                });
            }
        }
    }
}
