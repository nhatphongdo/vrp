﻿using System.ComponentModel.DataAnnotations;

namespace Vehicle_Routing_Planer.Models
{
    public class PlanViewModel
    {
        [Required]
        [Display(Name = "Number of customers")]
        public int NumberOfCustomers
        {
            get; set;
        }

        [Required]
        [Display(Name = "Distance / Duration computation")]
        public int ComputationMethod
        {
            get; set;
        }


        [Required]
        [Display(Name = "Google Map route type")]
        public int RouteType
        {
            get; set;
        }

        [Display(Name = "Average vehicle speed")]
        public double AverageVehicleSpeed
        {
            get; set;
        }

        [Required]
        [Display(Name = "Number of vehicle types")]
        public int NumberOfVehicleTypes
        {
            get; set;
        }

        [Required]
        [Display(Name = "Vehicles must return to the depot?")]
        public bool ReturnToDepot
        {
            get; set;
        }

        [Required]
        [Display(Name = "Time window type")]
        public int TimeWindowType
        {
            get; set;
        }

        [Required]
        [Display(Name = "Working time includes waiting time?")]
        public bool IsIncludingWaitingTime
        {
            get; set;
        }
    }
}
