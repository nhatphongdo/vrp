﻿
namespace Vehicle_Routing_Planer.Models
{
    public class VehicleViewModel
    {
        public int Id
        {
            get; set;
        }

        public string VehicleNumber
        {
            get; set;
        }

        public double Capacity
        {
            get; set;
        }

        public decimal FixedCostPerTrip
        {
            get; set;
        }

        public decimal CostPerKilometer
        {
            get; set;
        }

        public double DistanceLimit
        {
            get; set;
        }

        public double WorkStartTime
        {
            get; set;
        }

        public double DrivingTimeLimit
        {
            get; set;
        }

        public double WorkingTimeLimit
        {
            get; set;
        }
    }
}
