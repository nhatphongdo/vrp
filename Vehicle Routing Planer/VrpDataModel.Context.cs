﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vehicle_Routing_Planer
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class VrpDataEntities : DbContext
    {
        public VrpDataEntities()
            : base("name=VrpDataEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Distance> Distances { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<PlanResult> PlanResults { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
    
        public virtual int DeleteDistances(Nullable<int> planId)
        {
            var planIdParameter = planId.HasValue ?
                new ObjectParameter("planId", planId) :
                new ObjectParameter("planId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteDistances", planIdParameter);
        }
    
        public virtual int InsertDistance(Nullable<int> fromId, Nullable<int> toId, Nullable<int> planId, Nullable<double> distance, Nullable<double> time)
        {
            var fromIdParameter = fromId.HasValue ?
                new ObjectParameter("fromId", fromId) :
                new ObjectParameter("fromId", typeof(int));
    
            var toIdParameter = toId.HasValue ?
                new ObjectParameter("toId", toId) :
                new ObjectParameter("toId", typeof(int));
    
            var planIdParameter = planId.HasValue ?
                new ObjectParameter("planId", planId) :
                new ObjectParameter("planId", typeof(int));
    
            var distanceParameter = distance.HasValue ?
                new ObjectParameter("distance", distance) :
                new ObjectParameter("distance", typeof(double));
    
            var timeParameter = time.HasValue ?
                new ObjectParameter("time", time) :
                new ObjectParameter("time", typeof(double));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertDistance", fromIdParameter, toIdParameter, planIdParameter, distanceParameter, timeParameter);
        }
    }
}
