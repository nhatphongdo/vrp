﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Vehicle_Routing_Planer.Models;

namespace Vehicle_Routing_Planer
{
    /// <summary>
    /// Map services. This is implemented as reference, should not you because of quota limitation of Google Maps Service API.
    /// Instead we should use Google Maps client services to obtain the same result.
    /// </summary>
    public class MapServices
    {
        public const string GeocodingServiceByAddress = "https://maps.googleapis.com/maps/api/geocode/json?key={0}&address={1}&components=country:SG";
        public const string GeocodingServiceByAddressComponent = "https://maps.googleapis.com/maps/api/geocode/json?key={0}&address={1}&components=postal_code:{2}|country:SG";
        public const string GetRoutesService = "https://maps.googleapis.com/maps/api/directions/json?key={0}&origin={1}&destination={2}";
        public const string GetRoutesServiceWithWaypoints = "https://maps.googleapis.com/maps/api/directions/json?key={0}&origin={1}&destination={2}&waypoints={3}";
        public const string GetMatrixDistances = "https://maps.googleapis.com/maps/api/distancematrix/json?key={0}&origins={1}&destinations={2}";

        public const string BingMapCalculateRoute = "http://dev.virtualearth.net/REST/v1/Routes?";

        public static string OSRMCalculateRoute = SettingProvider.OSMServiceBase.TrimEnd('/') + "/viaroute?";

        #region Google map services
        /// <summary>
        /// Query to Map service to get result
        /// </summary>
        public static string GetServiceResult(string url)
        {
            var uri = new Uri(url);
            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;
            var response = (HttpWebResponse) request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream == null)
            {
                return string.Empty;
            }
            var reader = new StreamReader(stream);
            var output = reader.ReadToEnd();
            response.Close();

            return output;
        }

        /// <summary>
        /// Get geocoding (latitude and longitude) of an address; Postal Code is optional, used in case there are same address in many region
        /// </summary>
        public static LocationModel GetGeocodingOfAddress(string address, string postalCode = "")
        {
            var tries = 0;
            do
            {
                // Remove # part
                while (address.Contains("#"))
                {
                    var index = address.IndexOf('#');
                    var nextSpace = address.IndexOf(' ', index);
                    if (nextSpace < 0)
                    {
                        nextSpace = address.Length - 1;
                    }
                    address = address.Remove(index, nextSpace - index);
                }

                // Add Singapore
                if (!address.ToLower().Trim().EndsWith("singapore"))
                {
                    address += ", Singapore";
                }
                string json;
                if (string.IsNullOrEmpty(postalCode))
                {
                    json = GetServiceResult(string.Format(GeocodingServiceByAddress, SettingProvider.GoogleServerApi, address));
                }
                else
                {
                    json = GetServiceResult(string.Format(GeocodingServiceByAddressComponent, SettingProvider.GoogleServerApi, address, postalCode));
                }

                // Wait a little bit to avoid flood Map system
                System.Threading.Thread.Sleep(100);

                if (!string.IsNullOrEmpty(json))
                {
                    dynamic result = JsonConvert.DeserializeObject(json);
                    if (result.status == "OK")
                    {
                        return new LocationModel()
                        {
                            Latitude = result.results[0].geometry.location.lat,
                            Longitude = result.results[0].geometry.location.lng,
                        };
                    }
                }

                ++tries;
            } while (tries < SettingProvider.MaxRetriesQueryGoogleMaps);

            return null;
        }

        /// <summary>
        /// Get routes between two locations; Waypoints is optional, used in case we want route follows this way
        /// </summary>
        public static List<RouteStepModel> GetRoutes(string origin, string destination, string[] waypoints = null)
        {
            string json;
            if (waypoints == null)
            {
                json = GetServiceResult(string.Format(GetRoutesService, SettingProvider.GoogleServerApi, origin, destination));
            }
            else
            {
                json = GetServiceResult(string.Format(GetRoutesServiceWithWaypoints, SettingProvider.GoogleServerApi, origin, destination, string.Join("|", waypoints)));
            }

            var routeSteps = new List<RouteStepModel>();

            if (string.IsNullOrEmpty(json))
            {
                return routeSteps;
            }

            dynamic result = JsonConvert.DeserializeObject(json);
            if (result.status == "OK")
            {
                var route = result.routes[0];
                for (var i = 0; i < route.legs.length; i++)
                {
                    var step = new RouteStepModel()
                    {
                        StartAddress = route.legs[i].start_address,
                        StartLocation = new LocationModel()
                        {
                            Latitude = route.legs[i].start_location.lat,
                            Longitude = route.legs[i].start_location.lng
                        },
                        EndAddress = route.legs[i].end_address,
                        EndLocation = new LocationModel()
                        {
                            Latitude = route.legs[i].end_location.lat,
                            Longitude = route.legs[i].end_location.lng
                        },
                        Distance = new ValueTextModel()
                        {
                            Value = route.legs[i].distance.value,
                            Text = route.legs[i].distance.text
                        },
                        Duration = new ValueTextModel()
                        {
                            Value = route.legs[i].duration.value,
                            Text = route.legs[i].duration.text
                        },
                        Steps = new List<object>()
                    };

                    routeSteps.Add(step);
                }
            }

            return routeSteps;
        }

        /// <summary>
        /// Get distances between locations
        /// </summary>
        public static List<List<dynamic>> GetDistances(string[] origins, string[] destinations)
        {
            var tries = 0;
            do
            {
                var json = GetServiceResult(string.Format(GetMatrixDistances, SettingProvider.GoogleServerApi, string.Join("|", origins), string.Join("|", destinations)));

                // Wait a little bit to avoid flood Map system
                System.Threading.Thread.Sleep(1000);

                var distances = new List<List<dynamic>>();

                if (string.IsNullOrEmpty(json))
                {
                    return null;
                }

                dynamic result = JsonConvert.DeserializeObject(json);
                if (result.status == "OK")
                {
                    var failed = false;
                    for (var i = 0; i < result.rows.Count; i++)
                    {
                        var row = new List<dynamic>();
                        for (var j = 0; j < result.rows[i].elements.Count; j++)
                        {
                            if (result.rows[i].elements[j].status != "OK")
                            {
                                failed = true;
                                break;
                            }

                            var distance = new
                            {
                                Distance = result.rows[i].elements[j].distance.value,
                                Duration = result.rows[i].elements[j].duration.value
                            };

                            row.Add(distance);
                        }

                        if (failed)
                        {
                            break;
                        }

                        distances.Add(row);
                    }

                    if (!failed)
                    {
                        return distances;
                    }
                }

                ++tries;
            } while (tries < SettingProvider.MaxRetriesQueryGoogleMaps);

            return null;
        }

        #endregion

        #region Bing map Distance calculation

        public static int[] GenerateSequenceWaypoints(int numberOfLocations)
        {
            var result = new int[numberOfLocations * (numberOfLocations - 1) + 1];
            var k = 0;
            for (var i = 0; i < numberOfLocations; i++)
            {
                result[k] = i;
                ++k;
                for (var j = i + 2; j < numberOfLocations; j++)
                {
                    if (i != 0 || j != numberOfLocations - 1)
                    {
                        result[k] = j;
                        ++k;
                        result[k] = i;
                        ++k;
                    }
                }
            }

            result[k] = 0;
            ++k;

            if (k < result.Length)
            {
                for (var i = numberOfLocations - 1; i >= 0; i--)
                {
                    result[k] = i;
                    ++k;
                }
            }

            return result;
        }

        public static dynamic CalculateDistances(List<Location> waypoints)
        {
            var url = BingMapCalculateRoute;
            for (var i = 0; i < waypoints.Count; i++)
            {
                url += $"wp.{i}={waypoints[i].Latitude},{waypoints[i].Longitude}&";
            }
            url += "optimize=distance";
            url += "&key=" + SettingProvider.BingMapApiKey;

            var tries = 0;
            do
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(url)
                };

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                var response = client.GetAsync("").Result;  // Blocking call!
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    var result = response.Content.ReadAsStringAsync().Result;
                    var resultObj = JsonConvert.DeserializeObject(result) as dynamic;
                    if (resultObj == null)
                    {
                        ++tries;
                    }
                    else
                    {
                        if (resultObj.statusCode != "200")
                        {
                            ++tries;
                        }
                        else
                        {
                            return resultObj;
                        }
                    }
                }
                else
                {
                    ++tries;
                }

            } while (tries < SettingProvider.MaxRetriesQueryGoogleMaps);

            return null;
        }

        #endregion

        #region OSRM calculation

        public static dynamic CalculateDistancesOSRM(Location loc1, Location loc2)
        {
            var url = OSRMCalculateRoute;
            url += $"loc={loc1.Latitude},{loc1.Longitude}&";
            url += $"loc={loc2.Latitude},{loc2.Longitude}";

            var tries = 0;
            do
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(url)
                };

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                var response = client.GetAsync("").Result;  // Blocking call!
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    var result = response.Content.ReadAsStringAsync().Result;
                    var resultObj = JsonConvert.DeserializeObject(result) as dynamic;
                    if (resultObj == null)
                    {
                        ++tries;
                    }
                    else
                    {
                        return resultObj;
                    }
                }
                else
                {
                    ++tries;
                }

            } while (tries < SettingProvider.MaxRetriesQueryGoogleMaps);

            return null;
        }

        #endregion
    }
}
