﻿using System;
using Antlr.Runtime.Tree;

namespace Vehicle_Routing_Planer
{
    public class Utilities
    {
        /// <summary>
        /// Convert a number representation of time to string format
        /// </summary>
        public static string ConvertDoubleToHour(double input)
        {
            var hour = (int) input;
            var minute = (int) Math.Round((input - hour) * 60);

            return $"{hour:00}:{minute:00}";
        }

        /// <summary>
        /// Convert a number representation of time to string format
        /// </summary>
        public static string ConvertDoubleToFullyHour(double input)
        {
            var hour = (int) input;
            var minute = (int) Math.Round((input - hour) * 60);

            return $"{hour:0} hour(s) {minute:0} minute(s)";
        }

        /// <summary>
        /// Convert a string representation of time to number format
        /// </summary>
        public static double ConvertHourToDouble(string input)
        {
            var parts = input.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                var hour = parts.Length > 0 ? int.Parse(parts[0]) : 0;
                var minute = parts.Length > 1 ? int.Parse(parts[1]) : 0;
                return hour + (double) minute / 60.0;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static double ConvertTimeToDouble(DateTime input)
        {
            return input.Hour + (double) input.Minute / 60.0;
        }

        /// <summary>
        /// Calculate distance between two locations by Euclidian method
        /// </summary>
        public static double EuclidianDistance(double latitude1, double longitude1, double latitude2, double longitude2)
        {
            return Math.Sqrt(Math.Pow(latitude1 - latitude2, 2) + Math.Pow(longitude1 - longitude1, 2));
        }

        /// <summary>
        /// Calculate distance between two locations by Rectilinear method
        /// </summary>
        public static double RectilinearDistance(double latitude1, double longitude1, double latitude2, double longitude2)
        {
            return Math.Abs(latitude1 - latitude2) + Math.Abs(longitude1 - longitude2);
        }

        /// <summary>
        /// Calculate distance between two locations by Geosedic approximation method
        /// </summary>
        public static double GeosedicDistanceApproximation(double latitude1, double longitude1, double latitude2, double longitude2)
        {
            const double C_RADIUS_EARTH_KM = 6370.97327862;

            latitude1 = (latitude1 / 180) * Math.PI;
            latitude2 = (latitude2 / 180) * Math.PI;
            longitude1 = (longitude1 / 180) * Math.PI;
            longitude2 = (longitude2 / 180) * Math.PI;

            var delta = 2 *
                        Math.Asin(
                            Math.Sqrt(Math.Pow(Math.Sin((latitude1 - latitude2) / 2), 2) +
                                      Math.Cos(latitude1) * Math.Cos(latitude2) *
                                      Math.Pow(Math.Sin((longitude1 - longitude2) / 2), 2)));

            return delta * C_RADIUS_EARTH_KM;
        }
    }
}
