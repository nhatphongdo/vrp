﻿var curIdx = 0;
var focusIdx = -1;

$(function () {
    $('.timepicker').each(function () {
        $(this).datetimepicker({
            format: "HH:mm",
            //showClose: true,
            //debug: true,
            widgetPositioning: {
                vertical: $(this).parents('tr').parent().find('tr').index($(this).parents('tr')) >= 3 ? 'auto' : 'bottom'
            }
        });
    });

    $("input.number-spinner").each(function () {
        $(this).TouchSpin({
            min: $(this).data('min-value') || 0,
            max: $(this).data('max-value') || 1000000000,
            step: $(this).data('step') || 1,
            maxboostedstep: $(this).data('max-boosted-step') || 1000,
            prefix: $(this).data('prefix') || '',
            postfix: $(this).data('postfix') || '',
            decimals: $(this).data('decimals') || 0
        });
    });
});

function addRow() {
    // Load template
    var template = $('.template-content').html();
    ++curIdx;
    template = template.replace(/{index}/gi, curIdx);
    template = template.replace(/_index_/gi, curIdx);
    $('#addressList tbody').append(template);

    $('#addressList tbody').find('#postal' + curIdx).on('change', function () {
        // Query information of this postal code
        focusIdx = $(this).attr("id").substr('#postal'.length - 1);
        $.ajax({
            url: postalUrl,
            type: 'POST',
            data: {
                postalCode: $(this).val()
            },
            success: function (data) {
                $('#address' + focusIdx).val(data.UnitNumber + " " + data.StreetName);
                $('#latitude' + focusIdx).val(data.Latitude);
                $('#longitude' + focusIdx).val(data.Longitude);
            },
            error: function (err) {
                $('#address' + focusIdx).val("");
            }
        });
    });

    var indices = $('#indexList').val().split(',');
    if (indices.length > -1 && indices[0] == '') {
        // For empty string
        indices[0] = curIdx;
    } else {
        indices.push(curIdx);
    }
    $('#indexList').val(indices.join(','));
}

function putData() {
    $('#locWaitingModal').modal({
        keyboard: false,
        backdrop: 'static',
        show: false
    });
    $('#locWaitingModal').modal('show');
    $.ajax({
        url: locationListUrl,
        type: 'POST',
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                $('#id' + (i + 1)).val(data[i].Id);
                $('#name' + (i + 1)).val(data[i].CustomerName);
                $('#postal' + (i + 1)).val(data[i].PostalCode);
                $('#latitude' + (i + 1)).val(data[i].Latitude);
                $('#longitude' + (i + 1)).val(data[i].Longitude);
                $('#address' + (i + 1)).val(data[i].StreetName);
                $('#fromTime' + (i + 1)).val(data[i].FromTime);
                $('#toTime' + (i + 1)).val(data[i].ToTime);
                $('#time' + (i + 1)).val(data[i].ServiceTime);
                $('#pickup' + (i + 1)).val(data[i].PickupAmount);
                $('#delivery' + (i + 1)).val(data[i].DeliveryAmount);
                $('#profit' + (i + 1)).val(data[i].Profit);
                $('#visited' + (i + 1)).find('option[value="' + data[i].MustBeVisited + '"]').attr('selected', 'selected');
                $('#type' + (i + 1)).find('option[value="' + data[i].Type + '"]').attr('selected', 'selected');
            }

            $('#locWaitingModal').modal('hide');
        }
    });
}

function afterInitMap() {
    // Capture update on starting location
    $('#startPostalCode').on('change', function () {
        saveStartingPoint();

        // Query information of this postal code
        $.ajax({
            url: postalUrl + $(this).val(),
            type: 'POST',
            success: function (data) {
                $('#startAddress').val(data.UnitNumber + " " + data.StreetName);
                $('#startLatitude').val(data.Latitude);
                $('#startLongitude').val(data.Longitude);
                saveStartingPoint();
            },
            error: function (err) {
                $('#startAddress').val("");
                saveStartingPoint();
            }
        });
    });

    for (var i = 1; i <= numberOfCustomers; i++) {
        $('#postal' + i).on('change', function () {
            // Query information of this postal code
            focusIdx = $(this).attr("id").substr('#postal'.length - 1);
            $.ajax({
                url: postalUrl + $(this).val(),
                type: 'POST',
                success: function (data) {
                    $('#address' + focusIdx).val(data.UnitNumber + " " + data.StreetName);
                    $('#latitude' + focusIdx).val(data.Latitude);
                    $('#longitude' + focusIdx).val(data.Longitude);
                },
                error: function (err) {
                    $('#address' + focusIdx).val("");
                }
            });
        });
    }

    $('#rememberMe').on('change', function () {
        if (this.checked == false) {
            // Remove from cookie
            setCookie("starting_point", "", -1);
        } else {
            saveStartingPoint();
        }
    });

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('startAddress'), {
        componentRestrictions: {
            country: 'SG'
        }
    });
    autocomplete.bindTo('bounds', map);

    // Restore from cookie
    if (getCookie("starting_point")) {
        document.getElementById("rememberMe").checked = true;
        if (!$('#startAddress').val()) {
            var addr = getCookie("starting_point").split('|');
            $('#startPostalCode').val(addr[0]);
            $('#startLatitude').val(addr[1]);
            $('#startLongitude').val(addr[2]);
            $('#startAddress').val(addr[3]);
        } else {
            saveStartingPoint;
        }
    }

    //// Add placeholders for locations
    //for (var i = 0; i < numberOfCustomers; i++) {
    //    addRow();
    //}

    // Re-bind validation
    var form = $('#locationsForm')
        .removeData("validator") /* added by the raw jquery.validate plugin */
        .removeData("unobtrusiveValidation"); /* added by the jquery unobtrusive plugin */
    $.validator.unobtrusive.parse(form);

    //putData();
}

function saveStartingPoint() {
    if (document.getElementById('rememberMe').checked) {
        // Save to cookie
        setCookie("starting_point", $('#startPostalCode').val() + "|" + $('#startLatitude').val() + "|" + $('#startLongitude').val() + "|" + $('#startAddress').val(), 30);
    }
}
