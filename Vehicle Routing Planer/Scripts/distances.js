﻿var curIdx = 0;
var distances = [];
var locations = [];

function afterInitMap() {
}

var directioning = false;
var outOfLimit = false;
var delayBetweenQuery = 1500;
var maxElementsPerQuery = 25;
var counter = 0;
var timer;
function preProcessBeforeSubmit() {
    if (!$('#locationsForm').valid()) {
        return false;
    }

    if (outOfLimit == true) {
        sweetAlert({
            title: "Out of service!",
            text: "You have used out of quota of Google service. Please wait and try after some minutes.",
            type: "warning"
        });
        $('#waitingModal').modal('hide');
        $('#next').show();
        delayBetweenQuery += 250;
        if (delayBetweenQuery > 5000) {
            delayBetweenQuery = 5000;
        }
        return false;
    }

    $('#next').hide();

    //if (geocoding == false) {
    //    $('#waitingModal').modal({
    //        keyboard: false,
    //        backdrop: 'static',
    //        show: false
    //    });

    //    if (!$('#startLongitude').val() || !$('#startLatitude').val()) {
    //        $('#waitingModal').modal('show');
    //        geocoding = true;
    //        updateStarting();
    //    }

    //    for (var i = 1; i <= numberOfCustomers; i++) {
    //        if (!$('#latitude' + i).val() || !$('#longitude' + i).val()) {
    //            // Get location from address
    //            $('#waitingModal').modal('show');
    //            geocoding = true;
    //            updateLocation(i, $('#address' + i).val());
    //        }
    //    }
    //}

    //// Wait for all above processes complete
    //if (!validateForm()) {
    //    setTimeout(preProcessBeforeSubmit, 1000); // Recheck after 1s
    //    return false;
    //}

    //if (geocoding == true) {
    //    // It means we canceled form before for geocoding so we need to re-submit
    //    geocoding = false;
    //    outOfLimit = false;
    //    $('#locationsForm').submit();
    //}

    //geocoding = false;

    //if (computationMethod == 5 && directioning == false) {
    //    if (!validateDistances()) {
    //        directioning = true;

    //        // Ask user before query distances from Google Maps
    //        var numberOfInterations = (numberOfCustomers + 1) * Math.ceil((numberOfCustomers + 1) / (maxElementsPerQuery - 1));
    //        delayBetweenQuery = 10000.0 * Math.min(numberOfCustomers + 2, 25) / 100.0; // Maximum 100 elements / 10 seconds
    //        var estimation = Math.ceil((numberOfInterations - failedJobIndex) / (1000.0 / delayBetweenQuery));
    //        sweetAlert({
    //            title: "Are you sure?",
    //            text: "You are using Google Maps service to calculate distances. It takes long time (estimated " + estimation + " seconds) to run. Do you want to continue?",
    //            type: "warning",
    //            showCancelButton: true,
    //            cancelButtonText: "No, cancel please!",
    //            confirmButtonText: "Yes, do it!"
    //        }, function (isConfirm) {
    //            if (isConfirm) {
    //                $('#waitingModal').modal('show');

    //                $('#progressTotal').text(numberOfInterations);

    //                calculateDistances();

    //                setTimeout(preProcessBeforeSubmit, 1000); // Recheck after 1s
    //            } else {
    //                directioning = false;
    //                $('#next').show();
    //            }
    //        });

    //        return false;
    //    }
    //} else if (computationMethod == 5 && directioning == true) {
    //    if (!validateDistances()) {
    //        setTimeout(preProcessBeforeSubmit, 1000); // Recheck after 1s
    //        return false;
    //    } else {
    //        directioning = false;
    //        $('#distances').val(JSON.stringify(distances));
    //        outOfLimit = false;
    //        $('#locationsForm').submit();
    //    }
    //}

    $('#waitingModal').modal('show');

    var numberOfInterations = Math.ceil((numberOfCustomers + 1) / 10) * Math.ceil((numberOfCustomers + 1) / 10);
    var estimation = numberOfInterations + (numberOfCustomers + 1) * 10;
    $('#progressTotal').text(numberOfInterations);

    counter = 0;
    timer = setInterval(function () {
        ++counter;
        $('#progress').text(counter);
    }, 1000);

    directioning = false;
}

var jobs = [];
var failedJobIndex = 0;
function calculateDistances() {
    locations = [];
    locations.push(new google.maps.LatLng($('#startLatitude').val(), $('#startLongitude').val()));
    for (var i = 1; i <= numberOfCustomers; i++) {
        locations.push(new google.maps.LatLng($('#latitude' + i).val(), $('#longitude' + i).val()));
    }

    distances = new Array(numberOfCustomers + 1);
    for (var i = 0; i < locations.length; i++) {
        distances[i] = new Array(numberOfCustomers + 1);
        for (var j = 0; j < numberOfCustomers + 1; j++) {
            distances[i][j] = null;
        }
    }

    jobs = [];
    for (var i = 0; i < numberOfCustomers + 1; i++) {
        for (var j = 0; j < numberOfCustomers + 1; j += (maxElementsPerQuery - 1)) {
            var origins = [];
            origins.push(locations[i]);
            var destinations = [];
            for (var k = j; k < Math.min(numberOfCustomers + 1, j + (maxElementsPerQuery - 1)) ; k++) {
                destinations.push(locations[k]);
            }
            jobs.push({ start: origins, ends: destinations, i: i, j: j });
        }
    }

    getDistances(failedJobIndex);
}

function getDistances(index, retries) {
    retries = retries || 0;
    if (retries > maxRetries) {
        failedJobIndex = index;
        outOfLimit = true;
        return;
    }

    calculateMatrixDistances(jobs[index].start, jobs[index].ends, false, false,
        function (response, params) {
            var originList = response.originAddresses;
            var destinationList = response.destinationAddresses;

            for (var x = 0; x < originList.length; x++) {
                var results = response.rows[x].elements;
                for (var y = 0; y < results.length; y++) {
                    distances[jobs[params.index].i + x][jobs[params.index].j + y] = {
                        distance: results[y].distance.value,
                        duration: results[y].duration.value
                    }
                }
            }

            // Continue with next job
            ++index;
            if (index < jobs.length) {
                setTimeout(function (p) {
                    getDistances(p.index);
                }, delayBetweenQuery, { index: index });
            }
            $('#progress').text(index);
        }, { index: index, retries: retries },
        function (status, params) {
            console.log(status);
            setTimeout(function (p) {
                getDistances(p.index, p.retries + 1);
            }, delayBetweenQuery, params);
        });
}

function validateDistances() {
    if (distances.length == 0) {
        return false;
    }

    for (var i = 0; i < numberOfCustomers + 1; i++) {
        for (var j = 0; j < numberOfCustomers + 1; j++) {
            if (!distances[i][j]) {
                return false;
            }
        }
    }

    return true;
}
