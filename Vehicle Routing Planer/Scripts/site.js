﻿var googlePin = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=";

function getPinIcon(character, background, foreground) {
    return googlePin + character + "|" + background + "|" + foreground;
}

var map;
var geocoder;
var directionsService, directionsDisplay;
var matrixDistance;
var infowindow;

function initMap() {
    // Create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 1.352083, lng: 103.819836 },
        scrollwheel: true,
        zoom: 11
    });

    geocoder = new google.maps.Geocoder();

    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer({ map: map });

    matrixDistance = new google.maps.DistanceMatrixService;

    infowindow = new google.maps.InfoWindow({});

    // Callback to function after init map
    if (afterInitMap && typeof (afterInitMap) == 'function') {
        afterInitMap();
    }
}

var markers = [];

function dropMarker(id, position, title, label, icon) {
    // Check if this marker exists
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id == id) {
            markers[i].marker.setPosition(position);
            markers[i].marker.setAnimation(google.maps.Animation.DROP);
            markers[i].marker.setTitle(title);
            markers[i].marker.setLabel(label);
            if (icon) {
                markers[i].marker.setIcon(icon);
            }
            return;
        }
    }

    var marker = new google.maps.Marker({
        map: map,
        position: position,
        title: title,
        animation: google.maps.Animation.DROP,
        label: label
    });

    if (icon) {
        marker.setIcon(icon);
    }

    marker.addListener('click', function () {
        infowindow.setContent(this.getTitle());
        infowindow.open(map, this);
    });

    markers.push({
        id: id,
        marker: marker
    });
}

function geocodeAddress(address, callback, params, error, notfound) {
    geocoder.geocode({
        address: address,
        componentRestrictions: {
            country: 'SG' // Singapore
        }
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (callback) {
                callback(results[0].geometry.location, params);
            }
        }
        else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT || status == google.maps.GeocoderStatus.UNKNOWN_ERROR) {
            if (error) {
                error(status, params);
            }
        }
        else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
            if (notfound) {
                notfound(params);
            }
        }
        else {
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function geocodeAddressComponent(postalCode, address, callback, params, error, notfound) {
    geocoder.geocode({
        address: address,
        componentRestrictions: {
            //postalCode: postalCode,
            country: 'SG' // Singapore
        }
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (callback) {
                callback(results[0].geometry.location, params);
            }
        }
        else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT || status == google.maps.GeocoderStatus.UNKNOWN_ERROR) {
            if (error) {
                error(status, params);
            }
        }
        else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
            if (notfound) {
                notfound(params);
            }
        }
        else {
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function calculateDistance(start, end, callback, params, error) {
    directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            var route = response.routes[0];
            var leg = route.legs[0];
            if (callback) {
                callback(leg, params);
            }
        }
        else if (status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT || status == google.maps.DirectionsStatus.UNKNOWN_ERROR) {
            if (error) {
                error(status, params);
            }
        }
        else {
            console.log('Directions request failed due to ' + status);
        }
    });
}

function calculateRoute(start, end, callback, overlimit) {
    directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            // Extract steps in route
            var routeSteps = [];
            var route = response.routes[0];
            for (var i = 0; i < route.legs.length; i++) {
                var step = {
                    start_address: route.legs[i].start_address,
                    start_location: route.legs[i].start_location,
                    end_address: route.legs[i].end_address,
                    end_location: route.legs[i].end_location,
                    distance: route.legs[i].distance,
                    duration: route.legs[i].duration,
                    steps: route.legs[i].steps
                }
                routeSteps.push(step);
            }

            if (callback) {
                callback(routeSteps);
            }
        }
        else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
            if (overlimit) {
                overlimit();
            }
        }
        else {
            console.log('Directions request failed due to ' + status);
        }
    });
}

function calculateAndDisplayRoute(start, end) {
    directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            // Extract steps in route
            var routeSteps = [];
            var route = response.routes[0];
            for (var i = 0; i < route.legs.length; i++) {
                var step = {
                    start_address: route.legs[i].start_address,
                    start_location: route.legs[i].start_location,
                    end_address: route.legs[i].end_address,
                    end_location: route.legs[i].end_location,
                    distance: route.legs[i].distance,
                    duration: route.legs[i].duration,
                    steps: route.legs[i].steps
                }
                routeSteps.push(step);
            }
        } else {
            console.log('Directions request failed due to ' + status);
        }
    });
}

function calculateAndDisplayRouteWithWaypoints(start, end, waypoints) {
    var waypts = [];
    for (var i = 0; i < waypoints.length; i++) {
        waypts.push({
            location: waypoints[i],
            stopover: true
        });
    }

    directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING,
        waypoints: waypts,
        optimizeWaypoints: false
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            // Extract steps in route
            var routeSteps = [];
            var route = response.routes[0];
            for (var i = 0; i < route.legs.length; i++) {
                var step = {
                    start_address: route.legs[i].start_address,
                    start_location: route.legs[i].start_location,
                    end_address: route.legs[i].end_address,
                    end_location: route.legs[i].end_location,
                    distance: route.legs[i].distance,
                    duration: route.legs[i].duration,
                    steps: route.legs[i].steps
                }
                routeSteps.push(step);
            }
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

function calculateMatrixDistances(origins, destinations, avoidHighways, avoidTolls, callback, params, error) {
    matrixDistance.getDistanceMatrix({
        origins: origins,
        destinations: destinations,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: avoidHighways || false,
        avoidTolls: avoidTolls || false
    }, function (response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK) {
            if (callback) {
                callback(response, params);
            }
        }
        else if (status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT || status == google.maps.DirectionsStatus.UNKNOWN_ERROR) {
            if (error) {
                error(status, params);
            }
        }
        else {
            console.log('Get matrix distances failed because: ' + status);
        }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; path=/; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
