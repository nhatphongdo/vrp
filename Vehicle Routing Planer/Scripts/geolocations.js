﻿function afterInitMap() {
    $('#startLatitude, #startLongitude').on('change', function() {
        updateStarting();
    });

    // Load starting point
    updateStarting();

    for (var i = 0; i < numberOfCustomers; i++) {
        $('#latitude' + (i + 1) + ", " + '#longitude' + (i + 1)).on('change', function () {
            updateLocation(i + 1, $('#address' + (i + 1)).text());
        });

        // Drop marker
        if ($('#latitude' + (i + 1)).val() && $('#longitude' + (i + 1)).val()) {
            // Already have location
            dropMarker(i + 1, new google.maps.LatLng($('#latitude' + (i + 1)).val(), $('#longitude' + (i + 1)).val()),
                $('#name' + (i + 1)).text() + "<br />(" + $('#address' + (i + 1)).text() + ")", '', getPinIcon('D', '00FF00', '000000'));
        }
    }
}

function updateStarting(retries) {
    if ($('#startLatitude').val() && $('#startLongitude').val()) {
        var location = new google.maps.LatLng($('#startLatitude').val(), $('#startLongitude').val());
        dropMarker($('#startId').val(), location, $('#startAddress').text().trim(), '', getPinIcon('S', 'FF0000', 'FFFFFF'));
        map.setCenter(location);
        return;
    }

    if (!$('#startAddress').val()) {
        return;
    }

    retries = retries || 0;
    if (retries > maxRetries) {
        outOfLimit = true;
        return;
    }

    geocodeAddress($('#startAddress').text(), updateStartingCallback, null, function () {
        setTimeout(updateStarting, 1000, retries + 1); // Retry after 1s due to out of requests / unknown error
    }, function () {
        // Not found
        $('#startLatitude').addClass('input-validation-error');
        $('#startLongitude').addClass('input-validation-error');
        if (geocoding) {
            $('#waitingModal').modal('hide');
            $('#next').show();
        }
    });
}

function updateStartingCallback(location) {
    $('#startLatitude').val(location.lat());
    $('#startLongitude').val(location.lng());
    dropMarker($('#startId').val(), location, $('#startAddress').val(), '', getPinIcon('S', 'FF0000', 'FFFFFF'));

    map.setCenter(location);
}

function updateLocation(locId, address, retries) {
    if ($('#latitude' + locId).val() && $('#longitude' + locId)) {
        var location = new google.maps.LatLng($('#latitude' + locId).val(), $('#longitude' + locId).val());
        dropMarker(locId, location, $('#name' + locId).text() + "<br />(" + $('#address' + locId).text() + ")", '', getPinIcon('D', '00FF00', '000000'));
        return;
    }

    retries = retries || 0;
    if (retries > maxRetries) {
        $('#locWaitingModal').modal('hide');
        outOfLimit = true;
        return;
    }

    geocodeAddress(address, function (location, params) {
        dropMarker(params.id, location, $('#name' + params.id).text() + "<br />(" + $('#address' + params.id).text() + ")", '', getPinIcon('D', '00FF00', '000000'));
        $('#latitude' + params.id).val(location.lat());
        $('#longitude' + params.id).val(location.lng());

        // Update progress
        var current = parseInt($('#locProgress').text());
        if (current >= parseInt($('#locProgressTotal').text())) {
            $('#locWaitingModal').modal('hide');
        }
        $('#locProgress').text(current + 1);
    }, { id: locId, retries: retries }, function (status, params) {
        // Retry after 1s
        setTimeout(function (pid) {
            updateLocation(pid, $('#address' + pid).text(), params.retries + 1);
        }, 1000, params.id);
    }, function (params) {
        // Not found
        $('#latitude' + params.id).addClass('input-validation-error');
        $('#longitude' + params.id).addClass('input-validation-error');
        var current = parseInt($('#locProgress').text());
        if (current >= parseInt($('#locProgressTotal').text())) {
            $('#locWaitingModal').modal('hide');
        }
        $('#locProgress').text(current + 1);
        if (geocoding) {
            $('#waitingModal').modal('hide');
            $('#next').show();
        }
    });
}

var geocoding = false;
var outOfLimit = false;
var delayBetweenQuery = 1500;
var counter = 0;
var timer;
function preProcessBeforeSubmit() {
    if (!$('#locationsForm').valid()) {
        return false;
    }

    if (outOfLimit == true) {
        sweetAlert({
            title: "Out of service!",
            text: "You have used out of quota of Google service. Please wait and try after some minutes.",
            type: "warning"
        });
        $('#waitingModal').modal('hide');
        $('#next').show();
        delayBetweenQuery += 250;
        if (delayBetweenQuery > 5000) {
            delayBetweenQuery = 5000;
        }
        return false;
    }

    $('#next').hide();

    $('#waitingModal').modal('show');

    var numberOfInterations = Math.ceil((numberOfCustomers + 1) / 10) * Math.ceil((numberOfCustomers + 1) / 10);
    var estimation = numberOfInterations + (numberOfCustomers + 1) * 10;
    $('#progressTotal').text(numberOfInterations);

    counter = 0;
    timer = setInterval(function () {
        ++counter;
        $('#progress').text(counter);
    }, 1000);
}

function validateForm() {
    // Use in case client-base calculation
    if (!$('#startLongitude').val() || !$('#startLatitude').val()) {
        return false;
    }

    for (var i = 1; i <= numberOfCustomers; i++) {
        if (!$('#latitude' + i).val() || !$('#longitude' + i).val()) {
            return false;
        }
    }

    return true;
}
