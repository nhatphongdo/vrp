﻿var polylines = [];
var paths = [];
var colors;
var lineSymbol;
var customerNames = [];
var timer;
var counter = 0;
var hasWarning = false;
var hasError = false;

function afterInitMap() {
    $('#waitingModal').modal({
        keyboard: false,
        backdrop: 'static',
        show: false
    });

    colors = randomColor({ hue: 'random', luminosity: 'dark', count: 50 });

    lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        scale: 4
    };

    $('#rerun').on('click', function () {
        computeMap();
    });

    computeMap();
}

function showPathOnMap(id, delivery) {
    var poly = new google.maps.Polyline({
        strokeColor: colors[(id - 1) % 50],
        strokeOpacity: 0.8,
        strokeWeight: 5
    });
    poly.setMap(map);
    polylines[id - 1] = poly;

    var path = JSON.parse($('#path' + id).val());
    for (var idx = 0; idx < path.length; idx++) {
        if (idx > 0 && path[idx].Latitude == path[0].Latitude && path[idx].Longitude == path[0].Longitude) {
            continue;
        }
        var location = new google.maps.LatLng(path[idx].Latitude, path[idx].Longitude);
        if (idx == 0) {
            map.setCenter(location);
        }
        var color = colors[(id - 1) % 50].substr(1);
        var bgColor = colors[(id - 1) % 50].substr(1);
        if (delivery[idx]) {
            bgColor = '00FF00';
            color = '000000';
        } else {
            bgColor = 'FF0000';
            color = 'FFFFFF';
        }
        dropMarker(idx == 0 ? idx : (id + "_" + idx), location, customerNames[id - 1][idx], "", getPinIcon(idx == 0 ? "S" : idx, idx == 0 ? '44EADC' : bgColor, idx == 0 ? '000000' : color));
    }

    paths[id - 1] = [];
    startExploring(id, path, 0, poly);
}

function showOnlyThisPath(id) {
    for (var i = 0; i < polylines.length; i++) {
        polylines[i].setMap(null);
    }
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id.toString() == "0" || markers[i].id.toString().indexOf(id) == 0) {
            markers[i].marker.setMap(map);
        } else {
            markers[i].marker.setMap(null);
        }
    }
    polylines[id - 1].setMap(map);
}

function showAllPaths() {
    for (var i = 0; i < polylines.length; i++) {
        polylines[i].setMap(map);
    }
    for (var i = 0; i < markers.length; i++) {
        markers[i].marker.setMap(map);
    }
}

function startExploring(id, path, idx, poly) {
    if (idx >= path.length - 1) {
        poly.setPath(paths[id - 1]);
        //var icons = [];
        //var step = Math.round(paths[id - 1].length / 20);
        //for (var i = 0; i < paths[id - 1].length; i += step) {
        //    icons.push({
        //        icon: lineSymbol,
        //        offset: (i * 100.0 / paths[id - 1].length) + '%'
        //    });
        //}
        //poly.setOptions({
        //    icons: icons
        //});
        return;
    }
    var loc1 = new google.maps.LatLng(path[idx].Latitude, path[idx].Longitude);
    var loc2 = new google.maps.LatLng(path[idx + 1].Latitude, path[idx + 1].Longitude);
    calculateRoute(loc1, loc2, function (legs) {
        for (var i = 0; i < legs.length; i++) {
            for (var j = 0; j < legs[i].steps.length; j++) {
                paths[id - 1].push.apply(paths[id - 1], legs[i].steps[j].path);
            }
        }
        startExploring(id, path, idx + 1, poly);
    }, function () {
        setTimeout(function () {
            startExploring(id, path, idx, poly);
        }, 2000);
    });
}

function computeMap(force) {
    $('#infeasibilities').removeClass("alert alert-warning").html("");
    $('#resultTables').html("");

    $('#waitingModal').modal('show');

    $('#totalTime').text(totalTime);

    counter = 0;
    timer = setInterval(function () {
        ++counter;
        $('#runTime').text(counter);        
    }, 1000);

    hasError = false;
    hasWarning = false;
    $.ajax({
        url: computePlanUrl,
        data: {
            id: planId,
            force: force || false
        },
        type: 'POST',
        success: function (data) {
            $('#waitingModal').modal('hide');

            if (timer != null) {
                clearInterval(timer);
            }

            if (data.IsError == true) {
                if (data.Code == 1) {
                    // Need to confirm before re-execute
                    var text = data.Message + "<br />";
                    if (data.Infeasibilities) {
                        // There are infeasibilities
                        for (var i = 0; i < Math.min(3, data.Infeasibilities.length) ; i++) {
                            text += data.Infeasibilities[i] + "<br />";
                        }
                    }
                    text += "Do you want to ignore and continue?";
                    sweetAlert({
                        title: "Are you sure?",
                        text: text,
                        type: "warning",
                        html: true,
                        showCancelButton: true,
                        cancelButtonText: "No, cancel please!",
                        confirmButtonText: "Yes, do it!"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            computeMap(true);
                        }
                    });
                } else {
                    // Show error message
                    sweetAlert({
                        title: "Oops...",
                        text: data.Message,
                        type: "error",
                        html: true
                    });
                }
            } else {
                // Show success message
                sweetAlert({
                    title: "Got it...",
                    text: data.Message,
                    type: "success",
                    html: true
                });

                if (data.Infeasibilities) {
                    // There are infeasibilities
                    $('#infeasibilities').addClass("alert alert-warning");
                    var ul = $("<ul>").appendTo($('#infeasibilities'));
                    for (var i = 0; i < data.Infeasibilities.length; i++) {
                        $("<li>").html(data.Infeasibilities[i]).appendTo(ul);
                    }
                }

                var resultDiv = $('#resultTables');
                if (data.Routes) {
                    polylines = new Array(data.Routes.length);
                    paths = new Array(data.Routes.length);
                    customerNames = new Array(data.Routes.length);
                    for (var i = 0; i < data.Routes.length; i++) {
                        var route = data.Routes[i];
                        $('<input>').attr('type', 'hidden').attr('id', 'path' + (i + 1)).val(route.Path).appendTo(resultDiv);
                        var containerDiv = $('<div>').addClass("table-responsive").appendTo(resultDiv);
                        var table = $('<table>').addClass("table table-bordered table-hover table-striped").appendTo(containerDiv);
                        //- Number: {vehicleIndex}
                        var html = "\
                        <thead>\
                        <tr>\
                            <th style='background-color: {color}'><button class='btn btn-default' onclick='showOnlyThisPath({num})'>Show on map</button></th>\
                            <th>Vehicle:</th>\
                            <th colspan='4'>{vehicleName}</th>\
                            <th>Stops</th>\
                            <th>{stops}</th>\
                            <th>Gross Profit ($):</th>\
                            <th colspan='2'>{netProfit}</th>\
                        </tr>\
                        <tr>\
                            <th>Stop Count</th>\
                            <th>Location Name</th>\
                            <th>Cum Dist (km)</th>\
                            <th>Pickup</th>\
                            <th>Delivery</th>\
                            <th>Load</th>\
                            <th>Cum Drive Time</th>\
                            <th>Arrival</th>\
                            <th>Departure</th>\
                            <th>Cum Work Time</th>\
                            <th>Cum Gross Rev ($)</th>\
                        </tr>\
                        </thead>";
                        html = html.replace(/{color}/gi, colors[i % 50]);
                        html = html.replace(/{num}/gi, i + 1);
                        html = html.replace(/{stops}/gi, route.Stops);
                        html = html.replace(/{vehicleName}/gi, route.Vehicle.VehicleNumber);
                        html = html.replace(/{vehicleIndex}/gi, route.VehicleIndex);
                        html = html.replace(/{netProfit}/gi, route.NetProfit.toFixed(2));
                        table.append(html);

                        customerNames[i] = [];
                        var delivery = [];

                        var tbody = $("<tbody>").appendTo(table);
                        for (var j = 0; j < route.Steps.length; j++) {
                            var tr = $("<tr>").appendTo(tbody);
                            $("<td>").text(j).appendTo(tr);
                            var locIdx = route.Steps[j] - 1;
                            $("<td>").text(data.Locations[locIdx].CustomerName).appendTo(tr);
                            var distance = $("<td>").text(route.Distances[j].toFixed(2)).appendTo(tr);
                            if (data.Plan && route.Distances[j] > data.Plan.DistanceLimit) {
                                distance.addClass('warningCode');
                                hasWarning = true;
                            }
                            $("<td>").text(data.Locations[locIdx].PickupAmount.toFixed(0)).appendTo(tr);
                            $("<td>").text(data.Locations[locIdx].DeliveryAmount.toFixed(0)).appendTo(tr);
                            var load = $("<td>").text(route.Loads[j].toFixed(0)).appendTo(tr);
                            if (route.Loads[j] > route.Vehicle.Capacity) {
                                load.addClass('errorCode');
                                hasError = true;
                            }
                            var drivingTime = $("<td>").text(convertDoubleToHour(route.DrivingTimes[j])).appendTo(tr);
                            if (data.Plan && route.DrivingTimes[j] > data.Plan.DrivingTimeLimit) {
                                drivingTime.addClass('warningCode');
                                hasWarning = true;
                            }
                            var arrival = $("<td>").text(convertDoubleToHour(route.Arrivals[j])).appendTo(tr);
                            if (data.Plan && route.Arrivals[j] > data.Plan.WorkEndTime) {
                                arrival.addClass('warningCode');
                                hasWarning = true;
                            }
                            if ((data.Locations[locIdx].ToTime && route.Arrivals[j] > data.Locations[locIdx].ToTime) ||
                                (data.Locations[locIdx].FromTime && route.Arrivals[j] < data.Locations[locIdx].FromTime)) {
                                arrival.addClass('warningCode');
                                hasWarning = true;
                            }
                            if (route.Arrivals[j] >= 24) {
                                arrival.addClass('errorCode');
                                hasError = true;
                            }
                            var depature = $("<td>").text(convertDoubleToHour(route.Departures[j])).appendTo(tr);
                            if (data.Plan && route.Departures[j] > data.Plan.WorkEndTime) {
                                depature.addClass('warningCode');
                                hasWarning = true;
                            }
                            if ((data.Locations[locIdx].ToTime && route.Departures[j] > data.Locations[locIdx].ToTime) ||
                                (data.Locations[locIdx].FromTime && route.Departures[j] < data.Locations[locIdx].FromTime)) {
                                depature.addClass('warningCode');
                                hasWarning = true;
                            }
                            if (route.Departures[j] >= 24) {
                                depature.addClass('errorCode');
                                hasError = true;
                            }
                            var workingTime = $("<td>").text(convertDoubleToHour(route.WorkingTimes[j])).appendTo(tr);
                            if (data.Plan && route.WorkingTimes[j] > data.Plan.WorkingTimeLimit) {
                                workingTime.addClass('warningCode');
                                hasWarning = true;
                            }
                            if (route.WorkingTimes[j] >= 24) {
                                workingTime.addClass('errorCode');
                                hasError = true;
                            }
                            $("<td>").text(route.Profits[j].toFixed(2)).appendTo(tr);

                            customerNames[i].push(data.Locations[locIdx].CustomerName);

                            if (data.Locations[locIdx].DeliveryAmount > 0) {
                                delivery.push(true);
                            } else {
                                delivery.push(false);
                            }
                        }

                        showPathOnMap(i + 1, delivery);
                    }
                }
            }
        },
        error: function () {
            $('#waitingModal').modal('hide');
        }
    });
}

function convertDoubleToHour(input) {
    var hour = Math.floor(input);
    var minute = Math.round((input - hour) * 60);

    return (hour > 9 ? hour : ("0" + hour)) + ":" + (minute > 9 ? minute : ("0" + minute));
}

function errorWarn() {
    if (hasError || hasWarning) {
        var text = "";
        if (hasWarning) {
            text = "warnings";
        }
        if (hasError) {
            if (text != "") {
                text += " and ";
            }
            text += "errors";
        }
        sweetAlert(
            {
                title: "Are you sure?",
                text: "The current plan still has some " + text + ". Do you really want to use it?",
                type: "warning",
                html: true,
                showCancelButton: true,
                cancelButtonText: "No, cancel please!",
                confirmButtonText: "Yes, do it!"
            },
            function(isConfirm) {
                if (isConfirm) {
                    hasError = false;
                    hasWarning = false;
                    $("#exportForm").submit();
                    return false;
                }
            });
        return false;
    }
    else {
        return true;
    }
}