﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Excel;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using OfficeOpenXml;
using Vehicle_Routing_Planer.Models;

namespace Vehicle_Routing_Planer.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        #region Route planer steps

        public ActionResult Index(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                if (string.IsNullOrEmpty(CourierService.GetAccessToken()))
                {
                    var authenticationUrl = WebConfigurationManager.AppSettings["AuthenticationPageUrl"] ?? "";
                    return Redirect(string.Format(authenticationUrl, Url.Action("Index", "Home", null, Request.Url?.Scheme)));
                }

                var vehicles = CourierService.GetVehicles();

                var plan = context.Plans.FirstOrDefault(x => x.Id == id);

                var model = new PlanViewModel();

                if (plan == null)
                {
                    // Set default value
                    model.NumberOfCustomers = 1;
                    model.ComputationMethod = 7;
                    model.TimeWindowType = 0;
                    model.AverageVehicleSpeed = 70;
                    model.NumberOfVehicleTypes = 0;
                    model.RouteType = 1;
                    model.ReturnToDepot = true;
                    model.IsIncludingWaitingTime = true;
                }
                else
                {
                    model.NumberOfCustomers = plan.NumberOfCustomers;
                    model.ComputationMethod = plan.ComputationMethod;
                    model.TimeWindowType = plan.TimeWindowType;
                    model.AverageVehicleSpeed = plan.AverageVehicleSpeed;
                    model.NumberOfVehicleTypes = plan.NumberOfVehicleTypes;
                    model.RouteType = plan.RouteType;
                    model.ReturnToDepot = plan.ReturnToDepot;
                    model.IsIncludingWaitingTime = plan.IsIncludingWaitingTime;

                    ViewBag.SelectedVehicles = plan.Vehicles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }

                ViewBag.Vehicles = vehicles;

                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int? id, PlanViewModel model, FormCollection formCollection)
        {
            using (var context = new VrpDataEntities())
            {
                if (string.IsNullOrEmpty(CourierService.GetAccessToken()))
                {
                    var authenticationUrl = WebConfigurationManager.AppSettings["AuthenticationPageUrl"] ?? "";
                    return Redirect(string.Format(authenticationUrl, Url.Action("Index", "Home", null, Request.Url?.Scheme)));
                }

                var vehicles = CourierService.GetVehicles();
                ViewBag.SelectedVehicles = (formCollection["Vehicles"] ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                ViewBag.Vehicles = vehicles;

                if (string.IsNullOrEmpty(formCollection["Vehicles"]))
                {
                    ViewBag.ErrorMessage = "You must select at least 1 vehicle for transportation.";
                    return View();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        var plan = context.Plans.FirstOrDefault(x => x.Id == id);
                        if (plan == null)
                        {
                            plan = new Plan()
                                   {
                                       CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                       CreatedOn = DateTime.Now,
                                   };
                            context.Plans.Add(plan);
                        }
                        plan.NumberOfCustomers = model.NumberOfCustomers;
                        plan.NumberOfVehicleTypes = model.NumberOfVehicleTypes;
                        plan.Vehicles = formCollection["Vehicles"] ?? "";
                        plan.ComputationMethod = model.ComputationMethod;
                        plan.TimeWindowType = model.TimeWindowType;
                        plan.AverageVehicleSpeed = model.AverageVehicleSpeed;
                        plan.ReturnToDepot = model.ReturnToDepot;
                        plan.RouteType = model.RouteType;
                        plan.IsIncludingWaitingTime = model.IsIncludingWaitingTime;
                        plan.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        plan.UpdatedOn = DateTime.Now;

                        context.SaveChanges();

                        return RedirectToAction(
                                                "Locations",
                                                new
                                                {
                                                    id = plan.Id,
                                                    file = Request["file"]
                                                });
                    }
                    catch (Exception exc)
                    {
                        // Log and show error
                        ViewBag.ErrorMessage = "There is error in processing. Please contact to administrator for support with these information: <br />" +
                                               exc.Message + "<br />" + exc.StackTrace;
                    }
                }

                return View(model);
            }
        }

        public ActionResult Locations(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                ViewBag.Plan = context.Plans.FirstOrDefault(x => x.Id == id);

                if (ViewBag.Plan == null)
                {
                    // Return to first page if there is no plan
                    return RedirectToAction("Index");
                }

                if (TempData["ErrorMessage"] != null)
                {
                    ViewBag.ErrorMessage = TempData["ErrorMessage"];
                }

                if (TempData["ReopenImport"] != null)
                {
                    ViewBag.ReopenImport = (bool) TempData["ReopenImport"];
                }
                else
                {
                    ViewBag.ReopenImport = false;
                }

                ViewBag.StartingLocation = context.Locations.FirstOrDefault(x => x.Starting && x.PlanId == id);

                var locations = context.Locations.Where(x => x.PlanId == id && !x.Starting).ToList();
                if (locations.Count < ViewBag.Plan.NumberOfCustomers)
                {
                    for (var i = locations.Count; i < ViewBag.Plan.NumberOfCustomers; i++)
                    {
                        locations.Add(
                                      new Location()
                                      {
                                          MustBeVisited = 1
                                      });
                    }
                }

                ViewBag.Locations = locations;

                if (!string.IsNullOrEmpty(Request["file"]))
                {
                    // Download file
                    if (!Directory.Exists(Server.MapPath("~/Temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Temp"));
                    }
                    var filePath = Server.MapPath("~/Temp/") + Request["file"];
                    var client = new WebClient();
                    client.DownloadFile(SettingProvider.CourierSystemServiceBase.TrimEnd('/') + "/Temp/" + Request["file"], filePath);
                    var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    return ImportFile(ViewBag.Plan.Id, filePath, stream);
                }

                return View();
            }
        }

        [HttpPost]
        [ActionName("Locations")]
        [ValidateAntiForgeryToken]
        public ActionResult Locations_POST(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                var plan = context.Plans.FirstOrDefault(x => x.Id == id);

                if (plan == null)
                {
                    // Return to first page if there is no plan
                    return RedirectToAction("Index");
                }

                ViewBag.Plan = plan;
                ViewBag.ReopenImport = false;

                var locations = context.Locations.Where(x => x.PlanId == id);

                var starting = locations.FirstOrDefault(x => x.Starting);
                try
                {
                    // Save info to plan
                    plan.ReturnToDepot = Request["returnToStart"] == "on";
                    context.SaveChanges();

                    ViewBag.Plan = plan;

                    LocationModel geoLocation = null;
                    var tries = 0;
                    var locs = new List<Location>();

                    // Update starting location
                    if (starting == null)
                    {
                        starting = new Location()
                                   {
                                       CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                       CreatedOn = DateTime.Now,
                                       CustomerName = "Depot",
                                       PlanId = plan.Id,
                                       Starting = true
                                   };
                        context.Locations.Add(starting);
                    }
                    starting.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    starting.UpdatedOn = DateTime.Now;
                    starting.PostalCode = Request["startPostalCode"];
                    starting.Latitude = string.IsNullOrEmpty(Request["startLatitude"]) ? null : (double?) double.Parse(Request["startLatitude"]);
                    starting.Longitude = string.IsNullOrEmpty(Request["startLongitude"]) ? null : (double?) double.Parse(Request["startLongitude"]);
                    if (starting.Latitude == null || starting.Longitude == null)
                    {
                        var zipCode = CourierService.GetZipCode(starting.PostalCode);
                        if (zipCode != null)
                        {
                            starting.Latitude = zipCode.Latitude;
                            starting.Longitude = zipCode.Longitude;
                        }
                    }
                    starting.StreetName = Request["startAddress"]; // At this point, hold both Street and Unit number
                    starting.ServiceTime = Utilities.ConvertHourToDouble(string.IsNullOrEmpty(Request["startServiceTime"]) ? "00:00" : Request["startServiceTime"]);
                    starting.FromTime = Utilities.ConvertHourToDouble(string.IsNullOrEmpty(Request["startWorkStartTime"]) ? "00:00" : Request["startWorkStartTime"]);
                    starting.ToTime = Utilities.ConvertHourToDouble(string.IsNullOrEmpty(Request["startWorkEndTime"]) ? "23:59" : Request["startWorkEndTime"]);
                    //starting.UnitNumber = Request["startUnitNumber"];

                    // Update other locations
                    for (var i = 1; i <= plan.NumberOfCustomers; i++)
                    {
                        var locId = int.Parse(string.IsNullOrEmpty(Request["id" + i]) ? "0" : Request["id" + i]);
                        var location = locations.FirstOrDefault(x => x.Id == locId);
                        if (location == null)
                        {
                            location = new Location()
                                       {
                                           CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                           CreatedOn = DateTime.Now,
                                           PlanId = plan.Id,
                                           Starting = false
                                       };
                            context.Locations.Add(location);
                        }
                        location.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        location.UpdatedOn = DateTime.Now;
                        location.CustomerName = Request["name" + i];
                        location.PostalCode = Request["postal" + i];
                        location.Latitude = string.IsNullOrEmpty(Request["latitude" + i]) ? null : (double?) double.Parse(Request["latitude" + i]);
                        location.Longitude = string.IsNullOrEmpty(Request["longitude" + i]) ? null : (double?) double.Parse(Request["longitude" + i]);
                        if (location.Latitude == null || location.Longitude == null)
                        {
                            var zipCode = CourierService.GetZipCode(location.PostalCode);
                            if (zipCode != null)
                            {
                                location.Latitude = zipCode.Latitude;
                                location.Longitude = zipCode.Longitude;
                            }
                        }
                        //location.UnitNumber = Request["unitNumber" + i];
                        location.StreetName = Request["address" + i]; // At this point, hold both Street and Unit number
                        //location.MustBeVisited = int.Parse(Request["visited" + i]);
                        location.MustBeVisited = 1;
                        location.ServiceTime = Utilities.ConvertHourToDouble(string.IsNullOrEmpty(Request["time" + i]) ? "00:00" : Request["time" + i]);
                        //location.Service = int.Parse(Request["service" + i]);
                        location.Type = int.Parse(Request["type" + i]);
                        location.PickupAmount = double.Parse(string.IsNullOrEmpty(Request["pickup" + i]) ? "0" : Request["pickup" + i]);
                        location.DeliveryAmount = double.Parse(string.IsNullOrEmpty(Request["delivery" + i]) ? "0" : Request["delivery" + i]);
                        location.Profit = decimal.Parse(string.IsNullOrEmpty(Request["profit" + i]) ? "0" : Request["profit" + i]);
                        location.FromTime = Utilities.ConvertHourToDouble(string.IsNullOrEmpty(Request["fromTime" + i]) ? "00:00" : Request["fromTime" + i]);
                        location.ToTime = Utilities.ConvertHourToDouble(string.IsNullOrEmpty(Request["toTime" + i]) ? "23:59" : Request["toTime" + i]);

                        locs.Add(location);
                    }

                    context.SaveChanges();

                    // Move to GeoLocations page
                    return RedirectToAction(
                                            "GeoLocations",
                                            new
                                            {
                                                id = id
                                            });
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessage = "There is error in processing. Please contact to administrator for support with these information: <br />" +
                                           exc.Message + "<br />" + exc.StackTrace;
                }

                ViewBag.StartingLocation = starting;
                var loccs = locations.Where(x => !x.Starting).ToList();
                if (loccs.Count < ViewBag.Plan.NumberOfCustomers)
                {
                    for (var i = loccs.Count; i < ViewBag.Plan.NumberOfCustomers; i++)
                    {
                        loccs.Add(new Location());
                    }
                }

                ViewBag.Locations = loccs;

                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Import(int id)
        {
            var file = Request.Files.Get("importFile");

            return ImportFile(id, file?.FileName, file?.InputStream);
        }

        private ActionResult ImportFile(int id, string fileName, Stream fileStream)
        {
            if (!string.IsNullOrEmpty(fileName) && fileStream != null)
            {
                IExcelDataReader excelReader;

                if (fileName.ToLower().EndsWith(".xls"))
                {
                    // Load data from Excel 2003 file
                    excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                }
                else if (fileName.ToLower().EndsWith(".xlsx"))
                {
                    // Load data from Excel 2007 file
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                }
                else
                {
                    excelReader = null;
                }

                if (excelReader != null)
                {
                    var rowCount = 0;
                    try
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        var data = excelReader.AsDataSet();

                        if (data.Tables.Count == 0)
                        {
                            // There is no table in sheet
                            TempData["ErrorMessage"] = "Import file does not have correct format. There is no data sheet. Please refer the attached sample format.";
                            TempData["ReopenImport"] = true;
                        }
                        else
                        {
                            using (var context = new VrpDataEntities())
                            {
                                var plan = context.Plans.FirstOrDefault(x => x.Id == id);
                                if (plan == null)
                                {
                                    return RedirectToAction("Index");
                                }

                                // Find table with name "Locations"
                                DataTable locationTable;
                                if (data.Tables.Contains("Locations"))
                                {
                                    locationTable = data.Tables["Locations"];
                                }
                                else
                                {
                                    locationTable = data.Tables[0];
                                }

                                // Clear all old records
                                context.DeleteDistances(id);
                                context.Locations.RemoveRange(context.Locations.Where(x => x.PlanId == id));

                                rowCount = 0;
                                for (var i = 0; i < locationTable.Rows.Count; i++)
                                {
                                    // Process each row
                                    var row = locationTable.Rows[i];

                                    var startLocation = false;
                                    var mustBeVisited = 0;
                                    if (row[7].ToString().ToLower() == "starting location")
                                    {
                                        startLocation = true;
                                    }
                                    else
                                    {
                                        mustBeVisited = VrpCollections.GetIntValue(VrpCollections.MustBeVisitedOptions, row[7].ToString());
                                    }

                                    var location = new Location();
                                    location.PlanId = id;
                                    location.OrderId = row[1].ToString();
                                    location.PostalCode = row[3].ToString();
                                    location.StreetName = row[4].ToString();
                                    location.CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                                    location.CreatedOn = DateTime.Now;
                                    location.CustomerName = row[2].ToString();
                                    location.FromTime = Utilities.ConvertTimeToDouble((DateTime) row[13]);
                                    location.ToTime = Utilities.ConvertTimeToDouble((DateTime) row[14]);
                                    location.Latitude = string.IsNullOrEmpty(row[5].ToString()) ? null : (double?) double.Parse(row[5].ToString());
                                    location.Longitude = string.IsNullOrEmpty(row[6].ToString()) ? null : (double?) double.Parse(row[6].ToString());
                                    location.MustBeVisited = mustBeVisited;
                                    location.PickupAmount = (double) row[9];
                                    location.DeliveryAmount = (double) row[10];
                                    location.Profit = new decimal((double) row[11]);
                                    location.ServiceTime = Utilities.ConvertTimeToDouble((DateTime) row[8]);
                                    location.Starting = startLocation;
                                    location.Type = VrpCollections.GetIntValue(VrpCollections.ProductTypes, row[12].ToString());
                                    location.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                                    location.UpdatedOn = DateTime.Now;

                                    context.Locations.Add(location);
                                    if (!startLocation)
                                    {
                                        ++rowCount;
                                    }
                                }

                                if (rowCount > plan.NumberOfCustomers)
                                {
                                    plan.NumberOfCustomers = rowCount;
                                }

                                context.SaveChanges();
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        if (exc is InvalidCastException || exc is NullReferenceException)
                        {
                            TempData["ErrorMessage"] = $"Import file does not have correct format. Please refer the attached sample format. Error at row '{rowCount}'.<br />More information: {exc.ToString()}";
                        }
                        else
                        {
                            TempData["ErrorMessage"] = "There is error in processing. Please contact to administrator for support with these information: <br />" +
                                                       exc.Message + "<br />" + exc.StackTrace;
                        }
                        TempData["ReopenImport"] = true;
                    }
                    finally
                    {
                        excelReader.Close();
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Import file does not have correct format. Please refer the attached sample format.";
                    TempData["ReopenImport"] = true;
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Import file does not selected.";
                TempData["ReopenImport"] = true;
            }

            return RedirectToAction(
                                    "Locations",
                                    new
                                    {
                                        id = id
                                    });
        }

        public ActionResult GeoLocations(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                ViewBag.Plan = context.Plans.FirstOrDefault(x => x.Id == id);

                if (ViewBag.Plan == null)
                {
                    // Return to first page if there is no plan
                    return RedirectToAction("Index");
                }

                if (TempData["ErrorMessage"] != null)
                {
                    ViewBag.ErrorMessage = TempData["ErrorMessage"];
                }

                ViewBag.StartingLocation = context.Locations.FirstOrDefault(x => x.Starting && x.PlanId == id);

                ViewBag.VisitingLocations = context.Locations.Where(x => !x.Starting && x.PlanId == id).ToList();

                return View();
            }
        }

        [HttpPost]
        [ActionName("GeoLocations")]
        [ValidateAntiForgeryToken]
        public ActionResult GeoLocations_POST(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                var plan = context.Plans.FirstOrDefault(x => x.Id == id);

                if (plan == null)
                {
                    // Return to first page if there is no plan
                    return RedirectToAction("Index");
                }

                ViewBag.Plan = plan;

                var locations = context.Locations.Where(x => x.PlanId == id);

                var starting = locations.FirstOrDefault(x => x.Starting);
                if (starting == null)
                {
                    return RedirectToAction(
                                            "Locations",
                                            new
                                            {
                                                id = id
                                            });
                }

                ViewBag.StartingLocation = starting;

                ViewBag.VisitingLocations = locations.Where(x => !x.Starting && x.PlanId == id).ToList();

                try
                {
                    LocationModel geoLocation = null;

                    // Update starting location
                    starting.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                    starting.UpdatedOn = DateTime.Now;
                    if (string.IsNullOrEmpty(Request["startLatitude"]) || string.IsNullOrEmpty(Request["startLongitude"]))
                    {
                        geoLocation = MapServices.GetGeocodingOfAddress(starting.StreetName);
                        if (geoLocation != null)
                        {
                            starting.Latitude = geoLocation.Latitude;
                            starting.Longitude = geoLocation.Longitude;
                        }
                        else
                        {
                            // Error
                            ViewBag.ErrorMessage = "There is error in retrieving information from Google Map Service. Please check your data and try again after few minutes.";
                            ViewBag.StartingLocation = starting;
                            return View();
                        }
                    }
                    else
                    {
                        starting.Latitude = double.Parse(Request["startLatitude"]);
                        starting.Longitude = double.Parse(Request["startLongitude"]);
                    }

                    // Update other locations
                    for (var i = 1; i <= plan.NumberOfCustomers; i++)
                    {
                        var locId = int.Parse(string.IsNullOrEmpty(Request["id" + i]) ? "0" : Request["id" + i]);
                        var location = locations.FirstOrDefault(x => x.Id == locId);
                        if (location == null)
                        {
                            return RedirectToAction(
                                                    "Locations",
                                                    new
                                                    {
                                                        id = id
                                                    });
                        }

                        location.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        location.UpdatedOn = DateTime.Now;
                        if (string.IsNullOrEmpty(Request["latitude" + i]) || string.IsNullOrEmpty(Request["longitude" + i]))
                        {
                            geoLocation = MapServices.GetGeocodingOfAddress(location.StreetName);
                            if (geoLocation != null)
                            {
                                location.Latitude = geoLocation.Latitude;
                                location.Longitude = geoLocation.Longitude;
                            }
                            else
                            {
                                // Error
                                ViewBag.ErrorMessage = "There is error in retrieving information from Google Map Service. Please check your data and try again after few minutes.";
                                ViewBag.StartingLocation = starting;
                                return View();
                            }
                        }
                        else
                        {
                            location.Latitude = double.Parse(Request["latitude" + i]);
                            location.Longitude = double.Parse(Request["longitude" + i]);
                        }
                    }

                    context.SaveChanges();

                    // Move to Distances page
                    return RedirectToAction(
                                            "Distances",
                                            new
                                            {
                                                id = id
                                            });
                }
                catch (Exception exc)
                {
                    ViewBag.ErrorMessage = "There is error in processing. Please contact to administrator for support with these information: <br />" +
                                           exc.Message + "<br />" + exc.StackTrace;
                }

                return View();
            }
        }

        public ActionResult Distances(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                ViewBag.Plan = context.Plans.FirstOrDefault(x => x.Id == id);

                if (ViewBag.Plan == null)
                {
                    // Return to first page if there is no plan
                    return RedirectToAction("Index");
                }

                if (TempData["ErrorMessage"] != null)
                {
                    ViewBag.ErrorMessage = TempData["ErrorMessage"];
                }

                //var locations = context.Locations.Where(x => x.PlanId == id && x.Starting).Concat(context.Locations.Where(x => x.PlanId == id && !x.Starting)).ToList();
                //ViewBag.VisitingLocations = locations;
                //var distances = context.Distances.Where(x => x.PlanId == id).ToList();
                //var distanceList = new dynamic[locations.Count][];
                //for (var i = 0; i < locations.Count; i++)
                //{
                //    distanceList[i] = new dynamic[locations.Count];
                //    for (var j = 0; j < locations.Count; j++)
                //    {
                //        var dis = distances.FirstOrDefault(x => x.FromId == locations[i].Id && x.ToId == locations[j].Id);
                //        if (dis == null)
                //        {
                //            continue;
                //        }
                //        distanceList[i][j] = new
                //        {
                //            Distance = dis.Distance1 * 1000, // meter
                //            Duration = dis.Duration  // hour
                //        };
                //    }
                //}
                //ViewBag.Distances = distanceList;

                return View();
            }
        }

        [HttpPost]
        [ActionName("Distances")]
        [ValidateAntiForgeryToken]
        public ActionResult Distances_POST(int? id)
        {
            return RedirectToAction(
                                    "Plan",
                                    new
                                    {
                                        id = id
                                    });
        }

        public ActionResult Plan(int? id, bool historyMode = false)
        {
            using (var context = new VrpDataEntities())
            {
                if (historyMode)
                {
                    var history = context.PlanResults.FirstOrDefault(x => x.Id == id);
                    if (history == null)
                    {
                        // Return to first page if there is no plan
                        return RedirectToAction("Index");
                    }

                    ViewBag.RealPlanId = history.PlanId;
                }
                else
                {
                    var plan = context.Plans.FirstOrDefault(x => x.Id == id);
                    if (plan == null)
                    {
                        // Return to first page if there is no plan
                        return RedirectToAction("Index");
                    }

                    ViewBag.RealPlanId = plan.Id;
                }
            }

            if (TempData.ContainsKey("ErrorMessage"))
            {
                ViewBag.IsError = true;
                ViewBag.Message = TempData["ErrorMessage"];
            }

            ViewBag.HistoryMode = historyMode;
            ViewBag.PlanId = id;
            return View();
        }

        [HttpPost]
        public ActionResult ComputePlan(int? id, bool force)
        {
            using (var context = new VrpDataEntities())
            {
                if (string.IsNullOrEmpty(CourierService.GetAccessToken()))
                {
                    var authenticationUrl = WebConfigurationManager.AppSettings["AuthenticationPageUrl"] ?? "";
                    return Redirect(
                                    string.Format(
                                                  authenticationUrl,
                                                  Url.Action(
                                                             "Plan",
                                                             "Home",
                                                             new
                                                             {
                                                                 id = id
                                                             },
                                                             Request.Url?.Scheme)));
                }

                var vehicles = CourierService.GetVehicles();

                var plan = context.Plans.FirstOrDefault(x => x.Id == id);
                if (plan == null)
                {
                    // Return to first page if there is no plan
                    return RedirectToAction("Index");
                }

                ViewBag.Plan = plan;

                var usedVehicles = new List<VehicleViewModel>();
                foreach (var vid in (plan.Vehicles ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (vehicles.Any(x => x.Id.ToString() == vid))
                    {
                        usedVehicles.Add(vehicles.FirstOrDefault(x => x.Id.ToString() == vid));
                    }
                }

                if (usedVehicles.Count == 0)
                {
                    usedVehicles.AddRange(vehicles);
                }

                List<Location> locations;
                int[] ids;
                List<Distance> distances;
                try
                {
                    locations = context.Locations.AsNoTracking().Where(x => x.PlanId == id).ToList();
                    // Limit to the number of customers
                    var starting = locations.First(x => x.Starting);
                    locations = locations.Where(x => !x.Starting).OrderBy(x => x.Id).Take(plan.NumberOfCustomers).ToList();
                    locations.Insert(0, starting);
                    ids = locations.Select(x => x.Id).ToArray();
                    distances = context.Distances.AsNoTracking().Where(x => x.PlanId == id && ids.Contains(x.FromId) && ids.Contains(x.ToId)).ToList();
                }
                catch (Exception exc)
                {
                    return Json(
                                new
                                {
                                    IsError = true,
                                    Code = 255,
                                    Message = "There is error while loading paramters and data of this plan. Please go back and check the data."
                                });
                }

                // Initialize VRP Solver
                var solver = new VrpSolver(plan, usedVehicles, locations, distances);
                var result = solver.Run(force);

                if (result.IsError)
                {
                    return Json(
                                new
                                {
                                    IsError = true,
                                    Code = result.Code,
                                    Message = result.Message,
                                    Infeasibilities = result.Infeasibilities
                                });
                }
                else
                {
                    if (result.Solution == null)
                    {
                        return Json(
                                    new
                                    {
                                        IsError = true,
                                        Message = "There is problem in generating solution. Please try again."
                                    });
                    }

                    var routes = new List<dynamic>();
                    for (var i = 1; i <= solver.NumberOfVehicleTypes; i++)
                    {
                        for (var j = 1; j <= solver.VehicleTypes[i].NumberAvailable; j++)
                        {
                            if (result.Solution.RouteVertexCount[i][j] > 0)
                            {
                                var vId = solver.VehicleTypes[i].Id;
                                var route = new RouteResult()
                                            {
                                                Vehicle = usedVehicles.FirstOrDefault(x => x.Id == vId),
                                                VehicleIndex = j,
                                                Stops = 0,
                                                NetProfit = 0,
                                                Steps = new List<int>(),
                                                Distances = new List<double>(),
                                                DrivingTimes = new List<double>(),
                                                Loads = new List<double>(),
                                                Profits = new List<decimal>(),
                                                Arrivals = new List<double>(),
                                                Departures = new List<double>(),
                                                WorkingTimes = new List<double>(),
                                                Path = ""
                                            };
                                var path = new List<dynamic>();

                                var maxSteps = result.Solution.RouteVertexCount[i][j];

                                double load = 0;
                                for (var k = 1; k <= maxSteps; k++)
                                {
                                    var locIdx = result.Solution.RouteVertices[i][j][k];
                                    var locId = solver.Vertices[locIdx].Id;
                                    var loc = locations.First(x => x.Id == locId);
                                    load += loc.DeliveryAmount;
                                }

                                var baseIdx = 1;
                                route.Stops = maxSteps;
                                route.Steps.Add(baseIdx);
                                route.Distances.Add(0);
                                route.DrivingTimes.Add(0);
                                route.Loads.Add(load);
                                route.Profits.Add(0);
                                route.Arrivals.Add(solver.Vertices[baseIdx].FromTime / 60);
                                route.Departures.Add(solver.Vertices[baseIdx].FromTime / 60);
                                route.WorkingTimes.Add(0);
                                path.Add(
                                         new
                                         {
                                             Latitude = locations.First(x => x.Id == solver.Vertices[baseIdx].Id).Latitude,
                                             Longitude = locations.First(x => x.Id == solver.Vertices[baseIdx].Id).Longitude
                                         });

                                double distance = 0;
                                decimal profit = 0;
                                double drivingTime = 0;
                                double workingTime = 0;
                                for (var k = 1; k <= maxSteps; k++)
                                {
                                    var locIdx = result.Solution.RouteVertices[i][j][k];
                                    var locId = solver.Vertices[locIdx].Id;
                                    var loc = locations.First(x => x.Id == locId);
                                    route.Steps.Add(locIdx);
                                    distance += solver.Distances[route.Steps[k - 1]][locIdx];
                                    route.Distances.Add(distance);
                                    load = load + loc.PickupAmount - loc.DeliveryAmount;
                                    route.Loads.Add(load);
                                    profit += loc.Profit;
                                    route.Profits.Add(profit);
                                    var movingTime = solver.Durations[route.Steps[k - 1]][locIdx] / 60;
                                    drivingTime += movingTime;
                                    route.DrivingTimes.Add(drivingTime);
                                    route.Arrivals.Add(route.Departures[k - 1] + movingTime);
                                    route.Departures.Add(Math.Max(loc.FromTime / 60, route.Arrivals[k]) + loc.ServiceTime);
                                    workingTime += movingTime + loc.ServiceTime;
                                    route.WorkingTimes.Add(workingTime);
                                    path.Add(
                                             new
                                             {
                                                 Latitude = loc.Latitude,
                                                 Longitude = loc.Longitude
                                             });
                                }

                                if (plan.ReturnToDepot)
                                {
                                    ++route.Stops;
                                    var locId = solver.Vertices[baseIdx].Id;
                                    var loc = locations.First(x => x.Id == locId);
                                    route.Steps.Add(baseIdx);
                                    distance += solver.Distances[route.Steps[maxSteps]][baseIdx];
                                    route.Distances.Add(distance);
                                    route.Loads.Add(load);
                                    route.Profits.Add(profit);
                                    var movingTime = solver.Durations[route.Steps[maxSteps]][baseIdx] / 60;
                                    drivingTime += movingTime;
                                    route.DrivingTimes.Add(drivingTime);
                                    route.Arrivals.Add(route.Departures[maxSteps] + movingTime);
                                    route.Departures.Add(Math.Max(loc.FromTime / 60, route.Arrivals[maxSteps + 1]) + loc.ServiceTime);
                                    workingTime += movingTime + loc.ServiceTime;
                                    route.WorkingTimes.Add(workingTime);
                                    path.Add(
                                             new
                                             {
                                                 Latitude = loc.Latitude,
                                                 Longitude = loc.Longitude
                                             });
                                }

                                route.NetProfit = route.Profits[route.Stops] -
                                                  (decimal) route.Distances[route.Stops] * route.Vehicle.CostPerKilometer -
                                                  (route.Stops > 1 ? route.Vehicle.FixedCostPerTrip : 0);
                                route.Path = JsonConvert.SerializeObject(path);
                                routes.Add(route);
                            }
                        }
                    }

                    var finalPlan = new
                                    {
                                        IsError = false,
                                        Code = result.Code,
                                        Message = result.Message,
                                        Infeasibilities = result.Infeasibilities,
                                        Locations = solver.Vertices.Skip(1).Select(
                                                                                   x => new
                                                                                        {
                                                                                            x.Id,
                                                                                            x.CustomerName,
                                                                                            FromTime = x.FromTime / 60.0,
                                                                                            ToTime = x.ToTime / 60.0,
                                                                                            x.PickupAmount,
                                                                                            x.DeliveryAmount
                                                                                        }),
                                        Routes = routes,
                                        Plan = new
                                               {
                                                   plan.NumberOfCustomers,
                                                   plan.NumberOfVehicleTypes,
                                                   plan.ReturnToDepot
                                               }
                                    };

                    try
                    {
                        // Save to DB
                        var planResult = new PlanResult()
                                         {
                                             CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                             CreatedOn = DateTime.Now,
                                             PlanId = plan.Id,
                                             UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                             UpdatedOn = DateTime.Now,
                                             Result = JsonConvert.SerializeObject(finalPlan)
                                         };
                        context.PlanResults.Add(planResult);
                        context.SaveChanges();

                        return Json(finalPlan);
                    }
                    catch (Exception exc)
                    {
                        return Json(
                                    new
                                    {
                                        IsError = true,
                                        Message = "There is error in processing. Please contact to administrator for support with these information: <br />" +
                                                  exc.Message + "<br />" + exc.StackTrace
                                    });
                    }
                }
            }
        }

        [HttpPost]
        public ActionResult LocationList(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                var plan = context.Plans.FirstOrDefault(x => x.Id == id);
                if (plan == null)
                {
                    return RedirectToAction("Index");
                }

                var locations = context.Locations.Where(x => !x.Starting && x.PlanId == id)
                                       .ToList()
                                       .Select(
                                               x => new
                                                    {
                                                        x.PostalCode,
                                                        x.StreetName,
                                                        x.Latitude,
                                                        x.Longitude,
                                                        x.CustomerName,
                                                        FromTime = Utilities.ConvertDoubleToHour(x.FromTime),
                                                        ToTime = Utilities.ConvertDoubleToHour(x.ToTime),
                                                        ServiceTime = Utilities.ConvertDoubleToHour(x.ServiceTime),
                                                        x.MustBeVisited,
                                                        x.Type,
                                                        x.PickupAmount,
                                                        x.DeliveryAmount,
                                                        x.Profit,
                                                        x.Id
                                                    });

                return Json(locations);
            }
        }

        [HttpPost]
        public ActionResult GetHistory(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                var history = context.PlanResults.FirstOrDefault(x => x.Id == id);
                if (history == null)
                {
                    TempData["ErrorMessage"] = "History not found";
                    return RedirectToAction("History");
                }

                return Content(history.Result, "application/json");
            }
        }

        #endregion

        [HttpPost]
        public ActionResult ExportPlan(int? id)
        {
            if (!id.HasValue)
            {
                return new EmptyResult();
            }

            using (var context = new VrpDataEntities())
            {
                var plan = context.Plans.FirstOrDefault(x => x.Id == id);
                if (plan == null)
                {
                    // Return empty if there is no plan
                    return new EmptyResult();
                }

                var history = context.PlanResults
                                     .Where(x => x.PlanId == plan.Id)
                                     .OrderByDescending(x => x.UpdatedOn)
                                     .FirstOrDefault();
                if (history == null)
                {
                    // If there is no result then return to Plan page
                    TempData["ErrorMessage"] = "You don't run the computation for this plan yet. Please run computation before export.";
                    return RedirectToAction(
                                            "Plan",
                                            new
                                            {
                                                id = id
                                            });
                }

                var locations = context.Locations.Where(x => x.PlanId == plan.Id && x.Starting)
                                       .Select(
                                               x => new
                                                    {
                                                        OrderId = x.OrderId,
                                                        CustomerName = x.CustomerName,
                                                        Type = x.PickupAmount > 0 ? 0 : (x.DeliveryAmount > 0 ? 1 : 2)
                                                    })
                                       .ToList()
                                       .Concat(
                                               context.Locations.Where(x => x.PlanId == plan.Id && !x.Starting)
                                                      .OrderBy(x => x.Id).Take(plan.NumberOfCustomers)
                                                      .Select(
                                                              x => new
                                                                   {
                                                                       OrderId = x.OrderId,
                                                                       CustomerName = x.CustomerName,
                                                                       Type = x.PickupAmount > 0 ? 0 : (x.DeliveryAmount > 0 ? 1 : 2)
                                                                   })
                                                      .ToList()).ToList();

                var routes = JsonConvert.DeserializeObject<dynamic>(history.Result).Routes;

                // Export to Excel file
                var folderPath = Server.MapPath("~/Temp/");
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                var fileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".xlsx";
                var filePath = folderPath + fileName;
                var outputFile = new FileInfo(filePath);

                var package = new ExcelPackage(outputFile);
                // Add the sheet
                var ws = package.Workbook.Worksheets.Add("Routes");
                ws.View.ShowGridLines = false;

                // Headers
                ws.Cells["A1"].Value = "Vehicle Id";
                ws.Cells["B1"].Value = "Route";
                ws.Cells["A1:B1"].Style.Font.Bold = true;

                // Data
                var i = 0;
                foreach (var item in routes)
                {
                    var route = new
                                {
                                    Stops = item.Stops,
                                    NetProfit = item.NetProfit,
                                    Steps = item.Steps,
                                    Distances = item.Distances,
                                    Loads = item.Loads,
                                    Profits = item.Profits,
                                    DrivingTimes = item.DrivingTimes,
                                    Arrivals = item.Arrivals,
                                    Departures = item.Departures,
                                    WorkingTimes = item.WorkingTimes,
                                    Path = item.Path,
                                };

                    var vehicleId = item.Vehicle.Id;

                    ws.Cells[i + 2, 1].Value = vehicleId;
                    ws.Cells[i + 2, 2].Value = JsonConvert.SerializeObject(
                                                                           new
                                                                           {
                                                                               Locations = locations,
                                                                               Route = route
                                                                           });
                    ++i;
                }

                package.Save();

                return File(filePath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
        }

        public ActionResult History(int? page, string start, string end)
        {
            using (var context = new VrpDataEntities())
            {
                context.Database.CommandTimeout = 3600;

                if (!page.HasValue)
                {
                    page = 1;
                }

                var username = User.Identity.Name.ToLower();

                var itemsPerPage = 10;
                ViewBag.ItemsPerPage = itemsPerPage;
                ViewBag.Page = page.Value;

                var startTime = DateTime.MinValue;
                var endTime = DateTime.MaxValue;

                if (!string.IsNullOrEmpty(start))
                {
                    DateTime.TryParseExact(start, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out startTime);
                }
                if (!string.IsNullOrEmpty(end))
                {
                    DateTime.TryParseExact(end, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endTime);
                }

                ViewBag.ItemsCount = context.PlanResults.Where(
                                                               x => x.CreatedBy.ToLower() == username && !x.IsDeleted &&
                                                                    DbFunctions.TruncateTime(x.UpdatedOn) >= startTime && DbFunctions.TruncateTime(x.UpdatedOn) <= endTime)
                                            .GroupBy(x => x.PlanId).Count();

                var history = context.PlanResults.Where(
                                                        x => x.CreatedBy.ToLower() == username && !x.IsDeleted &&
                                                             DbFunctions.TruncateTime(x.UpdatedOn) >= startTime && DbFunctions.TruncateTime(x.UpdatedOn) <= endTime)
                                     .GroupBy(x => x.PlanId)
                                     .Select(
                                             g => new HistoryModel()
                                                  {
                                                      Id = g.OrderByDescending(x => x.UpdatedOn).Select(x => x.Id).FirstOrDefault(),
                                                      PlanId = g.Key,
                                                      Plan = context.Plans.FirstOrDefault(x => x.Id == g.Key),
                                                      Result = g.OrderByDescending(x => x.UpdatedOn).Select(x => x.Result).FirstOrDefault(),
                                                      UpdatedOn = g.OrderByDescending(x => x.UpdatedOn).Select(x => x.UpdatedOn).FirstOrDefault()
                                                  })
                                     .OrderByDescending(x => x.UpdatedOn);

                if (Request["DeleteAll"] != null && Request["DeleteAll"].ToLower() == "delete all")
                {
                    var planIds = history.Select(x => x.PlanId).ToList();
                    foreach (var item in planIds)
                    {
                        context.DeleteDistances(item);
                        context.Locations.RemoveRange(context.Locations.Where(x => x.PlanId == item));
                        context.PlanResults.RemoveRange(context.PlanResults.Where(x => x.PlanId == item));
                        context.Plans.Remove(context.Plans.FirstOrDefault(x => x.Id == item));
                    }

                    context.SaveChanges();
                    return RedirectToAction("History");
                }
                else
                {
                    return View(history.Skip((page.Value - 1) * itemsPerPage).Take(itemsPerPage).ToList());
                }
            }
        }

        public ActionResult Report(int? id)
        {
            using (var context = new VrpDataEntities())
            {
                var plan = context.Plans.FirstOrDefault(x => x.Id == id);
                if (plan == null)
                {
                    // Return to first page if there is no plan
                    return RedirectToAction("Index");
                }

                var history = context.PlanResults
                                     .Where(x => x.PlanId == plan.Id)
                                     .OrderByDescending(x => x.UpdatedOn)
                                     .FirstOrDefault();
                if (history == null)
                {
                    // If there is no result then return to Plan page
                    TempData["ErrorMessage"] = "You don't run the computation for this plan yet. Please run computation before view report.";
                    return RedirectToAction(
                                            "Plan",
                                            new
                                            {
                                                id = id
                                            });
                }

                // Render report with this history
                ViewBag.Locations = context.Locations.Where(x => x.PlanId == plan.Id).ToList();
                ViewBag.History = history;
                ViewBag.Result = JsonConvert.DeserializeObject(history.Result);
            }

            return View();
        }

        #region Settings section

        public ActionResult Settings()
        {
            if (TempData.ContainsKey("ErrorMessage"))
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }
            if (TempData.ContainsKey("Message"))
            {
                ViewBag.ErrorMessage = TempData["Message"];
            }

            using (var context = new VrpDataEntities())
            {
                ViewBag.Settings = context.Settings.ToList();
                return View();
            }
        }

        [HttpPost]
        [ActionName("Settings")]
        [ValidateAntiForgeryToken]
        public ActionResult Settings_POST()
        {
            using (var context = new VrpDataEntities())
            {
                // Save settings first
                foreach (var paramKey in Request.Form.AllKeys)
                {
                    if (paramKey.StartsWith("settingName_"))
                    {
                        // Get index of this param
                        var index = paramKey.Substring("settingName_".Length);
                        var name = Request.Form[paramKey];
                        var value = Request.Form["setting_" + index];

                        var setting = context.Settings.FirstOrDefault(x => x.Name.ToLower() == name.ToLower());
                        if (setting == null)
                        {
                            setting = new Setting()
                                      {
                                          Name = name,
                                          CreatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "",
                                          CreatedOn = DateTime.Now
                                      };
                            context.Settings.Add(setting);
                        }
                        setting.Value = value;
                        setting.UpdatedBy = User.Identity.IsAuthenticated ? User.Identity.Name : "";
                        setting.UpdatedOn = DateTime.Now;
                        context.SaveChanges();

                        // Clear cache
                        SettingProvider.ClearCache();
                    }
                }

                return RedirectToAction("Settings");
            }
        }

        #endregion

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (Request.IsAuthenticated && !string.IsNullOrEmpty(CourierService.GetAccessToken()))
            {
                return RedirectToAction("Index", "Home");
            }

            var authenticationUrl = WebConfigurationManager.AppSettings["AuthenticationPageUrl"] ?? "";
            return Redirect(string.Format(authenticationUrl, Url.Action("Index", "Home", null, Request.Url?.Scheme)));
        }

        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index");
        }
    }
}
