﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;

namespace Vehicle_Routing_Planer
{
    public class DistancesHub : Hub
    {
        public void Start(int planId)
        {
            using (var context = new VrpDataEntities())
            {
                var plan = context.Plans.FirstOrDefault(x => x.Id == planId);

                if (plan == null)
                {
                    // Return to first page if there is no plan
                    Clients.Caller.notFound();
                    return;
                }

                var locations = context.Locations.Where(x => x.PlanId == planId && x.Starting).Concat(context.Locations.Where(x => x.PlanId == planId && !x.Starting)).ToList();

                try
                {
                    var tries = 0;

                    var total = locations.Count * locations.Count;
                    var count = 0;

                    // Save distances
                    var distanceList = new dynamic[locations.Count][];
                    if (plan.ComputationMethod == 5)
                    {
                        for (var i = 0; i < locations.Count; i++)
                        {
                            distanceList[i] = new dynamic[locations.Count];
                        }

                        // Query Google map to get distance
                        const int MaxElements = 10;
                        for (var i = 0; i < locations.Count; i += MaxElements)
                        {
                            var origins = locations.Skip(i).Take(MaxElements).Select(x => ($"{x.Latitude},{x.Longitude}")).ToArray();
                            for (var j = 0; j < locations.Count; j += MaxElements)
                            {
                                var destinations = locations.Skip(j).Take(MaxElements).Select(x => ($"{x.Latitude},{x.Longitude}")).ToArray();

                                var result = MapServices.GetDistances(origins, destinations);
                                if (result == null)
                                {
                                    Clients.Caller.error("Cannot obtain distances from Google Maps service correctly. Please try again.");
                                    return;
                                }

                                for (var a = i; a < Math.Min(i + MaxElements, locations.Count); a++)
                                {
                                    for (var b = j; b < Math.Min(j + MaxElements, locations.Count); b++)
                                    {
                                        distanceList[a][b] = new
                                        {
                                            Distance = result[a - i][b - j].Distance, // meter
                                            Duration = result[a - i][b - j].Duration / 60.0 / 60.0  // hour
                                        };
                                    }
                                }

                                // Update progress
                                count += (MaxElements * MaxElements);
                                if (count > total)
                                {
                                    count = total;
                                }
                                Clients.Caller.updateProgress((float) count / total);
                            }
                        }
                    }
                    else if (plan.ComputationMethod == 6)
                    {
                        var i = 0;
                        for (i = 0; i < locations.Count; i++)
                        {
                            distanceList[i] = new dynamic[locations.Count];
                        }

                        // Query Bing map to get distance
                        const int MaxWayPoints = 23;
                        var sequences = MapServices.GenerateSequenceWaypoints(locations.Count);

                        var numberOfArcs = locations.Count * (locations.Count - 1);
                        i = 0;
                        do
                        {
                            var j = 0;
                            do
                            {
                                ++j;
                            } while (i + j < numberOfArcs && j <= MaxWayPoints);
                            var waypoints = new List<Location>();
                            for (var idx = i; idx <= i + j; ++idx)
                            {
                                waypoints.Add(locations[sequences[idx]]);
                            }
                            var result = MapServices.CalculateDistances(waypoints);

                            if (result == null)
                            {
                                Clients.Caller.error("Cannot obtain distances from Bing Maps service correctly. Please try again.");
                                return;
                            }

                            j = i;
                            do
                            {
                                var fromIndex = sequences[j];
                                var toIndex = sequences[j + 1];

                                var resourceSet = result.resourceSets[0];
                                var resource = resourceSet.resources[0];
                                var locationResult = resource.routeLegs[j - i];

                                distanceList[fromIndex][toIndex] = new
                                {
                                    Distance = locationResult.travelDistance * 1000.0, // meter
                                    Duration = locationResult.travelDuration / 60.0 / 60.0 // hour
                                };

                                ++j;
                            } while (j < numberOfArcs && j < i + MaxWayPoints);

                            // Wait a little bit to avoid flood Map system
                            System.Threading.Thread.Sleep(100);

                            i += MaxWayPoints;

                            // Update progress
                            if (i > total)
                            {
                                i = total;
                            }
                            Clients.Caller.updateProgress((float) i / total);
                        } while (i < numberOfArcs);

                        for (i = 0; i < locations.Count; i++)
                        {
                            distanceList[i][i] = new
                            {
                                Distance = 0,
                                Duration = 0
                            };
                        }
                    }
                    else if (plan.ComputationMethod == 7)
                    {
                        for (var i = 0; i < locations.Count; i++)
                        {
                            distanceList[i] = new dynamic[locations.Count];
                            for (var j = 0; j < locations.Count; j++)
                            {
                                // Query OSRM map to get distance
                                var result = MapServices.CalculateDistancesOSRM(locations[i], locations[j]);
                                if (result == null)
                                {
                                    Clients.Caller.error("Cannot obtain distances from OSRM Maps service correctly. Please try again.");
                                    return;
                                }

                                distanceList[i][j] = new
                                {
                                    Distance = result.route_summary.total_distance, // meter
                                    Duration = result.route_summary.total_time / 60.0 / 60.0 // hours
                                };

                                // Update progress
                                ++count;
                                Clients.Caller.updateProgress((float) count / total);
                            }
                        }
                    }
                    else
                    {
                        // Generate distance by method
                        for (var i = 0; i < locations.Count; i++)
                        {
                            distanceList[i] = new dynamic[locations.Count];
                            for (var j = 0; j < locations.Count; j++)
                            {
                                double distance = 0;
                                if (locations[i].Latitude.HasValue && locations[i].Longitude.HasValue &&
                                    locations[j].Latitude.HasValue && locations[j].Longitude.HasValue)
                                {
                                    if (plan.ComputationMethod == 1)
                                    {
                                        distance = Utilities.EuclidianDistance(locations[i].Latitude.Value,
                                            locations[i].Longitude.Value, locations[j].Latitude.Value,
                                            locations[j].Longitude.Value);
                                    }
                                    else if (plan.ComputationMethod == 2)
                                    {
                                        distance =
                                            Math.Round(Utilities.EuclidianDistance(locations[i].Latitude.Value,
                                                locations[i].Longitude.Value, locations[j].Latitude.Value,
                                                locations[j].Longitude.Value));
                                    }
                                    else if (plan.ComputationMethod == 3)
                                    {
                                        distance = Utilities.GeosedicDistanceApproximation(locations[i].Latitude.Value,
                                            locations[i].Longitude.Value, locations[j].Latitude.Value,
                                            locations[j].Longitude.Value);
                                    }
                                    else if (plan.ComputationMethod == 4)
                                    {
                                        distance = Utilities.RectilinearDistance(locations[i].Latitude.Value,
                                            locations[i].Longitude.Value, locations[j].Latitude.Value,
                                            locations[j].Longitude.Value);
                                    }
                                }
                                distanceList[i][j] = new
                                {
                                    Distance = distance * 1000, // meter
                                    Duration = distance / plan.AverageVehicleSpeed // hour
                                };

                                // Update progress
                                ++count;
                                Clients.Caller.updateProgress((float) count / total);
                            }
                        }
                    }

                    Clients.Caller.updateSaveProgress(0);

                    // Clear old records
                    context.DeleteDistances(plan.Id);
                    for (var i = 0; i < distanceList.Length; i++)
                    {
                        var fromId = locations[i].Id;
                        for (var j = 0; j < distanceList[i].Length; j++)
                        {
                            var toId = locations[j].Id;
                            context.InsertDistance(fromId, toId, plan.Id, (double) distanceList[i][j].Distance / 1000.0, (double) distanceList[i][j].Duration);
                        }

                        // Update progress
                        Clients.Caller.updateSaveProgress((float) (i + 1) / distanceList.Length);
                    }

                    Clients.Caller.success();
                }
                catch (Exception exc)
                {
                    Clients.Caller.error("There is error in processing. Please contact to administrator for support with these information: <br />" +
                            exc.Message + "<br />" + exc.StackTrace);
                }
            }
        }
    }
}