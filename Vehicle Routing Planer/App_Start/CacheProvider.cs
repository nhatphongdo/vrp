﻿using System;
using System.Runtime.Caching;

namespace Vehicle_Routing_Planer.App_Start
{
    public static class InMemoryCache
    {
        public static T GetOrSet<T>(string cacheKey, Func<T> getItemCallback)
        {
            T item;
            try
            {
                item = (T) MemoryCache.Default.Get(cacheKey);
            }
            catch (Exception exc)
            {
                item = default(T);
            }
            
            if (item != null)
            {
                return item;
            }

            item = getItemCallback();
            MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(60));
            return item;
        }

        public static void ClearCache()
        {
            MemoryCache.Default.Trim(100);
        }
    }
}
