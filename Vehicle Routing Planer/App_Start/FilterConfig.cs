﻿using System.Web;
using System.Web.Mvc;

namespace Vehicle_Routing_Planer
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
