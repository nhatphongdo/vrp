﻿using System;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Microsoft.Owin.Security.OAuth;

namespace Vehicle_Routing_Planer
{
    public partial class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions
        {
            get; private set;
        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            var cookieConfiguration = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Home/Login"),
                Provider = new CookieAuthenticationProvider
                {
                }
            };
            if (!string.IsNullOrEmpty(FormsAuthentication.CookieDomain))
            {
                cookieConfiguration.CookieDomain = FormsAuthentication.CookieDomain;
            }
            app.UseCookieAuthentication(cookieConfiguration);
        }
    }
}