﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using Vehicle_Routing_Planer.Models;

namespace Vehicle_Routing_Planer
{
    public static class CourierService
    {
        public static string GetAccessToken()
        {
            var token = HttpContext.Current.Request.QueryString["token"];
            if (string.IsNullOrEmpty(token))
            {
                token = HttpContext.Current.Request.Cookies["CdsAccessToken"]?.Value ?? "";
            }

            return token;
        }

        public static string GetServiceResult(string url)
        {
            var uri = new Uri(url);
            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.Headers.Add("Authorization", "Bearer " + GetAccessToken());
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = 0;
            //request.GetRequestStream().Write(data, 0, data.Length);

            try
            {
                var response = (HttpWebResponse) request.GetResponse();
                var stream = response.GetResponseStream();
                if (stream == null)
                {
                    return string.Empty;
                }
                var reader = new StreamReader(stream);
                var output = reader.ReadToEnd();
                response.Close();

                return output;
            }
            catch (Exception exc)
            {
                return "";
            }
        }

        public static List<VehicleViewModel> GetVehicles()
        {
            if (string.IsNullOrEmpty(SettingProvider.CourierSystemServiceBase))
            {
                return new List<VehicleViewModel>();
            }

            var response = GetServiceResult(SettingProvider.CourierSystemServiceBase.TrimEnd('/') + "/api/vehicles/list");
            if (string.IsNullOrEmpty(response))
            {
                return new List<VehicleViewModel>();
            }
            return JsonConvert.DeserializeObject<List<VehicleViewModel>>(response);
        }

        public static dynamic GetZipCode(string postalCode)
        {
            if (string.IsNullOrEmpty(SettingProvider.CourierSystemServiceBase))
            {
                return null;
            }

            var response = GetServiceResult(SettingProvider.CourierSystemServiceBase.TrimEnd('/') + "/api/locations/checkZipCode/" + postalCode);
            if (string.IsNullOrEmpty(response))
            {
                return null;
            }
            return JsonConvert.DeserializeObject<dynamic>(response);
        }
    }
}
