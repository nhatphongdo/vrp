﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vehicle_Routing_Planer.Models;

namespace Vehicle_Routing_Planer
{
    /// <summary>
    /// Solver for VRP problem
    /// </summary>
    public class VrpSolver
    {
        public const double Epsilon = 0.0001;

        #region Public properties

        public bool IsOpenVrp
        {
            get; set;
        }

        public bool IsSoftTimeWindows
        {
            get; set;
        }

        public bool WaitingIsWorking
        {
            get; set;
        }

        public int NumberOfCustomers => num_customers;

        public int NumberOfDepots => num_depots;

        public int NumberOfLocations => num_locations;

        public Location[] Vertices => _vertices;

        public int NumberOfVehicleTypes => num_vehicle_types;

        public VehicleTypeData[] VehicleTypes => vehicle_types;

        public double[][] Distances => _distances;

        public double[][] Durations => _durations;

        public decimal Penalty => penalty;

        #endregion

        #region Private properties

        private int num_depots;
        private int num_customers;
        private int num_locations;
        private Location[] _vertices;
        private int num_vehicle_types;
        private VehicleTypeData[] vehicle_types;
        private double[][] _distances;
        private double[][] _durations;
        private decimal penalty;
        private int _cpuTimeLimit;
        private double _lnsMinimumRemovalRate;
        private double _lnsMaximumRemovalRate;
        private double _lnsCandidateListSize;

        #endregion

        public VrpSolver(Plan plan, List<VehicleViewModel> vehicles, List<Location> locations, List<Distance> distances)
        {
            // Init attributes and data
            IsOpenVrp = plan.ReturnToDepot == false;
            IsSoftTimeWindows = (plan.TimeWindowType == 0);
            WaitingIsWorking = plan.IsIncludingWaitingTime;

            _cpuTimeLimit = SettingProvider.CpuTimeLimit;
            if (_cpuTimeLimit <= 0)
            {
                _cpuTimeLimit = 60;
            }

            _lnsMinimumRemovalRate = 0.1;
            _lnsMaximumRemovalRate = 0.4;
            _lnsCandidateListSize = 3;

            // Get vertices' data
            num_depots = locations.Count(x => x.Starting);
            num_customers = plan.NumberOfCustomers;
            num_locations = num_customers + num_depots;
            _vertices = new Location[num_locations + 1];
            var i = 0;
            foreach (var loc in locations.Where(x => x.Starting).OrderBy(x => x.Id).Select(location => new Location()
            {
                Id = location.Id,
                CustomerName = location.CustomerName,
                MustBeVisited = location.MustBeVisited,
                ServiceTime = location.ServiceTime * 60,
                Profit = location.Profit,
                FromTime = location.FromTime * 60,
                ToTime = location.ToTime * 60,
                PickupAmount = location.PickupAmount,
                DeliveryAmount = location.DeliveryAmount
            }))
            {
                ++i;
                _vertices[i] = loc;
                if (_vertices[i].ToTime == 0)
                {
                    _vertices[i].ToTime = 1439;
                }
            }
            foreach (var loc in locations.Where(x => !x.Starting).OrderBy(x => x.Id).Select(location => new Location()
            {
                Id = location.Id,
                CustomerName = location.CustomerName,
                MustBeVisited = location.MustBeVisited,
                ServiceTime = location.ServiceTime * 60,
                Profit = location.Profit,
                FromTime = location.FromTime * 60,
                ToTime = location.ToTime * 60,
                PickupAmount = location.PickupAmount,
                DeliveryAmount = location.DeliveryAmount
            }))
            {
                ++i;
                _vertices[i] = loc;
                if (_vertices[i].ToTime == 0)
                {
                    _vertices[i].ToTime = 1439;
                }
            }

            // Get vehicles' data
            var base_vehicle_types = Math.Min(plan.NumberOfVehicleTypes, vehicles.Count);
            num_vehicle_types = base_vehicle_types * num_depots;
            vehicle_types = new VehicleTypeData[num_vehicle_types + 1];
            var k = 1;
            for (i = 1; i <= num_depots; i++)
            {
                for (var j = 1; j <= base_vehicle_types; j++)
                {
                    var vehicle = new VehicleTypeData()
                    {
                        NumberAvailable = 1,
                        Capacity = vehicles[j - 1].Capacity,
                        Id = vehicles[j - 1].Id,
                        CostPerUnitDistance = vehicles[j - 1].CostPerKilometer,
                        FixedCostPerTrip = vehicles[j - 1].FixedCostPerTrip,
                        WorkStartTime = vehicles[j - 1].WorkStartTime * 60,
                        DistanceLimit = vehicles[j - 1].DistanceLimit > 0 ? vehicles[j - 1].DistanceLimit : 1, // Avoid divided by zero
                        DrivingTimeLimit = vehicles[j - 1].DrivingTimeLimit * 60,
                        WorkingTimeLimit = vehicles[j - 1].WorkingTimeLimit * 60,
                        BaseId = i,
                        TypeId = j
                    };
                    vehicle_types[k] = vehicle;
                    ++k;
                }
            }

            // Get distance data
            _distances = new double[num_locations + 1][];
            _durations = new double[num_locations + 1][];
            for (i = 1; i <= num_locations; i++)
            {
                _distances[i] = new double[num_locations + 1];
                _durations[i] = new double[num_locations + 1];

                for (var j = 1; j <= num_locations; j++)
                {
                    _distances[i][j] = GetDistance(distances, _vertices[i].Id, _vertices[j].Id);
                    _durations[i][j] = GetDuration(distances, _vertices[i].Id, _vertices[j].Id) * 60;
                }
            }

            // Determine penalty
            double distanceTotal = 0;
            for (i = 1; i <= num_locations; i++)
            {
                for (var j = 1; j <= num_locations; j++)
                {
                    distanceTotal += _distances[i][j];
                }
            }

            decimal costTotal = 0;
            for (i = 1; i <= num_vehicle_types; i++)
            {
                for (var j = 1; j <= VehicleTypes[i].NumberAvailable; j++)
                {
                    costTotal += VehicleTypes[i].CostPerUnitDistance;
                }
            }

            if (costTotal < 1)
            {
                costTotal = 1;
            }

            penalty = (decimal) distanceTotal * costTotal;

            for (i = 1; i <= num_vehicle_types; i++)
            {
                for (var j = 1; j <= VehicleTypes[i].NumberAvailable; j++)
                {
                    penalty += VehicleTypes[i].FixedCostPerTrip;
                }
            }
        }

        private static double GetDistance(List<Distance> distances, int fromId, int toId)
        {
            return distances.First(x => x.FromId == fromId && x.ToId == toId).Distance1;
        }

        private static double GetDuration(List<Distance> distances, int fromId, int toId)
        {
            return distances.First(x => x.FromId == fromId && x.ToId == toId).Duration;
        }

        public VrpResult Run(bool ignoreInfeasibilities = false)
        {
            var candidateList = new Candidate[num_customers + 1];

            var incumbent = new Solution(this);
            incumbent.Evaluate();

            var bestSolution = new Solution(this);
            bestSolution.AssignFrom(incumbent);

            var feasibilties = FeasibilityCheckData();
            if (feasibilties.Count > 0 && !ignoreInfeasibilities)
            {
                // There are feasibilities
                return new VrpResult()
                {
                    IsError = true,
                    Message = $"Infeasibilities detected.",
                    Code = 1,
                    Infeasibilities = feasibilties
                };
            }

            var startTime = DateTime.Now;
            var endTime = DateTime.Now;

            var random = new Random((int) DateTime.Now.Ticks);

            int vertex_to_be_added;
            var vehicle_type_to_add_to = 0;
            var vehicle_id_to_add_to = 0;
            var position_to_add_to = 0;

            decimal maxNetProfit;
            double minTotalDistance;
            int maxMandatory;

            int k;

            // Constructive phase
            do
            {
                vertex_to_be_added = -1;
                maxNetProfit = incumbent.NetProfit - penalty;
                minTotalDistance = incumbent.TotalDistance;
                maxMandatory = 0;

                for (var vertex = num_depots + 1; vertex <= num_locations; vertex++)
                {
                    if (_vertices[vertex].MustBeVisited >= 0 && incumbent.VerticesVisited[vertex] == 0)
                    {
                        for (var i = 1; i <= num_vehicle_types; i++)
                        {
                            for (var j = 1; j <= vehicle_types[i].NumberAvailable; j++)
                            {
                                if (j == 1)
                                {
                                    for (k = 1; k <= incumbent.RouteVertexCount[i][j] + 1; k++)
                                    {
                                        incumbent.AddVertex(vertex, i, j, k);

                                        if (_vertices[vertex].MustBeVisited > maxMandatory ||
                                            (_vertices[vertex].MustBeVisited >= maxMandatory && incumbent.NetProfit > maxNetProfit + (decimal) Epsilon) ||
                                            (_vertices[vertex].MustBeVisited >= maxMandatory && incumbent.NetProfit > maxNetProfit - (decimal) Epsilon && incumbent.TotalDistance < minTotalDistance - Epsilon))
                                        {
                                            maxNetProfit = incumbent.NetProfit;
                                            minTotalDistance = incumbent.TotalDistance;
                                            maxMandatory = _vertices[vertex].MustBeVisited;
                                            vertex_to_be_added = vertex;
                                            vehicle_type_to_add_to = i;
                                            vehicle_id_to_add_to = j;
                                            position_to_add_to = k;
                                        }

                                        incumbent.RemoveVertex(i, j, k);
                                    }
                                }
                                else if (incumbent.RouteVertexCount[i][j - 1] > 0)
                                {
                                    for (k = 1; k <= incumbent.RouteVertexCount[i][j] + 1; k++)
                                    {
                                        incumbent.AddVertex(vertex, i, j, k);

                                        if (_vertices[vertex].MustBeVisited > maxMandatory ||
                                            (_vertices[vertex].MustBeVisited >= maxMandatory && incumbent.NetProfit > maxNetProfit + (decimal) Epsilon) ||
                                            (_vertices[vertex].MustBeVisited >= maxMandatory && incumbent.NetProfit > maxNetProfit - (decimal) Epsilon && incumbent.TotalDistance < minTotalDistance - Epsilon))
                                        {
                                            maxNetProfit = incumbent.NetProfit;
                                            minTotalDistance = incumbent.TotalDistance;
                                            maxMandatory = _vertices[vertex].MustBeVisited;
                                            vertex_to_be_added = vertex;
                                            vehicle_type_to_add_to = i;
                                            vehicle_id_to_add_to = j;
                                            position_to_add_to = k;
                                        }

                                        incumbent.RemoveVertex(i, j, k);
                                    }
                                }
                            }
                        }
                    }
                }

                if (vertex_to_be_added != -1)
                {
                    incumbent.AddVertex(vertex_to_be_added, vehicle_type_to_add_to, vehicle_id_to_add_to, position_to_add_to);
                }

                incumbent.Evaluate();

                if ((incumbent.Feasible && !bestSolution.Feasible) ||
                    (incumbent.CoversMandatoryLocations && !bestSolution.CoversMandatoryLocations) ||
                    (incumbent.Feasible == bestSolution.Feasible && incumbent.NetProfit > bestSolution.NetProfit + (decimal) Epsilon))
                {
                    bestSolution.AssignFrom(incumbent);
                }

            } while (vertex_to_be_added != -1);

            if (Math.Abs(bestSolution.NetProfit - incumbent.NetProfit) > (decimal) Epsilon)
            {
                bestSolution.Improve();
                incumbent.Improve();
            }
            else
            {
                bestSolution.Improve();
            }

            if ((incumbent.Feasible && !bestSolution.Feasible) ||
                (incumbent.CoversMandatoryLocations && !bestSolution.CoversMandatoryLocations) ||
                (incumbent.Feasible == bestSolution.Feasible && incumbent.NetProfit > bestSolution.NetProfit + (decimal) Epsilon))
            {
                bestSolution.AssignFrom(incumbent);
            }

            // Improvement phase
            var iteration = 0;
            do
            {
                var removalRate = _lnsMinimumRemovalRate + (_lnsMaximumRemovalRate - _lnsMinimumRemovalRate) * random.NextDouble();

                // Randomly remove locations
                for (var i = 1; i <= num_vehicle_types; i++)
                {
                    for (var j = 1; j <= vehicle_types[i].NumberAvailable; j++)
                    {
                        for (k = 1; k <= incumbent.RouteVertexCount[i][j]; k++)
                        {
                            if (k <= incumbent.RouteVertexCount[i][j] && random.NextDouble() < removalRate)
                            {
                                incumbent.RemoveVertex(i, j, k);
                            }
                        }
                    }
                }

                incumbent.Improve();

                var candidateCount = 0;
                do
                {
                    candidateCount = 0;

                    for (var vertex = num_depots + 1; vertex <= num_locations; vertex++)
                    {
                        if (_vertices[vertex].MustBeVisited >= 0 && incumbent.VerticesVisited[vertex] == 0)
                        {
                            maxMandatory = 0;
                            maxNetProfit = incumbent.NetProfit - Penalty;
                            vehicle_type_to_add_to = -1;

                            for (var i = 1; i <= num_vehicle_types; i++)
                            {
                                for (var j = 1; j <= vehicle_types[i].NumberAvailable; j++)
                                {
                                    for (k = 1; k <= incumbent.RouteVertexCount[i][j] + 1; k++)
                                    {
                                        incumbent.AddVertex(vertex, i, j, k);

                                        if (_vertices[vertex].MustBeVisited > maxMandatory ||
                                            (_vertices[vertex].MustBeVisited >= maxMandatory && incumbent.NetProfit > maxNetProfit + (decimal) Epsilon) ||
                                            (_vertices[vertex].MustBeVisited >= maxMandatory && incumbent.NetProfit > maxNetProfit - (decimal) Epsilon && incumbent.TotalDistance < minTotalDistance - Epsilon))
                                        {
                                            maxNetProfit = incumbent.NetProfit;
                                            minTotalDistance = incumbent.TotalDistance;
                                            maxMandatory = _vertices[vertex].MustBeVisited;
                                            vehicle_type_to_add_to = i;
                                            vehicle_id_to_add_to = j;
                                            position_to_add_to = k;
                                        }

                                        incumbent.RemoveVertex(i, j, k);
                                    }
                                }
                            }

                            if (vehicle_type_to_add_to != -1)
                            {
                                ++candidateCount;
                                candidateList[candidateCount].NetProfit = maxNetProfit;
                                candidateList[candidateCount].TotalDistance = minTotalDistance;
                                candidateList[candidateCount].Mandatory = maxMandatory;
                                candidateList[candidateCount].VertexToBeAdded = vertex;
                                candidateList[candidateCount].VehicleTypeIndex = vehicle_type_to_add_to;
                                candidateList[candidateCount].VehicleId = vehicle_id_to_add_to;
                                candidateList[candidateCount].Position = position_to_add_to;
                            }
                        }
                    }

                    if (candidateCount > 0)
                    {
                        var finalCandidateCount = _lnsCandidateListSize;
                        if (finalCandidateCount > candidateCount)
                        {
                            finalCandidateCount = candidateCount;
                        }

                        for (var i = 1; i <= finalCandidateCount; i++)
                        {
                            k = -1;
                            maxMandatory = candidateList[i].Mandatory;
                            maxNetProfit = candidateList[i].NetProfit;
                            minTotalDistance = candidateList[i].TotalDistance;
                            for (var j = i + 1; j <= candidateCount; j++)
                            {
                                if (candidateList[j].Mandatory > maxMandatory ||
                                    (candidateList[j].Mandatory >= maxMandatory && candidateList[j].NetProfit > maxNetProfit + (decimal) Epsilon) ||
                                    (candidateList[j].Mandatory >= maxMandatory && candidateList[j].NetProfit > maxNetProfit + (decimal) Epsilon && candidateList[j].TotalDistance < minTotalDistance - Epsilon))
                                {
                                    maxMandatory = candidateList[j].Mandatory;
                                    maxNetProfit = candidateList[j].NetProfit;
                                    minTotalDistance = candidateList[j].TotalDistance;

                                    k = j;
                                }
                            }

                            if (k > -1)
                            {
                                var candidate = candidateList[i];
                                candidateList[i] = candidateList[k];
                                candidateList[k] = candidate;
                            }
                        }

                        k = (int) (finalCandidateCount * random.NextDouble()) + 1;
                        if (k < 1)
                        {
                            k = 1;
                        }

                        vertex_to_be_added = candidateList[k].VertexToBeAdded;
                        vehicle_type_to_add_to = candidateList[k].VehicleTypeIndex;
                        vehicle_id_to_add_to = candidateList[k].VehicleId;
                        position_to_add_to = candidateList[k].Position;

                        incumbent.AddVertex(vertex_to_be_added, vehicle_type_to_add_to, vehicle_id_to_add_to, position_to_add_to);
                    }

                    incumbent.Evaluate();

                    if ((incumbent.Feasible && !bestSolution.Feasible) ||
                        (incumbent.CoversMandatoryLocations && !bestSolution.CoversMandatoryLocations) ||
                        (incumbent.Feasible == bestSolution.Feasible && incumbent.NetProfit > bestSolution.NetProfit + (decimal) Epsilon) ||
                        (incumbent.Feasible == bestSolution.Feasible && incumbent.NetProfit > bestSolution.NetProfit - (decimal) Epsilon && incumbent.TotalDistance < bestSolution.TotalDistance - Epsilon))
                    {
                        bestSolution.AssignFrom(incumbent);
                    }

                } while (candidateCount > 0);

                incumbent.Improve();

                if ((incumbent.Feasible && !bestSolution.Feasible) ||
                    (incumbent.CoversMandatoryLocations && !bestSolution.CoversMandatoryLocations) ||
                    (incumbent.Feasible == bestSolution.Feasible && incumbent.NetProfit > bestSolution.NetProfit + (decimal) Epsilon) ||
                    (incumbent.Feasible == bestSolution.Feasible && incumbent.NetProfit > bestSolution.NetProfit - (decimal) Epsilon && incumbent.TotalDistance < bestSolution.TotalDistance - Epsilon))
                {
                    bestSolution.AssignFrom(incumbent);
                }
                else if (random.NextDouble() < Math.Pow((endTime - startTime).TotalSeconds / _cpuTimeLimit, 2))
                {
                    incumbent.AssignFrom(bestSolution);
                    incumbent.Evaluate();
                }

                ++iteration;

                endTime = DateTime.Now;

            } while ((endTime - startTime).TotalSeconds < _cpuTimeLimit);

            // Above error must go here

            // Squeeze the solution
            for (var i = 1; i <= num_vehicle_types; i++)
            {
                for (var j = 1; j <= vehicle_types[i].NumberAvailable - 1; j++)
                {
                    if (bestSolution.RouteVertexCount[i][j] == 0)
                    {
                        // Look for a vehicle that is used
                        var l = -1;
                        k = j + 1;
                        do
                        {
                            if (bestSolution.RouteVertexCount[i][k] > 0)
                            {
                                l = k;
                            }
                            ++k;
                        } while (l == -1 && k <= vehicle_types[i].NumberAvailable);

                        if (l != -1)
                        {
                            bestSolution.RouteVertexCount[i][j] = bestSolution.RouteVertexCount[i][l];
                            for (k = 1; k <= bestSolution.RouteVertexCount[i][l]; k++)
                            {
                                bestSolution.RouteVertices[i][j][k] = bestSolution.RouteVertices[i][l][k];
                            }
                            bestSolution.RouteVertexCount[i][l] = 0;
                        }
                    }
                }
            }

            bestSolution.Evaluate();

            // Write solution
            if (bestSolution.Feasible)
            {
                return new VrpResult()
                {
                    IsError = false,
                    Message = $"VRP Solver performed {iteration} LNS iterations and found a solution.",
                    Code = 0,
                    Solution = bestSolution
                };
            }
            else if (feasibilties.Count > 0)
            {
                return new VrpResult()
                {
                    IsError = false,
                    Message = $"VRP Solver performed {iteration} LNS iterations and found a solution with some infeasibilities.",
                    Code = 3,
                    Solution = bestSolution,
                    Infeasibilities = feasibilties
                };
            }
            else
            {
                return new VrpResult()
                {
                    IsError = false,
                    Message = $"The best found solution after {iteration} LNS iterations does not satisfy all constraints.",
                    Code = 2,
                    Solution = bestSolution
                };
            }

        }

        private List<string> FeasibilityCheckData()
        {
            var infeasibilities = new List<string>();

            double totalVehicleCapacity = 0;
            double maxVehicleCapacity = 0;
            for (var i = 1; i <= num_vehicle_types; i++)
            {
                if (maxVehicleCapacity < vehicle_types[i].Capacity)
                {
                    maxVehicleCapacity = vehicle_types[i].Capacity;
                }

                totalVehicleCapacity += vehicle_types[i].NumberAvailable * vehicle_types[i].Capacity;
            }

            double totalSupply = 0;
            for (var i = num_depots + 1; i <= num_locations; i++)
            {
                if (_vertices[i].MustBeVisited == 1)
                {
                    totalSupply += _vertices[i].PickupAmount;
                }
            }

            if (totalSupply > totalVehicleCapacity)
            {
                infeasibilities.Add("The capacity of the given fleet is not enough to transport the mandatory pickup.");
            }

            totalSupply = 0;
            for (var i = num_depots + 1; i <= num_locations; i++)
            {
                if (_vertices[i].MustBeVisited == 1)
                {
                    totalSupply += _vertices[i].DeliveryAmount;
                }
            }

            if (totalSupply > totalVehicleCapacity)
            {
                infeasibilities.Add("The capacity of the given fleet is not enough to transport the mandatory delivery.");
            }

            for (var i = num_depots + 1; i <= num_locations; i++)
            {
                if (_vertices[i].MustBeVisited == 1 && _vertices[i].PickupAmount > maxVehicleCapacity)
                {
                    infeasibilities.Add($"The supply of location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) is too large to fit into any vehicle.");
                }

                if (_vertices[i].MustBeVisited == 1 && _vertices[i].DeliveryAmount > maxVehicleCapacity)
                {
                    infeasibilities.Add($"The demand of location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) is too large to fit into any vehicle.");
                }

                if (_vertices[i].MustBeVisited == 1 && (_vertices[i].ToTime - _vertices[i].FromTime) < _vertices[i].ServiceTime)
                {
                    infeasibilities.Add($"The service time of location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) is shorter than its time window.");
                }
            }

            for (var i = num_depots + 1; i <= num_locations; i++)
            {
                if (!IsOpenVrp)
                {
                    var reachable = false;
                    var baseId = 0;

                    for (var j = 1; j <= num_vehicle_types; j++)
                    {
                        baseId = VehicleTypes[j].BaseId;
                        if (VehicleTypes[j].NumberAvailable > 0 && _distances[baseId][i] + _distances[i][baseId] < VehicleTypes[j].DistanceLimit)
                        {
                            reachable = true;
                        }
                    }

                    if (_vertices[i].MustBeVisited == 1 && !reachable)
                    {
                        infeasibilities.Add($"Location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) cannot be visited with the given distance limit.");
                    }

                    reachable = false;
                    baseId = 0;

                    for (var j = 1; j <= num_vehicle_types; j++)
                    {
                        baseId = VehicleTypes[j].BaseId;
                        if (VehicleTypes[j].NumberAvailable > 0 && _durations[baseId][i] + _durations[i][baseId] < VehicleTypes[j].DrivingTimeLimit)
                        {
                            reachable = true;
                        }
                    }

                    if (_vertices[i].MustBeVisited == 1 && !reachable)
                    {
                        infeasibilities.Add($"Location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) cannot be visited with the given driving time limit.");
                    }

                    reachable = false;
                    baseId = 0;

                    for (var j = 1; j <= num_vehicle_types; j++)
                    {
                        baseId = VehicleTypes[j].BaseId;
                        if (VehicleTypes[j].NumberAvailable > 0 && _durations[baseId][i] + _durations[i][baseId] + _vertices[i].ServiceTime < VehicleTypes[j].WorkingTimeLimit)
                        {
                            reachable = true;
                        }
                    }

                    if (_vertices[i].MustBeVisited == 1 && !reachable)
                    {
                        infeasibilities.Add($"Location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) cannot be served with the given working time limit.");
                    }
                }
                else
                {
                    var reachable = false;
                    var baseId = 0;

                    for (var j = 1; j <= num_vehicle_types; j++)
                    {
                        baseId = VehicleTypes[j].BaseId;
                        if (VehicleTypes[j].NumberAvailable > 0 && _distances[baseId][i] < VehicleTypes[j].DistanceLimit)
                        {
                            reachable = true;
                        }
                    }

                    if (_vertices[i].MustBeVisited == 1 && !reachable)
                    {
                        infeasibilities.Add($"Location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) cannot be visited with the given distance limit.");
                    }

                    reachable = false;
                    baseId = 0;

                    for (var j = 1; j <= num_vehicle_types; j++)
                    {
                        baseId = VehicleTypes[j].BaseId;
                        if (VehicleTypes[j].NumberAvailable > 0 && _durations[baseId][i] < VehicleTypes[j].DrivingTimeLimit)
                        {
                            reachable = true;
                        }
                    }

                    if (_vertices[i].MustBeVisited == 1 && !reachable)
                    {
                        infeasibilities.Add($"Location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) cannot be visited with the given driving time limit.");
                    }

                    reachable = false;
                    baseId = 0;

                    for (var j = 1; j <= num_vehicle_types; j++)
                    {
                        baseId = VehicleTypes[j].BaseId;
                        if (VehicleTypes[j].NumberAvailable > 0 && _durations[baseId][i] + _vertices[i].ServiceTime < VehicleTypes[j].WorkingTimeLimit)
                        {
                            reachable = true;
                        }
                    }

                    if (_vertices[i].MustBeVisited == 1 && !reachable)
                    {
                        infeasibilities.Add($"Location '{_vertices[i].CustomerName}' ({_vertices[i].Id}) cannot be served with the given working time limit.");
                    }
                }
            }

            return infeasibilities;
        }
    }

    public class Solution
    {
        #region Public properties

        public bool Feasible
        {
            get; set;
        }

        public bool CoversMandatoryLocations
        {
            get; set;
        }

        public decimal NetProfit
        {
            get; set;
        }

        public double TotalDistance
        {
            get; set;
        }

        public decimal[][] NetProfitPerRoute
        {
            get; set;
        }

        public double[][] TotalDistancePerRoute
        {
            get; set;
        }

        public int[][] RouteVertexCount
        {
            get; set;
        }

        public int[][][] RouteVertices
        {
            get; set;
        }

        public int[] VerticesVisited
        {
            get; set;
        }

        #endregion

        private VrpSolver _solver;

        public Solution(VrpSolver solver)
        {
            _solver = solver;

            Feasible = false;
            CoversMandatoryLocations = false;
            NetProfit = 0;
            TotalDistance = 0;

            var maxNumberOfVehicles = 0;
            for (var i = 1; i <= _solver.NumberOfVehicleTypes; i++)
            {
                if (maxNumberOfVehicles < _solver.VehicleTypes[i].NumberAvailable)
                {
                    maxNumberOfVehicles = _solver.VehicleTypes[i].NumberAvailable;
                }
            }

            // Initialize array with leading empty item - simulate 1-based array in VBA
            NetProfitPerRoute = new decimal[_solver.NumberOfVehicleTypes + 1][];
            TotalDistancePerRoute = new double[_solver.NumberOfVehicleTypes + 1][];
            RouteVertexCount = new int[_solver.NumberOfVehicleTypes + 1][];
            RouteVertices = new int[_solver.NumberOfVehicleTypes + 1][][];
            VerticesVisited = new int[_solver.NumberOfLocations + 1];

            for (var i = 1; i <= _solver.NumberOfVehicleTypes; i++)
            {
                NetProfitPerRoute[i] = new decimal[maxNumberOfVehicles + 1];
                TotalDistancePerRoute[i] = new double[maxNumberOfVehicles + 1];
                RouteVertexCount[i] = new int[maxNumberOfVehicles + 1];
                RouteVertices[i] = new int[maxNumberOfVehicles + 1][];
                for (var j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                {
                    NetProfitPerRoute[i][j] = 0;
                    TotalDistancePerRoute[i][j] = 0;
                    RouteVertexCount[i][j] = 0;
                    RouteVertices[i][j] = new int[_solver.NumberOfCustomers + 1];
                    for (var k = 1; k <= _solver.NumberOfCustomers; k++)
                    {
                        RouteVertices[i][j][k] = -1;
                    }
                }
            }

            for (var i = 1; i <= _solver.NumberOfLocations; i++)
            {
                VerticesVisited[i] = 0;
            }
        }

        public void AssignFrom(Solution another)
        {
            Feasible = another.Feasible;
            CoversMandatoryLocations = another.CoversMandatoryLocations;
            NetProfit = another.NetProfit;
            TotalDistance = another.TotalDistance;
            NetProfitPerRoute = new decimal[another.NetProfitPerRoute.Length][];
            for (var i = 0; i < another.NetProfitPerRoute.Length; i++)
            {
                var inner = another.NetProfitPerRoute[i];
                if (inner == null)
                {
                    // Ignore null value
                    continue;
                }
                var ilen = inner.Length;
                var newer = new decimal[ilen];
                Array.Copy(inner, newer, ilen);
                NetProfitPerRoute[i] = newer;
            }
            TotalDistancePerRoute = new double[another.TotalDistancePerRoute.Length][];
            for (var i = 0; i < another.TotalDistancePerRoute.Length; i++)
            {
                var inner = another.TotalDistancePerRoute[i];
                if (inner == null)
                {
                    // Ignore null value
                    continue;
                }
                var ilen = inner.Length;
                var newer = new double[ilen];
                Array.Copy(inner, newer, ilen);
                TotalDistancePerRoute[i] = newer;
            }
            RouteVertexCount = new int[another.RouteVertexCount.Length][];
            for (var i = 0; i < another.RouteVertexCount.Length; i++)
            {
                var inner = another.RouteVertexCount[i];
                if (inner == null)
                {
                    // Ignore null value
                    continue;
                }
                var ilen = inner.Length;
                var newer = new int[ilen];
                Array.Copy(inner, newer, ilen);
                RouteVertexCount[i] = newer;
            }
            RouteVertices = new int[another.RouteVertices.Length][][];
            for (var i = 0; i < another.RouteVertices.Length; i++)
            {
                var inner = another.RouteVertices[i];
                if (inner == null)
                {
                    // Ignore null value
                    continue;
                }
                var ilen = inner.Length;
                var newer = new int[ilen][];
                for (var j = 0; j < ilen; j++)
                {
                    var sinner = inner[j];
                    if (sinner == null)
                    {
                        // Ignore null value
                        continue;
                    }
                    var silen = sinner.Length;
                    var snewer = new int[silen];
                    Array.Copy(sinner, snewer, silen);
                    newer[j] = snewer;
                }
                RouteVertices[i] = newer;
            }
            VerticesVisited = new int[another.VerticesVisited.Length];
            Array.Copy(another.VerticesVisited, VerticesVisited, another.VerticesVisited.Length);
        }

        public void AddVertex(int vertex_to_be_added, int vehicle_type_index, int vehicle_id, int position)
        {
            // Set this location as visited
            ++VerticesVisited[vertex_to_be_added];

            // Shift
            for (var i = RouteVertexCount[vehicle_type_index][vehicle_id]; i >= position; i--)
            {
                RouteVertices[vehicle_type_index][vehicle_id][i + 1] = RouteVertices[vehicle_type_index][vehicle_id][i];
            }

            RouteVertices[vehicle_type_index][vehicle_id][position] = vertex_to_be_added;

            ++RouteVertexCount[vehicle_type_index][vehicle_id];

            EvaluateRoute(vehicle_type_index, vehicle_id);

            if (_solver.Vertices[vertex_to_be_added].MustBeVisited == 1)
            {
                NetProfit += _solver.Penalty;
            }
        }

        public void RemoveVertex(int vehicle_type_index, int vehicle_id, int position)
        {
            var vertex_to_be_removed = RouteVertices[vehicle_type_index][vehicle_id][position];
            --VerticesVisited[vertex_to_be_removed];

            // Shift
            for (var i = position; i <= RouteVertexCount[vehicle_type_index][vehicle_id] - 1; i++)
            {
                RouteVertices[vehicle_type_index][vehicle_id][i] = RouteVertices[vehicle_type_index][vehicle_id][i + 1];
            }
            --RouteVertexCount[vehicle_type_index][vehicle_id];

            EvaluateRoute(vehicle_type_index, vehicle_id);

            if (_solver.Vertices[vertex_to_be_removed].MustBeVisited == 1)
            {
                NetProfit -= _solver.Penalty;
            }
        }

        private void EvaluateRoute(int vehicle_type_index, int vehicle_id)
        {
            double amountOnBoard = 0;
            double distanceTraversed = 0;
            var timeAccumulated = _solver.VehicleTypes[vehicle_type_index].WorkStartTime;
            double drivingTimeTotal = 0;
            double workingTimeTotal = 0;

            decimal netProfitThisRoute = 0;
            double totalDistanceThisRoute = 0;

            var baseId = 0;

            NetProfit -= NetProfitPerRoute[vehicle_type_index][vehicle_id];
            TotalDistance -= TotalDistancePerRoute[vehicle_type_index][vehicle_id];

            if (RouteVertexCount[vehicle_type_index][vehicle_id] > 0)
            {
                netProfitThisRoute = -_solver.VehicleTypes[vehicle_type_index].FixedCostPerTrip;
                baseId = _solver.VehicleTypes[vehicle_type_index].BaseId;

                for (var i = 1; i <= RouteVertexCount[vehicle_type_index][vehicle_id]; i++)
                {
                    var thisVertex = RouteVertices[vehicle_type_index][vehicle_id][i];
                    amountOnBoard += _solver.Vertices[thisVertex].DeliveryAmount;
                }

                if (amountOnBoard > _solver.VehicleTypes[vehicle_type_index].Capacity)
                {
                    Feasible = false;
                    netProfitThisRoute -= _solver.Penalty * (decimal) (Math.Pow((amountOnBoard / _solver.VehicleTypes[vehicle_type_index].Capacity), 2) - 1);
                }

                for (var i = 1; i <= RouteVertexCount[vehicle_type_index][vehicle_id]; i++)
                {
                    var thisVertex = RouteVertices[vehicle_type_index][vehicle_id][i];
                    amountOnBoard = amountOnBoard - _solver.Vertices[thisVertex].DeliveryAmount + _solver.Vertices[thisVertex].PickupAmount;

                    if (amountOnBoard > _solver.VehicleTypes[vehicle_type_index].Capacity)
                    {
                        Feasible = false;
                        netProfitThisRoute -= _solver.Penalty * (decimal) (Math.Pow((amountOnBoard / _solver.VehicleTypes[vehicle_type_index].Capacity), 2) - 1);
                    }

                    if (amountOnBoard < 0)
                    {
                        Feasible = false;
                        netProfitThisRoute -= _solver.Penalty;
                    }

                    netProfitThisRoute += _solver.Vertices[thisVertex].Profit;

                    if (i == 1)
                    {
                        distanceTraversed += _solver.Distances[baseId][thisVertex];
                        timeAccumulated += _solver.Durations[baseId][thisVertex];
                        drivingTimeTotal += _solver.Durations[baseId][thisVertex];
                        workingTimeTotal += _solver.Durations[baseId][thisVertex];

                        totalDistanceThisRoute += _solver.Distances[baseId][thisVertex];
                    }
                    else
                    {
                        var previousLocation = RouteVertices[vehicle_type_index][vehicle_id][i - 1];

                        distanceTraversed += _solver.Distances[previousLocation][thisVertex];
                        timeAccumulated += _solver.Durations[previousLocation][thisVertex];
                        drivingTimeTotal += _solver.Durations[previousLocation][thisVertex];
                        workingTimeTotal += _solver.Durations[previousLocation][thisVertex];

                        totalDistanceThisRoute += _solver.Distances[previousLocation][thisVertex];
                    }

                    if (timeAccumulated < _solver.Vertices[thisVertex].FromTime)
                    {
                        if (_solver.WaitingIsWorking)
                        {
                            workingTimeTotal = workingTimeTotal + _solver.Vertices[thisVertex].FromTime - timeAccumulated;
                        }
                        timeAccumulated = _solver.Vertices[thisVertex].FromTime;
                    }

                    timeAccumulated += _solver.Vertices[thisVertex].ServiceTime;
                    workingTimeTotal += _solver.Vertices[thisVertex].ServiceTime;

                    if (timeAccumulated > _solver.Vertices[thisVertex].ToTime)
                    {
                        if (!_solver.IsSoftTimeWindows)
                        {
                            Feasible = false;
                        }
                        netProfitThisRoute -= _solver.Penalty * ((decimal) Math.Pow((timeAccumulated / _solver.Vertices[thisVertex].ToTime), 2) - 1);
                    }
                }

                if (!_solver.IsOpenVrp)
                {
                    var loc = RouteVertices[vehicle_type_index][vehicle_id][RouteVertexCount[vehicle_type_index][vehicle_id]];
                    distanceTraversed += _solver.Distances[loc][baseId];
                    timeAccumulated += _solver.Durations[loc][baseId];
                    drivingTimeTotal += _solver.Durations[loc][baseId];
                    workingTimeTotal += _solver.Durations[loc][baseId];

                    totalDistanceThisRoute += _solver.Distances[loc][baseId];

                    if (timeAccumulated > _solver.Vertices[baseId].ToTime)
                    {
                        if (!_solver.IsSoftTimeWindows)
                        {
                            Feasible = false;
                        }
                        netProfitThisRoute -= _solver.Penalty * ((decimal) Math.Pow((timeAccumulated / _solver.Vertices[baseId].ToTime), 2) - 1);
                    }
                }

                if (distanceTraversed > _solver.VehicleTypes[vehicle_type_index].DistanceLimit)
                {
                    Feasible = false;
                    netProfitThisRoute -= _solver.Penalty * ((decimal) Math.Pow((distanceTraversed / _solver.VehicleTypes[vehicle_type_index].DistanceLimit), 2) - 1);
                }

                if (amountOnBoard > _solver.VehicleTypes[vehicle_type_index].Capacity)
                {
                    Feasible = false;
                    netProfitThisRoute -= _solver.Penalty * ((decimal) Math.Pow((amountOnBoard / _solver.VehicleTypes[vehicle_type_index].Capacity), 2) - 1);
                }

                if (amountOnBoard < 0)
                {
                    Feasible = false;
                    netProfitThisRoute -= _solver.Penalty;
                }

                if (drivingTimeTotal > _solver.VehicleTypes[vehicle_type_index].DrivingTimeLimit)
                {
                    Feasible = false;
                    netProfitThisRoute -= _solver.Penalty * ((decimal) Math.Pow((drivingTimeTotal / _solver.VehicleTypes[vehicle_type_index].DrivingTimeLimit), 2) - 1);
                }

                if (workingTimeTotal > _solver.VehicleTypes[vehicle_type_index].WorkingTimeLimit)
                {
                    Feasible = false;
                    netProfitThisRoute -= _solver.Penalty * ((decimal) Math.Pow((workingTimeTotal / _solver.VehicleTypes[vehicle_type_index].WorkingTimeLimit), 2) - 1);
                }
            }

            TotalDistancePerRoute[vehicle_type_index][vehicle_id] = totalDistanceThisRoute;
            TotalDistance += totalDistanceThisRoute;

            netProfitThisRoute -= (decimal) totalDistanceThisRoute * _solver.VehicleTypes[vehicle_type_index].CostPerUnitDistance;
            NetProfitPerRoute[vehicle_type_index][vehicle_id] = netProfitThisRoute;
            NetProfit += netProfitThisRoute;
        }

        public void Evaluate()
        {
            NetProfit = 0;
            for (var i = 1; i <= _solver.NumberOfVehicleTypes; i++)
            {
                for (var j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                {
                    NetProfitPerRoute[i][j] = 0;
                }
            }
            Feasible = true;
            CoversMandatoryLocations = true;

            for (var i = 1; i <= _solver.NumberOfVehicleTypes; i++)
            {
                for (var j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                {
                    EvaluateRoute(i, j);
                }
            }

            // Check mandatory vertices and visits
            for (var i = _solver.NumberOfDepots + 1; i <= _solver.NumberOfLocations; i++)
            {
                if (_solver.Vertices[i].MustBeVisited == 1 && VerticesVisited[i] == 0)
                {
                    Feasible = false;
                    CoversMandatoryLocations = false;
                    NetProfit -= _solver.Penalty;
                }

                if (_solver.Vertices[i].MustBeVisited == -1 && VerticesVisited[i] == 1)
                {
                    Feasible = false;
                    NetProfit -= _solver.Penalty;
                }

                if (VerticesVisited[i] > 1)
                {
                    Feasible = false;
                    NetProfit -= _solver.Penalty;
                }
            }
        }

        public void Improve()
        {
            int i, j, k;
            int a, b, c;
            int vertex;
            var vertex_buffer = new int[_solver.NumberOfCustomers + 1];
            int cnt1, cnt2;

            var vehicle_type_to_swap1 = 0;
            var vehicle_type_to_swap2 = 0;
            var vehicle_id_to_swap1 = 0;
            var vehicle_id_to_swap2 = 0;
            var position_to_swap1 = 0;
            var position_to_swap2 = 0;
            var vertex_to_swap = 0;
            var vehicle_id_start_index = 0;

            var vehicle_type_to_relocate1 = 0;
            var vehicle_type_to_relocate2 = 0;
            var vehicle_id_to_relocate1 = 0;
            var vehicle_id_to_relocate2 = 0;
            var position_to_relocate1 = 0;
            var position_to_relocate2 = 0;

            var vehicle_type_for_2opt1 = 0;
            var vehicle_type_for_2opt2 = 0;
            var vehicle_id_for_2opt1 = 0;
            var vehicle_id_for_2opt2 = 0;
            var position_for_2opt1 = 0;
            var position_for_2opt2 = 0;
            var vertex_cnt_for_2opt1 = 0;
            var vertex_cnt_for_2opt2 = 0;

            var reversal_for_2opt1 = 0;
            var reversal_for_2opt2 = 0;

            var vehicle_type_for_chain_reversal = 0;
            var vehicle_id_for_chain_reversal = 0;
            var position_for_chain_reversal1 = 0;
            var position_for_chain_reversal2 = 0;
            int midpoint_for_chain_reversal;

            var vehicle_type_for_full_swap1 = 0;
            var vehicle_type_for_full_swap2 = 0;
            var vehicle_id_for_full_swap1 = 0;
            var vehicle_id_for_full_swap2 = 0;
            int max_vertex_cnt, vertex_cnt_to_swap;

            decimal max_net_profit;
            double min_total_distance;

            // Polishing
            do
            {
                max_net_profit = NetProfit;
                min_total_distance = TotalDistance;

                // Swap
                vehicle_type_to_swap1 = -1;
                for (i = 1; i <= _solver.NumberOfVehicleTypes; i++)
                {
                    for (j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                    {
                        for (k = 1; k <= RouteVertexCount[i][j]; k++)
                        {
                            for (a = i; a <= _solver.NumberOfVehicleTypes; a++)
                            {
                                if (a == i)
                                {
                                    vehicle_id_start_index = j;
                                }
                                else
                                {
                                    vehicle_id_start_index = 1;
                                }

                                for (b = vehicle_id_start_index; b <= _solver.VehicleTypes[a].NumberAvailable; b++)
                                {
                                    for (c = 1; c <= RouteVertexCount[a][b]; c++)
                                    {
                                        vertex_to_swap = RouteVertices[i][j][k];
                                        RouteVertices[i][j][k] = RouteVertices[a][b][c];
                                        RouteVertices[a][b][c] = vertex_to_swap;

                                        EvaluateRoute(i, j);
                                        EvaluateRoute(a, b);

                                        if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                            (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                        {
                                            max_net_profit = NetProfit;
                                            min_total_distance = TotalDistance;

                                            vehicle_type_to_swap1 = i;
                                            vehicle_id_to_swap1 = j;
                                            position_to_swap1 = k;

                                            vehicle_type_to_swap2 = a;
                                            vehicle_id_to_swap2 = b;
                                            position_to_swap2 = c;
                                        }

                                        vertex_to_swap = RouteVertices[i][j][k];
                                        RouteVertices[i][j][k] = RouteVertices[a][b][c];
                                        RouteVertices[a][b][c] = vertex_to_swap;

                                        EvaluateRoute(i, j);
                                        EvaluateRoute(a, b);
                                    }
                                }
                            }
                        }
                    }
                }

                // Relocate
                vehicle_type_to_relocate1 = -1;
                for (i = 1; i <= _solver.NumberOfVehicleTypes; i++)
                {
                    for (j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                    {
                        for (k = 1; k <= RouteVertexCount[i][j]; k++)
                        {
                            vertex = RouteVertices[i][j][k];
                            RemoveVertex(i, j, k);

                            for (a = 1; a <= _solver.NumberOfVehicleTypes; a++)
                            {
                                for (b = 1; b <= _solver.VehicleTypes[a].NumberAvailable; b++)
                                {
                                    for (c = 1; c <= RouteVertexCount[a][b] + 1; c++)
                                    {
                                        AddVertex(vertex, a, b, c);
                                        if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                            (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                        {
                                            max_net_profit = NetProfit;
                                            min_total_distance = TotalDistance;

                                            vehicle_type_to_relocate1 = i;
                                            vehicle_id_to_relocate1 = j;
                                            position_to_relocate1 = k;

                                            vehicle_type_to_relocate2 = a;
                                            vehicle_id_to_relocate2 = b;
                                            position_to_relocate2 = c;

                                            vehicle_type_to_swap1 = -1;
                                        }

                                        RemoveVertex(a, b, c);
                                    }
                                }
                            }

                            AddVertex(vertex, i, j, k);
                        }
                    }
                }

                // 2-opt
                vehicle_type_for_2opt1 = -1;
                for (i = 1; i <= _solver.NumberOfVehicleTypes; i++)
                {
                    for (j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                    {
                        for (a = i; a <= _solver.NumberOfVehicleTypes; a++)
                        {
                            if (a == i)
                            {
                                vehicle_id_start_index = j + 1;
                            }
                            else
                            {
                                vehicle_id_start_index = 1;
                            }

                            for (b = vehicle_id_start_index; b <= _solver.VehicleTypes[a].NumberAvailable; b++)
                            {
                                if (RouteVertexCount[i][j] > 2 && RouteVertexCount[a][b] > 2)
                                {
                                    for (k = 1; k <= RouteVertexCount[i][j] - 1; k++)
                                    {
                                        for (c = 1; c <= RouteVertexCount[a][b] - 1; c++)
                                        {
                                            vertex_cnt_for_2opt1 = RouteVertexCount[i][j] - k;
                                            vertex_cnt_for_2opt2 = RouteVertexCount[a][b] - c;

                                            for (vertex = k + 1; vertex <= RouteVertexCount[i][j]; vertex++)
                                            {
                                                vertex_buffer[vertex] = RouteVertices[i][j][vertex];
                                            }

                                            for (vertex = c + 1; vertex <= RouteVertexCount[a][b]; vertex++)
                                            {
                                                RouteVertices[i][j][k + vertex - c] = RouteVertices[a][b][vertex];
                                            }
                                            for (vertex = k + 1; vertex <= RouteVertexCount[i][j]; vertex++)
                                            {
                                                RouteVertices[a][b][c + vertex - k] = vertex_buffer[vertex];
                                            }

                                            RouteVertexCount[i][j] = k + vertex_cnt_for_2opt2;
                                            RouteVertexCount[a][b] = c + vertex_cnt_for_2opt1;

                                            EvaluateRoute(i, j);
                                            EvaluateRoute(a, b);

                                            if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                                (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                            {
                                                max_net_profit = NetProfit;
                                                min_total_distance = TotalDistance;

                                                vehicle_type_for_2opt1 = i;
                                                vehicle_id_for_2opt1 = j;
                                                position_for_2opt1 = k;

                                                vehicle_type_for_2opt2 = a;
                                                vehicle_id_for_2opt2 = b;
                                                position_for_2opt2 = c;

                                                reversal_for_2opt1 = 0;
                                                reversal_for_2opt2 = 0;

                                                vehicle_type_to_swap1 = -1;
                                                vehicle_type_to_relocate1 = -1;
                                            }

                                            // Revert route i, j
                                            midpoint_for_chain_reversal = (RouteVertexCount[i][j] - (k + 1)) / 2;

                                            for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                                            {
                                                vertex_to_swap = RouteVertices[i][j][k + 1 + vertex];
                                                RouteVertices[i][j][k + 1 + vertex] = RouteVertices[i][j][RouteVertexCount[i][j] - vertex];
                                                RouteVertices[i][j][RouteVertexCount[i][j] - vertex] = vertex_to_swap;
                                            }

                                            EvaluateRoute(i, j);

                                            if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                                (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                            {
                                                max_net_profit = NetProfit;
                                                min_total_distance = TotalDistance;

                                                vehicle_type_for_2opt1 = i;
                                                vehicle_id_for_2opt1 = j;
                                                position_for_2opt1 = k;

                                                vehicle_type_for_2opt2 = a;
                                                vehicle_id_for_2opt2 = b;
                                                position_for_2opt2 = c;

                                                reversal_for_2opt1 = 1;
                                                reversal_for_2opt2 = 0;

                                                vehicle_type_to_swap1 = -1;
                                                vehicle_type_to_relocate1 = -1;
                                            }

                                            // Revert route a, b
                                            midpoint_for_chain_reversal = (RouteVertexCount[a][b] - (c + 1)) / 2;

                                            for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                                            {
                                                vertex_to_swap = RouteVertices[a][b][c + 1 + vertex];
                                                RouteVertices[a][b][c + 1 + vertex] = RouteVertices[a][b][RouteVertexCount[a][b] - vertex];
                                                RouteVertices[a][b][RouteVertexCount[a][b] - vertex] = vertex_to_swap;
                                            }

                                            EvaluateRoute(a, b);

                                            if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                                (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                            {
                                                max_net_profit = NetProfit;
                                                min_total_distance = TotalDistance;

                                                vehicle_type_for_2opt1 = i;
                                                vehicle_id_for_2opt1 = j;
                                                position_for_2opt1 = k;

                                                vehicle_type_for_2opt2 = a;
                                                vehicle_id_for_2opt2 = b;
                                                position_for_2opt2 = c;

                                                reversal_for_2opt1 = 1;
                                                reversal_for_2opt2 = 1;

                                                vehicle_type_to_swap1 = -1;
                                                vehicle_type_to_relocate1 = -1;
                                            }

                                            // Revert route i, j again
                                            midpoint_for_chain_reversal = (RouteVertexCount[i][j] - (k + 1)) / 2;

                                            for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                                            {
                                                vertex_to_swap = RouteVertices[i][j][k + 1 + vertex];
                                                RouteVertices[i][j][k + 1 + vertex] = RouteVertices[i][j][RouteVertexCount[i][j] - vertex];
                                                RouteVertices[i][j][RouteVertexCount[i][j] - vertex] = vertex_to_swap;
                                            }

                                            EvaluateRoute(i, j);

                                            if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                                (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                            {
                                                max_net_profit = NetProfit;
                                                min_total_distance = TotalDistance;

                                                vehicle_type_for_2opt1 = i;
                                                vehicle_id_for_2opt1 = j;
                                                position_for_2opt1 = k;

                                                vehicle_type_for_2opt2 = a;
                                                vehicle_id_for_2opt2 = b;
                                                position_for_2opt2 = c;

                                                reversal_for_2opt1 = 0;
                                                reversal_for_2opt2 = 1;

                                                vehicle_type_to_swap1 = -1;
                                                vehicle_type_to_relocate1 = -1;
                                            }

                                            // Revert route a, b again
                                            midpoint_for_chain_reversal = (RouteVertexCount[a][b] - (c + 1)) / 2;

                                            for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                                            {
                                                vertex_to_swap = RouteVertices[a][b][c + 1 + vertex];
                                                RouteVertices[a][b][c + 1 + vertex] = RouteVertices[a][b][RouteVertexCount[a][b] - vertex];
                                                RouteVertices[a][b][RouteVertexCount[a][b] - vertex] = vertex_to_swap;
                                            }

                                            // EvaluateRoute(a, b);
                                            vertex_cnt_for_2opt1 = RouteVertexCount[i][j] - k;
                                            vertex_cnt_for_2opt2 = RouteVertexCount[a][b] - c;

                                            for (vertex = k + 1; vertex <= RouteVertexCount[i][j]; vertex++)
                                            {
                                                vertex_buffer[vertex] = RouteVertices[i][j][vertex];
                                            }

                                            for (vertex = c + 1; vertex <= RouteVertexCount[a][b]; vertex++)
                                            {
                                                RouteVertices[i][j][k + vertex - c] = RouteVertices[a][b][vertex];
                                            }

                                            for (vertex = k + 1; vertex <= RouteVertexCount[i][j]; vertex++)
                                            {
                                                RouteVertices[a][b][c + vertex - k] = vertex_buffer[vertex];
                                            }

                                            RouteVertexCount[i][j] = k + vertex_cnt_for_2opt2;
                                            RouteVertexCount[a][b] = c + vertex_cnt_for_2opt1;

                                            EvaluateRoute(i, j);
                                            EvaluateRoute(a, b);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Chain reversal (2-opt on a single route)
                vehicle_type_for_chain_reversal = -1;
                for (i = 1; i <= _solver.NumberOfVehicleTypes; i++)
                {
                    for (j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                    {
                        for (k = 1; k <= RouteVertexCount[i][j] - 3; k++)
                        {
                            for (c = k + 3; c <= RouteVertexCount[i][j]; c++)
                            {
                                midpoint_for_chain_reversal = (c - k) / 2;

                                for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                                {
                                    vertex_to_swap = RouteVertices[i][j][k + vertex];
                                    RouteVertices[i][j][k + vertex] = RouteVertices[i][j][c - vertex];
                                    RouteVertices[i][j][c - vertex] = vertex_to_swap;
                                }

                                EvaluateRoute(i, j);

                                if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                    (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                {
                                    max_net_profit = NetProfit;
                                    min_total_distance = TotalDistance;

                                    vehicle_type_for_chain_reversal = i;
                                    vehicle_id_for_chain_reversal = j;
                                    position_for_chain_reversal1 = k;
                                    position_for_chain_reversal2 = c;

                                    vehicle_type_to_swap1 = -1;
                                    vehicle_type_to_relocate1 = -1;
                                    vehicle_type_for_2opt1 = -1;
                                }
                                for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                                {
                                    vertex_to_swap = RouteVertices[i][j][k + vertex];
                                    RouteVertices[i][j][k + vertex] = RouteVertices[i][j][c - vertex];
                                    RouteVertices[i][j][c - vertex] = vertex_to_swap;
                                }

                                EvaluateRoute(i, j);
                            }
                        }
                    }
                }

                // Full swap - exchanging all customers on two vehicles of different types
                vehicle_type_for_full_swap1 = -1;
                if (_solver.NumberOfVehicleTypes > 1)
                {
                    for (i = 1; i <= _solver.NumberOfVehicleTypes; i++)
                    {
                        for (j = 1; j <= _solver.VehicleTypes[i].NumberAvailable; j++)
                        {
                            for (a = i + 1; a <= _solver.NumberOfVehicleTypes; a++)
                            {
                                for (b = 1; b <= _solver.VehicleTypes[a].NumberAvailable; b++)
                                {
                                    max_vertex_cnt = RouteVertexCount[i][j];
                                    if (max_vertex_cnt < RouteVertexCount[a][b])
                                    {
                                        max_vertex_cnt = RouteVertexCount[a][b];
                                    }

                                    for (k = 1; k <= max_vertex_cnt; k++)
                                    {
                                        vertex_to_swap = RouteVertices[i][j][k];
                                        RouteVertices[i][j][k] = RouteVertices[a][b][k];
                                        RouteVertices[a][b][k] = vertex_to_swap;
                                    }

                                    vertex_cnt_to_swap = RouteVertexCount[i][j];
                                    RouteVertexCount[i][j] = RouteVertexCount[a][b];
                                    RouteVertexCount[a][b] = vertex_cnt_to_swap;

                                    EvaluateRoute(i, j);
                                    EvaluateRoute(a, b);

                                    if (NetProfit > max_net_profit + (decimal) VrpSolver.Epsilon ||
                                        (NetProfit > max_net_profit - (decimal) VrpSolver.Epsilon && TotalDistance < min_total_distance - VrpSolver.Epsilon))
                                    {
                                        max_net_profit = NetProfit;
                                        min_total_distance = TotalDistance;

                                        vehicle_type_for_full_swap1 = i;
                                        vehicle_id_for_full_swap1 = j;
                                        vehicle_type_for_full_swap2 = a;
                                        vehicle_id_for_full_swap2 = b;

                                        vehicle_type_to_swap1 = -1;
                                        vehicle_type_to_relocate1 = -1;
                                        vehicle_type_for_2opt1 = -1;
                                        vehicle_type_for_chain_reversal = -1;
                                    }

                                    for (k = 1; k <= max_vertex_cnt; k++)
                                    {
                                        vertex_to_swap = RouteVertices[i][j][k];
                                        RouteVertices[i][j][k] = RouteVertices[a][b][k];
                                        RouteVertices[a][b][k] = vertex_to_swap;
                                    }

                                    vertex_cnt_to_swap = RouteVertexCount[i][j];
                                    RouteVertexCount[i][j] = RouteVertexCount[a][b];
                                    RouteVertexCount[a][b] = vertex_cnt_to_swap;

                                    EvaluateRoute(i, j);
                                    EvaluateRoute(a, b);
                                }
                            }
                        }
                    }
                }

                if (vehicle_type_to_swap1 != -1)
                {
                    vertex_to_swap = RouteVertices[vehicle_type_to_swap1][vehicle_id_to_swap1][position_to_swap1];
                    RouteVertices[vehicle_type_to_swap1][vehicle_id_to_swap1][position_to_swap1] = RouteVertices[vehicle_type_to_swap2][vehicle_id_to_swap2][position_to_swap2];
                    RouteVertices[vehicle_type_to_swap2][vehicle_id_to_swap2][position_to_swap2] = vertex_to_swap;

                    EvaluateRoute(vehicle_type_to_swap1, vehicle_id_to_swap1);
                    EvaluateRoute(vehicle_type_to_swap2, vehicle_id_to_swap2);
                }

                if (vehicle_type_to_relocate1 != -1)
                {
                    vertex = RouteVertices[vehicle_type_to_relocate1][vehicle_id_to_relocate1][position_to_relocate1];

                    RemoveVertex(vehicle_type_to_relocate1, vehicle_id_to_relocate1, position_to_relocate1);
                    AddVertex(vertex, vehicle_type_to_relocate2, vehicle_id_to_relocate2, position_to_relocate2);
                }

                if (vehicle_type_for_2opt1 != -1)
                {
                    vertex_cnt_for_2opt1 = RouteVertexCount[vehicle_type_for_2opt1][vehicle_id_for_2opt1] - position_for_2opt1;
                    vertex_cnt_for_2opt2 = RouteVertexCount[vehicle_type_for_2opt2][vehicle_id_for_2opt2] - position_for_2opt2;

                    for (vertex = position_for_2opt1 + 1; vertex <= RouteVertexCount[vehicle_type_for_2opt1][vehicle_id_for_2opt1]; vertex++)
                    {
                        vertex_buffer[vertex] = RouteVertices[vehicle_type_for_2opt1][vehicle_id_for_2opt1][vertex];
                    }

                    for (vertex = position_for_2opt2 + 1; vertex <= RouteVertexCount[vehicle_type_for_2opt2][vehicle_id_for_2opt2]; vertex++)
                    {
                        RouteVertices[vehicle_type_for_2opt1][vehicle_id_for_2opt1][position_for_2opt1 + vertex - position_for_2opt2] = RouteVertices[vehicle_type_for_2opt2][vehicle_id_for_2opt2][vertex];
                    }

                    for (vertex = position_for_2opt1 + 1; vertex <= RouteVertexCount[vehicle_type_for_2opt1][vehicle_id_for_2opt1]; vertex++)
                    {
                        RouteVertices[vehicle_type_for_2opt2][vehicle_id_for_2opt2][position_for_2opt2 + vertex - position_for_2opt1] = vertex_buffer[vertex];
                    }

                    RouteVertexCount[vehicle_type_for_2opt1][vehicle_id_for_2opt1] = position_for_2opt1 + vertex_cnt_for_2opt2;
                    RouteVertexCount[vehicle_type_for_2opt2][vehicle_id_for_2opt2] = position_for_2opt2 + vertex_cnt_for_2opt1;

                    if (reversal_for_2opt1 == 1)
                    {
                        i = vehicle_type_for_2opt1;
                        j = vehicle_id_for_2opt1;
                        k = position_for_2opt1;

                        midpoint_for_chain_reversal = (RouteVertexCount[i][j] - (k + 1)) / 2;
                        for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                        {
                            vertex_to_swap = RouteVertices[i][j][k + 1 + vertex];
                            RouteVertices[i][j][k + 1 + vertex] = RouteVertices[i][j][RouteVertexCount[i][j] - vertex];
                            RouteVertices[i][j][RouteVertexCount[i][j] - vertex] = vertex_to_swap;
                        }
                    }

                    if (reversal_for_2opt2 == 1)
                    {
                        a = vehicle_type_for_2opt2;
                        b = vehicle_id_for_2opt2;
                        c = position_for_2opt2;

                        midpoint_for_chain_reversal = (RouteVertexCount[a][b] - (c + 1)) / 2;

                        for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                        {
                            vertex_to_swap = RouteVertices[a][b][c + 1 + vertex];
                            RouteVertices[a][b][c + 1 + vertex] = RouteVertices[a][b][RouteVertexCount[a][b] - vertex];
                            RouteVertices[a][b][RouteVertexCount[a][b] - vertex] = vertex_to_swap;
                        }
                    }

                    EvaluateRoute(vehicle_type_for_2opt1, vehicle_id_for_2opt1);
                    EvaluateRoute(vehicle_type_for_2opt2, vehicle_id_for_2opt2);
                }

                if (vehicle_type_for_chain_reversal != -1)
                {
                    midpoint_for_chain_reversal = (position_for_chain_reversal2 - position_for_chain_reversal1) / 2;

                    for (vertex = 0; vertex <= midpoint_for_chain_reversal; vertex++)
                    {
                        vertex_to_swap = RouteVertices[vehicle_type_for_chain_reversal][vehicle_id_for_chain_reversal][position_for_chain_reversal1 + vertex];
                        RouteVertices[vehicle_type_for_chain_reversal][vehicle_id_for_chain_reversal][position_for_chain_reversal1 + vertex] = RouteVertices[vehicle_type_for_chain_reversal][vehicle_id_for_chain_reversal][position_for_chain_reversal2 - vertex];
                        RouteVertices[vehicle_type_for_chain_reversal][vehicle_id_for_chain_reversal][position_for_chain_reversal2 - vertex] = vertex_to_swap;
                    }

                    EvaluateRoute(vehicle_type_for_chain_reversal, vehicle_id_for_chain_reversal);
                }

                if (vehicle_type_for_full_swap1 != -1)
                {
                    max_vertex_cnt = RouteVertexCount[vehicle_type_for_full_swap1][vehicle_id_for_full_swap1];

                    if (max_vertex_cnt < RouteVertexCount[vehicle_type_for_full_swap2][vehicle_id_for_full_swap2])
                    {
                        max_vertex_cnt = RouteVertexCount[vehicle_type_for_full_swap2][vehicle_id_for_full_swap2];
                    }

                    for (vertex = 1; vertex <= max_vertex_cnt; vertex++)
                    {
                        vertex_to_swap = RouteVertices[vehicle_type_for_full_swap1][vehicle_id_for_full_swap1][vertex];
                        RouteVertices[vehicle_type_for_full_swap1][vehicle_id_for_full_swap1][vertex] = RouteVertices[vehicle_type_for_full_swap2][vehicle_id_for_full_swap2][vertex];
                        RouteVertices[vehicle_type_for_full_swap2][vehicle_id_for_full_swap2][vertex] = vertex_to_swap;
                    }

                    vertex_cnt_to_swap = RouteVertexCount[vehicle_type_for_full_swap1][vehicle_id_for_full_swap1];
                    RouteVertexCount[vehicle_type_for_full_swap1][vehicle_id_for_full_swap1] = RouteVertexCount[vehicle_type_for_full_swap2][vehicle_id_for_full_swap2];
                    RouteVertexCount[vehicle_type_for_full_swap2][vehicle_id_for_full_swap2] = vertex_cnt_to_swap;

                    EvaluateRoute(vehicle_type_for_full_swap1, vehicle_id_for_full_swap1);
                    EvaluateRoute(vehicle_type_for_full_swap2, vehicle_id_for_full_swap2);
                }
            } while (vehicle_type_to_swap1 != -1 || vehicle_type_to_relocate1 != -1 || vehicle_type_for_2opt1 != -1 || vehicle_type_for_chain_reversal != -1 || vehicle_type_for_full_swap1 != -1);

            Evaluate();
        }
    }

    public struct VehicleTypeData
    {
        public int Id
        {
            get; set;
        }

        public double Capacity
        {
            get; set;
        }

        public decimal FixedCostPerTrip
        {
            get; set;
        }

        public decimal CostPerUnitDistance
        {
            get; set;
        }

        public int NumberAvailable
        {
            get; set;
        }

        public double WorkStartTime
        {
            get; set;
        }

        public double DistanceLimit
        {
            get; set;
        }

        public double DrivingTimeLimit
        {
            get; set;
        }

        public double WorkingTimeLimit
        {
            get; set;
        }

        public int BaseId
        {
            get; set;
        }

        public int TypeId
        {
            get; set;
        }
    }

    public struct Candidate
    {
        public int Mandatory
        {
            get; set;
        }

        public decimal NetProfit
        {
            get; set;
        }

        public double TotalDistance
        {
            get; set;
        }

        public int VertexToBeAdded
        {
            get; set;
        }

        public int VehicleTypeIndex
        {
            get; set;
        }

        public int VehicleId
        {
            get; set;
        }

        public int Position
        {
            get; set;
        }
    }

    public struct VrpResult
    {
        public bool IsError
        {
            get; set;
        }

        public string Message
        {
            get; set;
        }

        public int Code
        {
            get; set;
        }

        public Solution Solution
        {
            get; set;
        }

        public List<string> Infeasibilities
        {
            get; set;
        }

        public object Attributes
        {
            get; set;
        }
    }
}
