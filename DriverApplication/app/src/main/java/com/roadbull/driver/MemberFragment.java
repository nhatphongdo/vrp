package com.roadbull.driver;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.zxing.common.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;


public class MemberFragment extends Fragment {

    private ArrayList<JSONObject> jobsList;
    private RouteItemsAdapter listAdapter;
    private ListView jobsListView;
    private ProgressDialog dialog;

    private TextView mTodayView;

    private Boolean needToRefresh;

    private Timer timer;
    private Boolean isLoadInfo;

    private ArrayList<Double> locations;
    private ArrayList<String> names;
    private ArrayList<String> consignmentNumbers;
    private ArrayList<Integer> types;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_member, container, false);

        jobsList = new ArrayList<JSONObject>();
        listAdapter = new RouteItemsAdapter(getActivity(), jobsList);
        jobsListView = (ListView) view.findViewById(R.id.jobsListView);
        jobsListView.setAdapter(listAdapter);

        mTodayView = (TextView) view.findViewById(R.id.time);

        // Load data
        needToRefresh = true;
        loadJobs();

        Button viewPins = (Button) view.findViewById(R.id.view_pins);
        viewPins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show dialog
                if (locations == null) {
                    return;
                }
                Intent intent = new Intent(getActivity(), MapDialogFragment.class);
                double[] loc = new double[locations.size()];
                for (int i = 0; i < loc.length; i++) {
                    loc[i] = locations.get(i).doubleValue();
                }
                String[] nameee = new String[names.size()];
                for (int i = 0; i < nameee.length; i++) {
                    nameee[i] = names.get(i);
                }
                String[] cnnn = new String[consignmentNumbers.size()];
                for (int i = 0; i < cnnn.length; i++) {
                    cnnn[i] = consignmentNumbers.get(i);
                }
                int[] typess = new int[types.size()];
                for (int i = 0; i < typess.length; i++) {
                    typess[i] = types.get(i).intValue();
                }
                intent.putExtra("Geolocations", loc);
                intent.putExtra("Names", nameee);
                intent.putExtra("Orders", cnnn);
                intent.putExtra("Types", typess);
                startActivity(intent);
            }
        });

        isLoadInfo = false;
        final Handler handler = new Handler();
        timer = new Timer(false);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadRemarks();
                    }
                });
            }
        };
        timer.schedule(timerTask, 10000, 10000);

        return view;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (needToRefresh) {
            loadJobs();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        needToRefresh = true;

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    private void loadJobs() {
        needToRefresh = false;
        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.message_loading), true);

        listAdapter.clear();
        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.SpecialJobs(token, new ApiServices.ServiceCallbackInterface() {

            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        if (getActivity() == null) {
                            return;
                        }
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getActivity().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    JSONArray jobs = result.getJSONArray("Result");

                    // If there is no job
                    if (jobs.length() == 0) {
                        showInfoAlert(R.string.message_no_job);
                        return;
                    }

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                    SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy");

                    mTodayView.setText(outputDateFormat.format(new Date()));

                    locations = new ArrayList<Double>();
                    names = new ArrayList<String>();
                    consignmentNumbers = new ArrayList<String>();
                    types = new ArrayList<Integer>();

                    for (int i = 0; i < jobs.length(); i++) {
                        JSONObject job = jobs.getJSONObject(i);
                        JSONArray orders = job.getJSONArray("Orders");
                        for (int j = 0; j < orders.length(); j++) {
                            JSONObject order = orders.getJSONObject(j);
                            JSONObject step = new JSONObject();
                            step.put("JobId", job.getString("Id"));
                            step.put("FromName", order.getString("FromName"));
                            step.put("FromCompany", order.optString("CustomerCompany", ""));
                            step.put("FromAddress", order.getString("FromAddress"));
                            step.put("FromMobilePhone", order.getString("FromMobilePhone"));
                            step.put("ToName", order.getString("ToName"));
                            step.put("ToAddress", order.getString("ToAddress"));
                            step.put("ToCompany", "");
                            step.put("ToMobilePhone", order.getString("ToMobilePhone"));
                            step.put("ConsignmentNumber", order.getString("ConsignmentNumber"));
                            step.put("Type", order.getInt("Type"));
                            step.put("Status", order.getInt("Status"));
                            step.put("SizeName", order.getString("SizeName"));
                            step.put("Cod", order.getString("CodOptionOfOrders"));
                            step.put("IsExchange", order.getBoolean("IsExchange"));
                            step.put("IsDone", false);
                            step.put("FromGeolocation", order.getJSONObject("FromGeolocation"));
                            step.put("ToGeolocation", order.getJSONObject("ToGeolocation"));
                            if (order.getInt("Type") == 0) {
                                locations.add(order.getJSONObject("FromGeolocation").optDouble("Latitude"));
                                locations.add(order.getJSONObject("FromGeolocation").optDouble("Longitude"));
                                names.add(order.getString("FromAddress"));
                                consignmentNumbers.add(order.getString("ConsignmentNumber"));
                            } else {
                                locations.add(order.getJSONObject("ToGeolocation").optDouble("Latitude"));
                                locations.add(order.getJSONObject("ToGeolocation").optDouble("Longitude"));
                                names.add(order.getString("ToAddress"));
                                consignmentNumbers.add(order.getString("ConsignmentNumber"));
                            }
                            types.add(order.getInt("Type"));
                            step.put("OperatorRemark", order.getString("OperatorRemark"));
                            step.put("ExtraRequests", order.getJSONArray("ExtraRequests"));
                            step.put("PickupTimeSlotId", order.isNull("PickupTimeSlotId") ? 0 : order.getInt("PickupTimeSlotId"));
                            step.put("DeliveryTimeSlotId", order.isNull("DeliveryTimeSlotId") ? 0 : order.get("DeliveryTimeSlotId"));
                            step.put("Timeslots", result.getJSONArray("Timeslots"));
                            step.put("HasRemark", false);
                            listAdapter.add(step);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.out.print(statusCode);
                error.printStackTrace();
                dialog.hide();
                showErrorAlert(R.string.error_unknown_error);
            }

        });
    }

    private void showInfoAlert(int messageResId) {
        if (getActivity() == null) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getTitle())
                .setMessage(getString(messageResId))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void showErrorAlert(int messageRedId) {
        if (getActivity() == null) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getTitle())
                .setMessage(getString(messageRedId))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void loadRemarks() {
        if (isLoadInfo == true || consignmentNumbers == null) {
            return;
        }
        isLoadInfo = true;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < consignmentNumbers.size(); i++) {
            if (i != 0) sb.append(",");
            sb.append(consignmentNumbers.get(i));
        }
        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.OrdersRemark(token, sb.toString(), new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                isLoadInfo = false;
                JSONObject result = (JSONObject) response;
                if (result.getInt("Code") != 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_arrive))
                            .setMessage(result.getString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return;
                }

                JSONArray orders = result.getJSONArray("Orders");
                for (int i = 0; i < listAdapter.getCount(); i++) {
                    for (int j = 0; j < orders.length(); j++) {
                        if (Arrays.asList(listAdapter.getItem(i).optString("ConsignmentNumber").split(",")).contains(orders.getJSONObject(j).optString("ConsignmentNumber"))) {
                            listAdapter.getItem(i).put("HasRemark", true);
                        }
                    }
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                isLoadInfo = false;
                error.printStackTrace();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_arrive))
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
