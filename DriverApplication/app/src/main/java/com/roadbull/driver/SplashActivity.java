package com.roadbull.driver;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ApiServices.UrlsSetting(new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.COMPANY_INFO_URL, ((JSONObject) response).optString("CompanyInfo"));
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.TERMS_AND_CONDITIONS_URL, ((JSONObject) response).optString("TermsAndConditions"));
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.NEWS_URL, ((JSONObject) response).optString("News"));

                ApiServices.RequestsSetting(new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.PICKUP_REQUESTS, ((JSONObject) response).optString("PickupRequests"));
                        GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.DELIVERY_REQUESTS, ((JSONObject) response).optString("DeliveryRequests"));
                        GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.FAILED_PICKUP_REASONS, ((JSONObject) response).optString("FailedPickupReasons"));
                        GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.FAILED_DELIVERY_REASONS, ((JSONObject) response).optString("FailedDeliveryReasons"));
                        GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.DRIVER_LOCATIONS_RADIUS, (float) ((JSONObject) response).optDouble("DriverLocationsRadius", 0));

                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        finish();
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        error.printStackTrace();
                        new AlertDialog.Builder(getContext())
                                .setTitle(getString(R.string.app_name))
                                .setMessage(getString(R.string.error_network_problem))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.app_name))
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private SplashActivity getContext() {
        return this;
    }

}
