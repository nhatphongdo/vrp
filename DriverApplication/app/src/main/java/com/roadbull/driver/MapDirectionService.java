package com.roadbull.driver;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MapDirectionService {

    interface DirectionCallbackInterface {

        void onSuccess(ArrayList<JSONObject> list);

        void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error);

    }

    public void getDirection(String start, String end, final DirectionCallbackInterface callback) {
        String url = "https://maps.googleapis.com/maps/api/directions/json?"
                + "origin=" + start + "&destination=" + end //+ "&key="
                + "&sensor=false&units=metric&mode=driving";

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    JSONObject json = new JSONObject(responseString);

                    if (!json.getString("status").equalsIgnoreCase("OK")) {
                        onFailure(statusCode, headers, responseBody, new Exception());
                    }

                    ArrayList<JSONObject> stepList = new ArrayList<JSONObject>();

                    JSONArray routes = json.getJSONArray("routes");
                    if (routes.length() > 0) {
                        JSONArray legs = routes.getJSONObject(0).getJSONArray("legs");
                        for (int i = 0; i < legs.length(); i++) {
                            JSONArray steps = legs.getJSONObject(i).getJSONArray("steps");
                            for (int j = 0; j < steps.length(); j++) {
                                String polyline = steps.getJSONObject(j).getJSONObject("polyline").getString("points");
                                ArrayList<LatLng> points = decodePoly(polyline);
                                JSONObject step = new JSONObject();
                                step.put("Points", points);
                                step.put("Instructions", steps.getJSONObject(j).getString("html_instructions"));
                                stepList.add(step);
                            }
                        }

                        if (callback != null) {
                            callback.onSuccess(stepList);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    private ArrayList<LatLng> decodePoly(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(position);
        }
        return poly;
    }
}