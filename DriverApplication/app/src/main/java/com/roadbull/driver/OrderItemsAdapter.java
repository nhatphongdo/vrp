package com.roadbull.driver;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Do Nhat Phong on 11/4/2015.
 */
public class OrderItemsAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private ArrayList<JSONObject> list;
    private final CompoundButton.OnCheckedChangeListener mCheckboxChanged;
    private final View.OnClickListener mConsignmentClicked;
    private AlertDialog mDialog;

    public OrderItemsAdapter(Context context, ArrayList<JSONObject> list, CompoundButton.OnCheckedChangeListener checkboxChanged, View.OnClickListener consignmentClicked) {
        super(context, R.layout.select_order_item, list);
        this.context = context;
        this.list = list;
        this.mCheckboxChanged = checkboxChanged;
        this.mConsignmentClicked = consignmentClicked;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.select_order_item, parent, false);

        TextView consignmentNumberView = (TextView) rowView.findViewById(R.id.consignmentNumber);
        TextView size = (TextView) rowView.findViewById(R.id.size);
        CheckBox selected = (CheckBox) rowView.findViewById(R.id.selected);

        try {
            JSONObject item = list.get(position);
            consignmentNumberView.setText(item.optString("ConsignmentNumber"));
            size.setText(item.optString("Size"));
            selected.setChecked(item.optBoolean("IsSelected", false));

            selected.setTag(item.optString("ConsignmentNumber"));

            if (item.optBoolean("IsDone", false)) {
                selected.setEnabled(false);
            }

            consignmentNumberView.setOnClickListener(mConsignmentClicked);
            selected.setOnCheckedChangeListener(mCheckboxChanged);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowView;
    }

    public void setDialog(AlertDialog dialog) {
        mDialog = dialog;
    }

    public AlertDialog getDialog() {
        return mDialog;
    }
}
