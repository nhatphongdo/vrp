package com.roadbull.driver;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class RegisterActivity extends AppCompatActivity implements View.OnFocusChangeListener, AdapterView.OnItemSelectedListener {

    // UI Reference
    private EditText mUserNameView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;
    private EditText mFullNameView;
    private EditText mEmailView;
    private Spinner mCountryCode;
    private EditText mMobileNumberView;
    private EditText mAddressView;
    private EditText mICNumberView;
    private EditText mDrivingLicenseView;
    private EditText mVehicleNumberView;
    private CheckBox mAgreementView;

    private CountrySpinnerAdapter countryCodeAdapter;

    private View mProgressView;
    private View mRegisterFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Load UI elements
        mUserNameView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);
        mConfirmPasswordView = (EditText) findViewById(R.id.reconfirm_password);
        mFullNameView = (EditText) findViewById(R.id.fullName);
        mEmailView = (EditText) findViewById(R.id.email);
        mCountryCode = (Spinner) findViewById(R.id.countryCode);
        mMobileNumberView = (EditText) findViewById(R.id.mobileNumber);
        mAddressView = (EditText) findViewById(R.id.address);
        mICNumberView = (EditText) findViewById(R.id.icNumber);
        mDrivingLicenseView = (EditText) findViewById(R.id.drivingLicense);
        mVehicleNumberView = (EditText) findViewById(R.id.vehicleNumber);
        mAgreementView = (CheckBox) findViewById(R.id.agreement);

        mRegisterFormView = findViewById(R.id.register_form_container);
        mProgressView = findViewById(R.id.register_progress);

        mVehicleNumberView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.register || id == EditorInfo.IME_NULL) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });

        mUserNameView.setOnFocusChangeListener(this);
        mPasswordView.setOnFocusChangeListener(this);
        mConfirmPasswordView.setOnFocusChangeListener(this);
        mFullNameView.setOnFocusChangeListener(this);
        mEmailView.setOnFocusChangeListener(this);
        mCountryCode.setOnFocusChangeListener(this);
        mMobileNumberView.setOnFocusChangeListener(this);
        mAddressView.setOnFocusChangeListener(this);
        mICNumberView.setOnFocusChangeListener(this);
        mDrivingLicenseView.setOnFocusChangeListener(this);
        mVehicleNumberView.setOnFocusChangeListener(this);

        Button signUpButton = (Button) findViewById(R.id.sign_up_button);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        // Load countries
        ApiServices.Countries(new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) {
                JSONArray countries = (JSONArray) response;
                ArrayList<JSONObject> list = new ArrayList<>();
                try {
                    for (int i = 0; i < countries.length(); i++) {
                        list.add(countries.getJSONObject(i));
                    }
                    // Create an ArrayAdapter using the string array and a default spinner layout
                    countryCodeAdapter = new CountrySpinnerAdapter(getContext(), android.R.layout.simple_spinner_item, list);
                    // Apply the adapter to the spinner
                    mCountryCode.setAdapter(countryCodeAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
            }
        });
    }

    private void attemptRegister() {
        // Reset errors
        mUserNameView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);
        mFullNameView.setError(null);
        mEmailView.setError(null);
        mMobileNumberView.setError(null);
        mAddressView.setError(null);
        mICNumberView.setError(null);
        mDrivingLicenseView.setError(null);
        mVehicleNumberView.setError(null);

        // Store values at the time of the register attempt.
        final String userName = mUserNameView.getText().toString();
        final String password = mPasswordView.getText().toString();
        final String confirmPassword = mConfirmPasswordView.getText().toString();
        final String fullName = mFullNameView.getText().toString();
        final String email = mEmailView.getText().toString();
        final String mobileNumber = mMobileNumberView.getText().toString();
        final String address = mAddressView.getText().toString();
        final String icNumber = mICNumberView.getText().toString();
        final String drivingLicense = mDrivingLicenseView.getText().toString();
        final String vehicleNumber = mVehicleNumberView.getText().toString();
        final String countryCode = ((JSONObject) mCountryCode.getSelectedItem()).optString("CountryCode");
        final boolean agreement = mAgreementView.isChecked();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid username
        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_username_required));
            focusView = mUserNameView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check confirm password
        if (TextUtils.isEmpty(confirmPassword)) {
            mConfirmPasswordView.setError(getString(R.string.error_invalid_confirm_password));
            focusView = mConfirmPasswordView;
            cancel = true;
        }
        if (!TextUtils.equals(password, confirmPassword)) {
            mConfirmPasswordView.setError(getString(R.string.error_not_match_passwords));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        // Check Full name
        if (TextUtils.isEmpty(fullName)) {
            mFullNameView.setError(getString(R.string.error_invalid_fullname));
            focusView = mFullNameView;
            cancel = true;
        }

        // Check email
        if (TextUtils.isEmpty(email) || !GlobalPreferences.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        // Check country code
        if (TextUtils.isEmpty(countryCode)) {
            mMobileNumberView.setError(getString(R.string.error_invalid_country_code));
            focusView = mMobileNumberView;
            cancel = true;
        }

        // Check mobile number
        if (TextUtils.isEmpty(mobileNumber) || !GlobalPreferences.isPhoneValid(mobileNumber)) {
            mMobileNumberView.setError(getString(R.string.error_invalid_mobile));
            focusView = mMobileNumberView;
            cancel = true;
        }

        // Check address
        if (TextUtils.isEmpty(address)) {
            mAddressView.setError(getString(R.string.error_invalid_address));
            focusView = mAddressView;
            cancel = true;
        }

        // Check IC number
        if (TextUtils.isEmpty(icNumber)) {
            mICNumberView.setError(getString(R.string.error_invalid_ic_number));
            focusView = mICNumberView;
            cancel = true;
        }

        // Check driving license
        if (TextUtils.isEmpty(drivingLicense)) {
            mDrivingLicenseView.setError(getString(R.string.error_invalid_driving_license));
            focusView = mDrivingLicenseView;
            cancel = true;
        }

        // Check vehicle number
        if (TextUtils.isEmpty(vehicleNumber)) {
            mVehicleNumberView.setError(getString(R.string.error_invalid_vehicle_number));
            focusView = mVehicleNumberView;
            cancel = true;
        }

        // Check agreement
        if (agreement == false) {
            mAgreementView.setError(getString(R.string.error_not_accept_agreement));
            focusView = mAgreementView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user register attempt.
            showProgress(true);

            ApiServices.Register(userName, password, email, fullName, countryCode, mobileNumber, address, icNumber, drivingLicense, vehicleNumber,
                    new ApiServices.ServiceCallbackInterface() {
                        @Override
                        public void onSuccess(int statusCode, Object response) {
                            try {
                                final JSONObject registerResult = (JSONObject) response;
                                if (registerResult.getInt("Code") != 0) {
                                    // Error
                                    showProgress(false);
                                    mUserNameView.setError(registerResult.getString("Message").replaceAll(" \\| ", "\n"));
                                    mUserNameView.requestFocus();
                                } else {
                                    // Register & Login successful, move to main screen
                                    // Remember for next login
                                    //GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_PASSWORD_KEY, mPasswordView.getText().toString());
                                    //GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.REMEMBER_ACCOUNT, true);

                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.TOKEN_KEY, registerResult.getString("Token"));
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_USERNAME_KEY, mUserNameView.getText().toString());
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_EMAIL_KEY, email);
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_ADDRESS_KEY, address);
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_COUNTRY_KEY, countryCode);
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_MOBILE_KEY, mobileNumber);
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_FULLNAME_KEY, fullName);
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_DRIVING_LICENSE_KEY, drivingLicense);
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_IC_NUMBER_KEY, icNumber);
                                    GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_VEHICLE_NUMBER_KEY, vehicleNumber);

                                    showProgress(false);
                                    finish();
                                    startActivity(new Intent(getContext(), MainActivity.class));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                showProgress(false);
                                mUserNameView.setError(getString(R.string.error_unknown_error));
                                mUserNameView.requestFocus();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            error.printStackTrace();
                            showProgress(false);
                            mUserNameView.setError(getString(R.string.error_network_problem));
                            mUserNameView.requestFocus();
                        }
                    });
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v.getId() == R.id.username || v.getId() == R.id.password || v.getId() == R.id.reconfirm_password
                || v.getId() == R.id.fullName || v.getId() == R.id.email || v.getId() == R.id.mobileNumber || v.getId() == R.id.address
                || v.getId() == R.id.icNumber || v.getId() == R.id.drivingLicense || v.getId() == R.id.vehicleNumber) && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        }
    }

    private RegisterActivity getContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            startActivity(new Intent(getContext(), LoginActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
