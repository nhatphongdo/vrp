package com.roadbull.driver;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class HistoryFragment extends ListFragment {

    private ArrayList<JSONObject> list;
    private HistoryItemsAdapter adapter;

    private ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.message_loading));
        dialog.setIndeterminate(true);

        list = new ArrayList<>();
        adapter = new HistoryItemsAdapter(getActivity(), list);
        setListAdapter(adapter);
        loadHistory();
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void loadHistory() {
        dialog.show();

        adapter.clear();
        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.History(token, new ApiServices.ServiceCallbackInterface() {

            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        if (getActivity() == null) {
                            return;
                        }
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getString(R.string.title_activity_staff))
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        return;
                    }

                    JSONArray jobs = result.getJSONArray("Result");

                    // If there is no job
                    if (jobs.length() == 0) {
                        dialog.hide();
                        return;
                    }

                    for (int i = 0; i < jobs.length(); i++) {
                        adapter.add(jobs.getJSONObject(i));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                if (getActivity() == null) {
                    return;
                }
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_staff))
                        .setMessage(getString(R.string.error_unknown_error))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
