package com.roadbull.driver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Do Nhat Phong on 11/4/2015.
 */
public class HistoryItemsAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private ArrayList<JSONObject> list;

    public HistoryItemsAdapter(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.history_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.history_item, parent, false);

        TextView vehicleNumber = (TextView) rowView.findViewById(R.id.vehicleNumber);
        TextView completedOn = (TextView) rowView.findViewById(R.id.completedOn);
        TextView pickupsDeliveries = (TextView) rowView.findViewById(R.id.pickupsDeliveries);
        TextView distances = (TextView) rowView.findViewById(R.id.distancesWorktimes);

        try {
            JSONObject item = list.get(position);

            vehicleNumber.setText(item.optString("VehicleNumber"));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            SimpleDateFormat convertFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            completedOn.setText(item.getString("CompletedOn").toLowerCase() == "null" ? "" : convertFormat.format(dateFormat.parse(item.getString("CompletedOn"))));
            pickupsDeliveries.setText(item.optInt("TotalPickups") + " / " + item.optInt("TotalDeliveries"));
            distances.setText(String.format("%.2f", item.optDouble("TotalDistances")) + " km / " + String.format("%.2f", item.optDouble("TotalWorkingTime")) + " hour(s)");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowView;
    }

}
