package com.roadbull.driver;

import android.*;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.generalscan.OnConnectFailListener;
import com.generalscan.OnConnectedListener;
import com.generalscan.OnDataReceive;
import com.generalscan.SendConstant;
import com.generalscan.bluetooth.BluetoothConnect;
import com.generalscan.bluetooth.BluetoothSend;
import com.generalscan.bluetooth.BluetoothSettings;
import com.generalscan.usb.UsbConnect;
import com.generalscan.usb.suspension.FloatWindowService;
import com.generalscan.usbcontroller.UsbConnectThread;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.IOException;


public class ScanQrFragment extends Fragment {

    private TextView mConsignmentNumberView;
    private TextView mFromNameView;
    private TextView mFromAddressView;
    private TextView mToNameView;
    private TextView mToAddressView;
    private TextView mServiceView;
    private TextView mProductTypeView;
    private TextView mSizeView;

    private ReadBroadcast mReadBroadcast;

    private static final int PERMISSIONS_REQUEST_CAMERA = 3;
    public static int OVERLAY_PERMISSION_REQ_CODE = 4;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(getActivity())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getActivity().getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
                return;
            }
        }

        startScanner();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scan_qr, container, false);

        mConsignmentNumberView = (TextView) view.findViewById(R.id.consignmentNumber);
        mFromNameView = (TextView) view.findViewById(R.id.from_name);
        mFromAddressView = (TextView) view.findViewById(R.id.from_address);
        mToNameView = (TextView) view.findViewById(R.id.to_name);
        mToAddressView = (TextView) view.findViewById(R.id.to_address);
        mServiceView = (TextView) view.findViewById(R.id.service);
        mProductTypeView = (TextView) view.findViewById(R.id.product_type);
        mSizeView = (TextView) view.findViewById(R.id.size);

        Button scanQrButton = (Button) view.findViewById(R.id.scan_qr_button);
        scanQrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
                } else {
                    IntentIntegrator.forFragment(getFragment()).initiateScan();
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
        }

        return view;
    }

    @Override
    public void onStart() {
        setBroadcast();
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mReadBroadcast != null) {
            getActivity().unregisterReceiver(mReadBroadcast);
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        BluetoothConnect.UnBindService(getActivity());
        super.onDestroy();
    }

    private Fragment getFragment() {
        return this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(getActivity())) {
                    startScanner();
                }
            }
        }
        else {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanResult != null) {
                // handle scan result
                processScanResult(scanResult.getContents());
            }
        }
    }

    private void startScanner() {
        BluetoothConnect.BindService(getActivity());
        //UsbConnect.BindService(getActivity());

        //BluetoothSettings.SetScaner(getActivity());

        BluetoothConnect.SetOnDataReceive(new OnDataReceive() {
            @Override
            public void DataReceive(String s) {
                System.out.println("Scanner data: " + s);
            }
        });

        BluetoothConnect.SetOnConnectFailListener(new OnConnectFailListener() {
            @Override
            public void ConnectFail() {
                System.out.println("Connect failed");
            }
        });

        BluetoothConnect.SetOnConnectedListener(new OnConnectedListener() {

            @Override
            public void Connected() {
                System.out.println("Connected");
            }

        });

        new Thread() {

            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                BluetoothConnect.Connect();
            }

        }.start();
    }

    private void processScanResult(String result) {
        String[] parts = result == null ? new String[]{} : result.split("\\|");
        if (parts.length >= 10) {
            mConsignmentNumberView.setText(parts[1]);
            mFromNameView.setText(parts[2]);
            mFromAddressView.setText(parts[3]);
            mToNameView.setText(parts[4]);
            mToAddressView.setText(parts[5]);
            mServiceView.setText(parts[7]);
            mProductTypeView.setText(parts[8]);
            mSizeView.setText(parts[9]);
        } else if (parts.length >= 8) {
            mConsignmentNumberView.setText(parts[1]);
            mFromNameView.setText(parts[2]);
            mToNameView.setText(parts[3]);
            mServiceView.setText(parts[5]);
            mProductTypeView.setText(parts[6]);
            mSizeView.setText(parts[7]);
        }
    }

    private void setBroadcast() {
        mReadBroadcast = new ReadBroadcast();
        IntentFilter filter = new IntentFilter();
        filter.addAction(SendConstant.GetDataAction);
        filter.addAction(SendConstant.GetReadDataAction);
        filter.addAction(SendConstant.GetBatteryDataAction);
        getActivity().registerReceiver(mReadBroadcast, filter);
    }

    public class ReadBroadcast extends BroadcastReceiver {

        public ReadBroadcast() {

        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SendConstant.GetBatteryDataAction)) {

                String data = intent.getStringExtra(SendConstant.GetBatteryData);

            }
            if (intent.getAction().equals(SendConstant.GetDataAction)) {

                String data = intent.getStringExtra(SendConstant.GetData);

                processScanResult(data);
            }
            if (intent.getAction().equals(SendConstant.GetReadDataAction)) {
                String name = intent.getStringExtra(SendConstant.GetReadName);
                String data = intent.getStringExtra(SendConstant.GetReadData);

            }
        }

    }
}
