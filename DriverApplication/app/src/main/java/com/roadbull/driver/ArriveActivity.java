package com.roadbull.driver;

import android.*;
import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.generalscan.SendConstant;
import com.generalscan.bluetooth.BluetoothConnect;
import com.generalscan.usb.UsbConnect;
import com.generalscan.usb.suspension.FloatWindowService;
import com.generalscan.usbcontroller.UsbConnectThread;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

import static android.os.Environment.DIRECTORY_PICTURES;
import static android.os.Environment.getExternalStoragePublicDirectory;

public class ArriveActivity extends AppCompatActivity {

    private static final String ARG_CONSIGNMENT_NUMBER = "CONSIGNMENT_NUMBER";
    private static final String ARG_SERVICE = "SERVICE";
    private static final String ARG_JOB_ID = "JOB_ID";
    private static final String ARG_CUSTOMER_NAME = "CUSTOMER_NAME";
    private static final String ARG_CUSTOMER_ADDRESS = "CUSTOMER_ADDRESS";
    private static final String ARG_CUSTOMER_MOBILE_PHONE = "CUSTOMER_MOBILE_PHONE";
    private static final String ARG_CUSTOMER_COMPANY = "CUSTOMER_COMPANY";
    private static final String ARG_SIZE = "ARG_SIZE";
    private static final String ARG_ORDERS = "ORDERS";
    private static final String ARG_OPERATOR_REMARK = "OPERATOR_REMARK";
    private static final String ARG_EXTRA_REQUESTS = "EXTRA_REQUESTS";
    private static final String ARG_STATUS = "STATUS";
    private static final String ARG_COD = "COD";
    private static final String ARG_EXCHANGE = "EXCHANGE";

    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_TAKE_REASON_PHOTO = 2;
    private static final int PERMISSIONS_REQUEST_CALL_PHONE = 1;
    private static final int PERMISSIONS_REQUEST_WRITE_STORAGE = 2;
    private static final int PERMISSIONS_REQUEST_CAMERA = 3;

    private String mConsignmentNumber;
    private String mService;
    private String mJobId;
    private String mCustomerName;
    private String mCustomerAddress;
    private String mCustomerCompany;
    private String mCustomerMobilePhone;
    private String mSize;
    private JSONArray mOrders;
    private String mOperatorRemark;
    private JSONArray mExtraRequests;
    private int mStatus;
    private JSONArray mCods;
    private Boolean mIsExchange;

    private TextView mServiceView;
    private TextView mSizeView;
    private TextView mConsignmentNumberView;
    private TextView mJobIdView;
    private TextView mTypeTextView;
    private EditText mCustomerNameView;
    private EditText mCustomerCompanyView;
    private EditText mCustomerAddressView;
    private EditText mCustomerMobilePhoneView;
    private EditText mFullNameView;
    private TextView mOpsRemark;
    private TextView mRequest;
    private SignaturePad mSignaturePad;
    private ImageButton mCaptureButton;
    private RatingBar mRatingBar;
    private TextView mRatingText;
    private Button optionButton;
    private View mRateView;
    private TextView mSuccessTextInfo;
    private ImageButton mCaptureSuccessImage;
    private Button failedOptionButton;
    private EditText mFailedReason;
    private View mCodView;
    private TextView mCod;
    private View mExchangeView;
    private TextView mExchange;

    private String[] consignmentNumbers;
    private int currentConsignmentNumberIdx;
    private int successfulConsignmentNumbers;
    private int failedConsignmentNumbers;
    private ArrayList<String> undoneConsignmentNumbers;
    private ArrayList<String> successConsignmentNumbers;

    private Button arriveButton;
    private Button failedButton;

    private View mProgressView;
    private ScrollView mSubmitView;

    private View mArrivedView;
    private View mFailedView;

    private Bitmap mProofImage;
    private Bitmap mSavedSignature;

    private Boolean isLoadInfo;

    private OrderItemsAdapter ordersAdapter;
    private String currentViewCN;

    private OptionItemsAdapter optionsAdapter;
    private int currentReasonIndex;

    private File photoFile;
    private Boolean isNoClient;

    private Timer timer;

    private ReadBroadcast mReadBroadcast;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public static Intent createInstance(Context context, String consignmentNumber, String service, String jobId,
                                        String customerName, String customerAddress, String customerMobilePhone, String company,
                                        String size, String orders, String operatorRemark, String extraRequests, int status, String cod, Boolean isExchange) {
        Intent intent = new Intent(context, ArriveActivity.class);
        intent.putExtra(ARG_CONSIGNMENT_NUMBER, consignmentNumber);
        intent.putExtra(ARG_SERVICE, service);
        intent.putExtra(ARG_JOB_ID, jobId);
        intent.putExtra(ARG_CUSTOMER_NAME, customerName);
        intent.putExtra(ARG_CUSTOMER_ADDRESS, customerAddress);
        intent.putExtra(ARG_CUSTOMER_MOBILE_PHONE, customerMobilePhone);
        intent.putExtra(ARG_CUSTOMER_COMPANY, company);
        intent.putExtra(ARG_SIZE, size);
        intent.putExtra(ARG_ORDERS, orders);
        intent.putExtra(ARG_OPERATOR_REMARK, operatorRemark);
        intent.putExtra(ARG_EXTRA_REQUESTS, extraRequests);
        intent.putExtra(ARG_STATUS, status);
        intent.putExtra(ARG_COD, cod);
        intent.putExtra(ARG_EXCHANGE, isExchange);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrive);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        isLoadInfo = false;
        isNoClient = false;

        Intent intent = getIntent();
        mConsignmentNumber = intent.getStringExtra(ARG_CONSIGNMENT_NUMBER);
        mService = intent.getStringExtra(ARG_SERVICE);
        mJobId = intent.getStringExtra(ARG_JOB_ID);
        mCustomerName = intent.getStringExtra(ARG_CUSTOMER_NAME);
        mCustomerAddress = intent.getStringExtra(ARG_CUSTOMER_ADDRESS);
        mCustomerMobilePhone = intent.getStringExtra(ARG_CUSTOMER_MOBILE_PHONE);
        mCustomerCompany = intent.getStringExtra(ARG_CUSTOMER_COMPANY);
        mSize = intent.getStringExtra(ARG_SIZE);
        mOperatorRemark = intent.getStringExtra(ARG_OPERATOR_REMARK);
        mStatus = intent.getIntExtra(ARG_STATUS, 0);

        try {
            mCods = new JSONArray(intent.getStringExtra(ARG_COD));
        } catch (JSONException e) {
            mCods = new JSONArray();
            e.printStackTrace();
        }
        mIsExchange = intent.getBooleanExtra(ARG_EXCHANGE, false);
        try {
            mExtraRequests = new JSONArray(intent.getStringExtra(ARG_EXTRA_REQUESTS));
        } catch (JSONException e) {
            mExtraRequests = new JSONArray();
            e.printStackTrace();
        }
        try {
            mOrders = new JSONArray(intent.getStringExtra(ARG_ORDERS));
        } catch (JSONException e) {
            mOrders = new JSONArray();
            // Add order from detail info
            for (String s : mConsignmentNumber.split(",")) {
                try {
                    JSONObject order = new JSONObject();
                    order.put("ConsignmentNumber", s);
                    order.put("Status", mStatus);
                    JSONObject size = new JSONObject();
                    size.put("SizeName", mSize);
                    order.put("Size", size);
                    order.put("CodOptionOfOrders", mCods);
                    order.put("IsExchange", mIsExchange);
                    mOrders.put(order);
                } catch (Exception ex) {
                }
            }
            e.printStackTrace();
        }

        mTypeTextView = (TextView) findViewById(R.id.typeTextView);
        mServiceView = (TextView) findViewById(R.id.assignedJob);
        mSizeView = (TextView) findViewById(R.id.sizeTextView);
        mConsignmentNumberView = (TextView) findViewById(R.id.consignmentNumber);
        mJobIdView = (TextView) findViewById(R.id.jobNumber);
        mCustomerNameView = (EditText) findViewById(R.id.receiverName);
        mCustomerCompanyView = (EditText) findViewById(R.id.companyName);
        mCustomerAddressView = (EditText) findViewById(R.id.receiverAddress);
        mCustomerMobilePhoneView = (EditText) findViewById(R.id.receiverContactNumber);
        mFullNameView = (EditText) findViewById(R.id.fullName);
        mOpsRemark = (TextView) findViewById(R.id.ops_remark);
        mRequest = (TextView) findViewById(R.id.request);
        mFailedReason = (EditText) findViewById(R.id.reason);
        mCodView = findViewById(R.id.cod_view);
        mCod = (TextView) findViewById(R.id.cod);
        mExchangeView = findViewById(R.id.exchange_view);
        mExchange = (TextView) findViewById(R.id.exchange);

        mArrivedView = findViewById(R.id.arrivedBox);
        mFailedView = findViewById(R.id.failedBox);

        mProgressView = findViewById(R.id.submit_progress);
        mSubmitView = (ScrollView) findViewById(R.id.submit_view);
        final Button submitArriveButton = (Button) findViewById(R.id.submit_arrive_button);

        mProofImage = null;

        submitArriveButton.setTag(2);

        mServiceView.setText(mService);
        mOpsRemark.setText(mOperatorRemark);
        if (mService.equalsIgnoreCase("Pickup")) {
            mServiceView.setTextColor(Color.RED);
            mTypeTextView.setText(R.string.pickup_from);
            setTitle(getString(R.string.title_pickup));
            try {
                for (int i = 0; i < mExtraRequests.length(); i++) {
                    if (mExtraRequests.getJSONObject(i).getInt("RequestType") == 1 || mExtraRequests.getJSONObject(i).getInt("RequestType") == 3) {
                        mRequest.setText(mExtraRequests.getJSONObject(i).isNull("RequestContent") ? "" : mExtraRequests.getJSONObject(i).getString("RequestContent"));
                    }
                }
            } catch (Exception exc) {
            }
        } else {
            mServiceView.setTextColor(Color.parseColor("#2fa33b"));
            mTypeTextView.setText(R.string.delivery_to);
            setTitle(getString(R.string.title_delivery));
            try {
                for (int i = 0; i < mExtraRequests.length(); i++) {
                    if (mExtraRequests.getJSONObject(i).getInt("RequestType") == 0 || mExtraRequests.getJSONObject(i).getInt("RequestType") == 2) {
                        mRequest.setText(mExtraRequests.getJSONObject(i).isNull("RequestContent") ? "" : mExtraRequests.getJSONObject(i).getString("RequestContent"));
                    }
                }

                String text = "";
                Boolean hasExchange = false;
                DecimalFormat format = new DecimalFormat("$#,###,###.##");
                for (int i = 0; i < mOrders.length(); i++) {
                    JSONArray cods = mOrders.getJSONObject(i).getJSONArray("CodOptionOfOrders");
                    for (int j = 0; j < cods.length(); j++) {
                        text += (cods.getJSONObject(j).optJSONObject("CodOption").optInt("Type") == 0 ? "Cash " : "Cheque ")
                                + format.format(cods.getJSONObject(j).getDouble("Amount"));

                        if (j < cods.length() - 1) {
                            text += "<br>";
                        }
                    }

                    if (i < mOrders.length() - 1 && cods.length() > 0) {
                        text += "<br>";
                    }

                    if (mOrders.getJSONObject(i).getBoolean("IsExchange")) {
                        hasExchange = true;
                    }
                }

                if (text != "") {
                    mCodView.setVisibility(View.VISIBLE);
                    mCod.setText(Html.fromHtml(text));
                }
                if (hasExchange) {
                    mExchangeView.setVisibility(View.VISIBLE);
                    mExchange.setText("Yes");
                }
            } catch (Exception exc) {
            }
        }

        mCustomerMobilePhoneView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getContext(), new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_CALL_PHONE);
                    return true;
                }
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mCustomerMobilePhoneView.getText()));
                startActivity(intent);
                return true;
            }
        });

        currentViewCN = "";
        undoneConsignmentNumbers = new ArrayList<>();
        successConsignmentNumbers = new ArrayList<>();
        mConsignmentNumberView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show popup with Consignment numbers
                final ArrayList<JSONObject> orders = new ArrayList<JSONObject>();
                try {
                    for (String cn : consignmentNumbers) {
                        for (int j = 0; j < mOrders.length(); j++) {
                            JSONObject order = mOrders.getJSONObject(j);
                            if (order.optString("ConsignmentNumber").equalsIgnoreCase(cn)) {
                                JSONObject item = new JSONObject();
                                item.put("ConsignmentNumber", order.optString("ConsignmentNumber"));
                                item.put("Size", order.optJSONObject("Size").optString("SizeName"));
                                item.put("IsSelected", successConsignmentNumbers.contains(cn));
                                item.put("IsDone", !undoneConsignmentNumbers.contains(cn));
                                orders.add(item);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Consignment Numbers");
                ordersAdapter = null;
                ordersAdapter = new OrderItemsAdapter(getContext(), orders, new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        String cn = (String) buttonView.getTag();
                        if (isChecked) {
                            successConsignmentNumbers.add(cn);
                        } else {
                            successConsignmentNumbers.remove(cn);
                        }
                        for (int i = 0; i < orders.size(); i++) {
                            if (orders.get(i).optString("ConsignmentNumber").equalsIgnoreCase(cn)) {
                                try {
                                    orders.get(i).put("IsSelected", isChecked);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String cn = ((TextView) v).getText().toString();
                        try {
                            for (int j = 0; j < mOrders.length(); j++) {
                                JSONObject order = mOrders.getJSONObject(j);
                                if (order.optString("ConsignmentNumber").equalsIgnoreCase(cn)) {
                                    currentConsignmentNumberIdx = j;
                                    mConsignmentNumberView.setText(String.format("%s (%d items)",
                                            consignmentNumbers[currentConsignmentNumberIdx], consignmentNumbers.length));
                                    loadInfo();
                                    break;
                                }
                            }

                            if (ordersAdapter != null && ordersAdapter.getDialog() != null) {
                                ordersAdapter.getDialog().dismiss();
                            }
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }
                    }
                });

                builder.setAdapter(ordersAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();

                ordersAdapter.setDialog(dialog);

                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Submit",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                // Update info
                                if (mService.equalsIgnoreCase("Pickup")) {
                                    if (arriveButton != null) {
                                        arriveButton.setText(getString(R.string.successful_pickup) + "\n" +
                                                String.format("%d/%d", successConsignmentNumbers.size() + successfulConsignmentNumbers, consignmentNumbers.length));
                                    }
                                    if (failedButton != null) {
                                        failedButton.setText(getString(R.string.action_failed_pickup) + "\n" +
                                                String.format("%d/%d", consignmentNumbers.length - successConsignmentNumbers.size() - successfulConsignmentNumbers + failedConsignmentNumbers, consignmentNumbers.length));
                                    }
                                } else {
                                    if (arriveButton != null) {
                                        arriveButton.setText(getString(R.string.successful_delivery) + "\n" +
                                                String.format("%d/%d", successConsignmentNumbers.size() + successfulConsignmentNumbers, consignmentNumbers.length));
                                    }
                                    if (failedButton != null) {
                                        failedButton.setText(getString(R.string.action_failed_delivery) + "\n" +
                                                String.format("%d/%d", consignmentNumbers.length - successConsignmentNumbers.size() - successfulConsignmentNumbers + failedConsignmentNumbers, consignmentNumbers.length));
                                    }
                                }
                                mJobIdView.setText(String.format("%s (%d/%d are successful)", mJobId, successfulConsignmentNumbers + successConsignmentNumbers.size(), consignmentNumbers.length));
                                if (successConsignmentNumbers.size() == 0) {
                                    arriveButton.setBackgroundColor(Color.parseColor("#333333"));
                                    arriveButton.setEnabled(false);
                                } else {
                                    arriveButton.setBackgroundColor(Color.parseColor("#2fa33b"));
                                    arriveButton.setEnabled(true);
                                }
                                if (successConsignmentNumbers.size() == consignmentNumbers.length - successfulConsignmentNumbers - failedConsignmentNumbers) {
                                    failedButton.setBackgroundColor(Color.parseColor("#333333"));
                                    failedButton.setEnabled(false);
                                } else {
                                    failedButton.setBackgroundColor(Color.parseColor("#fd0035"));
                                    failedButton.setEnabled(true);
                                }
                            }
                        });

                dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Scan QR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getContext(), new String[]{android.Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
                                } else {
                                    new IntentIntegrator(getContext()).initiateScan();
                                }
                            }
                        });

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button submit_button = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                        if (submit_button != null) {
                            submit_button.setBackgroundColor(Color.parseColor("#df872f"));
                            submit_button.setTextColor(Color.WHITE);
                        }

                        Button scan_button = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEUTRAL);
                        if (scan_button != null) {
                            scan_button.setBackgroundColor(Color.parseColor("#213f79"));
                            scan_button.setTextColor(Color.WHITE);
                        }

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                        submit_button.setLayoutParams(params);
                        submit_button.setPadding(10, 0, 10, 0);
                        scan_button.setLayoutParams(params);
                        scan_button.setPadding(10, 0, 10, 0);

                        submit_button.invalidate();
                        scan_button.invalidate();

                        if (currentViewCN != null && currentViewCN != "") {
                            for (int i = 0; i < consignmentNumbers.length; i++) {
                                if (consignmentNumbers[i].equalsIgnoreCase(currentViewCN)) {
                                    ((AlertDialog) dialog).getListView().smoothScrollToPosition(i);
                                }
                            }
                        }
                    }
                });

                dialog.show();
            }
        });

        consignmentNumbers = mConsignmentNumber.split(",");
        currentConsignmentNumberIdx = 0;
        successfulConsignmentNumbers = 0;
        failedConsignmentNumbers = 0;

        // Find first not process order
        try {
            for (int j = 0; j < mOrders.length(); j++) {
                JSONObject order = mOrders.getJSONObject(j);
                if ((mService.equalsIgnoreCase("Pickup") && order.getInt("Status") != 12 && order.getInt("Status") != 10) ||
                        (!mService.equalsIgnoreCase("Pickup") && order.getInt("Status") != 6 && order.getInt("Status") != 11)) {
                    if (order.getInt("Status") != 4 && order.getInt("Status") != 9) {
                        ++successfulConsignmentNumbers;
                    } else {
                        ++failedConsignmentNumbers;
                    }
                } else {
                    // Not done
                    undoneConsignmentNumbers.add(order.optString("ConsignmentNumber"));
                }
            }

            boolean found;
            do {
                if (currentConsignmentNumberIdx >= consignmentNumbers.length) {
                    break;
                }
                found = false;
                for (int j = 0; j < mOrders.length(); j++) {
                    JSONObject order = mOrders.getJSONObject(j);
                    if (order.optString("ConsignmentNumber").equalsIgnoreCase(consignmentNumbers[currentConsignmentNumberIdx])) {
                        if ((mService.equalsIgnoreCase("Pickup") && order.getInt("Status") != 12 && order.getInt("Status") != 10) ||
                                (!mService.equalsIgnoreCase("Pickup") && order.getInt("Status") != 6 && order.getInt("Status") != 11)) {
                            // Already done
                            ++currentConsignmentNumberIdx;
                            found = true;
                            break;
                        }
                    }
                }
            }
            while (found);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (currentConsignmentNumberIdx >= consignmentNumbers.length) {
            // Finish
            finish();
            return;
        }

        mConsignmentNumberView.setText(String.format("%s (%d items)",
                consignmentNumbers[currentConsignmentNumberIdx], consignmentNumbers.length));
//        mConsignmentNumberView.setText(String.format("%d items", consignmentNumbers.length));
        loadInfo();

        mJobIdView.setText(String.format("%s (%d/%d are successful)", mJobId, successfulConsignmentNumbers, consignmentNumbers.length));
        mCustomerNameView.setText(mCustomerName);
        mCustomerMobilePhoneView.setText(mCustomerMobilePhone);
        mCustomerAddressView.setText(mCustomerAddress);
        if (mCustomerCompany != null && !mCustomerCompany.equalsIgnoreCase("null")) {
            mCustomerCompanyView.setText(mCustomerCompany);
        }
        mSizeView.setText(mSize);

        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onSigned() {
                // Event triggered when the pad is signed
                mProofImage = mSignaturePad.getTransparentSignatureBitmap();
                if (mSavedSignature != null) {
                    mSavedSignature.recycle();
                    mSavedSignature = null;
                }
                mSavedSignature = Bitmap.createScaledBitmap(mProofImage, mProofImage.getWidth(), mProofImage.getHeight(), false);
            }

            @Override
            public void onClear() {
                // Event triggered when the pad is cleared
                mProofImage = null;
            }
        });

        mCaptureButton = (ImageButton) findViewById(R.id.captureImage);
        mCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        new AlertDialog.Builder(getContext())
                                .setTitle(getString(R.string.title_activity_arrive))
                                .setMessage(getString(R.string.error_access_memory))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }
                }
            }
        });

        mSuccessTextInfo = (TextView) findViewById(R.id.success_text_info);
        mCaptureSuccessImage = (ImageButton) findViewById(R.id.captureSuccessImage);
        mCaptureSuccessImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        new AlertDialog.Builder(getContext())
                                .setTitle(getString(R.string.title_activity_arrive))
                                .setMessage(getString(R.string.error_access_memory))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_REASON_PHOTO);
                    }
                }
            }
        });

        mRateView = findViewById(R.id.rate_view);
        mRatingText = (TextView) findViewById(R.id.rate_text);
        mRatingBar = (RatingBar) findViewById(R.id.ratingBar);
        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                // Rated
                mRatingText.setText(getString(R.string.you_rated_me));
                mRatingBar.setIsIndicator(true);
                mRateView.setBackgroundColor(Color.parseColor("#00000000"));
                mSignaturePad.setVisibility(View.VISIBLE);
                mSuccessTextInfo.setText(getString(R.string.signature_info));
                mCaptureSuccessImage.setVisibility(View.GONE);
                isNoClient = false;
                if (mCodView.getVisibility() == View.VISIBLE && mExchangeView.getVisibility() == View.VISIBLE) {
                    submitArriveButton.setText("Item & Money Collected?");
                    submitArriveButton.setTag(1);
                } else if (mCodView.getVisibility() == View.VISIBLE) {
                    submitArriveButton.setText("Cash/Cheque Collected?");
                    submitArriveButton.setTag(1);
                } else if (mExchangeView.getVisibility() == View.VISIBLE) {
                    submitArriveButton.setText("Item Collected?");
                    submitArriveButton.setTag(1);
                }
                if ((int) submitArriveButton.getTag() == 1) {
                    submitArriveButton.setBackgroundColor(Color.parseColor("#ff0000"));
                    submitArriveButton.setTextColor(Color.parseColor("#ffff00"));
                }
            }
        });

        currentReasonIndex = -1;
        optionButton = (Button) findViewById(R.id.successful_option_button);
        optionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ArrayList<JSONObject> options = new ArrayList<JSONObject>();
                try {
                    String[] requests;
                    if (mService.equalsIgnoreCase("Pickup")) {
                        requests = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.PICKUP_REQUESTS, "").split("\n");
                    } else {
                        requests = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.DELIVERY_REQUESTS, "").split("\n");
                    }

                    for (int i = 0; i < requests.length; i++) {
                        JSONObject item = new JSONObject();
                        item.put("Text", requests[i]);
                        item.put("IsSelected", i == currentReasonIndex);
                        item.put("IsOther", false);
                        options.add(item);
                    }

                    JSONObject item = new JSONObject();
                    item.put("Text", "");
                    item.put("IsSelected", currentReasonIndex == requests.length);
                    item.put("IsOther", true);
                    options.add(item);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Options");
                optionsAdapter = null;
                optionsAdapter = new OptionItemsAdapter(getContext(), options, new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        String reason = (String) buttonView.getTag();
                        if (isChecked) {
                            // Unselect the others
                            for (int i = 0; i < options.size(); i++) {
                                try {
                                    if (!options.get(i).optString("Text").equalsIgnoreCase(reason)) {
                                        options.get(i).put("IsSelected", false);
                                    } else {
                                        options.get(i).put("IsSelected", true);
                                        currentReasonIndex = i;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            optionsAdapter.notifyDataSetChanged();
                        }
                    }
                }, new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            options.get(options.size() - 1).put("Text", s);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                builder.setAdapter(optionsAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();

                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Submit",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (currentReasonIndex < 0 || currentReasonIndex >= options.size()) {
                                    return;
                                }
                                mSignaturePad.setVisibility(View.GONE);
                                mSuccessTextInfo.setText(getString(R.string.capture_info));
                                mCaptureSuccessImage.setVisibility(View.VISIBLE);
                                TextInputLayout fullNameInput = (TextInputLayout) findViewById(R.id.fullName_Input);
                                fullNameInput.setHint("Reason");
                                mFullNameView.setText(options.get(currentReasonIndex).optString("Text"));
                                mFullNameView.setEnabled(false);
                                isNoClient = true;
                            }
                        });

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button submit_button = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                        if (submit_button != null) {
                            submit_button.setBackgroundColor(Color.parseColor("#df872f"));
                            submit_button.setTextColor(Color.WHITE);
                        }

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                        submit_button.setLayoutParams(params);
                        submit_button.setPadding(10, 0, 10, 0);

                        submit_button.invalidate();
                    }
                });

                dialog.show();
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
        });

        failedOptionButton = (Button) findViewById(R.id.failed_option_button);
        failedOptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ArrayList<JSONObject> options = new ArrayList<JSONObject>();
                try {
                    String[] requests;
                    if (mService.equalsIgnoreCase("Pickup")) {
                        requests = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.FAILED_PICKUP_REASONS, "").split("\n");
                    } else {
                        requests = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.FAILED_DELIVERY_REASONS, "").split("\n");
                    }

                    for (int i = 0; i < requests.length; i++) {
                        JSONObject item = new JSONObject();
                        item.put("Text", requests[i]);
                        item.put("IsSelected", i == currentReasonIndex);
                        item.put("IsOther", false);
                        options.add(item);
                    }

                    JSONObject item = new JSONObject();
                    item.put("Text", "");
                    item.put("IsSelected", currentReasonIndex == requests.length);
                    item.put("IsOther", true);
                    options.add(item);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Options");
                optionsAdapter = null;
                optionsAdapter = new OptionItemsAdapter(getContext(), options, new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        String reason = (String) buttonView.getTag();
                        if (isChecked) {
                            // Unselect the others
                            for (int i = 0; i < options.size(); i++) {
                                try {
                                    if (!options.get(i).optString("Text").equalsIgnoreCase(reason)) {
                                        options.get(i).put("IsSelected", false);
                                    } else {
                                        options.get(i).put("IsSelected", true);
                                        currentReasonIndex = i;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            optionsAdapter.notifyDataSetChanged();
                        }
                    }
                }, new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            options.get(options.size() - 1).put("Text", s);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                builder.setAdapter(optionsAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();

                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Submit",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (currentReasonIndex < 0 || currentReasonIndex >= options.size()) {
                                    return;
                                }
                                mFailedReason.setText(options.get(currentReasonIndex).optString("Text"));
                            }
                        });

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button submit_button = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                        if (submit_button != null) {
                            submit_button.setBackgroundColor(Color.parseColor("#df872f"));
                            submit_button.setTextColor(Color.WHITE);
                        }

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                        submit_button.setLayoutParams(params);
                        submit_button.setPadding(10, 0, 10, 0);

                        submit_button.invalidate();
                    }
                });

                dialog.show();
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
        });

        arriveButton = (Button) findViewById(R.id.arrived_button);
        arriveButton.setEnabled(false);
        arriveButton.setBackgroundColor(Color.parseColor("#333333"));
        arriveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // arriveButton.setVisibility(View.GONE);
                // failedButton.setVisibility(View.GONE);
                mArrivedView.setVisibility(View.VISIBLE);
                mFailedView.setVisibility(View.GONE);
                currentReasonIndex = -1;
                mSubmitView.post(new Runnable() {
                    @Override
                    public void run() {
                        mSubmitView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });

                // Load saved signature
                if (mSavedSignature != null) {
                    mProofImage = Bitmap.createScaledBitmap(mSavedSignature, mSavedSignature.getWidth(), mSavedSignature.getHeight(), false);
                    mSignaturePad.setSignatureBitmap(mProofImage);
                }
            }
        });

        failedButton = (Button) findViewById(R.id.failed_button);
        failedButton.setEnabled(false);
        failedButton.setBackgroundColor(Color.parseColor("#333333"));
        failedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // arriveButton.setVisibility(View.GONE);
                // failedButton.setVisibility(View.GONE);
                mArrivedView.setVisibility(View.GONE);
                mFailedView.setVisibility(View.VISIBLE);
                currentReasonIndex = -1;
                mSubmitView.post(new Runnable() {
                    @Override
                    public void run() {
                        mSubmitView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });
            }
        });

        if (mService.equalsIgnoreCase("Pickup")) {
            arriveButton.setText(getString(R.string.successful_pickup) + "\n" + String.format("%d/%d", successfulConsignmentNumbers, consignmentNumbers.length));
            failedButton.setText(getString(R.string.action_failed_pickup) + "\n" + String.format("%d/%d", consignmentNumbers.length - successfulConsignmentNumbers - undoneConsignmentNumbers.size(), consignmentNumbers.length));
        } else {
            arriveButton.setText(getString(R.string.successful_delivery) + "\n" + String.format("%d/%d", successfulConsignmentNumbers, consignmentNumbers.length));
            failedButton.setText(getString(R.string.action_failed_delivery) + "\n" + String.format("%d/%d", consignmentNumbers.length - successfulConsignmentNumbers - undoneConsignmentNumbers.size(), consignmentNumbers.length));
        }

        submitArriveButton.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
            public void onSwipeLeft() {
                submitArriveButton.setBackgroundColor(Color.parseColor("#303F9F"));
                submitArriveButton.setTextColor(Color.parseColor("#ffffff"));
                submitArriveButton.setText("Submit");
                submitArriveButton.setTag(2);
            }

            public void onTap() {
                if ((int) submitArriveButton.getTag() == 2) {
                    submitArrive(true);
                }
            }
        });

        Button submitFailedButton = (Button) findViewById(R.id.submit_failed_button);
        submitFailedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitArrive(false);
            }
        });

        final Handler handler = new Handler();
        timer = new Timer(false);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadInfo();
                    }
                });
            }
        };
        timer.schedule(timerTask, 10000, 10000);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getContext(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getContext(), new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        BluetoothConnect.BindService(getContext());
        UsbConnect.BindService(getContext());

        new Thread() {

            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                BluetoothConnect.Connect();
            }

        }.start();

        UsbConnectThread.start(getContext());
        if (FloatWindowService.getInstance(getContext()) != null) {
            FloatWindowService.getInstance(getContext()).usbConnect();
        }
    }

    private void submitArrive(final boolean isSuccess) {
        String consignmentNumberList = "";
        if (isSuccess) {
            for (String cn : successConsignmentNumbers) {
                consignmentNumberList += cn + ",";
            }
        } else {
            for (String cn : undoneConsignmentNumbers) {
                if (!successConsignmentNumbers.contains(cn)) {
                    consignmentNumberList += cn + ",";
                }
            }
        }

        if (consignmentNumberList == "") {
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage(getString(R.string.error_missing_consignment_numbers))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        if (isSuccess && !isNoClient && mRatingBar.getRating() == 0) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage(getString(R.string.error_missing_rating))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        // Upload file
        if (mProofImage == null) {
            // No proof
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage(getString(isSuccess && !isNoClient ? R.string.error_missing_signature : R.string.error_missing_proof_image))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        if (isSuccess && TextUtils.isEmpty(mFullNameView.getText())) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage(getString(isNoClient ? R.string.error_missing_reason : R.string.error_missing_fullname))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }
        if (!isSuccess && TextUtils.isEmpty(mFailedReason.getText())) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage(getString(R.string.error_missing_reason))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        showProgress(true);

        final String finalConsignmentNumberList = consignmentNumberList;

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        mProofImage.compress(isSuccess ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 80, bao);
        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");
        ByteArrayInputStream bai = new ByteArrayInputStream(bao.toByteArray());
        ApiServices.UploadFile(isSuccess && !isNoClient ? ApiServices.UploadSignatureServiceUrl : ApiServices.UploadProofServiceUrl, token, bai,
                consignmentNumbers[currentConsignmentNumberIdx] + (isSuccess ? ".png" : ".jpg"), new ApiServices.ServiceCallbackInterface() {

                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        try {
                            JSONObject result = (JSONObject) response;
                            if (result.getInt("Code") != 0) {
                                showProgress(false);
                                new AlertDialog.Builder(getContext())
                                        .setTitle(getString(R.string.title_activity_arrive))
                                        .setMessage(result.getString("Message"))
                                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                                return;
                            }

                            ApiServices.ArriveConfirm(token, mJobId, finalConsignmentNumberList, result.getString("Path") + "|" + (isSuccess ? mFullNameView.getText() : mFailedReason.getText()), mService.equalsIgnoreCase("Pickup"), isSuccess, (int) mRatingBar.getRating(), new ApiServices.ServiceCallbackInterface() {
                                @Override
                                public void onSuccess(int statusCode, Object response) throws JSONException {
                                    showProgress(false);
                                    JSONObject result = (JSONObject) response;
                                    if (result.getInt("Code") != 0) {
                                        new AlertDialog.Builder(getContext())
                                                .setTitle(getString(R.string.title_activity_arrive))
                                                .setMessage(result.getString("Message"))
                                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                })
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();
                                        return;
                                    }

                                    // Show success message
                                    new AlertDialog.Builder(getContext())
                                            .setTitle(getString(R.string.title_activity_arrive))
                                            .setMessage(getString(
                                                    mService.equalsIgnoreCase("Pickup")
                                                            ? (isSuccess ? R.string.submit_pickup_successful : R.string.submit_pickup_failed)
                                                            : (isSuccess ? R.string.submit_delivery_successful : R.string.submit_delivery_failed)))
                                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    mProofImage.recycle();
                                                    mProofImage = null;

                                                    // This is done
                                                    String[] doneConsignments = finalConsignmentNumberList.split(",");
                                                    for (String cn : doneConsignments) {
                                                        undoneConsignmentNumbers.remove(cn);
                                                    }

                                                    if (isSuccess) {
                                                        successfulConsignmentNumbers += doneConsignments.length;
                                                    } else {
                                                        failedConsignmentNumbers += doneConsignments.length;
                                                    }

                                                    if (undoneConsignmentNumbers.size() > 0) {
                                                        try {
                                                            for (int j = 0; j < mOrders.length(); j++) {
                                                                JSONObject order = mOrders.getJSONObject(j);
                                                                if (order.optString("ConsignmentNumber").equalsIgnoreCase(undoneConsignmentNumbers.get(0))) {
                                                                    currentConsignmentNumberIdx = j;
                                                                }
                                                            }
                                                        } catch (Exception exc) {
                                                            exc.printStackTrace();
                                                        }
                                                    }

                                                    if (currentConsignmentNumberIdx >= consignmentNumbers.length || undoneConsignmentNumbers.size() == 0) {
                                                        finish();
                                                    } else {
                                                        // Go to next
                                                        mJobIdView.setText(String.format("%s (%d/%d are successful)", mJobId, successfulConsignmentNumbers, consignmentNumbers.length));
                                                        arriveButton.setVisibility(View.VISIBLE);
                                                        failedButton.setVisibility(View.VISIBLE);
                                                        if (isSuccess) {
                                                            arriveButton.setBackgroundColor(Color.parseColor("#a00f530b"));
                                                            arriveButton.setEnabled(false);
                                                        } else {
                                                            failedButton.setBackgroundColor(Color.parseColor("#a07d0000"));
                                                            failedButton.setEnabled(false);
                                                        }
                                                        mArrivedView.setVisibility(View.GONE);
                                                        mFailedView.setVisibility(View.GONE);
                                                        mCaptureButton.setImageBitmap(null);
                                                        mCaptureSuccessImage.setImageBitmap(null);
                                                        mSignaturePad.clear();
                                                    }
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_info)
                                            .show();
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    error.printStackTrace();
                                    showProgress(false);
                                    new AlertDialog.Builder(getContext())
                                            .setTitle(getString(R.string.title_activity_arrive))
                                            .setMessage(getString(R.string.error_network_problem))
                                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                            });
                        } catch (Exception exc) {
                            exc.printStackTrace();
                            showProgress(false);
                            new AlertDialog.Builder(getContext())
                                    .setTitle(getString(R.string.title_activity_arrive))
                                    .setMessage(getString(R.string.error_network_problem))
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        error.printStackTrace();
                        showProgress(false);
                        new AlertDialog.Builder(getContext())
                                .setTitle(getString(R.string.title_activity_arrive))
                                .setMessage(getString(R.string.error_network_problem))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                });
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalStoragePublicDirectory(DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        float scale = Math.min(scaleWidth, scaleHeight);
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scale, scale);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private ArriveActivity getContext() {
        return this;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSubmitView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSubmitView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSubmitView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSubmitView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle scan result
            processScanResult(scanResult.getContents());
//            if (parts.length < 2 || !parts[1].equalsIgnoreCase(consignmentNumbers[currentConsignmentNumberIdx])) {
//                // Must contain at least Order Id and Consignment Number
//                new AlertDialog.Builder(getContext())
//                        .setTitle(getString(R.string.title_activity_arrive))
//                        .setMessage(getString(R.string.error_wrong_qr_code))
//                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        })
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .show();
//                return;
//            }

            // Re-open popup
            mConsignmentNumberView.performClick();
        } else {
            if ((requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_TAKE_REASON_PHOTO) && resultCode == RESULT_OK) {
                Bitmap bm = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(photoFile.getAbsolutePath());
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                    int rotate = 0;
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }

                    Matrix matrix = new Matrix();
                    float sx = bm.getWidth() > 1024 ? 1024 / (float) bm.getWidth() : 1;
                    float sy = bm.getHeight() > 1024 ? 1024 / (float) bm.getHeight() : 1;
                    float scale = Math.min(sx, sy);
                    matrix.postScale(scale, scale);
                    matrix.postRotate(rotate);
                    mProofImage = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    bm.recycle();
                    bm = null;
                } catch (IOException e) {
                    bm.recycle();
                    e.printStackTrace();
                    bm = null;
                }
                if (requestCode == REQUEST_TAKE_PHOTO) {
                    mCaptureButton.setImageBitmap(mProofImage);
                } else {
                    mCaptureSuccessImage.setImageBitmap(mProofImage);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Arrive Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.roadbull.driver/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);

        setBroadcast();
    }

    @Override
    public void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Arrive Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.roadbull.driver/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }

        if (mReadBroadcast != null) {
            unregisterReceiver(mReadBroadcast);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BluetoothConnect.UnBindService(getContext());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mCustomerMobilePhoneView.getText()));
                        startActivity(intent);
                    }

                } else {

                    // permission denied

                }
                return;
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putStringArrayList("SuccessConsignmentNumbers", successConsignmentNumbers);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        successConsignmentNumbers = savedInstanceState.getStringArrayList("SuccessConsignmentNumbers");
    }

    private void loadInfo() {
        if (isLoadInfo == true) {
            return;
        }
        isLoadInfo = true;
        final String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.OrderInfo(token, consignmentNumbers[currentConsignmentNumberIdx], new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                isLoadInfo = false;
                JSONObject result = (JSONObject) response;
                if (result.getInt("Code") != 0) {
                    if (getContext().isDestroyed()) {
                        return;
                    }
                    new AlertDialog.Builder(getContext())
                            .setTitle(getString(R.string.title_activity_arrive))
                            .setMessage(result.getString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return;
                }

                // mSizeView.setText(result.getJSONObject("Order").getString("Size"));
                mOpsRemark.setText(result.getJSONObject("Order").isNull("OperatorRemark") ? "" : result.getJSONObject("Order").getString("OperatorRemark"));
                mRequest.setText("");
                JSONArray requests = result.getJSONObject("Order").getJSONArray("ExtraRequests");
                if (mService.equalsIgnoreCase("Pickup")) {
                    try {
                        for (int i = 0; i < requests.length(); i++) {
                            if (requests.getJSONObject(i).getInt("RequestType") == 1 || requests.getJSONObject(i).getInt("RequestType") == 3) {
                                mRequest.setText(requests.getJSONObject(i).isNull("RequestContent") ? "" : requests.getJSONObject(i).getString("RequestContent"));
                            }
                        }
                    } catch (Exception exc) {
                    }
                } else {
                    try {
                        for (int i = 0; i < requests.length(); i++) {
                            if (requests.getJSONObject(i).getInt("RequestType") == 0 || requests.getJSONObject(i).getInt("RequestType") == 2) {
                                mRequest.setText(requests.getJSONObject(i).isNull("RequestContent") ? "" : requests.getJSONObject(i).getString("RequestContent"));
                            }
                        }
                    } catch (Exception exc) {
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                isLoadInfo = false;
                error.printStackTrace();
                showProgress(false);
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.title_activity_arrive))
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private void processScanResult(String result) {
        String[] parts = result == null ? new String[]{} : result.split("\\|");
        currentViewCN = "";
        if (parts.length >= 2) {
            for (String s : consignmentNumbers) {
                if (s.equalsIgnoreCase(parts[1]) && !successConsignmentNumbers.contains(s)) {
                    successConsignmentNumbers.add(s);
                }
                if (s.equalsIgnoreCase(parts[1])) {
                    // Scroll to
                    currentViewCN = s;
                }
            }
        }
    }

    private void setBroadcast() {
        mReadBroadcast = new ReadBroadcast();
        IntentFilter filter = new IntentFilter();
        filter.addAction(SendConstant.GetDataAction);
        filter.addAction(SendConstant.GetReadDataAction);
        filter.addAction(SendConstant.GetBatteryDataAction);
        registerReceiver(mReadBroadcast, filter);
    }

    public class ReadBroadcast extends BroadcastReceiver {

        public ReadBroadcast() {

        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SendConstant.GetBatteryDataAction)) {

                String data = intent.getStringExtra(SendConstant.GetBatteryData);

            }
            if (intent.getAction().equals(SendConstant.GetDataAction)) {

                String data = intent.getStringExtra(SendConstant.GetData);

                processScanResult(data);
            }
            if (intent.getAction().equals(SendConstant.GetReadDataAction)) {
                String name = intent.getStringExtra(SendConstant.GetReadName);
                String data = intent.getStringExtra(SendConstant.GetReadData);

            }
        }

    }
}
