package com.roadbull.driver;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.util.TextUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistorySummaryFragment extends Fragment {

    private DatePicker datePicker;
    private Button fromDate;
    private Button toDate;
    private View pickupCount;
    private View deliveryCount;
    private TextView pickupCountValue;
    private TextView deliveryCountValue;
    private TextView amountEarned;

    private int fromYear;
    private int fromMonth;
    private int fromDay;
    private int toYear;
    private int toMonth;
    private int toDay;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_summary, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.message_loading));
        progressDialog.setIndeterminate(true);

        fromDate = (Button) view.findViewById(R.id.from_date);
        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        fromYear = year;
                        fromMonth = monthOfYear;
                        fromDay = dayOfMonth;
                        fromDate.setText(String.format("%02d/%02d/%d", dayOfMonth, monthOfYear + 1, year));

                        loadInfo();
                    }
                }, fromYear, fromMonth, fromDay);
                dialog.show();
            }
        });
        toDate = (Button) view.findViewById(R.id.to_date);
        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        toYear = year;
                        toMonth = monthOfYear;
                        toDay = dayOfMonth;
                        toDate.setText(String.format("%02d/%02d/%d", dayOfMonth, monthOfYear + 1, year));

                        loadInfo();
                    }
                }, toYear, toMonth, toDay);
                dialog.show();
            }
        });

        pickupCount = view.findViewById(R.id.pickup_count);
        pickupCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDetail(true);
            }
        });
        deliveryCount = view.findViewById(R.id.delivery_count);
        deliveryCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDetail(false);
            }
        });
        pickupCountValue = (TextView) view.findViewById(R.id.pickup_count_value);
        deliveryCountValue = (TextView) view.findViewById(R.id.delivery_count_value);
        amountEarned = (TextView) view.findViewById(R.id.amount_earned);

        Calendar calendar = Calendar.getInstance();
        fromYear = calendar.get(Calendar.YEAR);
        fromMonth = calendar.get(Calendar.MONTH);
        fromDay = calendar.get(Calendar.DAY_OF_MONTH);
        fromDate.setText(String.format("%02d/%02d/%d", fromDay, fromMonth + 1, fromYear));
        toYear = calendar.get(Calendar.YEAR);
        toMonth = calendar.get(Calendar.MONTH);
        toDay = calendar.get(Calendar.DAY_OF_MONTH);
        toDate.setText(String.format("%02d/%02d/%d", toDay, toMonth + 1, toYear));
        loadInfo();

        pickupCountValue.setText("0");
        deliveryCountValue.setText("0");
        amountEarned.setText("0");

        return view;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private Fragment getFragment() {
        return this;
    }

    private void loadInfo() {
        if (TextUtils.isEmpty(fromDate.getText())) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage("From Date must be chosen")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }
        if (TextUtils.isEmpty(toDate.getText())) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage("To Date must be chosen")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        progressDialog.show();

        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.HistorySummary(token, String.format("%02d/%02d/%d", fromMonth + 1, fromDay, fromYear), String.format("%02d/%02d/%d", toMonth + 1, toDay, toYear), new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                JSONObject result = (JSONObject) response;
                progressDialog.hide();
                if (result.getInt("Code") != 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_arrive))
                            .setMessage(result.getString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return;
                }

                pickupCountValue.setText("0");
                deliveryCountValue.setText("0");

                DecimalFormat formatter = new DecimalFormat("#,###,###");
                double total = 0;
                JSONArray list = result.optJSONArray("Result");
                for (int i = 0; i < list.length(); i++) {
                    JSONObject item = list.getJSONObject(i);
                    if (item.getBoolean("IsPickup")) {
                        pickupCountValue.setText(formatter.format(item.optInt("Count")));
                    } else {
                        deliveryCountValue.setText(formatter.format(item.optInt("Count")));
                    }
                    total += item.optDouble("Amount");
                }

                DecimalFormat moneyFormatter = new DecimalFormat("$#,###,###.##");
                amountEarned.setText(moneyFormatter.format(total));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                progressDialog.hide();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_arrive))
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private void loadDetail(Boolean isPickup) {
        if (TextUtils.isEmpty(fromDate.getText())) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage("From Date must be chosen")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }
        if (TextUtils.isEmpty(toDate.getText())) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.title_activity_arrive))
                    .setMessage("To Date must be chosen")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        progressDialog.show();

        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.HistoryDetail(token, String.format("%02d/%02d/%d", fromMonth + 1, fromDay, fromYear), String.format("%02d/%02d/%d", toMonth + 1, toDay, toYear), isPickup, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                JSONObject result = (JSONObject) response;
                progressDialog.hide();
                if (result.getInt("Code") != 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_arrive))
                            .setMessage(result.getString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return;
                }

                JSONArray list = result.optJSONArray("Result");
                ArrayList<JSONObject> items = new ArrayList<JSONObject>();
                for (int i = 0; i < list.length(); i++) {
                    items.add(list.getJSONObject(i));
                }

                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialoglayout = inflater.inflate(R.layout.history_detail_view, null);

                ListView listView = (ListView) dialoglayout.findViewById(R.id.job_list);
                JobHistoryItemsAdapter listAdapter = new JobHistoryItemsAdapter(getActivity(), items);
                listView.setAdapter(listAdapter);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("History");
                builder.setView(dialoglayout);
                builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                progressDialog.hide();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_arrive))
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }
}
