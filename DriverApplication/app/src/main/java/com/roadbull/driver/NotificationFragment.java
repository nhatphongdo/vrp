package com.roadbull.driver;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;

import java.util.ArrayList;

public class NotificationFragment extends ListFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<NotificationItem> items = NotificationItem.Load(getActivity());
        setListAdapter(new NotificationItemsAdapter(getActivity(), items));
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
