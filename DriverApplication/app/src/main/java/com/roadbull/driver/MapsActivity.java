package com.roadbull.driver;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private Marker currentPosition;
    public LocationManager locationManager;

    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 10; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
    private static final int PERMISSIONS_REQUEST_READ_LOCATIONS = 123;


    private static final String ARG_FROM_ADDRESS = "FROM_ADDRESS";
    private static final String ARG_TO_ADDRESS = "TO_ADDRESS";

    private static final String ARG_FROM_MOBILE_PHONE = "FROM_MOBILE_PHONE";
    private static final String ARG_TO_MOBILE_PHONE = "TO_MOBILE_PHONE";

    private static final String ARG_FROM_NAME = "FROM_NAME";
    private static final String ARG_TO_NAME = "TO_NAME";

    private static final String ARG_FROM_COMPANY = "FROM_COMPANY";
    private static final String ARG_TO_COMPANY = "TO_COMPANY";

    private static final String ARG_FROM_GEOLOCATION = "FROM_GEOLOCATION";
    private static final String ARG_TO_GEOLOCATION = "TO_GEOLOCATION";

    private static final String ARG_CONSIGNMENT_NUMBER = "CONSIGNMENT_NUMBER";
    private static final String ARG_SERVICE = "SERVICE";
    private static final String ARG_SIZE = "SIZE";
    private static final String ARG_JOB_ID = "JOB_ID";
    private static final String ARG_OPERATOR_REMARK = "OPERATOR_REMARK";
    private static final String ARG_EXTRA_REQUESTS = "EXTRA_REQUESTS";
    private static final String ARG_STATUS = "STATUS";
    private static final String ARG_ORDERS = "ORDERS";
    private static final String ARG_COD = "COD";
    private static final String ARG_EXCHANGE = "EXCHANGE";

    private String mFromAddress;
    private String mToAddress;
    private String mFromName;
    private String mToName;
    private String mFromMobilePhone;
    private String mToMobilePhone;
    private String mFromGeolocation;
    private String mToGeolocation;
    private String mConsignmentNumber;
    private String mService;
    private String mJobId;
    private String mFromCompany;
    private String mToCompany;
    private String mSize;
    private String mOrders;
    private String mOperatorRemark;
    private String mExtraRequests;
    private int mStatus;
    private String mCod;
    private Boolean mExchange;

    private TextView mServiceView;
    private TextView mConsignmentNumberView;

    private MapDirectionService service;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public static Intent createInstance(Context context, String fromName, String fromAddress, String fromMobilePhone, String fromGeolocation,
                                        String toName, String toAddress, String toMobilePhone, String toGeolocation,
                                        String consignmentNumber, String service, String jobId, String fromCompany, String toCompany,
                                        String sizeName, String orders, String operatorRemark, String extraRequests, int status, String cod, Boolean isExchange) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(ARG_FROM_ADDRESS, fromAddress);
        intent.putExtra(ARG_TO_ADDRESS, toAddress);
        intent.putExtra(ARG_FROM_NAME, fromName);
        intent.putExtra(ARG_TO_NAME, toName);
        intent.putExtra(ARG_FROM_MOBILE_PHONE, fromMobilePhone);
        intent.putExtra(ARG_TO_MOBILE_PHONE, toMobilePhone);
        intent.putExtra(ARG_FROM_GEOLOCATION, fromGeolocation);
        intent.putExtra(ARG_TO_GEOLOCATION, toGeolocation);
        intent.putExtra(ARG_CONSIGNMENT_NUMBER, consignmentNumber);
        intent.putExtra(ARG_SERVICE, service);
        intent.putExtra(ARG_JOB_ID, jobId);
        intent.putExtra(ARG_FROM_COMPANY, fromCompany);
        intent.putExtra(ARG_TO_COMPANY, toCompany);
        intent.putExtra(ARG_SIZE, sizeName);
        intent.putExtra(ARG_ORDERS, orders);
        intent.putExtra(ARG_OPERATOR_REMARK, operatorRemark);
        intent.putExtra(ARG_EXTRA_REQUESTS, extraRequests);
        intent.putExtra(ARG_STATUS, status);
        intent.putExtra(ARG_COD, cod);
        intent.putExtra(ARG_EXCHANGE, isExchange);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission.
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.title_activity_maps))
                        .setMessage(getString(R.string.message_location_explanation))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                        PERMISSIONS_REQUEST_READ_LOCATIONS);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();

            } else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSIONS_REQUEST_READ_LOCATIONS);
            }
        } else {
            try {
                boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(NETWORK_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATES, MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(GPS_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATES, MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
            } catch (SecurityException exc) {
                exc.printStackTrace();
            }
        }

        // Get parameters
        Intent intent = getIntent();
        mFromAddress = intent.getStringExtra(ARG_FROM_ADDRESS);
        mToAddress = intent.getStringExtra(ARG_TO_ADDRESS);
        mFromName = intent.getStringExtra(ARG_FROM_NAME);
        mToName = intent.getStringExtra(ARG_TO_NAME);
        mFromMobilePhone = intent.getStringExtra(ARG_FROM_MOBILE_PHONE);
        mToMobilePhone = intent.getStringExtra(ARG_TO_MOBILE_PHONE);
        mFromGeolocation = intent.getStringExtra(ARG_FROM_GEOLOCATION);
        mToGeolocation = intent.getStringExtra(ARG_TO_GEOLOCATION);
        mConsignmentNumber = intent.getStringExtra(ARG_CONSIGNMENT_NUMBER);
        mService = intent.getStringExtra(ARG_SERVICE);
        mJobId = intent.getStringExtra(ARG_JOB_ID);
        mFromCompany = intent.getStringExtra(ARG_FROM_COMPANY);
        mToCompany = intent.getStringExtra(ARG_TO_COMPANY);
        mSize = intent.getStringExtra(ARG_SIZE);
        mOrders = intent.getStringExtra(ARG_ORDERS);
        mOperatorRemark = intent.getStringExtra(ARG_OPERATOR_REMARK);
        mExtraRequests = intent.getStringExtra(ARG_EXTRA_REQUESTS);
        mStatus = intent.getIntExtra(ARG_STATUS, 0);
        mCod = intent.getStringExtra(ARG_COD);
        mExchange = intent.getBooleanExtra(ARG_EXCHANGE, false);

        mServiceView = (TextView) findViewById(R.id.service);
        mConsignmentNumberView = (TextView) findViewById(R.id.consignmentNumber);

        mServiceView.setText(mService);
        if (mService.equalsIgnoreCase("Pickup")) {
            mServiceView.setTextColor(Color.RED);
        } else {
            mServiceView.setTextColor(Color.parseColor("#2fa33b"));
        }

        final String[] consignmentNumbers = mConsignmentNumber.split(",");
        if (consignmentNumbers.length == 1) {
            mConsignmentNumberView.setText(mConsignmentNumber);
        } else {
            mConsignmentNumberView.setText(String.format(getContext().getString(R.string.message_number_of_orders), consignmentNumbers.length));
            mConsignmentNumberView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Show popup with Consignment numbers
                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
                    builderSingle.setTitle("Consignment Numbers");

                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                            getContext(),
                            android.R.layout.select_dialog_item);
                    for (String cn : consignmentNumbers) {
                        arrayAdapter.add(cn);
                    }

                    builderSingle.setPositiveButton(
                            "Close",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    builderSingle.setAdapter(
                            arrayAdapter,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    String strName = arrayAdapter.getItem(which);
//                                    AlertDialog.Builder builderInner = new AlertDialog.Builder(
//                                            getContext());
//                                    builderInner.setMessage(strName);
//                                    builderInner.setTitle("Your Selected Item is");
//                                    builderInner.setPositiveButton(
//                                            "Ok",
//                                            new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(
//                                                        DialogInterface dialog,
//                                                        int which) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                    builderInner.show();
                                }
                            });
                    builderSingle.show();
                }
            });
        }

        Button navigateButton = (Button) findViewById(R.id.navigate);
        navigateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri;
                if (mOrders == null || mOrders.isEmpty()) {
                    // Special job
                    if (mService.equalsIgnoreCase("Pickup")) {
                        gmmIntentUri = Uri.parse("google.navigation:q=" + mFromGeolocation + "&mode=d");
                    } else {
                        gmmIntentUri = Uri.parse("google.navigation:q=" + mToGeolocation + "&mode=d");
                    }
                } else {
                    gmmIntentUri = Uri.parse("google.navigation:q=" + mToGeolocation + "&mode=d");
                }

                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        Button arriveButton = (Button) findViewById(R.id.arrive);
        arriveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                if (mOrders == null || mOrders.isEmpty()) {
                    // Special job
                    if (mService.equalsIgnoreCase("Pickup")) {
                        startActivity(ArriveActivity.createInstance(getContext(), mConsignmentNumber, mService, mJobId, mFromName, mFromAddress, mFromMobilePhone, mFromCompany, mSize, mOrders, mOperatorRemark, mExtraRequests, mStatus, mCod, mExchange));
                    } else {
                        startActivity(ArriveActivity.createInstance(getContext(), mConsignmentNumber, mService, mJobId, mToName, mToAddress, mToMobilePhone, mToCompany, mSize, mOrders, mOperatorRemark, mExtraRequests, mStatus, mCod, mExchange));
                    }
                } else {
                    startActivity(ArriveActivity.createInstance(getContext(), mConsignmentNumber, mService, mJobId, mToName, mToAddress, mToMobilePhone, mToCompany, mSize, mOrders, mOperatorRemark, mExtraRequests, mStatus, mCod, mExchange));
                }
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private MapsActivity getContext() {
        return this;
    }

    @Override
    protected void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.roadbull.driver/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_LOCATIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    try {
                        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                        if (isNetworkEnabled) {
                            locationManager.requestLocationUpdates(NETWORK_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATES, MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, this);
                        }
                        if (isGPSEnabled) {
                            locationManager.requestLocationUpdates(GPS_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATES, MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, this);
                        }
                    } catch (SecurityException exc) {
                        exc.printStackTrace();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);

        String[] parts = mFromGeolocation.split(",");
        LatLng fromPoint = new LatLng(Double.parseDouble(parts[0]), Double.parseDouble(parts[1]));
        parts = mToGeolocation.split(",");
        LatLng toPoint = new LatLng(Double.parseDouble(parts[0]), Double.parseDouble(parts[1]));

        mMap.addMarker(new MarkerOptions()
                .position(fromPoint)
                .title(mFromName)
                .snippet(mFromAddress)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        mMap.addMarker(new MarkerOptions()
                .position(toPoint)
                .title(mToName)
                .snippet(mToAddress)
                .icon(BitmapDescriptorFactory.defaultMarker(mService.equalsIgnoreCase("Pickup") ? BitmapDescriptorFactory.HUE_RED : BitmapDescriptorFactory.HUE_GREEN)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(fromPoint));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(Math.min(mMap.getMaxZoomLevel(), 18)));

        service = new MapDirectionService();
        service.getDirection(mFromGeolocation, mToGeolocation, new MapDirectionService.DirectionCallbackInterface() {

            @Override
            public void onSuccess(ArrayList<JSONObject> list) {
                PolylineOptions rectLine = new PolylineOptions().width(20).color(Color.BLUE);

                try {
                    for (int i = 0; i < list.size(); i++) {
                        ArrayList<LatLng> points = (ArrayList<LatLng>) list.get(i).get("Points");
                        for (int j = 0; j < points.size(); j++) {
                            rectLine.add(points.get(j));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Polyline polyline = mMap.addPolyline(rectLine);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (getContext() == null || getContext().isFinishing()) {
                    return;
                }
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.title_activity_maps))
                        .setMessage(getString(R.string.error_map_service))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

        });
    }

    @Override
    public void onLocationChanged(Location location) {
        if (currentPosition != null) {
            currentPosition.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));

            // Submit to server
            String token = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.TOKEN_KEY, "");
            ApiServices.TrackLocation(token, mConsignmentNumber, mJobId, location.getLatitude(), location.getLongitude(), new ApiServices.ServiceCallbackInterface() {
                @Override
                public void onSuccess(int statusCode, Object response) throws JSONException {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                }
            });
        } else if (mMap != null) {
            currentPosition = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(location.getLatitude(), location.getLongitude()))
                    .title("Me")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.roadbull.driver/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

}
