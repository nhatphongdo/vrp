package com.roadbull.driver;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Do Nhat Phong on 11/11/2015.
 */
public class NotificationItem {

    public Date Time;

    public String Content;

    private static final String FILENAME = "notifications";

    public static ArrayList<NotificationItem> Load(Context context) {
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            String content = new String(buffer);

            // Split by \n
            ArrayList<NotificationItem> result = new ArrayList<>();
            String[] lines = content.split("\\n");
            for (int i = 0; i < lines.length; i++) {
                NotificationItem item = new NotificationItem();

                String time = lines[i].substring(0, lines[i].indexOf(";"));
                String text = lines[i].substring(lines[i].indexOf(";") + 1);
                item.Time = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(time);
                item.Content = text;

                result.add(item);
            }

            Collections.sort(result, new Comparator<NotificationItem>() {
                public int compare(NotificationItem o1, NotificationItem o2) {
                    return o1.Time.compareTo(o2.Time);
                }
            });

            if (result.size() > 100) {
                // Limit to 100 items
                result = new ArrayList<>(result.subList(0, 80));
                Save(context, result);
            }

            fis.close();

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static void Save(Context context, ArrayList<NotificationItem> list) {
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            for (int i = 0; i < list.size(); i++) {
                String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(list.get(i).Time);
                String content = date + ";" + list.get(i).Content + "\n";
                fos.write(content.getBytes());
            }
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Add(Context context, NotificationItem item) {
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_APPEND);
            String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(item.Time);
            String content = date + ";" + item.Content + "\n";
            fos.write(content.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
