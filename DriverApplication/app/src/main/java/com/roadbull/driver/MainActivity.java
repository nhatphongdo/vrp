package com.roadbull.driver;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, ProfileFragment.OnProfileFragmentListener {


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private ImageView avatarImage;
    private TextView mFullNameView;
    private TextView mVehicleNumberView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Set default fragment
        String staffId = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_STAFF_NUMBER_KEY, "");
        Fragment fragment;
        if (TextUtils.isEmpty(staffId)) {
            fragment = new JobFragment();
            setTitle(getString(R.string.title_activity_member));
        } else {
            fragment = new JobFragment();
            setTitle(getString(R.string.title_activity_staff));
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

        navigationView.setCheckedItem(R.id.nav_jobs);

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);

        avatarImage = (ImageView) headerLayout.findViewById(R.id.imageAvatar);
        Bitmap bitmap = GlobalPreferences.getAvatar();
        if (bitmap != null) {
            avatarImage.setImageBitmap(bitmap);
        } else {
            String photo = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_PHOTO_KEY, "");
            if (photo != "") {
                (new DownloadImageTask(avatarImage)).execute(photo);
            }
        }

        mFullNameView = (TextView) headerLayout.findViewById(R.id.fullName);
        mFullNameView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_FULLNAME_KEY, ""));

        mVehicleNumberView = (TextView) headerLayout.findViewById(R.id.vehicleNumber);
        mVehicleNumberView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_VEHICLE_NUMBER_KEY, ""));

        // Start GCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = GlobalPreferences.getInstance(context).get(GlobalPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                } else {
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private MainActivity getContext() {
        return this;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        if (id == R.id.nav_notifications) {
            fragment = new NotificationFragment();
            setTitle(getString(R.string.title_activity_notifications));
        } else if (id == R.id.nav_jobs) {
            String staffId = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_STAFF_NUMBER_KEY, "");
            fragment = new JobFragment();
            if (TextUtils.isEmpty(staffId)) {
                setTitle(getString(R.string.title_activity_member));
            } else {
                setTitle(getString(R.string.title_activity_staff));
            }
        }
        //else if (id == R.id.nav) {
        //    fragment = new JobFragment();
        //    setTitle(getString(R.string.title_activity_job));
        //}
        else if (id == R.id.nav_special_jobs) {
            fragment = new MemberFragment();
            setTitle(getString(R.string.title_activity_special_job));
        } else if (id == R.id.nav_scan_qr) {
            fragment = new ScanQrFragment();
            setTitle(getString(R.string.title_activity_scan_qr));
        } else if (id == R.id.nav_profile) {
            fragment = new ProfileFragment();
            setTitle(getString(R.string.title_activity_profile));
        } else if (id == R.id.nav_history) {
            //fragment = new HistoryFragment();
            fragment = new HistorySummaryFragment();
            setTitle(getString(R.string.title_activity_history));
        } else if (id == R.id.nav_news) {
            fragment = new NewsFragment();
            setTitle(getString(R.string.title_activity_news));
        } else if (id == R.id.nav_contact_info) {
            fragment = new ContactInfoFragment();
            setTitle(getString(R.string.title_activity_contact_info));
        } else if (id == R.id.nav_logout) {
            // Sign out, clear all information and return back to login screen
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.TOKEN_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.REMEMBER_ACCOUNT);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_ID_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_EMAIL_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_USERNAME_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_PASSWORD_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_FULLNAME_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_ADDRESS_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_COUNTRY_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_MOBILE_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_PHOTO_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_DRIVING_LICENSE_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_IC_NUMBER_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_STAFF_NUMBER_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_VEHICLE_NUMBER_KEY);
            GlobalPreferences.getInstance(this).remove(GlobalPreferences.USER_VEHICLE_TYPE_KEY);
            GlobalPreferences.setAvatar(null);

            finish();
            startActivity(new Intent(getContext(), LoginActivity.class));
        }

        if (fragment != null) {
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("Roadbull Driver", "This device is not supported.");

            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(GlobalPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onAvatarLoaded(Bitmap bitmap) {
        avatarImage.setImageBitmap(bitmap);
    }

    @Override
    public void onFullNameChanged(String name) {
        mFullNameView.setText(name);
    }
}
