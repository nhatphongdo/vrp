package com.roadbull.driver;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Do Nhat Phong on 10/31/2015.
 */
public class DriverWebViewClient extends WebViewClient {

    private ProgressDialog dialog;

    public DriverWebViewClient(Context context) {
        dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(R.string.message_loading));
        dialog.setIndeterminate(true);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        // Show loading popup
        dialog.show();
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        dialog.hide();
    }
}
