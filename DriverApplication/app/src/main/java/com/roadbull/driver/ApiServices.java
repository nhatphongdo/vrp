package com.roadbull.driver;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Do Nhat Phong on 10/19/2015.
 */
public class ApiServices {

    interface ServiceCallbackInterface {

        void onSuccess(int statusCode, Object response) throws JSONException;

        void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error);

    }

    public final static String BaseUrl = "http://sandcds.roadbull.com/";
    //public final static String BaseUrl = "http://192.168.1.164/";
    public final static String LoginServiceUrl = BaseUrl + "api/accounts/login";
    public final static String ProfileServiceUrl = BaseUrl + "api/accounts/profile";
    public final static String RegisterServiceUrl = BaseUrl + "api/accounts/register";
    public final static String ForgotPasswordServiceUrl = BaseUrl + "api/accounts/forgotpassword";
    public final static String UpdateProfileServiceUrl = BaseUrl + "api/accounts/update";
    public final static String UploadAccountPhotoServiceUrl = BaseUrl + "api/accounts/upload";
    public final static String UpdateNotificationTokenServiceUrl = BaseUrl + "api/accounts/notificationToken";
    public final static String CountriesServiceUrl = BaseUrl + "api/locations/countries";
    public final static String TrackLocationServiceUrl = BaseUrl + "api/locations/track";
    public final static String SettingUrlsServiceUrl = BaseUrl + "api/settings/urls";
    public final static String SettingRequestsServiceUrl = BaseUrl + "api/settings/requests";
    public final static String JobListServiceUrl = BaseUrl + "api/jobs/list";
    public final static String JobsServiceUrl = BaseUrl + "api/jobs/myjobs";
    public final static String LoadJobServiceUrl = BaseUrl + "api/jobs/loadjob";
    public final static String SpecialJobsServiceUrl = BaseUrl + "api/jobs/special";
    public final static String JobsHistoryServiceUrl = BaseUrl + "api/jobs/history";
    public final static String JobsHistorySummaryServiceUrl = BaseUrl + "api/jobs/historysummary";
    public final static String JobsHistoryDetailServiceUrl = BaseUrl + "api/jobs/historydetail";
    public final static String UploadProofServiceUrl = BaseUrl + "api/orders/uploadProof";
    public final static String UploadSignatureServiceUrl = BaseUrl + "api/orders/uploadSignature";
    public final static String ArriveConfirmServiceUrl = BaseUrl + "api/orders/arriveConfirm";
    public final static String OrderInfoServiceUrl = BaseUrl + "api/orders/info";
    public final static String OrdersRemarkServiceUrl = BaseUrl + "api/orders/remark";


    protected static void InvokeWebservice(String serviceUrl, String token, RequestParams params, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(600000);
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }
        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void UploadFile(String serviceUrl, String token, String filePath, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }

        File file = new File(filePath);
        RequestParams params = new RequestParams();
        try {
            params.put("uploadFile", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onFailure(500, null, null, e);
            }
        }

        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void UploadFile(String serviceUrl, String token, InputStream input, String filename, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }

        RequestParams params = new RequestParams();
        params.put("uploadFile", input, filename);

        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void Login(String username, String password, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("UserName", username);
        params.add("Password", password);
        InvokeWebservice(LoginServiceUrl, null, params, callback);
    }

    public static void Profile(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(ProfileServiceUrl, token, null, callback);
    }

    public static void Register(String username, String password, String email, String fullName, String countryCode, String mobileNumber,
                                String address, String icNumber, String drivingLicense, String vehicleNumber, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("UserName", username);
        params.add("Password", password);
        params.add("ConfirmPassword", password);
        params.add("Email", email);
        params.add("Name", fullName);
        params.add("CountryCode", countryCode);
        params.add("MobileNumber", mobileNumber);
        params.add("Address", address);
        params.add("ICNumber", icNumber);
        params.add("AccountType", "1");
        params.add("DrivingLicense", drivingLicense);
        params.add("VehicleNumber", vehicleNumber);
        InvokeWebservice(RegisterServiceUrl, null, params, callback);
    }

    public static void ForgotPassword(String username, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("UserName", username);
        InvokeWebservice(ForgotPasswordServiceUrl, null, params, callback);
    }

    public static void Countries(ServiceCallbackInterface callback) {
        InvokeWebservice(CountriesServiceUrl, null, null, callback);
    }

    public static void UrlsSetting(ServiceCallbackInterface callback) {
        InvokeWebservice(SettingUrlsServiceUrl, null, null, callback);
    }

    public static void RequestsSetting(ServiceCallbackInterface callback) {
        InvokeWebservice(SettingRequestsServiceUrl, null, null, callback);
    }

    public static void UpdateProfile(String token, String fullName, String countryCode, String mobileNumber, String password, String newPassword, String photo, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("Name", fullName);
        params.add("CountryCode", countryCode);
        params.add("MobileNumber", mobileNumber);
        params.add("Password", password);
        params.add("NewPassword", newPassword);
        params.add("Photo", photo);
        InvokeWebservice(UpdateProfileServiceUrl, token, params, callback);
    }

    public static void UpdateNotificationToken(String token, String notificationToken, final ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("Token", notificationToken);
        params.add("PhoneType", "1"); // Android
        SyncHttpClient client = new SyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }
        client.post(UpdateNotificationTokenServiceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void Jobs(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(JobListServiceUrl, token, null, callback);
    }

    public static void MyJobs(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(JobsServiceUrl, token, null, callback);
    }
    public static void LoadJob(String token, String jobId, ServiceCallbackInterface callback) {
        String url = LoadJobServiceUrl + "/" + jobId;
        InvokeWebservice(url, token, null, callback);
    }
    public static void ArriveConfirm(String token, String jobId, String consignmentNumbers, String proofPhoto, boolean isPickup, boolean isSuccessful, int rating, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.put("JobId", jobId);
        params.put("ConsignmentNumber", consignmentNumbers);
        params.put("IsPickup", isPickup);
        params.put("IsSuccessful", isSuccessful);
        params.put("ProofPath", proofPhoto);
        params.put("Rating", rating);
        InvokeWebservice(ArriveConfirmServiceUrl, token, params, callback);
    }

    public static void TrackLocation(String token, String consignmentNumber, String jobId, double latitude, double longitude, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.put("ConsignmentNumber", consignmentNumber);
        params.put("JobId", jobId);
        params.put("Latitude", latitude);
        params.put("Longitude", longitude);
        InvokeWebservice(TrackLocationServiceUrl, token, params, callback);
    }

    public static void History(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(JobsHistoryServiceUrl, token, null, callback);
    }

    public static void SpecialJobs(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(SpecialJobsServiceUrl, token, null, callback);
    }

    public static void OrderInfo(String token, String consignmentNumber, ServiceCallbackInterface callback) {
        InvokeWebservice(OrderInfoServiceUrl + "/" + consignmentNumber, token, null, callback);
    }

    public static void HistorySummary(String token, String from, String to, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.put("FromDate", from);
        params.put("ToDate", to);
        InvokeWebservice(JobsHistorySummaryServiceUrl, token, params, callback);
    }

    public static void HistoryDetail(String token, String from, String to, Boolean isPickup, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.put("FromDate", from);
        params.put("ToDate", to);
        params.put("IsPickup", isPickup);
        InvokeWebservice(JobsHistoryDetailServiceUrl, token, params, callback);
    }

    public static void OrdersRemark(String token, String consignmentNumbers, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.put("ConsignmentNumbers", consignmentNumbers);
        InvokeWebservice(OrdersRemarkServiceUrl, token, params, callback);
    }
}
