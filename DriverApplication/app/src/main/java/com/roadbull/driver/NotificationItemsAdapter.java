package com.roadbull.driver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Do Nhat Phong on 11/4/2015.
 */
public class NotificationItemsAdapter extends ArrayAdapter<NotificationItem> {

    private final Context context;
    private ArrayList<NotificationItem> list;

    public NotificationItemsAdapter(Context context, ArrayList<NotificationItem> list) {
        super(context, R.layout.notification_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.notification_item, parent, false);

        TextView time = (TextView) rowView.findViewById(R.id.time);
        TextView content = (TextView) rowView.findViewById(R.id.content);

        time.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(list.get(position).Time));
        content.setText(list.get(position).Content);

        return rowView;
    }

}
