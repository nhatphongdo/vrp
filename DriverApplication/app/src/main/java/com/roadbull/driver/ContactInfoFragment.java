package com.roadbull.driver;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class ContactInfoFragment extends Fragment {

    private WebView contentWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_info, container, false);
        contentWebView = (WebView) view.findViewById(R.id.contentWebView);
        WebSettings webSettings = contentWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        contentWebView.setWebViewClient(new DriverWebViewClient(getActivity()));
        String url = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.COMPANY_INFO_URL, "");
        if (!TextUtils.isEmpty(url)) {
            contentWebView.loadUrl(url);
        }
        return view;
    }

}
