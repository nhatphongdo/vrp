package com.roadbull.driver;

import android.*;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;


public class StaffFragment extends Fragment {

    private ArrayList<JSONObject> routesList;
    private RouteItemsAdapter listAdapter;
    private ListView routesListView;
    private ProgressDialog dialog;
    // Add search order
    private EditText inputSearch;
    private TextView mTodayView;
    private TextView mVehicleNumberView;
    private TextView mJobIdView;

    private Boolean needToRefresh;

    private Timer timer;
    private Boolean isLoadInfo;

    private JSONArray pins;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_staff, container, false);

        routesList = new ArrayList<JSONObject>();
        listAdapter = new RouteItemsAdapter(getActivity(), routesList);
        routesListView = (ListView) view.findViewById(R.id.routesListView);
        routesListView.setAdapter(listAdapter);


        mTodayView = (TextView) view.findViewById(R.id.time);
        mVehicleNumberView = (TextView) view.findViewById(R.id.vehicleNumber);
        mJobIdView = (TextView) view.findViewById(R.id.job);

        // Load data
        needToRefresh = true;
        loadJobs(null);

        //Duy: Add search Text box
        inputSearch = (EditText) view.findViewById(R.id.input_search);

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.toString().isEmpty() || charSequence == null){
                    loadJobs(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Button viewPins = (Button) view.findViewById(R.id.view_pins);
        viewPins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show dialog
                if (pins == null) {
                    return;
                }
                Intent intent = new Intent(getActivity(), MapDialogFragment.class);
                double[] locs = new double[pins.length() * 2];
                String[] names = new String[pins.length()];
                String[] orders = new String[pins.length()];
                int[] types = new int[pins.length()];
                for (int i = 0; i < pins.length(); i++) {
                    locs[i * 2] = pins.optJSONObject(i).optDouble("Latitude");
                    locs[i * 2 + 1] = pins.optJSONObject(i).optDouble("Longitude");
                    names[i] = pins.optJSONObject(i).optString("Name");
                    orders[i] = pins.optJSONObject(i).optString("Order");
                    types[i] = pins.optJSONObject(i).optInt("Type");
                }
                intent.putExtra("Geolocations", locs);
                intent.putExtra("Names", names);
                intent.putExtra("Orders", orders);
                intent.putExtra("Types", types);
                startActivity(intent);
            }
        });

        Button searchButton = (Button) view.findViewById(R.id.button_searchCn);
        searchButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                CharSequence charSequence = inputSearch.getText();
                loadJobs(charSequence);


            }
        });
        isLoadInfo = false;
        final Handler handler = new Handler();
        timer = new Timer(false);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadRemarks();
                    }
                });
            }
        };
        timer.schedule(timerTask, 10000, 10000);
        return view;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (needToRefresh) {
            loadJobs(null);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        needToRefresh = true;

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    private void loadJobs(final CharSequence charSequence) {
        needToRefresh = false;
        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.message_loading), true);

        listAdapter.clear();
        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.Jobs(token, new ApiServices.ServiceCallbackInterface() {

            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try {
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        if (getActivity() == null) {
                            return;
                        }
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getActivity().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    JSONArray jobs = result.getJSONArray("Result");

                    // If there is no job
                    if (jobs.length() == 0) {
                        showInfoAlert(R.string.message_no_job);
                        return;
                    }

                    JSONObject job = jobs.getJSONObject(0);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                    Date createdDate = null;
                    createdDate = dateFormat.parse(job.getString("CreatedOn"));
                    SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy");

                    mTodayView.setText(outputDateFormat.format(createdDate));
                    mVehicleNumberView.setText(job.getString("VehicleNumber"));
                    mJobIdView.setText(job.getString("Id"));

                    // Load routes in this job
                    JSONObject routes = job.getJSONObject("Routes");
                    // Load locations info
                    JSONArray locations = routes.getJSONArray("Locations");

                    // Route detail
                    JSONObject route = routes.getJSONObject("Route");

                    pins = new JSONArray();

                    int stops = route.getInt("Stops");
                    JSONArray stepIndices = route.getJSONArray("Steps");
                    JSONArray footSteps = route.getJSONArray("FootSteps");
                    for (int i = 1; i < stops; i++) {
                        JSONObject step = new JSONObject();
                        step.put("JobId", job.getString("Id"));
                        step.put("FromName", locations.getJSONObject(stepIndices.getInt(i - 1) - 1).getString("CustomerName"));
                        step.put("FromCompany", locations.getJSONObject(stepIndices.getInt(i - 1) - 1).isNull("CustomerCompany") ? "" : locations.getJSONObject(stepIndices.getInt(i - 1) - 1).getString("CustomerCompany"));
                        step.put("FromAddress", locations.getJSONObject(stepIndices.getInt(i - 1) - 1).isNull("CustomerAddress") ? "" : locations.getJSONObject(stepIndices.getInt(i - 1) - 1).getString("CustomerAddress"));
                        step.put("FromMobilePhone", locations.getJSONObject(stepIndices.getInt(i - 1) - 1).isNull("CustomerMobilePhone") ? "" : locations.getJSONObject(stepIndices.getInt(i - 1) - 1).getString("CustomerMobilePhone"));
                        step.put("ToName", locations.getJSONObject(stepIndices.getInt(i) - 1).getString("CustomerName"));
                        step.put("ToAddress", locations.getJSONObject(stepIndices.getInt(i) - 1).isNull("CustomerAddress") ? "" : locations.getJSONObject(stepIndices.getInt(i) - 1).getString("CustomerAddress"));
                        step.put("ToCompany", locations.getJSONObject(stepIndices.getInt(i) - 1).isNull("CustomerCompany") ? "" : locations.getJSONObject(stepIndices.getInt(i) - 1).getString("CustomerCompany"));
                        step.put("ToMobilePhone", locations.getJSONObject(stepIndices.getInt(i) - 1).isNull("CustomerMobilePhone") ? "" : locations.getJSONObject(stepIndices.getInt(i) - 1).getString("CustomerMobilePhone"));
                        JSONArray consignmentNumberList = locations.getJSONObject(stepIndices.getInt(i) - 1).getJSONArray("ConsignmentNumbers");
                        String consignmentNumbers = "";
                        for (int j = 0; j < consignmentNumberList.length(); j++) {
                            consignmentNumbers += consignmentNumberList.getString(j);
                            if (j < consignmentNumberList.length() - 1) {
                                consignmentNumbers += ",";
                            }
                        }
                        step.put("ConsignmentNumber", consignmentNumbers);
                        step.put("Orders", locations.getJSONObject(stepIndices.getInt(i) - 1).getString("Orders"));
                        step.put("Type", locations.getJSONObject(stepIndices.getInt(i) - 1).getInt("Type"));
                        step.put("SizeName", locations.getJSONObject(stepIndices.getInt(i) - 1).getString("SizeName"));
                        step.put("IsDone", locations.getJSONObject(stepIndices.getInt(i) - 1).getBoolean("IsDone"));
                        step.put("FromGeolocation", footSteps.getJSONObject(i - 1));
                        step.put("ToGeolocation", footSteps.getJSONObject(i));
                        JSONObject loc = new JSONObject();
                        loc.put("Latitude", footSteps.getJSONObject(i).optDouble("Latitude"));
                        loc.put("Longitude", footSteps.getJSONObject(i).optDouble("Longitude"));
                        loc.put("Order", consignmentNumbers);
                        loc.put("Type", locations.getJSONObject(stepIndices.getInt(i) - 1).getInt("Type"));
                        loc.put("Name", locations.getJSONObject(stepIndices.getInt(i) - 1).isNull("CustomerAddress") ? "" : locations.getJSONObject(stepIndices.getInt(i) - 1).getString("CustomerAddress"));
                        pins.put(loc);
                        JSONArray orders = new JSONArray(locations.getJSONObject(stepIndices.getInt(i) - 1).getString("Orders"));
                        step.put("OperatorRemark", orders.length() == 0 ? "" : orders.getJSONObject(0).getString("OperatorRemark"));
                        step.put("ExtraRequests", orders.length() == 0 ? new JSONArray() : orders.getJSONObject(0).getJSONArray("ExtraRequests"));
                        step.put("PickupTimeSlotId", orders.length() == 0 ? 0 : (orders.getJSONObject(0).isNull("PickupTimeSlotId") ? 0 : orders.getJSONObject(0).getInt("PickupTimeSlotId")));
                        step.put("DeliveryTimeSlotId", orders.length() == 0 ? 0 : (orders.getJSONObject(0).isNull("DeliveryTimeSlotId") ? 0 : orders.getJSONObject(0).get("DeliveryTimeSlotId")));
                        step.put("Timeslots", result.getJSONArray("Timeslots"));
                        step.put("HasRemark", false);

                        // Add search consignmentNumber logic
                        if(charSequence != null && charSequence.length() > 0){
                            if(consignmentNumbers.contains(charSequence.toString().toUpperCase())) {
                               listAdapter.add(step);
                            }

                        } else {
                            listAdapter.add(step);
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                dialog.hide();
                if (listAdapter.getCount() == 0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("No records found")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                showErrorAlert(R.string.error_unknown_error);
            }

        });
    }

    private void showInfoAlert(int messageResId) {
        if (getActivity() == null) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getTitle())
                .setMessage(getString(messageResId))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void showErrorAlert(int messageRedId) {
        if (getActivity() == null) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getTitle())
                .setMessage(getString(messageRedId))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void loadRemarks() {
        if (isLoadInfo == true || pins == null) {
            return;
        }
        isLoadInfo = true;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < pins.length(); i++) {
            if (i != 0) sb.append(",");
            sb.append(pins.optJSONObject(i).optString("Order"));
        }
        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.OrdersRemark(token, sb.toString(), new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                isLoadInfo = false;
                JSONObject result = (JSONObject) response;
                if (result.getInt("Code") != 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_arrive))
                            .setMessage(result.getString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return;
                }

                JSONArray orders = result.getJSONArray("Orders");
                for (int i = 0; i < listAdapter.getCount(); i++) {
                    for (int j = 0; j < orders.length(); j++) {
                        if (Arrays.asList(listAdapter.getItem(i).optString("ConsignmentNumber").split(",")).contains(orders.getJSONObject(j).optString("ConsignmentNumber"))) {
                            listAdapter.getItem(i).put("HasRemark", true);
                        }
                    }
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                isLoadInfo = false;
                error.printStackTrace();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_arrive))
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }


}
