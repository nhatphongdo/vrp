package com.roadbull.driver;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Do Nhat Phong on 11/4/2015.
 */
public class OptionItemsAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private ArrayList<JSONObject> list;
    private final CompoundButton.OnCheckedChangeListener mCheckboxChanged;
    private final TextWatcher mTextWatcher;
    private AlertDialog mDialog;

    public OptionItemsAdapter(Context context, ArrayList<JSONObject> list, CompoundButton.OnCheckedChangeListener checkboxChanged, TextWatcher textWatcher) {
        super(context, R.layout.select_option_item, list);
        this.context = context;
        this.list = list;
        this.mCheckboxChanged = checkboxChanged;
        this.mTextWatcher = textWatcher;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.select_option_item, parent, false);

        final TextView text = (TextView) rowView.findViewById(R.id.text);
        final EditText reason = (EditText) rowView.findViewById(R.id.reason);
        final RadioButton selected = (RadioButton) rowView.findViewById(R.id.selected);

        try {
            final JSONObject item = list.get(position);
            if (item.optBoolean("IsOther", false)) {
                text.setText("Other:");
                reason.setText(item.optString("Text"));
                LinearLayout.LayoutParams layout = (LinearLayout.LayoutParams) text.getLayoutParams();
                layout.weight = 0;
                text.setLayoutParams(layout);
                reason.setVisibility(View.VISIBLE);
            } else {
                text.setText(item.optString("Text"));
            }
            selected.setChecked(item.optBoolean("IsSelected", false));
            selected.setTag(item.optString("Text"));

            reason.addTextChangedListener(mTextWatcher);
            selected.setOnCheckedChangeListener(mCheckboxChanged);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowView;
    }

    public void setDialog(AlertDialog dialog) {
        mDialog = dialog;
    }

    public AlertDialog getDialog() {
        return mDialog;
    }
}
