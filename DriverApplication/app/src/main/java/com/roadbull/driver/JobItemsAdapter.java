package com.roadbull.driver;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Duy Tran on 12/02/2016.
 */
public class JobItemsAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private ArrayList<JSONObject> list;

    public JobItemsAdapter(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.job_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.job_item, parent, false);

        TextView jobId = (TextView) rowView.findViewById(R.id.jobId);
        TextView vehicleId = (TextView) rowView.findViewById(R.id.vehicleNumber);
        TextView createdOn = (TextView) rowView.findViewById(R.id.createdOn);

        ImageButton detailButton = (ImageButton) rowView.findViewById(R.id.jobDetailButton);
        detailButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    JSONObject item = list.get(position);

                    Intent intent = RouteActivity.createInstance(context,item.getString("Id"));
                    context.startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        });

        try {
            JSONObject item = list.get(position);
            jobId.setText(item.getString("Id"));
            vehicleId.setText(item.getString("VehicleNumber"));
            createdOn.setText(item.getString("CreatedOn"));


        } catch (Exception exc) {
            exc.printStackTrace();
        }

        return rowView;
    }
}
