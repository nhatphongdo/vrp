package com.roadbull.driver;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Do Nhat Phong on 11/4/2015.
 */
public class RouteItemsAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private ArrayList<JSONObject> list;

    public RouteItemsAdapter(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.route_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.route_item, parent, false);

        TextView routeType = (TextView) rowView.findViewById(R.id.routeType);
        TextView fromCity = (TextView) rowView.findViewById(R.id.fromCity);
        TextView toCity = (TextView) rowView.findViewById(R.id.toCity);

        ImageButton detailButton = (ImageButton) rowView.findViewById(R.id.detailButton);
        detailButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    JSONObject item = list.get(position);
                    String fromGeolocation = item.getJSONObject("FromGeolocation").getString("Latitude") + "," + item.getJSONObject("FromGeolocation").getString("Longitude");
                    String toGeolocation = item.getJSONObject("ToGeolocation").getString("Latitude") + "," + item.getJSONObject("ToGeolocation").getString("Longitude");
                    Intent intent = MapsActivity.createInstance(context, item.getString("FromName"), item.getString("FromAddress"), item.getString("FromMobilePhone"), fromGeolocation,
                            item.getString("ToName"), item.getString("ToAddress"), item.getString("ToMobilePhone"), toGeolocation,
                            item.getString("ConsignmentNumber"), item.getInt("Type") == 0 ? "Pickup" : "Delivery", item.getString("JobId"), item.optString("FromCompany"), item.optString("ToCompany"),
                            item.optString("SizeName"), item.optString("Orders"), item.optString("OperatorRemark"), item.optString("ExtraRequests"), item.optInt("Status"), item.optString("Cod"), item.optBoolean("IsExchange"));
                    context.startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        });

        try {
            JSONObject item = list.get(position);
            if (item.getInt("Type") == 0) {
                // 0 is pickup
                routeType.setBackgroundColor(Color.RED);
            } else {
                // 1 is delivery
                routeType.setBackgroundColor(Color.GREEN);
            }

            if (item.optBoolean("HasRemark", false)) {
                toCity.setTextColor(Color.parseColor("#f39c12"));
            } else {
                toCity.setTextColor(Color.BLACK);
            }

            JSONArray timeslots = item.getJSONArray("Timeslots");

            if (item.optString("Orders") == null || item.optString("Orders").isEmpty()) {
                // This is special order
                // fromCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.from) + " <b>" + getContext().getString(R.string.current_location) + "</b>"));
                if (item.getInt("Type") == 0) {
                    // 0 is pickup
                    String timeslot = "";
                    for (int i = 0; i < timeslots.length(); i++) {
                        if (timeslots.getJSONObject(i).optInt("Id") == (item.isNull("PickupTimeSlotId") ? 0 : item.getInt("PickupTimeSlotId"))) {
                            timeslot = timeslots.getJSONObject(i).optString("Text") + " (" + timeslots.getJSONObject(i).optString("FromTime") + " - " + timeslots.getJSONObject(i).optString("ToTime") + ")";
                            break;
                        }
                    }
                    if (item.getString("FromCompany").isEmpty()) {
                        toCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.to) + " <b>" + item.getString("FromName") + ", " + item.getString("FromAddress") + "</b><br /><font color=\"#ff0000\"><b>" + timeslot + "</b></font>"));
                    } else {
                        toCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.to) + " <b>" + item.getString("FromCompany") + ", " + item.getString("FromAddress") + "</b><br /><font color=\"#ff0000\"><b>" + timeslot + "</b></font>"));
                    }
                } else {
                    // 1 is delivery
                    String timeslot = "";
                    for (int i = 0; i < timeslots.length(); i++) {
                        if (timeslots.getJSONObject(i).optInt("Id") == (item.isNull("DeliveryTimeSlotId") ? 0 : item.getInt("DeliveryTimeSlotId"))) {
                            timeslot = timeslots.getJSONObject(i).optString("Text") + " (" + timeslots.getJSONObject(i).optString("FromTime") + " - " + timeslots.getJSONObject(i).optString("ToTime") + ")";
                            break;
                        }
                    }
                    if (item.getString("ToCompany").isEmpty()) {
                        toCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.to) + " <b>" + item.getString("ToName") + ", " + item.getString("ToAddress") + "</b><br /><font color=\"#00ff00\"><b>" + timeslot + "</b></font>"));
                    } else {
                        toCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.to) + " <b>" + item.getString("ToCompany") + ", " + item.getString("ToAddress") + "</b><br /><font color=\"#00ff00\"><b>" + timeslot + "</b></font>"));
                    }
                }
            } else {
//                if (item.getString("FromCompany").isEmpty()) {
//                    fromCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.from) + " <b>" + item.getString("FromName") + ", " + item.getString("FromAddress") + "</b>"));
//                } else {
//                    fromCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.from) + " <b>" + item.getString("FromCompany") + ", " + item.getString("FromAddress") + "</b>"));
//                }

                JSONArray orders = new JSONArray(item.optString("Orders"));
                String color;
                String timeslot = "";
                if (item.getInt("Type") == 0) {
                    // 0 is pickup
                    color = "#ff0000";
                    if (orders.length() > 0) {
                        for (int i = 0; i < timeslots.length(); i++) {
                            if (timeslots.getJSONObject(i).optInt("Id") == (orders.getJSONObject(0).isNull("PickupTimeSlotId") ? 0 : orders.getJSONObject(0).getInt("PickupTimeSlotId"))) {
                                timeslot = timeslots.getJSONObject(i).optString("Text") + " (" + timeslots.getJSONObject(i).optString("FromTime") + " - " + timeslots.getJSONObject(i).optString("ToTime") + ")";
                                break;
                            }
                        }
                    }
                } else {
                    // 1 is delivery
                    color = "#00ff00";
                    if (orders.length() > 0) {
                        for (int i = 0; i < timeslots.length(); i++) {
                            if (timeslots.getJSONObject(i).optInt("Id") == (orders.getJSONObject(0).isNull("DeliveryTimeSlotId") ? 0 : orders.getJSONObject(0).getInt("DeliveryTimeSlotId"))) {
                                timeslot = timeslots.getJSONObject(i).optString("Text") + " (" + timeslots.getJSONObject(i).optString("FromTime") + " - " + timeslots.getJSONObject(i).optString("ToTime") + ")";
                                break;
                            }
                        }
                    }
                }

                if (item.getString("ToCompany").isEmpty()) {
                    toCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.to) + " <b>" + item.getString("ToName") + ", " + item.getString("ToAddress") + "</b><br /><font color=" + color + "><b>" + timeslot + "</b></font>"));
                } else {
                    toCity.setText(Html.fromHtml("<b>" + String.valueOf(position + 1) + ".</b> " + getContext().getString(R.string.to) + " <b>" + item.getString("ToCompany") + ", " + item.getString("ToAddress") + "</b><br /><font color=" + color + "><b>" + timeslot + "</b></font>"));
                }
            }

            if (item.getBoolean("IsDone")) {
                routeType.setBackgroundColor(Color.LTGRAY);
                rowView.setBackgroundColor(Color.LTGRAY);
                detailButton.setVisibility(View.INVISIBLE);
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

        return rowView;
    }
}
