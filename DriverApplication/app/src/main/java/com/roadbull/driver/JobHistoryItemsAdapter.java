package com.roadbull.driver;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Do Nhat Phong on 11/4/2015.
 */
public class JobHistoryItemsAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private ArrayList<JSONObject> list;

    public JobHistoryItemsAdapter(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.history_detail_item, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.history_detail_item, parent, false);

        final TextView date = (TextView) rowView.findViewById(R.id.date);
        final TextView time = (TextView) rowView.findViewById(R.id.time);
        final TextView size = (TextView) rowView.findViewById(R.id.size);

        try {
            final JSONObject item = list.get(position);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            Date createdDate = null;
            createdDate = dateFormat.parse(item.getString("CompletedOn"));

            SimpleDateFormat getDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat getTimeFormat = new SimpleDateFormat("HH:mm");

            date.setText(getDateFormat.format(createdDate));
            time.setText(getTimeFormat.format(createdDate));
            size.setText(item.optString("Size"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowView;
    }
}
