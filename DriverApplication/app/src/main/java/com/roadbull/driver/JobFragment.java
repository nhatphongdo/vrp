package com.roadbull.driver;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;


public class JobFragment extends Fragment {

    private ArrayList<JSONObject> jobsList;
    private JobItemsAdapter listAdapter;
    private ListView jobListView;
    private ProgressDialog dialog;
    private Boolean needToRefresh;
    private TextView remaining;
    private TextView today;
    private Timer timer;

    //private Boolean isLoadInfo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jobs, container, false);

        jobsList = new ArrayList<JSONObject>();
        listAdapter = new JobItemsAdapter(getActivity(), jobsList);
        jobListView = (ListView) view.findViewById(R.id.job_list_view);
        jobListView.setAdapter(listAdapter);

        remaining = (TextView) view.findViewById(R.id.remaining_job);
        today = (TextView) view.findViewById(R.id.today);


        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        today.setText(dateFormat.format(date));

        // Load data
        needToRefresh = true;
        LoadJobs();
        return view;
    }

    //Load job
    private void LoadJobs(){
        needToRefresh = false;
        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.message_loading), true);

        listAdapter.clear();
        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.MyJobs(token, new ApiServices.ServiceCallbackInterface(){

            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                try{
                    dialog.hide();
                    JSONObject result = (JSONObject) response;

                    if (result.getInt("Code") != 0) {
                        if (getActivity() == null) {
                            return;
                        }
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getActivity().getTitle())
                                .setMessage(result.getString("Message"))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }

                    JSONArray jobs = result.getJSONArray("Result");

                    // If there is no job
                    if (jobs.length() == 0) {
                        showInfoAlert(R.string.message_no_job);
                        return;
                    }

                    remaining.setText(Integer.toString(jobs.length()));

                    for (int i = 0; i < jobs.length(); i++){
                        JSONObject job = new JSONObject();

                        job.put("Id",jobs.getJSONObject(i).getString("Id"));
                        job.put("VehicleNumber",jobs.getJSONObject(i).getString("VehicleNumber"));

                        // Format date
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                        Date createdDate = dateFormat.parse(jobs.getJSONObject(i).getString("CreatedOn"));
                        SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy");

                        job.put("CreatedOn",outputDateFormat.format(createdDate));
                        listAdapter.add(job);
                    }


                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                showErrorAlert(R.string.error_unknown_error);
            }
        });
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (needToRefresh) {
            LoadJobs();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        needToRefresh = true;

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    private void showInfoAlert(int messageResId) {
        if (getActivity() == null) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getTitle())
                .setMessage(getString(messageResId))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void showErrorAlert(int messageRedId) {
        if (getActivity() == null) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getTitle())
                .setMessage(getString(messageRedId))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
