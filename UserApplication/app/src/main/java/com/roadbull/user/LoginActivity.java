package com.roadbull.user;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class LoginActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    // UI references.
    private EditText mUserNameView;
    private EditText mPasswordView;
    private CheckBox mRememberMe;
    private View mProgressView;
    private View mLoginFormView;
    private ImageButton mForgotPasswordButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        mUserNameView = (EditText) findViewById(R.id.usernameEditText);
        mPasswordView = (EditText) findViewById(R.id.passwordEditText);
        mRememberMe = (CheckBox) findViewById(R.id.rememberMe);

        mUserNameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.next) {
                    mPasswordView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mUserNameView.setOnFocusChangeListener(this);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mPasswordView.setOnFocusChangeListener(this);

        ImageButton mStaffSignInButton = (ImageButton) findViewById(R.id.loginButton);
        mStaffSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mForgotPasswordButton = (ImageButton) findViewById(R.id.forgetPasswordButton);
        mForgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(getContext(), ForgotPasswordActivity.class));
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        // Try to login with remember
        boolean isRemember = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.REMEMBER_ACCOUNT, false);
        if (isRemember == true) {
            mRememberMe.setChecked(isRemember);
            mUserNameView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_USERNAME_KEY, ""));
            mPasswordView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_PASSWORD_KEY, ""));
            attemptLogin();
        }
    }


    private LoginActivity getContext() {
        return this;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors
        mUserNameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String userName = mUserNameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid username
        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_username_required));
            focusView = mUserNameView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            ApiServices.Login(userName, password, new ApiServices.ServiceCallbackInterface() {
                @Override
                public void onSuccess(int statusCode, Object response) {
                    try {
                        final JSONObject loginResult = (JSONObject) response;
                        if (loginResult.getInt("Code") != 0) {
                            // Error
                            showProgress(false);
                            mPasswordView.setError(loginResult.getString("Message").replaceAll(" \\| ", "\n"));
                            mPasswordView.requestFocus();
                        } else {
                            // Get profile
                            ApiServices.Profile(loginResult.getString("Token"), new ApiServices.ServiceCallbackInterface() {
                                @Override
                                public void onSuccess(int statusCode, Object response) {
                                    try {
                                        // Check role
                                        boolean found = false;
                                        JSONArray profiles = (JSONArray) response;
                                        JSONObject profile = null;
                                        for (int i = 0; i < profiles.length(); i++) {
                                            profile = profiles.getJSONObject(i).getJSONObject("Profile");
                                            if (profile.optBoolean("IsDriver") == false && profiles.getJSONObject(i).optString("ProfileType").equalsIgnoreCase("customer")) {
                                                found = true;
                                                break;
                                            }
                                        }

                                        if (found && profile != null) {
                                            // This is Customer
                                            if (profile.optBoolean("IsActive", true) == false) {
                                                showProgress(false);
                                                mUserNameView.setError(getString(R.string.error_not_active_account));
                                                mUserNameView.requestFocus();
                                                return;
                                            }

                                            // Save information
                                            if (mRememberMe.isChecked() == true) {
                                                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_PASSWORD_KEY, mPasswordView.getText().toString());
                                                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.REMEMBER_ACCOUNT, true);
                                            } else {
                                                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_PASSWORD_KEY);
                                                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.REMEMBER_ACCOUNT, false);
                                            }

                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.TOKEN_KEY, loginResult.getString("Token"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_USERNAME_KEY, mUserNameView.getText().toString());
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_ID_KEY, profile.optString("Id"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_ZIP_CODE_KEY, profile.optString("ZipCode"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_EMAIL_KEY, profile.optString("Email"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_ADDRESS_KEY, profile.optString("Address"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_COUNTRY_KEY, profile.optString("Country"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_MOBILE_KEY, profile.optString("Mobile"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_FULLNAME_KEY, profile.optString("Name"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_PHOTO_KEY, profile.optString("Photo"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_COMPANY_KEY, profile.optString("Company"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_PAYMENT_TYPE_ID_KEY, profile.optInt("PaymentTypeId"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_PAYMENT_TYPE_NAME_KEY, profile.optString("PaymentTypeName"));
                                            GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.USER_IS_ACTIVE_KEY, profile.optBoolean("IsActive"));

                                            showProgress(false);
                                            finish();
                                            startActivity(new Intent(getContext(), MainActivity.class));
                                        } else {
                                            // This is not customer
                                            showProgress(false);
                                            mUserNameView.setError(getString(R.string.error_invalid_permissions));
                                            mUserNameView.requestFocus();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        showProgress(false);
                                        mPasswordView.setError(getString(R.string.error_unknown_error));
                                        mPasswordView.requestFocus();
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    error.printStackTrace();
                                    showProgress(false);
                                    mPasswordView.setError(getString(R.string.error_unknown_error));
                                    mPasswordView.requestFocus();
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showProgress(false);
                        mPasswordView.setError(getString(R.string.error_unknown_error));
                        mPasswordView.requestFocus();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    error.printStackTrace();
                    showProgress(false);
                    mPasswordView.setError(getString(R.string.error_network_problem));
                    mPasswordView.requestFocus();
                }
            });
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mForgotPasswordButton.setVisibility(show ? View.GONE : View.VISIBLE);
            mForgotPasswordButton.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mForgotPasswordButton.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mForgotPasswordButton.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v.getId() == R.id.usernameEditText || v.getId() == R.id.passwordEditText) && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        }
    }
}
