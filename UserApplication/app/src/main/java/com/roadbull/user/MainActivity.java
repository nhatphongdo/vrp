package com.roadbull.user;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ProfileFragment.OnProfileFragmentListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private ImageView avatarImage;
    private TextView mFullNameView;
    private TextView mEmailView;
    private TextView mMemberFromView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, new NewOrderFragment())
                .commit();

        navigationView.setCheckedItem(R.id.nav_new_order);

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        avatarImage = (ImageView) headerLayout.findViewById(R.id.avatarImageView);
        avatarImage.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo));
        Bitmap bitmap = GlobalPreferences.getAvatar();
        if (bitmap != null) {
            avatarImage.setImageBitmap(bitmap);
        } else {
            String photo = GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_PHOTO_KEY, "");
            if (photo != "") {
                (new DownloadImageTask(avatarImage)).execute(photo);
            }
        }

        mFullNameView = (TextView) headerLayout.findViewById(R.id.fullNameTextView);
        mFullNameView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_FULLNAME_KEY, ""));

        mEmailView = (TextView) headerLayout.findViewById(R.id.emailTextView);
        mEmailView.setText(GlobalPreferences.getInstance(getContext()).get(GlobalPreferences.USER_EMAIL_KEY, ""));

        mMemberFromView = (TextView) headerLayout.findViewById(R.id.userSinceTextView);
        mMemberFromView.setText("");

        Button contactUsButton = (Button) drawer.findViewById(R.id.menu_contact_us);
        contactUsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Fragment fragment = new ContactUsFragment();
//                FragmentManager fragmentManager = getFragmentManager();
//                fragmentManager.beginTransaction()
//                        .replace(R.id.content_frame, fragment)
//                        .commit();
                Uri uri = Uri.parse("mailto:info@roadbull.com")
                        .buildUpon()
                        .build();

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(Intent.createChooser(emailIntent, "New Message"));
            }
        });

        Button logoutButton = (Button) drawer.findViewById(R.id.menu_logout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.TOKEN_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.REMEMBER_ACCOUNT);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_ID_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_EMAIL_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_USERNAME_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_PASSWORD_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_ZIP_CODE_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_FULLNAME_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_ADDRESS_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_PHOTO_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_COUNTRY_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_MOBILE_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_COMPANY_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_IS_ACTIVE_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_PAYMENT_TYPE_ID_KEY);
                GlobalPreferences.getInstance(getContext()).remove(GlobalPreferences.USER_PAYMENT_TYPE_NAME_KEY);

                finish();
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });

        // Start GCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = GlobalPreferences.getInstance(context).get(GlobalPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                } else {
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private MainActivity getContext() {
        return this;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        if (id == R.id.nav_profile) {
            fragment = new ProfileFragment();
        } else if (id == R.id.nav_new_order) {
            fragment = new NewOrderFragment();
        } else if (id == R.id.nav_track_item) {
            fragment = new TrackItemFragment();
//        } else if (id == R.id.nav_e_wallet) {
//            fragment = new WalletFragment();
        } else if (id == R.id.nav_guidelines) {
            String url = GlobalPreferences.getInstance(this).get(GlobalPreferences.SERVICE_GUIDELINES_URL, "");
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } else if (id == R.id.nav_types_of_service) {
            String url = GlobalPreferences.getInstance(this).get(GlobalPreferences.TYPE_OF_SERVICE_URL, "");
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("Roadbull Driver", "This device is not supported.");

            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(GlobalPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onAvatarLoaded(Bitmap bitmap) {
        avatarImage.setImageBitmap(bitmap);
    }

    @Override
    public void onInfoChanged(String name, String email) {
        mFullNameView.setText(name);
        mEmailView.setText(email);
    }
}
