package com.roadbull.user;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Do Nhat Phong on 12/4/2015.
 */
public class Order implements Parcelable {

    public int ProductTypeId;
    public int ServiceId;
    public int SizeId;
    public String FromName;
    public String FromZipCode;
    public String FromAddress;
    public String FromMobilePhone;
    public String ToName;
    public String ToZipCode;
    public String ToAddress;
    public String ToMobilePhone;
    public int PickupTimeSlotId;
    public Date PickupDate;
    public int DeliveryTimeSlotId;
    public Date DeliveryDate;
    public String Remark;
    public String PromoCode;
    public String CustomerId;
    public int PaymentTypeId;
    public double Price;
    public String PickupTimeSlotText;
    public String DeliveryTimeSlotText;
    public String ProductTypeText;
    public String ServiceText;
    public String SizeText;

    public Order() {
    }

    protected Order(Parcel in) {
        ProductTypeId = in.readInt();
        ServiceId = in.readInt();
        SizeId = in.readInt();
        FromName = in.readString();
        FromZipCode = in.readString();
        FromAddress = in.readString();
        FromMobilePhone = in.readString();
        ToName = in.readString();
        ToZipCode = in.readString();
        ToAddress = in.readString();
        ToMobilePhone = in.readString();
        PickupTimeSlotId = in.readInt();
        DeliveryTimeSlotId = in.readInt();
        Remark = in.readString();
        PromoCode = in.readString();
        CustomerId = in.readString();
        PaymentTypeId = in.readInt();
        Price = in.readDouble();
        long time = in.readLong();
        PickupDate = time == 0 ? null : new Date(time);
        time = in.readLong();
        DeliveryDate = time == 0 ? null : new Date(time);
        PickupTimeSlotText = in.readString();
        DeliveryTimeSlotText = in.readString();
        ProductTypeText = in.readString();
        SizeText = in.readString();
        ServiceText = in.readString();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(ProductTypeId);
        dest.writeInt(ServiceId);
        dest.writeInt(SizeId);
        dest.writeString(FromName);
        dest.writeString(FromZipCode);
        dest.writeString(FromAddress);
        dest.writeString(FromMobilePhone);
        dest.writeString(ToName);
        dest.writeString(ToZipCode);
        dest.writeString(ToAddress);
        dest.writeString(ToMobilePhone);
        dest.writeInt(PickupTimeSlotId);
        dest.writeInt(DeliveryTimeSlotId);
        dest.writeString(Remark);
        dest.writeString(PromoCode);
        dest.writeString(CustomerId);
        dest.writeInt(PaymentTypeId);
        dest.writeDouble(Price);
        dest.writeLong(PickupDate == null ? 0 : PickupDate.getTime());
        dest.writeLong(DeliveryDate == null ? 0 : DeliveryDate.getTime());
        dest.writeString(PickupTimeSlotText);
        dest.writeString(DeliveryTimeSlotText);
        dest.writeString(ProductTypeText);
        dest.writeString(SizeText);
        dest.writeString(ServiceText);

    }
}
