package com.roadbull.user;


import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.braintreepayments.api.BraintreePaymentActivity;
import com.braintreepayments.api.PaymentRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmOrderFragment extends Fragment {

    private TextView productTypeTextView;
    private TextView sizeTextView;
    private TextView serviceTextView;
    private TextView fromNameTextView;
    private TextView fromZipCodeTextView;
    private TextView fromAddressTextView;
    private TextView fromMobilePhoneTextView;
    private TextView fromDateTextView;
    private TextView fromTimeTextView;
    private TextView toNameTextView;
    private TextView toZipCodeTextView;
    private TextView toAddressTextView;
    private TextView toMobilePhoneTextView;
    private TextView toDateTextView;
    private TextView toTimeTextView;
    private TextView priceTextView;
    private Button paymentTypeButton;
    private Button confirmButton;

    private Order order = null;
    private double credit;

    private JSONObject selectedCreditCard;
    private int selectedCreditCardIndex;

    private ArrayList<JSONObject> creditCards;
    private PaymentAccountAdapter paymentAccountAdapter;

    private int BRAINTREE_REQUEST_CODE = 1000;
    private String currentOrderId = "";
    private ProgressDialog dialog;

    public ConfirmOrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_order, container, false);

        Bundle bundle = getArguments();
        order = bundle.getParcelable("Order");

        productTypeTextView = (TextView) view.findViewById(R.id.productTypeTextView);
        sizeTextView = (TextView) view.findViewById(R.id.sizeTextView);
        serviceTextView = (TextView) view.findViewById(R.id.serviceTextView);
        fromNameTextView = (TextView) view.findViewById(R.id.fromNameTextView);
        fromZipCodeTextView = (TextView) view.findViewById(R.id.fromZipCodeTextView);
        fromAddressTextView = (TextView) view.findViewById(R.id.fromAddressTextView);
        fromMobilePhoneTextView = (TextView) view.findViewById(R.id.fromMobileTextView);
        fromDateTextView = (TextView) view.findViewById(R.id.fromDateTextView);
        fromTimeTextView = (TextView) view.findViewById(R.id.fromTimeTextView);
        toNameTextView = (TextView) view.findViewById(R.id.toNameTextView);
        toZipCodeTextView = (TextView) view.findViewById(R.id.toZipCodeTextView);
        toAddressTextView = (TextView) view.findViewById(R.id.toAddressTextView);
        toMobilePhoneTextView = (TextView) view.findViewById(R.id.toMobileTextView);
        toDateTextView = (TextView) view.findViewById(R.id.toDateTextView);
        toTimeTextView = (TextView) view.findViewById(R.id.toTimeTextView);
        priceTextView = (TextView) view.findViewById(R.id.priceTextView);

        selectedCreditCard = null;

        creditCards = new ArrayList<>();
        paymentAccountAdapter = new PaymentAccountAdapter(getActivity(), creditCards);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        productTypeTextView.setText(order.ProductTypeText);
        if (order.ProductTypeText.compareToIgnoreCase("document") == 0) {
            Drawable docDrawable;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                docDrawable = getActivity().getDrawable(R.drawable.icon_document_selected);
            } else {
                docDrawable = getActivity().getResources().getDrawable(R.drawable.icon_document_selected);
            }
            productTypeTextView.setCompoundDrawablesWithIntrinsicBounds(null, docDrawable, null, null);
        } else {
            Drawable parcelDrawable;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                parcelDrawable = getActivity().getDrawable(R.drawable.icon_parcel_selected);
            } else {
                parcelDrawable = getActivity().getResources().getDrawable(R.drawable.icon_parcel_selected);
            }
            productTypeTextView.setCompoundDrawablesWithIntrinsicBounds(null, parcelDrawable, null, null);
        }
        sizeTextView.setText(order.SizeText);
        serviceTextView.setText(order.ServiceText);
        fromNameTextView.setText(order.FromName);
        fromZipCodeTextView.setText(order.FromZipCode);
        fromAddressTextView.setText(order.FromAddress);
        fromMobilePhoneTextView.setText(order.FromMobilePhone);
        fromDateTextView.setText(format.format(order.PickupDate == null ? new Date() : order.PickupDate));
        fromTimeTextView.setText(order.PickupTimeSlotText);
        toNameTextView.setText(order.ToName);
        toZipCodeTextView.setText(order.ToZipCode);
        toAddressTextView.setText(order.ToAddress);
        toMobilePhoneTextView.setText(order.ToMobilePhone);
        toDateTextView.setText(format.format(order.DeliveryDate == null ? new Date() : order.DeliveryDate));
        toTimeTextView.setText(order.DeliveryTimeSlotText);
        priceTextView.setText("Total Price\n" + String.format("%,.2f", order.Price));

        paymentTypeButton = (Button) view.findViewById(R.id.paymentTypeButton);
        paymentTypeButton.setEnabled(false);
        paymentTypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.credit_card_popup);

                final ListView listView = (ListView) dialog.findViewById(R.id.accountList);
                listView.setAdapter(paymentAccountAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectedCreditCardIndex = position;
                        paymentAccountAdapter.setSelected(position);
                    }
                });

                Button doneButton = (Button) dialog.findViewById(R.id.doneButton);
                doneButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (selectedCreditCardIndex >= 0 && selectedCreditCardIndex < creditCards.size()) {
                            selectedCreditCard = paymentAccountAdapter.getItem(selectedCreditCardIndex);
                        }

                        if (selectedCreditCard != null) {
                            String cardNumber = selectedCreditCard.optString("CardNumber");

                            // Hide all number except 4 last
                            if (cardNumber != null && cardNumber.length() > 4) {
                                int n = cardNumber.length() - 4;
                                char[] chars = new char[n];
                                Arrays.fill(chars, '*');
                                String result = new String(chars);
                                if (selectedCreditCard.optInt("AccountType") == 1) {
                                    result = "VISA CARD (" + result;
                                } else if (selectedCreditCard.optInt("AccountType") == 2) {
                                    result = "MASTER CARD (" + result;
                                }
                                cardNumber = result + cardNumber.substring(cardNumber.length() - 4, cardNumber.length()) + ")";
                            } else if (cardNumber == null || cardNumber.isEmpty()) {
                                if (selectedCreditCardIndex == 0) {
                                    cardNumber = "Credit Account ($" + String.format("%.2f", credit) + ")";
                                }
                                else {
                                    cardNumber = "Paypal account or VISA/MC";
                                }
                            }
                            paymentTypeButton.setText(cardNumber);
                        }

                        dialog.hide();
                    }
                });

                dialog.show();
            }
        });

        confirmButton = (Button) view.findViewById(R.id.confirmButton);
        confirmButton.setEnabled(false);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitOrder();
            }
        });

        loadPaymentAccounts();

        return view;
    }

    private void loadPaymentAccounts() {
        String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.PaymentAccounts(token, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                JSONObject result = (JSONObject) response;
                if (result.optInt("Code") != 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_wallet))
                            .setMessage(result.optString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                    return;
                } else {
                    creditCards.clear();
//                    JSONArray paymentAccounts = result.optJSONArray("PaymentAccounts");
//                    for (int i = 0; i < paymentAccounts.length(); i++) {
//                        JSONObject paymentAccount = paymentAccounts.getJSONObject(i);
//                        if (paymentAccount.optInt("AccountType") != 0) {
//                            creditCards.add(paymentAccount);
//                        }
//                    }

                    creditCards.add(new JSONObject());
                    JSONObject paypal = new JSONObject();
                    paypal.put("Id", -1);
                    creditCards.add(paypal);
                    credit = result.optDouble("Credit");
                    paymentAccountAdapter.refresh(creditCards, credit);

                    String paymentType = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_PAYMENT_TYPE_NAME_KEY, "");
                    if (paymentType.equalsIgnoreCase("postpaid")) {
                        paymentTypeButton.setEnabled(false);
                        paymentTypeButton.setText("Credit Account ($" + String.format("%.2f", credit) + ")");
                    } else {
                        paymentTypeButton.setEnabled(true);
                    }
                }

                confirmButton.setEnabled(true);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_wallet))
                        .setMessage(R.string.error_network_problem)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private void submitOrder() {
        if (paymentTypeButton.isEnabled() && selectedCreditCard == null) {
            paymentTypeButton.setError(getString(R.string.error_missing_payment_account));
            paymentTypeButton.requestFocus();
            return;
        }

        if (paymentTypeButton.isEnabled() == false || selectedCreditCard.optLong("Id") == 0) {
            if (order.Price > credit) {
                paymentTypeButton.setError(getString(R.string.error_not_enough_credit));
                paymentTypeButton.requestFocus();
                return;
            }
        }

        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.message_loading), true);
        dialog.show();

        final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");

        ApiServices.Book(token, order, selectedCreditCard.optLong("Id"), new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                final JSONObject result = (JSONObject) response;
                if (result.getInt("Code") == 300) {
                    // This is paypal / CC payment
                    currentOrderId = result.getString("OrderId");
                    ApiServices.Token(token, new ApiServices.ServiceCallbackInterface() {
                        @Override
                        public void onSuccess(int statusCode, Object response) throws JSONException {
                            JSONObject tokenResult = (JSONObject) response;

                            PaymentRequest paymentRequest = new PaymentRequest().clientToken(tokenResult.getString("Token"));
                            startActivityForResult(paymentRequest.getIntent(getActivity()), BRAINTREE_REQUEST_CODE);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            dialog.hide();
                            error.printStackTrace();
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(getString(R.string.title_activity_confirm_order))
                                    .setMessage(R.string.error_network_problem)
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    });
                } else if (result.getInt("Code") != 0) {
                    dialog.hide();
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_confirm_order))
                            .setMessage(result.getString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                    dialog.hide();
                    Fragment fragment = new CompleteOrderFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commit();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                dialog.hide();
                error.printStackTrace();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_confirm_order))
                        .setMessage(R.string.error_network_problem)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BRAINTREE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentMethodNonce paymentMethodNonce = data.getParcelableExtra(
                        BraintreePaymentActivity.EXTRA_PAYMENT_METHOD_NONCE
                );
                String nonce = paymentMethodNonce.getNonce();
                // Send the nonce to your server.
                String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
                ApiServices.Checkout(token, currentOrderId, nonce, new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        dialog.hide();
                        final JSONObject checkoutResult = (JSONObject) response;
                        if (checkoutResult.getInt("Code") != 0) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(getString(R.string.title_activity_confirm_order))
                                    .setMessage(checkoutResult.getString("Message"))
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        } else {
                            Fragment fragment = new CompleteOrderFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .commit();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        dialog.hide();
                        error.printStackTrace();
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getString(R.string.title_activity_confirm_order))
                                .setMessage(R.string.error_network_problem)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
            }
            else {
                dialog.hide();
            }
        }
    }
}
