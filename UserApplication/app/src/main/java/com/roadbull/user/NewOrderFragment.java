package com.roadbull.user;


import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewOrderFragment extends Fragment {

    private int productTypeId = 0;
    private int sizeId = 0;
    private int serviceId = 0;
    private Date fromDate = null;
    private Date toDate = null;
    private int fromTimeSlotId = 0;
    private int toTimeSlotId = 0;
    private String promoCode = "";
    private double price = 0;
    private JSONObject currentService = null;
    private double closeWindowTime = 0;

    private ArrayList<JSONObject> serviceList = new ArrayList<>();
    private JSONArray pickupTimeSlots;
    private JSONArray deliveryTimeSlots;
    private Date minPickupDate = null;
    private Date maxPickupDate = null;
    private Date minDeliveryDate = null;
    private Date maxDeliveryDate = null;

    private JSONObject[] mProductTypes;
    private ArrayList<String> serviceTexts = new ArrayList<>();
    private ArrayList<String> pickupTimeSlotTexts = new ArrayList<>();
    private ArrayList<String> deliveryTimeSlotTexts = new ArrayList<>();

    private ArrayList<Date> outOfServiceDays = new ArrayList<>();

    private View view;
    private LinearLayout mSizeContainer;
    private ProgressDialog dialog;
    private Button documentButton;
    private Button parcelButton;
    private Button serviceTypeButton;
    private ImageButton moreInfoButton;
    private Button fromDateButton;
    private Button toDateButton;
    private Button fromTimeButton;
    private Button toTimeButton;
    private Button bookButton;
    private EditText fromNameEditText;
    private EditText fromPostalCodeEditText;
    private EditText fromAddressEditText;
    private EditText fromMobileEditText;
    private EditText toNameEditText;
    private EditText toPostalCodeEditText;
    private EditText toAddressEditText;
    private EditText toMobileEditText;
    private TextView totalPriceTextView;
    private Button promoCodeButton;
    private EditText remarkEditText;

    public NewOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_new_order, container, false);

        mSizeContainer = (LinearLayout) view.findViewById(R.id.sizeContainer);
        documentButton = (Button) view.findViewById(R.id.documentButton);
        parcelButton = (Button) view.findViewById(R.id.parcelButton);
        serviceTypeButton = (Button) view.findViewById(R.id.serviceTypeButton);
        moreInfoButton = (ImageButton) view.findViewById(R.id.moreInfoButton);
        fromDateButton = (Button) view.findViewById(R.id.fromDateButton);
        toDateButton = (Button) view.findViewById(R.id.toDateButton);
        fromTimeButton = (Button) view.findViewById(R.id.fromTimeButton);
        toTimeButton = (Button) view.findViewById(R.id.toTimeButton);
        promoCodeButton = (Button) view.findViewById(R.id.promoCodeButton);
        bookButton = (Button) view.findViewById(R.id.bookButton);
        fromNameEditText = (EditText) view.findViewById(R.id.fromNameEditText);
        fromPostalCodeEditText = (EditText) view.findViewById(R.id.fromPostalCode);
        fromAddressEditText = (EditText) view.findViewById(R.id.fromAddressEditText);
        fromMobileEditText = (EditText) view.findViewById(R.id.fromMobileEditText);
        toNameEditText = (EditText) view.findViewById(R.id.toNameEditText);
        toPostalCodeEditText = (EditText) view.findViewById(R.id.toPostalCode);
        toAddressEditText = (EditText) view.findViewById(R.id.toAddressEditText);
        toMobileEditText = (EditText) view.findViewById(R.id.toMobileEditText);
        totalPriceTextView = (TextView) view.findViewById(R.id.totalPriceTextView);
        remarkEditText = (EditText) view.findViewById(R.id.remarkEditText);

        // Set default values
        fromNameEditText.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_FULLNAME_KEY, ""));
        fromPostalCodeEditText.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_ZIP_CODE_KEY, ""));
        fromAddressEditText.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_ADDRESS_KEY, ""));
        fromMobileEditText.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_MOBILE_KEY, ""));

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String[] offdays = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.OFF_DAYS_SETTING, "").split(",");
        for (int i = 0; i < offdays.length; i++) {
            Date date = null;
            try {
                date = format.parse(offdays[i]);
                outOfServiceDays.add(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        fromDateButton.setEnabled(false);
        fromTimeButton.setEnabled(false);
        toDateButton.setEnabled(false);
        toTimeButton.setEnabled(false);

        moreInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TYPE_OF_SERVICE_URL, "");
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });

        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.message_loading), true);

        // Load product types
        ApiServices.ProductTypes(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, ""), new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) {
                try {
                    dialog.hide();

                    JSONObject result = (JSONObject) response;

                    closeWindowTime = result.optDouble("CloseWindowTime");

                    final JSONArray productTypes = result.optJSONArray("ProductTypes");
                    mProductTypes = new JSONObject[2];

                    for (int i = 0; i < productTypes.length(); i++) {
                        JSONObject productType = productTypes.getJSONObject(i);
                        if (productType.optString("ProductTypeName").equalsIgnoreCase("document")) {
                            mProductTypes[0] = productType;
                        }
                        if (productType.optString("ProductTypeName").equalsIgnoreCase("parcel")) {
                            mProductTypes[1] = productType;
                        }
                    }

                    if (mProductTypes[0] == null || mProductTypes[1] == null) {
                        // Cannot retrieve product types correctly
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getString(R.string.title_activity_new_order))
                                .setMessage(R.string.error_network_problem)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                    documentButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Drawable docDrawable;
                            Drawable parcelDrawable;
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                                docDrawable = getActivity().getDrawable(R.drawable.icon_document_selected);
                                parcelDrawable = getActivity().getDrawable(R.drawable.icon_parcel);
                            } else {
                                docDrawable = getActivity().getResources().getDrawable(R.drawable.icon_document_selected);
                                parcelDrawable = getActivity().getResources().getDrawable(R.drawable.icon_parcel);
                            }
                            documentButton.setCompoundDrawablesWithIntrinsicBounds(null, docDrawable, null, null);
                            parcelButton.setCompoundDrawablesWithIntrinsicBounds(null, parcelDrawable, null, null);

                            productTypeId = mProductTypes[0].optInt("Id");
                            UpdateSizes();
                        }
                    });

                    parcelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Drawable docDrawable;
                            Drawable parcelDrawable;
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                                docDrawable = getActivity().getDrawable(R.drawable.icon_document);
                                parcelDrawable = getActivity().getDrawable(R.drawable.icon_parcel_selected);
                            } else {
                                docDrawable = getActivity().getResources().getDrawable(R.drawable.icon_document);
                                parcelDrawable = getActivity().getResources().getDrawable(R.drawable.icon_parcel_selected);
                            }
                            documentButton.setCompoundDrawablesWithIntrinsicBounds(null, docDrawable, null, null);
                            parcelButton.setCompoundDrawablesWithIntrinsicBounds(null, parcelDrawable, null, null);

                            productTypeId = mProductTypes[1].optInt("Id");
                            UpdateSizes();
                        }
                    });

                    serviceTypeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog selectDialog = new Dialog(getActivity());
                            selectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            selectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            selectDialog.setContentView(R.layout.services_popup);
                            ListView services = (ListView) selectDialog.findViewById(R.id.servicesListView);
                            services.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, serviceTexts));
                            selectDialog.show();
                            services.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    JSONObject service = serviceList.get(position);
                                    serviceId = service.optInt("Id");
                                    serviceTypeButton.setText(service.optString("ServiceName"));
                                    UpdateInputs();
                                    UpdatePrice();
                                    selectDialog.hide();
                                }
                            });
                        }
                    });

                    fromDateButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar now = Calendar.getInstance();
                            final CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", now.get(Calendar.MONTH) + 1, now.get(Calendar.YEAR));

                            if (minPickupDate != null) {
                                dialogCaldroidFragment.setMinDate(minPickupDate);
                            } else {
                                Calendar minPickupCalendar = Calendar.getInstance();
                                minPickupCalendar.set(Calendar.MILLISECOND, 0);
                                minPickupCalendar.set(Calendar.SECOND, 0);
                                minPickupCalendar.set(Calendar.MINUTE, 0);
                                minPickupCalendar.set(Calendar.HOUR_OF_DAY, 0);
                                dialogCaldroidFragment.setMinDate(minPickupCalendar.getTime());
                            }
                            if (maxPickupDate != null) {
                                dialogCaldroidFragment.setMaxDate(maxPickupDate);
                            }

                            dialogCaldroidFragment.setDisableDates(GetDisabledDates(minPickupDate, maxPickupDate));

                            // Add a listener as per documentation
                            dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

                                @Override
                                public void onSelectDate(Date date, View view) {
                                    // You can perform your method on selected date
                                    ChangePickupDate(date);
                                    dialogCaldroidFragment.dismiss();
                                }
                            });

                            // show the dialog
                            dialogCaldroidFragment.show(((AppCompatActivity) getActivity()).getSupportFragmentManager(), "TAG");
                        }
                    });

                    toDateButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar now = Calendar.getInstance();
                            final CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", now.get(Calendar.MONTH) + 1, now.get(Calendar.YEAR));

                            if (minDeliveryDate != null) {
                                dialogCaldroidFragment.setMinDate(minDeliveryDate);
                            } else {
                                Calendar minPickupCalendar = Calendar.getInstance();
                                minPickupCalendar.set(Calendar.MILLISECOND, 0);
                                minPickupCalendar.set(Calendar.SECOND, 0);
                                minPickupCalendar.set(Calendar.MINUTE, 0);
                                minPickupCalendar.set(Calendar.HOUR_OF_DAY, 0);
                                dialogCaldroidFragment.setMinDate(minPickupCalendar.getTime());
                            }
                            if (maxDeliveryDate != null) {
                                dialogCaldroidFragment.setMaxDate(maxDeliveryDate);
                            }

                            dialogCaldroidFragment.setDisableDates(GetDisabledDates(minDeliveryDate, maxDeliveryDate));

                            dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

                                @Override
                                public void onSelectDate(Date date, View view) {
                                    // You can perform your method on selected date
                                    ChangeDeliveryDate(date);
                                    dialogCaldroidFragment.dismiss();
                                }

                            });

                            // show the dialog
                            dialogCaldroidFragment.show(((AppCompatActivity) getActivity()).getSupportFragmentManager(), "TAG");
                        }
                    });

                    fromTimeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog selectDialog = new Dialog(getActivity());
                            selectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            selectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            selectDialog.setContentView(R.layout.services_popup);
                            ListView timeSlots = (ListView) selectDialog.findViewById(R.id.servicesListView);
                            timeSlots.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, pickupTimeSlotTexts));
                            selectDialog.show();
                            timeSlots.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    ChangePickupTimeSlot(position);
                                    selectDialog.hide();
                                }
                            });
                        }
                    });

                    toTimeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog selectDialog = new Dialog(getActivity());
                            selectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            selectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            selectDialog.setContentView(R.layout.services_popup);
                            ListView timeSlots = (ListView) selectDialog.findViewById(R.id.servicesListView);
                            timeSlots.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, deliveryTimeSlotTexts));
                            selectDialog.show();
                            timeSlots.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    ChangeDeliveryTimeSlot(position);
                                    selectDialog.hide();
                                }
                            });
                        }
                    });

                    promoCodeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog inputDialog = new Dialog(getActivity());
                            inputDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            inputDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            inputDialog.setContentView(R.layout.promo_popup);
                            inputDialog.show();
                            final EditText promoCodeEditText = (EditText) inputDialog.findViewById(R.id.promoCodeEditText);
                            promoCodeEditText.setText(promoCode);
                            promoCodeEditText.selectAll();
                            Button doneButton = (Button) inputDialog.findViewById(R.id.doneButton);
                            doneButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    promoCode = promoCodeEditText.getText().toString();
                                    if (promoCode.isEmpty()) {
                                        promoCodeButton.setText(R.string.promo_code);
                                    } else {
                                        promoCodeButton.setText(promoCode);
                                    }
                                    UpdatePrice();
                                    inputDialog.hide();
                                }
                            });
                        }
                    });

                    bookButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SubmitOrder();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.hide();
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_new_order))
                            .setMessage(R.string.error_network_problem)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                dialog.hide();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_new_order))
                        .setMessage(R.string.error_network_problem)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        fromPostalCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ApiServices.CheckZipCode(fromPostalCodeEditText.getText().toString(), new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        JSONObject postal = (JSONObject) response;
                        fromAddressEditText.setText(postal.optString("UnitNumber") + " " + postal.optString("StreetName"));
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        fromAddressEditText.setText("");
                    }
                });
            }
        });

        toPostalCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ApiServices.CheckZipCode(toPostalCodeEditText.getText().toString(), new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        JSONObject postal = (JSONObject) response;
                        toAddressEditText.setText(postal.optString("UnitNumber") + " " + postal.optString("StreetName"));
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        toAddressEditText.setText("");
                    }
                });
            }
        });

        return view;
    }

    private void ResetInputs() {
        currentService = null;
        fromDate = null;
        toDate = null;
        fromTimeSlotId = 0;
        toTimeSlotId = 0;
        minPickupDate = null;
        maxPickupDate = null;
        minDeliveryDate = null;
        maxDeliveryDate = null;
        pickupTimeSlotTexts.clear();
        deliveryTimeSlotTexts.clear();
        fromDateButton.setEnabled(false);
        fromTimeButton.setEnabled(false);
        toDateButton.setEnabled(false);
        toTimeButton.setEnabled(false);
        fromDateButton.setText(R.string.date);
        toDateButton.setText(R.string.date);
        fromTimeButton.setText(R.string.time);
        toTimeButton.setText(R.string.time);
    }

    private void UpdateSizes() {
        // Clear old controls
        mSizeContainer.removeAllViews();

        if (productTypeId <= 0) {
            return;
        }

        ResetInputs();
        serviceTypeButton.setVisibility(View.GONE);

        JSONObject productType = null;
        if (mProductTypes[0].optInt("Id") == productTypeId) {
            productType = mProductTypes[0];
        } else if (mProductTypes[1].optInt("Id") == productTypeId) {
            productType = mProductTypes[1];
        }

        if (productType != null && productType.optJSONArray("Sizes") != null) {
            JSONArray sizes = productType.optJSONArray("Sizes");
            for (int i = 0; i < sizes.length(); i++) {
                JSONObject size = sizes.optJSONObject(i);
                Button sizeButton = new Button(getActivity());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParams.setMargins(5, 0, 5, 0);
                layoutParams.weight = 1;
                sizeButton.setLayoutParams(layoutParams);
                //sizeButton.setText(size.optString("SizeName") + "\n" + size.optDouble("FromLength") + "-" + size.optDouble("ToLength") + " cm\n" + size.optDouble("FromWeight") + "-" + size.optDouble("ToWeight") + " kg");
                sizeButton.setText(size.optString("SizeName"));
                sizeButton.setBackgroundResource(R.drawable.round_text_box);
                sizeButton.setTag(size.optInt("Id"));
                sizeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int idx = 0; idx < mSizeContainer.getChildCount(); idx++) {
                            mSizeContainer.getChildAt(idx).setBackgroundResource(R.drawable.round_text_box);
                        }
                        v.setBackgroundResource(R.drawable.green_text_box);

                        sizeId = (int) v.getTag();
                        UpdateServices();
                    }
                });
                mSizeContainer.addView(sizeButton);
            }
        }
    }

    private void UpdateServices() {
        serviceTypeButton.setText(R.string.type_of_delivery);
        serviceTexts.clear();
        serviceList.clear();

        if (productTypeId <= 0 || sizeId <= 0) {
            return;
        }

        ResetInputs();

        JSONObject productType = null;
        if (mProductTypes[0].optInt("Id") == productTypeId) {
            productType = mProductTypes[0];
        } else if (mProductTypes[1].optInt("Id") == productTypeId) {
            productType = mProductTypes[1];
        }

        if (productType != null && productType.optJSONArray("Sizes") != null) {
            JSONArray sizes = productType.optJSONArray("Sizes");
            for (int i = 0; i < sizes.length(); i++) {
                JSONObject size = sizes.optJSONObject(i);
                if (size.optInt("Id") == sizeId) {
                    serviceTypeButton.setVisibility(View.VISIBLE);
                    JSONArray services = size.optJSONArray("Services");
                    if (services != null) {
                        for (int j = 0; j < services.length(); j++) {
                            serviceTexts.add(services.optJSONObject(j).optString("ServiceName"));
                            serviceList.add(services.optJSONObject(j));
                        }
                    }
                }
            }
        }
    }

    private void UpdateInputs() {
        if (productTypeId <= 0 || sizeId <= 0 || serviceId <= 0) {
            return;
        }

        ResetInputs();

        JSONObject productType = null;
        if (mProductTypes[0].optInt("Id") == productTypeId) {
            productType = mProductTypes[0];
        } else if (mProductTypes[1].optInt("Id") == productTypeId) {
            productType = mProductTypes[1];
        }

        if (productType != null && productType.optJSONArray("Sizes") != null) {
            JSONArray sizes = productType.optJSONArray("Sizes");
            for (int i = 0; i < sizes.length(); i++) {
                JSONObject size = sizes.optJSONObject(i);
                if (size.optInt("Id") == sizeId) {
                    serviceTypeButton.setVisibility(View.VISIBLE);
                    JSONArray services = size.optJSONArray("Services");
                    if (services != null) {
                        for (int j = 0; j < services.length(); j++) {
                            if (services.optJSONObject(j).optInt("Id") == serviceId) {
                                currentService = services.optJSONObject(j);
                                if (currentService.optBoolean("IsPickupDateAllow", false) &&
                                        currentService.optString("AvailablePickupDateRange") != null) {
                                    fromDateButton.setEnabled(true);
                                    String[] range = currentService.optString("AvailablePickupDateRange").split(",");
                                    Calendar now = Calendar.getInstance();
                                    now.set(Calendar.MILLISECOND, 0);
                                    now.set(Calendar.SECOND, 0);
                                    now.set(Calendar.MINUTE, 0);
                                    now.set(Calendar.HOUR_OF_DAY, 0);

                                    Calendar minPickupCalendar = Calendar.getInstance();
                                    minPickupCalendar.set(Calendar.MILLISECOND, 0);
                                    minPickupCalendar.set(Calendar.SECOND, 0);
                                    minPickupCalendar.set(Calendar.MINUTE, 0);
                                    minPickupCalendar.set(Calendar.HOUR_OF_DAY, 0);
                                    minPickupCalendar.add(Calendar.DATE, Integer.parseInt(range[0], 10));
                                    int shift = CountWeekend(now.getTime(), minPickupCalendar.getTime());
                                    minPickupCalendar.add(Calendar.DATE, shift);
                                    minPickupDate = minPickupCalendar.getTime();
                                    Calendar maxPickupCalendar = Calendar.getInstance();
                                    maxPickupCalendar.set(Calendar.MILLISECOND, 0);
                                    maxPickupCalendar.set(Calendar.SECOND, 0);
                                    maxPickupCalendar.set(Calendar.MINUTE, 0);
                                    maxPickupCalendar.set(Calendar.HOUR_OF_DAY, 0);
                                    maxPickupCalendar.add(Calendar.DATE, Integer.parseInt(range[1], 10));
                                    shift = CountWeekend(now.getTime(), maxPickupCalendar.getTime());
                                    maxPickupCalendar.add(Calendar.DATE, shift);
                                    maxPickupDate = maxPickupCalendar.getTime();

                                    // If one selection only
                                    if (minPickupDate.compareTo(maxPickupDate) == 0) {
                                        fromDateButton.setEnabled(false);
                                        ChangePickupDate(minPickupDate);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private int CountWeekend(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null) {
            return 0;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fromDate);
        int result = 0;
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++result;
        } else {
            for (int i = 0; i < outOfServiceDays.size(); i++) {
                if (outOfServiceDays.get(i).getTime() == calendar.getTime().getTime()) {
                    ++result;
                }
            }
        }
        while (calendar.getTime().before(toDate)) {
            calendar.add(Calendar.DATE, 1);
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                ++result;
            } else {
                for (int i = 0; i < outOfServiceDays.size(); i++) {
                    if (outOfServiceDays.get(i).getTime() == calendar.getTime().getTime()) {
                        ++result;
                    }
                }
            }
        }

        return result;
    }

    private ArrayList<Date> GetDisabledDates(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null) {
            return new ArrayList<>();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fromDate);
        ArrayList<Date> result = new ArrayList<>();
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            result.add(calendar.getTime());
        } else {
            for (int i = 0; i < outOfServiceDays.size(); i++) {
                if (outOfServiceDays.get(i).getTime() == calendar.getTime().getTime()) {
                    result.add(calendar.getTime());
                }
            }
        }
        while (calendar.getTime().before(toDate)) {
            calendar.add(Calendar.DATE, 1);
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                result.add(calendar.getTime());
            } else {
                for (int i = 0; i < outOfServiceDays.size(); i++) {
                    if (outOfServiceDays.get(i).getTime() == calendar.getTime().getTime()) {
                        result.add(calendar.getTime());
                    }
                }
            }
        }

        return result;
    }

    private void ChangePickupDate(Date date) {
        fromDate = date;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        fromDateButton.setText(format.format(fromDate));

        // Change Delivery date range
        toDate = null;
        toDateButton.setText(R.string.date);
        toDateButton.setEnabled(false);
        if (currentService.optBoolean("IsDeliveryDateAllow", false) && currentService.optString("AvailableDeliveryDateRange") != null) {
            String[] range = currentService.optString("AvailableDeliveryDateRange").split(",");
            Calendar minDeliveryCalendar = Calendar.getInstance();
            minDeliveryCalendar.setTime(date);
            minDeliveryCalendar.add(Calendar.DATE, Integer.parseInt(range[0]));
            int weekend = CountWeekend(date, minDeliveryCalendar.getTime());
            minDeliveryCalendar.add(Calendar.DATE, weekend);
            minDeliveryDate = minDeliveryCalendar.getTime();
            Calendar maxDeliveryCalendar = Calendar.getInstance();
            maxDeliveryCalendar.setTime(date);
            maxDeliveryCalendar.add(Calendar.DATE, Integer.parseInt(range[1]));
            weekend = CountWeekend(date, maxDeliveryCalendar.getTime());
            maxDeliveryCalendar.add(Calendar.DATE, weekend);
            maxDeliveryDate = maxDeliveryCalendar.getTime();

            if (minDeliveryDate.compareTo(maxDeliveryDate) == 0) {
                toDateButton.setEnabled(false);
                ChangeDeliveryDate(minDeliveryDate);
            } else {
                toDateButton.setEnabled(true);
            }
        }

        // Generate time slot
        fromTimeSlotId = 0;
        fromTimeButton.setEnabled(false);
        fromTimeButton.setText(R.string.time);
        pickupTimeSlotTexts.clear();
        pickupTimeSlots = currentService.optJSONArray("PickupTimeSlots");
        if (currentService.optBoolean("IsPickupTimeAllow", false) && pickupTimeSlots != null) {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
            for (int i = 0; i < pickupTimeSlots.length(); i++) {
                JSONObject timeSlot = pickupTimeSlots.optJSONObject(i);
                try {
                    Date fromTime = inputFormat.parse(timeSlot.optString("FromTime"));
                    Date toTime = inputFormat.parse(timeSlot.optString("ToTime"));

                    // Check valid
                    Calendar selectedDate = Calendar.getInstance();
                    selectedDate.setTime(date);
                    Calendar now = Calendar.getInstance();
                    if (selectedDate.get(Calendar.DATE) == now.get(Calendar.DATE) && selectedDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
                            && selectedDate.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
                        double limitHour = (now.get(Calendar.HOUR_OF_DAY) * 60.0 + now.get(Calendar.MINUTE) + closeWindowTime) / 60.0;
                        double nowHour = now.get(Calendar.HOUR_OF_DAY) + now.get(Calendar.MINUTE) / 60.0;
                        Calendar fromCalendar = Calendar.getInstance();
                        fromCalendar.setTime(fromTime);
                        Calendar toCalendar = Calendar.getInstance();
                        toCalendar.setTime(toTime);

                        if (timeSlot.optBoolean("IsAllowPreOrder")) {
                            if (limitHour < fromCalendar.get(Calendar.HOUR_OF_DAY) + fromCalendar.get(Calendar.MINUTE) / 60.0 &&
                                    nowHour <= toCalendar.get(Calendar.HOUR_OF_DAY) + toCalendar.get(Calendar.MINUTE) / 60.0) {
                                pickupTimeSlotTexts.add(timeSlot.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")");
                            }
                        } else {
                            if (nowHour >= fromCalendar.get(Calendar.HOUR_OF_DAY) + fromCalendar.get(Calendar.MINUTE) / 60.0 &&
                                    nowHour <= toCalendar.get(Calendar.HOUR_OF_DAY) + toCalendar.get(Calendar.MINUTE) / 60.0) {
                                pickupTimeSlotTexts.add(timeSlot.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")");
                            }
                        }
                    } else {
                        pickupTimeSlotTexts.add(timeSlot.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (pickupTimeSlotTexts.size() == 1) {
                // Select by default
                ChangePickupTimeSlot(0);
            } else if (pickupTimeSlotTexts.size() > 1) {
                fromTimeButton.setEnabled(true);
            }
        }
    }

    private void ChangeDeliveryDate(Date date) {
        toDate = date;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        toDateButton.setText(format.format(toDate));

        // Update Delivery time picker
        toTimeSlotId = 0;
        toTimeButton.setEnabled(false);
        toTimeButton.setText(R.string.time);
        if (pickupTimeSlots != null) {
            for (int i = 0; i < pickupTimeSlots.length(); i++) {
                if (pickupTimeSlots.optJSONObject(i).optInt("Id") == fromTimeSlotId) {
                    JSONObject ts = pickupTimeSlots.optJSONObject(i);
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
                    try {
                        Date fromTime = inputFormat.parse(ts.optString("FromTime"));
                        Date toTime = inputFormat.parse(ts.optString("ToTime"));
                        String text = ts.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")";
                        for (int j = 0; j < pickupTimeSlotTexts.size(); j++) {
                            if (text.equals(pickupTimeSlotTexts.get(j))) {
                                ChangePickupTimeSlot(j);
                            }
                        }
                    } catch (Exception exc) {
                    }
                }
            }
        }
    }

    private void ChangePickupTimeSlot(int position) {
        if (pickupTimeSlots == null || pickupTimeSlotTexts == null || position < 0 || position >= pickupTimeSlotTexts.size()) {
            return;
        }

        JSONObject timeSlot = null;
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        try {
            for (int i = 0; i < pickupTimeSlots.length(); i++) {
                JSONObject ts = pickupTimeSlots.optJSONObject(i);
                Date fromTime = inputFormat.parse(ts.optString("FromTime"));
                Date toTime = inputFormat.parse(ts.optString("ToTime"));
                String text = ts.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")";
                if (text.equals(pickupTimeSlotTexts.get(position))) {
                    timeSlot = ts;
                }
            }
        } catch (Exception exc) {
            timeSlot = null;
        }

        if (timeSlot == null) {
            return;
        }

        fromTimeSlotId = timeSlot.optInt("Id");
        fromTimeButton.setText(pickupTimeSlotTexts.get(position));

        // Update Delivery time slots
        toTimeSlotId = 0;
        toTimeButton.setEnabled(false);
        toTimeButton.setText(R.string.time);
        deliveryTimeSlotTexts.clear();
        deliveryTimeSlots = currentService.optJSONArray("DeliveryTimeSlots");

        if (currentService.optBoolean("IsDeliveryTimeAllow", false) && deliveryTimeSlots != null && toDate != null) {
            try {
                Date pickupCompareTime;
                if (mProductTypes[0].optInt("Id") == productTypeId) {
                    // Document
                    pickupCompareTime = inputFormat.parse(timeSlot.optString("FromTime"));
                } else {
                    pickupCompareTime = inputFormat.parse(timeSlot.optString("ToTime"));
                }

                for (int i = 0; i < deliveryTimeSlots.length(); i++) {
                    JSONObject deliveryTimeSlot = deliveryTimeSlots.optJSONObject(i);
                    Date fromTime = inputFormat.parse(deliveryTimeSlot.optString("FromTime"));
                    Date toTime = inputFormat.parse(deliveryTimeSlot.optString("ToTime"));

                    if (fromDate.compareTo(toDate) != 0) {
                        deliveryTimeSlotTexts.add(deliveryTimeSlot.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")");
                        continue;
                    }

                    // Check valid
                    Calendar fromCalendar = Calendar.getInstance();
                    fromCalendar.setTime(pickupCompareTime);
                    Calendar toCalendar = Calendar.getInstance();
                    toCalendar.setTime(fromTime);

                    double fromHour = fromCalendar.get(Calendar.HOUR_OF_DAY) + fromCalendar.get(Calendar.MINUTE) / 60.0;
                    double toHour = toCalendar.get(Calendar.HOUR_OF_DAY) + toCalendar.get(Calendar.MINUTE) / 60.0;

                    Calendar now = Calendar.getInstance();
                    double nowHour = now.get(Calendar.HOUR_OF_DAY) + now.get(Calendar.MINUTE) / 60.0;

                    if (deliveryTimeSlot.optBoolean("IsAllowPreOrder") && fromHour < toHour) {
                        deliveryTimeSlotTexts.add(deliveryTimeSlot.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")");
                    } else if (deliveryTimeSlot.optBoolean("IsAllowPreOrder") == false) {
                        fromCalendar.setTime(fromTime);
                        toCalendar.setTime(toTime);
                        Calendar selectedDate = Calendar.getInstance();
                        selectedDate.setTime(toDate);
                        if (selectedDate.get(Calendar.DATE) == now.get(Calendar.DATE) && selectedDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
                                && selectedDate.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
                            if (nowHour >= fromCalendar.get(Calendar.HOUR_OF_DAY) + fromCalendar.get(Calendar.MINUTE) / 60.0 &&
                                    nowHour <= toCalendar.get(Calendar.HOUR_OF_DAY) + toCalendar.get(Calendar.MINUTE) / 60.0) {
                                deliveryTimeSlotTexts.add(deliveryTimeSlot.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")");
                            }
                        }
                        else {
                            deliveryTimeSlotTexts.add(deliveryTimeSlot.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")");
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (deliveryTimeSlotTexts.size() == 1) {
                // Select by default
                toTimeButton.setEnabled(false);
                ChangeDeliveryTimeSlot(0);
            } else if (deliveryTimeSlotTexts.size() > 1) {
                toTimeButton.setEnabled(true);
            }
        }
    }

    private void ChangeDeliveryTimeSlot(int position) {
        if (deliveryTimeSlots == null || deliveryTimeSlotTexts == null || position < 0 || position >= deliveryTimeSlotTexts.size()) {
            return;
        }

        JSONObject timeSlot = null;
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        try {
            for (int i = 0; i < deliveryTimeSlots.length(); i++) {
                JSONObject ts = deliveryTimeSlots.optJSONObject(i);
                Date fromTime = inputFormat.parse(ts.optString("FromTime"));
                Date toTime = inputFormat.parse(ts.optString("ToTime"));
                String text = ts.optString("TimeSlotName") + " (" + hourFormat.format(fromTime) + " - " + hourFormat.format(toTime) + ")";
                if (text.equals(deliveryTimeSlotTexts.get(position))) {
                    timeSlot = ts;
                }
            }
        } catch (Exception exc) {
            timeSlot = null;
        }

        if (timeSlot == null) {
            return;
        }

        toTimeSlotId = timeSlot.optInt("Id");
        toTimeButton.setText(deliveryTimeSlotTexts.get(position));
    }

    private void UpdatePrice() {
        String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        String customerId = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_ID_KEY, "");
        ApiServices.Price(token, productTypeId, serviceId, sizeId, customerId, promoCode, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                price = ((JSONObject) response).optDouble("Total");
                totalPriceTextView.setText(String.format("%,.2f", price));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                price = 0;
                totalPriceTextView.setText("0");
            }
        });
    }

    private void SubmitOrder() {
        // Validate
        if (productTypeId <= 0) {
            documentButton.setError(getString(R.string.error_missing_product_type));
            documentButton.requestFocus();
            return;
        }

        if (sizeId <= 0) {
            documentButton.setError(getString(R.string.error_missing_size));
            documentButton.requestFocus();
            return;
        }

        if (serviceId <= 0) {
            serviceTypeButton.setError(getString(R.string.error_missing_service));
            serviceTypeButton.requestFocus();
            return;
        }

        if (fromNameEditText.getText().toString().isEmpty()) {
            fromNameEditText.setError(getString(R.string.error_missing_from_name));
            fromNameEditText.requestFocus();
            return;
        }

        if (fromPostalCodeEditText.getText().toString().isEmpty()) {
            fromPostalCodeEditText.setError(getString(R.string.error_missing_from_postal_code));
            fromPostalCodeEditText.requestFocus();
            return;
        }

        if (fromAddressEditText.getText().toString().isEmpty()) {
            fromAddressEditText.setError(getString(R.string.error_missing_from_address));
            fromAddressEditText.requestFocus();
            return;
        }

        if (fromMobileEditText.getText().toString().isEmpty()) {
            fromMobileEditText.setError(getString(R.string.error_missing_from_mobile));
            fromMobileEditText.requestFocus();
            return;
        }

        if (currentService.optBoolean("IsPickupDateAllow", false) && fromDate == null) {
            fromDateButton.setError(getString(R.string.error_missing_from_date));
            fromDateButton.requestFocus();
            return;
        }

        if (currentService.optBoolean("IsPickupTimeAllow", false) && fromTimeSlotId <= 0) {
            fromTimeButton.setError(getString(R.string.error_missing_from_time));
            fromTimeButton.requestFocus();
            return;
        }

        if (toNameEditText.getText().toString().isEmpty()) {
            toNameEditText.setError(getString(R.string.error_missing_to_name));
            toNameEditText.requestFocus();
            return;
        }

        if (toPostalCodeEditText.getText().toString().isEmpty()) {
            toPostalCodeEditText.setError(getString(R.string.error_missing_to_postal_code));
            toPostalCodeEditText.requestFocus();
            return;
        }

        if (toAddressEditText.getText().toString().isEmpty()) {
            toAddressEditText.setError(getString(R.string.error_missing_to_address));
            toAddressEditText.requestFocus();
            return;
        }

        if (toMobileEditText.getText().toString().isEmpty()) {
            toMobileEditText.setError(getString(R.string.error_missing_to_mobile));
            toMobileEditText.requestFocus();
            return;
        }

        if (currentService.optBoolean("IsDeliveryDateAllow", false) && toDate == null) {
            toDateButton.setError(getString(R.string.error_missing_to_date));
            toDateButton.requestFocus();
            return;
        }

        if (currentService.optBoolean("IsDeliveryTimeAllow", false) && toTimeSlotId <= 0) {
            toTimeButton.setError(getString(R.string.error_missing_to_time));
            toTimeButton.requestFocus();
            return;
        }

        Order order = new Order();
        order.ProductTypeId = productTypeId;
        order.ServiceId = serviceId;
        order.SizeId = sizeId;
        order.FromName = fromNameEditText.getText().toString();
        order.FromZipCode = fromPostalCodeEditText.getText().toString();
        order.FromAddress = fromAddressEditText.getText().toString();
        order.FromMobilePhone = fromMobileEditText.getText().toString();
        order.ToName = toNameEditText.getText().toString();
        order.ToZipCode = toPostalCodeEditText.getText().toString();
        order.ToAddress = toAddressEditText.getText().toString();
        order.ToMobilePhone = toMobileEditText.getText().toString();
        order.PickupDate = fromDate;
        order.PickupTimeSlotId = fromTimeSlotId;
        order.DeliveryDate = toDate;
        order.DeliveryTimeSlotId = toTimeSlotId;
        order.Remark = remarkEditText.getText().toString();
        order.PromoCode = promoCode;
        order.CustomerId = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_ID_KEY, "");
        order.PaymentTypeId = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_PAYMENT_TYPE_ID_KEY, 0);
        order.Price = price;
        order.PickupTimeSlotText = fromTimeSlotId > 0 ? fromTimeButton.getText().toString() : "";
        order.DeliveryTimeSlotText = toTimeSlotId > 0 ? toTimeButton.getText().toString() : "";

        JSONArray sizes = null;
        if (mProductTypes[0].optInt("Id") == productTypeId) {
            order.ProductTypeText = mProductTypes[0].optString("ProductTypeName");
            sizes = mProductTypes[0].optJSONArray("Sizes");
        } else if (mProductTypes[1].optInt("Id") == productTypeId) {
            order.ProductTypeText = mProductTypes[1].optString("ProductTypeName");
            sizes = mProductTypes[1].optJSONArray("Sizes");
        } else {
            documentButton.setError(getString(R.string.error_missing_product_type));
            documentButton.requestFocus();
            return;
        }

        if (sizes == null) {
            documentButton.setError(getString(R.string.error_missing_size));
            documentButton.requestFocus();
            return;
        }

        for (int i = 0; i < sizes.length(); i++) {
            JSONObject size = sizes.optJSONObject(i);
            if (size.optInt("Id") == sizeId) {
                order.SizeText = size.optString("SizeName") + ", " + size.optDouble("FromLength") + "-" + size.optDouble("ToLength") + " cm, " + size.optDouble("FromWeight") + "-" + size.optDouble("ToWeight") + " kg";
                JSONArray services = size.optJSONArray("Services");
                if (services != null) {
                    for (int j = 0; j < services.length(); j++) {
                        if (services.optJSONObject(j).optInt("Id") == serviceId) {
                            order.ServiceText = services.optJSONObject(j).optString("ServiceName");
                        }
                    }
                }
            }
        }

        Fragment fragment = new ConfirmOrderFragment();
        Bundle args = new Bundle();
        args.putParcelable("Order", order);
        fragment.setArguments(args);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}
