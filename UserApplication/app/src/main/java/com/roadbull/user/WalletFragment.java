package com.roadbull.user;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class WalletFragment extends Fragment {

    private EditText creditNumberEditText;
    private Spinner expireMonthSpinner;
    private Spinner expireYearSpinner;
    private RadioButton visaButton;
    private RadioButton masterButton;
    private Button verificationButton;
    private ListView paymentAccountsList;
    private ProgressDialog dialog;

    private ArrayList<JSONObject> creditCards;
    private PaymentAccountAdapter paymentAccountAdapter;

    private final int startYear = 1970;

    public WalletFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);

        creditNumberEditText = (EditText) view.findViewById(R.id.creditCardNumberEditText);
        expireMonthSpinner = (Spinner) view.findViewById(R.id.expireMonth);
        expireYearSpinner = (Spinner) view.findViewById(R.id.expireYear);
        paymentAccountsList = (ListView) view.findViewById(R.id.paymentAccountsList);
        visaButton = (RadioButton) view.findViewById(R.id.visaRadio);
        masterButton = (RadioButton) view.findViewById(R.id.masterRadio);

        ArrayList<String> months = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            months.add(String.valueOf(i));
        }
        expireMonthSpinner.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.large_spinner_item, months));

        ArrayList<String> years = new ArrayList<>();
        for (int i = startYear; i <= 2100; i++) {
            years.add(String.valueOf(i));
        }
        expireYearSpinner.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.large_spinner_item, years));

        creditCards = new ArrayList<>();
        paymentAccountAdapter = new PaymentAccountAdapter(getActivity(), creditCards);
        // Apply the adapter to the list
        paymentAccountsList.setAdapter(paymentAccountAdapter);

        verificationButton = (Button) view.findViewById(R.id.verificationButton);
        verificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addCard();
//                final Dialog dialog = new Dialog(getActivity());
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                dialog.setContentView(R.layout.verification_popup);
//                dialog.show();
            }
        });

        visaButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                masterButton.setChecked(!isChecked);
            }
        });

        masterButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                visaButton.setChecked(!isChecked);
            }
        });

        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.message_loading), true);

        loadPaymentAccounts();

        return view;
    }

    private void loadPaymentAccounts() {
        dialog.show();
        String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.PaymentAccounts(token, new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                JSONObject result = (JSONObject) response;
                dialog.hide();
                if (result.optInt("Code") != 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.title_activity_wallet))
                            .setMessage(result.optString("Message"))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                    return;
                } else {
                    creditCards.clear();
                    JSONArray paymentAccounts = result.optJSONArray("PaymentAccounts");
                    for (int i = 0; i < paymentAccounts.length(); i++) {
                        JSONObject paymentAccount = paymentAccounts.getJSONObject(i);
                        if (paymentAccount.optInt("AccountType") != 0) {
                            creditCards.add(paymentAccount);
                        }
                    }
                    paymentAccountAdapter.refresh(creditCards, result.optDouble("Credit"));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                dialog.hide();
                error.printStackTrace();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.title_activity_wallet))
                        .setMessage(R.string.error_network_problem)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private void addCard() {
        // Visa has type 1, Master has type 2
        if (!visaButton.isChecked() && !masterButton.isChecked()) {
            visaButton.setError(getString(R.string.error_missing_card_type));
            visaButton.requestFocus();
            return;
        }

        if (creditNumberEditText.getText().toString().isEmpty()) {
            creditNumberEditText.setError(getString(R.string.error_missing_credit_card));
            creditNumberEditText.requestFocus();
            return;
        }

        if (expireMonthSpinner.getSelectedItemPosition() < 0) {
            creditNumberEditText.setError(getString(R.string.error_missing_expire_month));
            expireMonthSpinner.requestFocus();
            return;
        }

        if (expireYearSpinner.getSelectedItemPosition() < 0) {
            creditNumberEditText.setError(getString(R.string.error_missing_expire_year));
            expireYearSpinner.requestFocus();
            return;
        }

        dialog.show();

        String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
        ApiServices.AddPaymentAccount(token, visaButton.isChecked() ? 1 : 2, creditNumberEditText.getText().toString(),
                expireMonthSpinner.getSelectedItemPosition() + 1, expireYearSpinner.getSelectedItemPosition() + startYear, new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        JSONObject result = (JSONObject) response;
                        dialog.hide();
                        if (result.optInt("Code") != 0) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(getString(R.string.title_activity_wallet))
                                    .setMessage(result.optString("Message"))
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                            return;
                        }

                        visaButton.setChecked(false);
                        masterButton.setChecked(false);
                        visaButton.setError(null);
                        creditNumberEditText.setText("");
                        creditNumberEditText.setError(null);
                        expireMonthSpinner.setSelection(0);
                        expireYearSpinner.setSelection(0);

                        loadPaymentAccounts();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        dialog.hide();
                        error.printStackTrace();
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getString(R.string.title_activity_wallet))
                                .setMessage(R.string.error_network_problem)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
    }
}
