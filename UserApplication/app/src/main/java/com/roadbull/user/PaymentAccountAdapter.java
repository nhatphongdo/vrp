package com.roadbull.user;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Do Nhat Phong on 11/4/2015.
 */
public class PaymentAccountAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private ArrayList<JSONObject> list;
    private double credit;

    public PaymentAccountAdapter(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.payment_account_item, list);
        this.context = context;
        this.list = list;
    }

    public void refresh(ArrayList<JSONObject> list, double credit) {
        this.list = list;
        this.credit = credit;
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.payment_account_item, parent, false);

        JSONObject item = list.get(position);
        String cardNumber = item.optString("CardNumber");

        // Hide all number except 4 last
        if (cardNumber != null && cardNumber.length() > 4) {
            int n = cardNumber.length() - 4;
            char[] chars = new char[n];
            Arrays.fill(chars, '*');
            String result = new String(chars);
            if (item.optInt("AccountType") == 1) {
                result = "VISA CARD (" + result;
            } else if (item.optInt("AccountType") == 2) {
                result = "MASTER CARD (" + result;
            }
            cardNumber = result + cardNumber.substring(cardNumber.length() - 4, cardNumber.length()) + ")";
        } else if (cardNumber == null || cardNumber.isEmpty()) {
            if (position == 0) {
                cardNumber = "Credit Account ($" + String.format("%.2f", credit) + ")";
            }
            else {
                cardNumber = "Paypal account or VISA/MC";
            }
        }

        TextView cardNumberView = (TextView) rowView.findViewById(R.id.textView);
        cardNumberView.setText(cardNumber);

        if (item.optBoolean("IsSelected") == true) {
            rowView.setBackgroundColor(Color.rgb(235, 104, 35));
        }

        return rowView;
    }

    public void setSelected(int position) {
        try {
            for (int i = 0; i < list.size(); i++) {
                if (i != position) {
                    list.get(i).put("IsSelected", false);
                } else {
                    list.get(i).put("IsSelected", true);
                }
            }

            this.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
