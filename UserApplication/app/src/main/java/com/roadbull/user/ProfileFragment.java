package com.roadbull.user;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class ProfileFragment extends Fragment implements View.OnFocusChangeListener {

    private OnProfileFragmentListener mListener;

    private int PICK_IMAGE_REQUEST = 1;

    private CircularImageView mAvatarButton;
    private EditText mFullNameView;
    private EditText mEmailView;
    private Spinner mCountryCodeView;
    private EditText mMobileNumberView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;
    private EditText mCompanyEditText;
    private EditText mAddressEditText;
    private EditText mPostalCodeEditText;
    private boolean avatarHasChanged;
    private String avatarFileName;
    private Bitmap avatarBitmap;

    private CountrySpinnerAdapter countryCodeAdapter;

    private View mProgressView;
    private View mProfileFormView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        // Get UI reference
        mAvatarButton = (CircularImageView) view.findViewById(R.id.avatarImageView);
        mFullNameView = (EditText) view.findViewById(R.id.fullNameEditText);
        mEmailView = (EditText) view.findViewById(R.id.emailEditText);
        mCountryCodeView = (Spinner) view.findViewById(R.id.countryCode);
        mMobileNumberView = (EditText) view.findViewById(R.id.mobilePhoneEditText);
        mPasswordView = (EditText) view.findViewById(R.id.password);
        mConfirmPasswordView = (EditText) view.findViewById(R.id.reconfirm_password);
        mCompanyEditText = (EditText) view.findViewById(R.id.companyEditText);
        mAddressEditText = (EditText) view.findViewById(R.id.addressEditText);
        mPostalCodeEditText = (EditText) view.findViewById(R.id.postalCodeEditText);
        avatarHasChanged = false;

        mProfileFormView = view.findViewById(R.id.profile_form_container);
        mProgressView = view.findViewById(R.id.profile_progress);

        mFullNameView.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_FULLNAME_KEY, ""));
        mEmailView.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_EMAIL_KEY, ""));
        final String code = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_COUNTRY_KEY, "");
        mMobileNumberView.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_MOBILE_KEY, ""));
        mCompanyEditText.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_COMPANY_KEY, ""));
        mAddressEditText.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_ADDRESS_KEY, ""));
        mPostalCodeEditText.setText(GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_ZIP_CODE_KEY, ""));

        mMobileNumberView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.submit || id == EditorInfo.IME_NULL) {
                    attemptUpdate();
                    return true;
                }
                return false;
            }
        });

        mFullNameView.setOnFocusChangeListener(this);
        mMobileNumberView.setOnFocusChangeListener(this);
        mPasswordView.setOnFocusChangeListener(this);

        mPostalCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ApiServices.CheckZipCode(mPostalCodeEditText.getText().toString(), new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        JSONObject postal = (JSONObject) response;
                        mAddressEditText.setText(postal.optString("UnitNumber") + " " + postal.optString("StreetName"));
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        mAddressEditText.setText("");
                    }
                });
            }
        });

        mAvatarButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }

        });

        mAvatarButton.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo));
        Bitmap bitmap = GlobalPreferences.getAvatar();
        if (bitmap != null) {
            mAvatarButton.setImageBitmap(bitmap);
        } else {
            String photo = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.USER_PHOTO_KEY, "");
            if (photo != "") {
                (new DownloadImageTask(mAvatarButton)).execute(photo);
            }
        }

        final Button changePassword = (Button) view.findViewById(R.id.change_password_button);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Show password
                mPasswordView.setEnabled(true);
                mConfirmPasswordView.setVisibility(View.VISIBLE);
                changePassword.setVisibility(View.GONE);
            }
        });

        Button submit = (Button) view.findViewById(R.id.update_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptUpdate();
            }
        });

        // Load countries
        ApiServices.Countries(new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) {
                JSONArray countries = (JSONArray) response;
                ArrayList<JSONObject> list = new ArrayList<>();
                try {
                    int selectedPos = -1;
                    for (int i = 0; i < countries.length(); i++) {
                        list.add(countries.getJSONObject(i));
                        if (countries.getJSONObject(i).optString("CountryCode") == code) {
                            selectedPos = i;
                        }
                    }

                    // Create an ArrayAdapter using the string array and a default spinner layout
                    countryCodeAdapter = new CountrySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, list);
                    countryCodeAdapter.setForeColor(Color.BLACK);
                    // Apply the adapter to the spinner
                    mCountryCodeView.setAdapter(countryCodeAdapter);

                    if (selectedPos != -1) {
                        mCountryCodeView.setSelection(selectedPos);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
            }
        });

        return view;
    }

    private void attemptUpdate() {
        // Reset errors
        mFullNameView.setError(null);
        mMobileNumberView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);

        // Store values at the time of the register attempt.
        final String password = mPasswordView.getText().toString();
        final String confirmPassword = mConfirmPasswordView.getText().toString();
        final String fullName = mFullNameView.getText().toString();
        final String email = mEmailView.getText().toString();
        final String mobileNumber = mMobileNumberView.getText().toString();
        final String countryCode = ((JSONObject) mCountryCodeView.getSelectedItem()).optString("CountryCode");
        final String company = mCompanyEditText.getText().toString();
        final String address = mAddressEditText.getText().toString();
        final String postalCode = mPostalCodeEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check Full name
        if (TextUtils.isEmpty(fullName)) {
            mFullNameView.setError(getString(R.string.error_invalid_fullname));
            focusView = mFullNameView;
            cancel = true;
        }

        // Check Email
        if (TextUtils.isEmpty(email)) {
            mFullNameView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && confirmPassword != password) {
            mFullNameView.setError(getString(R.string.error_not_match_passwords));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        // Check country code
        if (TextUtils.isEmpty(countryCode)) {
            mMobileNumberView.setError(getString(R.string.error_invalid_country_code));
            focusView = mMobileNumberView;
            cancel = true;
        }

        // Check mobile number
        if (TextUtils.isEmpty(mobileNumber) || !GlobalPreferences.isPhoneValid(mobileNumber)) {
            mMobileNumberView.setError(getString(R.string.error_invalid_mobile));
            focusView = mMobileNumberView;
            cancel = true;
        }

        // Check postal code
        if (TextUtils.isEmpty(postalCode)) {
            mPostalCodeEditText.setError(getString(R.string.error_invalid_postal_code));
            focusView = mPostalCodeEditText;
            cancel = true;
        }

        // Check address
        if (TextUtils.isEmpty(address)) {
            mAddressEditText.setError(getString(R.string.error_invalid_address));
            focusView = mAddressEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user register attempt.
            showProgress(true);

            final String token = GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.TOKEN_KEY, "");
            if (avatarHasChanged) {
                ApiServices.UploadFile(ApiServices.UploadAccountPhotoServiceUrl, token, avatarFileName, new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        // Upload finish
                        showProgress(false);
                        if (((JSONObject) response).getInt("Code") == 0) {
                            updateProfile(token, fullName, email, countryCode, mobileNumber, password, ((JSONObject) response).optString("Path"), company, postalCode, address);
                        } else {
                            if (getActivity() != null) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(getString(R.string.title_activity_profile))
                                        .setMessage(((JSONObject) response).getString("Message"))
                                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        error.printStackTrace();
                        showProgress(false);
                        if (getActivity() != null) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(getString(R.string.title_activity_profile))
                                    .setMessage(getString(R.string.error_network_problem))
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    }
                });
            } else {
                updateProfile(token, fullName, email, countryCode, mobileNumber, password, "", company, postalCode, address);
            }
        }
    }

    private void updateProfile(final String token, final String fullName, final String email, final String countryCode, final String mobileNumber, final String password, final String photo, final String company, final String postalCode, final String address) {
        ApiServices.UpdateProfile(token, fullName, email, countryCode, mobileNumber, password, photo, company, postalCode, address,
                new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) {
                        try {
                            final JSONObject registerResult = (JSONObject) response;
                            if (registerResult.getInt("Code") != 0) {
                                // Error
                                showProgress(false);
                                mFullNameView.setError(registerResult.getString("Message").replaceAll(" \\| ", "\n"));
                                mFullNameView.requestFocus();
                            } else {
                                if (GlobalPreferences.getInstance(getActivity()).get(GlobalPreferences.REMEMBER_ACCOUNT, false) == true && !TextUtils.isEmpty(password)) {
                                    GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_PASSWORD_KEY, password);
                                }

                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_COUNTRY_KEY, countryCode);
                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_MOBILE_KEY, mobileNumber);
                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_FULLNAME_KEY, fullName);
                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_PHOTO_KEY, photo);
                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_ZIP_CODE_KEY, postalCode);
                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_COMPANY_KEY, company);
                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_ADDRESS_KEY, address);
                                GlobalPreferences.getInstance(getActivity()).save(GlobalPreferences.USER_EMAIL_KEY, email);
                                if (avatarBitmap != null) {
                                    GlobalPreferences.getInstance(getActivity()).setAvatar(avatarBitmap);
                                }

                                if (mListener != null) {
                                    if (avatarBitmap != null) {
                                        mListener.onAvatarLoaded(avatarBitmap);
                                    }
                                    mListener.onInfoChanged(fullName, email);
                                }

                                showProgress(false);

                                // Show success message
                                if (getActivity() != null) {
                                    new AlertDialog.Builder(getActivity())
                                            .setTitle(getString(R.string.title_activity_profile))
                                            .setMessage(getString(R.string.update_info_successful))
                                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_info)
                                            .show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showProgress(false);
                            mFullNameView.setError(getString(R.string.error_unknown_error));
                            mFullNameView.requestFocus();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        error.printStackTrace();
                        showProgress(false);
                        mFullNameView.setError(getString(R.string.error_network_problem));
                        mFullNameView.requestFocus();
                    }
                });
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mProfileFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v.getId() == R.id.fullNameEditText || v.getId() == R.id.emailEditText || v.getId() == R.id.password ||
                v.getId() == R.id.reconfirm_password || v.getId() == R.id.mobilePhoneEditText || v.getId() == R.id.companyEditText ||
                v.getId() == R.id.addressEditText) && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                mAvatarButton.setImageBitmap(bitmap);

                avatarBitmap = bitmap;
                avatarFileName = getPath(getActivity(), uri);
                avatarHasChanged = true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                avatarHasChanged = false;
            } catch (IOException e) {
                e.printStackTrace();
                avatarHasChanged = false;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        try {
            mListener = (OnProfileFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnProfileFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnProfileFragmentListener {
        void onAvatarLoaded(Bitmap bitmap);

        void onInfoChanged(String name, String email);
    }
}
