package com.roadbull.user;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Do Nhat Phong on 10/19/2015.
 */
public class ApiServices {

    interface ServiceCallbackInterface {

        void onSuccess(int statusCode, Object response) throws JSONException;

        void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error);

    }

    public final static String BaseUrl = "http://cds.roadbull.com/";
    public final static String LoginServiceUrl = BaseUrl + "api/accounts/login";
    public final static String ProfileServiceUrl = BaseUrl + "api/accounts/profile";
    public final static String ForgotPasswordServiceUrl = BaseUrl + "api/accounts/forgotpassword";
    public final static String UpdateProfileServiceUrl = BaseUrl + "api/accounts/update";
    public final static String UploadAccountPhotoServiceUrl = BaseUrl + "api/accounts/uploadcustomer";
    public final static String UpdateNotificationTokenServiceUrl = BaseUrl + "api/accounts/notificationToken";
    public final static String CountriesServiceUrl = BaseUrl + "api/locations/countries";
    public final static String CheckZipCodeServiceUrl = BaseUrl + "api/locations/checkZipCode";
    public final static String SettingUrlsServiceUrl = BaseUrl + "api/settings/urls";
    public final static String SettingOffDaysServiceUrl = BaseUrl + "api/settings/offdays";
    public final static String OrderProductTypesServiceUrl = BaseUrl + "api/orders/producttypes";
    public final static String OrderPricesServiceUrl = BaseUrl + "api/orders/price";
    public final static String OrderTrackingServiceUrl = BaseUrl + "api/orders/tracking";
    public final static String OrderBookingServiceUrl = BaseUrl + "api/orders/book";
    public final static String OrderBraintreeTokenServiceUrl = BaseUrl + "api/orders/token";
    public final static String OrderCheckoutServiceUrl = BaseUrl + "api/orders/checkout";
    public final static String PaymentAccountsListServiceUrl = BaseUrl + "api/paymentaccounts/list";
    public final static String PaymentAccountsAddServiceUrl = BaseUrl + "api/paymentaccounts/add";


    protected static void InvokeWebservice(String serviceUrl, String token, RequestParams params, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }
        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void UploadFile(String serviceUrl, String token, String filePath, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }

        File file = new File(filePath);
        RequestParams params = new RequestParams();
        try {
            params.put("uploadFile", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onFailure(500, null, null, e);
            }
        }

        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void UploadFile(String serviceUrl, String token, InputStream input, String filename, final ServiceCallbackInterface callback) {
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }

        RequestParams params = new RequestParams();
        params.put("uploadFile", input, filename);

        client.post(serviceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void Login(String username, String password, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("UserName", username);
        params.add("Password", password);
        InvokeWebservice(LoginServiceUrl, null, params, callback);
    }

    public static void Profile(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(ProfileServiceUrl, token, null, callback);
    }

    public static void ForgotPassword(String username, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("UserName", username);
        InvokeWebservice(ForgotPasswordServiceUrl, null, params, callback);
    }

    public static void Countries(ServiceCallbackInterface callback) {
        InvokeWebservice(CountriesServiceUrl, null, null, callback);
    }

    public static void CheckZipCode(String zipCode, ServiceCallbackInterface callback) {
        InvokeWebservice(CheckZipCodeServiceUrl + "/" + zipCode, null, null, callback);
    }

    public static void UrlsSetting(ServiceCallbackInterface callback) {
        InvokeWebservice(SettingUrlsServiceUrl, null, null, callback);
    }

    public static void OffDaysSetting(ServiceCallbackInterface callback) {
        InvokeWebservice(SettingOffDaysServiceUrl, null, null, callback);
    }

    public static void UpdateProfile(String token, String fullName, String email, String countryCode, String mobileNumber, String password, String photo, String company, String postalCode, String address, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("Name", fullName);
        params.add("Email", email);
        params.add("CountryCode", countryCode);
        params.add("MobileNumber", mobileNumber);
        params.add("Password", password);
        params.add("Photo", photo);
        params.add("Company", company);
        params.add("Address", address);
        params.add("PostalCode", postalCode);
        InvokeWebservice(UpdateProfileServiceUrl, token, params, callback);
    }

    public static void UpdateNotificationToken(String token, String notificationToken, final ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("Token", notificationToken);
        params.add("PhoneType", "1"); // Android
        SyncHttpClient client = new SyncHttpClient();
        if (token != null && token != "") {
            client.addHeader("Authorization", "Bearer " + token);
        }
        client.post(UpdateNotificationTokenServiceUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responseString;
                try {
                    // JSON Object
                    responseString = new String(responseBody, "UTF-8");
                    Object obj;
                    if (responseString.startsWith("[")) {
                        obj = new JSONArray(responseString);
                    } else {
                        obj = new JSONObject(responseString);
                    }
                    if (callback != null) {
                        callback.onSuccess(statusCode, obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.onFailure(statusCode, headers, responseBody, e);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                if (callback != null) {
                    callback.onFailure(statusCode, headers, responseBody, error);
                }
            }
        });
    }

    public static void ProductTypes(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(OrderProductTypesServiceUrl, token, null, callback);
    }

    public static void Price(String token, int productTypeId, int serviceId, int sizeId, String customerId, String promoCode, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("ProductTypeId", String.valueOf(productTypeId));
        params.add("ServiceId", String.valueOf(serviceId));
        params.add("SizeId", String.valueOf(sizeId));
        params.add("CustomerId", customerId);
        params.add("AddOnCode", promoCode);
        InvokeWebservice(OrderPricesServiceUrl, token, params, callback);
    }

    public static void Tracking(String consignmentNumbers, ServiceCallbackInterface callback) {
        InvokeWebservice(OrderTrackingServiceUrl + "/" + consignmentNumbers, null, null, callback);
    }

    public static void PaymentAccounts(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(PaymentAccountsListServiceUrl, token, null, callback);
    }

    public static void AddPaymentAccount(String token, int accountType, String cardNumber, int expireMonth, int expireYear, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("AccountType", String.valueOf(accountType));
        params.add("CardNumber", cardNumber);
        params.add("ExpireMonth", String.valueOf(expireMonth));
        params.add("ExpireYear", String.valueOf(expireYear));
        InvokeWebservice(PaymentAccountsAddServiceUrl, token, params, callback);
    }

    public static void Book(String token, Order order, long paymentAccountId, ServiceCallbackInterface callback) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        RequestParams params = new RequestParams();
        params.add("FromName", order.FromName);
        params.add("FromZipCode", order.FromZipCode);
        params.add("FromAddress", order.FromAddress);
        params.add("FromMobilePhone", order.FromMobilePhone);
        params.add("ToName", order.ToName);
        params.add("ToZipCode", order.ToZipCode);
        params.add("ToAddress", order.ToAddress);
        params.add("ToMobilePhone", order.ToMobilePhone);
        params.add("ServiceId", String.valueOf(order.ServiceId));
        params.add("ProductTypeId", String.valueOf(order.ProductTypeId));
        params.add("SizeId", String.valueOf(order.SizeId));
        params.add("PickupTimeSlotId", String.valueOf(order.PickupTimeSlotId));
        params.add("PickupDate", format.format(order.PickupDate));
        params.add("DeliveryTimeSlotId", String.valueOf(order.DeliveryTimeSlotId));
        params.add("DeliveryDate", format.format(order.DeliveryDate));
        params.add("Remark", order.Remark);
        params.add("PromoCode", order.PromoCode);
        params.add("CustomerId", order.CustomerId);
        params.add("PaymentTypeId", String.valueOf(order.PaymentTypeId));
        params.add("PaymentAccountId", String.valueOf(paymentAccountId));

        InvokeWebservice(OrderBookingServiceUrl, token, params, callback);
    }

    public static void Token(String token, ServiceCallbackInterface callback) {
        InvokeWebservice(OrderBraintreeTokenServiceUrl, token, null, callback);
    }

    public static void Checkout(String token, String consignments, String paymentNonce, ServiceCallbackInterface callback) {
        RequestParams params = new RequestParams();
        params.add("OrderId", consignments);
        params.add("PaymentMethodNonce", paymentNonce);

        InvokeWebservice(OrderCheckoutServiceUrl, token, params, callback);
    }
}
