package com.roadbull.user;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Do Nhat Phong on 10/30/2015.
 */
public class CountrySpinnerAdapter extends ArrayAdapter<JSONObject> {

    private Context context;
    private ArrayList<JSONObject> list;
    private LayoutInflater inflater;
    private int foreColor;

    public CountrySpinnerAdapter(Context context, int textViewResourceId, ArrayList<JSONObject> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        foreColor = -1;
    }

    public int getCount() {
        return list.size();
    }

    public JSONObject getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public void setForeColor(int color) {
        foreColor = color;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.spinner_item, parent, false);
        TextView label = (TextView) row.findViewById(R.id.textView);
        if (foreColor != -1) {
            label.setTextColor(foreColor);
        }
        try {
            label.setText(list.get(position).getString("CountryCode"));
        } catch (JSONException e) {
            label.setText("");
            e.printStackTrace();
        }
        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.spinner_item, parent, false);
        TextView label = (TextView) row.findViewById(R.id.textView);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion > Build.VERSION_CODES.KITKAT) {
            label.setTextColor(Color.BLACK);
        } else {
            label.setTextColor(Color.WHITE);
        }

        try {
            label.setText(list.get(position).getString("CountryName"));
        } catch (JSONException e) {
            label.setText("");
            e.printStackTrace();
        }
        return row;
    }

}
