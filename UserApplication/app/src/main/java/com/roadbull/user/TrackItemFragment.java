package com.roadbull.user;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrackItemFragment extends Fragment {

    private EditText searchEditText;
    private Button trackNowButton;
    private ImageView statusImageView;
    private TextView statusTextView;
    private TextView status1;
    private TextView status2;
    private TextView status3;
    private TextView status4;
    private TextView status5;
    private TextView time1;
    private TextView time2;
    private TextView time3;
    private TextView time4;
    private TextView time5;

    public TrackItemFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_track_item, container, false);

        statusImageView = (ImageView) view.findViewById(R.id.statusImageView);
        statusTextView = (TextView) view.findViewById(R.id.statusTextView);
        searchEditText = (EditText) view.findViewById(R.id.searchEditText);
        trackNowButton = (Button) view.findViewById(R.id.trackNowButton);
        status1 = (TextView) view.findViewById(R.id.status1);
        status2 = (TextView) view.findViewById(R.id.status2);
        status3 = (TextView) view.findViewById(R.id.status3);
        status4 = (TextView) view.findViewById(R.id.status4);
        status5 = (TextView) view.findViewById(R.id.status5);
        time1 = (TextView) view.findViewById(R.id.time1);
        time2 = (TextView) view.findViewById(R.id.time2);
        time3 = (TextView) view.findViewById(R.id.time3);
        time4 = (TextView) view.findViewById(R.id.time4);
        time5 = (TextView) view.findViewById(R.id.time5);

        trackNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                track();
            }
        });

        return view;
    }

    private void track() {
        if (searchEditText.getText().toString().isEmpty()) {
            searchEditText.setError(getString(R.string.error_missing_tracking_number));
            searchEditText.requestFocus();
            return;
        }

        ApiServices.Tracking(searchEditText.getText().toString(), new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                JSONObject result = (JSONObject) response;
                if (result.optInt("Code", 1) > 0) {
                    searchEditText.setError(result.optString("Message"));
                    searchEditText.requestFocus();
                    return;
                }

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                JSONArray orders = result.optJSONArray("Orders");
                for (int i = 0; i < orders.length(); i++) {
                    JSONObject order = orders.getJSONObject(i);
                    int status = order.optInt("Status", 0);
                    switch (status) {
                        case 0:
                            statusImageView.setImageBitmap(null);
                            statusTextView.setText(getString(R.string.status_submitted));
                            break;
                        case 1:
                            statusImageView.setImageBitmap(null);
                            statusTextView.setText(getString(R.string.status_confirmed));
                            break;
                        case 3:
                            statusImageView.setImageResource(R.drawable.ic_item_collected);
                            statusTextView.setText(getString(R.string.status_collected));
                            break;
                        case 4:
                            statusImageView.setImageBitmap(null);
                            statusTextView.setText(getString(R.string.status_pickup_unsuccessful));
                            break;
                        case 5:
                            statusImageView.setImageResource(R.drawable.ic_item_at_warehouse);
                            statusTextView.setText(getString(R.string.status_at_warehouse));
                            break;
                        case 6:
                            statusImageView.setImageResource(R.drawable.ic_delivery_in_progress);
                            statusTextView.setText(getString(R.string.status_delivery_in_progress));
                            break;
                        case 7:
                            statusImageView.setImageResource(R.drawable.ic_delivery_successful);
                            statusTextView.setText(getString(R.string.status_delivery_successful));
                            break;
                        case 8:
                            statusImageView.setImageBitmap(null);
                            statusTextView.setText(getString(R.string.status_canceled));
                            break;
                        case 9:
                            statusImageView.setImageResource(R.drawable.ic_delivery_unsuccessful);
                            statusTextView.setText(getString(R.string.status_delivery_unsuccessful));
                            break;
                    }

                    status1.setVisibility(View.GONE);
                    status2.setVisibility(View.GONE);
                    status3.setVisibility(View.GONE);
                    status4.setVisibility(View.GONE);
                    status5.setVisibility(View.GONE);
                    time1.setVisibility(View.GONE);
                    time2.setVisibility(View.GONE);
                    time3.setVisibility(View.GONE);
                    time4.setVisibility(View.GONE);
                    time5.setVisibility(View.GONE);

                    try {
                        if (status >= 1) {
                            // Confirmed
                            status1.setText(getString(R.string.status_confirmed));
                            time1.setText(format.format(inputFormat.parse(order.optString("ConfirmedOn"))));
                            status1.setVisibility(View.VISIBLE);
                            time1.setVisibility(View.VISIBLE);
                        }

                        if (status == 8) {
                            // Canceled
                            status2.setText(getString(R.string.status_canceled));
                            time2.setText(format.format(inputFormat.parse(order.optString("CanceledOn"))));
                            status2.setVisibility(View.VISIBLE);
                            time2.setVisibility(View.VISIBLE);
                        } else if (status == 4) {
                            status2.setText(getString(R.string.status_pickup_unsuccessful));
                            time2.setText(format.format(inputFormat.parse(order.optString("CollectedOn"))));
                            status2.setVisibility(View.VISIBLE);
                            time2.setVisibility(View.VISIBLE);
                        } else if (status >= 3) {
                            status2.setText(getString(R.string.status_collected));
                            time2.setText(format.format(inputFormat.parse(order.optString("CollectedOn"))));
                            status2.setVisibility(View.VISIBLE);
                            time2.setVisibility(View.VISIBLE);
                        }

                        if (status >= 5 && status != 8) {
                            status3.setText(getString(R.string.status_at_warehouse));
                            time3.setText(format.format(inputFormat.parse(order.optString("ProceededOn"))));
                            status3.setVisibility(View.VISIBLE);
                            time3.setVisibility(View.VISIBLE);
                        }

                        if (status >= 6 && status != 8) {
                            status4.setText(getString(R.string.status_delivery_in_progress));
                            time4.setText(format.format(inputFormat.parse(order.optString("ProceededOutOn"))));
                            status4.setVisibility(View.VISIBLE);
                            time4.setVisibility(View.VISIBLE);
                        }

                        if (status == 7) {
                            status5.setText(getString(R.string.status_delivery_successful));
                            time5.setText(format.format(inputFormat.parse(order.optString("DeliveredOn"))));
                            status5.setVisibility(View.VISIBLE);
                            time5.setVisibility(View.VISIBLE);
                        } else if (status == 9) {
                            status5.setText(getString(R.string.status_delivery_unsuccessful));
                            time5.setText(format.format(inputFormat.parse(order.optString("DeliveredOn"))));
                            status5.setVisibility(View.VISIBLE);
                            time5.setVisibility(View.VISIBLE);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    break;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                searchEditText.setError(getString(R.string.error_network_problem));
                searchEditText.requestFocus();
            }
        });
    }
}
