package com.roadbull.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        ApiServices.UrlsSetting(new ApiServices.ServiceCallbackInterface() {
            @Override
            public void onSuccess(int statusCode, Object response) throws JSONException {
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.COMPANY_INFO_URL, ((JSONObject) response).optString("CompanyInfo"));
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.TERMS_AND_CONDITIONS_URL, ((JSONObject) response).optString("TermsAndConditions"));
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.NEWS_URL, ((JSONObject) response).optString("News"));
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.SERVICE_GUIDELINES_URL, ((JSONObject) response).optString("ServiceGuidelines"));
                GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.TYPE_OF_SERVICE_URL, ((JSONObject) response).optString("TypeOfService"));

                ApiServices.OffDaysSetting(new ApiServices.ServiceCallbackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Object response) throws JSONException {
                        GlobalPreferences.getInstance(getContext()).save(GlobalPreferences.OFF_DAYS_SETTING, ((JSONObject) response).optString("OutOfServiceDays"));

                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        finish();
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        error.printStackTrace();
                        new AlertDialog.Builder(getContext())
                                .setTitle(getString(R.string.app_name))
                                .setMessage(getString(R.string.error_network_problem))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.app_name))
                        .setMessage(getString(R.string.error_network_problem))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private SplashActivity getContext() {
        return this;
    }
}
